﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : Actualiza_InstalacionBussines
/// File                    : Actualiza_InstalacionBussines.cs
/// Creation date           : 20/09/2017
/// Creation time           : 11:33 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class Actualiza_Instalacion
    {

        #region Constructors
        public Actualiza_Instalacion() { }
        #endregion

        /// <summary>
        ///Adds Actualiza_Instalacion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(Actualiza_InstalacionEntity objActualiza_Instalacion)
        {
            int result = ProviderSoftv.Actualiza_Instalacion.AddActualiza_Instalacion(objActualiza_Instalacion);
            return result;
        }     

        /// <summary>
        ///Update Actualiza_Instalacion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(Actualiza_InstalacionEntity objActualiza_Instalacion)
        {
            int result = ProviderSoftv.Actualiza_Instalacion.EditActualiza_Instalacion(objActualiza_Instalacion);
            return result;
        }
        /// <summary>
        ///Get Actualiza_Instalacion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<Actualiza_InstalacionEntity> GetAll(int? CLV_LLAVE, int? Clv_TipoCliente, int? opc)
        {
            List<Actualiza_InstalacionEntity> entities = new List<Actualiza_InstalacionEntity>();
            entities = ProviderSoftv.Actualiza_Instalacion.GetActualiza_Instalacion(CLV_LLAVE, Clv_TipoCliente, opc);

            return entities ?? new List<Actualiza_InstalacionEntity>();
        }
    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : AdelantarBussines
/// File                    : AdelantarBussines.cs
/// Creation date           : 28/10/2016
/// Creation time           : 12:27 p. m.
///</summary> 
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class Adelantar
    {

        #region Constructors
        public Adelantar() { }
        #endregion



        /// <summary>
        ///Get Adelantar By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static AdelantarEntity GetOneDeep(long? IdSession)
        {
            AdelantarEntity result = ProviderSoftv.Adelantar.GetAdelantarById(IdSession);

            return result;
        }


        [DataObjectMethod(DataObjectMethodType.Select)]
        public static AdelantarEntity GetDeepChecaAdelantarPagosDif(long? Contrato)
        {
            AdelantarEntity result = ProviderSoftv.Adelantar.GetDeepChecaAdelantarPagosDif(Contrato);

            return result;
        }


    }




    #region Customs Methods

    #endregion
}

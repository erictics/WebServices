﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : BusCliPorContrato_FacBussines
/// File                    : BusCliPorContrato_FacBussines.cs
/// Creation date           : 13/10/2016
/// Creation time           : 06:52 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class BusCliPorContrato_Fac
    {

        #region Constructors
        public BusCliPorContrato_Fac() { }
        #endregion
       
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<BusCliPorContrato_FacEntity> GetAll(int? Id, String ContratoC)
        {
            List<BusCliPorContrato_FacEntity> entities = new List<BusCliPorContrato_FacEntity>();
            entities = ProviderSoftv.BusCliPorContrato_Fac.GetBusCliPorContrato_Fac(Id, ContratoC);

            return entities ?? new List<BusCliPorContrato_FacEntity>();
        } 


    }




    #region Customs Methods

    #endregion
}

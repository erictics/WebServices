﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : BuscaBloqueadoBussines
/// File                    : BuscaBloqueadoBussines.cs
/// Creation date           : 13/02/2017
/// Creation time           : 01:30 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class BuscaBloqueado
    {

        #region Constructors
        public BuscaBloqueado() { }
        #endregion       

        /// <summary>
        ///Get BuscaBloqueado By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static BuscaBloqueadoEntity GetOne(long? Contrato)
        {
            BuscaBloqueadoEntity result = ProviderSoftv.BuscaBloqueado.GetBuscaBloqueadoById(Contrato);

            return result;
        }
        /// <summary>
        ///Get BuscaBloqueado By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static BuscaBloqueadoEntity GetOneDeep(long? Contrato)
        {
            BuscaBloqueadoEntity result = ProviderSoftv.BuscaBloqueado.GetBuscaBloqueadoById(Contrato);

            return result;
        }
    }




    #region Customs Methods

    #endregion
}

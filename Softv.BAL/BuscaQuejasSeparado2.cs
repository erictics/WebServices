﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : BuscaQuejasSeparado2Bussines
/// File                    : BuscaQuejasSeparado2Bussines.cs
/// Creation date           : 10/02/2017
/// Creation time           : 10:40 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class BuscaQuejasSeparado2
    {

        #region Constructors
        public BuscaQuejasSeparado2() { }
        #endregion


        /// <summary>
        ///Get BuscaQuejasSeparado2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<BuscaQuejasSeparado2Entity> GetAll(int? Clv_TipSer, long? Clv_Queja, String Contrato, String NOMBRE, String AP, String AM, String CALLE, String NUMERO, String SetupBox, String Status, int? Op, int? ClvColonia, int? IdCompania, int? ClvUsuario, bool SoloNivel2, int? NoTicket)
        {
            List<BuscaQuejasSeparado2Entity> entities = new List<BuscaQuejasSeparado2Entity>();
            entities = ProviderSoftv.BuscaQuejasSeparado2.GetBuscaQuejasSeparado2(Clv_TipSer, Clv_Queja, Contrato, NOMBRE, AP, AM, CALLE, NUMERO, SetupBox, Status, Op, ClvColonia, IdCompania, ClvUsuario, SoloNivel2, NoTicket);

            return entities ?? new List<BuscaQuejasSeparado2Entity>();
        }

     


    }




    #region Customs Methods

    #endregion
}

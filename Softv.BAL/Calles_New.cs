﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// 
/// Generated by            : Class Generator (c) 2014
/// Description             : Calles_NewBussines
/// File                    : Calles_NewBussines.cs
/// Creation date           : 25/09/2017
/// Creation time           : 05:09 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class Calles_New
    {

        #region Constructors
        public Calles_New() { }
        #endregion

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(Calles_NewEntity objCalles_New)
        {
            int result = ProviderSoftv.Calles_New.AddCalles_New(objCalles_New);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.Calles_New.DeleteCalles_New();
            return resultado;
        }

        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(Calles_NewEntity objCalles_New)
        {
            int result = ProviderSoftv.Calles_New.EditCalles_New(objCalles_New);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<Calles_NewEntity> GetAll(int? Clv_Calle, String NOMBRE, int? Op, int? Idcompania)
        {
            List<Calles_NewEntity> entities = new List<Calles_NewEntity>();
            entities = ProviderSoftv.Calles_New.GetCalles_New(Clv_Calle, NOMBRE, Op, Idcompania);

            return entities ?? new List<Calles_NewEntity>();
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<Calles_NewEntity> GetAll(List<int> lid)
        {
            List<Calles_NewEntity> entities = new List<Calles_NewEntity>();
            entities = ProviderSoftv.Calles_New.GetCalles_New(lid);
            return entities ?? new List<Calles_NewEntity>();
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static Calles_NewEntity GetOne(int? Clv_Calle)
        {
            Calles_NewEntity result = ProviderSoftv.Calles_New.GetCalles_NewById(Clv_Calle);

            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static Calles_NewEntity GetOneDeep(int? Clv_Calle)
        {
            Calles_NewEntity result = ProviderSoftv.Calles_New.GetCalles_NewById(Clv_Calle);

            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static ValidaEntity GetValidaColServCli(long? IdCompania, long? Clv_Estado, long? Clv_Ciudad, long? Clv_Localidad, long? Clv_Colonia)
        {
            ValidaEntity result = ProviderSoftv.Calles_New.GetValidaColServCli(IdCompania, Clv_Estado, Clv_Ciudad, Clv_Localidad, Clv_Colonia);
            return result;
        }








        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<Calles_NewEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<Calles_NewEntity> entities = new SoftvList<Calles_NewEntity>();
            entities = ProviderSoftv.Calles_New.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<Calles_NewEntity>();
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<Calles_NewEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<Calles_NewEntity> entities = new SoftvList<Calles_NewEntity>();
            entities = ProviderSoftv.Calles_New.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<Calles_NewEntity>();
        }

    }




    #region Customs Methods

    #endregion
}

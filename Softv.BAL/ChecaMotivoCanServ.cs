﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ChecaMotivoCanServ
    {

        #region Constructors
        public ChecaMotivoCanServ() { }
        #endregion
        
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ChecaMotivoCanServEntity GetOne(long? ClvOrden)
        {
            ChecaMotivoCanServEntity result = ProviderSoftv.ChecaMotivoCanServ.GetChecaMotivoCanServById(ClvOrden);
            return result;
        }
     
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ChecaMotivoCanServEntity GetOneDeep(long? ClvOrden){
            ChecaMotivoCanServEntity result = ProviderSoftv.ChecaMotivoCanServ.GetChecaMotivoCanServById(ClvOrden);
            return result;
        }
        
    }




    #region Customs Methods

    #endregion
}

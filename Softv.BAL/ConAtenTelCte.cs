﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ConAtenTelCteBussines
/// File                    : ConAtenTelCteBussines.cs
/// Creation date           : 04/07/2017
/// Creation time           : 11:13 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ConAtenTelCte
    {

        #region Constructors
        public ConAtenTelCte() { }
        #endregion
        
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ConAtenTelCteEntity GetOne(long? Contrato)
        {
            ConAtenTelCteEntity result = ProviderSoftv.ConAtenTelCte.GetConAtenTelCteById(Contrato);

            return result;
        }

        /// <summary>
        ///Get ConAtenTelCte By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ConAtenTelCteEntity GetOneDeep(long? Contrato)
        {
            ConAtenTelCteEntity result = ProviderSoftv.ConAtenTelCte.GetConAtenTelCteById(Contrato);

            return result;
        }




    }




    #region Customs Methods

    #endregion
}

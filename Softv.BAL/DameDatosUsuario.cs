﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : DameDatosUsuarioBussines
/// File                    : DameDatosUsuarioBussines.cs
/// Creation date           : 14/10/2016
/// Creation time           : 10:52 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class DameDatosUsuario
    {

        #region Constructors
        public DameDatosUsuario() { }
        #endregion

        /// <summary>
        ///Adds DameDatosUsuario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(DameDatosUsuarioEntity objDameDatosUsuario)
        {
            int result = ProviderSoftv.DameDatosUsuario.AddDameDatosUsuario(objDameDatosUsuario);
            return result;
        }

        /// <summary>
        ///Delete DameDatosUsuario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.DameDatosUsuario.DeleteDameDatosUsuario();
            return resultado;
        }

        /// <summary>
        ///Update DameDatosUsuario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(DameDatosUsuarioEntity objDameDatosUsuario)
        {
            int result = ProviderSoftv.DameDatosUsuario.EditDameDatosUsuario(objDameDatosUsuario);
            return result;
        }

        /// <summary>
        ///Get DameDatosUsuario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<DameDatosUsuarioEntity> GetAll()
        {
            List<DameDatosUsuarioEntity> entities = new List<DameDatosUsuarioEntity>();
            entities = ProviderSoftv.DameDatosUsuario.GetDameDatosUsuario();

            return entities ?? new List<DameDatosUsuarioEntity>();
        }

        /// <summary>
        ///Get DameDatosUsuario List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<DameDatosUsuarioEntity> GetAll(List<int> lid)
        {
            List<DameDatosUsuarioEntity> entities = new List<DameDatosUsuarioEntity>();
            entities = ProviderSoftv.DameDatosUsuario.GetDameDatosUsuario(lid);
            return entities ?? new List<DameDatosUsuarioEntity>();
        }

        /// <summary>
        ///Get DameDatosUsuario By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static DameDatosUsuarioEntity GetOne()
        {
            DameDatosUsuarioEntity result = ProviderSoftv.DameDatosUsuario.GetDameDatosUsuarioById();

            return result;
        }

        /// <summary>
        ///Get DameDatosUsuario By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static DameDatosUsuarioEntity GetOneDeep()
        {
            DameDatosUsuarioEntity result = ProviderSoftv.DameDatosUsuario.GetDameDatosUsuarioById();

            return result;
        }



        /// <summary>
        ///Get DameDatosUsuario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<DameDatosUsuarioEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<DameDatosUsuarioEntity> entities = new SoftvList<DameDatosUsuarioEntity>();
            entities = ProviderSoftv.DameDatosUsuario.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<DameDatosUsuarioEntity>();
        }

        /// <summary>
        ///Get DameDatosUsuario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<DameDatosUsuarioEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<DameDatosUsuarioEntity> entities = new SoftvList<DameDatosUsuarioEntity>();
            entities = ProviderSoftv.DameDatosUsuario.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<DameDatosUsuarioEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : DatoBancarioBussines
/// File                    : DatoBancarioBussines.cs
/// Creation date           : 27/02/2016
/// Creation time           : 01:07 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class DatoBancario
    {

        #region Constructors
        public DatoBancario() { }
        #endregion

        /// <summary>
        ///Adds DatoBancario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(DatoBancarioEntity objDatoBancario)
        {
            int result = ProviderSoftv.DatoBancario.AddDatoBancario(objDatoBancario);
            return result;
        }

        /// <summary>
        ///Delete DatoBancario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(long? IdContrato)
        {
            int resultado = ProviderSoftv.DatoBancario.DeleteDatoBancario(IdContrato);
            return resultado;
        }

        /// <summary>
        ///Update DatoBancario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(DatoBancarioEntity objDatoBancario)
        {
            int result = ProviderSoftv.DatoBancario.EditDatoBancario(objDatoBancario);
            return result;
        }

        /// <summary>
        ///Get DatoBancario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<DatoBancarioEntity> GetAll()
        {
            List<DatoBancarioEntity> entities = new List<DatoBancarioEntity>();
            entities = ProviderSoftv.DatoBancario.GetDatoBancario();

            List<BancoEntity> lBanco = ProviderSoftv.Banco.GetBanco(entities.Where(x => x.IdBanco.HasValue).Select(x => x.IdBanco.Value).ToList());
            lBanco.ForEach(XBanco => entities.Where(x => x.IdBanco.HasValue).Where(x => x.IdBanco == XBanco.IdBanco).ToList().ForEach(y => y.Banco = XBanco));

            List<TipoTarjetaEntity> lTipoTarjeta = ProviderSoftv.TipoTarjeta.GetTipoTarjeta(entities.Where(x => x.IdTipoTarjeta.HasValue).Select(x => x.IdTipoTarjeta.Value).ToList());
            lTipoTarjeta.ForEach(XTipoTarjeta => entities.Where(x => x.IdTipoTarjeta.HasValue).Where(x => x.IdTipoTarjeta == XTipoTarjeta.IdTipoTarjeta).ToList().ForEach(y => y.TipoTarjeta = XTipoTarjeta));

            return entities ?? new List<DatoBancarioEntity>();
        }

        /// <summary>
        ///Get DatoBancario List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<DatoBancarioEntity> GetAll(List<int> lid)
        {
            List<DatoBancarioEntity> entities = new List<DatoBancarioEntity>();
            entities = ProviderSoftv.DatoBancario.GetDatoBancario(lid);
            return entities ?? new List<DatoBancarioEntity>();
        }

        /// <summary>
        ///Get DatoBancario By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static DatoBancarioEntity GetOne(long? IdContrato)
        {
            DatoBancarioEntity result = ProviderSoftv.DatoBancario.GetDatoBancarioById(IdContrato);

            //if (result.IdBanco != null)
            //    result.Banco = ProviderSoftv.Banco.GetBancoById(result.IdBanco);

            //if (result.IdTipoTarjeta != null)
            //    result.TipoTarjeta = ProviderSoftv.TipoTarjeta.GetTipoTarjetaById(result.IdTipoTarjeta);

            return result;
        }

        /// <summary>
        ///Get DatoBancario By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static DatoBancarioEntity GetOneDeep(long? IdContrato)
        {
            DatoBancarioEntity result = ProviderSoftv.DatoBancario.GetDatoBancarioById(IdContrato);

            //if (result.IdBanco != null)
            //    result.Banco = ProviderSoftv.Banco.GetBancoById(result.IdBanco);

            //if (result.IdTipoTarjeta != null)
            //    result.TipoTarjeta = ProviderSoftv.TipoTarjeta.GetTipoTarjetaById(result.IdTipoTarjeta);

            //result.Cliente = ProviderSoftv.Cliente.GetClienteByIdContrato(result.IdContrato);

            return result;
        }

        public static List<DatoBancarioEntity> GetDatoBancarioByIdBanco(int? IdBanco)
        {
            List<DatoBancarioEntity> entities = new List<DatoBancarioEntity>();
            entities = ProviderSoftv.DatoBancario.GetDatoBancarioByIdBanco(IdBanco);
            return entities ?? new List<DatoBancarioEntity>();
        }

        public static List<DatoBancarioEntity> GetDatoBancarioByIdTipoTarjeta(int? IdTipoTarjeta)
        {
            List<DatoBancarioEntity> entities = new List<DatoBancarioEntity>();
            entities = ProviderSoftv.DatoBancario.GetDatoBancarioByIdTipoTarjeta(IdTipoTarjeta);
            return entities ?? new List<DatoBancarioEntity>();
        }



        /// <summary>
        ///Get DatoBancario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<DatoBancarioEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<DatoBancarioEntity> entities = new SoftvList<DatoBancarioEntity>();
            entities = ProviderSoftv.DatoBancario.GetPagedList(pageIndex, pageSize);

            List<BancoEntity> lBanco = ProviderSoftv.Banco.GetBanco(entities.Where(x => x.IdBanco.HasValue).Select(x => x.IdBanco.Value).Distinct().ToList());
            lBanco.ForEach(XBanco => entities.Where(x => x.IdBanco.HasValue).Where(x => x.IdBanco == XBanco.IdBanco).ToList().ForEach(y => y.Banco = XBanco));

            List<TipoTarjetaEntity> lTipoTarjeta = ProviderSoftv.TipoTarjeta.GetTipoTarjeta(entities.Where(x => x.IdTipoTarjeta.HasValue).Select(x => x.IdTipoTarjeta.Value).Distinct().ToList());
            lTipoTarjeta.ForEach(XTipoTarjeta => entities.Where(x => x.IdTipoTarjeta.HasValue).Where(x => x.IdTipoTarjeta == XTipoTarjeta.IdTipoTarjeta).ToList().ForEach(y => y.TipoTarjeta = XTipoTarjeta));

            return entities ?? new SoftvList<DatoBancarioEntity>();
        }

        /// <summary>
        ///Get DatoBancario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<DatoBancarioEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<DatoBancarioEntity> entities = new SoftvList<DatoBancarioEntity>();
            entities = ProviderSoftv.DatoBancario.GetPagedList(pageIndex, pageSize, xml);

            List<BancoEntity> lBanco = ProviderSoftv.Banco.GetBanco(entities.Where(x => x.IdBanco.HasValue).Select(x => x.IdBanco.Value).Distinct().ToList());
            lBanco.ForEach(XBanco => entities.Where(x => x.IdBanco.HasValue).Where(x => x.IdBanco == XBanco.IdBanco).ToList().ForEach(y => y.Banco = XBanco));

            List<TipoTarjetaEntity> lTipoTarjeta = ProviderSoftv.TipoTarjeta.GetTipoTarjeta(entities.Where(x => x.IdTipoTarjeta.HasValue).Select(x => x.IdTipoTarjeta.Value).Distinct().ToList());
            lTipoTarjeta.ForEach(XTipoTarjeta => entities.Where(x => x.IdTipoTarjeta.HasValue).Where(x => x.IdTipoTarjeta == XTipoTarjeta.IdTipoTarjeta).ToList().ForEach(y => y.TipoTarjeta = XTipoTarjeta));

            return entities ?? new SoftvList<DatoBancarioEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

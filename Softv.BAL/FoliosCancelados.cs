﻿
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Data;
    using System.ComponentModel;
    using System.Linq;
    using Softv.Providers;
    using Softv.Entities;
    using Globals;

    /// <summary>
    /// Class                   : Softv.BAL.Client.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : FoliosCanceladosBussines
    /// File                    : FoliosCanceladosBussines.cs
    /// Creation date           : 06/12/2017
    /// Creation time           : 12:03 p. m.
    ///</summary>
    namespace Softv.BAL
    {

    [DataObject]
    [Serializable]
    public class FoliosCancelados
    {

    #region Constructors
    public FoliosCancelados(){}
    #endregion

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static FoliosCanceladosEntity Get_clv_session_Reportes(FoliosCanceladosEntity ObjClvSession)
    {
        FoliosCanceladosEntity result = ProviderSoftv.FoliosCancelados.Get_clv_session_Reportes(ObjClvSession);
        return result;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<FoliosCanceladosEntity> GetConVentasVendedoresPro(FoliosCanceladosEntity ObjVendedor)
    {
        List<FoliosCanceladosEntity> result = ProviderSoftv.FoliosCancelados.GetConVentasVendedoresPro(ObjVendedor);
        return result;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<FoliosCanceladosEntity> GetConVentasVendedoresTmp(FoliosCanceladosEntity ObjVendedor)
    {
        List<FoliosCanceladosEntity> result = ProviderSoftv.FoliosCancelados.GetConVentasVendedoresTmp(ObjVendedor);
        return result;
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public static int? GetInsertarVendedorTmp(FoliosCanceladosEntity ObjVendedor)
    {
        int? result = ProviderSoftv.FoliosCancelados.GetInsertarVendedorTmp(ObjVendedor);
        return result;
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public static int? GetBorrarVendedorTmp(FoliosCanceladosEntity ObjVendedor)
    {
        int? result = ProviderSoftv.FoliosCancelados.GetBorrarVendedorTmp(ObjVendedor);
        return result;
    }

    }


    #region Customs Methods
    
    #endregion
    }
  
﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : InsertMotCanServBussines
/// File                    : InsertMotCanServBussines.cs
/// Creation date           : 13/06/2017
/// Creation time           : 01:12 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class InsertMotCanServ
    {

        #region Constructors
        public InsertMotCanServ() { }
        #endregion

        /// <summary>
        ///Adds InsertMotCanServ
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(InsertMotCanServEntity objInsertMotCanServ)
        {
            int result = ProviderSoftv.InsertMotCanServ.AddInsertMotCanServ(objInsertMotCanServ);
            return result;
        }

        /// <summary>
        ///Delete InsertMotCanServ
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.InsertMotCanServ.DeleteInsertMotCanServ();
            return resultado;
        }

        /// <summary>
        ///Update InsertMotCanServ
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(InsertMotCanServEntity objInsertMotCanServ)
        {
            int result = ProviderSoftv.InsertMotCanServ.EditInsertMotCanServ(objInsertMotCanServ);
            return result;
        }

        /// <summary>
        ///Get InsertMotCanServ
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<InsertMotCanServEntity> GetAll()
        {
            List<InsertMotCanServEntity> entities = new List<InsertMotCanServEntity>();
            entities = ProviderSoftv.InsertMotCanServ.GetInsertMotCanServ();

            return entities ?? new List<InsertMotCanServEntity>();
        }

        /// <summary>
        ///Get InsertMotCanServ List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<InsertMotCanServEntity> GetAll(List<int> lid)
        {
            List<InsertMotCanServEntity> entities = new List<InsertMotCanServEntity>();
            entities = ProviderSoftv.InsertMotCanServ.GetInsertMotCanServ(lid);
            return entities ?? new List<InsertMotCanServEntity>();
        }

        /// <summary>
        ///Get InsertMotCanServ By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static InsertMotCanServEntity GetOne()
        {
            InsertMotCanServEntity result = ProviderSoftv.InsertMotCanServ.GetInsertMotCanServById();

            return result;
        }

        /// <summary>
        ///Get InsertMotCanServ By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static InsertMotCanServEntity GetOneDeep()
        {
            InsertMotCanServEntity result = ProviderSoftv.InsertMotCanServ.GetInsertMotCanServById();

            return result;
        }



        /// <summary>
        ///Get InsertMotCanServ
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<InsertMotCanServEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<InsertMotCanServEntity> entities = new SoftvList<InsertMotCanServEntity>();
            entities = ProviderSoftv.InsertMotCanServ.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<InsertMotCanServEntity>();
        }

        /// <summary>
        ///Get InsertMotCanServ
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<InsertMotCanServEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<InsertMotCanServEntity> entities = new SoftvList<InsertMotCanServEntity>();
            entities = ProviderSoftv.InsertMotCanServ.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<InsertMotCanServEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

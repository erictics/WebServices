﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : LocalidadCAMDOBussines
/// File                    : LocalidadCAMDOBussines.cs
/// Creation date           : 25/10/2016
/// Creation time           : 06:28 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class LocalidadCAMDO
    {

        #region Constructors
        public LocalidadCAMDO() { }
        #endregion


        /// <summary>
        ///Get LocalidadCAMDO
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<LocalidadCAMDOEntity> GetAll(long? Contrato, int? Ciudad)
        {
            List<LocalidadCAMDOEntity> entities = new List<LocalidadCAMDOEntity>();
            entities = ProviderSoftv.LocalidadCAMDO.GetLocalidadCAMDO(Contrato, Ciudad);

            return entities ?? new List<LocalidadCAMDOEntity>();
        }



    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MuestraAparatosCobroAdeudoBussines
/// File                    : MuestraAparatosCobroAdeudoBussines.cs
/// Creation date           : 22/10/2016
/// Creation time           : 01:07 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class MuestraAparatosCobroAdeudo
    {

        #region Constructors
        public MuestraAparatosCobroAdeudo() { }
        #endregion

        /// <summary>
        ///Adds MuestraAparatosCobroAdeudo
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(MuestraAparatosCobroAdeudoEntity objMuestraAparatosCobroAdeudo)
        {
            int result = ProviderSoftv.MuestraAparatosCobroAdeudo.AddMuestraAparatosCobroAdeudo(objMuestraAparatosCobroAdeudo);
            return result;
        }

        /// <summary>
        ///Delete MuestraAparatosCobroAdeudo
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.MuestraAparatosCobroAdeudo.DeleteMuestraAparatosCobroAdeudo();
            return resultado;
        }

        /// <summary>
        ///Update MuestraAparatosCobroAdeudo
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(MuestraAparatosCobroAdeudoEntity objMuestraAparatosCobroAdeudo)
        {
            int result = ProviderSoftv.MuestraAparatosCobroAdeudo.EditMuestraAparatosCobroAdeudo(objMuestraAparatosCobroAdeudo);
            return result;
        }

        /// <summary>
        ///Get MuestraAparatosCobroAdeudo
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraAparatosCobroAdeudoEntity> GetAll(long? IdSession, long? IdDetalle, long? Contrato)
        {
            List<MuestraAparatosCobroAdeudoEntity> entities = new List<MuestraAparatosCobroAdeudoEntity>();
            entities = ProviderSoftv.MuestraAparatosCobroAdeudo.GetMuestraAparatosCobroAdeudo(IdSession, IdDetalle, Contrato);

            return entities ?? new List<MuestraAparatosCobroAdeudoEntity>();
        }

        /// <summary>
        ///Get MuestraAparatosCobroAdeudo List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraAparatosCobroAdeudoEntity> GetAll(List<int> lid)
        {
            List<MuestraAparatosCobroAdeudoEntity> entities = new List<MuestraAparatosCobroAdeudoEntity>();
            entities = ProviderSoftv.MuestraAparatosCobroAdeudo.GetMuestraAparatosCobroAdeudo(lid);
            return entities ?? new List<MuestraAparatosCobroAdeudoEntity>();
        }

        /// <summary>
        ///Get MuestraAparatosCobroAdeudo By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraAparatosCobroAdeudoEntity GetOne()
        {
            MuestraAparatosCobroAdeudoEntity result = ProviderSoftv.MuestraAparatosCobroAdeudo.GetMuestraAparatosCobroAdeudoById();

            return result;
        }

        /// <summary>
        ///Get MuestraAparatosCobroAdeudo By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraAparatosCobroAdeudoEntity GetOneDeep()
        {
            MuestraAparatosCobroAdeudoEntity result = ProviderSoftv.MuestraAparatosCobroAdeudo.GetMuestraAparatosCobroAdeudoById();

            return result;
        }



        /// <summary>
        ///Get MuestraAparatosCobroAdeudo
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraAparatosCobroAdeudoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraAparatosCobroAdeudoEntity> entities = new SoftvList<MuestraAparatosCobroAdeudoEntity>();
            entities = ProviderSoftv.MuestraAparatosCobroAdeudo.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<MuestraAparatosCobroAdeudoEntity>();
        }

        /// <summary>
        ///Get MuestraAparatosCobroAdeudo
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraAparatosCobroAdeudoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraAparatosCobroAdeudoEntity> entities = new SoftvList<MuestraAparatosCobroAdeudoEntity>();
            entities = ProviderSoftv.MuestraAparatosCobroAdeudo.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<MuestraAparatosCobroAdeudoEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

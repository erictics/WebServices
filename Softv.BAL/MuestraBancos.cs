﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MuestraBancosBussines
/// File                    : MuestraBancosBussines.cs
/// Creation date           : 27/10/2016
/// Creation time           : 01:39 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class MuestraBancos
    {

        #region Constructors
        public MuestraBancos() { }
        #endregion



        /// <summary>
        ///Get MuestraBancos
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraBancosEntity> GetAll()
        {
            List<MuestraBancosEntity> entities = new List<MuestraBancosEntity>();
            entities = ProviderSoftv.MuestraBancos.GetMuestraBancos();

            return entities ?? new List<MuestraBancosEntity>();
        }

   


    }




    #region Customs Methods

    #endregion
}

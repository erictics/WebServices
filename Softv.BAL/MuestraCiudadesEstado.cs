﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MuestraCiudadesEstadoBussines
/// File                    : MuestraCiudadesEstadoBussines.cs
/// Creation date           : 05/09/2017
/// Creation time           : 04:51 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class MuestraCiudadesEstado
    {

        #region Constructors
        public MuestraCiudadesEstado() { }
        #endregion

        /// <summary>
        ///Adds MuestraCiudadesEstado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(MuestraCiudadesEstadoEntity objMuestraCiudadesEstado)
        {
            int result = ProviderSoftv.MuestraCiudadesEstado.AddMuestraCiudadesEstado(objMuestraCiudadesEstado);
            return result;
        }

        /// <summary>
        ///Delete MuestraCiudadesEstado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.MuestraCiudadesEstado.DeleteMuestraCiudadesEstado();
            return resultado;
        }

        /// <summary>
        ///Update MuestraCiudadesEstado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(MuestraCiudadesEstadoEntity objMuestraCiudadesEstado)
        {
            int result = ProviderSoftv.MuestraCiudadesEstado.EditMuestraCiudadesEstado(objMuestraCiudadesEstado);
            return result;
        }

        /// <summary>
        ///Get MuestraCiudadesEstado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraCiudadesEstadoEntity> GetAll(int? clv_estado, int? idcompania)
        {
            List<MuestraCiudadesEstadoEntity> entities = new List<MuestraCiudadesEstadoEntity>();
            entities = ProviderSoftv.MuestraCiudadesEstado.GetMuestraCiudadesEstado(clv_estado, idcompania);

            return entities ?? new List<MuestraCiudadesEstadoEntity>();
        }

        /// <summary>
        ///Get MuestraCiudadesEstado List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraCiudadesEstadoEntity> GetAll(List<int> lid)
        {
            List<MuestraCiudadesEstadoEntity> entities = new List<MuestraCiudadesEstadoEntity>();
            entities = ProviderSoftv.MuestraCiudadesEstado.GetMuestraCiudadesEstado(lid);
            return entities ?? new List<MuestraCiudadesEstadoEntity>();
        }

        /// <summary>
        ///Get MuestraCiudadesEstado By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraCiudadesEstadoEntity GetOne()
        {
            MuestraCiudadesEstadoEntity result = ProviderSoftv.MuestraCiudadesEstado.GetMuestraCiudadesEstadoById();

            return result;
        }

        /// <summary>
        ///Get MuestraCiudadesEstado By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraCiudadesEstadoEntity GetOneDeep()
        {
            MuestraCiudadesEstadoEntity result = ProviderSoftv.MuestraCiudadesEstado.GetMuestraCiudadesEstadoById();

            return result;
        }



        /// <summary>
        ///Get MuestraCiudadesEstado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraCiudadesEstadoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraCiudadesEstadoEntity> entities = new SoftvList<MuestraCiudadesEstadoEntity>();
            entities = ProviderSoftv.MuestraCiudadesEstado.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<MuestraCiudadesEstadoEntity>();
        }

        /// <summary>
        ///Get MuestraCiudadesEstado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraCiudadesEstadoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraCiudadesEstadoEntity> entities = new SoftvList<MuestraCiudadesEstadoEntity>();
            entities = ProviderSoftv.MuestraCiudadesEstado.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<MuestraCiudadesEstadoEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

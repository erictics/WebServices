﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MuestraColonias_CalleBussines
/// File                    : MuestraColonias_CalleBussines.cs
/// Creation date           : 28/09/2017
/// Creation time           : 11:35 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class MuestraColonias_Calle
    {

        #region Constructors
        public MuestraColonias_Calle() { }
        #endregion

        /// <summary>
        ///Adds MuestraColonias_Calle
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(MuestraColonias_CalleEntity objMuestraColonias_Calle)
        {
            int result = ProviderSoftv.MuestraColonias_Calle.AddMuestraColonias_Calle(objMuestraColonias_Calle);
            return result;
        }

        /// <summary>
        ///Delete MuestraColonias_Calle
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.MuestraColonias_Calle.DeleteMuestraColonias_Calle();
            return resultado;
        }

        /// <summary>
        ///Update MuestraColonias_Calle
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(MuestraColonias_CalleEntity objMuestraColonias_Calle)
        {
            int result = ProviderSoftv.MuestraColonias_Calle.EditMuestraColonias_Calle(objMuestraColonias_Calle);
            return result;
        }

        /// <summary>
        ///Get MuestraColonias_Calle
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraColonias_CalleEntity> GetAll(int? clv_localidad)
        {
            List<MuestraColonias_CalleEntity> entities = new List<MuestraColonias_CalleEntity>();
            entities = ProviderSoftv.MuestraColonias_Calle.GetMuestraColonias_Calle(clv_localidad);

            return entities ?? new List<MuestraColonias_CalleEntity>();
        }

        /// <summary>
        ///Get MuestraColonias_Calle List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraColonias_CalleEntity> GetAll(List<int> lid)
        {
            List<MuestraColonias_CalleEntity> entities = new List<MuestraColonias_CalleEntity>();
            entities = ProviderSoftv.MuestraColonias_Calle.GetMuestraColonias_Calle(lid);
            return entities ?? new List<MuestraColonias_CalleEntity>();
        }

        /// <summary>
        ///Get MuestraColonias_Calle By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraColonias_CalleEntity GetOne()
        {
            MuestraColonias_CalleEntity result = ProviderSoftv.MuestraColonias_Calle.GetMuestraColonias_CalleById();

            return result;
        }

        /// <summary>
        ///Get MuestraColonias_Calle By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraColonias_CalleEntity GetOneDeep()
        {
            MuestraColonias_CalleEntity result = ProviderSoftv.MuestraColonias_Calle.GetMuestraColonias_CalleById();

            return result;
        }



        /// <summary>
        ///Get MuestraColonias_Calle
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraColonias_CalleEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraColonias_CalleEntity> entities = new SoftvList<MuestraColonias_CalleEntity>();
            entities = ProviderSoftv.MuestraColonias_Calle.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<MuestraColonias_CalleEntity>();
        }

        /// <summary>
        ///Get MuestraColonias_Calle
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraColonias_CalleEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraColonias_CalleEntity> entities = new SoftvList<MuestraColonias_CalleEntity>();
            entities = ProviderSoftv.MuestraColonias_Calle.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<MuestraColonias_CalleEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

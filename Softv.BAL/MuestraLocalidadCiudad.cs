﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MuestraLocalidadCiudadBussines
/// File                    : MuestraLocalidadCiudadBussines.cs
/// Creation date           : 05/09/2017
/// Creation time           : 05:01 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class MuestraLocalidadCiudad
    {

        #region Constructors
        public MuestraLocalidadCiudad() { }
        #endregion

        /// <summary>
        ///Adds MuestraLocalidadCiudad
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(MuestraLocalidadCiudadEntity objMuestraLocalidadCiudad)
        {
            int result = ProviderSoftv.MuestraLocalidadCiudad.AddMuestraLocalidadCiudad(objMuestraLocalidadCiudad);
            return result;
        }

        /// <summary>
        ///Delete MuestraLocalidadCiudad
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.MuestraLocalidadCiudad.DeleteMuestraLocalidadCiudad();
            return resultado;
        }

        /// <summary>
        ///Update MuestraLocalidadCiudad
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(MuestraLocalidadCiudadEntity objMuestraLocalidadCiudad)
        {
            int result = ProviderSoftv.MuestraLocalidadCiudad.EditMuestraLocalidadCiudad(objMuestraLocalidadCiudad);
            return result;
        }

        /// <summary>
        ///Get MuestraLocalidadCiudad
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraLocalidadCiudadEntity> GetAll(int? clv_ciudad)
        {
            List<MuestraLocalidadCiudadEntity> entities = new List<MuestraLocalidadCiudadEntity>();
            entities = ProviderSoftv.MuestraLocalidadCiudad.GetMuestraLocalidadCiudad(clv_ciudad);

            return entities ?? new List<MuestraLocalidadCiudadEntity>();
        }

        /// <summary>
        ///Get MuestraLocalidadCiudad List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraLocalidadCiudadEntity> GetAll(List<int> lid)
        {
            List<MuestraLocalidadCiudadEntity> entities = new List<MuestraLocalidadCiudadEntity>();
            entities = ProviderSoftv.MuestraLocalidadCiudad.GetMuestraLocalidadCiudad(lid);
            return entities ?? new List<MuestraLocalidadCiudadEntity>();
        }

        /// <summary>
        ///Get MuestraLocalidadCiudad By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraLocalidadCiudadEntity GetOne()
        {
            MuestraLocalidadCiudadEntity result = ProviderSoftv.MuestraLocalidadCiudad.GetMuestraLocalidadCiudadById();

            return result;
        }

        /// <summary>
        ///Get MuestraLocalidadCiudad By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraLocalidadCiudadEntity GetOneDeep()
        {
            MuestraLocalidadCiudadEntity result = ProviderSoftv.MuestraLocalidadCiudad.GetMuestraLocalidadCiudadById();

            return result;
        }



        /// <summary>
        ///Get MuestraLocalidadCiudad
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraLocalidadCiudadEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraLocalidadCiudadEntity> entities = new SoftvList<MuestraLocalidadCiudadEntity>();
            entities = ProviderSoftv.MuestraLocalidadCiudad.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<MuestraLocalidadCiudadEntity>();
        }

        /// <summary>
        ///Get MuestraLocalidadCiudad
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraLocalidadCiudadEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraLocalidadCiudadEntity> entities = new SoftvList<MuestraLocalidadCiudadEntity>();
            entities = ProviderSoftv.MuestraLocalidadCiudad.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<MuestraLocalidadCiudadEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

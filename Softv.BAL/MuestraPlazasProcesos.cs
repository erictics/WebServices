﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MuestraPlazasProcesosBussines
/// File                    : MuestraPlazasProcesosBussines.cs
/// Creation date           : 13/12/2016
/// Creation time           : 11:24 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class MuestraPlazasProcesos
    {

        #region Constructors
        public MuestraPlazasProcesos() { }
        #endregion

        /// <summary>
        ///Adds MuestraPlazasProcesos
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(MuestraPlazasProcesosEntity objMuestraPlazasProcesos)
        {
            int result = ProviderSoftv.MuestraPlazasProcesos.AddMuestraPlazasProcesos(objMuestraPlazasProcesos);
            return result;
        }

        /// <summary>
        ///Delete MuestraPlazasProcesos
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.MuestraPlazasProcesos.DeleteMuestraPlazasProcesos();
            return resultado;
        }

        /// <summary>
        ///Update MuestraPlazasProcesos
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(MuestraPlazasProcesosEntity objMuestraPlazasProcesos)
        {
            int result = ProviderSoftv.MuestraPlazasProcesos.EditMuestraPlazasProcesos(objMuestraPlazasProcesos);
            return result;
        }

        /// <summary>
        ///Get MuestraPlazasProcesos
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraPlazasProcesosEntity> GetAll(long? IdUsuario)
        {
            List<MuestraPlazasProcesosEntity> entities = new List<MuestraPlazasProcesosEntity>();
            entities = ProviderSoftv.MuestraPlazasProcesos.GetMuestraPlazasProcesos(IdUsuario);

            return entities ?? new List<MuestraPlazasProcesosEntity>();
        }

        /// <summary>
        ///Get MuestraPlazasProcesos List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraPlazasProcesosEntity> GetAll(List<int> lid)
        {
            List<MuestraPlazasProcesosEntity> entities = new List<MuestraPlazasProcesosEntity>();
            entities = ProviderSoftv.MuestraPlazasProcesos.GetMuestraPlazasProcesos(lid);
            return entities ?? new List<MuestraPlazasProcesosEntity>();
        }

        /// <summary>
        ///Get MuestraPlazasProcesos By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraPlazasProcesosEntity GetOne()
        {
            MuestraPlazasProcesosEntity result = ProviderSoftv.MuestraPlazasProcesos.GetMuestraPlazasProcesosById();

            return result;
        }

        /// <summary>
        ///Get MuestraPlazasProcesos By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraPlazasProcesosEntity GetOneDeep()
        {
            MuestraPlazasProcesosEntity result = ProviderSoftv.MuestraPlazasProcesos.GetMuestraPlazasProcesosById();

            return result;
        }



        /// <summary>
        ///Get MuestraPlazasProcesos
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraPlazasProcesosEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraPlazasProcesosEntity> entities = new SoftvList<MuestraPlazasProcesosEntity>();
            entities = ProviderSoftv.MuestraPlazasProcesos.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<MuestraPlazasProcesosEntity>();
        }

        /// <summary>
        ///Get MuestraPlazasProcesos
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraPlazasProcesosEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraPlazasProcesosEntity> entities = new SoftvList<MuestraPlazasProcesosEntity>();
            entities = ProviderSoftv.MuestraPlazasProcesos.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<MuestraPlazasProcesosEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MuestraTipSerPrincipal2Bussines
/// File                    : MuestraTipSerPrincipal2Bussines.cs
/// Creation date           : 14/01/2017
/// Creation time           : 11:05 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class MuestraTipSerPrincipal2
    {

        #region Constructors
        public MuestraTipSerPrincipal2() { }
        #endregion

        /// <summary>
        ///Adds MuestraTipSerPrincipal2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(MuestraTipSerPrincipal2Entity objMuestraTipSerPrincipal2)
        {
            int result = ProviderSoftv.MuestraTipSerPrincipal2.AddMuestraTipSerPrincipal2(objMuestraTipSerPrincipal2);
            return result;
        }

        /// <summary>
        ///Delete MuestraTipSerPrincipal2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.MuestraTipSerPrincipal2.DeleteMuestraTipSerPrincipal2();
            return resultado;
        }

        /// <summary>
        ///Update MuestraTipSerPrincipal2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(MuestraTipSerPrincipal2Entity objMuestraTipSerPrincipal2)
        {
            int result = ProviderSoftv.MuestraTipSerPrincipal2.EditMuestraTipSerPrincipal2(objMuestraTipSerPrincipal2);
            return result;
        }

        /// <summary>
        ///Get MuestraTipSerPrincipal2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraTipSerPrincipal2Entity> GetAll()
        {
            List<MuestraTipSerPrincipal2Entity> entities = new List<MuestraTipSerPrincipal2Entity>();
            entities = ProviderSoftv.MuestraTipSerPrincipal2.GetMuestraTipSerPrincipal2();

            return entities ?? new List<MuestraTipSerPrincipal2Entity>();
        }

        /// <summary>
        ///Get MuestraTipSerPrincipal2 List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MuestraTipSerPrincipal2Entity> GetAll(List<int> lid)
        {
            List<MuestraTipSerPrincipal2Entity> entities = new List<MuestraTipSerPrincipal2Entity>();
            entities = ProviderSoftv.MuestraTipSerPrincipal2.GetMuestraTipSerPrincipal2(lid);
            return entities ?? new List<MuestraTipSerPrincipal2Entity>();
        }

        /// <summary>
        ///Get MuestraTipSerPrincipal2 By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraTipSerPrincipal2Entity GetOne()
        {
            MuestraTipSerPrincipal2Entity result = ProviderSoftv.MuestraTipSerPrincipal2.GetMuestraTipSerPrincipal2ById();

            return result;
        }

        /// <summary>
        ///Get MuestraTipSerPrincipal2 By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MuestraTipSerPrincipal2Entity GetOneDeep()
        {
            MuestraTipSerPrincipal2Entity result = ProviderSoftv.MuestraTipSerPrincipal2.GetMuestraTipSerPrincipal2ById();

            return result;
        }



        /// <summary>
        ///Get MuestraTipSerPrincipal2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraTipSerPrincipal2Entity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraTipSerPrincipal2Entity> entities = new SoftvList<MuestraTipSerPrincipal2Entity>();
            entities = ProviderSoftv.MuestraTipSerPrincipal2.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<MuestraTipSerPrincipal2Entity>();
        }

        /// <summary>
        ///Get MuestraTipSerPrincipal2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MuestraTipSerPrincipal2Entity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraTipSerPrincipal2Entity> entities = new SoftvList<MuestraTipSerPrincipal2Entity>();
            entities = ProviderSoftv.MuestraTipSerPrincipal2.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<MuestraTipSerPrincipal2Entity>();
        }


    }




    #region Customs Methods

    #endregion
}

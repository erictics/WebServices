﻿
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Data;
    using System.ComponentModel;
    using System.Linq;
    using Softv.Providers;
    using Softv.Entities;
    using Globals;

    /// <summary>
    /// Class                   : Softv.BAL.Client.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraTipoFacturaHistorialBussines
    /// File                    : MuestraTipoFacturaHistorialBussines.cs
    /// Creation date           : 21/10/2016
    /// Creation time           : 12:35 p. m.
    ///</summary>
    namespace Softv.BAL
    {

    [DataObject]
    [Serializable]
    public class MuestraTipoFacturaHistorial
    {

    #region Constructors
    public MuestraTipoFacturaHistorial(){}
    #endregion

    /// <summary>
    ///Adds MuestraTipoFacturaHistorial
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public static int Add(MuestraTipoFacturaHistorialEntity objMuestraTipoFacturaHistorial)
  {
  int result = ProviderSoftv.MuestraTipoFacturaHistorial.AddMuestraTipoFacturaHistorial(objMuestraTipoFacturaHistorial);
    return result;
    }

    /// <summary>
    ///Delete MuestraTipoFacturaHistorial
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public static int Delete()
    {
    int resultado = ProviderSoftv.MuestraTipoFacturaHistorial.DeleteMuestraTipoFacturaHistorial();
    return resultado;
    }

    /// <summary>
    ///Update MuestraTipoFacturaHistorial
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public static int Edit(MuestraTipoFacturaHistorialEntity objMuestraTipoFacturaHistorial)
    {
    int result = ProviderSoftv.MuestraTipoFacturaHistorial.EditMuestraTipoFacturaHistorial(objMuestraTipoFacturaHistorial);
    return result;
    }

    /// <summary>
    ///Get MuestraTipoFacturaHistorial
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<MuestraTipoFacturaHistorialEntity> GetAll()
    {
    List<MuestraTipoFacturaHistorialEntity> entities = new List<MuestraTipoFacturaHistorialEntity> ();
    entities = ProviderSoftv.MuestraTipoFacturaHistorial.GetMuestraTipoFacturaHistorial();
    
    return entities ?? new List<MuestraTipoFacturaHistorialEntity>();
    }

    /// <summary>
    ///Get MuestraTipoFacturaHistorial List<lid>
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<MuestraTipoFacturaHistorialEntity> GetAll(List<int> lid)
    {
    List<MuestraTipoFacturaHistorialEntity> entities = new List<MuestraTipoFacturaHistorialEntity> ();
    entities = ProviderSoftv.MuestraTipoFacturaHistorial.GetMuestraTipoFacturaHistorial(lid);    
    return entities ?? new List<MuestraTipoFacturaHistorialEntity>();
    }

    /// <summary>
    ///Get MuestraTipoFacturaHistorial By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static MuestraTipoFacturaHistorialEntity GetOne()
    {
    MuestraTipoFacturaHistorialEntity result = ProviderSoftv.MuestraTipoFacturaHistorial.GetMuestraTipoFacturaHistorialById();
    
    return result;
    }

    /// <summary>
    ///Get MuestraTipoFacturaHistorial By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static MuestraTipoFacturaHistorialEntity GetOneDeep()
    {
    MuestraTipoFacturaHistorialEntity result = ProviderSoftv.MuestraTipoFacturaHistorial.GetMuestraTipoFacturaHistorialById();
    
    return result;
    }
    

    
      /// <summary>
      ///Get MuestraTipoFacturaHistorial
      ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<MuestraTipoFacturaHistorialEntity> GetPagedList(int pageIndex, int pageSize)
    {
    SoftvList<MuestraTipoFacturaHistorialEntity> entities = new SoftvList<MuestraTipoFacturaHistorialEntity>();
    entities = ProviderSoftv.MuestraTipoFacturaHistorial.GetPagedList(pageIndex, pageSize);
    
    return entities ?? new SoftvList<MuestraTipoFacturaHistorialEntity>();
    }

    /// <summary>
    ///Get MuestraTipoFacturaHistorial
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<MuestraTipoFacturaHistorialEntity> GetPagedList(int pageIndex, int pageSize,String xml)
    {
    SoftvList<MuestraTipoFacturaHistorialEntity> entities = new SoftvList<MuestraTipoFacturaHistorialEntity>();
    entities = ProviderSoftv.MuestraTipoFacturaHistorial.GetPagedList(pageIndex, pageSize,xml);
    
    return entities ?? new SoftvList<MuestraTipoFacturaHistorialEntity>();
    }


    }




    #region Customs Methods
    
    #endregion
    }
  
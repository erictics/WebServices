﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : MunicipioBussines
/// File                    : MunicipioBussines.cs
/// Creation date           : 03/02/2016
/// Creation time           : 11:29 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class Municipio
    {

        #region Constructors
        public Municipio() { }
        #endregion

        /// <summary>
        ///Adds Municipio
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(MunicipioEntity objMunicipio)
        {

            var a = objMunicipio.Nombre.LongCount();

            if (objMunicipio.Nombre == string.Empty || objMunicipio.Nombre == null)
                throw new Exception("Codigo 00001");

            if (a > 150)
                throw new Exception("Codigo 00002");


            int result = ProviderSoftv.Municipio.AddMunicipio(objMunicipio);
            return result;
        }

        /// <summary>
        ///Delete Municipio
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(int? IdMunicipio)
        {
            int resultado = ProviderSoftv.Municipio.DeleteMunicipio(IdMunicipio);
            return resultado;
        }

        /// <summary>
        ///Update Municipio
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(MunicipioEntity objMunicipio)
        {
            int result = ProviderSoftv.Municipio.EditMunicipio(objMunicipio);
            return result;
        }

        /// <summary>
        ///Get Municipio
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MunicipioEntity> GetAll()
        {
            List<MunicipioEntity> entities = new List<MunicipioEntity>();
            entities = ProviderSoftv.Municipio.GetMunicipio();

            return entities ?? new List<MunicipioEntity>();
        }




        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MunicipioEntity> GetMunicipioList2()
        {
            List<MunicipioEntity> entities = new List<MunicipioEntity>();
            entities = ProviderSoftv.Municipio.GetMunicipioList2();

            return entities ?? new List<MunicipioEntity>();
        }





        /// <summary>
        ///Get Municipio List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MunicipioEntity> GetAll(List<int> lid)
        {
            List<MunicipioEntity> entities = new List<MunicipioEntity>();
            entities = ProviderSoftv.Municipio.GetMunicipio(lid);
            return entities ?? new List<MunicipioEntity>();
        }

        /// <summary>
        ///Get Municipio By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MunicipioEntity GetOne(int? IdMunicipio)
        {
            MunicipioEntity result = ProviderSoftv.Municipio.GetMunicipioById(IdMunicipio);

            return result;
        }

        /// <summary>
        ///Get Municipio By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static MunicipioEntity GetOneDeep(int? IdMunicipio)
        {
            MunicipioEntity result = ProviderSoftv.Municipio.GetMunicipioById(IdMunicipio);

            //result.Cliente = ProviderSoftv.Cliente.GetClienteByIdMunicipio(result.IdMunicipio);

            result.RelMunicipioEst = ProviderSoftv.RelMunicipioEst.GetRelMunicipioEstByIdMunicipio(result.IdMunicipio);

            List<EstadoEntity> lEstado = ProviderSoftv.Estado.GetEstado(result.RelMunicipioEst.Where(x => x.IdEstado.HasValue).Select(x => x.IdEstado.Value).ToList());
            lEstado.ForEach(XEstado => result.RelMunicipioEst.Where(x => x.IdEstado.HasValue).Where(x => x.IdEstado == XEstado.IdEstado).ToList().ForEach(y => y.Estado = XEstado));


            return result;
        }



        /// <summary>
        ///Get Municipio
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MunicipioEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MunicipioEntity> entities = new SoftvList<MunicipioEntity>();
            entities = ProviderSoftv.Municipio.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<MunicipioEntity>();
        }

        /// <summary>
        ///Get Municipio
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<MunicipioEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MunicipioEntity> entities = new SoftvList<MunicipioEntity>();
            entities = ProviderSoftv.Municipio.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<MunicipioEntity>();
        }





























        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int AddRelEstMunL(String xml)
        {
            int result = ProviderSoftv.Municipio.AddRelEstMunL(xml);
            return result;
        }


        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int UpdateRelEstMunL(String xml)
        {
            int result = ProviderSoftv.Municipio.UpdateRelEstMunL(xml);
            return result;
        }




    }




    #region Customs Methods

    #endregion
}

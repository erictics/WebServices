﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;
namespace Softv.BAL
{
    [DataObject]
    [Serializable]
    public class NotaCredito
    {
      
        public static List<NotaCreditoEntity> GetBUSCANOTASDECREDITO(int? op, int? Clv_NotadeCredito, string Fecha, string contrato, int? sucursal, string Nombre, int? Idcompania)
        {
            List<NotaCreditoEntity> result = ProviderSoftv.NotaCreditoProvider.GetBUSCANOTASDECREDITO(op, Clv_NotadeCredito, Fecha, contrato, sucursal, Nombre, Idcompania);
            return result;
        }

       
        public static List<StatusNotaEntity> GetStatusNotadeCredito()
        {
            List<StatusNotaEntity> result = ProviderSoftv.NotaCreditoProvider.GetStatusNotadeCredito();
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static List<DetalleNotaEntity> GetDetalle_NotasdeCredito(long Factura, long? Clv_NotadeCredito)
        {
            List<DetalleNotaEntity> result = ProviderSoftv.NotaCreditoProvider.GetDetalle_NotasdeCredito(Factura, Clv_NotadeCredito);
            return result;
        }

       
        public static List<FacturaClienteEntity> GetDAME_FACTURASDECLIENTE(long? contrato, long clv_nota)
        {
            List<FacturaClienteEntity> result = ProviderSoftv.NotaCreditoProvider.GetDAME_FACTURASDECLIENTE(contrato, clv_nota);
            return result;
        }

       
        public static NotaCreditoEntity GetConsulta_NotaCredito(long? Clv_NotadeCredito)
        {
            NotaCreditoEntity result = ProviderSoftv.NotaCreditoProvider.GetConsulta_NotaCredito(Clv_NotadeCredito);
            return result;
        }




        public static DatosTicket GetObtieneDatosTicket(long? Clv_NotadeCredito)
        {
            DatosTicket result = ProviderSoftv.NotaCreditoProvider.GetObtieneDatosTicket(Clv_NotadeCredito);
            return result;
        }


      
        public static int? GetNueva_NotadeCredito(NotaCreditoEntity nota)
        {
            int ? result = ProviderSoftv.NotaCreditoProvider.GetNueva_NotadeCredito(nota);
            return result;
        }

      
        public static int? GetGuarda_DetalleNota(long? Clv_Factura, long? Clv_Nota)
        {
            int? result = ProviderSoftv.NotaCreditoProvider.GetGuarda_DetalleNota(Clv_Factura, Clv_Nota);
            return result;
        }


       
        public static string GetReportesNotasDeCredito(long? Clv_Nota)
        {
            string result = ProviderSoftv.NotaCreditoProvider.GetReportesNotasDeCredito( Clv_Nota);
            return result;
        }


        public static int? GetBorrar_Session_Notas(long Clv_factura, long? Clv_Nota)
        {
            int ? result = ProviderSoftv.NotaCreditoProvider.GetBorrar_Session_Notas(Clv_factura, Clv_Nota);
            return result;
        }


        public static int? GetModifica_DetalleNotas(long? Clv_detalle, long? Clv_factura, bool? secobra, decimal? importe)
        {
            int? result = ProviderSoftv.NotaCreditoProvider.GetModifica_DetalleNotas(Clv_detalle, Clv_factura, secobra, importe);
            return result;
        }

        public static long? GetCANCELACIONFACTURAS_Notas(long? Clv_Factura, int? op)
        {
            long? result = ProviderSoftv.NotaCreditoProvider.GetCANCELACIONFACTURAS_Notas(Clv_Factura, op);
            return result;

        }


        public static decimal? GetDameMonto_NotadeCredito(long? Clv_Nota, long? contrato)
        {
            decimal? result = ProviderSoftv.NotaCreditoProvider.GetDameMonto_NotadeCredito(Clv_Nota, contrato);
            return result;

        }

    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ObservacionClienteBussines
/// File                    : ObservacionClienteBussines.cs
/// Creation date           : 27/02/2016
/// Creation time           : 01:08 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ObservacionCliente
    {

        #region Constructors
        public ObservacionCliente() { }
        #endregion

        /// <summary>
        ///Adds ObservacionCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ObservacionClienteEntity objObservacionCliente)
        {
            int result = ProviderSoftv.ObservacionCliente.AddObservacionCliente(objObservacionCliente);
            return result;
        }

        /// <summary>
        ///Delete ObservacionCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(long? IdContrato)
        {
            int resultado = ProviderSoftv.ObservacionCliente.DeleteObservacionCliente(IdContrato);
            return resultado;
        }

        /// <summary>
        ///Update ObservacionCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ObservacionClienteEntity objObservacionCliente)
        {
            int result = ProviderSoftv.ObservacionCliente.EditObservacionCliente(objObservacionCliente);
            return result;
        }

        /// <summary>
        ///Get ObservacionCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ObservacionClienteEntity> GetAll()
        {
            List<ObservacionClienteEntity> entities = new List<ObservacionClienteEntity>();
            entities = ProviderSoftv.ObservacionCliente.GetObservacionCliente();

            return entities ?? new List<ObservacionClienteEntity>();
        }

        /// <summary>
        ///Get ObservacionCliente List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ObservacionClienteEntity> GetAll(List<int> lid)
        {
            List<ObservacionClienteEntity> entities = new List<ObservacionClienteEntity>();
            entities = ProviderSoftv.ObservacionCliente.GetObservacionCliente(lid);
            return entities ?? new List<ObservacionClienteEntity>();
        }

        /// <summary>
        ///Get ObservacionCliente By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ObservacionClienteEntity GetOne(long? IdContrato)
        {
            ObservacionClienteEntity result = ProviderSoftv.ObservacionCliente.GetObservacionClienteById(IdContrato);

            return result;
        }

        /// <summary>
        ///Get ObservacionCliente By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ObservacionClienteEntity GetOneDeep(long? IdContrato)
        {
            ObservacionClienteEntity result = ProviderSoftv.ObservacionCliente.GetObservacionClienteById(IdContrato);

            //result.Cliente = ProviderSoftv.Cliente.GetClienteByIdContrato(result.IdContrato);

            return result;
        }



        /// <summary>
        ///Get ObservacionCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ObservacionClienteEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ObservacionClienteEntity> entities = new SoftvList<ObservacionClienteEntity>();
            entities = ProviderSoftv.ObservacionCliente.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ObservacionClienteEntity>();
        }

        /// <summary>
        ///Get ObservacionCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ObservacionClienteEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ObservacionClienteEntity> entities = new SoftvList<ObservacionClienteEntity>();
            entities = ProviderSoftv.ObservacionCliente.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ObservacionClienteEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

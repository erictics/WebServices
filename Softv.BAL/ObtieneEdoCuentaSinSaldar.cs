﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ObtieneEdoCuentaSinSaldarBussines
/// File                    : ObtieneEdoCuentaSinSaldarBussines.cs
/// Creation date           : 03/03/2017
/// Creation time           : 01:25 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ObtieneEdoCuentaSinSaldar
    {

        #region Constructors
        public ObtieneEdoCuentaSinSaldar() { }
        #endregion

        /// <summary>
        ///Adds ObtieneEdoCuentaSinSaldar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ObtieneEdoCuentaSinSaldarEntity objObtieneEdoCuentaSinSaldar)
        {
            int result = ProviderSoftv.ObtieneEdoCuentaSinSaldar.AddObtieneEdoCuentaSinSaldar(objObtieneEdoCuentaSinSaldar);
            return result;
        }

        /// <summary>
        ///Delete ObtieneEdoCuentaSinSaldar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ObtieneEdoCuentaSinSaldar.DeleteObtieneEdoCuentaSinSaldar();
            return resultado;
        }

        /// <summary>
        ///Update ObtieneEdoCuentaSinSaldar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ObtieneEdoCuentaSinSaldarEntity objObtieneEdoCuentaSinSaldar)
        {
            int result = ProviderSoftv.ObtieneEdoCuentaSinSaldar.EditObtieneEdoCuentaSinSaldar(objObtieneEdoCuentaSinSaldar);
            return result;
        }

        /// <summary>
        ///Get ObtieneEdoCuentaSinSaldar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ObtieneEdoCuentaSinSaldarEntity> GetAll(long? Contrato, long? ClvSession)
        {
            List<ObtieneEdoCuentaSinSaldarEntity> entities = new List<ObtieneEdoCuentaSinSaldarEntity>();
            entities = ProviderSoftv.ObtieneEdoCuentaSinSaldar.GetObtieneEdoCuentaSinSaldar(Contrato, ClvSession);

            return entities ?? new List<ObtieneEdoCuentaSinSaldarEntity>();
        }

        /// <summary>
        ///Get ObtieneEdoCuentaSinSaldar List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ObtieneEdoCuentaSinSaldarEntity> GetAll(List<int> lid)
        {
            List<ObtieneEdoCuentaSinSaldarEntity> entities = new List<ObtieneEdoCuentaSinSaldarEntity>();
            entities = ProviderSoftv.ObtieneEdoCuentaSinSaldar.GetObtieneEdoCuentaSinSaldar(lid);
            return entities ?? new List<ObtieneEdoCuentaSinSaldarEntity>();
        }

        /// <summary>
        ///Get ObtieneEdoCuentaSinSaldar By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ObtieneEdoCuentaSinSaldarEntity GetOne()
        {
            ObtieneEdoCuentaSinSaldarEntity result = ProviderSoftv.ObtieneEdoCuentaSinSaldar.GetObtieneEdoCuentaSinSaldarById();

            return result;
        }

        /// <summary>
        ///Get ObtieneEdoCuentaSinSaldar By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ObtieneEdoCuentaSinSaldarEntity GetOneDeep()
        {
            ObtieneEdoCuentaSinSaldarEntity result = ProviderSoftv.ObtieneEdoCuentaSinSaldar.GetObtieneEdoCuentaSinSaldarById();

            return result;
        }



        /// <summary>
        ///Get ObtieneEdoCuentaSinSaldar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ObtieneEdoCuentaSinSaldarEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ObtieneEdoCuentaSinSaldarEntity> entities = new SoftvList<ObtieneEdoCuentaSinSaldarEntity>();
            entities = ProviderSoftv.ObtieneEdoCuentaSinSaldar.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ObtieneEdoCuentaSinSaldarEntity>();
        }

        /// <summary>
        ///Get ObtieneEdoCuentaSinSaldar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ObtieneEdoCuentaSinSaldarEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ObtieneEdoCuentaSinSaldarEntity> entities = new SoftvList<ObtieneEdoCuentaSinSaldarEntity>();
            entities = ProviderSoftv.ObtieneEdoCuentaSinSaldar.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ObtieneEdoCuentaSinSaldarEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

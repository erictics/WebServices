﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ObtieneSucursalesEspeciales_ReimpresionBussines
/// File                    : ObtieneSucursalesEspeciales_ReimpresionBussines.cs
/// Creation date           : 03/02/2017
/// Creation time           : 06:11 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ObtieneSucursalesEspeciales_Reimpresion
    {

        #region Constructors
        public ObtieneSucursalesEspeciales_Reimpresion() { }
        #endregion

        /// <summary>
        ///Adds ObtieneSucursalesEspeciales_Reimpresion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ObtieneSucursalesEspeciales_ReimpresionEntity objObtieneSucursalesEspeciales_Reimpresion)
        {
            int result = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.AddObtieneSucursalesEspeciales_Reimpresion(objObtieneSucursalesEspeciales_Reimpresion);
            return result;
        }

        /// <summary>
        ///Delete ObtieneSucursalesEspeciales_Reimpresion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.DeleteObtieneSucursalesEspeciales_Reimpresion();
            return resultado;
        }

        /// <summary>
        ///Update ObtieneSucursalesEspeciales_Reimpresion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ObtieneSucursalesEspeciales_ReimpresionEntity objObtieneSucursalesEspeciales_Reimpresion)
        {
            int result = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.EditObtieneSucursalesEspeciales_Reimpresion(objObtieneSucursalesEspeciales_Reimpresion);
            return result;
        }

        /// <summary>
        ///Get ObtieneSucursalesEspeciales_Reimpresion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ObtieneSucursalesEspeciales_ReimpresionEntity> GetAll()
        {
            List<ObtieneSucursalesEspeciales_ReimpresionEntity> entities = new List<ObtieneSucursalesEspeciales_ReimpresionEntity>();
            entities = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.GetObtieneSucursalesEspeciales_Reimpresion();

            return entities ?? new List<ObtieneSucursalesEspeciales_ReimpresionEntity>();
        }

        /// <summary>
        ///Get ObtieneSucursalesEspeciales_Reimpresion List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ObtieneSucursalesEspeciales_ReimpresionEntity> GetAll(List<int> lid)
        {
            List<ObtieneSucursalesEspeciales_ReimpresionEntity> entities = new List<ObtieneSucursalesEspeciales_ReimpresionEntity>();
            entities = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.GetObtieneSucursalesEspeciales_Reimpresion(lid);
            return entities ?? new List<ObtieneSucursalesEspeciales_ReimpresionEntity>();
        }

        /// <summary>
        ///Get ObtieneSucursalesEspeciales_Reimpresion By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ObtieneSucursalesEspeciales_ReimpresionEntity GetOne()
        {
            ObtieneSucursalesEspeciales_ReimpresionEntity result = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.GetObtieneSucursalesEspeciales_ReimpresionById();

            return result;
        }

        /// <summary>
        ///Get ObtieneSucursalesEspeciales_Reimpresion By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ObtieneSucursalesEspeciales_ReimpresionEntity GetOneDeep()
        {
            ObtieneSucursalesEspeciales_ReimpresionEntity result = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.GetObtieneSucursalesEspeciales_ReimpresionById();

            return result;
        }



        /// <summary>
        ///Get ObtieneSucursalesEspeciales_Reimpresion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity> entities = new SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity>();
            entities = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity>();
        }

        /// <summary>
        ///Get ObtieneSucursalesEspeciales_Reimpresion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity> entities = new SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity>();
            entities = ProviderSoftv.ObtieneSucursalesEspeciales_Reimpresion.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ObtieneSucursalesEspeciales_ReimpresionEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

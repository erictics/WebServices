﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : OrdSerBussines
/// File                    : OrdSerBussines.cs
/// Creation date           : 16/02/2017
/// Creation time           : 05:44 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class OrdSer
    {

        #region Constructors
        public OrdSer() { }
        #endregion

        /// <summary>
        ///Adds OrdSer
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(OrdSerEntity objOrdSer)
        {
            int result = ProviderSoftv.OrdSer.AddOrdSer(objOrdSer);
            return result;
        }

        /// <summary>
        ///Delete OrdSer
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(long? clv_orden)
        {
            int resultado = ProviderSoftv.OrdSer.DeleteOrdSer(clv_orden);
            return resultado;
        }

        /// <summary>
        ///Update OrdSer
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(OrdSerEntity objOrdSer)
        {
            int result = ProviderSoftv.OrdSer.EditOrdSer(objOrdSer);
            return result;
        }

        /// <summary>
        ///Get OrdSer
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<OrdSerEntity> GetAll()
        {
            List<OrdSerEntity> entities = new List<OrdSerEntity>();
            entities = ProviderSoftv.OrdSer.GetOrdSer();

            return entities ?? new List<OrdSerEntity>();
        }




        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<OrdSerEntity> GetModOrdSer(long? clv_orden, int? Clv_TipSer, long? Contrato, String Fec_Sol, String Fec_Eje, String Visita1, String Visita2, String Status, int? Clv_Tecnico, bool? IMPRESA, long? Clv_FACTURA, String Obs, String ListadeArticulos)
        {
            List<OrdSerEntity> entities = new List<OrdSerEntity>();
            entities = ProviderSoftv.OrdSer.GetModOrdSer(clv_orden, Clv_TipSer, Contrato, Fec_Sol, Fec_Eje, Visita1, Visita2, Status, Clv_Tecnico, IMPRESA, Clv_FACTURA, Obs, ListadeArticulos);

            return entities ?? new List<OrdSerEntity>();
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static string GetSP_ValidaGuardaOrdSerAparatos(long? CLV_ORDEN, string OPCION, string STATUS, int? OP2, long? Clv_Tecnico)
        {

            string entities = ProviderSoftv.OrdSer.GetSP_ValidaGuardaOrdSerAparatos(CLV_ORDEN, OPCION, STATUS, OP2, Clv_Tecnico);
            return entities;
        }



        /// <summary>
        ///Get OrdSer List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<OrdSerEntity> GetAll(List<int> lid)
        {
            List<OrdSerEntity> entities = new List<OrdSerEntity>();
            entities = ProviderSoftv.OrdSer.GetOrdSer(lid);
            return entities ?? new List<OrdSerEntity>();
        }

        /// <summary>
        ///Get OrdSer By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static OrdSerEntity GetOne(long? clv_orden)
        {
            OrdSerEntity result = ProviderSoftv.OrdSer.GetOrdSerById(clv_orden);

            return result;
        }

        /// <summary>
        ///Get OrdSer By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static OrdSerEntity GetOneDeep(long? clv_orden)
        {
            OrdSerEntity result = ProviderSoftv.OrdSer.GetOrdSerById(clv_orden);

            return result;
        }



        /// <summary>
        ///Get OrdSer
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<OrdSerEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<OrdSerEntity> entities = new SoftvList<OrdSerEntity>();
            entities = ProviderSoftv.OrdSer.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<OrdSerEntity>();
        }

        /// <summary>
        ///Get OrdSer
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<OrdSerEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<OrdSerEntity> entities = new SoftvList<OrdSerEntity>();
            entities = ProviderSoftv.OrdSer.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<OrdSerEntity>();
        }


        public static List<StatusAparatosEntity> GetSP_StatusAparatos()
        {
            List<StatusAparatosEntity> lista = null;
            lista = ProviderSoftv.OrdSer.GetSP_StatusAparatos();
            return lista;

        }


        public static sp_validaEliminarOrdenEntity Getsp_validaEliminarOrdenser(String ClvUsuario)
        {
            sp_validaEliminarOrdenEntity enti = null;

            enti = ProviderSoftv.OrdSer.Getsp_validaEliminarOrdenser(ClvUsuario);

            return enti;
        }


        public static string GetReporteOrdenServicio(ReporteOrdenServicioEntity obj)
        {
            string result = ProviderSoftv.OrdSer.GetReporteOrdenServicio(obj);

            return result;
        }


        public static Softv_ObtenTipoMaterialEntity GetSoftv_ObtenTipoMaterial(Softv_ObtenTipoMaterialEntity Softv_ObtenTipoMaterialEntity)
        {

            Softv_ObtenTipoMaterialEntity obj = null;
            obj = ProviderSoftv.OrdSer.GetSoftv_ObtenTipoMaterial(Softv_ObtenTipoMaterialEntity);
            return obj;
        }


        public static DameCitaOrdenQuejaEntity GetDameCitaOrdenQueja(long? clv_queja_orden, int? opcion)
        {
            DameCitaOrdenQuejaEntity obj = null;
            obj = ProviderSoftv.OrdSer.GetDameCitaOrdenQueja(clv_queja_orden, opcion);
            return obj;

        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int Getsp_BorraArticulosAsignados(long? clv_orden)
        {
            int result;
            result = ProviderSoftv.OrdSer.Getsp_BorraArticulosAsignados(clv_orden);

            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<TapEntity> GetObtieneTap(long? contrato)
        {
            List<TapEntity> result = ProviderSoftv.OrdSer.GetObtieneTap(contrato);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<TapEntity> GetObtieneNap(long? contrato)
        {
            List<TapEntity> result = ProviderSoftv.OrdSer.GetObtieneNap(contrato);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int? GetGuardarRelClienteTap(long? CONTRATO, string TAP, int? idtap)
        {
            int? result = ProviderSoftv.OrdSer.GetGuardarRelClienteTap(CONTRATO, TAP, idtap);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int? GetGuardarRelClienteNap(long? CONTRATO, string TAP, int? IDTAP)
        {
            int? result = ProviderSoftv.OrdSer.GetGuardarRelClienteNap(CONTRATO, TAP, IDTAP);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static ClienteTapNapEntity GetTieneTapNap(ClienteTapNapEntity ObjClienteTapNap)
        {
            ClienteTapNapEntity result = ProviderSoftv.OrdSer.GetTieneTapNap(ObjClienteTapNap);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static TapEntity GetConTapCliente(long? Contrato)
        {
            TapEntity result = ProviderSoftv.OrdSer.GetConTapCliente(Contrato);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static TapEntity GetTraerNap(int? contrato)
        {
            TapEntity result = ProviderSoftv.OrdSer.GetTraerNap(contrato);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int? GetGuardaHoraOrden(long? Clv_orden, string horaInicio, string horaFin, int? opcion)
        {
            int? result = ProviderSoftv.OrdSer.GetGuardaHoraOrden(Clv_orden, horaInicio, horaFin, opcion);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int? GetNUECANEX(ExtencionEntity ObjExt)
        {
            int? result = ProviderSoftv.OrdSer.GetNUECANEX(ObjExt);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static ExtencionEntity GetDameExteciones_Cli(long? Contrato)
        {
            ExtencionEntity result = ProviderSoftv.OrdSer.GetDameExteciones_Cli(Contrato);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static ExtencionEntity GetCONCANEX(ExtencionEntity ObjExt)
        {
            ExtencionEntity result = ProviderSoftv.OrdSer.GetCONCANEX(ObjExt);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int? GetMODCANEX(ExtencionEntity ObjExt)
        {
            int? result = ProviderSoftv.OrdSer.GetMODCANEX(ObjExt);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int? GetBORDetOrdSer_INTELIGENTE(long? Clave)
        {
            int? result = ProviderSoftv.OrdSer.GetBORDetOrdSer_INTELIGENTE(Clave);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static int? GetBORCANEX(ExtencionEntity ObjExt)
        {
            int? result = ProviderSoftv.OrdSer.GetBORCANEX(ObjExt);
            return result;
        }


        public static int? GetCONCONEX(long? Clave, long? Clv_Orden, long? Contrato)
        {
            int? result = ProviderSoftv.OrdSer.GetCONCONEX(Clave, Clv_Orden, Contrato);
            return result;

        }
        public static int? GetNueConex(long? Clave, long? Clv_Orden, long? Contrato, int? ExtAdic)
        {
            int? result = ProviderSoftv.OrdSer.GetNueConex(Clave, Clv_Orden, Contrato, ExtAdic);
            return result;
        }
        public static int? GetMODCONEX(long? Clave, long? Clv_Orden, long? Contrato, int? ExtAdic)
        {
            int? result = ProviderSoftv.OrdSer.GetNueConex(Clave, Clv_Orden, Contrato, ExtAdic);
            return result;
        }

        public static TecnicosEntity GetConTecnicoAgenda(long? Clv, int? Op)
        {
            TecnicosEntity result = ProviderSoftv.OrdSer.GetConTecnicoAgenda(Clv, Op);
            return result;
        }

        public static ResumenOrdSerEntity GetResumenOrdenes(long? IdCompania,long ? op){

            ResumenOrdSerEntity result = ProviderSoftv.OrdSer.GetResumenOrdenes(IdCompania, op);
            return result;

        }

        public static HoraOrdQueEntity GetConHIHF_OrdenQeja(string Op, long? Clave)
        {
            HoraOrdQueEntity result = ProviderSoftv.OrdSer.GetConHIHF_OrdenQeja(Op, Clave);
            return result;
        }

    }

    #region Customs Methods

    #endregion
}

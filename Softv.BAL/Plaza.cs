﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : PlazaBussines
/// File                    : PlazaBussines.cs
/// Creation date           : 02/03/2016
/// Creation time           : 10:04 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class Plaza
    {

        #region Constructors
        public Plaza() { }
        #endregion

        /// <summary>
        ///Adds Plaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(DireccionplazaAlmacenEntity objPlaza)
        {
            int result = ProviderSoftv.Plaza.AddPlaza(objPlaza);
            return result;
        }

        /// <summary>
        ///Delete Plaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(int? IdPlaza)
        {
            int resultado = ProviderSoftv.Plaza.DeletePlaza(IdPlaza);
            return resultado;
        }

        /// <summary>
        ///Update Plaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(DireccionplazaAlmacenEntity objPlaza)
        {
            int result = ProviderSoftv.Plaza.EditPlaza(objPlaza);
            return result;
        }


        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static DireccionplazaAlmacenEntity GetObtendatosPlaza(int? idcompania)
        {
            DireccionplazaAlmacenEntity result = ProviderSoftv.Plaza.GetObtendatosPlaza(idcompania);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static List<RelCompaniaCiudadEntity> GetAgregaEliminaRelCompaniaCiudad2(int? opcion, int? idcompania, int? clv_ciudad, int? clv_estado)
        {
            List<RelCompaniaCiudadEntity> result = ProviderSoftv.Plaza.GetAgregaEliminaRelCompaniaCiudad2( opcion, idcompania,clv_ciudad, clv_estado);
            return result;
        }


        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static List<CiudadCAMDOEntity> GetMuestra_Ciudad_RelCompania(int? idcompania, int? clvestado)
        {
             List< CiudadCAMDOEntity > result = ProviderSoftv.Plaza.GetMuestra_Ciudad_RelCompania( idcompania, clvestado);
            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static List<EstadoEntity> GetMuestraEstadosFrmCompania(int? idcompania)
        {
            List<EstadoEntity> result = ProviderSoftv.Plaza.GetMuestraEstadosFrmCompania(idcompania);
            return result;
        }


        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static List<PlazaEntity> GetBrwMuestraCompanias(int? opcion, string razon, int? idcompania)
        {
            List<PlazaEntity> result = ProviderSoftv.Plaza.GetBrwMuestraCompanias(opcion, razon,idcompania);
            return result;
        }


        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int AddRelMunicipioEst(String xml)
        {
            int result = ProviderSoftv.Plaza.AddRelMunicipioEst(xml);
            return result;
        }


        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int UpdateRelMunicipioEst(String xml)
        {
            int result = ProviderSoftv.Plaza.UpdateRelMunicipioEst(xml);
            return result;
        }











        /// <summary>
        ///Get Plaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<PlazaEntity> GetAll(long? IdUsuario)
        {
            List<PlazaEntity> entities = new List<PlazaEntity>();
            entities = ProviderSoftv.Plaza.GetPlaza(IdUsuario);

            return entities ?? new List<PlazaEntity>();
        }

        /// <summary>
        ///Get Plaza List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<PlazaEntity> GetAll(List<int> lid)
        {
            List<PlazaEntity> entities = new List<PlazaEntity>();
            entities = ProviderSoftv.Plaza.GetPlaza(lid);
            return entities ?? new List<PlazaEntity>();
        }

        /// <summary>
        ///Get Plaza By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static PlazaEntity GetOne(int? IdPlaza)
        {
            PlazaEntity result = ProviderSoftv.Plaza.GetPlazaById(IdPlaza);

            return result;
        }

        /// <summary>
        ///Get Plaza By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static PlazaEntity GetOneDeep(int? IdPlaza)
        {
            PlazaEntity result = ProviderSoftv.Plaza.GetPlazaById(IdPlaza);

            //result.Distribuidor = ProviderSoftv.Distribuidor.GetDistribuidorByIdDistribuidor(result.IdDistribuidor);

            //result.Cliente = ProviderSoftv.Cliente.GetClienteByIdPlaza(result.IdPlaza);

            //result.RelPlazaEstMun = ProviderSoftv.RelPlazaEstMun.GetRelPlazaEstMunByIdPlaza(result.IdPlaza);


            //List<MunicipioEntity> lMunicipio = ProviderSoftv.Municipio.GetMunicipio(result.RelPlazaEstMun.Where(x => x.IdMunicipio.HasValue).Select(x => x.IdMunicipio.Value).ToList());
            //lMunicipio.ForEach(XMunicipio => result.RelPlazaEstMun.Where(x => x.IdMunicipio.HasValue).Where(x => x.IdMunicipio == XMunicipio.IdMunicipio).ToList().ForEach(y => y.Municipio = XMunicipio));

            //List<EstadoEntity> lEstado = ProviderSoftv.Estado.GetEstado(result.RelPlazaEstMun.Where(x => x.IdEstado.HasValue).Select(x => x.IdEstado.Value).ToList());
            //lEstado.ForEach(XEstado => result.RelPlazaEstMun.Where(x => x.IdEstado.HasValue).Where(x => x.IdEstado == XEstado.IdEstado).ToList().ForEach(y => y.Estado = XEstado));

            //List<DistribuidorEntity> lDistribuidor = ProviderSoftv.Distribuidor.GetDistribuidor(result.RelPlazaEstMun.Where(x => x.IdPlaza.HasValue).Select(x => x.IdEstado.Value).ToList());
            //lEstado.ForEach(XEstado => result.RelPlazaEstMun.Where(x => x.IdEstado.HasValue).Where(x => x.IdEstado == XEstado.IdEstado).ToList().ForEach(y => y.Estado = XEstado));

            return result;
        }

        /// <summary>
        ///Change State Plaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int ChangeState(int? IdPlaza, bool State)
        {
            int resultado = ProviderSoftv.Plaza.ChangeStatePlaza(IdPlaza, State);
            return resultado;
        }



        /// <summary>
        ///Get Plaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<PlazaEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<PlazaEntity> entities = new SoftvList<PlazaEntity>();
            entities = ProviderSoftv.Plaza.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<PlazaEntity>();
        }

        /// <summary>
        ///Get Plaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<PlazaEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<PlazaEntity> entities = new SoftvList<PlazaEntity>();
            entities = ProviderSoftv.Plaza.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<PlazaEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

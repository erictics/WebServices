﻿
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Data;
    using System.ComponentModel;
    using System.Linq;
    using Softv.Providers;
    using Softv.Entities;
    using Globals;

    /// <summary>
    /// Class                   : Softv.BAL.Client.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : PlazasXmlBussines
    /// File                    : PlazasXmlBussines.cs
    /// Creation date           : 18/11/2016
    /// Creation time           : 10:30 a. m.
    ///</summary>
    namespace Softv.BAL
    {

    [DataObject]
    [Serializable]
    public class PlazasXml
    {

    #region Constructors
    public PlazasXml(){}
    #endregion

    /// <summary>
    ///Adds PlazasXml
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public static int Add(PlazasXmlEntity objPlazasXml)
  {
  int result = ProviderSoftv.PlazasXml.AddPlazasXml(objPlazasXml);
    return result;
    }

    /// <summary>
    ///Delete PlazasXml
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public static int Delete()
    {
    int resultado = ProviderSoftv.PlazasXml.DeletePlazasXml();
    return resultado;
    }

    /// <summary>
    ///Update PlazasXml
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public static int Edit(PlazasXmlEntity objPlazasXml)
    {
    int result = ProviderSoftv.PlazasXml.EditPlazasXml(objPlazasXml);
    return result;
    }

    /// <summary>
    ///Get PlazasXml
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<PlazasXmlEntity> GetAll()
    {
    List<PlazasXmlEntity> entities = new List<PlazasXmlEntity> ();
    entities = ProviderSoftv.PlazasXml.GetPlazasXml();
    
    return entities ?? new List<PlazasXmlEntity>();
    }

    /// <summary>
    ///Get PlazasXml List<lid>
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<PlazasXmlEntity> GetAll(List<int> lid)
    {
    List<PlazasXmlEntity> entities = new List<PlazasXmlEntity> ();
    entities = ProviderSoftv.PlazasXml.GetPlazasXml(lid);    
    return entities ?? new List<PlazasXmlEntity>();
    }

    /// <summary>
    ///Get PlazasXml By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static PlazasXmlEntity GetOne()
    {
    PlazasXmlEntity result = ProviderSoftv.PlazasXml.GetPlazasXmlById();
    
    return result;
    }

    /// <summary>
    ///Get PlazasXml By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static PlazasXmlEntity GetOneDeep()
    {
    PlazasXmlEntity result = ProviderSoftv.PlazasXml.GetPlazasXmlById();
    
    return result;
    }
    

    
      /// <summary>
      ///Get PlazasXml
      ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<PlazasXmlEntity> GetPagedList(int pageIndex, int pageSize)
    {
    SoftvList<PlazasXmlEntity> entities = new SoftvList<PlazasXmlEntity>();
    entities = ProviderSoftv.PlazasXml.GetPagedList(pageIndex, pageSize);
    
    return entities ?? new SoftvList<PlazasXmlEntity>();
    }

    /// <summary>
    ///Get PlazasXml
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<PlazasXmlEntity> GetPagedList(int pageIndex, int pageSize,String xml)
    {
    SoftvList<PlazasXmlEntity> entities = new SoftvList<PlazasXmlEntity>();
    entities = ProviderSoftv.PlazasXml.GetPagedList(pageIndex, pageSize,xml);
    
    return entities ?? new SoftvList<PlazasXmlEntity>();
    }


    }




    #region Customs Methods
    
    #endregion
    }
  
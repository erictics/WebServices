﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;
using System.IO;

namespace Softv.BAL
{
    [DataObject]
    [Serializable]
    public class PolizaMaestro
    {
        #region Constructors
        public PolizaMaestro() { }
        #endregion

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static List<PolizasMaestroEntity> GetObtienePolizasMaestro(FiltroPolizaEntity filtros)
        {
            List<PolizasMaestroEntity> result = ProviderSoftv.PolizaMaestro.GetObtienePolizasMaestro(filtros);

            return result;
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static int? GetEliminaPoliza(FiltroPolizaEntity filtros)
        {
            int? a = ProviderSoftv.PolizaMaestro.GetEliminaPoliza(filtros);

            return a;
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static List<DetallePolizaEntity> GetDetallesPolizaMaestro(FiltroPolizaEntity filtros)
        {
            List<DetallePolizaEntity> a = ProviderSoftv.PolizaMaestro.GetDetallesPolizaMaestro(filtros);

            return a;
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static PolizasMaestroEntity GetGeneraNuevaPolizaMaestro(FiltroPolizaEntity filtros)
        {
            PolizasMaestroEntity a = ProviderSoftv.PolizaMaestro.GetGeneraNuevaPolizaMaestro(filtros);

            return a;
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static string GetPolizaTxt(FiltroPolizaEntity filtros)
        {
            string a = ProviderSoftv.PolizaMaestro.GetPolizaTxt(filtros);

            return a;
        }
    }

    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : QuitarAntenaDevolucionBussines
/// File                    : QuitarAntenaDevolucionBussines.cs
/// Creation date           : 24/10/2016
/// Creation time           : 06:33 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class QuitarAntenaDevolucion
    {

        #region Constructors
        public QuitarAntenaDevolucion() { }
        #endregion


        /// <summary>
        ///Delete QuitarAntenaDevolucion
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(long? IdSession, int? IdTipSer)
        {
            int resultado = ProviderSoftv.QuitarAntenaDevolucion.DeleteQuitarAntenaDevolucion(IdSession, IdTipSer);
            return resultado;
        }

        



        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int DeleteBorraCobraAdeudo(long? IdSession) 
        {
            int resultado = ProviderSoftv.QuitarAntenaDevolucion.DeleteBorraCobraAdeudo(IdSession);
            return resultado;
        }




    }




    #region Customs Methods

    #endregion
}

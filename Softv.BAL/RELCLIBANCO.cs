﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : RELCLIBANCOBussines
/// File                    : RELCLIBANCOBussines.cs
/// Creation date           : 08/09/2017
/// Creation time           : 12:36 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class RELCLIBANCO
    {

        #region Constructors
        public RELCLIBANCO() { }
        #endregion

        /// <summary>
        ///Adds RELCLIBANCO
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(RELCLIBANCOEntity objRELCLIBANCO)
        {
            int result = ProviderSoftv.RELCLIBANCO.AddRELCLIBANCO(objRELCLIBANCO);
            return result;
        }

        /// <summary>
        ///Delete RELCLIBANCO
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(long? Contrato)
        {
            int resultado = ProviderSoftv.RELCLIBANCO.DeleteRELCLIBANCO(Contrato);
            return resultado;
        }

        /// <summary>
        ///Update RELCLIBANCO
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(RELCLIBANCOEntity objRELCLIBANCO)
        {
            int result = ProviderSoftv.RELCLIBANCO.EditRELCLIBANCO(objRELCLIBANCO);
            return result;
        }

        /// <summary>
        ///Get RELCLIBANCO
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<RELCLIBANCOEntity> GetAll(long? Contrato)
        {
            List<RELCLIBANCOEntity> entities = new List<RELCLIBANCOEntity>();
            entities = ProviderSoftv.RELCLIBANCO.GetRELCLIBANCO(Contrato);

            return entities ?? new List<RELCLIBANCOEntity>();
        }

        /// <summary>
        ///Get RELCLIBANCO List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<RELCLIBANCOEntity> GetAll(List<int> lid)
        {
            List<RELCLIBANCOEntity> entities = new List<RELCLIBANCOEntity>();
            entities = ProviderSoftv.RELCLIBANCO.GetRELCLIBANCO(lid);
            return entities ?? new List<RELCLIBANCOEntity>();
        }

        /// <summary>
        ///Get RELCLIBANCO By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static RELCLIBANCOEntity GetOne()
        {
            RELCLIBANCOEntity result = ProviderSoftv.RELCLIBANCO.GetRELCLIBANCOById();

            return result;
        }

        /// <summary>
        ///Get RELCLIBANCO By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static RELCLIBANCOEntity GetOneDeep()
        {
            RELCLIBANCOEntity result = ProviderSoftv.RELCLIBANCO.GetRELCLIBANCOById();

            return result;
        }



        /// <summary>
        ///Get RELCLIBANCO
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<RELCLIBANCOEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<RELCLIBANCOEntity> entities = new SoftvList<RELCLIBANCOEntity>();
            entities = ProviderSoftv.RELCLIBANCO.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<RELCLIBANCOEntity>();
        }

        /// <summary>
        ///Get RELCLIBANCO
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<RELCLIBANCOEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<RELCLIBANCOEntity> entities = new SoftvList<RELCLIBANCOEntity>();
            entities = ProviderSoftv.RELCLIBANCO.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<RELCLIBANCOEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

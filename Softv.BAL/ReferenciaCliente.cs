﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ReferenciaClienteBussines
/// File                    : ReferenciaClienteBussines.cs
/// Creation date           : 27/02/2016
/// Creation time           : 01:09 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ReferenciaCliente
    {

        #region Constructors
        public ReferenciaCliente() { }
        #endregion

        /// <summary>
        ///Adds ReferenciaCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ReferenciaClienteEntity objReferenciaCliente)
        {
            int result = ProviderSoftv.ReferenciaCliente.AddReferenciaCliente(objReferenciaCliente);
            return result;
        }

        /// <summary>
        ///Delete ReferenciaCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(int? IdReferencia)
        {
            int resultado = ProviderSoftv.ReferenciaCliente.DeleteReferenciaCliente(IdReferencia);
            return resultado;
        }

        /// <summary>
        ///Update ReferenciaCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ReferenciaClienteEntity objReferenciaCliente)
        {
            int result = ProviderSoftv.ReferenciaCliente.EditReferenciaCliente(objReferenciaCliente);
            return result;
        }

        /// <summary>
        ///Get ReferenciaCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ReferenciaClienteEntity> GetAll()
        {
            List<ReferenciaClienteEntity> entities = new List<ReferenciaClienteEntity>();
            entities = ProviderSoftv.ReferenciaCliente.GetReferenciaCliente();

            return entities ?? new List<ReferenciaClienteEntity>();
        }















        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ReferenciaClienteEntity> GetReferenciaClienteL(long? IdContrato)
        {
            List<ReferenciaClienteEntity> entities = new List<ReferenciaClienteEntity>();
            entities = ProviderSoftv.ReferenciaCliente.GetReferenciaClienteL(IdContrato);

            return entities ?? new List<ReferenciaClienteEntity>();
        }

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int UpdateReferencia(String xml)
        {
            int result = ProviderSoftv.ReferenciaCliente.UpdateReferencia(xml);
            return result;
        }













        /// <summary>
        ///Get ReferenciaCliente List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ReferenciaClienteEntity> GetAll(List<int> lid)
        {
            List<ReferenciaClienteEntity> entities = new List<ReferenciaClienteEntity>();
            entities = ProviderSoftv.ReferenciaCliente.GetReferenciaCliente(lid);
            return entities ?? new List<ReferenciaClienteEntity>();
        }

        /// <summary>
        ///Get ReferenciaCliente By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ReferenciaClienteEntity GetOne(int? IdReferencia)
        {
            ReferenciaClienteEntity result = ProviderSoftv.ReferenciaCliente.GetReferenciaClienteById(IdReferencia);

            return result;
        }

        /// <summary>
        ///Get ReferenciaCliente By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ReferenciaClienteEntity GetOneDeep(int? IdReferencia)
        {
            ReferenciaClienteEntity result = ProviderSoftv.ReferenciaCliente.GetReferenciaClienteById(IdReferencia);

            result.Cliente = ProviderSoftv.Cliente.GetClienteByIdContrato(result.IdContrato);

            return result;
        }



        /// <summary>
        ///Get ReferenciaCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ReferenciaClienteEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ReferenciaClienteEntity> entities = new SoftvList<ReferenciaClienteEntity>();
            entities = ProviderSoftv.ReferenciaCliente.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ReferenciaClienteEntity>();
        }

        /// <summary>
        ///Get ReferenciaCliente
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ReferenciaClienteEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ReferenciaClienteEntity> entities = new SoftvList<ReferenciaClienteEntity>();
            entities = ProviderSoftv.ReferenciaCliente.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ReferenciaClienteEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : RelLocalidadMunEstBussines
/// File                    : RelLocalidadMunEstBussines.cs
/// Creation date           : 03/02/2016
/// Creation time           : 06:54 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class RelLocalidadMunEst
    {

        #region Constructors
        public RelLocalidadMunEst() { }
        #endregion

        /// <summary>
        ///Adds RelLocalidadMunEst
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(RelLocalidadMunEstEntity objRelLocalidadMunEst)
        {
            int result = ProviderSoftv.RelLocalidadMunEst.AddRelLocalidadMunEst(objRelLocalidadMunEst);
            return result;
        }

        /// <summary>
        ///Delete RelLocalidadMunEst
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(int? IdLocalidad, int? IdMunicipio, int? IdEstado)
        {
            int resultado = ProviderSoftv.RelLocalidadMunEst.DeleteRelLocalidadMunEst(IdLocalidad, IdMunicipio, IdEstado);
            return resultado;
        }

        /// <summary>
        ///Update RelLocalidadMunEst
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(RelLocalidadMunEstEntity objRelLocalidadMunEst)
        {
            int result = ProviderSoftv.RelLocalidadMunEst.EditRelLocalidadMunEst(objRelLocalidadMunEst);
            return result;
        }

        /// <summary>
        ///Get RelLocalidadMunEst
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<RelLocalidadMunEstEntity> GetAll()
        {
            List<RelLocalidadMunEstEntity> entities = new List<RelLocalidadMunEstEntity>();
            entities = ProviderSoftv.RelLocalidadMunEst.GetRelLocalidadMunEst();

            List<EstadoEntity> lEstado = ProviderSoftv.Estado.GetEstado(entities.Where(x => x.IdEstado.HasValue).Select(x => x.IdEstado.Value).ToList());
            lEstado.ForEach(XEstado => entities.Where(x => x.IdEstado.HasValue).Where(x => x.IdEstado == XEstado.IdEstado).ToList().ForEach(y => y.Estado = XEstado));

            List<LocalidadEntity> lLocalidad = ProviderSoftv.Localidad.GetLocalidad(entities.Where(x => x.IdLocalidad.HasValue).Select(x => x.IdLocalidad.Value).ToList());
            lLocalidad.ForEach(XLocalidad => entities.Where(x => x.IdLocalidad.HasValue).Where(x => x.IdLocalidad == XLocalidad.IdLocalidad).ToList().ForEach(y => y.Localidad = XLocalidad));

            List<MunicipioEntity> lMunicipio = ProviderSoftv.Municipio.GetMunicipio(entities.Where(x => x.IdMunicipio.HasValue).Select(x => x.IdMunicipio.Value).ToList());
            lMunicipio.ForEach(XMunicipio => entities.Where(x => x.IdMunicipio.HasValue).Where(x => x.IdMunicipio == XMunicipio.IdMunicipio).ToList().ForEach(y => y.Municipio = XMunicipio));

            return entities ?? new List<RelLocalidadMunEstEntity>();
        }

        /// <summary>
        ///Get RelLocalidadMunEst List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<RelLocalidadMunEstEntity> GetAll(List<int> lid)
        {
            List<RelLocalidadMunEstEntity> entities = new List<RelLocalidadMunEstEntity>();
            entities = ProviderSoftv.RelLocalidadMunEst.GetRelLocalidadMunEst(lid);
            return entities ?? new List<RelLocalidadMunEstEntity>();
        }

        /// <summary>
        ///Get RelLocalidadMunEst By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static RelLocalidadMunEstEntity GetOne(int? IdLocalidad, int? IdMunicipio, int? IdEstado)
        {
            RelLocalidadMunEstEntity result = ProviderSoftv.RelLocalidadMunEst.GetRelLocalidadMunEstById(IdLocalidad, IdMunicipio, IdEstado);

            if (result.IdEstado != null)
                result.Estado = ProviderSoftv.Estado.GetEstadoById(result.IdEstado);

            if (result.IdLocalidad != null)
                result.Localidad = ProviderSoftv.Localidad.GetLocalidadById(result.IdLocalidad);

            if (result.IdMunicipio != null)
                result.Municipio = ProviderSoftv.Municipio.GetMunicipioById(result.IdMunicipio);

            return result;
        }

        /// <summary>
        ///Get RelLocalidadMunEst By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static RelLocalidadMunEstEntity GetOneDeep(int? IdLocalidad, int? IdMunicipio, int? IdEstado)
        {
            RelLocalidadMunEstEntity result = ProviderSoftv.RelLocalidadMunEst.GetRelLocalidadMunEstById(IdLocalidad, IdMunicipio, IdEstado);

            if (result.IdEstado != null)
                result.Estado = ProviderSoftv.Estado.GetEstadoById(result.IdEstado);

            if (result.IdLocalidad != null)
                result.Localidad = ProviderSoftv.Localidad.GetLocalidadById(result.IdLocalidad);

            if (result.IdMunicipio != null)
                result.Municipio = ProviderSoftv.Municipio.GetMunicipioById(result.IdMunicipio);

            return result;
        }

        public static List<RelLocalidadMunEstEntity> GetRelLocalidadMunEstByIdEstado(int? IdEstado)
        {
            List<RelLocalidadMunEstEntity> entities = new List<RelLocalidadMunEstEntity>();
            entities = ProviderSoftv.RelLocalidadMunEst.GetRelLocalidadMunEstByIdEstado(IdEstado);

            List<MunicipioEntity> lMunicipio = ProviderSoftv.Municipio.GetMunicipio(entities.Where(x => x.IdMunicipio.HasValue).Select(x => x.IdMunicipio.Value).ToList());
            lMunicipio.ForEach(XMunicipio => entities.Where(x => x.IdMunicipio.HasValue).Where(x => x.IdMunicipio == XMunicipio.IdMunicipio).ToList().ForEach(y => y.Municipio = XMunicipio));

            List<LocalidadEntity> lLocalidad = ProviderSoftv.Localidad.GetLocalidad(entities.Where(x => x.IdLocalidad.HasValue).Select(x => x.IdLocalidad.Value).ToList());
            lLocalidad.ForEach(XLocalidad => entities.Where(x => x.IdLocalidad.HasValue).Where(x => x.IdLocalidad == XLocalidad.IdLocalidad).ToList().ForEach(y => y.Localidad = XLocalidad));


            return entities ?? new List<RelLocalidadMunEstEntity>();
        }

        public static List<RelLocalidadMunEstEntity> GetRelLocalidadMunEstByIdLocalidad(int? IdLocalidad)
        {
            List<RelLocalidadMunEstEntity> entities = new List<RelLocalidadMunEstEntity>();
            entities = ProviderSoftv.RelLocalidadMunEst.GetRelLocalidadMunEstByIdLocalidad(IdLocalidad);

            List<EstadoEntity> lEstado = ProviderSoftv.Estado.GetEstado(entities.Where(x => x.IdEstado.HasValue).Select(x => x.IdEstado.Value).ToList());
            lEstado.ForEach(XEstado => entities.Where(x => x.IdEstado.HasValue).Where(x => x.IdEstado == XEstado.IdEstado).ToList().ForEach(y => y.Estado = XEstado));


            List<MunicipioEntity> lMunicipio = ProviderSoftv.Municipio.GetMunicipio(entities.Where(x => x.IdMunicipio.HasValue).Select(x => x.IdMunicipio.Value).ToList());
            lMunicipio.ForEach(XMunicipio => entities.Where(x => x.IdMunicipio.HasValue).Where(x => x.IdMunicipio == XMunicipio.IdMunicipio).ToList().ForEach(y => y.Municipio = XMunicipio));




            return entities ?? new List<RelLocalidadMunEstEntity>();
        }

        public static List<RelLocalidadMunEstEntity> GetRelLocalidadMunEstByIdMunicipio(int? IdMunicipio)
        {
            List<RelLocalidadMunEstEntity> entities = new List<RelLocalidadMunEstEntity>();
            entities = ProviderSoftv.RelLocalidadMunEst.GetRelLocalidadMunEstByIdMunicipio(IdMunicipio);

            List<LocalidadEntity> lLocalidad = ProviderSoftv.Localidad.GetLocalidad(entities.Where(x => x.IdLocalidad.HasValue).Select(x => x.IdLocalidad.Value).ToList());
            lLocalidad.ForEach(XLocalidad => entities.Where(x => x.IdLocalidad.HasValue).Where(x => x.IdLocalidad == XLocalidad.IdLocalidad).ToList().ForEach(y => y.Localidad = XLocalidad));
            
            return entities ?? new List<RelLocalidadMunEstEntity>();
        }



        /// <summary>
        ///Get RelLocalidadMunEst
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<RelLocalidadMunEstEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<RelLocalidadMunEstEntity> entities = new SoftvList<RelLocalidadMunEstEntity>();
            entities = ProviderSoftv.RelLocalidadMunEst.GetPagedList(pageIndex, pageSize);

            List<EstadoEntity> lEstado = ProviderSoftv.Estado.GetEstado(entities.Where(x => x.IdEstado.HasValue).Select(x => x.IdEstado.Value).Distinct().ToList());
            lEstado.ForEach(XEstado => entities.Where(x => x.IdEstado.HasValue).Where(x => x.IdEstado == XEstado.IdEstado).ToList().ForEach(y => y.Estado = XEstado));

            List<LocalidadEntity> lLocalidad = ProviderSoftv.Localidad.GetLocalidad(entities.Where(x => x.IdLocalidad.HasValue).Select(x => x.IdLocalidad.Value).Distinct().ToList());
            lLocalidad.ForEach(XLocalidad => entities.Where(x => x.IdLocalidad.HasValue).Where(x => x.IdLocalidad == XLocalidad.IdLocalidad).ToList().ForEach(y => y.Localidad = XLocalidad));

            List<MunicipioEntity> lMunicipio = ProviderSoftv.Municipio.GetMunicipio(entities.Where(x => x.IdMunicipio.HasValue).Select(x => x.IdMunicipio.Value).Distinct().ToList());
            lMunicipio.ForEach(XMunicipio => entities.Where(x => x.IdMunicipio.HasValue).Where(x => x.IdMunicipio == XMunicipio.IdMunicipio).ToList().ForEach(y => y.Municipio = XMunicipio));

            return entities ?? new SoftvList<RelLocalidadMunEstEntity>();
        }

        /// <summary>
        ///Get RelLocalidadMunEst
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<RelLocalidadMunEstEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<RelLocalidadMunEstEntity> entities = new SoftvList<RelLocalidadMunEstEntity>();
            entities = ProviderSoftv.RelLocalidadMunEst.GetPagedList(pageIndex, pageSize, xml);

            List<EstadoEntity> lEstado = ProviderSoftv.Estado.GetEstado(entities.Where(x => x.IdEstado.HasValue).Select(x => x.IdEstado.Value).Distinct().ToList());
            lEstado.ForEach(XEstado => entities.Where(x => x.IdEstado.HasValue).Where(x => x.IdEstado == XEstado.IdEstado).ToList().ForEach(y => y.Estado = XEstado));

            List<LocalidadEntity> lLocalidad = ProviderSoftv.Localidad.GetLocalidad(entities.Where(x => x.IdLocalidad.HasValue).Select(x => x.IdLocalidad.Value).Distinct().ToList());
            lLocalidad.ForEach(XLocalidad => entities.Where(x => x.IdLocalidad.HasValue).Where(x => x.IdLocalidad == XLocalidad.IdLocalidad).ToList().ForEach(y => y.Localidad = XLocalidad));

            List<MunicipioEntity> lMunicipio = ProviderSoftv.Municipio.GetMunicipio(entities.Where(x => x.IdMunicipio.HasValue).Select(x => x.IdMunicipio.Value).Distinct().ToList());
            lMunicipio.ForEach(XMunicipio => entities.Where(x => x.IdMunicipio.HasValue).Where(x => x.IdMunicipio == XMunicipio.IdMunicipio).ToList().ForEach(y => y.Municipio = XMunicipio));

            return entities ?? new SoftvList<RelLocalidadMunEstEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

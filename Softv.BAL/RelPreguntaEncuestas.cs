﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : RelPreguntaEncuestasBussines
/// File                    : RelPreguntaEncuestasBussines.cs
/// Creation date           : 17/06/2017
/// Creation time           : 11:06 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class RelPreguntaEncuestas
    {

        #region Constructors
        public RelPreguntaEncuestas() { }
        #endregion

        /// <summary>
        ///Adds RelPreguntaEncuestas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(RelPreguntaEncuestasEntity objRelPreguntaEncuestas)
        {
            int result = ProviderSoftv.RelPreguntaEncuestas.AddRelPreguntaEncuestas(objRelPreguntaEncuestas);
            return result;
        }

        /// <summary>
        ///Delete RelPreguntaEncuestas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.RelPreguntaEncuestas.DeleteRelPreguntaEncuestas();
            return resultado;
        }

        /// <summary>
        ///Update RelPreguntaEncuestas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(RelPreguntaEncuestasEntity objRelPreguntaEncuestas)
        {
            int result = ProviderSoftv.RelPreguntaEncuestas.EditRelPreguntaEncuestas(objRelPreguntaEncuestas);
            return result;
        }

        /// <summary>
        ///Get RelPreguntaEncuestas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<RelPreguntaEncuestasEntity> GetAll()
        {
            List<RelPreguntaEncuestasEntity> entities = new List<RelPreguntaEncuestasEntity>();
            entities = ProviderSoftv.RelPreguntaEncuestas.GetRelPreguntaEncuestas();

            List<PreguntasEntity> lPreguntas = ProviderSoftv.Preguntas.GetPreguntas(entities.Where(x => x.IdPregunta.HasValue).Select(x => x.IdPregunta.Value).ToList());
            lPreguntas.ForEach(XPreguntas => entities.Where(x => x.IdPregunta.HasValue).Where(x => x.IdPregunta == XPreguntas.IdPregunta).ToList().ForEach(y => y.Preguntas = XPreguntas));

            //List<PreguntasEntity> lPreguntas = ProviderSoftv.Preguntas.GetPreguntas(entities.Where(x => x.IdPregunta.HasValue).Select(x => x.IdPregunta.Value).ToList());
            //lPreguntas.ForEach(XPreguntas => entities.Where(x => x.IdPregunta.HasValue).Where(x => x.IdPregunta == XPreguntas.IdPregunta).ToList().ForEach(y => y.Preguntas = XPreguntas));

            return entities ?? new List<RelPreguntaEncuestasEntity>();
        }

        /// <summary>
        ///Get RelPreguntaEncuestas List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<RelPreguntaEncuestasEntity> GetAll(List<int> lid)
        {
            List<RelPreguntaEncuestasEntity> entities = new List<RelPreguntaEncuestasEntity>();
            entities = ProviderSoftv.RelPreguntaEncuestas.GetRelPreguntaEncuestas(lid);
            return entities ?? new List<RelPreguntaEncuestasEntity>();
        }

        /// <summary>
        ///Get RelPreguntaEncuestas By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static RelPreguntaEncuestasEntity GetOne()
        {
            RelPreguntaEncuestasEntity result = ProviderSoftv.RelPreguntaEncuestas.GetRelPreguntaEncuestasByIdd();

            if (result.IdPregunta != null)
                result.Preguntas = ProviderSoftv.Preguntas.GetPreguntasById(result.IdPregunta);

            //if (result.IdPregunta != null)
            //    result.Preguntas = ProviderSoftv.Preguntas.GetPreguntasById(result.IdPregunta);

            return result;
        }

        /// <summary>
        ///Get RelPreguntaEncuestas By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static RelPreguntaEncuestasEntity GetOneDeep()
        {
            RelPreguntaEncuestasEntity result = ProviderSoftv.RelPreguntaEncuestas.GetRelPreguntaEncuestasByIdd();

            if (result.IdPregunta != null)
                result.Preguntas = ProviderSoftv.Preguntas.GetPreguntasById(result.IdPregunta);

            //if (result.IdPregunta != null)
            //    result.Preguntas = ProviderSoftv.Preguntas.GetPreguntasById(result.IdPregunta);

            return result;
        }



        /// <summary>
        ///Get RelPreguntaEncuestas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<RelPreguntaEncuestasEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<RelPreguntaEncuestasEntity> entities = new SoftvList<RelPreguntaEncuestasEntity>();
            entities = ProviderSoftv.RelPreguntaEncuestas.GetPagedList(pageIndex, pageSize);

            List<PreguntasEntity> lPreguntas = ProviderSoftv.Preguntas.GetPreguntas(entities.Where(x => x.IdPregunta.HasValue).Select(x => x.IdPregunta.Value).Distinct().ToList());
            lPreguntas.ForEach(XPreguntas => entities.Where(x => x.IdPregunta.HasValue).Where(x => x.IdPregunta == XPreguntas.IdPregunta).ToList().ForEach(y => y.Preguntas = XPreguntas));

            //List<PreguntasEntity> lPreguntas = ProviderSoftv.Preguntas.GetPreguntas(entities.Where(x => x.IdPregunta.HasValue).Select(x => x.IdPregunta.Value).Distinct().ToList());
            //lPreguntas.ForEach(XPreguntas => entities.Where(x => x.IdPregunta.HasValue).Where(x => x.IdPregunta == XPreguntas.IdPregunta).ToList().ForEach(y => y.Preguntas = XPreguntas));

            return entities ?? new SoftvList<RelPreguntaEncuestasEntity>();
        }

        /// <summary>
        ///Get RelPreguntaEncuestas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<RelPreguntaEncuestasEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<RelPreguntaEncuestasEntity> entities = new SoftvList<RelPreguntaEncuestasEntity>();
            entities = ProviderSoftv.RelPreguntaEncuestas.GetPagedList(pageIndex, pageSize, xml);

            List<PreguntasEntity> lPreguntas = ProviderSoftv.Preguntas.GetPreguntas(entities.Where(x => x.IdPregunta.HasValue).Select(x => x.IdPregunta.Value).Distinct().ToList());
            lPreguntas.ForEach(XPreguntas => entities.Where(x => x.IdPregunta.HasValue).Where(x => x.IdPregunta == XPreguntas.IdPregunta).ToList().ForEach(y => y.Preguntas = XPreguntas));

            //List<PreguntasEntity> lPreguntas = ProviderSoftv.Preguntas.GetPreguntas(entities.Where(x => x.IdPregunta.HasValue).Select(x => x.IdPregunta.Value).Distinct().ToList());
            //lPreguntas.ForEach(XPreguntas => entities.Where(x => x.IdPregunta.HasValue).Where(x => x.IdPregunta == XPreguntas.IdPregunta).ToList().ForEach(y => y.Preguntas = XPreguntas));

            return entities ?? new SoftvList<RelPreguntaEncuestasEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

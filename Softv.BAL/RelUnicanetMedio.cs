﻿
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Data;
    using System.ComponentModel;
    using System.Linq;
    using Softv.Providers;
    using Softv.Entities;
    using Globals;

    /// <summary>
    /// Class                   : Softv.BAL.Client.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelUnicanetMedioBussines
    /// File                    : RelUnicanetMedioBussines.cs
    /// Creation date           : 22/08/2017
    /// Creation time           : 12:55 p. m.
    ///</summary>
    namespace Softv.BAL
    {

    [DataObject]
    [Serializable]
    public class RelUnicanetMedio
    {

    #region Constructors
    public RelUnicanetMedio(){}
    #endregion

    /// <summary>
    ///Adds RelUnicanetMedio
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public static int Add(RelUnicanetMedioEntity objRelUnicanetMedio)
  {
  int result = ProviderSoftv.RelUnicanetMedio.AddRelUnicanetMedio(objRelUnicanetMedio);
    return result;
    }

    /// <summary>
    ///Delete RelUnicanetMedio
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public static int Delete()
    {
    int resultado = ProviderSoftv.RelUnicanetMedio.DeleteRelUnicanetMedio();
    return resultado;
    }

    /// <summary>
    ///Update RelUnicanetMedio
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public static int Edit(RelUnicanetMedioEntity objRelUnicanetMedio)
    {
    int result = ProviderSoftv.RelUnicanetMedio.EditRelUnicanetMedio(objRelUnicanetMedio);
    return result;
    }

    /// <summary>
    ///Get RelUnicanetMedio
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<RelUnicanetMedioEntity> GetAll()
    {
    List<RelUnicanetMedioEntity> entities = new List<RelUnicanetMedioEntity> ();
    entities = ProviderSoftv.RelUnicanetMedio.GetRelUnicanetMedio();
    
    return entities ?? new List<RelUnicanetMedioEntity>();
    }

    /// <summary>
    ///Get RelUnicanetMedio List<lid>
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<RelUnicanetMedioEntity> GetAll(List<int> lid)
    {
    List<RelUnicanetMedioEntity> entities = new List<RelUnicanetMedioEntity> ();
    entities = ProviderSoftv.RelUnicanetMedio.GetRelUnicanetMedio(lid);    
    return entities ?? new List<RelUnicanetMedioEntity>();
    }

    /// <summary>
    ///Get RelUnicanetMedio By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static RelUnicanetMedioEntity GetOne()
    {
    RelUnicanetMedioEntity result = ProviderSoftv.RelUnicanetMedio.GetRelUnicanetMedioById();
    
    return result;
    }

    /// <summary>
    ///Get RelUnicanetMedio By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static RelUnicanetMedioEntity GetOneDeep()
    {
    RelUnicanetMedioEntity result = ProviderSoftv.RelUnicanetMedio.GetRelUnicanetMedioById();
    
    return result;
    }
    

    
      /// <summary>
      ///Get RelUnicanetMedio
      ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<RelUnicanetMedioEntity> GetPagedList(int pageIndex, int pageSize)
    {
    SoftvList<RelUnicanetMedioEntity> entities = new SoftvList<RelUnicanetMedioEntity>();
    entities = ProviderSoftv.RelUnicanetMedio.GetPagedList(pageIndex, pageSize);
    
    return entities ?? new SoftvList<RelUnicanetMedioEntity>();
    }

    /// <summary>
    ///Get RelUnicanetMedio
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<RelUnicanetMedioEntity> GetPagedList(int pageIndex, int pageSize,String xml)
    {
    SoftvList<RelUnicanetMedioEntity> entities = new SoftvList<RelUnicanetMedioEntity>();
    entities = ProviderSoftv.RelUnicanetMedio.GetPagedList(pageIndex, pageSize,xml);
    
    return entities ?? new SoftvList<RelUnicanetMedioEntity>();
    }


    }




    #region Customs Methods
    
    #endregion
    }
  
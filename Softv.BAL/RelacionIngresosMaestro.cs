﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;
using System.IO;

namespace Softv.BAL
{
    [DataObject]
    [Serializable]
    public class RelacionIngresosMaestro
    {

        #region Constructors
        public RelacionIngresosMaestro() { }
        #endregion

        /// <summary>
        ///Adds ContratoMaestroFac
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static string GetRelacionIngresosMaestro(List<int> Distribuidores, string FechaInicial, string FechaFinal, int Dolares)
        {
            string result = ProviderSoftv.RelacionIngresosMaestro.GetRelacionIngresosMaestro(Distribuidores, FechaInicial, FechaFinal, Dolares);
            return result;
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ReporteFacturasMaestrasVencidasBussines
/// File                    : ReporteFacturasMaestrasVencidasBussines.cs
/// Creation date           : 24/07/2017
/// Creation time           : 10:34 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ReporteFacturasMaestrasVencidas
    {

        #region Constructors
        public ReporteFacturasMaestrasVencidas() { }
        #endregion

        /// <summary>
        ///Adds ReporteFacturasMaestrasVencidas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ReporteFacturasMaestrasVencidasEntity objReporteFacturasMaestrasVencidas)
        {
            int result = ProviderSoftv.ReporteFacturasMaestrasVencidas.AddReporteFacturasMaestrasVencidas(objReporteFacturasMaestrasVencidas);
            return result;
        }

        /// <summary>
        ///Delete ReporteFacturasMaestrasVencidas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ReporteFacturasMaestrasVencidas.DeleteReporteFacturasMaestrasVencidas();
            return resultado;
        }

        /// <summary>
        ///Update ReporteFacturasMaestrasVencidas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ReporteFacturasMaestrasVencidasEntity objReporteFacturasMaestrasVencidas)
        {
            int result = ProviderSoftv.ReporteFacturasMaestrasVencidas.EditReporteFacturasMaestrasVencidas(objReporteFacturasMaestrasVencidas);
            return result;
        }

        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ReporteFacturasMaestrasVencidasEntity> GetAll(String Fecha, int? Vencidas, int? PorVencer)
        {
            List<ReporteFacturasMaestrasVencidasEntity> entities = new List<ReporteFacturasMaestrasVencidasEntity>();
            entities = ProviderSoftv.ReporteFacturasMaestrasVencidas.GetReporteFacturasMaestrasVencidas(Fecha, Vencidas, PorVencer);

            return entities ?? new List<ReporteFacturasMaestrasVencidasEntity>();
        }

        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ReporteFacturasMaestrasVencidasEntity> GetAll(List<int> lid)
        {
            List<ReporteFacturasMaestrasVencidasEntity> entities = new List<ReporteFacturasMaestrasVencidasEntity>();
            entities = ProviderSoftv.ReporteFacturasMaestrasVencidas.GetReporteFacturasMaestrasVencidas(lid);
            return entities ?? new List<ReporteFacturasMaestrasVencidasEntity>();
        }

        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ReporteFacturasMaestrasVencidasEntity GetOne()
        {
            ReporteFacturasMaestrasVencidasEntity result = ProviderSoftv.ReporteFacturasMaestrasVencidas.GetReporteFacturasMaestrasVencidasById();

            return result;
        }

        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ReporteFacturasMaestrasVencidasEntity GetOneDeep()
        {
            ReporteFacturasMaestrasVencidasEntity result = ProviderSoftv.ReporteFacturasMaestrasVencidas.GetReporteFacturasMaestrasVencidasById();

            return result;
        }



        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ReporteFacturasMaestrasVencidasEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ReporteFacturasMaestrasVencidasEntity> entities = new SoftvList<ReporteFacturasMaestrasVencidasEntity>();
            entities = ProviderSoftv.ReporteFacturasMaestrasVencidas.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ReporteFacturasMaestrasVencidasEntity>();
        }

        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ReporteFacturasMaestrasVencidasEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ReporteFacturasMaestrasVencidasEntity> entities = new SoftvList<ReporteFacturasMaestrasVencidasEntity>();
            entities = ProviderSoftv.ReporteFacturasMaestrasVencidas.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ReporteFacturasMaestrasVencidasEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

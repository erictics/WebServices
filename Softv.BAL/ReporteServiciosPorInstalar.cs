﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ReporteServiciosPorInstalarBussines
/// File                    : ReporteServiciosPorInstalarBussines.cs
/// Creation date           : 24/07/2017
/// Creation time           : 11:12 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ReporteServiciosPorInstalar
    {

        #region Constructors
        public ReporteServiciosPorInstalar() { }
        #endregion

        /// <summary>
        ///Adds ReporteServiciosPorInstalar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ReporteServiciosPorInstalarEntity objReporteServiciosPorInstalar)
        {
            int result = ProviderSoftv.ReporteServiciosPorInstalar.AddReporteServiciosPorInstalar(objReporteServiciosPorInstalar);
            return result;
        }

        /// <summary>
        ///Delete ReporteServiciosPorInstalar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ReporteServiciosPorInstalar.DeleteReporteServiciosPorInstalar();
            return resultado;
        }

        /// <summary>
        ///Update ReporteServiciosPorInstalar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ReporteServiciosPorInstalarEntity objReporteServiciosPorInstalar)
        {
            int result = ProviderSoftv.ReporteServiciosPorInstalar.EditReporteServiciosPorInstalar(objReporteServiciosPorInstalar);
            return result;
        }

        /// <summary>
        ///Get ReporteServiciosPorInstalar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static RepServXInstEntity GetAll(long? ContratoMaestro)
        {
            //List<RepServXInstEntity> entities = new List<RepServXInstEntity>();
            RepServXInstEntity entities;
            entities = ProviderSoftv.ReporteServiciosPorInstalar.GetReporteServiciosPorInstalar(ContratoMaestro);

            //return entities ?? new List<RepServXInstEntity>();
            return entities;
        }

        /// <summary>
        ///Get ReporteServiciosPorInstalar List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ReporteServiciosPorInstalarEntity> GetAll(List<int> lid)
        {
            List<ReporteServiciosPorInstalarEntity> entities = new List<ReporteServiciosPorInstalarEntity>();
            entities = ProviderSoftv.ReporteServiciosPorInstalar.GetReporteServiciosPorInstalar(lid);
            return entities ?? new List<ReporteServiciosPorInstalarEntity>();
        }

        /// <summary>
        ///Get ReporteServiciosPorInstalar By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ReporteServiciosPorInstalarEntity GetOne()
        {
            ReporteServiciosPorInstalarEntity result = ProviderSoftv.ReporteServiciosPorInstalar.GetReporteServiciosPorInstalarById();

            return result;
        }

        /// <summary>
        ///Get ReporteServiciosPorInstalar By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ReporteServiciosPorInstalarEntity GetOneDeep()
        {
            ReporteServiciosPorInstalarEntity result = ProviderSoftv.ReporteServiciosPorInstalar.GetReporteServiciosPorInstalarById();

            return result;
        }



        /// <summary>
        ///Get ReporteServiciosPorInstalar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ReporteServiciosPorInstalarEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ReporteServiciosPorInstalarEntity> entities = new SoftvList<ReporteServiciosPorInstalarEntity>();
            entities = ProviderSoftv.ReporteServiciosPorInstalar.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ReporteServiciosPorInstalarEntity>();
        }

        /// <summary>
        ///Get ReporteServiciosPorInstalar
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ReporteServiciosPorInstalarEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ReporteServiciosPorInstalarEntity> entities = new SoftvList<ReporteServiciosPorInstalarEntity>();
            entities = ProviderSoftv.ReporteServiciosPorInstalar.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ReporteServiciosPorInstalarEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

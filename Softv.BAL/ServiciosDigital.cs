﻿
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Data;
    using System.ComponentModel;
    using System.Linq;
    using Softv.Providers;
    using Softv.Entities;
    using Globals;

    /// <summary>
    /// Class                   : Softv.BAL.Client.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ServiciosDigitalBussines
    /// File                    : ServiciosDigitalBussines.cs
    /// Creation date           : 07/02/2017
    /// Creation time           : 06:26 p. m.
    ///</summary>
    namespace Softv.BAL
    {

    [DataObject]
    [Serializable]
    public class ServiciosDigital
    {

    #region Constructors
    public ServiciosDigital(){}
    #endregion

    /// <summary>
    ///Adds ServiciosDigital
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public static int Add(ServiciosDigitalEntity objServiciosDigital)
  {
  int result = ProviderSoftv.ServiciosDigital.AddServiciosDigital(objServiciosDigital);
    return result;
    }

    /// <summary>
    ///Delete ServiciosDigital
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public static int Delete()
    {
    int resultado = ProviderSoftv.ServiciosDigital.DeleteServiciosDigital();
    return resultado;
    }

    /// <summary>
    ///Update ServiciosDigital
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public static int Edit(ServiciosDigitalEntity objServiciosDigital)
    {
    int result = ProviderSoftv.ServiciosDigital.EditServiciosDigital(objServiciosDigital);
    return result;
    }

    /// <summary>
    ///Get ServiciosDigital
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<ServiciosDigitalEntity> GetAll()
    {
    List<ServiciosDigitalEntity> entities = new List<ServiciosDigitalEntity> ();
    entities = ProviderSoftv.ServiciosDigital.GetServiciosDigital();
    
    return entities ?? new List<ServiciosDigitalEntity>();
    }

    /// <summary>
    ///Get ServiciosDigital List<lid>
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static List<ServiciosDigitalEntity> GetAll(List<int> lid)
    {
    List<ServiciosDigitalEntity> entities = new List<ServiciosDigitalEntity> ();
    entities = ProviderSoftv.ServiciosDigital.GetServiciosDigital(lid);    
    return entities ?? new List<ServiciosDigitalEntity>();
    }

    /// <summary>
    ///Get ServiciosDigital By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static ServiciosDigitalEntity GetOne()
    {
    ServiciosDigitalEntity result = ProviderSoftv.ServiciosDigital.GetServiciosDigitalById();
    
    return result;
    }

    /// <summary>
    ///Get ServiciosDigital By Id
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static ServiciosDigitalEntity GetOneDeep()
    {
    ServiciosDigitalEntity result = ProviderSoftv.ServiciosDigital.GetServiciosDigitalById();
    
    return result;
    }
    

    
      /// <summary>
      ///Get ServiciosDigital
      ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<ServiciosDigitalEntity> GetPagedList(int pageIndex, int pageSize)
    {
    SoftvList<ServiciosDigitalEntity> entities = new SoftvList<ServiciosDigitalEntity>();
    entities = ProviderSoftv.ServiciosDigital.GetPagedList(pageIndex, pageSize);
    
    return entities ?? new SoftvList<ServiciosDigitalEntity>();
    }

    /// <summary>
    ///Get ServiciosDigital
    ///</summary>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static SoftvList<ServiciosDigitalEntity> GetPagedList(int pageIndex, int pageSize,String xml)
    {
    SoftvList<ServiciosDigitalEntity> entities = new SoftvList<ServiciosDigitalEntity>();
    entities = ProviderSoftv.ServiciosDigital.GetPagedList(pageIndex, pageSize,xml);
    
    return entities ?? new SoftvList<ServiciosDigitalEntity>();
    }


    }




    #region Customs Methods
    
    #endregion
    }
  
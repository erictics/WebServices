﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : SessionWebBussines
/// File                    : SessionWebBussines.cs
/// Creation date           : 20/10/2016
/// Creation time           : 09:24 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class SessionWeb
    {

        #region Constructors
        public SessionWeb() { }
        #endregion


        ///// <summary>
        /////Get SessionWeb
        /////</summary>
        //[DataObjectMethod(DataObjectMethodType.Select, true)]
        //public static List<SessionWebEntity> GetAll(String Token)
        //{
        //    List<SessionWebEntity> entities = new List<SessionWebEntity>();
        //    entities = ProviderSoftv.SessionWeb.GetSessionWeb(Token);

        //    return entities ?? new List<SessionWebEntity>();
        //}



        /// <summary>
        ///Get SessionWeb By Id
        ///</summary>
        //[DataObjectMethod(DataObjectMethodType.Select)]
        //public static SessionWebEntity GetOne(String Token)
        //{
        //    SessionWebEntity result = ProviderSoftv.SessionWeb.GetSessionWebById(Token);

        //    return result;
        //}

        ///// <summary>
        /////Get SessionWeb By Id
        /////</summary>
        //[DataObjectMethod(DataObjectMethodType.Select)]
        //public static SessionWebEntity GetOneDeep(String Token)
        //{
        //    SessionWebEntity result = ProviderSoftv.SessionWeb.GetSessionWebById(Token);

        //    return result;
        //}


        /// <summary>
        ///Get SessionWeb By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static DameSessionWEntity GetAuthentication(string usuario, String password)
        {
            DameSessionWEntity result = ProviderSoftv.SessionWeb.GetAuthentication(usuario, password);

            return result;
        }

      

        public static SessionWebEntity GetSucursalPorIp(string ip)
        {
            SessionWebEntity SessionWebEntity = new SessionWebEntity();
            SessionWebEntity result = ProviderSoftv.SessionWeb.GetSucursalPorIp(ip);

            return result;
        }








    }




    #region Customs Methods

    #endregion
}

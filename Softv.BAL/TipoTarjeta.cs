﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : TipoTarjetaBussines
/// File                    : TipoTarjetaBussines.cs
/// Creation date           : 26/02/2016
/// Creation time           : 04:23 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class TipoTarjeta
    {

        #region Constructors
        public TipoTarjeta() { }
        #endregion

        /// <summary>
        ///Adds TipoTarjeta
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(TipoTarjetaEntity objTipoTarjeta)
        {
            int result = ProviderSoftv.TipoTarjeta.AddTipoTarjeta(objTipoTarjeta);
            return result;
        }

        /// <summary>
        ///Delete TipoTarjeta
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete(int? IdTipoTarjeta)
        {
            int resultado = ProviderSoftv.TipoTarjeta.DeleteTipoTarjeta(IdTipoTarjeta);
            return resultado;
        }

        /// <summary>
        ///Update TipoTarjeta
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(TipoTarjetaEntity objTipoTarjeta)
        {
            int result = ProviderSoftv.TipoTarjeta.EditTipoTarjeta(objTipoTarjeta);
            return result;
        }

        /// <summary>
        ///Get TipoTarjeta
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<TipoTarjetaEntity> GetAll()
        {
            List<TipoTarjetaEntity> entities = new List<TipoTarjetaEntity>();
            entities = ProviderSoftv.TipoTarjeta.GetTipoTarjeta();

            //List<DatoBancarioEntity> lDatoBancario = ProviderSoftv.DatoBancario.GetDatoBancario(entities.Where(x => x.IdTipoTarjeta.HasValue).Select(x => x.IdTipoTarjeta.Value).ToList());
            //lDatoBancario.ForEach(XDatoBancario => entities.Where(x => x.IdTipoTarjeta.HasValue).Where(x => x.IdTipoTarjeta == XDatoBancario.IdTipoTarjeta).ToList().ForEach(y => y.DatoBancario = XDatoBancario));

            return entities ?? new List<TipoTarjetaEntity>();
        }

        /// <summary>
        ///Get TipoTarjeta List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<TipoTarjetaEntity> GetAll(List<int> lid)
        {
            List<TipoTarjetaEntity> entities = new List<TipoTarjetaEntity>();
            entities = ProviderSoftv.TipoTarjeta.GetTipoTarjeta(lid);
            return entities ?? new List<TipoTarjetaEntity>();
        }

        /// <summary>
        ///Get TipoTarjeta By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static TipoTarjetaEntity GetOne(int? IdTipoTarjeta)
        {
            TipoTarjetaEntity result = ProviderSoftv.TipoTarjeta.GetTipoTarjetaById(IdTipoTarjeta);

            if (result.IdTipoTarjeta != null)
                result.DatoBancario = ProviderSoftv.DatoBancario.GetDatoBancarioById(result.IdTipoTarjeta);

            return result;
        }

        /// <summary>
        ///Get TipoTarjeta By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static TipoTarjetaEntity GetOneDeep(int? IdTipoTarjeta)
        {
            TipoTarjetaEntity result = ProviderSoftv.TipoTarjeta.GetTipoTarjetaById(IdTipoTarjeta);

            if (result.IdTipoTarjeta != null)
                result.DatoBancario = ProviderSoftv.DatoBancario.GetDatoBancarioById(result.IdTipoTarjeta);

            return result;
        }

        public static List<TipoTarjetaEntity> GetTipoTarjetaByIdTipoTarjeta(int? IdTipoTarjeta)
        {
            List<TipoTarjetaEntity> entities = new List<TipoTarjetaEntity>();
            entities = ProviderSoftv.TipoTarjeta.GetTipoTarjetaByIdTipoTarjeta(IdTipoTarjeta);
            return entities ?? new List<TipoTarjetaEntity>();
        }



        /// <summary>
        ///Get TipoTarjeta
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<TipoTarjetaEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<TipoTarjetaEntity> entities = new SoftvList<TipoTarjetaEntity>();
            entities = ProviderSoftv.TipoTarjeta.GetPagedList(pageIndex, pageSize);

            List<DatoBancarioEntity> lDatoBancario = ProviderSoftv.DatoBancario.GetDatoBancario(entities.Where(x => x.IdTipoTarjeta.HasValue).Select(x => x.IdTipoTarjeta.Value).Distinct().ToList());
            lDatoBancario.ForEach(XDatoBancario => entities.Where(x => x.IdTipoTarjeta.HasValue).Where(x => x.IdTipoTarjeta == XDatoBancario.IdTipoTarjeta).ToList().ForEach(y => y.DatoBancario = XDatoBancario));

            return entities ?? new SoftvList<TipoTarjetaEntity>();
        }

        /// <summary>
        ///Get TipoTarjeta
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<TipoTarjetaEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<TipoTarjetaEntity> entities = new SoftvList<TipoTarjetaEntity>();
            entities = ProviderSoftv.TipoTarjeta.GetPagedList(pageIndex, pageSize, xml);

            List<DatoBancarioEntity> lDatoBancario = ProviderSoftv.DatoBancario.GetDatoBancario(entities.Where(x => x.IdTipoTarjeta.HasValue).Select(x => x.IdTipoTarjeta.Value).Distinct().ToList());
            lDatoBancario.ForEach(XDatoBancario => entities.Where(x => x.IdTipoTarjeta.HasValue).Where(x => x.IdTipoTarjeta == XDatoBancario.IdTipoTarjeta).ToList().ForEach(y => y.DatoBancario = XDatoBancario));

            return entities ?? new SoftvList<TipoTarjetaEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ValidaFacturaFiscalBussines
/// File                    : ValidaFacturaFiscalBussines.cs
/// Creation date           : 07/02/2017
/// Creation time           : 11:15 a. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ValidaFacturaFiscal
    {

        #region Constructors
        public ValidaFacturaFiscal() { }
        #endregion

        /// <summary>
        ///Adds ValidaFacturaFiscal
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ValidaFacturaFiscalEntity objValidaFacturaFiscal)
        {
            int result = ProviderSoftv.ValidaFacturaFiscal.AddValidaFacturaFiscal(objValidaFacturaFiscal);
            return result;
        }

        /// <summary>
        ///Delete ValidaFacturaFiscal
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ValidaFacturaFiscal.DeleteValidaFacturaFiscal();
            return resultado;
        }

        /// <summary>
        ///Update ValidaFacturaFiscal
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ValidaFacturaFiscalEntity objValidaFacturaFiscal)
        {
            int result = ProviderSoftv.ValidaFacturaFiscal.EditValidaFacturaFiscal(objValidaFacturaFiscal);
            return result;
        }

        /// <summary>
        ///Get ValidaFacturaFiscal
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidaFacturaFiscalEntity> GetAll()
        {
            List<ValidaFacturaFiscalEntity> entities = new List<ValidaFacturaFiscalEntity>();
            entities = ProviderSoftv.ValidaFacturaFiscal.GetValidaFacturaFiscal();

            return entities ?? new List<ValidaFacturaFiscalEntity>();
        }

        /// <summary>
        ///Get ValidaFacturaFiscal List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidaFacturaFiscalEntity> GetAll(List<int> lid)
        {
            List<ValidaFacturaFiscalEntity> entities = new List<ValidaFacturaFiscalEntity>();
            entities = ProviderSoftv.ValidaFacturaFiscal.GetValidaFacturaFiscal(lid);
            return entities ?? new List<ValidaFacturaFiscalEntity>();
        }

        /// <summary>
        ///Get ValidaFacturaFiscal By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidaFacturaFiscalEntity GetOne(long? ClvFactura)
        {
            ValidaFacturaFiscalEntity result = ProviderSoftv.ValidaFacturaFiscal.GetValidaFacturaFiscalById(ClvFactura);

            return result;
        }

        /// <summary>
        ///Get ValidaFacturaFiscal By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidaFacturaFiscalEntity GetOneDeep(long? ClvFactura)
        {
            ValidaFacturaFiscalEntity result = ProviderSoftv.ValidaFacturaFiscal.GetValidaFacturaFiscalById(ClvFactura);

            return result;
        }



        /// <summary>
        ///Get ValidaFacturaFiscal
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidaFacturaFiscalEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ValidaFacturaFiscalEntity> entities = new SoftvList<ValidaFacturaFiscalEntity>();
            entities = ProviderSoftv.ValidaFacturaFiscal.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ValidaFacturaFiscalEntity>();
        }

        /// <summary>
        ///Get ValidaFacturaFiscal
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidaFacturaFiscalEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ValidaFacturaFiscalEntity> entities = new SoftvList<ValidaFacturaFiscalEntity>();
            entities = ProviderSoftv.ValidaFacturaFiscal.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ValidaFacturaFiscalEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

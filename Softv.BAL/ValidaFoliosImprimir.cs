﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ValidaFoliosImprimirBussines
/// File                    : ValidaFoliosImprimirBussines.cs
/// Creation date           : 23/11/2017
/// Creation time           : 01:49 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ValidaFoliosImprimir
    {

        #region Constructors
        public ValidaFoliosImprimir() { }
        #endregion

        /// <summary>
        ///Adds ValidaFoliosImprimir
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ValidaFoliosImprimirEntity objValidaFoliosImprimir)
        {
            int result = ProviderSoftv.ValidaFoliosImprimir.AddValidaFoliosImprimir(objValidaFoliosImprimir);
            return result;
        }

        /// <summary>
        ///Delete ValidaFoliosImprimir
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ValidaFoliosImprimir.DeleteValidaFoliosImprimir();
            return resultado;
        }

        /// <summary>
        ///Update ValidaFoliosImprimir
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ValidaFoliosImprimirEntity objValidaFoliosImprimir)
        {
            int result = ProviderSoftv.ValidaFoliosImprimir.EditValidaFoliosImprimir(objValidaFoliosImprimir);
            return result;
        }

        /// <summary>
        ///Get ValidaFoliosImprimir
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidaFoliosImprimirEntity> GetAll()
        {
            List<ValidaFoliosImprimirEntity> entities = new List<ValidaFoliosImprimirEntity>();
            entities = ProviderSoftv.ValidaFoliosImprimir.GetValidaFoliosImprimir();

            return entities ?? new List<ValidaFoliosImprimirEntity>();
        }

        /// <summary>
        ///Get ValidaFoliosImprimir List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidaFoliosImprimirEntity> GetAll(List<int> lid)
        {
            List<ValidaFoliosImprimirEntity> entities = new List<ValidaFoliosImprimirEntity>();
            entities = ProviderSoftv.ValidaFoliosImprimir.GetValidaFoliosImprimir(lid);
            return entities ?? new List<ValidaFoliosImprimirEntity>();
        }

        /// <summary>
        ///Get ValidaFoliosImprimir By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidaFoliosImprimirEntity GetOne()
        {
            ValidaFoliosImprimirEntity result = ProviderSoftv.ValidaFoliosImprimir.GetValidaFoliosImprimirById();

            return result;
        }

        /// <summary>
        ///Get ValidaFoliosImprimir By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidaFoliosImprimirEntity GetOneDeep()
        {
            ValidaFoliosImprimirEntity result = ProviderSoftv.ValidaFoliosImprimir.GetValidaFoliosImprimirById();

            return result;
        }



        /// <summary>
        ///Get ValidaFoliosImprimir
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidaFoliosImprimirEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ValidaFoliosImprimirEntity> entities = new SoftvList<ValidaFoliosImprimirEntity>();
            entities = ProviderSoftv.ValidaFoliosImprimir.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ValidaFoliosImprimirEntity>();
        }

        /// <summary>
        ///Get ValidaFoliosImprimir
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidaFoliosImprimirEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ValidaFoliosImprimirEntity> entities = new SoftvList<ValidaFoliosImprimirEntity>();
            entities = ProviderSoftv.ValidaFoliosImprimir.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ValidaFoliosImprimirEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

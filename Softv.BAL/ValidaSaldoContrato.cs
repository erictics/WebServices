﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ValidaSaldoContratoBussines
/// File                    : ValidaSaldoContratoBussines.cs
/// Creation date           : 03/03/2017
/// Creation time           : 12:59 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ValidaSaldoContrato
    {

        #region Constructors
        public ValidaSaldoContrato() { }
        #endregion

        /// <summary>
        ///Adds ValidaSaldoContrato
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ValidaSaldoContratoEntity objValidaSaldoContrato)
        {
            int result = ProviderSoftv.ValidaSaldoContrato.AddValidaSaldoContrato(objValidaSaldoContrato);
            return result;
        }

        /// <summary>
        ///Delete ValidaSaldoContrato
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ValidaSaldoContrato.DeleteValidaSaldoContrato();
            return resultado;
        }

        /// <summary>
        ///Update ValidaSaldoContrato
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ValidaSaldoContratoEntity objValidaSaldoContrato)
        {
            int result = ProviderSoftv.ValidaSaldoContrato.EditValidaSaldoContrato(objValidaSaldoContrato);
            return result;
        }

        /// <summary>
        ///Get ValidaSaldoContrato
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidaSaldoContratoEntity> GetAll()
        {
            List<ValidaSaldoContratoEntity> entities = new List<ValidaSaldoContratoEntity>();
            entities = ProviderSoftv.ValidaSaldoContrato.GetValidaSaldoContrato();

            return entities ?? new List<ValidaSaldoContratoEntity>();
        }

        /// <summary>
        ///Get ValidaSaldoContrato List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidaSaldoContratoEntity> GetAll(List<int> lid)
        {
            List<ValidaSaldoContratoEntity> entities = new List<ValidaSaldoContratoEntity>();
            entities = ProviderSoftv.ValidaSaldoContrato.GetValidaSaldoContrato(lid);
            return entities ?? new List<ValidaSaldoContratoEntity>();
        }

        /// <summary>
        ///Get ValidaSaldoContrato By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidaSaldoContratoEntity GetOne(long? Contrato)
        {
            ValidaSaldoContratoEntity result = ProviderSoftv.ValidaSaldoContrato.GetValidaSaldoContratoById(Contrato);

            return result;
        }

        /// <summary>
        ///Get ValidaSaldoContrato By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidaSaldoContratoEntity GetOneDeep(long? Contrato)
        {
            ValidaSaldoContratoEntity result = ProviderSoftv.ValidaSaldoContrato.GetValidaSaldoContratoById(Contrato);

            return result;
        }



        /// <summary>
        ///Get ValidaSaldoContrato
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidaSaldoContratoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ValidaSaldoContratoEntity> entities = new SoftvList<ValidaSaldoContratoEntity>();
            entities = ProviderSoftv.ValidaSaldoContrato.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ValidaSaldoContratoEntity>();
        }

        /// <summary>
        ///Get ValidaSaldoContrato
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidaSaldoContratoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ValidaSaldoContratoEntity> entities = new SoftvList<ValidaSaldoContratoEntity>();
            entities = ProviderSoftv.ValidaSaldoContrato.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ValidaSaldoContratoEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

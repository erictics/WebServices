﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : ValidacionLoginCajeraBussines
/// File                    : ValidacionLoginCajeraBussines.cs
/// Creation date           : 22/12/2016
/// Creation time           : 05:55 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class ValidacionLoginCajera
    {

        #region Constructors
        public ValidacionLoginCajera() { }
        #endregion

        /// <summary>
        ///Adds ValidacionLoginCajera
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(ValidacionLoginCajeraEntity objValidacionLoginCajera)
        {
            int result = ProviderSoftv.ValidacionLoginCajera.AddValidacionLoginCajera(objValidacionLoginCajera);
            return result;
        }

        /// <summary>
        ///Delete ValidacionLoginCajera
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.ValidacionLoginCajera.DeleteValidacionLoginCajera();
            return resultado;
        }

        /// <summary>
        ///Update ValidacionLoginCajera
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(ValidacionLoginCajeraEntity objValidacionLoginCajera)
        {
            int result = ProviderSoftv.ValidacionLoginCajera.EditValidacionLoginCajera(objValidacionLoginCajera);
            return result;
        }

        /// <summary>
        ///Get ValidacionLoginCajera
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidacionLoginCajeraEntity> GetAll()
        {
            List<ValidacionLoginCajeraEntity> entities = new List<ValidacionLoginCajeraEntity>();
            entities = ProviderSoftv.ValidacionLoginCajera.GetValidacionLoginCajera();

            return entities ?? new List<ValidacionLoginCajeraEntity>();
        }

        /// <summary>
        ///Get ValidacionLoginCajera List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ValidacionLoginCajeraEntity> GetAll(List<int> lid)
        {
            List<ValidacionLoginCajeraEntity> entities = new List<ValidacionLoginCajeraEntity>();
            entities = ProviderSoftv.ValidacionLoginCajera.GetValidacionLoginCajera(lid);
            return entities ?? new List<ValidacionLoginCajeraEntity>();
        }

        /// <summary>
        ///Get ValidacionLoginCajera By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidacionLoginCajeraEntity GetOne(String ClvUsuario, String Usuario, String Pass, int? Id)
        {
            ValidacionLoginCajeraEntity result = ProviderSoftv.ValidacionLoginCajera.GetValidacionLoginCajeraById(ClvUsuario, Usuario, Pass, Id);

            return result;
        }

        /// <summary>
        ///Get ValidacionLoginCajera By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static ValidacionLoginCajeraEntity GetOneDeep(String ClvUsuario, String Usuario, String Pass, int? Id)
        {
            ValidacionLoginCajeraEntity result = ProviderSoftv.ValidacionLoginCajera.GetValidacionLoginCajeraById(ClvUsuario, Usuario, Pass, Id);

            return result;
        }



        /// <summary>
        ///Get ValidacionLoginCajera
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidacionLoginCajeraEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ValidacionLoginCajeraEntity> entities = new SoftvList<ValidacionLoginCajeraEntity>();
            entities = ProviderSoftv.ValidacionLoginCajera.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<ValidacionLoginCajeraEntity>();
        }

        /// <summary>
        ///Get ValidacionLoginCajera
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<ValidacionLoginCajeraEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ValidacionLoginCajeraEntity> entities = new SoftvList<ValidacionLoginCajeraEntity>();
            entities = ProviderSoftv.ValidacionLoginCajera.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<ValidacionLoginCajeraEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

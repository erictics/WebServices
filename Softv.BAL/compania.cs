﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : companiaBussines
/// File                    : companiaBussines.cs
/// Creation date           : 07/02/2017
/// Creation time           : 06:02 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class compania
    {

        #region Constructors
        public compania() { }
        #endregion

        /// <summary>
        ///Adds compania
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(companiaEntity objcompania)
        {
            int result = ProviderSoftv.compania.Addcompania(objcompania);
            return result;
        }

        /// <summary>
        ///Delete compania
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.compania.Deletecompania();
            return resultado;
        }

        /// <summary>
        ///Update compania
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(companiaEntity objcompania)
        {
            int result = ProviderSoftv.compania.Editcompania(objcompania);
            return result;
        }

        /// <summary>
        ///Get compania
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<companiaEntity> GetAll()
        {
            List<companiaEntity> entities = new List<companiaEntity>();
            entities = ProviderSoftv.compania.Getcompania();

            return entities ?? new List<companiaEntity>();
        }

        /// <summary>
        ///Get compania List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<companiaEntity> GetAll(List<int> lid)
        {
            List<companiaEntity> entities = new List<companiaEntity>();
            entities = ProviderSoftv.compania.Getcompania(lid);
            return entities ?? new List<companiaEntity>();
        }

        /// <summary>
        ///Get compania By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static companiaEntity GetOne()
        {
            companiaEntity result = ProviderSoftv.compania.GetcompaniaById();

            return result;
        }

        /// <summary>
        ///Get compania By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static companiaEntity GetOneDeep()
        {
            companiaEntity result = ProviderSoftv.compania.GetcompaniaById();

            return result;
        }



        /// <summary>
        ///Get compania
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<companiaEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<companiaEntity> entities = new SoftvList<companiaEntity>();
            entities = ProviderSoftv.compania.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<companiaEntity>();
        }

        /// <summary>
        ///Get compania
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<companiaEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<companiaEntity> entities = new SoftvList<companiaEntity>();
            entities = ProviderSoftv.compania.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<companiaEntity>();
        }
        //-------------------------------------------------------

        /// <summary>
        ///Get GetDistribuidorByUsuario
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<PlazaRepVariosEntity> GetDistribuidorByUsuario(int? clv_usuario)
        {
            return ProviderSoftv.compania.GetDistribuidorByUsuario(clv_usuario);
        }

  

        /// <summary>
        ///Get GetEstadosByplaza
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<EstadoEntity> GetEstadosByplaza(List<companiaEntity> plazas)
        {
            return ProviderSoftv.compania.GetEstadosByplaza(plazas);
        }

        /// <summary>
        ///Get GetCiudadesBy_PlazasEstado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<CiudadCAMDOEntity> GetCiudadesBy_PlazasEstado(List<companiaEntity> Plazas, List<EstadoEntity> Estados)
        {
            return ProviderSoftv.compania.GetCiudadesBy_PlazasEstado(Plazas, Estados);
        }

        /// <summary>
        ///Get GetLocalidadesbyCiudad
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<LocalidadCAMDOEntity> GetLocalidadesbyCiudad(int? clv_usuario, List<companiaEntity> Companias, List<CiudadCAMDOEntity> Ciudades, List<EstadoEntity> Estados) //
        {
            return ProviderSoftv.compania.GetLocalidadesbyCiudad(clv_usuario, Companias, Ciudades, Estados);
        }


        /// <summary>
        ///Get GetColoniasBy_Ciudad_Localidad    //_Estado_Ciudad_Localidad(banderaCiudad, banderaLocalidad, Estados, Ciudades, Localidades);
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ColoniaCAMDOEntity> GetColoniasBy_Ciudad_Localidad(int? clv_usuario, int? banderaLocalidad, List<companiaEntity> Companias, List<EstadoEntity> Estados, List<CiudadCAMDOEntity> Ciudades, List<LocalidadCAMDOEntity> Localidades)
        {
            return ProviderSoftv.compania.GetColoniasBy_Ciudad_Localidad(clv_usuario, banderaLocalidad,  Companias, Estados,  Ciudades,  Localidades);
        }

        /// <summary>
        ///Get GetCallesBy_Ciudad_Localidad_Colonia
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<CalleCAMDOEntity> GetCallesBy_Ciudad_Localidad_Colonia(int? clv_usuario, int? banderaLocalidad, int? banderaColonia, List<PlazaRepVariosEntity> Distribuidores,
            List<CiudadCAMDOEntity> Ciudades, List<LocalidadCAMDOEntity> Localidades, List<ColoniaCAMDOEntity> Colonias, List<companiaEntity> Companias, List<EstadoEntity> Estados)
        {
            return ProviderSoftv.compania.GetCallesBy_Ciudad_Localidad_Colonia( clv_usuario, banderaLocalidad,  banderaColonia, Distribuidores, Ciudades, Localidades,  Colonias,  Companias,  Estados);
        }


       
        

            
        /// <summary>
        ///Get GetServiciosList
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<ServiciosInternetEntity> GetServiciosList(int? clv_usuario, int? clvTipServ, List<companiaEntity> Companias, int? banderaTodoSeleccionado)
        {

            return ProviderSoftv.compania.GetServiciosList( clv_usuario,  clvTipServ, Companias, banderaTodoSeleccionado);
        }
        

      

        /// <summary>
        ///Get GetTipoClienteList
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<TiposClienteEntity> GetTipoClienteList()
        {

            return ProviderSoftv.compania.GetTipoClienteList();
        }



        /// <summary>
        ///Get GetTipServicioList
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<TipServEntity> GetTipServicioList()
        {
            return ProviderSoftv.compania.GetTipServicioList();
        }

        /// <summary>
        ///Get GetPeriodosRepVarList
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<PeriodosEntity> GetPeriodosRepVarList()
        {
            return ProviderSoftv.compania.GetPeriodosRepVarList();
        }

        /// <summary>
        ///Get GetMotCancelacionList
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<MotCanEntity> GetMotCancelacionList()
        {
            return ProviderSoftv.compania.GetMotCancelacionList();
        }



        //------------------------ Crear cada xml 



        /// <summary>
        ///Get CreateXmlBeforeReport
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<String> GetCreateXmlBeforeReport(objPrincipal objPrincipal, objParametros objParametros,
            objRangoFechas objRangoFechas, estatusCliente estatusCliente, RepVar_DistribuidorEntity[] distriArray,
            RepVar_PlazaEntity[] plazaArray, RepVar_EstadoEntity[] estadoArray, RepVar_CiudadEntity[] ciudadArray, RepVar_LocalidadEntity[] localidadArray, RepVar_ColoniaEntity[] coloniaArray,
            RepVar_ServicioEntity[] servicioArray, RepVar_TipoClienteEntity[] tipoCliArray, RepVar_PeriodoEntity[] periodoArray, RepVar_CalleEntity[] calleArray)
        {
            return ProviderSoftv.compania.GetCreateXmlBeforeReport(objPrincipal, objParametros, objRangoFechas, estatusCliente,
             distriArray, plazaArray, estadoArray, ciudadArray, localidadArray, coloniaArray, servicioArray, tipoCliArray, periodoArray, calleArray);
        }



        //------------------------------------------------------ R E P O R T E S

  

        /// <summary>
        ///Get GetReporteDig_1
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_1(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_1(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_2(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_2(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_3
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_3(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_3(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_4
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_4(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_4(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_5
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_5(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_5(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_6
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_6(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_6(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_7
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_7(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_7(reportData);
        }


        /// <summary>
        ///Get GetReporteDig_12
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_12(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_12(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_13
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_13(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_13(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_8
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_8(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_8(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_9
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_9(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_9(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_10
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_10(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_10(reportData);
        }


        /// <summary>
        ///Get GetReporteDig_11
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_11(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_11(reportData);
        }


        /// <summary>
        ///Get GetReporteDig_14
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_14(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_14(reportData);
        }

        /// <summary>
        ///Get GetReporteDig_15
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_15(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_15(reportData);
        }

              /// <summary>
        ///Get GetReporteDig_16
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteDig_16(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteDig_16(reportData);
        }




        /// <summary>
        ///Get GetReporteInt_1
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteInt_1(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteInt_1(reportData);
        }

        /// <summary>
        ///Get GetReporteInt_2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteInt_2(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteInt_2(reportData);
        }

        /// <summary>
        ///Get GetReporteInt_3
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteInt_3(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteInt_3(reportData);
        }

        /// <summary>
        ///Get GetReporteInt_5
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static String GetReporteInt_5(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteInt_5(reportData);
        }

        /// <summary>
        ///Get GetReporteInt_7
        ///</summary>
        public static String GetReporteInt_7(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteInt_7( reportData);
        }

        /// <summary>
        ///Get GetReporteInt_4
        ///</summary>
        public static String GetReporteInt_4(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteInt_4(reportData);
        }

        /// <summary>
        ///Get GetReporteInt_6
        ///</summary>
        public static String GetReporteInt_6(RepVar_datosXmlEntity reportData)
        {
            return ProviderSoftv.compania.GetReporteInt_6(reportData);
        }


        // Llenado de combos

        /// <summary>
        ///Get GetPlazasByDistribuidor
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<companiaEntity> GetPlazasByDistribuidor(int? clv_usuario, List<PlazaRepVariosEntity> distribuidores)
        {
            return ProviderSoftv.compania.GetPlazasByDistribuidor(clv_usuario, distribuidores);
        }




    }

    #region Customs Methods

    #endregion
}

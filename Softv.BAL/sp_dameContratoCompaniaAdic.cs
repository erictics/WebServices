﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : sp_dameContratoCompaniaAdicBussines
/// File                    : sp_dameContratoCompaniaAdicBussines.cs
/// Creation date           : 30/01/2017
/// Creation time           : 06:45 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class sp_dameContratoCompaniaAdic
    {

        #region Constructors
        public sp_dameContratoCompaniaAdic() { }
        #endregion

        /// <summary>
        ///Adds sp_dameContratoCompaniaAdic
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(sp_dameContratoCompaniaAdicEntity objsp_dameContratoCompaniaAdic)
        {
            int result = ProviderSoftv.sp_dameContratoCompaniaAdic.Addsp_dameContratoCompaniaAdic(objsp_dameContratoCompaniaAdic);
            return result;
        }

        /// <summary>
        ///Delete sp_dameContratoCompaniaAdic
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.sp_dameContratoCompaniaAdic.Deletesp_dameContratoCompaniaAdic();
            return resultado;
        }

        /// <summary>
        ///Update sp_dameContratoCompaniaAdic
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(sp_dameContratoCompaniaAdicEntity objsp_dameContratoCompaniaAdic)
        {
            int result = ProviderSoftv.sp_dameContratoCompaniaAdic.Editsp_dameContratoCompaniaAdic(objsp_dameContratoCompaniaAdic);
            return result;
        }

        /// <summary>
        ///Get sp_dameContratoCompaniaAdic
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_dameContratoCompaniaAdicEntity> GetAll(String ContratoCom, int? ClvUsuario, String Modulo)
        {
            List<sp_dameContratoCompaniaAdicEntity> entities = new List<sp_dameContratoCompaniaAdicEntity>();
            entities = ProviderSoftv.sp_dameContratoCompaniaAdic.Getsp_dameContratoCompaniaAdic(ContratoCom, ClvUsuario, Modulo);

            return entities ?? new List<sp_dameContratoCompaniaAdicEntity>();
        }

        /// <summary>
        ///Get sp_dameContratoCompaniaAdic List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_dameContratoCompaniaAdicEntity> GetAll(List<int> lid)
        {
            List<sp_dameContratoCompaniaAdicEntity> entities = new List<sp_dameContratoCompaniaAdicEntity>();
            entities = ProviderSoftv.sp_dameContratoCompaniaAdic.Getsp_dameContratoCompaniaAdic(lid);
            return entities ?? new List<sp_dameContratoCompaniaAdicEntity>();
        }

        /// <summary>
        ///Get sp_dameContratoCompaniaAdic By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_dameContratoCompaniaAdicEntity GetOne()
        {
            sp_dameContratoCompaniaAdicEntity result = ProviderSoftv.sp_dameContratoCompaniaAdic.Getsp_dameContratoCompaniaAdicById();

            return result;
        }

        /// <summary>
        ///Get sp_dameContratoCompaniaAdic By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_dameContratoCompaniaAdicEntity GetOneDeep()
        {
            sp_dameContratoCompaniaAdicEntity result = ProviderSoftv.sp_dameContratoCompaniaAdic.Getsp_dameContratoCompaniaAdicById();

            return result;
        }



        /// <summary>
        ///Get sp_dameContratoCompaniaAdic
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_dameContratoCompaniaAdicEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<sp_dameContratoCompaniaAdicEntity> entities = new SoftvList<sp_dameContratoCompaniaAdicEntity>();
            entities = ProviderSoftv.sp_dameContratoCompaniaAdic.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<sp_dameContratoCompaniaAdicEntity>();
        }

        /// <summary>
        ///Get sp_dameContratoCompaniaAdic
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_dameContratoCompaniaAdicEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<sp_dameContratoCompaniaAdicEntity> entities = new SoftvList<sp_dameContratoCompaniaAdicEntity>();
            entities = ProviderSoftv.sp_dameContratoCompaniaAdic.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<sp_dameContratoCompaniaAdicEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : sp_dameInfodelCobroBussines
/// File                    : sp_dameInfodelCobroBussines.cs
/// Creation date           : 02/03/2017
/// Creation time           : 06:36 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class sp_dameInfodelCobro
    {

        #region Constructors
        public sp_dameInfodelCobro() { }
        #endregion

        /// <summary>
        ///Adds sp_dameInfodelCobro
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(sp_dameInfodelCobroEntity objsp_dameInfodelCobro)
        {
            int result = ProviderSoftv.sp_dameInfodelCobro.Addsp_dameInfodelCobro(objsp_dameInfodelCobro);
            return result;
        }

        /// <summary>
        ///Delete sp_dameInfodelCobro
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.sp_dameInfodelCobro.Deletesp_dameInfodelCobro();
            return resultado;
        }

        /// <summary>
        ///Update sp_dameInfodelCobro
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(sp_dameInfodelCobroEntity objsp_dameInfodelCobro)
        {
            int result = ProviderSoftv.sp_dameInfodelCobro.Editsp_dameInfodelCobro(objsp_dameInfodelCobro);
            return result;
        }

        /// <summary>
        ///Get sp_dameInfodelCobro
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_dameInfodelCobroEntity> GetAll()
        {
            List<sp_dameInfodelCobroEntity> entities = new List<sp_dameInfodelCobroEntity>();
            entities = ProviderSoftv.sp_dameInfodelCobro.Getsp_dameInfodelCobro();

            return entities ?? new List<sp_dameInfodelCobroEntity>();
        }

        /// <summary>
        ///Get sp_dameInfodelCobro List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_dameInfodelCobroEntity> GetAll(List<int> lid)
        {
            List<sp_dameInfodelCobroEntity> entities = new List<sp_dameInfodelCobroEntity>();
            entities = ProviderSoftv.sp_dameInfodelCobro.Getsp_dameInfodelCobro(lid);
            return entities ?? new List<sp_dameInfodelCobroEntity>();
        }

        /// <summary>
        ///Get sp_dameInfodelCobro By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_dameInfodelCobroEntity GetOne(long? ClvSession, long? ClvDetalle)
        {
            sp_dameInfodelCobroEntity result = ProviderSoftv.sp_dameInfodelCobro.Getsp_dameInfodelCobroById(ClvSession, ClvDetalle);

            return result;
        }

        /// <summary>
        ///Get sp_dameInfodelCobro By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_dameInfodelCobroEntity GetOneDeep(long? ClvSession, long? ClvDetalle)
        {
            sp_dameInfodelCobroEntity result = ProviderSoftv.sp_dameInfodelCobro.Getsp_dameInfodelCobroById(ClvSession, ClvDetalle);

            return result;
        }



        /// <summary>
        ///Get sp_dameInfodelCobro
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_dameInfodelCobroEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<sp_dameInfodelCobroEntity> entities = new SoftvList<sp_dameInfodelCobroEntity>();
            entities = ProviderSoftv.sp_dameInfodelCobro.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<sp_dameInfodelCobroEntity>();
        }

        /// <summary>
        ///Get sp_dameInfodelCobro
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_dameInfodelCobroEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<sp_dameInfodelCobroEntity> entities = new SoftvList<sp_dameInfodelCobroEntity>();
            entities = ProviderSoftv.sp_dameInfodelCobro.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<sp_dameInfodelCobroEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : sp_validaEliminarOrdenBussines
/// File                    : sp_validaEliminarOrdenBussines.cs
/// Creation date           : 19/04/2017
/// Creation time           : 01:22 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class sp_validaEliminarOrden
    {

        #region Constructors
        public sp_validaEliminarOrden() { }
        #endregion

        /// <summary>
        ///Adds sp_validaEliminarOrden
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(sp_validaEliminarOrdenEntity objsp_validaEliminarOrden)
        {
            int result = ProviderSoftv.sp_validaEliminarOrden.Addsp_validaEliminarOrden(objsp_validaEliminarOrden);
            return result;
        }

        /// <summary>
        ///Delete sp_validaEliminarOrden
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.sp_validaEliminarOrden.Deletesp_validaEliminarOrden();
            return resultado;
        }

        /// <summary>
        ///Update sp_validaEliminarOrden
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(sp_validaEliminarOrdenEntity objsp_validaEliminarOrden)
        {
            int result = ProviderSoftv.sp_validaEliminarOrden.Editsp_validaEliminarOrden(objsp_validaEliminarOrden);
            return result;
        }

        /// <summary>
        ///Get sp_validaEliminarOrden
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_validaEliminarOrdenEntity> GetAll()
        {
            List<sp_validaEliminarOrdenEntity> entities = new List<sp_validaEliminarOrdenEntity>();
            entities = ProviderSoftv.sp_validaEliminarOrden.Getsp_validaEliminarOrden();

            return entities ?? new List<sp_validaEliminarOrdenEntity>();
        }

        /// <summary>
        ///Get sp_validaEliminarOrden List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_validaEliminarOrdenEntity> GetAll(List<int> lid)
        {
            List<sp_validaEliminarOrdenEntity> entities = new List<sp_validaEliminarOrdenEntity>();
            entities = ProviderSoftv.sp_validaEliminarOrden.Getsp_validaEliminarOrden(lid);
            return entities ?? new List<sp_validaEliminarOrdenEntity>();
        }

        /// <summary>
        ///Get sp_validaEliminarOrden By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_validaEliminarOrdenEntity GetOne(String ClvUsuario)
        {
            sp_validaEliminarOrdenEntity result = ProviderSoftv.sp_validaEliminarOrden.Getsp_validaEliminarOrdenById(ClvUsuario);

            return result;
        }

        /// <summary>
        ///Get sp_validaEliminarOrden By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_validaEliminarOrdenEntity GetOneDeep(String ClvUsuario)
        {
            sp_validaEliminarOrdenEntity result = ProviderSoftv.sp_validaEliminarOrden.Getsp_validaEliminarOrdenById(ClvUsuario);

            return result;
        }



        /// <summary>
        ///Get sp_validaEliminarOrden
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_validaEliminarOrdenEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<sp_validaEliminarOrdenEntity> entities = new SoftvList<sp_validaEliminarOrdenEntity>();
            entities = ProviderSoftv.sp_validaEliminarOrden.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<sp_validaEliminarOrdenEntity>();
        }

        /// <summary>
        ///Get sp_validaEliminarOrden
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_validaEliminarOrdenEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<sp_validaEliminarOrdenEntity> entities = new SoftvList<sp_validaEliminarOrdenEntity>();
            entities = ProviderSoftv.sp_validaEliminarOrden.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<sp_validaEliminarOrdenEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : sp_verificaBimProveedorBussines
/// File                    : sp_verificaBimProveedorBussines.cs
/// Creation date           : 24/07/2017
/// Creation time           : 05:58 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class sp_verificaBimProveedor
    {

        #region Constructors
        public sp_verificaBimProveedor() { }
        #endregion

        /// <summary>
        ///Adds sp_verificaBimProveedor
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(sp_verificaBimProveedorEntity objsp_verificaBimProveedor)
        {
            int result = ProviderSoftv.sp_verificaBimProveedor.Addsp_verificaBimProveedor(objsp_verificaBimProveedor);
            return result;
        }

        /// <summary>
        ///Delete sp_verificaBimProveedor
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.sp_verificaBimProveedor.Deletesp_verificaBimProveedor();
            return resultado;
        }

        /// <summary>
        ///Update sp_verificaBimProveedor
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(sp_verificaBimProveedorEntity objsp_verificaBimProveedor)
        {
            int result = ProviderSoftv.sp_verificaBimProveedor.Editsp_verificaBimProveedor(objsp_verificaBimProveedor);
            return result;
        }

        /// <summary>
        ///Get sp_verificaBimProveedor
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_verificaBimProveedorEntity> GetAll()
        {
            List<sp_verificaBimProveedorEntity> entities = new List<sp_verificaBimProveedorEntity>();
            entities = ProviderSoftv.sp_verificaBimProveedor.Getsp_verificaBimProveedor();

            return entities ?? new List<sp_verificaBimProveedorEntity>();
        }

        /// <summary>
        ///Get sp_verificaBimProveedor List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<sp_verificaBimProveedorEntity> GetAll(List<int> lid)
        {
            List<sp_verificaBimProveedorEntity> entities = new List<sp_verificaBimProveedorEntity>();
            entities = ProviderSoftv.sp_verificaBimProveedor.Getsp_verificaBimProveedor(lid);
            return entities ?? new List<sp_verificaBimProveedorEntity>();
        }

        /// <summary>
        ///Get sp_verificaBimProveedor By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_verificaBimProveedorEntity GetOne(long? Clv_orden)
        {
            sp_verificaBimProveedorEntity result = ProviderSoftv.sp_verificaBimProveedor.Getsp_verificaBimProveedorById(Clv_orden);

            return result;
        }

        /// <summary>
        ///Get sp_verificaBimProveedor By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static sp_verificaBimProveedorEntity GetOneDeep(long? Clv_orden)
        {
            sp_verificaBimProveedorEntity result = ProviderSoftv.sp_verificaBimProveedor.Getsp_verificaBimProveedorById(Clv_orden);

            return result;
        }



        /// <summary>
        ///Get sp_verificaBimProveedor
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_verificaBimProveedorEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<sp_verificaBimProveedorEntity> entities = new SoftvList<sp_verificaBimProveedorEntity>();
            entities = ProviderSoftv.sp_verificaBimProveedor.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<sp_verificaBimProveedorEntity>();
        }

        /// <summary>
        ///Get sp_verificaBimProveedor
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<sp_verificaBimProveedorEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<sp_verificaBimProveedorEntity> entities = new SoftvList<sp_verificaBimProveedorEntity>();
            entities = ProviderSoftv.sp_verificaBimProveedor.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<sp_verificaBimProveedorEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

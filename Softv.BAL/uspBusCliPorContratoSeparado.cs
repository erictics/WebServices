﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : uspBusCliPorContratoSeparadoBussines
/// File                    : uspBusCliPorContratoSeparadoBussines.cs
/// Creation date           : 13/10/2016
/// Creation time           : 06:57 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class uspBusCliPorContratoSeparado
    {

        #region Constructors
        public uspBusCliPorContratoSeparado() { }
        #endregion

        /// <summary>
        ///Adds uspBusCliPorContratoSeparado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(uspBusCliPorContratoSeparadoEntity objuspBusCliPorContratoSeparado)
        {
            int result = ProviderSoftv.uspBusCliPorContratoSeparado.AdduspBusCliPorContratoSeparado(objuspBusCliPorContratoSeparado);
            return result;
        }

        /// <summary>
        ///Delete uspBusCliPorContratoSeparado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.uspBusCliPorContratoSeparado.DeleteuspBusCliPorContratoSeparado();
            return resultado;
        }

        /// <summary>
        ///Update uspBusCliPorContratoSeparado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(uspBusCliPorContratoSeparadoEntity objuspBusCliPorContratoSeparado)
        {
            int result = ProviderSoftv.uspBusCliPorContratoSeparado.EdituspBusCliPorContratoSeparado(objuspBusCliPorContratoSeparado);
            return result;
        }

        /// <summary>
        ///Get uspBusCliPorContratoSeparado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<uspBusCliPorContratoSeparadoEntity> GetAll(String ContratoC, String Nombre, String Apellido_Paterno, String Apellido_Materno, String Calle, String Numero, int? Op, int? ClvColonia, long? Usuario, string COLONIA)
        {
            List<uspBusCliPorContratoSeparadoEntity> entities = new List<uspBusCliPorContratoSeparadoEntity>();
            entities = ProviderSoftv.uspBusCliPorContratoSeparado.GetuspBusCliPorContratoSeparado(ContratoC, Nombre, Apellido_Paterno, Apellido_Materno, Calle, Numero, Op, ClvColonia, Usuario, COLONIA);

            return entities ?? new List<uspBusCliPorContratoSeparadoEntity>();
        }

        /// <summary>
        ///Get uspBusCliPorContratoSeparado List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<uspBusCliPorContratoSeparadoEntity> GetAll(List<int> lid)
        {
            List<uspBusCliPorContratoSeparadoEntity> entities = new List<uspBusCliPorContratoSeparadoEntity>();
            entities = ProviderSoftv.uspBusCliPorContratoSeparado.GetuspBusCliPorContratoSeparado(lid);
            return entities ?? new List<uspBusCliPorContratoSeparadoEntity>();
        }

        /// <summary>
        ///Get uspBusCliPorContratoSeparado By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static uspBusCliPorContratoSeparadoEntity GetOne()
        {
            uspBusCliPorContratoSeparadoEntity result = ProviderSoftv.uspBusCliPorContratoSeparado.GetuspBusCliPorContratoSeparadoById();

            return result;
        }

        /// <summary>
        ///Get uspBusCliPorContratoSeparado By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static uspBusCliPorContratoSeparadoEntity GetOneDeep()
        {
            uspBusCliPorContratoSeparadoEntity result = ProviderSoftv.uspBusCliPorContratoSeparado.GetuspBusCliPorContratoSeparadoById();

            return result;
        }



        /// <summary>
        ///Get uspBusCliPorContratoSeparado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<uspBusCliPorContratoSeparadoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<uspBusCliPorContratoSeparadoEntity> entities = new SoftvList<uspBusCliPorContratoSeparadoEntity>();
            entities = ProviderSoftv.uspBusCliPorContratoSeparado.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<uspBusCliPorContratoSeparadoEntity>();
        }

        /// <summary>
        ///Get uspBusCliPorContratoSeparado
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<uspBusCliPorContratoSeparadoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<uspBusCliPorContratoSeparadoEntity> entities = new SoftvList<uspBusCliPorContratoSeparadoEntity>();
            entities = ProviderSoftv.uspBusCliPorContratoSeparado.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<uspBusCliPorContratoSeparadoEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

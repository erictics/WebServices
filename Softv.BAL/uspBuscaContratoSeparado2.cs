﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : uspBuscaContratoSeparado2Bussines
/// File                    : uspBuscaContratoSeparado2Bussines.cs
/// Creation date           : 24/01/2017
/// Creation time           : 12:15 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class uspBuscaContratoSeparado2
    {

        #region Constructors
        public uspBuscaContratoSeparado2() { }
        #endregion

        /// <summary>
        ///Adds uspBuscaContratoSeparado2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(uspBuscaContratoSeparado2Entity objuspBuscaContratoSeparado2)
        {
            int result = ProviderSoftv.uspBuscaContratoSeparado2.AdduspBuscaContratoSeparado2(objuspBuscaContratoSeparado2);
            return result;
        }

        /// <summary>
        ///Delete uspBuscaContratoSeparado2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.uspBuscaContratoSeparado2.DeleteuspBuscaContratoSeparado2();
            return resultado;
        }

        /// <summary>
        ///Update uspBuscaContratoSeparado2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(uspBuscaContratoSeparado2Entity objuspBuscaContratoSeparado2)
        {
            int result = ProviderSoftv.uspBuscaContratoSeparado2.EdituspBuscaContratoSeparado2(objuspBuscaContratoSeparado2);
            return result;
        }

        /// <summary>
        ///Get uspBuscaContratoSeparado2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<uspBuscaContratoSeparado2Entity> GetAll(String ContratoCom, String Nombre, String Apellido_Paterno, String Apellido_Materno, String CALLE, String NUMERO, int? ClvColonia, String SetupBox, int? IdUsuario, int? TipoSer, int? Op)
        {
            List<uspBuscaContratoSeparado2Entity> entities = new List<uspBuscaContratoSeparado2Entity>();
            entities = ProviderSoftv.uspBuscaContratoSeparado2.GetuspBuscaContratoSeparado2(ContratoCom, Nombre, Apellido_Paterno, Apellido_Materno, CALLE, NUMERO, ClvColonia, SetupBox, IdUsuario, TipoSer, Op);

            return entities ?? new List<uspBuscaContratoSeparado2Entity>();
        }



        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<uspBuscaContratoSeparado2Entity> GetByIdDis(String ContratoCom, String Nombre, String Apellido_Paterno, String Apellido_Materno, String CALLE, String NUMERO, int? ClvColonia, String SetupBox, int? IdUsuario, int? TipoSer, int? Op, int? IdDistribuidor)
        {
            List<uspBuscaContratoSeparado2Entity> entities = new List<uspBuscaContratoSeparado2Entity>();
            entities = ProviderSoftv.uspBuscaContratoSeparado2.GetByIdDis(ContratoCom, Nombre, Apellido_Paterno, Apellido_Materno, CALLE, NUMERO, ClvColonia, SetupBox, IdUsuario, TipoSer, Op, IdDistribuidor);

            return entities ?? new List<uspBuscaContratoSeparado2Entity>();
        }















        /// <summary>
        ///Get uspBuscaContratoSeparado2 List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<uspBuscaContratoSeparado2Entity> GetAll(List<int> lid)
        {
            List<uspBuscaContratoSeparado2Entity> entities = new List<uspBuscaContratoSeparado2Entity>();
            entities = ProviderSoftv.uspBuscaContratoSeparado2.GetuspBuscaContratoSeparado2(lid);
            return entities ?? new List<uspBuscaContratoSeparado2Entity>();
        }

        /// <summary>
        ///Get uspBuscaContratoSeparado2 By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static uspBuscaContratoSeparado2Entity GetOne()
        {
            uspBuscaContratoSeparado2Entity result = ProviderSoftv.uspBuscaContratoSeparado2.GetuspBuscaContratoSeparado2ById();

            return result;
        }

        /// <summary>
        ///Get uspBuscaContratoSeparado2 By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static uspBuscaContratoSeparado2Entity GetOneDeep()
        {
            uspBuscaContratoSeparado2Entity result = ProviderSoftv.uspBuscaContratoSeparado2.GetuspBuscaContratoSeparado2ById();

            return result;
        }



        /// <summary>
        ///Get uspBuscaContratoSeparado2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<uspBuscaContratoSeparado2Entity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<uspBuscaContratoSeparado2Entity> entities = new SoftvList<uspBuscaContratoSeparado2Entity>();
            entities = ProviderSoftv.uspBuscaContratoSeparado2.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<uspBuscaContratoSeparado2Entity>();
        }

        /// <summary>
        ///Get uspBuscaContratoSeparado2
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<uspBuscaContratoSeparado2Entity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<uspBuscaContratoSeparado2Entity> entities = new SoftvList<uspBuscaContratoSeparado2Entity>();
            entities = ProviderSoftv.uspBuscaContratoSeparado2.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<uspBuscaContratoSeparado2Entity>();
        }


    }




    #region Customs Methods

    #endregion
}

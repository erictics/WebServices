﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Linq;
using Softv.Providers;
using Softv.Entities;
using Globals;

/// <summary>
/// Class                   : Softv.BAL.Client.cs
/// Generated by            : Class Generator (c) 2014
/// Description             : uspConsultaTblClasificacionProblemasBussines
/// File                    : uspConsultaTblClasificacionProblemasBussines.cs
/// Creation date           : 24/01/2017
/// Creation time           : 12:47 p. m.
///</summary>
namespace Softv.BAL
{

    [DataObject]
    [Serializable]
    public class uspConsultaTblClasificacionProblemas
    {

        #region Constructors
        public uspConsultaTblClasificacionProblemas() { }
        #endregion

        /// <summary>
        ///Adds uspConsultaTblClasificacionProblemas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public static int Add(uspConsultaTblClasificacionProblemasEntity objuspConsultaTblClasificacionProblemas)
        {
            int result = ProviderSoftv.uspConsultaTblClasificacionProblemas.AdduspConsultaTblClasificacionProblemas(objuspConsultaTblClasificacionProblemas);
            return result;
        }

        /// <summary>
        ///Delete uspConsultaTblClasificacionProblemas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public static int Delete()
        {
            int resultado = ProviderSoftv.uspConsultaTblClasificacionProblemas.DeleteuspConsultaTblClasificacionProblemas();
            return resultado;
        }

        /// <summary>
        ///Update uspConsultaTblClasificacionProblemas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public static int Edit(uspConsultaTblClasificacionProblemasEntity objuspConsultaTblClasificacionProblemas)
        {
            int result = ProviderSoftv.uspConsultaTblClasificacionProblemas.EdituspConsultaTblClasificacionProblemas(objuspConsultaTblClasificacionProblemas);
            return result;
        }

        /// <summary>
        ///Get uspConsultaTblClasificacionProblemas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<uspConsultaTblClasificacionProblemasEntity> GetAll()
        {
            List<uspConsultaTblClasificacionProblemasEntity> entities = new List<uspConsultaTblClasificacionProblemasEntity>();
            entities = ProviderSoftv.uspConsultaTblClasificacionProblemas.GetuspConsultaTblClasificacionProblemas();

            return entities ?? new List<uspConsultaTblClasificacionProblemasEntity>();
        }

        /// <summary>
        ///Get uspConsultaTblClasificacionProblemas List<lid>
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<uspConsultaTblClasificacionProblemasEntity> GetAll(List<int> lid)
        {
            List<uspConsultaTblClasificacionProblemasEntity> entities = new List<uspConsultaTblClasificacionProblemasEntity>();
            entities = ProviderSoftv.uspConsultaTblClasificacionProblemas.GetuspConsultaTblClasificacionProblemas(lid);
            return entities ?? new List<uspConsultaTblClasificacionProblemasEntity>();
        }

        /// <summary>
        ///Get uspConsultaTblClasificacionProblemas By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static uspConsultaTblClasificacionProblemasEntity GetOne()
        {
            uspConsultaTblClasificacionProblemasEntity result = ProviderSoftv.uspConsultaTblClasificacionProblemas.GetuspConsultaTblClasificacionProblemasById();

            return result;
        }

        /// <summary>
        ///Get uspConsultaTblClasificacionProblemas By Id
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static uspConsultaTblClasificacionProblemasEntity GetOneDeep()
        {
            uspConsultaTblClasificacionProblemasEntity result = ProviderSoftv.uspConsultaTblClasificacionProblemas.GetuspConsultaTblClasificacionProblemasById();

            return result;
        }



        /// <summary>
        ///Get uspConsultaTblClasificacionProblemas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<uspConsultaTblClasificacionProblemasEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<uspConsultaTblClasificacionProblemasEntity> entities = new SoftvList<uspConsultaTblClasificacionProblemasEntity>();
            entities = ProviderSoftv.uspConsultaTblClasificacionProblemas.GetPagedList(pageIndex, pageSize);

            return entities ?? new SoftvList<uspConsultaTblClasificacionProblemasEntity>();
        }

        /// <summary>
        ///Get uspConsultaTblClasificacionProblemas
        ///</summary>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static SoftvList<uspConsultaTblClasificacionProblemasEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<uspConsultaTblClasificacionProblemasEntity> entities = new SoftvList<uspConsultaTblClasificacionProblemasEntity>();
            entities = ProviderSoftv.uspConsultaTblClasificacionProblemas.GetPagedList(pageIndex, pageSize, xml);

            return entities ?? new SoftvList<uspConsultaTblClasificacionProblemasEntity>();
        }


    }




    #region Customs Methods

    #endregion
}

﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ActualizaFacturaGeneraFiscalElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ActualizaFacturaGeneraFiscal class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ActualizaFacturaGeneraFiscal
        ///</summary>
        [ConfigurationProperty("DataClassActualizaFacturaGeneraFiscal", DefaultValue = "Softv.DAO.ActualizaFacturaGeneraFiscalData")]
        public String DataClass
        {
          get { return (string)base["DataClassActualizaFacturaGeneraFiscal"]; }
        }

        /// <summary>
        /// Gets connection string for database ActualizaFacturaGeneraFiscal access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
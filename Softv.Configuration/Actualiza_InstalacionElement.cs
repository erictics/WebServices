﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Actualiza_InstalacionElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Actualiza_Instalacion class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Actualiza_Instalacion
        ///</summary>
        [ConfigurationProperty("DataClassActualiza_Instalacion", DefaultValue = "Softv.DAO.Actualiza_InstalacionData")]
        public String DataClass
        {
          get { return (string)base["DataClassActualiza_Instalacion"]; }
        }

        /// <summary>
        /// Gets connection string for database Actualiza_Instalacion access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
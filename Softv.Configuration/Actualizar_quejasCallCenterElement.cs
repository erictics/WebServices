﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Actualizar_quejasCallCenterElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Actualizar_quejasCallCenter class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Actualizar_quejasCallCenter
        ///</summary>
        [ConfigurationProperty("DataClassActualizar_quejasCallCenter", DefaultValue = "Softv.DAO.Actualizar_quejasCallCenterData")]
        public String DataClass
        {
          get { return (string)base["DataClassActualizar_quejasCallCenter"]; }
        }

        /// <summary>
        /// Gets connection string for database Actualizar_quejasCallCenter access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
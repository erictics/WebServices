﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class AdelantarElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Adelantar class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Adelantar
        ///</summary>
        [ConfigurationProperty("DataClassAdelantar", DefaultValue = "Softv.DAO.AdelantarData")]
        public String DataClass
        {
          get { return (string)base["DataClassAdelantar"]; }
        }

        /// <summary>
        /// Gets connection string for database Adelantar access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
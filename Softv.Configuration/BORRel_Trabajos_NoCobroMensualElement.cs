﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BORRel_Trabajos_NoCobroMensualElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BORRel_Trabajos_NoCobroMensual class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BORRel_Trabajos_NoCobroMensual
        ///</summary>
        [ConfigurationProperty("DataClassBORRel_Trabajos_NoCobroMensual", DefaultValue = "Softv.DAO.BORRel_Trabajos_NoCobroMensualData")]
        public String DataClass
        {
          get { return (string)base["DataClassBORRel_Trabajos_NoCobroMensual"]; }
        }

        /// <summary>
        /// Gets connection string for database BORRel_Trabajos_NoCobroMensual access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BUSCAFACTURASElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BUSCAFACTURAS class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BUSCAFACTURAS
        ///</summary>
        [ConfigurationProperty("DataClassBUSCAFACTURAS", DefaultValue = "Softv.DAO.BUSCAFACTURASData")]
        public String DataClass
        {
          get { return (string)base["DataClassBUSCAFACTURAS"]; }
        }

        /// <summary>
        /// Gets connection string for database BUSCAFACTURAS access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
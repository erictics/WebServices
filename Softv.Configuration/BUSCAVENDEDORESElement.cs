﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BUSCAVENDEDORESElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BUSCAVENDEDORES class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BUSCAVENDEDORES
        ///</summary>
        [ConfigurationProperty("DataClassBUSCAVENDEDORES", DefaultValue = "Softv.DAO.BUSCAVENDEDORESData")]
        public String DataClass
        {
          get { return (string)base["DataClassBUSCAVENDEDORES"]; }
        }

        /// <summary>
        /// Gets connection string for database BUSCAVENDEDORES access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BUSCLIPORCONTRATO_OrdSerElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BUSCLIPORCONTRATO_OrdSer class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BUSCLIPORCONTRATO_OrdSer
        ///</summary>
        [ConfigurationProperty("DataClassBUSCLIPORCONTRATO_OrdSer", DefaultValue = "Softv.DAO.BUSCLIPORCONTRATO_OrdSerData")]
        public String DataClass
        {
          get { return (string)base["DataClassBUSCLIPORCONTRATO_OrdSer"]; }
        }

        /// <summary>
        /// Gets connection string for database BUSCLIPORCONTRATO_OrdSer access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
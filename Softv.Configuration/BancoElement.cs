﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BancoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Banco class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Banco
        ///</summary>
        [ConfigurationProperty("DataClassBanco", DefaultValue = "Softv.DAO.BancoData")]
        public String DataClass
        {
          get { return (string)base["DataClassBanco"]; }
        }

        /// <summary>
        /// Gets connection string for database Banco access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
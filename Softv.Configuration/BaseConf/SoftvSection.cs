﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftvConfiguration
{
    public class SoftvSection : ConfigurationSection
    {
        /// <summary>
        /// Gets default connection String. If it doesn't exist then
        /// returns global connection string
        /// </summary>
        [ConfigurationProperty("DefaultConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["DefaultConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ?
                    Globals.DataAccess.GlobalConectionString :
                    (string)base["DefaultConnectionString"];
                return connectionString;
            }
        }

        /// <summary>
        /// Gets default assembly name for TestSimulator data clases
        /// </summary>
        [ConfigurationProperty("DefaultAssembly", DefaultValue = "Softv.SQL")]
        public String Assembly
        {
            get { return (string)base["DefaultAssembly"]; }
        }
        /// <summary>
        /// Gets Usuario configuration data
        /// </summary>
        [ConfigurationProperty("Usuario")]
        public UsuarioElement Usuario
        {
            get { return (UsuarioElement)base["Usuario"]; }
        }
        /// <summary>
        /// Gets Role configuration data
        /// </summary>
        [ConfigurationProperty("Role")]
        public RoleElement Role
        {
            get { return (RoleElement)base["Role"]; }
        }

        /// <summary>
        /// Gets Permiso configuration data
        /// </summary>
        [ConfigurationProperty("Permiso")]
        public PermisoElement Permiso
        {
            get { return (PermisoElement)base["Permiso"]; }
        }
        /// <summary>
        /// Gets Module configuration data
        /// </summary>
        [ConfigurationProperty("Module")]
        public ModuleElement Module
        {
            get { return (ModuleElement)base["Module"]; }
        }

        /// <summary>
        /// Gets Secutity configuration data
        /// </summary>
        [ConfigurationProperty("Secutity")]
        public SecutityElement Secutity
        {
            get { return (SecutityElement)base["Secutity"]; }
        }

        /// <summary>
        /// Gets Session configuration data
        /// </summary>
        [ConfigurationProperty("Session")]
        public SessionElement Session
        {
            get { return (SessionElement)base["Session"]; }
        }







        /// <summary>
        /// Gets DameClv_Session configuration data
        /// </summary>
        [ConfigurationProperty("DameClv_Session")]
        public DameClv_SessionElement DameClv_Session
        {
            get { return (DameClv_SessionElement)base["DameClv_Session"]; }
        }

        /// <summary>
        /// Gets BusCliPorContrato_Fac configuration data
        /// </summary>
        [ConfigurationProperty("BusCliPorContrato_Fac")]
        public BusCliPorContrato_FacElement BusCliPorContrato_Fac
        {
            get { return (BusCliPorContrato_FacElement)base["BusCliPorContrato_Fac"]; }
        }

        /// <summary>
        /// Gets uspBusCliPorContratoSeparado configuration data
        /// </summary>
        [ConfigurationProperty("uspBusCliPorContratoSeparado")]
        public uspBusCliPorContratoSeparadoElement uspBusCliPorContratoSeparado
        {
            get { return (uspBusCliPorContratoSeparadoElement)base["uspBusCliPorContratoSeparado"]; }
        }

        /// <summary>
        /// Gets DameDatosUsuario configuration data
        /// </summary>
        [ConfigurationProperty("DameDatosUsuario")]
        public DameDatosUsuarioElement DameDatosUsuario
        {
            get { return (DameDatosUsuarioElement)base["DameDatosUsuario"]; }
        }


        /// <summary>
        /// Gets DameNombreSucursal configuration data
        /// </summary>
        [ConfigurationProperty("DameNombreSucursal")]
        public DameNombreSucursalElement DameNombreSucursal
        {
            get { return (DameNombreSucursalElement)base["DameNombreSucursal"]; }
        }

        /// <summary>
        /// Gets DameDistribuidorUsuario configuration data
        /// </summary>
        [ConfigurationProperty("DameDistribuidorUsuario")]
        public DameDistribuidorUsuarioElement DameDistribuidorUsuario
        {
            get { return (DameDistribuidorUsuarioElement)base["DameDistribuidorUsuario"]; }
        }

        /// <summary>
        /// Gets DameSerDelCliFac configuration data
        /// </summary>
        [ConfigurationProperty("DameSerDelCliFac")]
        public DameSerDelCliFacElement DameSerDelCliFac
        {
            get { return (DameSerDelCliFacElement)base["DameSerDelCliFac"]; }
        }

        /// <summary>
        /// Gets DameTiposClientes configuration data
        /// </summary>
        [ConfigurationProperty("DameTiposClientes")]
        public DameTiposClientesElement DameTiposClientes
        {
            get { return (DameTiposClientesElement)base["DameTiposClientes"]; }
        }

        /// <summary>
        /// Gets DameDetalle configuration data
        /// </summary>
        [ConfigurationProperty("DameDetalle")]
        public DameDetalleElement DameDetalle
        {
            get { return (DameDetalleElement)base["DameDetalle"]; }
        }

        /// <summary>
        /// Gets SumaDetalle configuration data
        /// </summary>
        [ConfigurationProperty("SumaDetalle")]
        public SumaDetalleElement SumaDetalle
        {
            get { return (SumaDetalleElement)base["SumaDetalle"]; }
        }





        /// <summary>
        /// Gets DameDatosUp configuration data
        /// </summary>
        [ConfigurationProperty("DameDatosUp")]
        public DameDatosUpElement DameDatosUp
        {
            get { return (DameDatosUpElement)base["DameDatosUp"]; }
        }

        /// <summary>
        /// Gets SessionWeb configuration data
        /// </summary>
        [ConfigurationProperty("SessionWeb")]
        public SessionWebElement SessionWeb
        {
            get { return (SessionWebElement)base["SessionWeb"]; }
        }


        /// <summary>
        /// Gets DameSessionWconfiguration data
        /// </summary>
        [ConfigurationProperty("DameSessionW")]
        public DameSessionWElement DameSessionW
        {
            get { return (DameSessionWElement)base["DameSessionW"]; }
        }

        /// <summary>
        /// Gets MuestraTipoFacturaHistorial configuration data
        /// </summary>
        [ConfigurationProperty("MuestraTipoFacturaHistorial")]
        public MuestraTipoFacturaHistorialElement MuestraTipoFacturaHistorial
        {
            get { return (MuestraTipoFacturaHistorialElement)base["MuestraTipoFacturaHistorial"]; }
        }


        /// <summary>
        /// Gets BuscaFacturasHistorial configuration data
        /// </summary>
        [ConfigurationProperty("BuscaFacturasHistorial")]
        public BuscaFacturasHistorialElement BuscaFacturasHistorial
        {
            get { return (BuscaFacturasHistorialElement)base["BuscaFacturasHistorial"]; }
        }

        /// <summary>
        /// Gets BuscaOrdenServicio configuration data
        /// </summary>
        [ConfigurationProperty("BuscaOrdenServicio")]
        public BuscaOrdenServicioElement BuscaOrdenServicio
        {
            get { return (BuscaOrdenServicioElement)base["BuscaOrdenServicio"]; }
        }

        /// <summary>
        /// Gets MuestraTipSerPrincipal configuration data
        /// </summary>
        [ConfigurationProperty("MuestraTipSerPrincipal")]
        public MuestraTipSerPrincipalElement MuestraTipSerPrincipal
        {
            get { return (MuestraTipSerPrincipalElement)base["MuestraTipSerPrincipal"]; }
        }

        /// <summary>
        /// Gets BuscaQuejasL configuration data
        /// </summary>
        [ConfigurationProperty("BuscaQuejasL")]
        public BuscaQuejasLElement BuscaQuejasL
        {
            get { return (BuscaQuejasLElement)base["BuscaQuejasL"]; }
        }

        /// <summary>
        /// Gets InformacionClientePeriodos configuration data
        /// </summary>
        [ConfigurationProperty("InformacionClientePeriodos")]
        public InformacionClientePeriodosElement InformacionClientePeriodos
        {
            get { return (InformacionClientePeriodosElement)base["InformacionClientePeriodos"]; }
        }

        /// <summary>
        /// Gets BotonClabe configuration data
        /// </summary>
        [ConfigurationProperty("BotonClabe")]
        public BotonClabeElement BotonClabe
        {
            get { return (BotonClabeElement)base["BotonClabe"]; }
        }

        /// <summary>
        /// Gets SuspencionTemporal configuration data
        /// </summary>
        [ConfigurationProperty("SuspencionTemporal")]
        public SuspencionTemporalElement SuspencionTemporal
        {
            get { return (SuspencionTemporalElement)base["SuspencionTemporal"]; }
        }

        /// <summary>
        /// Gets ValidaPideAparatos configuration data
        /// </summary>
        [ConfigurationProperty("ValidaPideAparatos")]
        public ValidaPideAparatosElement ValidaPideAparatos
        {
            get { return (ValidaPideAparatosElement)base["ValidaPideAparatos"]; }
        }

        /// <summary>
        /// Gets ValidaPideSiTieneMasAparatos configuration data
        /// </summary>
        [ConfigurationProperty("ValidaPideSiTieneMasAparatos")]
        public ValidaPideSiTieneMasAparatosElement ValidaPideSiTieneMasAparatos
        {
            get { return (ValidaPideSiTieneMasAparatosElement)base["ValidaPideSiTieneMasAparatos"]; }
        }

        /// <summary>
        /// Gets MuestraAparatosCobroAdeudo configuration data
        /// </summary>
        [ConfigurationProperty("MuestraAparatosCobroAdeudo")]
        public MuestraAparatosCobroAdeudoElement MuestraAparatosCobroAdeudo
        {
            get { return (MuestraAparatosCobroAdeudoElement)base["MuestraAparatosCobroAdeudo"]; }
        }

        /// <summary>
        /// Gets GrabaDevolucionAparato configuration data
        /// </summary>
        [ConfigurationProperty("GrabaDevolucionAparato")]
        public GrabaDevolucionAparatoElement GrabaDevolucionAparato
        {
            get { return (GrabaDevolucionAparatoElement)base["GrabaDevolucionAparato"]; }
        }

        /// <summary>
        /// Gets CobraAdeudo configuration data
        /// </summary>
        [ConfigurationProperty("CobraAdeudo")]
        public CobraAdeudoElement CobraAdeudo
        {
            get { return (CobraAdeudoElement)base["CobraAdeudo"]; }
        }

        /// <summary>
        /// Gets QuitarAntenaDevolucion configuration data
        /// </summary>
        [ConfigurationProperty("QuitarAntenaDevolucion")]
        public QuitarAntenaDevolucionElement QuitarAntenaDevolucion
        {
            get { return (QuitarAntenaDevolucionElement)base["QuitarAntenaDevolucion"]; }
        }

        /// <summary>
        /// Gets MotCan configuration data
        /// </summary>
        [ConfigurationProperty("MotCan")]
        public MotCanElement MotCan
        {
            get { return (MotCanElement)base["MotCan"]; }
        }

        /// <summary>
        /// Gets ChecarServiciosCancelados configuration data
        /// </summary>
        [ConfigurationProperty("ChecarServiciosCancelados")]
        public ChecarServiciosCanceladosElement ChecarServiciosCancelados
        {
            get { return (ChecarServiciosCanceladosElement)base["ChecarServiciosCancelados"]; }
        }

        /// <summary>
        /// Gets MuestraColonias configuration data
        /// </summary>
        [ConfigurationProperty("MuestraColonias")]
        public MuestraColoniasElement MuestraColonias
        {
            get { return (MuestraColoniasElement)base["MuestraColonias"]; }
        }

        /// <summary>
        /// Gets MuestraServiciosFAC configuration data
        /// </summary>
        [ConfigurationProperty("MuestraServiciosFAC")]
        public MuestraServiciosFACElement MuestraServiciosFAC
        {
            get { return (MuestraServiciosFACElement)base["MuestraServiciosFAC"]; }
        }

        /// <summary>
        /// Gets CiudadCAMDO configuration data
        /// </summary>
        [ConfigurationProperty("CiudadCAMDO")]
        public CiudadCAMDOElement CiudadCAMDO
        {
            get { return (CiudadCAMDOElement)base["CiudadCAMDO"]; }
        }

        /// <summary>
        /// Gets LocalidadCAMDO configuration data
        /// </summary>
        [ConfigurationProperty("LocalidadCAMDO")]
        public LocalidadCAMDOElement LocalidadCAMDO
        {
            get { return (LocalidadCAMDOElement)base["LocalidadCAMDO"]; }
        }

        /// <summary>
        /// Gets ColoniaCAMDO configuration data
        /// </summary>
        [ConfigurationProperty("ColoniaCAMDO")]
        public ColoniaCAMDOElement ColoniaCAMDO
        {
            get { return (ColoniaCAMDOElement)base["ColoniaCAMDO"]; }
        }

        /// <summary>
        /// Gets CalleCAMDO configuration data
        /// </summary>
        [ConfigurationProperty("CalleCAMDO")]
        public CalleCAMDOElement CalleCAMDO
        {
            get { return (CalleCAMDOElement)base["CalleCAMDO"]; }
        }

        /// <summary>
        /// Gets CAMDOFAC configuration data
        /// </summary>
        [ConfigurationProperty("CAMDOFAC")]
        public CAMDOFACElement CAMDOFAC
        {
            get { return (CAMDOFACElement)base["CAMDOFAC"]; }
        }

        /// <summary>
        /// Gets QuitarDetalle configuration data
        /// </summary>
        [ConfigurationProperty("QuitarDetalle")]
        public QuitarDetalleElement QuitarDetalle
        {
            get { return (QuitarDetalleElement)base["QuitarDetalle"]; }
        }

        /// <summary>
        /// Gets Bitacora configuration data
        /// </summary>
        [ConfigurationProperty("Bitacora")]
        public BitacoraElement Bitacora
        {
            get { return (BitacoraElement)base["Bitacora"]; }
        }

        /// <summary>
        /// Gets DameRelSucursalCompa configuration data
        /// </summary>
        [ConfigurationProperty("DameRelSucursalCompa")]
        public DameRelSucursalCompaElement DameRelSucursalCompa
        {
            get { return (DameRelSucursalCompaElement)base["DameRelSucursalCompa"]; }
        }

        /// <summary>
        /// Gets SumaTotalDetalle configuration data
        /// </summary>
        [ConfigurationProperty("SumaTotalDetalle")]
        public SumaTotalDetalleElement SumaTotalDetalle
        {
            get { return (SumaTotalDetalleElement)base["SumaTotalDetalle"]; }
        }

        /// <summary>
        /// Gets MuestraBancos configuration data
        /// </summary>
        [ConfigurationProperty("MuestraBancos")]
        public MuestraBancosElement MuestraBancos
        {
            get { return (MuestraBancosElement)base["MuestraBancos"]; }
        }

        /// <summary>
        /// Gets MuestraTiposTarjetas configuration data
        /// </summary>
        [ConfigurationProperty("MuestraTiposTarjetas")]
        public MuestraTiposTarjetasElement MuestraTiposTarjetas
        {
            get { return (MuestraTiposTarjetasElement)base["MuestraTiposTarjetas"]; }
        }

        /// <summary>
        /// Gets MontoNotaCredito configuration data
        /// </summary>
        [ConfigurationProperty("MontoNotaCredito")]
        public MontoNotaCreditoElement MontoNotaCredito
        {
            get { return (MontoNotaCreditoElement)base["MontoNotaCredito"]; }
        }

        /// <summary>
        /// Gets NuevoPago configuration data
        /// </summary>
        [ConfigurationProperty("NuevoPago")]
        public NuevoPagoElement NuevoPago
        {
            get { return (NuevoPagoElement)base["NuevoPago"]; }
        }

        /// <summary>
        /// Gets VendedoresL configuration data
        /// </summary>
        [ConfigurationProperty("VendedoresL")]
        public VendedoresLElement VendedoresL
        {
            get { return (VendedoresLElement)base["VendedoresL"]; }
        }

        /// <summary>
        /// Gets UltimoSerieYFolio configuration data
        /// </summary>
        [ConfigurationProperty("UltimoSerieYFolio")]
        public UltimoSerieYFolioElement UltimoSerieYFolio
        {
            get { return (UltimoSerieYFolioElement)base["UltimoSerieYFolio"]; }
        }

        /// <summary>
        /// Gets FolioDisponible configuration data
        /// </summary>
        [ConfigurationProperty("FolioDisponible")]
        public FolioDisponibleElement FolioDisponible
        {
            get { return (FolioDisponibleElement)base["FolioDisponible"]; }
        }

        /// <summary>
        /// Gets ChecaFolioUsado configuration data
        /// </summary>
        [ConfigurationProperty("ChecaFolioUsado")]
        public ChecaFolioUsadoElement ChecaFolioUsado
        {
            get { return (ChecaFolioUsadoElement)base["ChecaFolioUsado"]; }
        }

        /// <summary>
        /// Gets Adelantar configuration data
        /// </summary>
        [ConfigurationProperty("Adelantar")]
        public AdelantarElement Adelantar
        {
            get { return (AdelantarElement)base["Adelantar"]; }
        }

        /// <summary>
        /// Gets PagoAdelantado configuration data
        /// </summary>
        [ConfigurationProperty("PagoAdelantado")]
        public PagoAdelantadoElement PagoAdelantado
        {
            get { return (PagoAdelantadoElement)base["PagoAdelantado"]; }
        }

        /// <summary>
        /// Gets GrabaFacturas2 configuration data
        /// </summary>
        [ConfigurationProperty("GrabaFacturas2")]
        public GrabaFacturas2Element GrabaFacturas2
        {
            get { return (GrabaFacturas2Element)base["GrabaFacturas2"]; }
        }

        /// <summary>
        /// Gets AddServicioAdicionales configuration data
        /// </summary>
        [ConfigurationProperty("AddServicioAdicionales")]
        public AddServicioAdicionalesElement AddServicioAdicionales
        {
            get { return (AddServicioAdicionalesElement)base["AddServicioAdicionales"]; }
        }

        /// <summary>
        /// Gets CobraBajasOPrecobraAdeudo configuration data
        /// </summary>
        [ConfigurationProperty("CobraBajasOPrecobraAdeudo")]
        public CobraBajasOPrecobraAdeudoElement CobraBajasOPrecobraAdeudo
        {
            get { return (CobraBajasOPrecobraAdeudoElement)base["CobraBajasOPrecobraAdeudo"]; }
        }

        /// <summary>
        /// Gets CrearTicketTable configuration data
        /// </summary>
        [ConfigurationProperty("CrearTicketTable")]
        public CrearTicketTableElement CrearTicketTable
        {
            get { return (CrearTicketTableElement)base["CrearTicketTable"]; }
        }

        /// <summary>
        /// Gets ConceptosTicketPagos configuration data
        /// </summary>
        [ConfigurationProperty("ConceptosTicketPagos")]
        public ConceptosTicketPagosElement ConceptosTicketPagos
        {
            get { return (ConceptosTicketPagosElement)base["ConceptosTicketPagos"]; }
        }

        /// <summary>
        /// Gets ConsultaOrsSer configuration data
        /// </summary>
        [ConfigurationProperty("ConsultaOrsSer")]
        public ConsultaOrsSerElement ConsultaOrsSer
        {
            get { return (ConsultaOrsSerElement)base["ConsultaOrsSer"]; }
        }

        /// <summary>
        /// Gets ConsultarQuejasTable configuration data
        /// </summary>
        [ConfigurationProperty("ConsultarQuejasTable")]
        public ConsultarQuejasTableElement ConsultarQuejasTable
        {
            get { return (ConsultarQuejasTableElement)base["ConsultarQuejasTable"]; }
        }
        /// <summary>
        /// Gets DistribuidorReporte configuration data
        /// </summary>
        [ConfigurationProperty("DistribuidorReporte")]
        public DistribuidorReporteElement DistribuidorReporte
        {
            get { return (DistribuidorReporteElement)base["DistribuidorReporte"]; }
        }

        /// <summary>
        /// Gets RepDistribuidores configuration data
        /// </summary>
        [ConfigurationProperty("RepDistribuidores")]
        public RepDistribuidoresElement RepDistribuidores
        {
            get { return (RepDistribuidoresElement)base["RepDistribuidores"]; }
        }

        /// <summary>
        /// Gets PlazaReporte configuration data
        /// </summary>
        [ConfigurationProperty("PlazaReporte")]
        public PlazaReporteElement PlazaReporte
        {
            get { return (PlazaReporteElement)base["PlazaReporte"]; }
        }

        /// <summary>
        /// Gets RepPlaza configuration data
        /// </summary>
        [ConfigurationProperty("RepPlaza")]
        public RepPlazaElement RepPlaza
        {
            get { return (RepPlazaElement)base["RepPlaza"]; }
        }

        /// <summary>
        /// Gets Muestra_SucursalesCortes configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_SucursalesCortes")]
        public Muestra_SucursalesCortesElement Muestra_SucursalesCortes
        {
            get { return (Muestra_SucursalesCortesElement)base["Muestra_SucursalesCortes"]; }
        }

        /// <summary>
        /// Gets Muestra_UsuariosCortes configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_UsuariosCortes")]
        public Muestra_UsuariosCortesElement Muestra_UsuariosCortes
        {
            get { return (Muestra_UsuariosCortesElement)base["Muestra_UsuariosCortes"]; }
        }

        /// <summary>
        /// Gets Muestra_VendedoresCortes configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_VendedoresCortes")]
        public Muestra_VendedoresCortesElement Muestra_VendedoresCortes
        {
            get { return (Muestra_VendedoresCortesElement)base["Muestra_VendedoresCortes"]; }
        }

        /// <summary>
        /// Gets Muestra_CajasCortes configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_CajasCortes")]
        public Muestra_CajasCortesElement Muestra_CajasCortes
        {
            get { return (Muestra_CajasCortesElement)base["Muestra_CajasCortes"]; }
        }

        /// <summary>
        /// Gets OpcionesCortesFacturas configuration data
        /// </summary>
        [ConfigurationProperty("OpcionesCortesFacturas")]
        public OpcionesCortesFacturasElement OpcionesCortesFacturas
        {
            get { return (OpcionesCortesFacturasElement)base["OpcionesCortesFacturas"]; }
        }

        /// <summary>
        /// Gets CatalogoReportesFac configuration data
        /// </summary>
        [ConfigurationProperty("CatalogoReportesFac")]
        public CatalogoReportesFacElement CatalogoReportesFac
        {
            get { return (CatalogoReportesFacElement)base["CatalogoReportesFac"]; }
        }

        /// <summary>
        /// Gets AddTokenSeguridadFac configuration data
        /// </summary>
        [ConfigurationProperty("AddTokenSeguridadFac")]
        public AddTokenSeguridadFacElement AddTokenSeguridadFac
        {
            get { return (AddTokenSeguridadFacElement)base["AddTokenSeguridadFac"]; }
        }

        /// <summary>
        /// Gets SeguridadToken configuration data
        /// </summary>
        [ConfigurationProperty("SeguridadToken")]
        public SeguridadTokenElement SeguridadToken
        {
            get { return (SeguridadTokenElement)base["SeguridadToken"]; }
        }

        /// <summary>
        /// Gets PlazasXml configuration data
        /// </summary>
        [ConfigurationProperty("PlazasXml")]
        public PlazasXmlElement PlazasXml
        {
            get { return (PlazasXmlElement)base["PlazasXml"]; }
        }

        /// <summary>
        /// Gets ReporteCortes configuration data
        /// </summary>
        [ConfigurationProperty("ReporteCortes")]
        public ReporteCortesElement ReporteCortes
        {
            get { return (ReporteCortesElement)base["ReporteCortes"]; }
        }

        /// <summary>
        /// Gets DistribuidoresXml configuration data
        /// </summary>
        [ConfigurationProperty("DistribuidoresXml")]
        public DistribuidoresXmlElement DistribuidoresXml
        {
            get { return (DistribuidoresXmlElement)base["DistribuidoresXml"]; }
        }

        /// <summary>
        /// Gets ReporteCortesFac configuration data
        /// </summary>
        [ConfigurationProperty("ReporteCortesFac")]
        public ReporteCortesFacElement ReporteCortesFac
        {
            get { return (ReporteCortesFacElement)base["ReporteCortesFac"]; }
        }

        /// <summary>
        /// Gets ReporteFacFis configuration data
        /// </summary>
        [ConfigurationProperty("ReporteFacFis")]
        public ReporteFacFisElement ReporteFacFis
        {
            get { return (ReporteFacFisElement)base["ReporteFacFis"]; }
        }

        /// <summary>
        /// Gets ReportesCortesPlaza configuration data
        /// </summary>
        [ConfigurationProperty("ReportesCortesPlaza")]
        public ReportesCortesPlazaElement ReportesCortesPlaza
        {
            get { return (ReportesCortesPlazaElement)base["ReportesCortesPlaza"]; }
        }

        /// <summary>
        /// Gets SucursalesEspeciales configuration data
        /// </summary>
        [ConfigurationProperty("SucursalesEspeciales")]
        public SucursalesEspecialesElement SucursalesEspeciales
        {
            get { return (SucursalesEspecialesElement)base["SucursalesEspeciales"]; }
        }


        /// <summary>
        /// Gets ReporteCortesEspeciales configuration data
        /// </summary>
        [ConfigurationProperty("ReporteCortesEspeciales")]
        public ReporteCortesEspecialesElement ReporteCortesEspeciales
        {
            get { return (ReporteCortesEspecialesElement)base["ReporteCortesEspeciales"]; }
        }

        /// <summary>
        /// Gets RepFacFisEspeciales configuration data
        /// </summary>
        [ConfigurationProperty("RepFacFisEspeciales")]
        public RepFacFisEspecialesElement RepFacFisEspeciales
        {
            get { return (RepFacFisEspecialesElement)base["RepFacFisEspeciales"]; }
        }

        /// <summary>
        /// Gets MuestraPlazasProcesos configuration data
        /// </summary>
        [ConfigurationProperty("MuestraPlazasProcesos")]
        public MuestraPlazasProcesosElement MuestraPlazasProcesos
        {
            get { return (MuestraPlazasProcesosElement)base["MuestraPlazasProcesos"]; }
        }

        /// <summary>
        /// Gets MuestraCajerosProcesos configuration data
        /// </summary>
        [ConfigurationProperty("MuestraCajerosProcesos")]
        public MuestraCajerosProcesosElement MuestraCajerosProcesos
        {
            get { return (MuestraCajerosProcesosElement)base["MuestraCajerosProcesos"]; }
        }

        /// <summary>
        /// Gets EntregaParcial configuration data
        /// </summary>
        [ConfigurationProperty("EntregaParcial")]
        public EntregaParcialElement EntregaParcial
        {
            get { return (EntregaParcialElement)base["EntregaParcial"]; }
        }

        /// <summary>
        /// Gets BuscaParciales configuration data
        /// </summary>
        [ConfigurationProperty("BuscaParciales")]
        public BuscaParcialesElement BuscaParciales
        {
            get { return (BuscaParcialesElement)base["BuscaParciales"]; }
        }

        /// <summary>
        /// Gets uspChecaSiTieneDesglose configuration data
        /// </summary>
        [ConfigurationProperty("uspChecaSiTieneDesglose")]
        public uspChecaSiTieneDesgloseElement uspChecaSiTieneDesglose
        {
            get { return (uspChecaSiTieneDesgloseElement)base["uspChecaSiTieneDesglose"]; }
        }

        /// <summary>
        /// Gets DesgloseDeMoneda configuration data
        /// </summary>
        [ConfigurationProperty("DesgloseDeMoneda")]
        public DesgloseDeMonedaElement DesgloseDeMoneda
        {
            get { return (DesgloseDeMonedaElement)base["DesgloseDeMoneda"]; }
        }

        /// <summary>
        /// Gets BuscaDesgloseDeMoneda configuration data
        /// </summary>
        [ConfigurationProperty("BuscaDesgloseDeMoneda")]
        public BuscaDesgloseDeMonedaElement BuscaDesgloseDeMoneda
        {
            get { return (BuscaDesgloseDeMonedaElement)base["BuscaDesgloseDeMoneda"]; }
        }

        /// <summary>
        /// Gets T1RepArqueo configuration data
        /// </summary>
        [ConfigurationProperty("T1RepArqueo")]
        public T1RepArqueoElement T1RepArqueo
        {
            get { return (T1RepArqueoElement)base["T1RepArqueo"]; }
        }

        /// <summary>
        /// Gets ValidacionLoginCajera configuration data
        /// </summary>
        [ConfigurationProperty("ValidacionLoginCajera")]
        public ValidacionLoginCajeraElement ValidacionLoginCajera
        {
            get { return (ValidacionLoginCajeraElement)base["ValidacionLoginCajera"]; }
        }

        /// <summary>
        /// Gets uspHaz_Pregunta configuration data
        /// </summary>
        [ConfigurationProperty("uspHaz_Pregunta")]
        public uspHaz_PreguntaElement uspHaz_Pregunta
        {
            get { return (uspHaz_PreguntaElement)base["uspHaz_Pregunta"]; }
        }

        /// <summary>
        /// Gets ChecaOrdenRetiro configuration data
        /// </summary>
        [ConfigurationProperty("ChecaOrdenRetiro")]
        public ChecaOrdenRetiroElement ChecaOrdenRetiro
        {
            get { return (ChecaOrdenRetiroElement)base["ChecaOrdenRetiro"]; }
        }

        /// <summary>
        /// Gets ConRelClienteObs configuration data
        /// </summary>
        [ConfigurationProperty("ConRelClienteObs")]
        public ConRelClienteObsElement ConRelClienteObs
        {
            get { return (ConRelClienteObsElement)base["ConRelClienteObs"]; }
        }

        /// <summary>
        /// Gets SessionWebDos configuration data
        /// </summary>
        [ConfigurationProperty("SessionWebDos")]
        public SessionWebDosElement SessionWebDos
        {
            get { return (SessionWebDosElement)base["SessionWebDos"]; }
        }

        /// <summary>
        /// Gets Muestra_Compania_RelUsuario configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_Compania_RelUsuario")]
        public Muestra_Compania_RelUsuarioElement Muestra_Compania_RelUsuario
        {
            get { return (Muestra_Compania_RelUsuarioElement)base["Muestra_Compania_RelUsuario"]; }
        }

        /// <summary>
        /// Gets MuestraTipSerPrincipal2 configuration data
        /// </summary>
        [ConfigurationProperty("MuestraTipSerPrincipal2")]
        public MuestraTipSerPrincipal2Element MuestraTipSerPrincipal2
        {
            get { return (MuestraTipSerPrincipal2Element)base["MuestraTipSerPrincipal2"]; }
        }

        /// <summary>
        /// Gets MUESTRAUSUARIOS configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRAUSUARIOS")]
        public MUESTRAUSUARIOSElement MUESTRAUSUARIOS
        {
            get { return (MUESTRAUSUARIOSElement)base["MUESTRAUSUARIOS"]; }
        }

        /// <summary>
        /// Gets uspConsultaColonias configuration data
        /// </summary>
        [ConfigurationProperty("uspConsultaColonias")]
        public uspConsultaColoniasElement uspConsultaColonias
        {
            get { return (uspConsultaColoniasElement)base["uspConsultaColonias"]; }
        }

        /// <summary>
        /// Gets uspBuscaLLamadasDeInternet configuration data
        /// </summary>
        [ConfigurationProperty("uspBuscaLLamadasDeInternet")]
        public uspBuscaLLamadasDeInternetElement uspBuscaLLamadasDeInternet
        {
            get { return (uspBuscaLLamadasDeInternetElement)base["uspBuscaLLamadasDeInternet"]; }
        }


        /// <summary>
        /// Gets uspConsultaColoniasPorUsuario configuration data
        /// </summary>
        [ConfigurationProperty("uspConsultaColoniasPorUsuario")]
        public uspConsultaColoniasPorUsuarioElement uspConsultaColoniasPorUsuario
        {
            get { return (uspConsultaColoniasPorUsuarioElement)base["uspConsultaColoniasPorUsuario"]; }
        }

        /// <summary>
        /// Gets uspBuscaContratoSeparado2 configuration data
        /// </summary>
        [ConfigurationProperty("uspBuscaContratoSeparado2")]
        public uspBuscaContratoSeparado2Element uspBuscaContratoSeparado2
        {
            get { return (uspBuscaContratoSeparado2Element)base["uspBuscaContratoSeparado2"]; }
        }

        /// <summary>
        /// Gets uspConsultaTblClasificacionProblemas configuration data
        /// </summary>
        [ConfigurationProperty("uspConsultaTblClasificacionProblemas")]
        public uspConsultaTblClasificacionProblemasElement uspConsultaTblClasificacionProblemas
        {
            get { return (uspConsultaTblClasificacionProblemasElement)base["uspConsultaTblClasificacionProblemas"]; }
        }

        /// <summary>
        /// Gets MUESTRATRABAJOSQUEJAS configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRATRABAJOSQUEJAS")]
        public MUESTRATRABAJOSQUEJASElement MUESTRATRABAJOSQUEJAS
        {
            get { return (MUESTRATRABAJOSQUEJASElement)base["MUESTRATRABAJOSQUEJAS"]; }
        }

        /// <summary>
        /// Gets uspContratoServ configuration data
        /// </summary>
        [ConfigurationProperty("uspContratoServ")]
        public uspContratoServElement uspContratoServ
        {
            get { return (uspContratoServElement)base["uspContratoServ"]; }
        }

        /// <summary>
        /// Gets MUESTRACLASIFICACIONQUEJAS configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRACLASIFICACIONQUEJAS")]
        public MUESTRACLASIFICACIONQUEJASElement MUESTRACLASIFICACIONQUEJAS
        {
            get { return (MUESTRACLASIFICACIONQUEJASElement)base["MUESTRACLASIFICACIONQUEJAS"]; }
        }


        /// <summary>
        /// Gets Softv_GetPrioridadQueja configuration data
        /// </summary>
        [ConfigurationProperty("Softv_GetPrioridadQueja")]
        public Softv_GetPrioridadQuejaElement Softv_GetPrioridadQueja
        {
            get { return (Softv_GetPrioridadQuejaElement)base["Softv_GetPrioridadQueja"]; }
        }

        /// <summary>
        /// Gets VALIDAOrdenQueja configuration data
        /// </summary>
        [ConfigurationProperty("VALIDAOrdenQueja")]
        public VALIDAOrdenQuejaElement VALIDAOrdenQueja
        {
            get { return (VALIDAOrdenQuejaElement)base["VALIDAOrdenQueja"]; }
        }

        /// <summary>
        /// Gets ObtenerUsarioPorClave configuration data
        /// </summary>
        [ConfigurationProperty("ObtenerUsarioPorClave")]
        public ObtenerUsarioPorClaveElement ObtenerUsarioPorClave
        {
            get { return (ObtenerUsarioPorClaveElement)base["ObtenerUsarioPorClave"]; }
        }

        /// <summary>
        /// Gets Muestra_Tecnicos_Almacen configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_Tecnicos_Almacen")]
        public Muestra_Tecnicos_AlmacenElement Muestra_Tecnicos_Almacen
        {
            get { return (Muestra_Tecnicos_AlmacenElement)base["Muestra_Tecnicos_Almacen"]; }
        }


        /// <summary>
        /// Gets spConsultaTurnos configuration data
        /// </summary>
        [ConfigurationProperty("spConsultaTurnos")]
        public spConsultaTurnosElement spConsultaTurnos
        {
            get { return (spConsultaTurnosElement)base["spConsultaTurnos"]; }
        }

        /// <summary>
        /// Gets LLamadasdeInternet configuration data
        /// </summary>
        [ConfigurationProperty("LLamadasdeInternet")]
        public LLamadasdeInternetElement LLamadasdeInternet
        {
            get { return (LLamadasdeInternetElement)base["LLamadasdeInternet"]; }
        }


        /// <summary>
        /// Gets Quejas configuration data
        /// </summary>
        [ConfigurationProperty("Quejas")]
        public QuejasElement Quejas
        {
            get { return (QuejasElement)base["Quejas"]; }
        }

        /// <summary>
        /// Gets Actualizar_quejasCallCenter configuration data
        /// </summary>
        [ConfigurationProperty("Actualizar_quejasCallCenter")]
        public Actualizar_quejasCallCenterElement Actualizar_quejasCallCenter
        {
            get { return (Actualizar_quejasCallCenterElement)base["Actualizar_quejasCallCenter"]; }
        }

        /// <summary>
        /// Gets BotonEscalarQuejas configuration data
        /// </summary>
        [ConfigurationProperty("BotonEscalarQuejas")]
        public BotonEscalarQuejasElement BotonEscalarQuejas
        {
            get { return (BotonEscalarQuejasElement)base["BotonEscalarQuejas"]; }
        }

        /// <summary>
        /// Gets DameBonificacion configuration data
        /// </summary>
        [ConfigurationProperty("DameBonificacion")]
        public DameBonificacionElement DameBonificacion
        {
            get { return (DameBonificacionElement)base["DameBonificacion"]; }
        }

        /// <summary>
        /// Gets sp_dameContratoCompaniaAdic configuration data
        /// </summary>
        [ConfigurationProperty("sp_dameContratoCompaniaAdic")]
        public sp_dameContratoCompaniaAdicElement sp_dameContratoCompaniaAdic
        {
            get { return (sp_dameContratoCompaniaAdicElement)base["sp_dameContratoCompaniaAdic"]; }
        }

        /// <summary>
        /// Gets MUESTRATIPOFACTURA configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRATIPOFACTURA")]
        public MUESTRATIPOFACTURAElement MUESTRATIPOFACTURA
        {
            get { return (MUESTRATIPOFACTURAElement)base["MUESTRATIPOFACTURA"]; }
        }

        /// <summary>
        /// Gets BUSCAFACTURAS configuration data
        /// </summary>
        [ConfigurationProperty("BUSCAFACTURAS")]
        public BUSCAFACTURASElement BUSCAFACTURAS
        {
            get { return (BUSCAFACTURASElement)base["BUSCAFACTURAS"]; }
        }

        /// <summary>
        /// Gets ValidaCancelacionFactura configuration data
        /// </summary>
        [ConfigurationProperty("ValidaCancelacionFactura")]
        public ValidaCancelacionFacturaElement ValidaCancelacionFactura
        {
            get { return (ValidaCancelacionFacturaElement)base["ValidaCancelacionFactura"]; }
        }

        /// <summary>
        /// Gets MUESTRAMOTIVOS configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRAMOTIVOS")]
        public MUESTRAMOTIVOSElement MUESTRAMOTIVOS
        {
            get { return (MUESTRAMOTIVOSElement)base["MUESTRAMOTIVOS"]; }
        }

        /// <summary>
        /// Gets GuardaMotivos configuration data
        /// </summary>
        [ConfigurationProperty("GuardaMotivos")]
        public GuardaMotivosElement GuardaMotivos
        {
            get { return (GuardaMotivosElement)base["GuardaMotivos"]; }
        }

        /// <summary>
        /// Gets ObtieneSucursalesEspeciales_Reimpresion configuration data
        /// </summary>
        [ConfigurationProperty("ObtieneSucursalesEspeciales_Reimpresion")]
        public ObtieneSucursalesEspeciales_ReimpresionElement ObtieneSucursalesEspeciales_Reimpresion
        {
            get { return (ObtieneSucursalesEspeciales_ReimpresionElement)base["ObtieneSucursalesEspeciales_Reimpresion"]; }
        }

        /// <summary>
        /// Gets MUESTRATIPOFACTURA_Reimpresion configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRATIPOFACTURA_Reimpresion")]
        public MUESTRATIPOFACTURA_ReimpresionElement MUESTRATIPOFACTURA_Reimpresion
        {
            get { return (MUESTRATIPOFACTURA_ReimpresionElement)base["MUESTRATIPOFACTURA_Reimpresion"]; }
        }

        /// <summary>
        /// Gets BuscaFacturasEspeciales configuration data
        /// </summary>
        [ConfigurationProperty("BuscaFacturasEspeciales")]
        public BuscaFacturasEspecialesElement BuscaFacturasEspeciales
        {
            get { return (BuscaFacturasEspecialesElement)base["BuscaFacturasEspeciales"]; }
        }

        /// <summary>
        /// Gets ValidaFacturaFiscal configuration data
        /// </summary>
        [ConfigurationProperty("ValidaFacturaFiscal")]
        public ValidaFacturaFiscalElement ValidaFacturaFiscal
        {
            get { return (ValidaFacturaFiscalElement)base["ValidaFacturaFiscal"]; }
        }

        /// <summary>
        /// Gets CANCELACIONFACTURAS configuration data
        /// </summary>
        [ConfigurationProperty("CANCELACIONFACTURAS")]
        public CANCELACIONFACTURASElement CANCELACIONFACTURAS
        {
            get { return (CANCELACIONFACTURASElement)base["CANCELACIONFACTURAS"]; }
        }

        /// <summary>
        /// Gets compania configuration data
        /// </summary>
        [ConfigurationProperty("compania")]
        public companiaElement compania
        {
            get { return (companiaElement)base["compania"]; }
        }


        /// <summary>
        /// Gets Estado configuration data
        /// </summary>
        [ConfigurationProperty("Estado")]
        public EstadoElement Estado
        {
            get { return (EstadoElement)base["Estado"]; }
        }

        /// <summary>
        /// Gets Periodos configuration data
        /// </summary>
        [ConfigurationProperty("Periodos")]
        public PeriodosElement Periodos
        {
            get { return (PeriodosElement)base["Periodos"]; }
        }

        /// <summary>
        /// Gets PlazaRepVarios configuration data
        /// </summary>
        [ConfigurationProperty("PlazaRepVarios")]
        public PlazaRepVariosElement PlazaRepVarios
        {
            get { return (PlazaRepVariosElement)base["PlazaRepVarios"]; }
        }

        /// <summary>
        /// Gets ServiciosDigital configuration data
        /// </summary>
        [ConfigurationProperty("ServiciosDigital")]
        public ServiciosDigitalElement ServiciosDigital
        {
            get { return (ServiciosDigitalElement)base["ServiciosDigital"]; }
        }

        /// <summary>
        /// Gets ServiciosInternet configuration data
        /// </summary>
        [ConfigurationProperty("ServiciosInternet")]
        public ServiciosInternetElement ServiciosInternet
        {
            get { return (ServiciosInternetElement)base["ServiciosInternet"]; }
        }

        /// <summary>
        /// Gets TipServ configuration data
        /// </summary>
        [ConfigurationProperty("TipServ")]
        public TipServElement TipServ
        {
            get { return (TipServElement)base["TipServ"]; }
        }

        /// <summary>
        /// Gets TiposCliente configuration data
        /// </summary>
        [ConfigurationProperty("TiposCliente")]
        public TiposClienteElement TiposCliente
        {
            get { return (TiposClienteElement)base["TiposCliente"]; }
        }

        /// <summary>
        /// Gets uspBuscaOrdSer_BuscaOrdSerSeparado2 configuration data
        /// </summary>
        [ConfigurationProperty("uspBuscaOrdSer_BuscaOrdSerSeparado2")]
        public uspBuscaOrdSer_BuscaOrdSerSeparado2Element uspBuscaOrdSer_BuscaOrdSerSeparado2
        {
            get { return (uspBuscaOrdSer_BuscaOrdSerSeparado2Element)base["uspBuscaOrdSer_BuscaOrdSerSeparado2"]; }
        }

        /// <summary>
        /// Gets uspChecaCuantasOrdenesQuejas configuration data
        /// </summary>
        [ConfigurationProperty("uspChecaCuantasOrdenesQuejas")]
        public uspChecaCuantasOrdenesQuejasElement uspChecaCuantasOrdenesQuejas
        {
            get { return (uspChecaCuantasOrdenesQuejasElement)base["uspChecaCuantasOrdenesQuejas"]; }
        }

        /// <summary>
        /// Gets MUESTRATRABAJOSPorTipoUsuario configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRATRABAJOSPorTipoUsuario")]
        public MUESTRATRABAJOSPorTipoUsuarioElement MUESTRATRABAJOSPorTipoUsuario
        {
            get { return (MUESTRATRABAJOSPorTipoUsuarioElement)base["MUESTRATRABAJOSPorTipoUsuario"]; }
        }

        /// <summary>
        /// Gets Dime_Que_servicio_Tiene_cliente configuration data
        /// </summary>
        [ConfigurationProperty("Dime_Que_servicio_Tiene_cliente")]
        public Dime_Que_servicio_Tiene_clienteElement Dime_Que_servicio_Tiene_cliente
        {
            get { return (Dime_Que_servicio_Tiene_clienteElement)base["Dime_Que_servicio_Tiene_cliente"]; }
        }

        /// <summary>
        /// Gets Dimesihay_Conex configuration data
        /// </summary>
        [ConfigurationProperty("Dimesihay_Conex")]
        public Dimesihay_ConexElement Dimesihay_Conex
        {
            get { return (Dimesihay_ConexElement)base["Dimesihay_Conex"]; }
        }

        /// <summary>
        /// Gets BuscaQuejasSeparado2 configuration data
        /// </summary>
        [ConfigurationProperty("BuscaQuejasSeparado2")]
        public BuscaQuejasSeparado2Element BuscaQuejasSeparado2
        {
            get { return (BuscaQuejasSeparado2Element)base["BuscaQuejasSeparado2"]; }
        }

        /// <summary>
        /// Gets uspBorraQuejasOrdenes configuration data
        /// </summary>
        [ConfigurationProperty("uspBorraQuejasOrdenes")]
        public uspBorraQuejasOrdenesElement uspBorraQuejasOrdenes
        {
            get { return (uspBorraQuejasOrdenesElement)base["uspBorraQuejasOrdenes"]; }
        }

        /// <summary>
        /// Gets ValidaQuejaCompaniaAdic configuration data
        /// </summary>
        [ConfigurationProperty("ValidaQuejaCompaniaAdic")]
        public ValidaQuejaCompaniaAdicElement ValidaQuejaCompaniaAdic
        {
            get { return (ValidaQuejaCompaniaAdicElement)base["ValidaQuejaCompaniaAdic"]; }
        }

        /// <summary>
        /// Gets BuscaBloqueado configuration data
        /// </summary>
        [ConfigurationProperty("BuscaBloqueado")]
        public BuscaBloqueadoElement BuscaBloqueado
        {
            get { return (BuscaBloqueadoElement)base["BuscaBloqueado"]; }
        }

        /// <summary>
        /// Gets OrdSer configuration data
        /// </summary>
        [ConfigurationProperty("OrdSer")]
        public OrdSerElement OrdSer
        {
            get { return (OrdSerElement)base["OrdSer"]; }
        }

        /// <summary>
        /// Gets DetOrdSer configuration data
        /// </summary>
        [ConfigurationProperty("DetOrdSer")]
        public DetOrdSerElement DetOrdSer
        {
            get { return (DetOrdSerElement)base["DetOrdSer"]; }
        }

        /// <summary>
        /// Gets CAMDO configuration data
        /// </summary>
        [ConfigurationProperty("CAMDO")]
        public CAMDOElement CAMDO
        {
            get { return (CAMDOElement)base["CAMDO"]; }
        }

        /// <summary>
        /// Gets IPAQU configuration data
        /// </summary>
        [ConfigurationProperty("IPAQU")]
        public IPAQUElement IPAQU
        {
            get { return (IPAQUElement)base["IPAQU"]; }
        }


        /// <summary>
        /// Gets MuestraGuaBor configuration data
        /// </summary>
        [ConfigurationProperty("MuestraGuaBor")]
        public MuestraGuaBorElement MuestraGuaBor
        {
            get { return (MuestraGuaBorElement)base["MuestraGuaBor"]; }
        }

        /// <summary>
        /// Gets TblFacturasOpciones configuration data
        /// </summary>
        [ConfigurationProperty("TblFacturasOpciones")]
        public TblFacturasOpcionesElement TblFacturasOpciones
        {
            get { return (TblFacturasOpcionesElement)base["TblFacturasOpciones"]; }
        }

        /// <summary>
        /// Gets ContratoMaestroFac configuration data
        /// </summary>
        [ConfigurationProperty("ContratoMaestroFac")]
        public ContratoMaestroFacElement ContratoMaestroFac
        {
            get { return (ContratoMaestroFacElement)base["ContratoMaestroFac"]; }
        }

        /// <summary>
        /// Gets TiposCortesClientes configuration data
        /// </summary>
        [ConfigurationProperty("TiposCortesClientes")]
        public TiposCortesClientesElement TiposCortesClientes
        {
            get { return (TiposCortesClientesElement)base["TiposCortesClientes"]; }
        }

        /// <summary>
        /// Gets TipoPagosFacturas configuration data
        /// </summary>
        [ConfigurationProperty("TipoPagosFacturas")]
        public TipoPagosFacturasElement TipoPagosFacturas
        {
            get { return (TipoPagosFacturasElement)base["TipoPagosFacturas"]; }
        }

        /// <summary>
        /// Gets DomicilioFiscal configuration data
        /// </summary>
        [ConfigurationProperty("DomicilioFiscal")]
        public DomicilioFiscalElement DomicilioFiscal
        {
            get { return (DomicilioFiscalElement)base["DomicilioFiscal"]; }
        }


        /// <summary>
        /// Gets ValidaSiContratoExiste_CM configuration data
        /// </summary>
        [ConfigurationProperty("ValidaSiContratoExiste_CM")]
        public ValidaSiContratoExiste_CMElement ValidaSiContratoExiste_CM
        {
            get { return (ValidaSiContratoExiste_CMElement)base["ValidaSiContratoExiste_CM"]; }
        }

        /// <summary>
        /// Gets RelContratoMaestro_ContratoSoftv configuration data
        /// </summary>
        [ConfigurationProperty("RelContratoMaestro_ContratoSoftv")]
        public RelContratoMaestro_ContratoSoftvElement RelContratoMaestro_ContratoSoftv
        {
            get { return (RelContratoMaestro_ContratoSoftvElement)base["RelContratoMaestro_ContratoSoftv"]; }
        }



        /// <summary>
        /// Gets tieneEdoCuenta configuration data
        /// </summary>
        [ConfigurationProperty("tieneEdoCuenta")]
        public tieneEdoCuentaElement tieneEdoCuenta
        {
            get { return (tieneEdoCuentaElement)base["tieneEdoCuenta"]; }
        }

        /// <summary>
        /// Gets CrearNotaCredito configuration data
        /// </summary>
        [ConfigurationProperty("CrearNotaCredito")]
        public CrearNotaCreditoElement CrearNotaCredito
        {
            get { return (CrearNotaCreditoElement)base["CrearNotaCredito"]; }
        }

        /// <summary>
        /// Gets ConceptosTicketNotasCredito configuration data
        /// </summary>
        [ConfigurationProperty("ConceptosTicketNotasCredito")]
        public ConceptosTicketNotasCreditoElement ConceptosTicketNotasCredito
        {
            get { return (ConceptosTicketNotasCreditoElement)base["ConceptosTicketNotasCredito"]; }
        }


        /// <summary>
        /// Gets sp_dameInfodelCobro configuration data
        /// </summary>
        [ConfigurationProperty("sp_dameInfodelCobro")]
        public sp_dameInfodelCobroElement sp_dameInfodelCobro
        {
            get { return (sp_dameInfodelCobroElement)base["sp_dameInfodelCobro"]; }
        }

        /// <summary>
        /// Gets DamelasOrdenesque_GeneroFacturaAgendaOrdser configuration data
        /// </summary>
        [ConfigurationProperty("DamelasOrdenesque_GeneroFacturaAgendaOrdser")]
        public DamelasOrdenesque_GeneroFacturaAgendaOrdserElement DamelasOrdenesque_GeneroFacturaAgendaOrdser
        {
            get { return (DamelasOrdenesque_GeneroFacturaAgendaOrdserElement)base["DamelasOrdenesque_GeneroFacturaAgendaOrdser"]; }
        }

        /// <summary>
        /// Gets Genera_Cita_OrdserFac configuration data
        /// </summary>
        [ConfigurationProperty("Genera_Cita_OrdserFac")]
        public Genera_Cita_OrdserFacElement Genera_Cita_OrdserFac
        {
            get { return (Genera_Cita_OrdserFacElement)base["Genera_Cita_OrdserFac"]; }
        }

        /// <summary>
        /// Gets ValidaSaldoContrato configuration data
        /// </summary>
        [ConfigurationProperty("ValidaSaldoContrato")]
        public ValidaSaldoContratoElement ValidaSaldoContrato
        {
            get { return (ValidaSaldoContratoElement)base["ValidaSaldoContrato"]; }
        }

        /// <summary>
        /// Gets CobraSaldo configuration data
        /// </summary>
        [ConfigurationProperty("CobraSaldo")]
        public CobraSaldoElement CobraSaldo
        {
            get { return (CobraSaldoElement)base["CobraSaldo"]; }
        }

        /// <summary>
        /// Gets ObtieneEdoCuentaSinSaldar configuration data
        /// </summary>
        [ConfigurationProperty("ObtieneEdoCuentaSinSaldar")]
        public ObtieneEdoCuentaSinSaldarElement ObtieneEdoCuentaSinSaldar
        {
            get { return (ObtieneEdoCuentaSinSaldarElement)base["ObtieneEdoCuentaSinSaldar"]; }
        }

        /// <summary>
        /// Gets ValidaHistorialContrato configuration data
        /// </summary>
        [ConfigurationProperty("ValidaHistorialContrato")]
        public ValidaHistorialContratoElement ValidaHistorialContrato
        {
            get { return (ValidaHistorialContratoElement)base["ValidaHistorialContrato"]; }
        }

        /// <summary>
        /// Gets ObtieneEdoCuentaPorContrato configuration data
        /// </summary>
        [ConfigurationProperty("ObtieneEdoCuentaPorContrato")]
        public ObtieneEdoCuentaPorContratoElement ObtieneEdoCuentaPorContrato
        {
            get { return (ObtieneEdoCuentaPorContratoElement)base["ObtieneEdoCuentaPorContrato"]; }
        }

        /// <summary>
        /// Gets DameLineasCaptura configuration data
        /// </summary>
        [ConfigurationProperty("DameLineasCaptura")]
        public DameLineasCapturaElement DameLineasCaptura
        {
            get { return (DameLineasCapturaElement)base["DameLineasCaptura"]; }
        }

        /// <summary>
        /// Gets ReenviaEstadosCuentaPorContrato configuration data
        /// </summary>
        [ConfigurationProperty("ReenviaEstadosCuentaPorContrato")]
        public ReenviaEstadosCuentaPorContratoElement ReenviaEstadosCuentaPorContrato
        {
            get { return (ReenviaEstadosCuentaPorContratoElement)base["ReenviaEstadosCuentaPorContrato"]; }
        }

        /// <summary>
        /// Gets ObtieneInformacionEnvioCorreo configuration data
        /// </summary>
        [ConfigurationProperty("ObtieneInformacionEnvioCorreo")]
        public ObtieneInformacionEnvioCorreoElement ObtieneInformacionEnvioCorreo
        {
            get { return (ObtieneInformacionEnvioCorreoElement)base["ObtieneInformacionEnvioCorreo"]; }
        }

        /// <summary>
        /// Gets ValidaReprocesoPorContrato configuration data
        /// </summary>
        [ConfigurationProperty("ValidaReprocesoPorContrato")]
        public ValidaReprocesoPorContratoElement ValidaReprocesoPorContrato
        {
            get { return (ValidaReprocesoPorContratoElement)base["ValidaReprocesoPorContrato"]; }
        }

        /// <summary>
        /// Gets ReprocesaEdoCuentaContrato configuration data
        /// </summary>
        [ConfigurationProperty("ReprocesaEdoCuentaContrato")]
        public ReprocesaEdoCuentaContratoElement ReprocesaEdoCuentaContrato
        {
            get { return (ReprocesaEdoCuentaContratoElement)base["ReprocesaEdoCuentaContrato"]; }
        }

        /// <summary>
        /// Gets ReporteEstadoCuentaNuevo configuration data
        /// </summary>
        [ConfigurationProperty("ReporteEstadoCuentaNuevo")]
        public ReporteEstadoCuentaNuevoElement ReporteEstadoCuentaNuevo
        {
            get { return (ReporteEstadoCuentaNuevoElement)base["ReporteEstadoCuentaNuevo"]; }
        }


        /// <summary>
        /// Gets GuardaMotivoCanServ configuration data
        /// </summary>
        [ConfigurationProperty("GuardaMotivoCanServ")]
        public GuardaMotivoCanServElement GuardaMotivoCanServ
        {
            get { return (GuardaMotivoCanServElement)base["GuardaMotivoCanServ"]; }
        }

        /// <summary>
        /// Gets BUSCLIPORCONTRATO_OrdSer configuration data
        /// </summary>
        [ConfigurationProperty("BUSCLIPORCONTRATO_OrdSer")]
        public BUSCLIPORCONTRATO_OrdSerElement BUSCLIPORCONTRATO_OrdSer
        {
            get { return (BUSCLIPORCONTRATO_OrdSerElement)base["BUSCLIPORCONTRATO_OrdSer"]; }
        }

        /// <summary>
        /// Gets BUSCADetOrdSer configuration data
        /// </summary>
        [ConfigurationProperty("BUSCADetOrdSer")]
        public BUSCADetOrdSerElement BUSCADetOrdSer
        {
            get { return (BUSCADetOrdSerElement)base["BUSCADetOrdSer"]; }
        }

        /// <summary>
        /// Gets MUESTRAAPARATOS_DISCPONIBLES configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRAAPARATOS_DISCPONIBLES")]
        public MUESTRAAPARATOS_DISCPONIBLESElement MUESTRAAPARATOS_DISCPONIBLES
        {
            get { return (MUESTRAAPARATOS_DISCPONIBLESElement)base["MUESTRAAPARATOS_DISCPONIBLES"]; }
        }

        /// <summary>
        /// Gets SP_GuardaIAPARATOS configuration data
        /// </summary>
        [ConfigurationProperty("SP_GuardaIAPARATOS")]
        public SP_GuardaIAPARATOSElement SP_GuardaIAPARATOS
        {
            get { return (SP_GuardaIAPARATOSElement)base["SP_GuardaIAPARATOS"]; }
        }

        /// <summary>
        /// Gets ActualizaFacturaMaestro configuration data
        /// </summary>
        [ConfigurationProperty("ActualizaFacturaMaestro")]
        public ActualizaFacturaMaestroElement ActualizaFacturaMaestro
        {
            get { return (ActualizaFacturaMaestroElement)base["ActualizaFacturaMaestro"]; }
        }

        /// <summary>
        /// Gets GrabaFacturaCMaestro configuration data
        /// </summary>
        [ConfigurationProperty("GrabaFacturaCMaestro")]
        public GrabaFacturaCMaestroElement GrabaFacturaCMaestro
        {
            get { return (GrabaFacturaCMaestroElement)base["GrabaFacturaCMaestro"]; }
        }

        /// <summary>
        /// Gets CobraSaldoContratoMaestro configuration data
        /// </summary>
        [ConfigurationProperty("CobraSaldoContratoMaestro")]
        public CobraSaldoContratoMaestroElement CobraSaldoContratoMaestro
        {
            get { return (CobraSaldoContratoMaestroElement)base["CobraSaldoContratoMaestro"]; }
        }

        /// <summary>
        /// Gets DimeSiGrabaOrd configuration data
        /// </summary>
        [ConfigurationProperty("DimeSiGrabaOrd")]
        public DimeSiGrabaOrdElement DimeSiGrabaOrd
        {
            get { return (DimeSiGrabaOrdElement)base["DimeSiGrabaOrd"]; }
        }

        /// <summary>
        /// Gets Valida_DetOrden configuration data
        /// </summary>
        [ConfigurationProperty("Valida_DetOrden")]
        public Valida_DetOrdenElement Valida_DetOrden
        {
            get { return (Valida_DetOrdenElement)base["Valida_DetOrden"]; }
        }

        /// <summary>
        /// Gets Checa_si_tiene_camdo configuration data
        /// </summary>
        [ConfigurationProperty("Checa_si_tiene_camdo")]
        public Checa_si_tiene_camdoElement Checa_si_tiene_camdo
        {
            get { return (Checa_si_tiene_camdoElement)base["Checa_si_tiene_camdo"]; }
        }

        /// <summary>
        /// Gets Cambia_Tipo_cablemodem configuration data
        /// </summary>
        [ConfigurationProperty("Cambia_Tipo_cablemodem")]
        public Cambia_Tipo_cablemodemElement Cambia_Tipo_cablemodem
        {
            get { return (Cambia_Tipo_cablemodemElement)base["Cambia_Tipo_cablemodem"]; }
        }

        /// <summary>
        /// Gets ChecaMotivoCanServ configuration data
        /// </summary>
        [ConfigurationProperty("ChecaMotivoCanServ")]
        public ChecaMotivoCanServElement ChecaMotivoCanServ
        {
            get { return (ChecaMotivoCanServElement)base["ChecaMotivoCanServ"]; }
        }

        /// <summary>
        /// Gets VALIDADECODERS configuration data
        /// </summary>
        [ConfigurationProperty("VALIDADECODERS")]
        public VALIDADECODERSElement VALIDADECODERS
        {
            get { return (VALIDADECODERSElement)base["VALIDADECODERS"]; }
        }

        /// <summary>
        /// Gets NueRelOrdenUsuario configuration data
        /// </summary>
        [ConfigurationProperty("NueRelOrdenUsuario")]
        public NueRelOrdenUsuarioElement NueRelOrdenUsuario
        {
            get { return (NueRelOrdenUsuarioElement)base["NueRelOrdenUsuario"]; }
        }

        /// <summary>
        /// Gets DimeSiYaGrabeUnaFacMaestro configuration data
        /// </summary>
        [ConfigurationProperty("DimeSiYaGrabeUnaFacMaestro")]
        public DimeSiYaGrabeUnaFacMaestroElement DimeSiYaGrabeUnaFacMaestro
        {
            get { return (DimeSiYaGrabeUnaFacMaestroElement)base["DimeSiYaGrabeUnaFacMaestro"]; }
        }

        /// <summary>
        /// Gets GuardaPagoFacturaMaestro configuration data
        /// </summary>
        [ConfigurationProperty("GuardaPagoFacturaMaestro")]
        public GuardaPagoFacturaMaestroElement GuardaPagoFacturaMaestro
        {
            get { return (GuardaPagoFacturaMaestroElement)base["GuardaPagoFacturaMaestro"]; }
        }

        /// <summary>
        /// Gets MuestraFacturasMaestro configuration data
        /// </summary>
        [ConfigurationProperty("MuestraFacturasMaestro")]
        public MuestraFacturasMaestroElement MuestraFacturasMaestro
        {
            get { return (MuestraFacturasMaestroElement)base["MuestraFacturasMaestro"]; }
        }

        /// <summary>
        /// Gets BuscaFacturasMaestro configuration data
        /// </summary>
        [ConfigurationProperty("BuscaFacturasMaestro")]
        public BuscaFacturasMaestroElement BuscaFacturasMaestro
        {
            get { return (BuscaFacturasMaestroElement)base["BuscaFacturasMaestro"]; }
        }

        /// <summary>
        /// Gets ConsultaContratoNetCAdeudo configuration data
        /// </summary>
        [ConfigurationProperty("ConsultaContratoNetCAdeudo")]
        public ConsultaContratoNetCAdeudoElement ConsultaContratoNetCAdeudo
        {
            get { return (ConsultaContratoNetCAdeudoElement)base["ConsultaContratoNetCAdeudo"]; }
        }

        /// <summary>
        /// Gets NUEPago_En_EfectivoDetPago configuration data
        /// </summary>
        [ConfigurationProperty("NUEPago_En_EfectivoDetPago")]
        public NUEPago_En_EfectivoDetPagoElement NUEPago_En_EfectivoDetPago
        {
            get { return (NUEPago_En_EfectivoDetPagoElement)base["NUEPago_En_EfectivoDetPago"]; }
        }

        /// <summary>
        /// Gets NUEPago_En_EfectivoDetMaestro configuration data
        /// </summary>
        [ConfigurationProperty("NUEPago_En_EfectivoDetMaestro")]
        public NUEPago_En_EfectivoDetMaestroElement NUEPago_En_EfectivoDetMaestro
        {
            get { return (NUEPago_En_EfectivoDetMaestroElement)base["NUEPago_En_EfectivoDetMaestro"]; }
        }

        /// <summary>
        /// Gets SP_GuardaOrdSerAparatos configuration data
        /// </summary>
        [ConfigurationProperty("SP_GuardaOrdSerAparatos")]
        public SP_GuardaOrdSerAparatosElement SP_GuardaOrdSerAparatos
        {
            get { return (SP_GuardaOrdSerAparatosElement)base["SP_GuardaOrdSerAparatos"]; }
        }

        /// <summary>
        /// Gets MODORDSER configuration data
        /// </summary>
        [ConfigurationProperty("MODORDSER")]
        public MODORDSERElement MODORDSER
        {
            get { return (MODORDSERElement)base["MODORDSER"]; }
        }

        /// <summary>
        /// Gets ChecaOrdSerRetiro configuration data
        /// </summary>
        [ConfigurationProperty("ChecaOrdSerRetiro")]
        public ChecaOrdSerRetiroElement ChecaOrdSerRetiro
        {
            get { return (ChecaOrdSerRetiroElement)base["ChecaOrdSerRetiro"]; }
        }

        /// <summary>
        /// Gets SP_LLena_Bitacora_Ordenes configuration data
        /// </summary>
        [ConfigurationProperty("SP_LLena_Bitacora_Ordenes")]
        public SP_LLena_Bitacora_OrdenesElement SP_LLena_Bitacora_Ordenes
        {
            get { return (SP_LLena_Bitacora_OrdenesElement)base["SP_LLena_Bitacora_Ordenes"]; }
        }

        /// <summary>
        /// Gets Imprime_Orden configuration data
        /// </summary>
        [ConfigurationProperty("Imprime_Orden")]
        public Imprime_OrdenElement Imprime_Orden
        {
            get { return (Imprime_OrdenElement)base["Imprime_Orden"]; }
        }

        /// <summary>
        /// Gets ConsultaOrdSer configuration data
        /// </summary>
        [ConfigurationProperty("ConsultaOrdSer")]
        public ConsultaOrdSerElement ConsultaOrdSer
        {
            get { return (ConsultaOrdSerElement)base["ConsultaOrdSer"]; }
        }

        /// <summary>
        /// Gets GuardaMovSist configuration data
        /// </summary>
        [ConfigurationProperty("GuardaMovSist")]
        public GuardaMovSistElement GuardaMovSist
        {
            get { return (GuardaMovSistElement)base["GuardaMovSist"]; }
        }

        /// <summary>
        /// Gets MovSist configuration data
        /// </summary>
        [ConfigurationProperty("MovSist")]
        public MovSistElement MovSist
        {
            get { return (MovSistElement)base["MovSist"]; }
        }

        /// <summary>
        /// Gets ObtieneMediosPago configuration data
        /// </summary>
        [ConfigurationProperty("ObtieneMediosPago")]
        public ObtieneMediosPagoElement ObtieneMediosPago
        {
            get { return (ObtieneMediosPagoElement)base["ObtieneMediosPago"]; }
        }

        /// <summary>
        /// Gets sp_validaEliminarOrden configuration data
        /// </summary>
        [ConfigurationProperty("sp_validaEliminarOrden")]
        public sp_validaEliminarOrdenElement sp_validaEliminarOrden
        {
            get { return (sp_validaEliminarOrdenElement)base["sp_validaEliminarOrden"]; }
        }

        /// <summary>
        /// Gets DameDetalle_FacturaMaestro configuration data
        /// </summary>
        [ConfigurationProperty("DameDetalle_FacturaMaestro")]
        public DameDetalle_FacturaMaestroElement DameDetalle_FacturaMaestro
        {
            get { return (DameDetalle_FacturaMaestroElement)base["DameDetalle_FacturaMaestro"]; }
        }

        /// <summary>
        /// Gets ObtieneHistorialPagosFacturaMaestro configuration data
        /// </summary>
        [ConfigurationProperty("ObtieneHistorialPagosFacturaMaestro")]
        public ObtieneHistorialPagosFacturaMaestroElement ObtieneHistorialPagosFacturaMaestro
        {
            get { return (ObtieneHistorialPagosFacturaMaestroElement)base["ObtieneHistorialPagosFacturaMaestro"]; }
        }

        /// <summary>
        /// Gets BuscaFacturasMaestroConsulta configuration data
        /// </summary>
        [ConfigurationProperty("BuscaFacturasMaestroConsulta")]
        public BuscaFacturasMaestroConsultaElement BuscaFacturasMaestroConsulta
        {
            get { return (BuscaFacturasMaestroConsultaElement)base["BuscaFacturasMaestroConsulta"]; }
        }

        /// <summary>
        /// Gets CobraContratoMaestro configuration data
        /// </summary>
        [ConfigurationProperty("CobraContratoMaestro")]
        public CobraContratoMaestroElement CobraContratoMaestro
        {
            get { return (CobraContratoMaestroElement)base["CobraContratoMaestro"]; }
        }

        /// <summary>
        /// Gets DetalleContratosMaestros configuration data
        /// </summary>
        [ConfigurationProperty("DetalleContratosMaestros")]
        public DetalleContratosMaestrosElement DetalleContratosMaestros
        {
            get { return (DetalleContratosMaestrosElement)base["DetalleContratosMaestros"]; }
        }

        /// <summary>
        /// Gets NotasDeCredito_ContraMaeFac configuration data
        /// </summary>
        [ConfigurationProperty("NotasDeCredito_ContraMaeFac")]
        public NotasDeCredito_ContraMaeFacElement NotasDeCredito_ContraMaeFac
        {
            get { return (NotasDeCredito_ContraMaeFacElement)base["NotasDeCredito_ContraMaeFac"]; }
        }

        /// <summary>
        /// Gets StatusNotadeCredito configuration data
        /// </summary>
        [ConfigurationProperty("StatusNotadeCredito")]
        public StatusNotadeCreditoElement StatusNotadeCredito
        {
            get { return (StatusNotadeCreditoElement)base["StatusNotadeCredito"]; }
        }

        /// <summary>
        /// Gets DAME_FACTURASDECLIENTE configuration data
        /// </summary>
        [ConfigurationProperty("DAME_FACTURASDECLIENTE")]
        public DAME_FACTURASDECLIENTEElement DAME_FACTURASDECLIENTE
        {
            get { return (DAME_FACTURASDECLIENTEElement)base["DAME_FACTURASDECLIENTE"]; }
        }

        /// <summary>
        /// Gets Detalle_NotasdeCredito configuration data
        /// </summary>
        [ConfigurationProperty("Detalle_NotasdeCredito")]
        public Detalle_NotasdeCreditoElement Detalle_NotasdeCredito
        {
            get { return (Detalle_NotasdeCreditoElement)base["Detalle_NotasdeCredito"]; }
        }

        /// <summary>
        /// Gets Calcula_monto configuration data
        /// </summary>
        [ConfigurationProperty("Calcula_monto")]
        public Calcula_montoElement Calcula_monto
        {
            get { return (Calcula_montoElement)base["Calcula_monto"]; }
        }

        /// <summary>
        /// Gets GeneraRefBanamexMaestro configuration data
        /// </summary>
        [ConfigurationProperty("GeneraRefBanamexMaestro")]
        public GeneraRefBanamexMaestroElement GeneraRefBanamexMaestro
        {
            get { return (GeneraRefBanamexMaestroElement)base["GeneraRefBanamexMaestro"]; }
        }

        /// <summary>
        /// Gets TblNotasMaestraOpciones configuration data
        /// </summary>
        [ConfigurationProperty("TblNotasMaestraOpciones")]
        public TblNotasMaestraOpcionesElement TblNotasMaestraOpciones
        {
            get { return (TblNotasMaestraOpcionesElement)base["TblNotasMaestraOpciones"]; }
        }

        /// <summary>
        /// Gets Sp_DameDetalleFacturaMaestra configuration data
        /// </summary>
        [ConfigurationProperty("Sp_DameDetalleFacturaMaestra")]
        public Sp_DameDetalleFacturaMaestraElement Sp_DameDetalleFacturaMaestra
        {
            get { return (Sp_DameDetalleFacturaMaestraElement)base["Sp_DameDetalleFacturaMaestra"]; }
        }

        /// <summary>
        /// Gets MuestraFacturasMaestroConsulta configuration data
        /// </summary>
        [ConfigurationProperty("MuestraFacturasMaestroConsulta")]
        public MuestraFacturasMaestroConsultaElement MuestraFacturasMaestroConsulta
        {
            get { return (MuestraFacturasMaestroConsultaElement)base["MuestraFacturasMaestroConsulta"]; }
        }

        /// <summary>
        /// Gets DetalleContratosFM configuration data
        /// </summary>
        [ConfigurationProperty("DetalleContratosFM")]
        public DetalleContratosFMElement DetalleContratosFM
        {
            get { return (DetalleContratosFMElement)base["DetalleContratosFM"]; }
        }

        /// <summary>
        /// Gets DameDetalle_FacturaporCli configuration data
        /// </summary>
        [ConfigurationProperty("DameDetalle_FacturaporCli")]
        public DameDetalle_FacturaporCliElement DameDetalle_FacturaporCli
        {
            get { return (DameDetalle_FacturaporCliElement)base["DameDetalle_FacturaporCli"]; }
        }

        /// <summary>
        /// Gets AgregaDetalleNotaDeCreditoMaestro configuration data
        /// </summary>
        [ConfigurationProperty("AgregaDetalleNotaDeCreditoMaestro")]
        public AgregaDetalleNotaDeCreditoMaestroElement AgregaDetalleNotaDeCreditoMaestro
        {
            get { return (AgregaDetalleNotaDeCreditoMaestroElement)base["AgregaDetalleNotaDeCreditoMaestro"]; }
        }


        /// <summary>
        /// Gets BuscaFacturasMaestroPendientes configuration data
        /// </summary>
        [ConfigurationProperty("BuscaFacturasMaestroPendientes")]
        public BuscaFacturasMaestroPendientesElement BuscaFacturasMaestroPendientes
        {
            get { return (BuscaFacturasMaestroPendientesElement)base["BuscaFacturasMaestroPendientes"]; }
        }

        /// <summary>
        /// Gets MuestraRelOrdenesTecnicos configuration data
        /// </summary>
        [ConfigurationProperty("MuestraRelOrdenesTecnicos")]
        public MuestraRelOrdenesTecnicosElement MuestraRelOrdenesTecnicos
        {
            get { return (MuestraRelOrdenesTecnicosElement)base["MuestraRelOrdenesTecnicos"]; }
        }

        /// <summary>
        /// Gets ValidaOrdSerManuales configuration data
        /// </summary>
        [ConfigurationProperty("ValidaOrdSerManuales")]
        public ValidaOrdSerManualesElement ValidaOrdSerManuales
        {
            get { return (ValidaOrdSerManualesElement)base["ValidaOrdSerManuales"]; }
        }

        /// <summary>
        /// Gets InsertMotCanServ configuration data
        /// </summary>
        [ConfigurationProperty("InsertMotCanServ")]
        public InsertMotCanServElement InsertMotCanServ
        {
            get { return (InsertMotCanServElement)base["InsertMotCanServ"]; }
        }





        /// <summary>
        /// Gets Encuestas configuration data
        /// </summary>
        [ConfigurationProperty("Encuestas")]
        public EncuestasElement Encuestas
        {
            get { return (EncuestasElement)base["Encuestas"]; }
        }

        /// <summary>
        /// Gets Preguntas configuration data
        /// </summary>
        [ConfigurationProperty("Preguntas")]
        public PreguntasElement Preguntas
        {
            get { return (PreguntasElement)base["Preguntas"]; }
        }

        /// <summary>
        /// Gets ResOpcMults configuration data
        /// </summary>
        [ConfigurationProperty("ResOpcMults")]
        public ResOpcMultsElement ResOpcMults
        {
            get { return (ResOpcMultsElement)base["ResOpcMults"]; }
        }

        /// <summary>
        /// Gets TipoPreguntas configuration data
        /// </summary>
        [ConfigurationProperty("TipoPreguntas")]
        public TipoPreguntasElement TipoPreguntas
        {
            get { return (TipoPreguntasElement)base["TipoPreguntas"]; }
        }

        /// <summary>
        /// Gets ProcesosEncuestas configuration data
        /// </summary>
        [ConfigurationProperty("ProcesosEncuestas")]
        public ProcesosEncuestasElement ProcesosEncuestas
        {
            get { return (ProcesosEncuestasElement)base["ProcesosEncuestas"]; }
        }

        /// <summary>
        /// Gets UniversoEncuesta configuration data
        /// </summary>
        [ConfigurationProperty("UniversoEncuesta")]
        public UniversoEncuestaElement UniversoEncuesta
        {
            get { return (UniversoEncuestaElement)base["UniversoEncuesta"]; }
        }

        /// <summary>
        /// Gets Llamadas configuration data
        /// </summary>
        [ConfigurationProperty("Llamadas")]
        public LlamadasElement Llamadas
        {
            get { return (LlamadasElement)base["Llamadas"]; }
        }

        /// <summary>
        /// Gets ClientesNoContesto configuration data
        /// </summary>
        [ConfigurationProperty("ClientesNoContesto")]
        public ClientesNoContestoElement ClientesNoContesto
        {
            get { return (ClientesNoContestoElement)base["ClientesNoContesto"]; }
        }


        /// <summary>
        /// Gets RelEncuestaClientes configuration data
        /// </summary>
        [ConfigurationProperty("RelEncuestaClientes")]
        public RelEncuestaClientesElement RelEncuestaClientes
        {
            get { return (RelEncuestaClientesElement)base["RelEncuestaClientes"]; }
        }

        /// <summary>
        /// Gets RelPreguntaOpcMults configuration data
        /// </summary>
        [ConfigurationProperty("RelPreguntaOpcMults")]
        public RelPreguntaOpcMultsElement RelPreguntaOpcMults
        {
            get { return (RelPreguntaOpcMultsElement)base["RelPreguntaOpcMults"]; }
        }

        /// <summary>
        /// Gets RelPreguntaEncuestas configuration data
        /// </summary>
        [ConfigurationProperty("RelPreguntaEncuestas")]
        public RelPreguntaEncuestasElement RelPreguntaEncuestas
        {
            get { return (RelPreguntaEncuestasElement)base["RelPreguntaEncuestas"]; }
        }

        /// <summary>
        /// Gets RelEnProcesos configuration data
        /// </summary>
        [ConfigurationProperty("RelEnProcesos")]
        public RelEnProcesosElement RelEnProcesos
        {
            get { return (RelEnProcesosElement)base["RelEnProcesos"]; }
        }

        /// <summary>
        /// Gets RelEncuestaPreguntaRes configuration data
        /// </summary>
        [ConfigurationProperty("RelEncuestaPreguntaRes")]
        public RelEncuestaPreguntaResElement RelEncuestaPreguntaRes
        {
            get { return (RelEncuestaPreguntaResElement)base["RelEncuestaPreguntaRes"]; }
        }



        /// <summary>
        /// Gets Muestra_Detalle_Bitacora_2 configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_Detalle_Bitacora_2")]
        public Muestra_Detalle_Bitacora_2Element Muestra_Detalle_Bitacora_2
        {
            get { return (Muestra_Detalle_Bitacora_2Element)base["Muestra_Detalle_Bitacora_2"]; }
        }

        /// <summary>
        /// Gets Muestra_Descripcion_Articulo_2 configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_Descripcion_Articulo_2")]
        public Muestra_Descripcion_Articulo_2Element Muestra_Descripcion_Articulo_2
        {
            get { return (Muestra_Descripcion_Articulo_2Element)base["Muestra_Descripcion_Articulo_2"]; }
        }

        /// <summary>
        /// Gets Softv_DimeSiTieneBitacora configuration data
        /// </summary>
        [ConfigurationProperty("Softv_DimeSiTieneBitacora")]
        public Softv_DimeSiTieneBitacoraElement Softv_DimeSiTieneBitacora
        {
            get { return (Softv_DimeSiTieneBitacoraElement)base["Softv_DimeSiTieneBitacora"]; }
        }

        /// <summary>
        /// Gets Softv_GetDescargaMaterialEstaPagado configuration data
        /// </summary>
        [ConfigurationProperty("Softv_GetDescargaMaterialEstaPagado")]
        public Softv_GetDescargaMaterialEstaPagadoElement Softv_GetDescargaMaterialEstaPagado
        {
            get { return (Softv_GetDescargaMaterialEstaPagadoElement)base["Softv_GetDescargaMaterialEstaPagado"]; }
        }

        /// <summary>
        /// Gets Softv_ExistenciasTecnico configuration data
        /// </summary>
        [ConfigurationProperty("Softv_ExistenciasTecnico")]
        public Softv_ExistenciasTecnicoElement Softv_ExistenciasTecnico
        {
            get { return (Softv_ExistenciasTecnicoElement)base["Softv_ExistenciasTecnico"]; }
        }

        /// <summary>
        /// Gets TblDescargaMaterial configuration data
        /// </summary>
        [ConfigurationProperty("TblDescargaMaterial")]
        public TblDescargaMaterialElement TblDescargaMaterial
        {
            get { return (TblDescargaMaterialElement)base["TblDescargaMaterial"]; }
        }


        /// <summary>
        /// Gets GetDescargaMaterialArticulosByIdClvOrden configuration data
        /// </summary>
        [ConfigurationProperty("GetDescargaMaterialArticulosByIdClvOrden")]
        public GetDescargaMaterialArticulosByIdClvOrdenElement GetDescargaMaterialArticulosByIdClvOrden
        {
            get { return (GetDescargaMaterialArticulosByIdClvOrdenElement)base["GetDescargaMaterialArticulosByIdClvOrden"]; }
        }


        /// <summary>
        /// Gets MuestraPreguntas_Encuestas configuration data
        /// </summary>
        [ConfigurationProperty("MuestraPreguntas_Encuestas")]
        public MuestraPreguntas_EncuestasElement MuestraPreguntas_Encuestas
        {
            get { return (MuestraPreguntas_EncuestasElement)base["MuestraPreguntas_Encuestas"]; }
        }

        /// <summary>
        /// Gets MuestraRespuestas_Encuestas configuration data
        /// </summary>
        [ConfigurationProperty("MuestraRespuestas_Encuestas")]
        public MuestraRespuestas_EncuestasElement MuestraRespuestas_Encuestas
        {
            get { return (MuestraRespuestas_EncuestasElement)base["MuestraRespuestas_Encuestas"]; }
        }

        /// <summary>
        /// Gets Muestra_DistribuidoresEnc configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_DistribuidoresEnc")]
        public Muestra_DistribuidoresEncElement Muestra_DistribuidoresEnc
        {
            get { return (Muestra_DistribuidoresEncElement)base["Muestra_DistribuidoresEnc"]; }
        }

        /// <summary>
        /// Gets Muestra_PlazaEnc configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_PlazaEnc")]
        public Muestra_PlazaEncElement Muestra_PlazaEnc
        {
            get { return (Muestra_PlazaEncElement)base["Muestra_PlazaEnc"]; }
        }

        /// <summary>
        /// Gets Get_UniversoEncuesta configuration data
        /// </summary>
        [ConfigurationProperty("Get_UniversoEncuesta")]
        public Get_UniversoEncuestaElement Get_UniversoEncuesta
        {
            get { return (Get_UniversoEncuestaElement)base["Get_UniversoEncuesta"]; }
        }

        /// <summary>
        /// Gets GraficasPreguntas configuration data
        /// </summary>
        [ConfigurationProperty("GraficasPreguntas")]
        public GraficasPreguntasElement GraficasPreguntas
        {
            get { return (GraficasPreguntasElement)base["GraficasPreguntas"]; }
        }

        /// <summary>
        /// Gets ConAtenTelCte configuration data
        /// </summary>
        [ConfigurationProperty("ConAtenTelCte")]
        public ConAtenTelCteElement ConAtenTelCte
        {
            get { return (ConAtenTelCteElement)base["ConAtenTelCte"]; }
        }

        /// <summary>
        /// Gets SP_InsertaTbl_NoEntregados configuration data
        /// </summary>
        [ConfigurationProperty("SP_InsertaTbl_NoEntregados")]
        public SP_InsertaTbl_NoEntregadosElement SP_InsertaTbl_NoEntregados
        {
            get { return (SP_InsertaTbl_NoEntregadosElement)base["SP_InsertaTbl_NoEntregados"]; }
        }

        /// <summary>
        /// Gets TipoDeCambio configuration data
        /// </summary>
        [ConfigurationProperty("TipoDeCambio")]
        public TipoDeCambioElement TipoDeCambio
        {
            get { return (TipoDeCambioElement)base["TipoDeCambio"]; }
        }

        /// <summary>
        /// Gets ContratosPorSaldarMaestro configuration data
        /// </summary>
        [ConfigurationProperty("ContratosPorSaldarMaestro")]
        public ContratosPorSaldarMaestroElement ContratosPorSaldarMaestro
        {
            get { return (ContratosPorSaldarMaestroElement)base["ContratosPorSaldarMaestro"]; }
        }

        /// <summary>
        /// Gets ContratosSaldadosMaestro configuration data
        /// </summary>
        [ConfigurationProperty("ContratosSaldadosMaestro")]
        public ContratosSaldadosMaestroElement ContratosSaldadosMaestro
        {
            get { return (ContratosSaldadosMaestroElement)base["ContratosSaldadosMaestro"]; }
        }

        /// <summary>
        /// Gets ResumenFacturasDolares configuration data
        /// </summary>
        [ConfigurationProperty("ResumenFacturasDolares")]
        public ResumenFacturasDolaresElement ResumenFacturasDolares
        {
            get { return (ResumenFacturasDolaresElement)base["ResumenFacturasDolares"]; }
        }

        /// <summary>
        /// Gets ValidarNuevo configuration data
        /// </summary>
        [ConfigurationProperty("ValidarNuevo")]
        public ValidarNuevoElement ValidarNuevo
        {
            get { return (ValidarNuevoElement)base["ValidarNuevo"]; }
        }

        /// <summary>
        /// Gets BuscaSiTieneQueja configuration data
        /// </summary>
        [ConfigurationProperty("BuscaSiTieneQueja")]
        public BuscaSiTieneQuejaElement BuscaSiTieneQueja
        {
            get { return (BuscaSiTieneQuejaElement)base["BuscaSiTieneQueja"]; }
        }

        /// <summary>
        /// Gets ComisionesTecnicosWeb configuration data
        /// </summary>
        [ConfigurationProperty("ComisionesTecnicosWeb")]
        public ComisionesTecnicosWebElement ComisionesTecnicosWeb
        {
            get { return (ComisionesTecnicosWebElement)base["ComisionesTecnicosWeb"]; }
        }

        /// <summary>
        /// Gets ComisionesVendedoresWeb configuration data
        /// </summary>
        [ConfigurationProperty("ComisionesVendedoresWeb")]
        public ComisionesVendedoresWebElement ComisionesVendedoresWeb
        {
            get { return (ComisionesVendedoresWebElement)base["ComisionesVendedoresWeb"]; }
        }

        /// <summary>
        /// Gets sp_verificaCoordenadas configuration data
        /// </summary>
        [ConfigurationProperty("sp_verificaCoordenadas")]
        public sp_verificaCoordenadasElement sp_verificaCoordenadas
        {
            get { return (sp_verificaCoordenadasElement)base["sp_verificaCoordenadas"]; }
        }

        /// <summary>
        /// Gets sp_verificaBimProveedor configuration data
        /// </summary>
        [ConfigurationProperty("sp_verificaBimProveedor")]
        public sp_verificaBimProveedorElement sp_verificaBimProveedor
        {
            get { return (sp_verificaBimProveedorElement)base["sp_verificaBimProveedor"]; }
        }

        /// <summary>
        /// Gets NueRelClienteTab configuration data
        /// </summary>
        [ConfigurationProperty("NueRelClienteTab")]
        public NueRelClienteTabElement NueRelClienteTab
        {
            get { return (NueRelClienteTabElement)base["NueRelClienteTab"]; }
        }

        /// <summary>
        /// Gets ServiciosWeb configuration data
        /// </summary>
        [ConfigurationProperty("ServiciosWeb")]
        public ServiciosWebElement ServiciosWeb
        {
            get { return (ServiciosWebElement)base["ServiciosWeb"]; }
        }


        /// <summary>
        /// Gets ValidaTrabajos configuration data
        /// </summary>
        [ConfigurationProperty("ValidaTrabajos")]
        public ValidaTrabajosElement ValidaTrabajos
        {
            get { return (ValidaTrabajosElement)base["ValidaTrabajos"]; }
        }

        /// <summary>
        /// Gets ValidaInstalacionCorrecta configuration data
        /// </summary>
        [ConfigurationProperty("ValidaInstalacionCorrecta")]
        public ValidaInstalacionCorrectaElement ValidaInstalacionCorrecta
        {
            get { return (ValidaInstalacionCorrectaElement)base["ValidaInstalacionCorrecta"]; }
        }

        /// <summary>
        /// Gets Plaza_ReportesVentas configuration data
        /// </summary>
        [ConfigurationProperty("Plaza_ReportesVentas")]
        public Plaza_ReportesVentasElement Plaza_ReportesVentas
        {
            get { return (Plaza_ReportesVentasElement)base["Plaza_ReportesVentas"]; }
        }

        /// <summary>
        /// Gets Muestra_PlazasPorUsuario configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_PlazasPorUsuario")]
        public Muestra_PlazasPorUsuarioElement Muestra_PlazasPorUsuario
        {
            get { return (Muestra_PlazasPorUsuarioElement)base["Muestra_PlazasPorUsuario"]; }
        }

        /// <summary>
        /// Gets Vendores_ReportesVentas configuration data
        /// </summary>
        [ConfigurationProperty("Vendores_ReportesVentas")]
        public Vendores_ReportesVentasElement Vendores_ReportesVentas
        {
            get { return (Vendores_ReportesVentasElement)base["Vendores_ReportesVentas"]; }
        }

        /// <summary>
        /// Gets Sucursales_ReportesVentas configuration data
        /// </summary>
        [ConfigurationProperty("Sucursales_ReportesVentas")]
        public Sucursales_ReportesVentasElement Sucursales_ReportesVentas
        {
            get { return (Sucursales_ReportesVentasElement)base["Sucursales_ReportesVentas"]; }
        }

        /// <summary>
        /// Gets Graficas_RepVentas configuration data
        /// </summary>
        [ConfigurationProperty("Graficas_RepVentas")]
        public Graficas_RepVentasElement Graficas_RepVentas
        {
            get { return (Graficas_RepVentasElement)base["Graficas_RepVentas"]; }
        }

        /// <summary>
        /// Gets RepGralComisiones_RepVentas configuration data
        /// </summary>
        [ConfigurationProperty("RepGralComisiones_RepVentas")]
        public RepGralComisiones_RepVentasElement RepGralComisiones_RepVentas
        {
            get { return (RepGralComisiones_RepVentasElement)base["RepGralComisiones_RepVentas"]; }
        }

        /// <summary>
        /// Gets RepGral_StatusVentas configuration data
        /// </summary>
        [ConfigurationProperty("RepGral_StatusVentas")]
        public RepGral_StatusVentasElement RepGral_StatusVentas
        {
            get { return (RepGral_StatusVentasElement)base["RepGral_StatusVentas"]; }
        }

        /// <summary>
        /// Gets Calle configuration data
        /// </summary>
        [ConfigurationProperty("Calle")]
        public CalleElement Calle
        {
            get { return (CalleElement)base["Calle"]; }
        }

        /// <summary>
        /// Gets Colonia configuration data
        /// </summary>
        [ConfigurationProperty("Colonia")]
        public ColoniaElement Colonia
        {
            get { return (ColoniaElement)base["Colonia"]; }
        }

        /// <summary>
        /// Gets Localidad configuration data
        /// </summary>
        [ConfigurationProperty("Localidad")]
        public LocalidadElement Localidad
        {
            get { return (LocalidadElement)base["Localidad"]; }
        }

        /// <summary>
        /// Gets Municipio configuration data
        /// </summary>
        [ConfigurationProperty("Municipio")]
        public MunicipioElement Municipio
        {
            get { return (MunicipioElement)base["Municipio"]; }
        }

        /// <summary>
        /// Gets RelCalleColonia configuration data
        /// </summary>
        [ConfigurationProperty("RelCalleColonia")]
        public RelCalleColoniaElement RelCalleColonia
        {
            get { return (RelCalleColoniaElement)base["RelCalleColonia"]; }
        }

        /// <summary>
        /// Gets RelColoniaLocMunEst configuration data
        /// </summary>
        [ConfigurationProperty("RelColoniaLocMunEst")]
        public RelColoniaLocMunEstElement RelColoniaLocMunEst
        {
            get { return (RelColoniaLocMunEstElement)base["RelColoniaLocMunEst"]; }
        }

        /// <summary>
        /// Gets RelLocalidadMunEst configuration data
        /// </summary>
        [ConfigurationProperty("RelLocalidadMunEst")]
        public RelLocalidadMunEstElement RelLocalidadMunEst
        {
            get { return (RelLocalidadMunEstElement)base["RelLocalidadMunEst"]; }
        }

        /// <summary>
        /// Gets RelMunicipioEst configuration data
        /// </summary>
        [ConfigurationProperty("RelMunicipioEst")]
        public RelMunicipioEstElement RelMunicipioEst
        {
            get { return (RelMunicipioEstElement)base["RelMunicipioEst"]; }
        }

        /// <summary>
        /// Gets Cliente configuration data
        /// </summary>
        [ConfigurationProperty("Cliente")]
        public ClienteElement Cliente
        {
            get { return (ClienteElement)base["Cliente"]; }
        }

        /// <summary>
        /// Gets TipoColonia configuration data
        /// </summary>
        [ConfigurationProperty("TipoColonia")]
        public TipoColoniaElement TipoColonia
        {
            get { return (TipoColoniaElement)base["TipoColonia"]; }
        }

        /// <summary>
        /// Gets Distribuidor configuration data
        /// </summary>
        [ConfigurationProperty("Distribuidor")]
        public DistribuidorElement Distribuidor
        {
            get { return (DistribuidorElement)base["Distribuidor"]; }
        }

        /// <summary>
        /// Gets Plaza configuration data
        /// </summary>
        [ConfigurationProperty("Plaza")]
        public PlazaElement Plaza
        {
            get { return (PlazaElement)base["Plaza"]; }
        }


        /// <summary>
        /// Gets TipoCliente configuration data
        /// </summary>
        [ConfigurationProperty("TipoCliente")]
        public TipoClienteElement TipoCliente
        {
            get { return (TipoClienteElement)base["TipoCliente"]; }
        }

        /// <summary>
        /// Gets RelColoniaServicio configuration data
        /// </summary>
        [ConfigurationProperty("RelColoniaServicio")]
        public RelColoniaServicioElement RelColoniaServicio
        {
            get { return (RelColoniaServicioElement)base["RelColoniaServicio"]; }
        }
        /// <summary>
        /// Gets ReferenciaCliente configuration data
        /// </summary>
        [ConfigurationProperty("ReferenciaCliente")]
        public ReferenciaClienteElement ReferenciaCliente
        {
            get { return (ReferenciaClienteElement)base["ReferenciaCliente"]; }
        }

        /// <summary>
        /// Gets RoboDeSenal configuration data
        /// </summary>
        [ConfigurationProperty("RoboDeSenal")]
        public RoboDeSenalElement RoboDeSenal
        {
            get { return (RoboDeSenalElement)base["RoboDeSenal"]; }
        }

        /// <summary>
        /// Gets ObservacionCliente configuration data
        /// </summary>
        [ConfigurationProperty("ObservacionCliente")]
        public ObservacionClienteElement ObservacionCliente
        {
            get { return (ObservacionClienteElement)base["ObservacionCliente"]; }
        }

        /// <summary>
        /// Gets DatoBancario configuration data
        /// </summary>
        [ConfigurationProperty("DatoBancario")]
        public DatoBancarioElement DatoBancario
        {
            get { return (DatoBancarioElement)base["DatoBancario"]; }
        }

        /// <summary>
        /// Gets DatoFiscal configuration data
        /// </summary>
        [ConfigurationProperty("DatoFiscal")]
        public DatoFiscalElement DatoFiscal
        {
            get { return (DatoFiscalElement)base["DatoFiscal"]; }
        }

        /// <summary>
        /// Gets TipoTarjeta configuration data
        /// </summary>
        [ConfigurationProperty("TipoTarjeta")]
        public TipoTarjetaElement TipoTarjeta
        {
            get { return (TipoTarjetaElement)base["TipoTarjeta"]; }
        }

        /// <summary>
        /// Gets RelTarifadosServicios configuration data
        /// </summary>
        [ConfigurationProperty("RelTarifadosServicios")]
        public RelTarifadosServiciosElement RelTarifadosServicios
        {
            get { return (RelTarifadosServiciosElement)base["RelTarifadosServicios"]; }
        }


        /// <summary>
        /// Gets RelPlazaEstMun configuration data
        /// </summary>
        [ConfigurationProperty("RelPlazaEstMun")]
        public RelPlazaEstMunElement RelPlazaEstMun
        {
            get { return (RelPlazaEstMunElement)base["RelPlazaEstMun"]; }
        }


        /// <summary>
        /// Gets PeriodoCobro configuration data
        /// </summary>
        [ConfigurationProperty("PeriodoCobro")]
        public PeriodoCobroElement PeriodoCobro
        {
            get { return (PeriodoCobroElement)base["PeriodoCobro"]; }
        }


        /// <summary>
        /// Gets Banco configuration data
        /// </summary>
        [ConfigurationProperty("Banco")]
        public BancoElement Banco
        {
            get { return (BancoElement)base["Banco"]; }
        }

        /// <summary>
        /// Gets ListadoNotas configuration data
        /// </summary>
        [ConfigurationProperty("ListadoNotas")]
        public ListadoNotasElement ListadoNotas
        {
            get { return (ListadoNotasElement)base["ListadoNotas"]; }
        }


        /// <summary>
        /// Gets MuestraMedioPorServicoContratado configuration data
        /// </summary>
        [ConfigurationProperty("MuestraMedioPorServicoContratado")]
        public MuestraMedioPorServicoContratadoElement MuestraMedioPorServicoContratado
        {
            get { return (MuestraMedioPorServicoContratadoElement)base["MuestraMedioPorServicoContratado"]; }
        }

        /// <summary>
        /// Gets Medio_Softv configuration data
        /// </summary>
        [ConfigurationProperty("Medio_Softv")]
        public Medio_SoftvElement Medio_Softv
        {
            get { return (Medio_SoftvElement)base["Medio_Softv"]; }
        }

        /// <summary>
        /// Gets RelUnicanetMedio configuration data
        /// </summary>
        [ConfigurationProperty("RelUnicanetMedio")]
        public RelUnicanetMedioElement RelUnicanetMedio
        {
            get { return (RelUnicanetMedioElement)base["RelUnicanetMedio"]; }
        }

        /// <summary>
        /// Gets MuestraArbolServiciosAparatosPorinstalar configuration data
        /// </summary>
        [ConfigurationProperty("MuestraArbolServiciosAparatosPorinstalar")]
        public MuestraArbolServiciosAparatosPorinstalarElement MuestraArbolServiciosAparatosPorinstalar
        {
            get { return (MuestraArbolServiciosAparatosPorinstalarElement)base["MuestraArbolServiciosAparatosPorinstalar"]; }
        }

        /// <summary>
        /// Gets MuestraTipoAparato configuration data
        /// </summary>
        [ConfigurationProperty("MuestraTipoAparato")]
        public MuestraTipoAparatoElement MuestraTipoAparato
        {
            get { return (MuestraTipoAparatoElement)base["MuestraTipoAparato"]; }
        }

        /// <summary>
        /// Gets MuestraServiciosRelTipoAparato configuration data
        /// </summary>
        [ConfigurationProperty("MuestraServiciosRelTipoAparato")]
        public MuestraServiciosRelTipoAparatoElement MuestraServiciosRelTipoAparato
        {
            get { return (MuestraServiciosRelTipoAparatoElement)base["MuestraServiciosRelTipoAparato"]; }
        }

        /// <summary>
        /// Gets MuestraAparatosDisponibles configuration data
        /// </summary>
        [ConfigurationProperty("MuestraAparatosDisponibles")]
        public MuestraAparatosDisponiblesElement MuestraAparatosDisponibles
        {
            get { return (MuestraAparatosDisponiblesElement)base["MuestraAparatosDisponibles"]; }
        }

        /// <summary>
        /// Gets MuestraEstadosCompania configuration data
        /// </summary>
        [ConfigurationProperty("MuestraEstadosCompania")]
        public MuestraEstadosCompaniaElement MuestraEstadosCompania
        {
            get { return (MuestraEstadosCompaniaElement)base["MuestraEstadosCompania"]; }
        }

        /// <summary>
        /// Gets MuestraCiudadesEstado configuration data
        /// </summary>
        [ConfigurationProperty("MuestraCiudadesEstado")]
        public MuestraCiudadesEstadoElement MuestraCiudadesEstado
        {
            get { return (MuestraCiudadesEstadoElement)base["MuestraCiudadesEstado"]; }
        }

        /// <summary>
        /// Gets MuestraLocalidadCiudad configuration data
        /// </summary>
        [ConfigurationProperty("MuestraLocalidadCiudad")]
        public MuestraLocalidadCiudadElement MuestraLocalidadCiudad
        {
            get { return (MuestraLocalidadCiudadElement)base["MuestraLocalidadCiudad"]; }
        }

        /// <summary>
        /// Gets MuestraColoniaLocalidad configuration data
        /// </summary>
        [ConfigurationProperty("MuestraColoniaLocalidad")]
        public MuestraColoniaLocalidadElement MuestraColoniaLocalidad
        {
            get { return (MuestraColoniaLocalidadElement)base["MuestraColoniaLocalidad"]; }
        }

        /// <summary>
        /// Gets MuestraCalleColonia configuration data
        /// </summary>
        [ConfigurationProperty("MuestraCalleColonia")]
        public MuestraCalleColoniaElement MuestraCalleColonia
        {
            get { return (MuestraCalleColoniaElement)base["MuestraCalleColonia"]; }
        }

        /// <summary>
        /// Gets muestraCP_ColoniaLocalidad configuration data
        /// </summary>
        [ConfigurationProperty("muestraCP_ColoniaLocalidad")]
        public muestraCP_ColoniaLocalidadElement muestraCP_ColoniaLocalidad
        {
            get { return (muestraCP_ColoniaLocalidadElement)base["muestraCP_ColoniaLocalidad"]; }
        }

        /// <summary>
        /// Gets AsignaAparatosAlServicio configuration data
        /// </summary>
        [ConfigurationProperty("AsignaAparatosAlServicio")]
        public AsignaAparatosAlServicioElement AsignaAparatosAlServicio
        {
            get { return (AsignaAparatosAlServicioElement)base["AsignaAparatosAlServicio"]; }
        }

        /// <summary>
        /// Gets CatalogoCajas configuration data
        /// </summary>
        [ConfigurationProperty("CatalogoCajas")]
        public CatalogoCajasElement CatalogoCajas
        {
            get { return (CatalogoCajasElement)base["CatalogoCajas"]; }
        }

        /// <summary>
        /// Gets SUCURSALES configuration data
        /// </summary>
        [ConfigurationProperty("SUCURSALES")]
        public SUCURSALESElement SUCURSALES
        {
            get { return (SUCURSALESElement)base["SUCURSALES"]; }
        }


        /// <summary>
        /// Gets CLIENTES_New configuration data
        /// </summary>
        [ConfigurationProperty("CLIENTES_New")]
        public CLIENTES_NewElement CLIENTES_New
        {
            get { return (CLIENTES_NewElement)base["CLIENTES_New"]; }
        }

        /// <summary>
        /// Gets DatosFiscales configuration data
        /// </summary>
        [ConfigurationProperty("DatosFiscales")]
        public DatosFiscalesElement DatosFiscales
        {
            get { return (DatosFiscalesElement)base["DatosFiscales"]; }
        }

        /// <summary>
        /// Gets RELCLIBANCO configuration data
        /// </summary>
        [ConfigurationProperty("RELCLIBANCO")]
        public RELCLIBANCOElement RELCLIBANCO
        {
            get { return (RELCLIBANCOElement)base["RELCLIBANCO"]; }
        }

        /// <summary>
        /// Gets MUESTRATIPOSDECUENTA configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRATIPOSDECUENTA")]
        public MUESTRATIPOSDECUENTAElement MUESTRATIPOSDECUENTA
        {
            get { return (MUESTRATIPOSDECUENTAElement)base["MUESTRATIPOSDECUENTA"]; }
        }

        /// <summary>
        /// Gets MUESTRABANCOS_DatosBancarios configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRABANCOS_DatosBancarios")]
        public MUESTRABANCOS_DatosBancariosElement MUESTRABANCOS_DatosBancarios
        {
            get { return (MUESTRABANCOS_DatosBancariosElement)base["MUESTRABANCOS_DatosBancarios"]; }
        }

        /// <summary>
        /// Gets tblReferenciasClietes configuration data
        /// </summary>
        [ConfigurationProperty("tblReferenciasClietes")]
        public tblReferenciasClietesElement tblReferenciasClietes
        {
            get { return (tblReferenciasClietesElement)base["tblReferenciasClietes"]; }
        }

        /// <summary>
        /// Gets RELCLIENTEOBS configuration data
        /// </summary>
        [ConfigurationProperty("RELCLIENTEOBS")]
        public RELCLIENTEOBSElement RELCLIENTEOBS
        {
            get { return (RELCLIENTEOBSElement)base["RELCLIENTEOBS"]; }
        }

        /// <summary>
        /// Gets RoboDeSeñal_New configuration data
        /// </summary>
        [ConfigurationProperty("RoboDeSeñal_New")]
        public RoboDeSeñal_NewElement RoboDeSeñal_New
        {
            get { return (RoboDeSeñal_NewElement)base["RoboDeSeñal_New"]; }
        }

        /// <summary>
        /// Gets TipServ_New configuration data
        /// </summary>
        [ConfigurationProperty("TipServ_New")]
        public TipServ_NewElement TipServ_New
        {
            get { return (TipServ_NewElement)base["TipServ_New"]; }
        }

        /// <summary>
        /// Gets Tipo_Colonias1_New configuration data
        /// </summary>
        [ConfigurationProperty("Tipo_Colonias1_New")]
        public Tipo_Colonias1_NewElement Tipo_Colonias1_New
        {
            get { return (Tipo_Colonias1_NewElement)base["Tipo_Colonias1_New"]; }
        }

        /// <summary>
        /// Gets Estados_New configuration data
        /// </summary>
        [ConfigurationProperty("Estados_New")]
        public Estados_NewElement Estados_New
        {
            get { return (Estados_NewElement)base["Estados_New"]; }
        }


        /// <summary>
        /// Gets Ciudades_New configuration data
        /// </summary>
        [ConfigurationProperty("Ciudades_New")]
        public Ciudades_NewElement Ciudades_New
        {
            get { return (Ciudades_NewElement)base["Ciudades_New"]; }
        }

        /// <summary>
        /// Gets RelEstadoCiudad_New configuration data
        /// </summary>
        [ConfigurationProperty("RelEstadoCiudad_New")]
        public RelEstadoCiudad_NewElement RelEstadoCiudad_New
        {
            get { return (RelEstadoCiudad_NewElement)base["RelEstadoCiudad_New"]; }
        }


        [ConfigurationProperty("Configuracion")]
        public ConfiguracionElement Configuracion
        {
            get { return (ConfiguracionElement)base["Configuracion"]; }
        }


        /// <summary>
        /// Gets Procesos configuration data
        /// </summary>
        [ConfigurationProperty("Procesos")]
        public ProcesosElement Procesos
        {
            get { return (ProcesosElement)base["Procesos"]; }
        }

        /// <summary>
        /// Gets MuestraTipSerPrincipal_SER configuration data
        /// </summary>
        [ConfigurationProperty("MuestraTipSerPrincipal_SER")]
        public MuestraTipSerPrincipal_SERElement MuestraTipSerPrincipal_SER
        {
            get { return (MuestraTipSerPrincipal_SERElement)base["MuestraTipSerPrincipal_SER"]; }
        }

        /// <summary>
        /// Gets Servicios_New configuration data
        /// </summary>
        [ConfigurationProperty("Servicios_New")]
        public Servicios_NewElement Servicios_New
        {
            get { return (Servicios_NewElement)base["Servicios_New"]; }
        }

        /// <summary>
        /// Gets BORRel_Trabajos_NoCobroMensual configuration data
        /// </summary>
        [ConfigurationProperty("BORRel_Trabajos_NoCobroMensual")]
        public BORRel_Trabajos_NoCobroMensualElement BORRel_Trabajos_NoCobroMensual
        {
            get { return (BORRel_Trabajos_NoCobroMensualElement)base["BORRel_Trabajos_NoCobroMensual"]; }
        }

        /// <summary>
        /// Gets GUARDARel_Trabajos_NoCobroMensual configuration data
        /// </summary>
        [ConfigurationProperty("GUARDARel_Trabajos_NoCobroMensual")]
        public GUARDARel_Trabajos_NoCobroMensualElement GUARDARel_Trabajos_NoCobroMensual
        {
            get { return (GUARDARel_Trabajos_NoCobroMensualElement)base["GUARDARel_Trabajos_NoCobroMensual"]; }
        }

        /// <summary>
        /// Gets NUEVOClv_Equi configuration data
        /// </summary>
        [ConfigurationProperty("NUEVOClv_Equi")]
        public NUEVOClv_EquiElement NUEVOClv_Equi
        {
            get { return (NUEVOClv_EquiElement)base["NUEVOClv_Equi"]; }
        }

        /// <summary>
        /// Gets ValidaAplicaSoloInternet configuration data
        /// </summary>
        [ConfigurationProperty("ValidaAplicaSoloInternet")]
        public ValidaAplicaSoloInternetElement ValidaAplicaSoloInternet
        {
            get { return (ValidaAplicaSoloInternetElement)base["ValidaAplicaSoloInternet"]; }
        }

        /// <summary>
        /// Gets NueAplicaSoloInternet configuration data
        /// </summary>
        [ConfigurationProperty("NueAplicaSoloInternet")]
        public NueAplicaSoloInternetElement NueAplicaSoloInternet
        {
            get { return (NueAplicaSoloInternetElement)base["NueAplicaSoloInternet"]; }
        }

        /// <summary>
        /// Gets BorAplicaSoloInternet configuration data
        /// </summary>
        [ConfigurationProperty("BorAplicaSoloInternet")]
        public BorAplicaSoloInternetElement BorAplicaSoloInternet
        {
            get { return (BorAplicaSoloInternetElement)base["BorAplicaSoloInternet"]; }
        }

        /// <summary>
        /// Gets Valida_borra_servicio_New configuration data
        /// </summary>
        [ConfigurationProperty("Valida_borra_servicio_New")]
        public Valida_borra_servicio_NewElement Valida_borra_servicio_New
        {
            get { return (Valida_borra_servicio_NewElement)base["Valida_borra_servicio_New"]; }
        }


        /// <summary>
        /// Gets ValidaCambioDClvtxtServ configuration data
        /// </summary>
        [ConfigurationProperty("ValidaCambioDClvtxtServ")]
        public ValidaCambioDClvtxtServElement ValidaCambioDClvtxtServ
        {
            get { return (ValidaCambioDClvtxtServElement)base["ValidaCambioDClvtxtServ"]; }
        }


        /// <summary>
        /// Gets MUESTRASOLOTARIFADOS configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRASOLOTARIFADOS")]
        public MUESTRASOLOTARIFADOSElement MUESTRASOLOTARIFADOS
        {
            get { return (MUESTRASOLOTARIFADOSElement)base["MUESTRASOLOTARIFADOS"]; }
        }

        /// <summary>
        /// Gets MUESTRATRABAJOS_New configuration data
        /// </summary>
        [ConfigurationProperty("MUESTRATRABAJOS_New")]
        public MUESTRATRABAJOS_NewElement MUESTRATRABAJOS_New
        {
            get { return (MUESTRATRABAJOS_NewElement)base["MUESTRATRABAJOS_New"]; }
        }

        /// <summary>
        /// Gets MuestraTipoPromocion configuration data
        /// </summary>
        [ConfigurationProperty("MuestraTipoPromocion")]
        public MuestraTipoPromocionElement MuestraTipoPromocion
        {
            get { return (MuestraTipoPromocionElement)base["MuestraTipoPromocion"]; }
        }

        /// <summary>
        /// Gets ValidaPeriodos configuration data
        /// </summary>
        [ConfigurationProperty("ValidaPeriodos")]
        public ValidaPeriodosElement ValidaPeriodos
        {
            get { return (ValidaPeriodosElement)base["ValidaPeriodos"]; }
        }

        /// <summary>
        /// Gets REL_TARIFADOS_SERVICIOS_New configuration data
        /// </summary>
        [ConfigurationProperty("REL_TARIFADOS_SERVICIOS_New")]
        public REL_TARIFADOS_SERVICIOS_NewElement REL_TARIFADOS_SERVICIOS_New
        {
            get { return (REL_TARIFADOS_SERVICIOS_NewElement)base["REL_TARIFADOS_SERVICIOS_New"]; }
        }


        /// <summary>
        /// Gets RelTarifadosServiciosCostoPorCaja_New configuration data
        /// </summary>
        [ConfigurationProperty("RelTarifadosServiciosCostoPorCaja_New")]
        public RelTarifadosServiciosCostoPorCaja_NewElement RelTarifadosServiciosCostoPorCaja_New
        {
            get { return (RelTarifadosServiciosCostoPorCaja_NewElement)base["RelTarifadosServiciosCostoPorCaja_New"]; }
        }

        /// <summary>
        /// Gets NUEPuntos_Pago_Adelantado configuration data
        /// </summary>
        [ConfigurationProperty("NUEPuntos_Pago_Adelantado")]
        public NUEPuntos_Pago_AdelantadoElement NUEPuntos_Pago_Adelantado
        {
            get { return (NUEPuntos_Pago_AdelantadoElement)base["NUEPuntos_Pago_Adelantado"]; }
        }

        /// <summary>
        /// Gets ModRentaAparato configuration data
        /// </summary>
        [ConfigurationProperty("ModRentaAparato")]
        public ModRentaAparatoElement ModRentaAparato
        {
            get { return (ModRentaAparatoElement)base["ModRentaAparato"]; }
        }

        /// <summary>
        /// Gets Actualiza_Instalacion configuration data
        /// </summary>
        [ConfigurationProperty("Actualiza_Instalacion")]
        public Actualiza_InstalacionElement Actualiza_Instalacion
        {
            get { return (Actualiza_InstalacionElement)base["Actualiza_Instalacion"]; }
        }

        /// <summary>
        /// Gets ValidaEliminaClvLlave configuration data
        /// </summary>
        [ConfigurationProperty("ValidaEliminaClvLlave")]
        public ValidaEliminaClvLlaveElement ValidaEliminaClvLlave
        {
            get { return (ValidaEliminaClvLlaveElement)base["ValidaEliminaClvLlave"]; }
        }

        /// <summary>
        /// Gets Muestra_Plazas_ConfiguracionServicios configuration data
        /// </summary>
        [ConfigurationProperty("Muestra_Plazas_ConfiguracionServicios")]
        public Muestra_Plazas_ConfiguracionServiciosElement Muestra_Plazas_ConfiguracionServicios
        {
            get { return (Muestra_Plazas_ConfiguracionServiciosElement)base["Muestra_Plazas_ConfiguracionServicios"]; }
        }

        /// <summary>
        /// Gets DameRelCompaniaEstadoCiudad configuration data
        /// </summary>
        [ConfigurationProperty("DameRelCompaniaEstadoCiudad")]
        public DameRelCompaniaEstadoCiudadElement DameRelCompaniaEstadoCiudad
        {
            get { return (DameRelCompaniaEstadoCiudadElement)base["DameRelCompaniaEstadoCiudad"]; }
        }

        /// <summary>
        /// Gets DameServiciosRelComEdoCd_PorServicio1_New configuration data
        /// </summary>
        [ConfigurationProperty("DameServiciosRelComEdoCd_PorServicio1_New")]
        public DameServiciosRelComEdoCd_PorServicio1_NewElement DameServiciosRelComEdoCd_PorServicio1_New
        {
            get { return (DameServiciosRelComEdoCd_PorServicio1_NewElement)base["DameServiciosRelComEdoCd_PorServicio1_New"]; }
        }

        /// <summary>
        /// Gets insertaServiciosRelCompaniaEstadoCiudad configuration data
        /// </summary>
        [ConfigurationProperty("insertaServiciosRelCompaniaEstadoCiudad")]
        public insertaServiciosRelCompaniaEstadoCiudadElement insertaServiciosRelCompaniaEstadoCiudad
        {
            get { return (insertaServiciosRelCompaniaEstadoCiudadElement)base["insertaServiciosRelCompaniaEstadoCiudad"]; }
        }

        /// <summary>
        /// Gets insertaServiciosRelCompaniaEstadoCiudadATodos configuration data
        /// </summary>
        [ConfigurationProperty("insertaServiciosRelCompaniaEstadoCiudadATodos")]
        public insertaServiciosRelCompaniaEstadoCiudadATodosElement insertaServiciosRelCompaniaEstadoCiudadATodos
        {
            get { return (insertaServiciosRelCompaniaEstadoCiudadATodosElement)base["insertaServiciosRelCompaniaEstadoCiudadATodos"]; }
        }

        /// <summary>
        /// Gets ValidaNombreLocalidad configuration data
        /// </summary>
        [ConfigurationProperty("ValidaNombreLocalidad")]
        public ValidaNombreLocalidadElement ValidaNombreLocalidad
        {
            get { return (ValidaNombreLocalidadElement)base["ValidaNombreLocalidad"]; }
        }

        /// <summary>
        /// Gets Localidades_New configuration data
        /// </summary>
        [ConfigurationProperty("Localidades_New")]
        public Localidades_NewElement Localidades_New
        {
            get { return (Localidades_NewElement)base["Localidades_New"]; }
        }

        /// <summary>
        /// Gets SPRelCiudadLocalidad configuration data
        /// </summary>
        [ConfigurationProperty("SPRelCiudadLocalidad")]
        public SPRelCiudadLocalidadElement SPRelCiudadLocalidad
        {
            get { return (SPRelCiudadLocalidadElement)base["SPRelCiudadLocalidad"]; }
        }

        /// <summary>
        /// Gets ValidaEliminaRelLocalidadCiudad configuration data
        /// </summary>
        [ConfigurationProperty("ValidaEliminaRelLocalidadCiudad")]
        public ValidaEliminaRelLocalidadCiudadElement ValidaEliminaRelLocalidadCiudad
        {
            get { return (ValidaEliminaRelLocalidadCiudadElement)base["ValidaEliminaRelLocalidadCiudad"]; }
        }

        /// <summary>
        /// Gets Plaza_DistribuidoresNew configuration data
        /// </summary>
        [ConfigurationProperty("Plaza_DistribuidoresNew")]
        public Plaza_DistribuidoresNewElement Plaza_DistribuidoresNew
        {
            get { return (Plaza_DistribuidoresNewElement)base["Plaza_DistribuidoresNew"]; }
        }

        /// <summary>
        /// Gets MuestraServiciosDelCli_porOpcion configuration data
        /// </summary>
        [ConfigurationProperty("MuestraServiciosDelCli_porOpcion")]
        public MuestraServiciosDelCli_porOpcionElement MuestraServiciosDelCli_porOpcion
        {
            get { return (MuestraServiciosDelCli_porOpcionElement)base["MuestraServiciosDelCli_porOpcion"]; }
        }

        /// <summary>
        /// Gets Colonias_New configuration data
        /// </summary>
        [ConfigurationProperty("Colonias_New")]
        public Colonias_NewElement Colonias_New
        {
            get { return (Colonias_NewElement)base["Colonias_New"]; }
        }

        /// <summary>
        /// Gets ValidaNombreColonia configuration data
        /// </summary>
        [ConfigurationProperty("ValidaNombreColonia")]
        public ValidaNombreColoniaElement ValidaNombreColonia
        {
            get { return (ValidaNombreColoniaElement)base["ValidaNombreColonia"]; }
        }

        /// <summary>
        /// Gets MuestraEstados_RelCol configuration data
        /// </summary>
        [ConfigurationProperty("MuestraEstados_RelCol")]
        public MuestraEstados_RelColElement MuestraEstados_RelCol
        {
            get { return (MuestraEstados_RelColElement)base["MuestraEstados_RelCol"]; }
        }

        /// <summary>
        /// Gets MuestraCdsEdo_RelColonia configuration data
        /// </summary>
        [ConfigurationProperty("MuestraCdsEdo_RelColonia")]
        public MuestraCdsEdo_RelColoniaElement MuestraCdsEdo_RelColonia
        {
            get { return (MuestraCdsEdo_RelColoniaElement)base["MuestraCdsEdo_RelColonia"]; }
        }

        /// <summary>
        /// Gets MuestraLoc_RelColonia configuration data
        /// </summary>
        [ConfigurationProperty("MuestraLoc_RelColonia")]
        public MuestraLoc_RelColoniaElement MuestraLoc_RelColonia
        {
            get { return (MuestraLoc_RelColoniaElement)base["MuestraLoc_RelColonia"]; }
        }

        /// <summary>
        /// Gets InsertaRelColoniaLocalidad configuration data
        /// </summary>
        [ConfigurationProperty("InsertaRelColoniaLocalidad")]
        public InsertaRelColoniaLocalidadElement InsertaRelColoniaLocalidad
        {
            get { return (InsertaRelColoniaLocalidadElement)base["InsertaRelColoniaLocalidad"]; }
        }

        /// <summary>
        /// Gets ValidaCVELOCCOL configuration data
        /// </summary>
        [ConfigurationProperty("ValidaCVELOCCOL")]
        public ValidaCVELOCCOLElement ValidaCVELOCCOL
        {
            get { return (ValidaCVELOCCOLElement)base["ValidaCVELOCCOL"]; }
        }

        /// <summary>
        /// Gets RelColoniasSer configuration data
        /// </summary>
        [ConfigurationProperty("RelColoniasSer")]
        public RelColoniasSerElement RelColoniasSer
        {
            get { return (RelColoniasSerElement)base["RelColoniasSer"]; }
        }

        /// <summary>
        /// Gets RelColoniaMedio configuration data
        /// </summary>
        [ConfigurationProperty("RelColoniaMedio")]
        public RelColoniaMedioElement RelColoniaMedio
        {
            get { return (RelColoniaMedioElement)base["RelColoniaMedio"]; }
        }

        /// <summary>
        /// Gets AreaTecnica configuration data
        /// </summary>
        [ConfigurationProperty("AreaTecnica")]
        public AreaTecnicaElement AreaTecnica
        {
            get { return (AreaTecnicaElement)base["AreaTecnica"]; }
        }

        /// <summary>
        /// Gets ValidaNombreCalle configuration data
        /// </summary>
        [ConfigurationProperty("ValidaNombreCalle")]
        public ValidaNombreCalleElement ValidaNombreCalle
        {
            get { return (ValidaNombreCalleElement)base["ValidaNombreCalle"]; }
        }

        /// <summary>
        /// Gets Calles_New configuration data
        /// </summary>
        [ConfigurationProperty("Calles_New")]
        public Calles_NewElement Calles_New
        {
            get { return (Calles_NewElement)base["Calles_New"]; }
        }

        /// <summary>
        /// Gets MuestraLocalidades_Calle configuration data
        /// </summary>
        [ConfigurationProperty("MuestraLocalidades_Calle")]
        public MuestraLocalidades_CalleElement MuestraLocalidades_Calle
        {
            get { return (MuestraLocalidades_CalleElement)base["MuestraLocalidades_Calle"]; }
        }

        /// <summary>
        /// Gets MuestraColonias_Calle configuration data
        /// </summary>
        [ConfigurationProperty("MuestraColonias_Calle")]
        public MuestraColonias_CalleElement MuestraColonias_Calle
        {
            get { return (MuestraColonias_CalleElement)base["MuestraColonias_Calle"]; }
        }

        /// <summary>
        /// Gets RelColoniasCalles_New configuration data
        /// </summary>
        [ConfigurationProperty("RelColoniasCalles_New")]
        public RelColoniasCalles_NewElement RelColoniasCalles_New
        {
            get { return (RelColoniasCalles_NewElement)base["RelColoniasCalles_New"]; }
        }

        /// <summary>
        /// Gets MuestraMedios_New configuration data
        /// </summary>
        [ConfigurationProperty("MuestraMedios_New")]
        public MuestraMedios_NewElement MuestraMedios_New
        {
            get { return (MuestraMedios_NewElement)base["MuestraMedios_New"]; }
        }

        /// <summary>
        /// Gets ValidaEliminarRelColoniaCalle configuration data
        /// </summary>
        [ConfigurationProperty("ValidaEliminarRelColoniaCalle")]
        public ValidaEliminarRelColoniaCalleElement ValidaEliminarRelColoniaCalle
        {
            get { return (ValidaEliminarRelColoniaCalleElement)base["ValidaEliminarRelColoniaCalle"]; }
        }

        /// <summary>
        /// Gets CatalogoMotivos configuration data
        /// </summary>
        [ConfigurationProperty("CatalogoMotivos")]
        public CatalogoMotivosElement CatalogoMotivos
        {
            get { return (CatalogoMotivosElement)base["CatalogoMotivos"]; }
        }

        /// <summary>
        /// Gets CatalogoAgenda configuration data
        /// </summary>
        [ConfigurationProperty("CatalogoAgenda")]
        public CatalogoAgendaElement CatalogoAgenda
        {
            get { return (CatalogoAgendaElement)base["CatalogoAgenda"]; }
        }

        /// <summary>
        /// Gets MuestraArbolServicios_Clientes configuration data
        /// </summary>
        [ConfigurationProperty("MuestraArbolServicios_Clientes")]
        public MuestraArbolServicios_ClientesElement MuestraArbolServicios_Clientes
        {
            get { return (MuestraArbolServicios_ClientesElement)base["MuestraArbolServicios_Clientes"]; }
        }



        /// <summary>
        /// Gets ContratacionServicio configuration data
        /// </summary>
        [ConfigurationProperty("ContratacionServicio")]
        public ContratacionServicioElement ContratacionServicio
        {
            get { return (ContratacionServicioElement)base["ContratacionServicio"]; }
        }

        /// <summary>
        /// Gets ClientesServicio configuration data
        /// </summary>
        [ConfigurationProperty("ClientesServicio")]
        public ClientesServicioElement ClientesServicio
        {
            get { return (ClientesServicioElement)base["ClientesServicio"]; }
        }

        /// <summary>
        /// Gets ClientesAparato configuration data
        /// </summary>
        [ConfigurationProperty("ClientesAparato")]
        public ClientesAparatoElement ClientesAparato
        {
            get { return (ClientesAparatoElement)base["ClientesAparato"]; }
        }

        /// <summary>
        /// Gets ModeloAparato configuration data
        /// </summary>
        [ConfigurationProperty("ModeloAparato")]
        public ModeloAparatoElement ModeloAparato
        {
            get { return (ModeloAparatoElement)base["ModeloAparato"]; }
        }

        /// <summary>
        /// Gets RelTipoServCliente configuration data
        /// </summary>
        [ConfigurationProperty("RelTipoServCliente")]
        public RelTipoServClienteElement RelTipoServCliente
        {
            get { return (RelTipoServClienteElement)base["RelTipoServCliente"]; }
        }

        /// <summary>
        /// Gets InfoTvs configuration data
        /// </summary>
        [ConfigurationProperty("InfoTvs")]
        public InfoTvsElement InfoTvs
        {
            get { return (InfoTvsElement)base["InfoTvs"]; }
        }

        /// <summary>
        /// Gets MuestraContratoReal configuration data
        /// </summary>
        [ConfigurationProperty("MuestraContratoReal")]
        public MuestraContratoRealElement MuestraContratoReal
        {
            get { return (MuestraContratoRealElement)base["MuestraContratoReal"]; }
        }

        /// <summary>
        /// Gets instalaservicios configuration data
        /// </summary>
        [ConfigurationProperty("instalaservicios")]
        public instalaserviciosElement instalaservicios
        {
            get { return (instalaserviciosElement)base["instalaservicios"]; }
        }

        /// <summary>
        /// Gets tbl_politicasFibra configuration data
        /// </summary>
        [ConfigurationProperty("tbl_politicasFibra")]
        public tbl_politicasFibraElement tbl_politicasFibra
        {
            get { return (tbl_politicasFibraElement)base["tbl_politicasFibra"]; }
        }

        /// <summary>
        /// Gets DameUnidadesMedidasDeVelocidad configuration data
        /// </summary>
        [ConfigurationProperty("DameUnidadesMedidasDeVelocidad")]
        public DameUnidadesMedidasDeVelocidadElement DameUnidadesMedidasDeVelocidad
        {
            get { return (DameUnidadesMedidasDeVelocidadElement)base["DameUnidadesMedidasDeVelocidad"]; }
        }

        /// <summary>
        /// Gets TblNet configuration data
        /// </summary>
        [ConfigurationProperty("TblNet")]
        public TblNetElement TblNet
        {
            get { return (TblNetElement)base["TblNet"]; }
        }

        /// <summary>
        /// Gets Rel_Trabajos_NoCobroMensual configuration data
        /// </summary>
        [ConfigurationProperty("Rel_Trabajos_NoCobroMensual")]
        public Rel_Trabajos_NoCobroMensualElement Rel_Trabajos_NoCobroMensual
        {
            get { return (Rel_Trabajos_NoCobroMensualElement)base["Rel_Trabajos_NoCobroMensual"]; }
        }

        /// <summary>
        /// Gets Catalogo_Ips configuration data
        /// </summary>
        [ConfigurationProperty("Catalogo_Ips")]
        public Catalogo_IpsElement Catalogo_Ips
        {
            get { return (Catalogo_IpsElement)base["Catalogo_Ips"]; }
        }

        /// <summary>
        /// Gets catalogoIps_dos configuration data
        /// </summary>
        [ConfigurationProperty("catalogoIps_dos")]
        public catalogoIps_dosElement catalogoIps_dos
        {
            get { return (catalogoIps_dosElement)base["catalogoIps_dos"]; }
        }

        /// <summary>
        /// Gets RelRedPlaza configuration data
        /// </summary>
        [ConfigurationProperty("RelRedPlaza")]
        public RelRedPlazaElement RelRedPlaza
        {
            get { return (RelRedPlazaElement)base["RelRedPlaza"]; }
        }

        /// <summary>
        /// Gets General_Sistema_II configuration data
        /// </summary>
        [ConfigurationProperty("General_Sistema_II")]
        public General_Sistema_IIElement General_Sistema_II
        {
            get { return (General_Sistema_IIElement)base["General_Sistema_II"]; }
        }

        /// <summary>
        /// Gets RelRedCompania configuration data
        /// </summary>
        [ConfigurationProperty("RelRedCompania")]
        public RelRedCompaniaElement RelRedCompania
        {
            get { return (RelRedCompaniaElement)base["RelRedCompania"]; }
        }

        /// <summary>
        /// Gets RelRedEstado configuration data
        /// </summary>
        [ConfigurationProperty("RelRedEstado")]
        public RelRedEstadoElement RelRedEstado
        {
            get { return (RelRedEstadoElement)base["RelRedEstado"]; }
        }

        /// <summary>
        /// Gets RelRedCiudad configuration data
        /// </summary>
        [ConfigurationProperty("RelRedCiudad")]
        public RelRedCiudadElement RelRedCiudad
        {
            get { return (RelRedCiudadElement)base["RelRedCiudad"]; }
        }

        /// <summary>
        /// Gets RelRedLocalidad configuration data
        /// </summary>
        [ConfigurationProperty("RelRedLocalidad")]
        public RelRedLocalidadElement RelRedLocalidad
        {
            get { return (RelRedLocalidadElement)base["RelRedLocalidad"]; }
        }

        /// <summary>
        /// Gets RelRedMedio configuration data
        /// </summary>
        [ConfigurationProperty("RelRedMedio")]
        public RelRedMedioElement RelRedMedio
        {
            get { return (RelRedMedioElement)base["RelRedMedio"]; }
        }

        /// <summary>
        /// Gets CatMedios configuration data
        /// </summary>
        [ConfigurationProperty("CatMedios")]
        public CatMediosElement CatMedios
        {
            get { return (CatMediosElement)base["CatMedios"]; }
        }

        /// <summary>
        /// Gets BUSCAVENDEDORES configuration data
        /// </summary>
        [ConfigurationProperty("BUSCAVENDEDORES")]
        public BUSCAVENDEDORESElement BUSCAVENDEDORES
        {
            get { return (BUSCAVENDEDORESElement)base["BUSCAVENDEDORES"]; }
        }

        /// <summary>
        /// Gets Vendedores configuration data
        /// </summary>
        [ConfigurationProperty("Vendedores")]
        public VendedoresElement Vendedores
        {
            get { return (VendedoresElement)base["Vendedores"]; }
        }

        /// <summary>
        /// Gets CatalogoSeries configuration data
        /// </summary>
        [ConfigurationProperty("CatalogoSeries")]
        public CatalogoSeriesElement CatalogoSeries
        {
            get { return (CatalogoSeriesElement)base["CatalogoSeries"]; }
        }

        /// <summary>
        /// Gets VALIDACatalogoSeries configuration data
        /// </summary>
        [ConfigurationProperty("VALIDACatalogoSeries")]
        public VALIDACatalogoSeriesElement VALIDACatalogoSeries
        {
            get { return (VALIDACatalogoSeriesElement)base["VALIDACatalogoSeries"]; }
        }

        /// <summary>
        /// Gets Ultimo_SERIEYFOLIO configuration data
        /// </summary>
        [ConfigurationProperty("Ultimo_SERIEYFOLIO")]
        public Ultimo_SERIEYFOLIOElement Ultimo_SERIEYFOLIO
        {
            get { return (Ultimo_SERIEYFOLIOElement)base["Ultimo_SERIEYFOLIO"]; }
        }

        /// <summary>
        /// Gets Folio_Disponible configuration data
        /// </summary>
        [ConfigurationProperty("Folio_Disponible")]
        public Folio_DisponibleElement Folio_Disponible
        {
            get { return (Folio_DisponibleElement)base["Folio_Disponible"]; }
        }

        /// <summary>
        /// Gets Cancela_Folios configuration data
        /// </summary>
        [ConfigurationProperty("Cancela_Folios")]
        public Cancela_FoliosElement Cancela_Folios
        {
            get { return (Cancela_FoliosElement)base["Cancela_Folios"]; }
        }

        /// <summary>
        /// Gets GuardaEvidenciaCancelacionFolio configuration data
        /// </summary>
        [ConfigurationProperty("GuardaEvidenciaCancelacionFolio")]
        public GuardaEvidenciaCancelacionFolioElement GuardaEvidenciaCancelacionFolio
        {
            get { return (GuardaEvidenciaCancelacionFolioElement)base["GuardaEvidenciaCancelacionFolio"]; }
        }


        /// <summary>
        /// Gets SP_SerieFolio configuration data
        /// </summary>
        [ConfigurationProperty("SP_SerieFolio")]
        public SP_SerieFolioElement SP_SerieFolio
        {
            get { return (SP_SerieFolioElement)base["SP_SerieFolio"]; }
        }

        /// <summary>
        /// Gets DameTipoSerie configuration data
        /// </summary>
        [ConfigurationProperty("DameTipoSerie")]
        public DameTipoSerieElement DameTipoSerie
        {
            get { return (DameTipoSerieElement)base["DameTipoSerie"]; }
        }

        /// <summary>
        /// Gets ValidaFoliosImprimir configuration data
        /// </summary>
        [ConfigurationProperty("ValidaFoliosImprimir")]
        public ValidaFoliosImprimirElement ValidaFoliosImprimir
        {
            get { return (ValidaFoliosImprimirElement)base["ValidaFoliosImprimir"]; }
        }

        /// <summary>
        /// Gets ReimprimirFolios configuration data
        /// </summary>
        [ConfigurationProperty("ReimprimirFolios")]
        public ReimprimirFoliosElement ReimprimirFolios
        {
            get { return (ReimprimirFoliosElement)base["ReimprimirFolios"]; }
        }


        /// <summary>
        /// Gets PolizaMaestro configuration data
        /// </summary>
        [ConfigurationProperty("PolizaMaestro")]
        public PolizaMaestroElement PolizaMaestro
        {
            get { return (PolizaMaestroElement)base["PolizaMaestro"]; }
        }

        [ConfigurationProperty("RelacionIngresosMaestro")]
        public RelacionIngresosMaestroElement RelacionIngresosMaestro
        {
            get { return (RelacionIngresosMaestroElement)base["RelacionIngresosMaestro"]; }
        }

        /// <summary>
        /// Gets BuscaFacturasFisca configuration data
        /// </summary>
        [ConfigurationProperty("BuscaFacturasFisca")]
        public BuscaFacturasFiscaElement BuscaFacturasFisca
        {
            get { return (BuscaFacturasFiscaElement)base["BuscaFacturasFisca"]; }
        }

        /// <summary>
        /// Gets DameDetalle_FacturaMaestroFiscal configuration data
        /// </summary>
        [ConfigurationProperty("DameDetalle_FacturaMaestroFiscal")]
        public DameDetalle_FacturaMaestroFiscalElement DameDetalle_FacturaMaestroFiscal
        {
            get { return (DameDetalle_FacturaMaestroFiscalElement)base["DameDetalle_FacturaMaestroFiscal"]; }
        }


        /// <summary>
        /// Gets ActualizaFacturaGeneraFiscal configuration data
        /// </summary>
        [ConfigurationProperty("ActualizaFacturaGeneraFiscal")]
        public ActualizaFacturaGeneraFiscalElement ActualizaFacturaGeneraFiscal
        {
            get { return (ActualizaFacturaGeneraFiscalElement)base["ActualizaFacturaGeneraFiscal"]; }
        }

        /// <summary>
        /// Gets ReporteFacturasMaestrasVencidas configuration data
        /// </summary>
        [ConfigurationProperty("ReporteFacturasMaestrasVencidas")]
        public ReporteFacturasMaestrasVencidasElement ReporteFacturasMaestrasVencidas
        {
            get { return (ReporteFacturasMaestrasVencidasElement)base["ReporteFacturasMaestrasVencidas"]; }
        }

        /// <summary>
        /// Gets ReporteServiciosInstalados configuration data
        /// </summary>
        [ConfigurationProperty("ReporteServiciosInstalados")]
        public ReporteServiciosInstaladosElement ReporteServiciosInstalados
        {
            get { return (ReporteServiciosInstaladosElement)base["ReporteServiciosInstalados"]; }
        }

        /// <summary>
        /// Gets ReporteServiciosPorInstalar configuration data
        /// </summary>
        [ConfigurationProperty("ReporteServiciosPorInstalar")]
        public ReporteServiciosPorInstalarElement ReporteServiciosPorInstalar
        {
            get { return (ReporteServiciosPorInstalarElement)base["ReporteServiciosPorInstalar"]; }
        }

        /// <summary>
        /// Gets FoliosCancelados configuration data
        /// </summary>
        [ConfigurationProperty("FoliosCancelados")]
        public FoliosCanceladosElement FoliosCancelados
        {
            get { return (FoliosCanceladosElement)base["FoliosCancelados"]; }
        }

        /// <summary>
        /// Gets EvidenciasDeCancelados configuration data
        /// </summary>
        [ConfigurationProperty("EvidenciasDeCancelados")]
        public EvidenciasDeCanceladosElement EvidenciasDeCancelados
        {
            get { return (EvidenciasDeCanceladosElement)base["EvidenciasDeCancelados"]; }
        }

        /// <summary>
        /// Gets Rangos configuration data
        /// </summary>
        [ConfigurationProperty("Rangos")]
        public RangosElement Rangos
        {
            get { return (RangosElement)base["Rangos"]; }
        }

        /// <summary>
        /// Gets ComisionesPorServicio configuration data
        /// </summary>
        [ConfigurationProperty("ComisionesPorServicio")]
        public ComisionesPorServicioElement ComisionesPorServicio
        {
            get { return (ComisionesPorServicioElement)base["ComisionesPorServicio"]; }
        }

        /// <summary>
        /// Gets GrupoVentas configuration data
        /// </summary>
        [ConfigurationProperty("GrupoVentas")]
        public GrupoVentasElement GrupoVentas
        {
            get { return (GrupoVentasElement)base["GrupoVentas"]; }
        }

        /// <summary>
        /// Gets DocumentosVendedoresClientes configuration data
        /// </summary>
        [ConfigurationProperty("DocumentosVendedoresClientes")]
        public DocumentosVendedoresClientesElement DocumentosVendedoresClientes
        {
            get { return (DocumentosVendedoresClientesElement)base["DocumentosVendedoresClientes"]; }
        }



        /// <summary>
        /// Gets DocumentosVendedoresClientes configuration data
        /// </summary>
        [ConfigurationProperty("Reportes")]
        public ReportesElement Reportes
        {
            get { return (ReportesElement)base["Reportes"]; }
        }

        /// <summary>
        /// Gets Recontratacion configuration data
        /// </summary>
        [ConfigurationProperty("Recontratacion")]
        public RecontratacionElement Recontratacion
        {
            get { return (RecontratacionElement)base["Recontratacion"]; }
        }



        /// <summary>
        /// Gets Gastos configuration data
        /// </summary>
        [ConfigurationProperty("Gastos")]
        public GastosElement Gastos
        {
            get { return (GastosElement)base["Gastos"]; }
        }

        /// <summary>
        /// Gets QuejasMasivas configuration data
        /// </summary>
        [ConfigurationProperty("QuejasMasivas")]
        public QuejasMasivasElement QuejasMasivas
        {
            get { return (QuejasMasivasElement)base["QuejasMasivas"]; }
        }



        /// <summary>
        /// Gets BusquedaQuejasMasivas configuration data
        /// </summary>
        [ConfigurationProperty("BusquedaQuejasMasivas")]
        public BusquedaQuejasMasivasElement BusquedaQuejasMasivas
        {
            get { return (BusquedaQuejasMasivasElement)base["BusquedaQuejasMasivas"]; }
        }

        /// <summary>
        /// Gets UspChecaSiTieneExtensiones configuration data
        /// </summary>
        [ConfigurationProperty("UspChecaSiTieneExtensiones")]
        public UspChecaSiTieneExtensionesElement UspChecaSiTieneExtensiones
        {
            get { return (UspChecaSiTieneExtensionesElement)base["UspChecaSiTieneExtensiones"]; }
        }

        /// <summary>
        /// Gets UspLlenaComboExtensiones configuration data
        /// </summary>
        [ConfigurationProperty("UspLlenaComboExtensiones")]
        public UspLlenaComboExtensionesElement UspLlenaComboExtensiones
        {
            get { return (UspLlenaComboExtensionesElement)base["UspLlenaComboExtensiones"]; }
        }


        /// <summary>
        /// Gets CargoAutomatico configuration data
        /// </summary>
        [ConfigurationProperty("CargoAutomatico")]
        public CargoAutomaticoElement CargoAutomatico
        {
            get { return (CargoAutomaticoElement)base["CargoAutomatico"]; }
        }

        /// <summary>
        /// Gets FacturaGloblal configuration data
        /// </summary>
        [ConfigurationProperty("FacturaGloblal")]
        public FacturaGloblalElement FacturaGloblal
        {
            get { return (FacturaGloblalElement)base["FacturaGloblal"]; }
        }
  

        [ConfigurationProperty("NotaCreditoElement")]
        public NotaCreditoElement NotaCreditoElement
        {
            get { return (NotaCreditoElement)base["NotaCreditoElement"]; }
        }

        

    }
}

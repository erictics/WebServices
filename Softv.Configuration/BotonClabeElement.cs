﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BotonClabeElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BotonClabe class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BotonClabe
        ///</summary>
        [ConfigurationProperty("DataClassBotonClabe", DefaultValue = "Softv.DAO.BotonClabeData")]
        public String DataClass
        {
          get { return (string)base["DataClassBotonClabe"]; }
        }

        /// <summary>
        /// Gets connection string for database BotonClabe access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
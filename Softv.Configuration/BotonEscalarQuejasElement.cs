﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BotonEscalarQuejasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BotonEscalarQuejas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BotonEscalarQuejas
        ///</summary>
        [ConfigurationProperty("DataClassBotonEscalarQuejas", DefaultValue = "Softv.DAO.BotonEscalarQuejasData")]
        public String DataClass
        {
          get { return (string)base["DataClassBotonEscalarQuejas"]; }
        }

        /// <summary>
        /// Gets connection string for database BotonEscalarQuejas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class BusCliPorContrato_FacElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for BusCliPorContrato_Fac class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for BusCliPorContrato_Fac
        ///</summary>
        [ConfigurationProperty("DataClassBusCliPorContrato_Fac", DefaultValue = "Softv.DAO.BusCliPorContrato_FacData")]
        public String DataClass
        {
            get { return (string)base["DataClassBusCliPorContrato_Fac"]; }
        }

        /// <summary>
        /// Gets connection string for database BusCliPorContrato_Fac access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


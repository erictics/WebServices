﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BuscaBloqueadoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BuscaBloqueado class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BuscaBloqueado
        ///</summary>
        [ConfigurationProperty("DataClassBuscaBloqueado", DefaultValue = "Softv.DAO.BuscaBloqueadoData")]
        public String DataClass
        {
          get { return (string)base["DataClassBuscaBloqueado"]; }
        }

        /// <summary>
        /// Gets connection string for database BuscaBloqueado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
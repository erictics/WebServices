﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BuscaFacturasHistorialElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BuscaFacturasHistorial class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BuscaFacturasHistorial
        ///</summary>
        [ConfigurationProperty("DataClassBuscaFacturasHistorial", DefaultValue = "Softv.DAO.BuscaFacturasHistorialData")]
        public String DataClass
        {
          get { return (string)base["DataClassBuscaFacturasHistorial"]; }
        }

        /// <summary>
        /// Gets connection string for database BuscaFacturasHistorial access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BuscaFacturasMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BuscaFacturasMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BuscaFacturasMaestro
        ///</summary>
        [ConfigurationProperty("DataClassBuscaFacturasMaestro", DefaultValue = "Softv.DAO.BuscaFacturasMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassBuscaFacturasMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database BuscaFacturasMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
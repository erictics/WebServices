﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BuscaQuejasSeparado2Element: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BuscaQuejasSeparado2 class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BuscaQuejasSeparado2
        ///</summary>
        [ConfigurationProperty("DataClassBuscaQuejasSeparado2", DefaultValue = "Softv.DAO.BuscaQuejasSeparado2Data")]
        public String DataClass
        {
          get { return (string)base["DataClassBuscaQuejasSeparado2"]; }
        }

        /// <summary>
        /// Gets connection string for database BuscaQuejasSeparado2 access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
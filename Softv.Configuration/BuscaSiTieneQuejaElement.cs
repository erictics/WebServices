﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BuscaSiTieneQuejaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BuscaSiTieneQueja class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BuscaSiTieneQueja
        ///</summary>
        [ConfigurationProperty("DataClassBuscaSiTieneQueja", DefaultValue = "Softv.DAO.BuscaSiTieneQuejaData")]
        public String DataClass
        {
          get { return (string)base["DataClassBuscaSiTieneQueja"]; }
        }

        /// <summary>
        /// Gets connection string for database BuscaSiTieneQueja access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
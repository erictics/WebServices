﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class BusquedaQuejasMasivasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for BusquedaQuejasMasivas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for BusquedaQuejasMasivas
        ///</summary>
        [ConfigurationProperty("DataClassBusquedaQuejasMasivas", DefaultValue = "Softv.DAO.BusquedaQuejasMasivasData")]
        public String DataClass
        {
          get { return (string)base["DataClassBusquedaQuejasMasivas"]; }
        }

        /// <summary>
        /// Gets connection string for database BusquedaQuejasMasivas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
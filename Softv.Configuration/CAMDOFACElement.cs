﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CAMDOFACElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CAMDOFAC class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CAMDOFAC
        ///</summary>
        [ConfigurationProperty("DataClassCAMDOFAC", DefaultValue = "Softv.DAO.CAMDOFACData")]
        public String DataClass
        {
          get { return (string)base["DataClassCAMDOFAC"]; }
        }

        /// <summary>
        /// Gets connection string for database CAMDOFAC access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
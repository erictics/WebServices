﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CANCELACIONFACTURASElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CANCELACIONFACTURAS class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CANCELACIONFACTURAS
        ///</summary>
        [ConfigurationProperty("DataClassCANCELACIONFACTURAS", DefaultValue = "Softv.DAO.CANCELACIONFACTURASData")]
        public String DataClass
        {
          get { return (string)base["DataClassCANCELACIONFACTURAS"]; }
        }

        /// <summary>
        /// Gets connection string for database CANCELACIONFACTURAS access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
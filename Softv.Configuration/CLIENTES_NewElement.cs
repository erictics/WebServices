﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CLIENTES_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CLIENTES_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CLIENTES_New
        ///</summary>
        [ConfigurationProperty("DataClassCLIENTES_New", DefaultValue = "Softv.DAO.CLIENTES_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassCLIENTES_New"]; }
        }

        /// <summary>
        /// Gets connection string for database CLIENTES_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
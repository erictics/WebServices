﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class CalleElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for Calle class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for Calle
        ///</summary>
        [ConfigurationProperty("DataClassCalle", DefaultValue = "Softv.DAO.CalleData")]
        public String DataClass
        {
            get { return (string)base["DataClassCalle"]; }
        }

        /// <summary>
        /// Gets connection string for database Calle access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


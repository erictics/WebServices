﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Cambia_Tipo_cablemodemElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Cambia_Tipo_cablemodem class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Cambia_Tipo_cablemodem
        ///</summary>
        [ConfigurationProperty("DataClassCambia_Tipo_cablemodem", DefaultValue = "Softv.DAO.Cambia_Tipo_cablemodemData")]
        public String DataClass
        {
          get { return (string)base["DataClassCambia_Tipo_cablemodem"]; }
        }

        /// <summary>
        /// Gets connection string for database Cambia_Tipo_cablemodem access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Cancela_FoliosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Cancela_Folios class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Cancela_Folios
        ///</summary>
        [ConfigurationProperty("DataClassCancela_Folios", DefaultValue = "Softv.DAO.Cancela_FoliosData")]
        public String DataClass
        {
          get { return (string)base["DataClassCancela_Folios"]; }
        }

        /// <summary>
        /// Gets connection string for database Cancela_Folios access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CatMediosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CatMedios class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CatMedios
        ///</summary>
        [ConfigurationProperty("DataClassCatMedios", DefaultValue = "Softv.DAO.CatMediosData")]
        public String DataClass
        {
          get { return (string)base["DataClassCatMedios"]; }
        }

        /// <summary>
        /// Gets connection string for database CatMedios access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
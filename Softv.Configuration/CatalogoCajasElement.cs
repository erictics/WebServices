﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CatalogoCajasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CatalogoCajas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CatalogoCajas
        ///</summary>
        [ConfigurationProperty("DataClassCatalogoCajas", DefaultValue = "Softv.DAO.CatalogoCajasData")]
        public String DataClass
        {
          get { return (string)base["DataClassCatalogoCajas"]; }
        }

        /// <summary>
        /// Gets connection string for database CatalogoCajas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CatalogoReportesFacElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CatalogoReportesFac class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CatalogoReportesFac
        ///</summary>
        [ConfigurationProperty("DataClassCatalogoReportesFac", DefaultValue = "Softv.DAO.CatalogoReportesFacData")]
        public String DataClass
        {
          get { return (string)base["DataClassCatalogoReportesFac"]; }
        }

        /// <summary>
        /// Gets connection string for database CatalogoReportesFac access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
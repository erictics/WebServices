﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CatalogoSeriesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CatalogoSeries class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CatalogoSeries
        ///</summary>
        [ConfigurationProperty("DataClassCatalogoSeries", DefaultValue = "Softv.DAO.CatalogoSeriesData")]
        public String DataClass
        {
          get { return (string)base["DataClassCatalogoSeries"]; }
        }

        /// <summary>
        /// Gets connection string for database CatalogoSeries access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
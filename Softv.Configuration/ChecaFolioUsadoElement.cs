﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ChecaFolioUsadoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ChecaFolioUsado class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ChecaFolioUsado
        ///</summary>
        [ConfigurationProperty("DataClassChecaFolioUsado", DefaultValue = "Softv.DAO.ChecaFolioUsadoData")]
        public String DataClass
        {
          get { return (string)base["DataClassChecaFolioUsado"]; }
        }

        /// <summary>
        /// Gets connection string for database ChecaFolioUsado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
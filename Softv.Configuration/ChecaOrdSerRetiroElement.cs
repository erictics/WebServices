﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ChecaOrdSerRetiroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ChecaOrdSerRetiro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ChecaOrdSerRetiro
        ///</summary>
        [ConfigurationProperty("DataClassChecaOrdSerRetiro", DefaultValue = "Softv.DAO.ChecaOrdSerRetiroData")]
        public String DataClass
        {
          get { return (string)base["DataClassChecaOrdSerRetiro"]; }
        }

        /// <summary>
        /// Gets connection string for database ChecaOrdSerRetiro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
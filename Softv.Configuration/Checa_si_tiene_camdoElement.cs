﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Checa_si_tiene_camdoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Checa_si_tiene_camdo class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Checa_si_tiene_camdo
        ///</summary>
        [ConfigurationProperty("DataClassCheca_si_tiene_camdo", DefaultValue = "Softv.DAO.Checa_si_tiene_camdoData")]
        public String DataClass
        {
          get { return (string)base["DataClassCheca_si_tiene_camdo"]; }
        }

        /// <summary>
        /// Gets connection string for database Checa_si_tiene_camdo access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
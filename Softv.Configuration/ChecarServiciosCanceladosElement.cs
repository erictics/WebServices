﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ChecarServiciosCanceladosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ChecarServiciosCancelados class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ChecarServiciosCancelados
        ///</summary>
        [ConfigurationProperty("DataClassChecarServiciosCancelados", DefaultValue = "Softv.DAO.ChecarServiciosCanceladosData")]
        public String DataClass
        {
          get { return (string)base["DataClassChecarServiciosCancelados"]; }
        }

        /// <summary>
        /// Gets connection string for database ChecarServiciosCancelados access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
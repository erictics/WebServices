﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CiudadCAMDOElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CiudadCAMDO class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CiudadCAMDO
        ///</summary>
        [ConfigurationProperty("DataClassCiudadCAMDO", DefaultValue = "Softv.DAO.CiudadCAMDOData")]
        public String DataClass
        {
          get { return (string)base["DataClassCiudadCAMDO"]; }
        }

        /// <summary>
        /// Gets connection string for database CiudadCAMDO access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
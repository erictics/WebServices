﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Ciudades_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Ciudades_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Ciudades_New
        ///</summary>
        [ConfigurationProperty("DataClassCiudades_New", DefaultValue = "Softv.DAO.Ciudades_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassCiudades_New"]; }
        }

        /// <summary>
        /// Gets connection string for database Ciudades_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
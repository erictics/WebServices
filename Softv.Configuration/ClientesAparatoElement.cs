﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ClientesAparatoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ClientesAparato class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ClientesAparato
        ///</summary>
        [ConfigurationProperty("DataClassClientesAparato", DefaultValue = "Softv.DAO.ClientesAparatoData")]
        public String DataClass
        {
          get { return (string)base["DataClassClientesAparato"]; }
        }

        /// <summary>
        /// Gets connection string for database ClientesAparato access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
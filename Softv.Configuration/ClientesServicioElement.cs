﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ClientesServicioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ClientesServicio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ClientesServicio
        ///</summary>
        [ConfigurationProperty("DataClassClientesServicio", DefaultValue = "Softv.DAO.ClientesServicioData")]
        public String DataClass
        {
          get { return (string)base["DataClassClientesServicio"]; }
        }

        /// <summary>
        /// Gets connection string for database ClientesServicio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
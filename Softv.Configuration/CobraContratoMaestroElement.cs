﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CobraContratoMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CobraContratoMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CobraContratoMaestro
        ///</summary>
        [ConfigurationProperty("DataClassCobraContratoMaestro", DefaultValue = "Softv.DAO.CobraContratoMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassCobraContratoMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database CobraContratoMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CobraSaldoContratoMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CobraSaldoContratoMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CobraSaldoContratoMaestro
        ///</summary>
        [ConfigurationProperty("DataClassCobraSaldoContratoMaestro", DefaultValue = "Softv.DAO.CobraSaldoContratoMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassCobraSaldoContratoMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database CobraSaldoContratoMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ComisionesVendedoresWebElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ComisionesVendedoresWeb class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ComisionesVendedoresWeb
        ///</summary>
        [ConfigurationProperty("DataClassComisionesVendedoresWeb", DefaultValue = "Softv.DAO.ComisionesVendedoresWebData")]
        public String DataClass
        {
          get { return (string)base["DataClassComisionesVendedoresWeb"]; }
        }

        /// <summary>
        /// Gets connection string for database ComisionesVendedoresWeb access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ConAtenTelCteElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ConAtenTelCte class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ConAtenTelCte
        ///</summary>
        [ConfigurationProperty("DataClassConAtenTelCte", DefaultValue = "Softv.DAO.ConAtenTelCteData")]
        public String DataClass
        {
          get { return (string)base["DataClassConAtenTelCte"]; }
        }

        /// <summary>
        /// Gets connection string for database ConAtenTelCte access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
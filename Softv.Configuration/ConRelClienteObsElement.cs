﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ConRelClienteObsElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ConRelClienteObs class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ConRelClienteObs
        ///</summary>
        [ConfigurationProperty("DataClassConRelClienteObs", DefaultValue = "Softv.DAO.ConRelClienteObsData")]
        public String DataClass
        {
          get { return (string)base["DataClassConRelClienteObs"]; }
        }

        /// <summary>
        /// Gets connection string for database ConRelClienteObs access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
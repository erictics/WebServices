﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ConceptosTicketNotasCreditoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ConceptosTicketNotasCredito class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ConceptosTicketNotasCredito
        ///</summary>
        [ConfigurationProperty("DataClassConceptosTicketNotasCredito", DefaultValue = "Softv.DAO.ConceptosTicketNotasCreditoData")]
        public String DataClass
        {
          get { return (string)base["DataClassConceptosTicketNotasCredito"]; }
        }

        /// <summary>
        /// Gets connection string for database ConceptosTicketNotasCredito access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ConceptosTicketPagosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ConceptosTicketPagos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ConceptosTicketPagos
        ///</summary>
        [ConfigurationProperty("DataClassConceptosTicketPagos", DefaultValue = "Softv.DAO.ConceptosTicketPagosData")]
        public String DataClass
        {
          get { return (string)base["DataClassConceptosTicketPagos"]; }
        }

        /// <summary>
        /// Gets connection string for database ConceptosTicketPagos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
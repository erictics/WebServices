﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class ConfiguracionElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for Cliente class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for Cliente
        ///</summary>
        [ConfigurationProperty("DataClassConfiguracion", DefaultValue = "Softv.DAO.ConfiguracionData")]
        public String DataClass
        {
            get { return (string)base["DataClassConfiguracion"]; }
        }

        /// <summary>
        /// Gets connection string for database Cliente access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


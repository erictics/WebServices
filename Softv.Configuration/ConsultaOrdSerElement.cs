﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ConsultaOrdSerElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ConsultaOrdSer class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ConsultaOrdSer
        ///</summary>
        [ConfigurationProperty("DataClassConsultaOrdSer", DefaultValue = "Softv.DAO.ConsultaOrdSerData")]
        public String DataClass
        {
          get { return (string)base["DataClassConsultaOrdSer"]; }
        }

        /// <summary>
        /// Gets connection string for database ConsultaOrdSer access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
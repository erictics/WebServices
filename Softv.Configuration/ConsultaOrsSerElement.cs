﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ConsultaOrsSerElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ConsultaOrsSer class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ConsultaOrsSer
        ///</summary>
        [ConfigurationProperty("DataClassConsultaOrsSer", DefaultValue = "Softv.DAO.ConsultaOrsSerData")]
        public String DataClass
        {
          get { return (string)base["DataClassConsultaOrsSer"]; }
        }

        /// <summary>
        /// Gets connection string for database ConsultaOrsSer access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
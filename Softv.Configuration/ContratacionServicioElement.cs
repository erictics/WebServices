﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ContratacionServicioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ContratacionServicio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ContratacionServicio
        ///</summary>
        [ConfigurationProperty("DataClassContratacionServicio", DefaultValue = "Softv.DAO.ContratacionServicioData")]
        public String DataClass
        {
          get { return (string)base["DataClassContratacionServicio"]; }
        }

        /// <summary>
        /// Gets connection string for database ContratacionServicio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
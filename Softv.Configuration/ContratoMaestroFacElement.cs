﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ContratoMaestroFacElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ContratoMaestroFac class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ContratoMaestroFac
        ///</summary>
        [ConfigurationProperty("DataClassContratoMaestroFac", DefaultValue = "Softv.DAO.ContratoMaestroFacData")]
        public String DataClass
        {
          get { return (string)base["DataClassContratoMaestroFac"]; }
        }

        /// <summary>
        /// Gets connection string for database ContratoMaestroFac access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
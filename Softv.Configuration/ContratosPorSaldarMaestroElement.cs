﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ContratosPorSaldarMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ContratosPorSaldarMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ContratosPorSaldarMaestro
        ///</summary>
        [ConfigurationProperty("DataClassContratosPorSaldarMaestro", DefaultValue = "Softv.DAO.ContratosPorSaldarMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassContratosPorSaldarMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database ContratosPorSaldarMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class CrearTicketTableElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for CrearTicketTable class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for CrearTicketTable
        ///</summary>
        [ConfigurationProperty("DataClassCrearTicketTable", DefaultValue = "Softv.DAO.CrearTicketTableData")]
        public String DataClass
        {
          get { return (string)base["DataClassCrearTicketTable"]; }
        }

        /// <summary>
        /// Gets connection string for database CrearTicketTable access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
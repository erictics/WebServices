﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class DameClv_SessionElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for DameClv_Session class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for DameClv_Session
        ///</summary>
        [ConfigurationProperty("DataClassDameClv_Session", DefaultValue = "Softv.DAO.DameClv_SessionData")]
        public String DataClass
        {
            get { return (string)base["DataClassDameClv_Session"]; }
        }

        /// <summary>
        /// Gets connection string for database DameClv_Session access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


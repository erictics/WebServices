﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DameDetalle_FacturaMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DameDetalle_FacturaMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DameDetalle_FacturaMaestro
        ///</summary>
        [ConfigurationProperty("DataClassDameDetalle_FacturaMaestro", DefaultValue = "Softv.DAO.DameDetalle_FacturaMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassDameDetalle_FacturaMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database DameDetalle_FacturaMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
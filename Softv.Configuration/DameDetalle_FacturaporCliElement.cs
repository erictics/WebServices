﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DameDetalle_FacturaporCliElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DameDetalle_FacturaporCli class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DameDetalle_FacturaporCli
        ///</summary>
        [ConfigurationProperty("DataClassDameDetalle_FacturaporCli", DefaultValue = "Softv.DAO.DameDetalle_FacturaporCliData")]
        public String DataClass
        {
          get { return (string)base["DataClassDameDetalle_FacturaporCli"]; }
        }

        /// <summary>
        /// Gets connection string for database DameDetalle_FacturaporCli access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class DameNombreSucursalElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for DameNombreSucursal class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for DameNombreSucursal
        ///</summary>
        [ConfigurationProperty("DataClassDameNombreSucursal", DefaultValue = "Softv.DAO.DameNombreSucursalData")]
        public String DataClass
        {
            get { return (string)base["DataClassDameNombreSucursal"]; }
        }

        /// <summary>
        /// Gets connection string for database DameNombreSucursal access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DameRelCompaniaEstadoCiudadElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DameRelCompaniaEstadoCiudad class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DameRelCompaniaEstadoCiudad
        ///</summary>
        [ConfigurationProperty("DataClassDameRelCompaniaEstadoCiudad", DefaultValue = "Softv.DAO.DameRelCompaniaEstadoCiudadData")]
        public String DataClass
        {
          get { return (string)base["DataClassDameRelCompaniaEstadoCiudad"]; }
        }

        /// <summary>
        /// Gets connection string for database DameRelCompaniaEstadoCiudad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
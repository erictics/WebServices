﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class DameSerDelCliFacElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for DameSerDelCliFac class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for DameSerDelCliFac
        ///</summary>
        [ConfigurationProperty("DataClassDameSerDelCliFac", DefaultValue = "Softv.DAO.DameSerDelCliFacData")]
        public String DataClass
        {
            get { return (string)base["DataClassDameSerDelCliFac"]; }
        }

        /// <summary>
        /// Gets connection string for database DameSerDelCliFac access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


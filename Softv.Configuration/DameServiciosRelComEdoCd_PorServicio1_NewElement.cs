﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DameServiciosRelComEdoCd_PorServicio1_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DameServiciosRelComEdoCd_PorServicio1_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DameServiciosRelComEdoCd_PorServicio1_New
        ///</summary>
        [ConfigurationProperty("DataClassDameServiciosRelComEdoCd_PorServicio1_New", DefaultValue = "Softv.DAO.DameServiciosRelComEdoCd_PorServicio1_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassDameServiciosRelComEdoCd_PorServicio1_New"]; }
        }

        /// <summary>
        /// Gets connection string for database DameServiciosRelComEdoCd_PorServicio1_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
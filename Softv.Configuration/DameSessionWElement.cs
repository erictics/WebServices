﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class DameSessionWElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for DameSessionW class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for DameSessionW
        ///</summary>
        [ConfigurationProperty("DataClassDameSessionW", DefaultValue = "Softv.DAO.DameSessionWData")]
        public String DataClass
        {
            get { return (string)base["DataClassDameSessionW"]; }
        }

        /// <summary>
        /// Gets connection string for database DameSessionW access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DameTipoSerieElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DameTipoSerie class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DameTipoSerie
        ///</summary>
        [ConfigurationProperty("DataClassDameTipoSerie", DefaultValue = "Softv.DAO.DameTipoSerieData")]
        public String DataClass
        {
          get { return (string)base["DataClassDameTipoSerie"]; }
        }

        /// <summary>
        /// Gets connection string for database DameTipoSerie access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
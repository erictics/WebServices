﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DameUnidadesMedidasDeVelocidadElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DameUnidadesMedidasDeVelocidad class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DameUnidadesMedidasDeVelocidad
        ///</summary>
        [ConfigurationProperty("DataClassDameUnidadesMedidasDeVelocidad", DefaultValue = "Softv.DAO.DameUnidadesMedidasDeVelocidadData")]
        public String DataClass
        {
          get { return (string)base["DataClassDameUnidadesMedidasDeVelocidad"]; }
        }

        /// <summary>
        /// Gets connection string for database DameUnidadesMedidasDeVelocidad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
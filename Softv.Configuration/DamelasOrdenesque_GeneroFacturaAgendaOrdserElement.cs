﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DamelasOrdenesque_GeneroFacturaAgendaOrdserElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DamelasOrdenesque_GeneroFacturaAgendaOrdser class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DamelasOrdenesque_GeneroFacturaAgendaOrdser
        ///</summary>
        [ConfigurationProperty("DataClassDamelasOrdenesque_GeneroFacturaAgendaOrdser", DefaultValue = "Softv.DAO.DamelasOrdenesque_GeneroFacturaAgendaOrdserData")]
        public String DataClass
        {
          get { return (string)base["DataClassDamelasOrdenesque_GeneroFacturaAgendaOrdser"]; }
        }

        /// <summary>
        /// Gets connection string for database DamelasOrdenesque_GeneroFacturaAgendaOrdser access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
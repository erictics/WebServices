﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DetalleContratosFMElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DetalleContratosFM class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DetalleContratosFM
        ///</summary>
        [ConfigurationProperty("DataClassDetalleContratosFM", DefaultValue = "Softv.DAO.DetalleContratosFMData")]
        public String DataClass
        {
          get { return (string)base["DataClassDetalleContratosFM"]; }
        }

        /// <summary>
        /// Gets connection string for database DetalleContratosFM access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
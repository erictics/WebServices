﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Detalle_NotasdeCreditoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Detalle_NotasdeCredito class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Detalle_NotasdeCredito
        ///</summary>
        [ConfigurationProperty("DataClassDetalle_NotasdeCredito", DefaultValue = "Softv.DAO.Detalle_NotasdeCreditoData")]
        public String DataClass
        {
          get { return (string)base["DataClassDetalle_NotasdeCredito"]; }
        }

        /// <summary>
        /// Gets connection string for database Detalle_NotasdeCredito access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DimeSiGrabaOrdElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DimeSiGrabaOrd class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DimeSiGrabaOrd
        ///</summary>
        [ConfigurationProperty("DataClassDimeSiGrabaOrd", DefaultValue = "Softv.DAO.DimeSiGrabaOrdData")]
        public String DataClass
        {
          get { return (string)base["DataClassDimeSiGrabaOrd"]; }
        }

        /// <summary>
        /// Gets connection string for database DimeSiGrabaOrd access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
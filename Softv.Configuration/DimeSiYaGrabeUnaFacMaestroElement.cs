﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DimeSiYaGrabeUnaFacMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DimeSiYaGrabeUnaFacMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        [ConfigurationProperty("DataClassDimeSiYaGrabeUnaFacMaestro", DefaultValue = "Softv.DAO.DimeSiYaGrabeUnaFacMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassDimeSiYaGrabeUnaFacMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database DimeSiYaGrabeUnaFacMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
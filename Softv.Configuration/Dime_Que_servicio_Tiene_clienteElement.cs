﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Dime_Que_servicio_Tiene_clienteElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Dime_Que_servicio_Tiene_cliente class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Dime_Que_servicio_Tiene_cliente
        ///</summary>
        [ConfigurationProperty("DataClassDime_Que_servicio_Tiene_cliente", DefaultValue = "Softv.DAO.Dime_Que_servicio_Tiene_clienteData")]
        public String DataClass
        {
          get { return (string)base["DataClassDime_Que_servicio_Tiene_cliente"]; }
        }

        /// <summary>
        /// Gets connection string for database Dime_Que_servicio_Tiene_cliente access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Dimesihay_ConexElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Dimesihay_Conex class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Dimesihay_Conex
        ///</summary>
        [ConfigurationProperty("DataClassDimesihay_Conex", DefaultValue = "Softv.DAO.Dimesihay_ConexData")]
        public String DataClass
        {
          get { return (string)base["DataClassDimesihay_Conex"]; }
        }

        /// <summary>
        /// Gets connection string for database Dimesihay_Conex access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class DocumentosVendedoresClientesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for DocumentosVendedoresClientes class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for DocumentosVendedoresClientes
        ///</summary>
        [ConfigurationProperty("DataClassDocumentosVendedoresClientes", DefaultValue = "Softv.DAO.DocumentosVendedoresClientesData")]
        public String DataClass
        {
          get { return (string)base["DataClassDocumentosVendedoresClientes"]; }
        }

        /// <summary>
        /// Gets connection string for database DocumentosVendedoresClientes access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
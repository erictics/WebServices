﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class EncuestasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Encuestas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Encuestas
        ///</summary>
        [ConfigurationProperty("DataClassEncuestas", DefaultValue = "Softv.DAO.EncuestasData")]
        public String DataClass
        {
          get { return (string)base["DataClassEncuestas"]; }
        }

        /// <summary>
        /// Gets connection string for database Encuestas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
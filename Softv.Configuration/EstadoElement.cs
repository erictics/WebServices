﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class EstadoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Estado class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Estado
        ///</summary>
        [ConfigurationProperty("DataClassEstado", DefaultValue = "Softv.DAO.EstadoData")]
        public String DataClass
        {
          get { return (string)base["DataClassEstado"]; }
        }

        /// <summary>
        /// Gets connection string for database Estado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
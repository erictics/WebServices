﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Estados_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Estados_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Estados_New
        ///</summary>
        [ConfigurationProperty("DataClassEstados_New", DefaultValue = "Softv.DAO.Estados_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassEstados_New"]; }
        }

        /// <summary>
        /// Gets connection string for database Estados_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
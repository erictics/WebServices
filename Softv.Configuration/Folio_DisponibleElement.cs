﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Folio_DisponibleElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Folio_Disponible class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Folio_Disponible
        ///</summary>
        [ConfigurationProperty("DataClassFolio_Disponible", DefaultValue = "Softv.DAO.Folio_DisponibleData")]
        public String DataClass
        {
          get { return (string)base["DataClassFolio_Disponible"]; }
        }

        /// <summary>
        /// Gets connection string for database Folio_Disponible access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
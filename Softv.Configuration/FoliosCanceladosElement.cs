﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class FoliosCanceladosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for FoliosCancelados class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for FoliosCancelados
        ///</summary>
        [ConfigurationProperty("DataClassFoliosCancelados", DefaultValue = "Softv.DAO.FoliosCanceladosData")]
        public String DataClass
        {
          get { return (string)base["DataClassFoliosCancelados"]; }
        }

        /// <summary>
        /// Gets connection string for database FoliosCancelados access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
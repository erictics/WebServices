﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class GUARDARel_Trabajos_NoCobroMensualElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for GUARDARel_Trabajos_NoCobroMensual class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for GUARDARel_Trabajos_NoCobroMensual
        ///</summary>
        [ConfigurationProperty("DataClassGUARDARel_Trabajos_NoCobroMensual", DefaultValue = "Softv.DAO.GUARDARel_Trabajos_NoCobroMensualData")]
        public String DataClass
        {
          get { return (string)base["DataClassGUARDARel_Trabajos_NoCobroMensual"]; }
        }

        /// <summary>
        /// Gets connection string for database GUARDARel_Trabajos_NoCobroMensual access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Genera_Cita_OrdserFacElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Genera_Cita_OrdserFac class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Genera_Cita_OrdserFac
        ///</summary>
        [ConfigurationProperty("DataClassGenera_Cita_OrdserFac", DefaultValue = "Softv.DAO.Genera_Cita_OrdserFacData")]
        public String DataClass
        {
          get { return (string)base["DataClassGenera_Cita_OrdserFac"]; }
        }

        /// <summary>
        /// Gets connection string for database Genera_Cita_OrdserFac access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
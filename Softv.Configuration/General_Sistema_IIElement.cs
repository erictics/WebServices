﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class General_Sistema_IIElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for General_Sistema_II class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for General_Sistema_II
        ///</summary>
        [ConfigurationProperty("DataClassGeneral_Sistema_II", DefaultValue = "Softv.DAO.General_Sistema_IIData")]
        public String DataClass
        {
          get { return (string)base["DataClassGeneral_Sistema_II"]; }
        }

        /// <summary>
        /// Gets connection string for database General_Sistema_II access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
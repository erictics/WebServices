﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class GetDescargaMaterialArticulosByIdClvOrdenElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for GetDescargaMaterialArticulosByIdClvOrden class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for GetDescargaMaterialArticulosByIdClvOrden
        ///</summary>
        [ConfigurationProperty("DataClassGetDescargaMaterialArticulosByIdClvOrden", DefaultValue = "Softv.DAO.GetDescargaMaterialArticulosByIdClvOrdenData")]
        public String DataClass
        {
          get { return (string)base["DataClassGetDescargaMaterialArticulosByIdClvOrden"]; }
        }

        /// <summary>
        /// Gets connection string for database GetDescargaMaterialArticulosByIdClvOrden access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Get_UniversoEncuestaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Get_UniversoEncuesta class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Get_UniversoEncuesta
        ///</summary>
        [ConfigurationProperty("DataClassGet_UniversoEncuesta", DefaultValue = "Softv.DAO.Get_UniversoEncuestaData")]
        public String DataClass
        {
          get { return (string)base["DataClassGet_UniversoEncuesta"]; }
        }

        /// <summary>
        /// Gets connection string for database Get_UniversoEncuesta access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
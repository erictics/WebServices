﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class GraficasPreguntasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for GraficasPreguntas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for GraficasPreguntas
        ///</summary>
        [ConfigurationProperty("DataClassGraficasPreguntas", DefaultValue = "Softv.DAO.GraficasPreguntasData")]
        public String DataClass
        {
          get { return (string)base["DataClassGraficasPreguntas"]; }
        }

        /// <summary>
        /// Gets connection string for database GraficasPreguntas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
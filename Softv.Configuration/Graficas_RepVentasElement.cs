﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Graficas_RepVentasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Graficas_RepVentas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Graficas_RepVentas
        ///</summary>
        [ConfigurationProperty("DataClassGraficas_RepVentas", DefaultValue = "Softv.DAO.Graficas_RepVentasData")]
        public String DataClass
        {
          get { return (string)base["DataClassGraficas_RepVentas"]; }
        }

        /// <summary>
        /// Gets connection string for database Graficas_RepVentas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
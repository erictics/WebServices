﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class GuardaEvidenciaCancelacionFolioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for GuardaEvidenciaCancelacionFolio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for GuardaEvidenciaCancelacionFolio
        ///</summary>
        [ConfigurationProperty("DataClassGuardaEvidenciaCancelacionFolio", DefaultValue = "Softv.DAO.GuardaEvidenciaCancelacionFolioData")]
        public String DataClass
        {
          get { return (string)base["DataClassGuardaEvidenciaCancelacionFolio"]; }
        }

        /// <summary>
        /// Gets connection string for database GuardaEvidenciaCancelacionFolio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
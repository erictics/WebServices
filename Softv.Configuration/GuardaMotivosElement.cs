﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class GuardaMotivosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for GuardaMotivos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for GuardaMotivos
        ///</summary>
        [ConfigurationProperty("DataClassGuardaMotivos", DefaultValue = "Softv.DAO.GuardaMotivosData")]
        public String DataClass
        {
          get { return (string)base["DataClassGuardaMotivos"]; }
        }

        /// <summary>
        /// Gets connection string for database GuardaMotivos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class GuardaMovSistElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for GuardaMovSist class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for GuardaMovSist
        ///</summary>
        [ConfigurationProperty("DataClassGuardaMovSist", DefaultValue = "Softv.DAO.GuardaMovSistData")]
        public String DataClass
        {
          get { return (string)base["DataClassGuardaMovSist"]; }
        }

        /// <summary>
        /// Gets connection string for database GuardaMovSist access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class GuardaPagoFacturaMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for GuardaPagoFacturaMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for GuardaPagoFacturaMaestro
        ///</summary>
        [ConfigurationProperty("DataClassGuardaPagoFacturaMaestro", DefaultValue = "Softv.DAO.GuardaPagoFacturaMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassGuardaPagoFacturaMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database GuardaPagoFacturaMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
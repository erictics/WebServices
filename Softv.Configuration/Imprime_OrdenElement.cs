﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Imprime_OrdenElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Imprime_Orden class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Imprime_Orden
        ///</summary>
        [ConfigurationProperty("DataClassImprime_Orden", DefaultValue = "Softv.DAO.Imprime_OrdenData")]
        public String DataClass
        {
          get { return (string)base["DataClassImprime_Orden"]; }
        }

        /// <summary>
        /// Gets connection string for database Imprime_Orden access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class InfoTvsElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for InfoTvs class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for InfoTvs
        ///</summary>
        [ConfigurationProperty("DataClassInfoTvs", DefaultValue = "Softv.DAO.InfoTvsData")]
        public String DataClass
        {
          get { return (string)base["DataClassInfoTvs"]; }
        }

        /// <summary>
        /// Gets connection string for database InfoTvs access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
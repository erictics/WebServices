﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class InsertMotCanServElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for InsertMotCanServ class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for InsertMotCanServ
        ///</summary>
        [ConfigurationProperty("DataClassInsertMotCanServ", DefaultValue = "Softv.DAO.InsertMotCanServData")]
        public String DataClass
        {
          get { return (string)base["DataClassInsertMotCanServ"]; }
        }

        /// <summary>
        /// Gets connection string for database InsertMotCanServ access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
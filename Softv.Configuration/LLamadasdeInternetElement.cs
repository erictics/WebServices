﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class LLamadasdeInternetElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for LLamadasdeInternet class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for LLamadasdeInternet
        ///</summary>
        [ConfigurationProperty("DataClassLLamadasdeInternet", DefaultValue = "Softv.DAO.LLamadasdeInternetData")]
        public String DataClass
        {
          get { return (string)base["DataClassLLamadasdeInternet"]; }
        }

        /// <summary>
        /// Gets connection string for database LLamadasdeInternet access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ListadoNotasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ListadoNotas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ListadoNotas
        ///</summary>
        [ConfigurationProperty("DataClassListadoNotas", DefaultValue = "Softv.DAO.ListadoNotasData")]
        public String DataClass
        {
          get { return (string)base["DataClassListadoNotas"]; }
        }

        /// <summary>
        /// Gets connection string for database ListadoNotas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
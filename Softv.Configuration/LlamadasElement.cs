﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class LlamadasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Llamadas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Llamadas
        ///</summary>
        [ConfigurationProperty("DataClassLlamadas", DefaultValue = "Softv.DAO.LlamadasData")]
        public String DataClass
        {
          get { return (string)base["DataClassLlamadas"]; }
        }

        /// <summary>
        /// Gets connection string for database Llamadas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
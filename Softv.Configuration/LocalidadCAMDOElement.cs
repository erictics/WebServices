﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class LocalidadCAMDOElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for LocalidadCAMDO class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for LocalidadCAMDO
        ///</summary>
        [ConfigurationProperty("DataClassLocalidadCAMDO", DefaultValue = "Softv.DAO.LocalidadCAMDOData")]
        public String DataClass
        {
          get { return (string)base["DataClassLocalidadCAMDO"]; }
        }

        /// <summary>
        /// Gets connection string for database LocalidadCAMDO access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
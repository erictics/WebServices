﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class LocalidadElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for Localidad class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for Localidad
        ///</summary>
        [ConfigurationProperty("DataClassLocalidad", DefaultValue = "Softv.DAO.LocalidadData")]
        public String DataClass
        {
            get { return (string)base["DataClassLocalidad"]; }
        }

        /// <summary>
        /// Gets connection string for database Localidad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


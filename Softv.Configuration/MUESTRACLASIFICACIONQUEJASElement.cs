﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MUESTRACLASIFICACIONQUEJASElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MUESTRACLASIFICACIONQUEJAS class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MUESTRACLASIFICACIONQUEJAS
        ///</summary>
        [ConfigurationProperty("DataClassMUESTRACLASIFICACIONQUEJAS", DefaultValue = "Softv.DAO.MUESTRACLASIFICACIONQUEJASData")]
        public String DataClass
        {
          get { return (string)base["DataClassMUESTRACLASIFICACIONQUEJAS"]; }
        }

        /// <summary>
        /// Gets connection string for database MUESTRACLASIFICACIONQUEJAS access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
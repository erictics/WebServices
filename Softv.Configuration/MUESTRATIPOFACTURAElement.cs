﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MUESTRATIPOFACTURAElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MUESTRATIPOFACTURA class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MUESTRATIPOFACTURA
        ///</summary>
        [ConfigurationProperty("DataClassMUESTRATIPOFACTURA", DefaultValue = "Softv.DAO.MUESTRATIPOFACTURAData")]
        public String DataClass
        {
          get { return (string)base["DataClassMUESTRATIPOFACTURA"]; }
        }

        /// <summary>
        /// Gets connection string for database MUESTRATIPOFACTURA access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MUESTRATIPOFACTURA_ReimpresionElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MUESTRATIPOFACTURA_Reimpresion class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MUESTRATIPOFACTURA_Reimpresion
        ///</summary>
        [ConfigurationProperty("DataClassMUESTRATIPOFACTURA_Reimpresion", DefaultValue = "Softv.DAO.MUESTRATIPOFACTURA_ReimpresionData")]
        public String DataClass
        {
          get { return (string)base["DataClassMUESTRATIPOFACTURA_Reimpresion"]; }
        }

        /// <summary>
        /// Gets connection string for database MUESTRATIPOFACTURA_Reimpresion access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
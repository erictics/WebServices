﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MUESTRATRABAJOSQUEJASElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MUESTRATRABAJOSQUEJAS class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MUESTRATRABAJOSQUEJAS
        ///</summary>
        [ConfigurationProperty("DataClassMUESTRATRABAJOSQUEJAS", DefaultValue = "Softv.DAO.MUESTRATRABAJOSQUEJASData")]
        public String DataClass
        {
          get { return (string)base["DataClassMUESTRATRABAJOSQUEJAS"]; }
        }

        /// <summary>
        /// Gets connection string for database MUESTRATRABAJOSQUEJAS access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
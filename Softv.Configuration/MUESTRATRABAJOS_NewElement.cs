﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MUESTRATRABAJOS_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MUESTRATRABAJOS_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MUESTRATRABAJOS_New
        ///</summary>
        [ConfigurationProperty("DataClassMUESTRATRABAJOS_New", DefaultValue = "Softv.DAO.MUESTRATRABAJOS_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassMUESTRATRABAJOS_New"]; }
        }

        /// <summary>
        /// Gets connection string for database MUESTRATRABAJOS_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
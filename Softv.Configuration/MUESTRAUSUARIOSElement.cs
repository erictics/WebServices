﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MUESTRAUSUARIOSElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MUESTRAUSUARIOS class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MUESTRAUSUARIOS
        ///</summary>
        [ConfigurationProperty("DataClassMUESTRAUSUARIOS", DefaultValue = "Softv.DAO.MUESTRAUSUARIOSData")]
        public String DataClass
        {
          get { return (string)base["DataClassMUESTRAUSUARIOS"]; }
        }

        /// <summary>
        /// Gets connection string for database MUESTRAUSUARIOS access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
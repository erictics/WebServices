﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Medio_SoftvElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Medio_Softv class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Medio_Softv
        ///</summary>
        [ConfigurationProperty("DataClassMedio_Softv", DefaultValue = "Softv.DAO.Medio_SoftvData")]
        public String DataClass
        {
          get { return (string)base["DataClassMedio_Softv"]; }
        }

        /// <summary>
        /// Gets connection string for database Medio_Softv access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
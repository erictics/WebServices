﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ModRentaAparatoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ModRentaAparato class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ModRentaAparato
        ///</summary>
        [ConfigurationProperty("DataClassModRentaAparato", DefaultValue = "Softv.DAO.ModRentaAparatoData")]
        public String DataClass
        {
          get { return (string)base["DataClassModRentaAparato"]; }
        }

        /// <summary>
        /// Gets connection string for database ModRentaAparato access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
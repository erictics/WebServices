﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ModeloAparatoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ModeloAparato class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ModeloAparato
        ///</summary>
        [ConfigurationProperty("DataClassModeloAparato", DefaultValue = "Softv.DAO.ModeloAparatoData")]
        public String DataClass
        {
          get { return (string)base["DataClassModeloAparato"]; }
        }

        /// <summary>
        /// Gets connection string for database ModeloAparato access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MovSistElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MovSist class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MovSist
        ///</summary>
        [ConfigurationProperty("DataClassMovSist", DefaultValue = "Softv.DAO.MovSistData")]
        public String DataClass
        {
          get { return (string)base["DataClassMovSist"]; }
        }

        /// <summary>
        /// Gets connection string for database MovSist access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
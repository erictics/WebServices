﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraAparatosCobroAdeudoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraAparatosCobroAdeudo class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraAparatosCobroAdeudo
        ///</summary>
        [ConfigurationProperty("DataClassMuestraAparatosCobroAdeudo", DefaultValue = "Softv.DAO.MuestraAparatosCobroAdeudoData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraAparatosCobroAdeudo"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraAparatosCobroAdeudo access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
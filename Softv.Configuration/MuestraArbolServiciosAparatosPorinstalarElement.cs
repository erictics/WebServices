﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraArbolServiciosAparatosPorinstalarElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraArbolServiciosAparatosPorinstalar class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraArbolServiciosAparatosPorinstalar
        ///</summary>
        [ConfigurationProperty("DataClassMuestraArbolServiciosAparatosPorinstalar", DefaultValue = "Softv.DAO.MuestraArbolServiciosAparatosPorinstalarData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraArbolServiciosAparatosPorinstalar"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraArbolServiciosAparatosPorinstalar access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraCajerosProcesosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraCajerosProcesos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraCajerosProcesos
        ///</summary>
        [ConfigurationProperty("DataClassMuestraCajerosProcesos", DefaultValue = "Softv.DAO.MuestraCajerosProcesosData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraCajerosProcesos"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraCajerosProcesos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
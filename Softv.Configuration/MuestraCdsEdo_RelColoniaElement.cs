﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraCdsEdo_RelColoniaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraCdsEdo_RelColonia class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraCdsEdo_RelColonia
        ///</summary>
        [ConfigurationProperty("DataClassMuestraCdsEdo_RelColonia", DefaultValue = "Softv.DAO.MuestraCdsEdo_RelColoniaData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraCdsEdo_RelColonia"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraCdsEdo_RelColonia access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraCiudadesEstadoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraCiudadesEstado class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraCiudadesEstado
        ///</summary>
        [ConfigurationProperty("DataClassMuestraCiudadesEstado", DefaultValue = "Softv.DAO.MuestraCiudadesEstadoData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraCiudadesEstado"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraCiudadesEstado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
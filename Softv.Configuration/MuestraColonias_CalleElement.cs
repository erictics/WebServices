﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraColonias_CalleElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraColonias_Calle class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraColonias_Calle
        ///</summary>
        [ConfigurationProperty("DataClassMuestraColonias_Calle", DefaultValue = "Softv.DAO.MuestraColonias_CalleData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraColonias_Calle"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraColonias_Calle access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
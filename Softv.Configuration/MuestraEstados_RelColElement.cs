﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraEstados_RelColElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraEstados_RelCol class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraEstados_RelCol
        ///</summary>
        [ConfigurationProperty("DataClassMuestraEstados_RelCol", DefaultValue = "Softv.DAO.MuestraEstados_RelColData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraEstados_RelCol"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraEstados_RelCol access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
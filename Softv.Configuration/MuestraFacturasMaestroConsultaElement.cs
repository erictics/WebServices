﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraFacturasMaestroConsultaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraFacturasMaestroConsulta class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraFacturasMaestroConsulta
        ///</summary>
        [ConfigurationProperty("DataClassMuestraFacturasMaestroConsulta", DefaultValue = "Softv.DAO.MuestraFacturasMaestroConsultaData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraFacturasMaestroConsulta"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraFacturasMaestroConsulta access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraLoc_RelColoniaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraLoc_RelColonia class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraLoc_RelColonia
        ///</summary>
        [ConfigurationProperty("DataClassMuestraLoc_RelColonia", DefaultValue = "Softv.DAO.MuestraLoc_RelColoniaData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraLoc_RelColonia"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraLoc_RelColonia access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
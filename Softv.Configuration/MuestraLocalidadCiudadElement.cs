﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraLocalidadCiudadElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraLocalidadCiudad class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraLocalidadCiudad
        ///</summary>
        [ConfigurationProperty("DataClassMuestraLocalidadCiudad", DefaultValue = "Softv.DAO.MuestraLocalidadCiudadData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraLocalidadCiudad"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraLocalidadCiudad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
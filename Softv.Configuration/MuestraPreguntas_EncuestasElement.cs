﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraPreguntas_EncuestasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraPreguntas_Encuestas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraPreguntas_Encuestas
        ///</summary>
        [ConfigurationProperty("DataClassMuestraPreguntas_Encuestas", DefaultValue = "Softv.DAO.MuestraPreguntas_EncuestasData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraPreguntas_Encuestas"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraPreguntas_Encuestas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraRelOrdenesTecnicosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraRelOrdenesTecnicos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraRelOrdenesTecnicos
        ///</summary>
        [ConfigurationProperty("DataClassMuestraRelOrdenesTecnicos", DefaultValue = "Softv.DAO.MuestraRelOrdenesTecnicosData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraRelOrdenesTecnicos"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraRelOrdenesTecnicos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
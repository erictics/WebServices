﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraServiciosDelCli_porOpcionElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraServiciosDelCli_porOpcion class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraServiciosDelCli_porOpcion
        ///</summary>
        [ConfigurationProperty("DataClassMuestraServiciosDelCli_porOpcion", DefaultValue = "Softv.DAO.MuestraServiciosDelCli_porOpcionData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraServiciosDelCli_porOpcion"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraServiciosDelCli_porOpcion access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
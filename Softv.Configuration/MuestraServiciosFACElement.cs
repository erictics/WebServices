﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraServiciosFACElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraServiciosFAC class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraServiciosFAC
        ///</summary>
        [ConfigurationProperty("DataClassMuestraServiciosFAC", DefaultValue = "Softv.DAO.MuestraServiciosFACData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraServiciosFAC"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraServiciosFAC access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
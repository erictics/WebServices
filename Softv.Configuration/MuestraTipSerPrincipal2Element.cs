﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraTipSerPrincipal2Element: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraTipSerPrincipal2 class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraTipSerPrincipal2
        ///</summary>
        [ConfigurationProperty("DataClassMuestraTipSerPrincipal2", DefaultValue = "Softv.DAO.MuestraTipSerPrincipal2Data")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraTipSerPrincipal2"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraTipSerPrincipal2 access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
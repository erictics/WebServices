﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraTipoAparatoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraTipoAparato class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraTipoAparato
        ///</summary>
        [ConfigurationProperty("DataClassMuestraTipoAparato", DefaultValue = "Softv.DAO.MuestraTipoAparatoData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraTipoAparato"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraTipoAparato access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraTipoFacturaHistorialElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraTipoFacturaHistorial class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraTipoFacturaHistorial
        ///</summary>
        [ConfigurationProperty("DataClassMuestraTipoFacturaHistorial", DefaultValue = "Softv.DAO.MuestraTipoFacturaHistorialData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraTipoFacturaHistorial"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraTipoFacturaHistorial access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
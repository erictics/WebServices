﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraTipoPromocionElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraTipoPromocion class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraTipoPromocion
        ///</summary>
        [ConfigurationProperty("DataClassMuestraTipoPromocion", DefaultValue = "Softv.DAO.MuestraTipoPromocionData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraTipoPromocion"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraTipoPromocion access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
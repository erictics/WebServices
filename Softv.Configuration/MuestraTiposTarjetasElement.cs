﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MuestraTiposTarjetasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for MuestraTiposTarjetas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for MuestraTiposTarjetas
        ///</summary>
        [ConfigurationProperty("DataClassMuestraTiposTarjetas", DefaultValue = "Softv.DAO.MuestraTiposTarjetasData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestraTiposTarjetas"]; }
        }

        /// <summary>
        /// Gets connection string for database MuestraTiposTarjetas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Muestra_CajasCortesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Muestra_CajasCortes class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Muestra_CajasCortes
        ///</summary>
        [ConfigurationProperty("DataClassMuestra_CajasCortes", DefaultValue = "Softv.DAO.Muestra_CajasCortesData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestra_CajasCortes"]; }
        }

        /// <summary>
        /// Gets connection string for database Muestra_CajasCortes access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
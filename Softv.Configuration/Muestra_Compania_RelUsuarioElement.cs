﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Muestra_Compania_RelUsuarioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Muestra_Compania_RelUsuario class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Muestra_Compania_RelUsuario
        ///</summary>
        [ConfigurationProperty("DataClassMuestra_Compania_RelUsuario", DefaultValue = "Softv.DAO.Muestra_Compania_RelUsuarioData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestra_Compania_RelUsuario"]; }
        }

        /// <summary>
        /// Gets connection string for database Muestra_Compania_RelUsuario access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
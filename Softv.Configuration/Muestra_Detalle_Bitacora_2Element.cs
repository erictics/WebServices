﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Muestra_Detalle_Bitacora_2Element: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Muestra_Detalle_Bitacora_2 class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Muestra_Detalle_Bitacora_2
        ///</summary>
        [ConfigurationProperty("DataClassMuestra_Detalle_Bitacora_2", DefaultValue = "Softv.DAO.Muestra_Detalle_Bitacora_2Data")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestra_Detalle_Bitacora_2"]; }
        }

        /// <summary>
        /// Gets connection string for database Muestra_Detalle_Bitacora_2 access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
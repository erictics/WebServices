﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Muestra_Plazas_ConfiguracionServiciosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Muestra_Plazas_ConfiguracionServicios class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Muestra_Plazas_ConfiguracionServicios
        ///</summary>
        [ConfigurationProperty("DataClassMuestra_Plazas_ConfiguracionServicios", DefaultValue = "Softv.DAO.Muestra_Plazas_ConfiguracionServiciosData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestra_Plazas_ConfiguracionServicios"]; }
        }

        /// <summary>
        /// Gets connection string for database Muestra_Plazas_ConfiguracionServicios access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
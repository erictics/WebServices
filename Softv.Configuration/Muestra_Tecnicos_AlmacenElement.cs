﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Muestra_Tecnicos_AlmacenElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Muestra_Tecnicos_Almacen class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Muestra_Tecnicos_Almacen
        ///</summary>
        [ConfigurationProperty("DataClassMuestra_Tecnicos_Almacen", DefaultValue = "Softv.DAO.Muestra_Tecnicos_AlmacenData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestra_Tecnicos_Almacen"]; }
        }

        /// <summary>
        /// Gets connection string for database Muestra_Tecnicos_Almacen access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
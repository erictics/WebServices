﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Muestra_VendedoresCortesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Muestra_VendedoresCortes class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Muestra_VendedoresCortes
        ///</summary>
        [ConfigurationProperty("DataClassMuestra_VendedoresCortes", DefaultValue = "Softv.DAO.Muestra_VendedoresCortesData")]
        public String DataClass
        {
          get { return (string)base["DataClassMuestra_VendedoresCortes"]; }
        }

        /// <summary>
        /// Gets connection string for database Muestra_VendedoresCortes access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
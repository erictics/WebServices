﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class MunicipioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Municipio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Municipio
        ///</summary>
        [ConfigurationProperty("DataClassMunicipio", DefaultValue = "Softv.DAO.MunicipioData")]
        public String DataClass
        {
          get { return (string)base["DataClassMunicipio"]; }
        }

        /// <summary>
        /// Gets connection string for database Municipio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
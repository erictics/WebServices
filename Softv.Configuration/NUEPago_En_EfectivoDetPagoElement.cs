﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class NUEPago_En_EfectivoDetPagoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for NUEPago_En_EfectivoDetPago class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for NUEPago_En_EfectivoDetPago
        ///</summary>
        [ConfigurationProperty("DataClassNUEPago_En_EfectivoDetPago", DefaultValue = "Softv.DAO.NUEPago_En_EfectivoDetPagoData")]
        public String DataClass
        {
          get { return (string)base["DataClassNUEPago_En_EfectivoDetPago"]; }
        }

        /// <summary>
        /// Gets connection string for database NUEPago_En_EfectivoDetPago access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
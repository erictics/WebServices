﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class NUEPuntos_Pago_AdelantadoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for NUEPuntos_Pago_Adelantado class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for NUEPuntos_Pago_Adelantado
        ///</summary>
        [ConfigurationProperty("DataClassNUEPuntos_Pago_Adelantado", DefaultValue = "Softv.DAO.NUEPuntos_Pago_AdelantadoData")]
        public String DataClass
        {
          get { return (string)base["DataClassNUEPuntos_Pago_Adelantado"]; }
        }

        /// <summary>
        /// Gets connection string for database NUEPuntos_Pago_Adelantado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class NUEVOClv_EquiElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for NUEVOClv_Equi class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for NUEVOClv_Equi
        ///</summary>
        [ConfigurationProperty("DataClassNUEVOClv_Equi", DefaultValue = "Softv.DAO.NUEVOClv_EquiData")]
        public String DataClass
        {
          get { return (string)base["DataClassNUEVOClv_Equi"]; }
        }

        /// <summary>
        /// Gets connection string for database NUEVOClv_Equi access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
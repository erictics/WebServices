﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class NotasDeCredito_ContraMaeFacElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for NotasDeCredito_ContraMaeFac class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for NotasDeCredito_ContraMaeFac
        ///</summary>
        [ConfigurationProperty("DataClassNotasDeCredito_ContraMaeFac", DefaultValue = "Softv.DAO.NotasDeCredito_ContraMaeFacData")]
        public String DataClass
        {
          get { return (string)base["DataClassNotasDeCredito_ContraMaeFac"]; }
        }

        /// <summary>
        /// Gets connection string for database NotasDeCredito_ContraMaeFac access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
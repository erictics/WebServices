﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class NueAplicaSoloInternetElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for NueAplicaSoloInternet class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for NueAplicaSoloInternet
        ///</summary>
        [ConfigurationProperty("DataClassNueAplicaSoloInternet", DefaultValue = "Softv.DAO.NueAplicaSoloInternetData")]
        public String DataClass
        {
          get { return (string)base["DataClassNueAplicaSoloInternet"]; }
        }

        /// <summary>
        /// Gets connection string for database NueAplicaSoloInternet access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
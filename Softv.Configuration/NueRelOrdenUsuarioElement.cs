﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class NueRelOrdenUsuarioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for NueRelOrdenUsuario class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for NueRelOrdenUsuario
        ///</summary>
        [ConfigurationProperty("DataClassNueRelOrdenUsuario", DefaultValue = "Softv.DAO.NueRelOrdenUsuarioData")]
        public String DataClass
        {
          get { return (string)base["DataClassNueRelOrdenUsuario"]; }
        }

        /// <summary>
        /// Gets connection string for database NueRelOrdenUsuario access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
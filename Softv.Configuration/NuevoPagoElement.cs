﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class NuevoPagoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for NuevoPago class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for NuevoPago
        ///</summary>
        [ConfigurationProperty("DataClassNuevoPago", DefaultValue = "Softv.DAO.NuevoPagoData")]
        public String DataClass
        {
          get { return (string)base["DataClassNuevoPago"]; }
        }

        /// <summary>
        /// Gets connection string for database NuevoPago access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
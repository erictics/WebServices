﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ObservacionClienteElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ObservacionCliente class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ObservacionCliente
        ///</summary>
        [ConfigurationProperty("DataClassObservacionCliente", DefaultValue = "Softv.DAO.ObservacionClienteData")]
        public String DataClass
        {
          get { return (string)base["DataClassObservacionCliente"]; }
        }

        /// <summary>
        /// Gets connection string for database ObservacionCliente access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
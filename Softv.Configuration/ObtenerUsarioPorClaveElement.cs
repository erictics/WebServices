﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ObtenerUsarioPorClaveElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ObtenerUsarioPorClave class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ObtenerUsarioPorClave
        ///</summary>
        [ConfigurationProperty("DataClassObtenerUsarioPorClave", DefaultValue = "Softv.DAO.ObtenerUsarioPorClaveData")]
        public String DataClass
        {
          get { return (string)base["DataClassObtenerUsarioPorClave"]; }
        }

        /// <summary>
        /// Gets connection string for database ObtenerUsarioPorClave access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
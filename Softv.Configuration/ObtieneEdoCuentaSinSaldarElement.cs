﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ObtieneEdoCuentaSinSaldarElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ObtieneEdoCuentaSinSaldar class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ObtieneEdoCuentaSinSaldar
        ///</summary>
        [ConfigurationProperty("DataClassObtieneEdoCuentaSinSaldar", DefaultValue = "Softv.DAO.ObtieneEdoCuentaSinSaldarData")]
        public String DataClass
        {
          get { return (string)base["DataClassObtieneEdoCuentaSinSaldar"]; }
        }

        /// <summary>
        /// Gets connection string for database ObtieneEdoCuentaSinSaldar access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
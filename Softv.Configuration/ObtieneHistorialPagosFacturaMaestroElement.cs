﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ObtieneHistorialPagosFacturaMaestroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ObtieneHistorialPagosFacturaMaestro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        [ConfigurationProperty("DataClassObtieneHistorialPagosFacturaMaestro", DefaultValue = "Softv.DAO.ObtieneHistorialPagosFacturaMaestroData")]
        public String DataClass
        {
          get { return (string)base["DataClassObtieneHistorialPagosFacturaMaestro"]; }
        }

        /// <summary>
        /// Gets connection string for database ObtieneHistorialPagosFacturaMaestro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
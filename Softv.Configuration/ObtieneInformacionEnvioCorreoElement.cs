﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ObtieneInformacionEnvioCorreoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ObtieneInformacionEnvioCorreo class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ObtieneInformacionEnvioCorreo
        ///</summary>
        [ConfigurationProperty("DataClassObtieneInformacionEnvioCorreo", DefaultValue = "Softv.DAO.ObtieneInformacionEnvioCorreoData")]
        public String DataClass
        {
          get { return (string)base["DataClassObtieneInformacionEnvioCorreo"]; }
        }

        /// <summary>
        /// Gets connection string for database ObtieneInformacionEnvioCorreo access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
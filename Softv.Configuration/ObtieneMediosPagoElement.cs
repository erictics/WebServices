﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ObtieneMediosPagoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ObtieneMediosPago class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ObtieneMediosPago
        ///</summary>
        [ConfigurationProperty("DataClassObtieneMediosPago", DefaultValue = "Softv.DAO.ObtieneMediosPagoData")]
        public String DataClass
        {
          get { return (string)base["DataClassObtieneMediosPago"]; }
        }

        /// <summary>
        /// Gets connection string for database ObtieneMediosPago access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ObtieneSucursalesEspeciales_ReimpresionElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ObtieneSucursalesEspeciales_Reimpresion class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ObtieneSucursalesEspeciales_Reimpresion
        ///</summary>
        [ConfigurationProperty("DataClassObtieneSucursalesEspeciales_Reimpresion", DefaultValue = "Softv.DAO.ObtieneSucursalesEspeciales_ReimpresionData")]
        public String DataClass
        {
          get { return (string)base["DataClassObtieneSucursalesEspeciales_Reimpresion"]; }
        }

        /// <summary>
        /// Gets connection string for database ObtieneSucursalesEspeciales_Reimpresion access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
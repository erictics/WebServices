﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class OrdSerElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for OrdSer class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for OrdSer
        ///</summary>
        [ConfigurationProperty("DataClassOrdSer", DefaultValue = "Softv.DAO.OrdSerData")]
        public String DataClass
        {
          get { return (string)base["DataClassOrdSer"]; }
        }

        /// <summary>
        /// Gets connection string for database OrdSer access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
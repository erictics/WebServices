﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class PagoAdelantadoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for PagoAdelantado class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for PagoAdelantado
        ///</summary>
        [ConfigurationProperty("DataClassPagoAdelantado", DefaultValue = "Softv.DAO.PagoAdelantadoData")]
        public String DataClass
        {
          get { return (string)base["DataClassPagoAdelantado"]; }
        }

        /// <summary>
        /// Gets connection string for database PagoAdelantado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class PlazaRepVariosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for PlazaRepVarios class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for PlazaRepVarios
        ///</summary>
        [ConfigurationProperty("DataClassPlazaRepVarios", DefaultValue = "Softv.DAO.PlazaRepVariosData")]
        public String DataClass
        {
          get { return (string)base["DataClassPlazaRepVarios"]; }
        }

        /// <summary>
        /// Gets connection string for database PlazaRepVarios access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
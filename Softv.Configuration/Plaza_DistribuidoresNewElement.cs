﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Plaza_DistribuidoresNewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Plaza_DistribuidoresNew class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Plaza_DistribuidoresNew
        ///</summary>
        [ConfigurationProperty("DataClassPlaza_DistribuidoresNew", DefaultValue = "Softv.DAO.Plaza_DistribuidoresNewData")]
        public String DataClass
        {
          get { return (string)base["DataClassPlaza_DistribuidoresNew"]; }
        }

        /// <summary>
        /// Gets connection string for database Plaza_DistribuidoresNew access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
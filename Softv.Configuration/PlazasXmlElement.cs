﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class PlazasXmlElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for PlazasXml class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for PlazasXml
        ///</summary>
        [ConfigurationProperty("DataClassPlazasXml", DefaultValue = "Softv.DAO.PlazasXmlData")]
        public String DataClass
        {
          get { return (string)base["DataClassPlazasXml"]; }
        }

        /// <summary>
        /// Gets connection string for database PlazasXml access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
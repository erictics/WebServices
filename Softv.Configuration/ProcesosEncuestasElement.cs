﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ProcesosEncuestasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ProcesosEncuestas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ProcesosEncuestas
        ///</summary>
        [ConfigurationProperty("DataClassProcesosEncuestas", DefaultValue = "Softv.DAO.ProcesosEncuestasData")]
        public String DataClass
        {
          get { return (string)base["DataClassProcesosEncuestas"]; }
        }

        /// <summary>
        /// Gets connection string for database ProcesosEncuestas access
        ///</summary>
        [ConfigurationProperty("ConnectionStringEncuestas")]
        public String ConnectionStringEncuestas
        {
          get
          {
            string connectionString = (string)base["ConnectionStringEncuestas"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionStringEncuestas"];
            return connectionString;
          }
        }
      }
    }

  
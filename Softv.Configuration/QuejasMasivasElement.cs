﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class QuejasMasivasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for QuejasMasivas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for QuejasMasivas
        ///</summary>
        [ConfigurationProperty("DataClassQuejasMasivas", DefaultValue = "Softv.DAO.QuejasMasivasData")]
        public String DataClass
        {
          get { return (string)base["DataClassQuejasMasivas"]; }
        }

        /// <summary>
        /// Gets connection string for database QuejasMasivas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
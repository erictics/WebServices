﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class QuitarAntenaDevolucionElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for QuitarAntenaDevolucion class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for QuitarAntenaDevolucion
        ///</summary>
        [ConfigurationProperty("DataClassQuitarAntenaDevolucion", DefaultValue = "Softv.DAO.QuitarAntenaDevolucionData")]
        public String DataClass
        {
          get { return (string)base["DataClassQuitarAntenaDevolucion"]; }
        }

        /// <summary>
        /// Gets connection string for database QuitarAntenaDevolucion access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
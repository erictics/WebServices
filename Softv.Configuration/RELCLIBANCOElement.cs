﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RELCLIBANCOElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RELCLIBANCO class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RELCLIBANCO
        ///</summary>
        [ConfigurationProperty("DataClassRELCLIBANCO", DefaultValue = "Softv.DAO.RELCLIBANCOData")]
        public String DataClass
        {
          get { return (string)base["DataClassRELCLIBANCO"]; }
        }

        /// <summary>
        /// Gets connection string for database RELCLIBANCO access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
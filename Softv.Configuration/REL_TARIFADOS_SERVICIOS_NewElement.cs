﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class REL_TARIFADOS_SERVICIOS_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for REL_TARIFADOS_SERVICIOS_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for REL_TARIFADOS_SERVICIOS_New
        ///</summary>
        [ConfigurationProperty("DataClassREL_TARIFADOS_SERVICIOS_New", DefaultValue = "Softv.DAO.REL_TARIFADOS_SERVICIOS_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassREL_TARIFADOS_SERVICIOS_New"]; }
        }

        /// <summary>
        /// Gets connection string for database REL_TARIFADOS_SERVICIOS_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RangosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Rangos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Rangos
        ///</summary>
        [ConfigurationProperty("DataClassRangos", DefaultValue = "Softv.DAO.RangosData")]
        public String DataClass
        {
          get { return (string)base["DataClassRangos"]; }
        }

        /// <summary>
        /// Gets connection string for database Rangos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RecontratacionElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Recontratacion class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Recontratacion
        ///</summary>
        [ConfigurationProperty("DataClassRecontratacion", DefaultValue = "Softv.DAO.RecontratacionData")]
        public String DataClass
        {
          get { return (string)base["DataClassRecontratacion"]; }
        }

        /// <summary>
        /// Gets connection string for database Recontratacion access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ReenviaEstadosCuentaPorContratoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ReenviaEstadosCuentaPorContrato class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ReenviaEstadosCuentaPorContrato
        ///</summary>
        [ConfigurationProperty("DataClassReenviaEstadosCuentaPorContrato", DefaultValue = "Softv.DAO.ReenviaEstadosCuentaPorContratoData")]
        public String DataClass
        {
          get { return (string)base["DataClassReenviaEstadosCuentaPorContrato"]; }
        }

        /// <summary>
        /// Gets connection string for database ReenviaEstadosCuentaPorContrato access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
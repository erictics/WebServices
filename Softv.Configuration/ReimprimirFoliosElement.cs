﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ReimprimirFoliosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ReimprimirFolios class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ReimprimirFolios
        ///</summary>
        [ConfigurationProperty("DataClassReimprimirFolios", DefaultValue = "Softv.DAO.ReimprimirFoliosData")]
        public String DataClass
        {
          get { return (string)base["DataClassReimprimirFolios"]; }
        }

        /// <summary>
        /// Gets connection string for database ReimprimirFolios access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelCalleColoniaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelCalleColonia class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelCalleColonia
        ///</summary>
        [ConfigurationProperty("DataClassRelCalleColonia", DefaultValue = "Softv.DAO.RelCalleColoniaData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelCalleColonia"]; }
        }

        /// <summary>
        /// Gets connection string for database RelCalleColonia access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
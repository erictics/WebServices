﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelColoniaLocMunEstElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelColoniaLocMunEst class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelColoniaLocMunEst
        ///</summary>
        [ConfigurationProperty("DataClassRelColoniaLocMunEst", DefaultValue = "Softv.DAO.RelColoniaLocMunEstData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelColoniaLocMunEst"]; }
        }

        /// <summary>
        /// Gets connection string for database RelColoniaLocMunEst access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
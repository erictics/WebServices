﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelColoniaMedioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelColoniaMedio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelColoniaMedio
        ///</summary>
        [ConfigurationProperty("DataClassRelColoniaMedio", DefaultValue = "Softv.DAO.RelColoniaMedioData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelColoniaMedio"]; }
        }

        /// <summary>
        /// Gets connection string for database RelColoniaMedio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
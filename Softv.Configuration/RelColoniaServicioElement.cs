﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelColoniaServicioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelColoniaServicio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelColoniaServicio
        ///</summary>
        [ConfigurationProperty("DataClassRelColoniaServicio", DefaultValue = "Softv.DAO.RelColoniaServicioData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelColoniaServicio"]; }
        }

        /// <summary>
        /// Gets connection string for database RelColoniaServicio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
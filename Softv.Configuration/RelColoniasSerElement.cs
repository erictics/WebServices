﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelColoniasSerElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelColoniasSer class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelColoniasSer
        ///</summary>
        [ConfigurationProperty("DataClassRelColoniasSer", DefaultValue = "Softv.DAO.RelColoniasSerData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelColoniasSer"]; }
        }

        /// <summary>
        /// Gets connection string for database RelColoniasSer access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
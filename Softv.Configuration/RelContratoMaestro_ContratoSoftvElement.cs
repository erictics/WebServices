﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelContratoMaestro_ContratoSoftvElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelContratoMaestro_ContratoSoftv class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelContratoMaestro_ContratoSoftv
        ///</summary>
        [ConfigurationProperty("DataClassRelContratoMaestro_ContratoSoftv", DefaultValue = "Softv.DAO.RelContratoMaestro_ContratoSoftvData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelContratoMaestro_ContratoSoftv"]; }
        }

        /// <summary>
        /// Gets connection string for database RelContratoMaestro_ContratoSoftv access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
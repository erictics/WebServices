﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelMunicipioEstElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelMunicipioEst class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelMunicipioEst
        ///</summary>
        [ConfigurationProperty("DataClassRelMunicipioEst", DefaultValue = "Softv.DAO.RelMunicipioEstData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelMunicipioEst"]; }
        }

        /// <summary>
        /// Gets connection string for database RelMunicipioEst access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
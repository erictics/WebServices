﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelPlazaEstMunElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelPlazaEstMun class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelPlazaEstMun
        ///</summary>
        [ConfigurationProperty("DataClassRelPlazaEstMun", DefaultValue = "Softv.DAO.RelPlazaEstMunData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelPlazaEstMun"]; }
        }

        /// <summary>
        /// Gets connection string for database RelPlazaEstMun access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelRedCiudadElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelRedCiudad class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelRedCiudad
        ///</summary>
        [ConfigurationProperty("DataClassRelRedCiudad", DefaultValue = "Softv.DAO.RelRedCiudadData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelRedCiudad"]; }
        }

        /// <summary>
        /// Gets connection string for database RelRedCiudad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelRedEstadoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelRedEstado class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelRedEstado
        ///</summary>
        [ConfigurationProperty("DataClassRelRedEstado", DefaultValue = "Softv.DAO.RelRedEstadoData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelRedEstado"]; }
        }

        /// <summary>
        /// Gets connection string for database RelRedEstado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelRedLocalidadElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelRedLocalidad class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelRedLocalidad
        ///</summary>
        [ConfigurationProperty("DataClassRelRedLocalidad", DefaultValue = "Softv.DAO.RelRedLocalidadData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelRedLocalidad"]; }
        }

        /// <summary>
        /// Gets connection string for database RelRedLocalidad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
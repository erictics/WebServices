﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelRedMedioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelRedMedio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelRedMedio
        ///</summary>
        [ConfigurationProperty("DataClassRelRedMedio", DefaultValue = "Softv.DAO.RelRedMedioData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelRedMedio"]; }
        }

        /// <summary>
        /// Gets connection string for database RelRedMedio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
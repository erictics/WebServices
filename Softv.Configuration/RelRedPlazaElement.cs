﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelRedPlazaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelRedPlaza class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelRedPlaza
        ///</summary>
        [ConfigurationProperty("DataClassRelRedPlaza", DefaultValue = "Softv.DAO.RelRedPlazaData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelRedPlaza"]; }
        }

        /// <summary>
        /// Gets connection string for database RelRedPlaza access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
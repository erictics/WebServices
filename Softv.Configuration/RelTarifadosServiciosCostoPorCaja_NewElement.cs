﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelTarifadosServiciosCostoPorCaja_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelTarifadosServiciosCostoPorCaja_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelTarifadosServiciosCostoPorCaja_New
        ///</summary>
        [ConfigurationProperty("DataClassRelTarifadosServiciosCostoPorCaja_New", DefaultValue = "Softv.DAO.RelTarifadosServiciosCostoPorCaja_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelTarifadosServiciosCostoPorCaja_New"]; }
        }

        /// <summary>
        /// Gets connection string for database RelTarifadosServiciosCostoPorCaja_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
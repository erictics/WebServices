﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class RelTarifadosServiciosElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for RelTarifadosServicios class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for RelTarifadosServicios
        ///</summary>
        [ConfigurationProperty("DataClassRelTarifadosServicios", DefaultValue = "Softv.DAO.RelTarifadosServiciosData")]
        public String DataClass
        {
            get { return (string)base["DataClassRelTarifadosServicios"]; }
        }

        /// <summary>
        /// Gets connection string for database RelTarifadosServicios access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


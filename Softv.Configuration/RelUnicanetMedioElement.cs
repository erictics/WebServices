﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RelUnicanetMedioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RelUnicanetMedio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RelUnicanetMedio
        ///</summary>
        [ConfigurationProperty("DataClassRelUnicanetMedio", DefaultValue = "Softv.DAO.RelUnicanetMedioData")]
        public String DataClass
        {
          get { return (string)base["DataClassRelUnicanetMedio"]; }
        }

        /// <summary>
        /// Gets connection string for database RelUnicanetMedio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
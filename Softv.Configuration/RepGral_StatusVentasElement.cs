﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RepGral_StatusVentasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RepGral_StatusVentas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RepGral_StatusVentas
        ///</summary>
        [ConfigurationProperty("DataClassRepGral_StatusVentas", DefaultValue = "Softv.DAO.RepGral_StatusVentasData")]
        public String DataClass
        {
          get { return (string)base["DataClassRepGral_StatusVentas"]; }
        }

        /// <summary>
        /// Gets connection string for database RepGral_StatusVentas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
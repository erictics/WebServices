﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RepPlazaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RepPlaza class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RepPlaza
        ///</summary>
        [ConfigurationProperty("DataClassRepPlaza", DefaultValue = "Softv.DAO.RepPlazaData")]
        public String DataClass
        {
          get { return (string)base["DataClassRepPlaza"]; }
        }

        /// <summary>
        /// Gets connection string for database RepPlaza access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
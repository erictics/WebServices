﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ReporteCortesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ReporteCortes class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ReporteCortes
        ///</summary>
        [ConfigurationProperty("DataClassReporteCortes", DefaultValue = "Softv.DAO.ReporteCortesData")]
        public String DataClass
        {
          get { return (string)base["DataClassReporteCortes"]; }
        }

        /// <summary>
        /// Gets connection string for database ReporteCortes access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ReporteEstadoCuentaNuevoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ReporteEstadoCuentaNuevo class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ReporteEstadoCuentaNuevo
        ///</summary>
        [ConfigurationProperty("DataClassReporteEstadoCuentaNuevo", DefaultValue = "Softv.DAO.ReporteEstadoCuentaNuevoData")]
        public String DataClass
        {
          get { return (string)base["DataClassReporteEstadoCuentaNuevo"]; }
        }

        /// <summary>
        /// Gets connection string for database ReporteEstadoCuentaNuevo access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
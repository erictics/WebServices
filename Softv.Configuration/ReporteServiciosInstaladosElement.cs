﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ReporteServiciosInstaladosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ReporteServiciosInstalados class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ReporteServiciosInstalados
        ///</summary>
        [ConfigurationProperty("DataClassReporteServiciosInstalados", DefaultValue = "Softv.DAO.ReporteServiciosInstaladosData")]
        public String DataClass
        {
          get { return (string)base["DataClassReporteServiciosInstalados"]; }
        }

        /// <summary>
        /// Gets connection string for database ReporteServiciosInstalados access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
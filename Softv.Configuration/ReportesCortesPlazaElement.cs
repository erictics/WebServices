﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ReportesCortesPlazaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ReportesCortesPlaza class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ReportesCortesPlaza
        ///</summary>
        [ConfigurationProperty("DataClassReportesCortesPlaza", DefaultValue = "Softv.DAO.ReportesCortesPlazaData")]
        public String DataClass
        {
          get { return (string)base["DataClassReportesCortesPlaza"]; }
        }

        /// <summary>
        /// Gets connection string for database ReportesCortesPlaza access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
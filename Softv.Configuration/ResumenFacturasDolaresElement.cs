﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ResumenFacturasDolaresElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ResumenFacturasDolares class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ResumenFacturasDolares
        ///</summary>
        [ConfigurationProperty("DataClassResumenFacturasDolares", DefaultValue = "Softv.DAO.ResumenFacturasDolaresData")]
        public String DataClass
        {
          get { return (string)base["DataClassResumenFacturasDolares"]; }
        }

        /// <summary>
        /// Gets connection string for database ResumenFacturasDolares access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RoboDeSenalElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RoboDeSenal class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RoboDeSenal
        ///</summary>
        [ConfigurationProperty("DataClassRoboDeSenal", DefaultValue = "Softv.DAO.RoboDeSenalData")]
        public String DataClass
        {
          get { return (string)base["DataClassRoboDeSenal"]; }
        }

        /// <summary>
        /// Gets connection string for database RoboDeSenal access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
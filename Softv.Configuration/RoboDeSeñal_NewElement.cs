﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class RoboDeSeñal_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for RoboDeSeñal_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for RoboDeSeñal_New
        ///</summary>
        [ConfigurationProperty("DataClassRoboDeSeñal_New", DefaultValue = "Softv.DAO.RoboDeSeñal_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassRoboDeSeñal_New"]; }
        }

        /// <summary>
        /// Gets connection string for database RoboDeSeñal_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
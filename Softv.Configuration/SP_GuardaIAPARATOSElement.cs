﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class SP_GuardaIAPARATOSElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for SP_GuardaIAPARATOS class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for SP_GuardaIAPARATOS
        ///</summary>
        [ConfigurationProperty("DataClassSP_GuardaIAPARATOS", DefaultValue = "Softv.DAO.SP_GuardaIAPARATOSData")]
        public String DataClass
        {
          get { return (string)base["DataClassSP_GuardaIAPARATOS"]; }
        }

        /// <summary>
        /// Gets connection string for database SP_GuardaIAPARATOS access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
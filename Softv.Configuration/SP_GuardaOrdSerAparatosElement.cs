﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class SP_GuardaOrdSerAparatosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for SP_GuardaOrdSerAparatos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for SP_GuardaOrdSerAparatos
        ///</summary>
        [ConfigurationProperty("DataClassSP_GuardaOrdSerAparatos", DefaultValue = "Softv.DAO.SP_GuardaOrdSerAparatosData")]
        public String DataClass
        {
          get { return (string)base["DataClassSP_GuardaOrdSerAparatos"]; }
        }

        /// <summary>
        /// Gets connection string for database SP_GuardaOrdSerAparatos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class SP_InsertaTbl_NoEntregadosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for SP_InsertaTbl_NoEntregados class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for SP_InsertaTbl_NoEntregados
        ///</summary>
        [ConfigurationProperty("DataClassSP_InsertaTbl_NoEntregados", DefaultValue = "Softv.DAO.SP_InsertaTbl_NoEntregadosData")]
        public String DataClass
        {
          get { return (string)base["DataClassSP_InsertaTbl_NoEntregados"]; }
        }

        /// <summary>
        /// Gets connection string for database SP_InsertaTbl_NoEntregados access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
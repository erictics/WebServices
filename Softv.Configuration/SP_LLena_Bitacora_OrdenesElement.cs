﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class SP_LLena_Bitacora_OrdenesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for SP_LLena_Bitacora_Ordenes class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for SP_LLena_Bitacora_Ordenes
        ///</summary>
        [ConfigurationProperty("DataClassSP_LLena_Bitacora_Ordenes", DefaultValue = "Softv.DAO.SP_LLena_Bitacora_OrdenesData")]
        public String DataClass
        {
          get { return (string)base["DataClassSP_LLena_Bitacora_Ordenes"]; }
        }

        /// <summary>
        /// Gets connection string for database SP_LLena_Bitacora_Ordenes access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
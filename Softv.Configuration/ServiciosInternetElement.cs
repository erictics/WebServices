﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ServiciosInternetElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ServiciosInternet class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ServiciosInternet
        ///</summary>
        [ConfigurationProperty("DataClassServiciosInternet", DefaultValue = "Softv.DAO.ServiciosInternetData")]
        public String DataClass
        {
          get { return (string)base["DataClassServiciosInternet"]; }
        }

        /// <summary>
        /// Gets connection string for database ServiciosInternet access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
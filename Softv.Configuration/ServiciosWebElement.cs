﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ServiciosWebElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ServiciosWeb class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ServiciosWeb
        ///</summary>
        [ConfigurationProperty("DataClassServiciosWeb", DefaultValue = "Softv.DAO.ServiciosWebData")]
        public String DataClass
        {
          get { return (string)base["DataClassServiciosWeb"]; }
        }

        /// <summary>
        /// Gets connection string for database ServiciosWeb access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
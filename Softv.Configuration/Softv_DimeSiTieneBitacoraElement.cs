﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Softv_DimeSiTieneBitacoraElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Softv_DimeSiTieneBitacora class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Softv_DimeSiTieneBitacora
        ///</summary>
        [ConfigurationProperty("DataClassSoftv_DimeSiTieneBitacora", DefaultValue = "Softv.DAO.Softv_DimeSiTieneBitacoraData")]
        public String DataClass
        {
          get { return (string)base["DataClassSoftv_DimeSiTieneBitacora"]; }
        }

        /// <summary>
        /// Gets connection string for database Softv_DimeSiTieneBitacora access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
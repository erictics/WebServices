﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Softv_ExistenciasTecnicoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Softv_ExistenciasTecnico class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Softv_ExistenciasTecnico
        ///</summary>
        [ConfigurationProperty("DataClassSoftv_ExistenciasTecnico", DefaultValue = "Softv.DAO.Softv_ExistenciasTecnicoData")]
        public String DataClass
        {
          get { return (string)base["DataClassSoftv_ExistenciasTecnico"]; }
        }

        /// <summary>
        /// Gets connection string for database Softv_ExistenciasTecnico access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
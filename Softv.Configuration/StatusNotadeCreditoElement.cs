﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class StatusNotadeCreditoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for StatusNotadeCredito class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for StatusNotadeCredito
        ///</summary>
        [ConfigurationProperty("DataClassStatusNotadeCredito", DefaultValue = "Softv.DAO.StatusNotadeCreditoData")]
        public String DataClass
        {
          get { return (string)base["DataClassStatusNotadeCredito"]; }
        }

        /// <summary>
        /// Gets connection string for database StatusNotadeCredito access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
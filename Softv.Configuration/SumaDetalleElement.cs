﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class SumaDetalleElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for SumaDetalle class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for SumaDetalle
        ///</summary>
        [ConfigurationProperty("DataClassSumaDetalle", DefaultValue = "Softv.DAO.SumaDetalleData")]
        public String DataClass
        {
            get { return (string)base["DataClassSumaDetalle"]; }
        }

        /// <summary>
        /// Gets connection string for database SumaDetalle access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


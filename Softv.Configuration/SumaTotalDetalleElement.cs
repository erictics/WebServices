﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class SumaTotalDetalleElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for SumaTotalDetalle class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for SumaTotalDetalle
        ///</summary>
        [ConfigurationProperty("DataClassSumaTotalDetalle", DefaultValue = "Softv.DAO.SumaTotalDetalleData")]
        public String DataClass
        {
          get { return (string)base["DataClassSumaTotalDetalle"]; }
        }

        /// <summary>
        /// Gets connection string for database SumaTotalDetalle access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
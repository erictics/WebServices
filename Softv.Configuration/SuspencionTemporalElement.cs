﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class SuspencionTemporalElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for SuspencionTemporal class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for SuspencionTemporal
        ///</summary>
        [ConfigurationProperty("DataClassSuspencionTemporal", DefaultValue = "Softv.DAO.SuspencionTemporalData")]
        public String DataClass
        {
          get { return (string)base["DataClassSuspencionTemporal"]; }
        }

        /// <summary>
        /// Gets connection string for database SuspencionTemporal access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
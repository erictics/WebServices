﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class T1RepArqueoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for T1RepArqueo class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for T1RepArqueo
        ///</summary>
        [ConfigurationProperty("DataClassT1RepArqueo", DefaultValue = "Softv.DAO.T1RepArqueoData")]
        public String DataClass
        {
          get { return (string)base["DataClassT1RepArqueo"]; }
        }

        /// <summary>
        /// Gets connection string for database T1RepArqueo access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class TblFacturasOpcionesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for TblFacturasOpciones class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for TblFacturasOpciones
        ///</summary>
        [ConfigurationProperty("DataClassTblFacturasOpciones", DefaultValue = "Softv.DAO.TblFacturasOpcionesData")]
        public String DataClass
        {
          get { return (string)base["DataClassTblFacturasOpciones"]; }
        }

        /// <summary>
        /// Gets connection string for database TblFacturasOpciones access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
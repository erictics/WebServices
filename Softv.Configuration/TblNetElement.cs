﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class TblNetElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for TblNet class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for TblNet
        ///</summary>
        [ConfigurationProperty("DataClassTblNet", DefaultValue = "Softv.DAO.TblNetData")]
        public String DataClass
        {
          get { return (string)base["DataClassTblNet"]; }
        }

        /// <summary>
        /// Gets connection string for database TblNet access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
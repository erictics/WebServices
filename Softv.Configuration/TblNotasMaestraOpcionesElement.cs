﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class TblNotasMaestraOpcionesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for TblNotasMaestraOpciones class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for TblNotasMaestraOpciones
        ///</summary>
        [ConfigurationProperty("DataClassTblNotasMaestraOpciones", DefaultValue = "Softv.DAO.TblNotasMaestraOpcionesData")]
        public String DataClass
        {
          get { return (string)base["DataClassTblNotasMaestraOpciones"]; }
        }

        /// <summary>
        /// Gets connection string for database TblNotasMaestraOpciones access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
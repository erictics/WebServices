﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class TipoDeCambioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for TipoDeCambio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for TipoDeCambio
        ///</summary>
        [ConfigurationProperty("DataClassTipoDeCambio", DefaultValue = "Softv.DAO.TipoDeCambioData")]
        public String DataClass
        {
          get { return (string)base["DataClassTipoDeCambio"]; }
        }

        /// <summary>
        /// Gets connection string for database TipoDeCambio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
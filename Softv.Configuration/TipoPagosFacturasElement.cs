﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class TipoPagosFacturasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for TipoPagosFacturas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for TipoPagosFacturas
        ///</summary>
        [ConfigurationProperty("DataClassTipoPagosFacturas", DefaultValue = "Softv.DAO.TipoPagosFacturasData")]
        public String DataClass
        {
          get { return (string)base["DataClassTipoPagosFacturas"]; }
        }

        /// <summary>
        /// Gets connection string for database TipoPagosFacturas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
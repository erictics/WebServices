﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class TipoTarjetaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for TipoTarjeta class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for TipoTarjeta
        ///</summary>
        [ConfigurationProperty("DataClassTipoTarjeta", DefaultValue = "Softv.DAO.TipoTarjetaData")]
        public String DataClass
        {
          get { return (string)base["DataClassTipoTarjeta"]; }
        }

        /// <summary>
        /// Gets connection string for database TipoTarjeta access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
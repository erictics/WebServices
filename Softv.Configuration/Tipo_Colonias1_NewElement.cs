﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Tipo_Colonias1_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Tipo_Colonias1_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Tipo_Colonias1_New
        ///</summary>
        [ConfigurationProperty("DataClassTipo_Colonias1_New", DefaultValue = "Softv.DAO.Tipo_Colonias1_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassTipo_Colonias1_New"]; }
        }

        /// <summary>
        /// Gets connection string for database Tipo_Colonias1_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
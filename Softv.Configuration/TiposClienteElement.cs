﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class TiposClienteElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for TiposCliente class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for TiposCliente
        ///</summary>
        [ConfigurationProperty("DataClassTiposCliente", DefaultValue = "Softv.DAO.TiposClienteData")]
        public String DataClass
        {
          get { return (string)base["DataClassTiposCliente"]; }
        }

        /// <summary>
        /// Gets connection string for database TiposCliente access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class UltimoSerieYFolioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for UltimoSerieYFolio class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for UltimoSerieYFolio
        ///</summary>
        [ConfigurationProperty("DataClassUltimoSerieYFolio", DefaultValue = "Softv.DAO.UltimoSerieYFolioData")]
        public String DataClass
        {
          get { return (string)base["DataClassUltimoSerieYFolio"]; }
        }

        /// <summary>
        /// Gets connection string for database UltimoSerieYFolio access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Ultimo_SERIEYFOLIOElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Ultimo_SERIEYFOLIO class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Ultimo_SERIEYFOLIO
        ///</summary>
        [ConfigurationProperty("DataClassUltimo_SERIEYFOLIO", DefaultValue = "Softv.DAO.Ultimo_SERIEYFOLIOData")]
        public String DataClass
        {
          get { return (string)base["DataClassUltimo_SERIEYFOLIO"]; }
        }

        /// <summary>
        /// Gets connection string for database Ultimo_SERIEYFOLIO access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
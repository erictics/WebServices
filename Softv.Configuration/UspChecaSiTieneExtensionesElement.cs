﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class UspChecaSiTieneExtensionesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for UspChecaSiTieneExtensiones class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for UspChecaSiTieneExtensiones
        ///</summary>
        [ConfigurationProperty("DataClassUspChecaSiTieneExtensiones", DefaultValue = "Softv.DAO.UspChecaSiTieneExtensionesData")]
        public String DataClass
        {
          get { return (string)base["DataClassUspChecaSiTieneExtensiones"]; }
        }

        /// <summary>
        /// Gets connection string for database UspChecaSiTieneExtensiones access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class UspLlenaComboExtensionesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for UspLlenaComboExtensiones class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for UspLlenaComboExtensiones
        ///</summary>
        [ConfigurationProperty("DataClassUspLlenaComboExtensiones", DefaultValue = "Softv.DAO.UspLlenaComboExtensionesData")]
        public String DataClass
        {
          get { return (string)base["DataClassUspLlenaComboExtensiones"]; }
        }

        /// <summary>
        /// Gets connection string for database UspLlenaComboExtensiones access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
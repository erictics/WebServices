﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class VALIDACatalogoSeriesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for VALIDACatalogoSeries class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for VALIDACatalogoSeries
        ///</summary>
        [ConfigurationProperty("DataClassVALIDACatalogoSeries", DefaultValue = "Softv.DAO.VALIDACatalogoSeriesData")]
        public String DataClass
        {
          get { return (string)base["DataClassVALIDACatalogoSeries"]; }
        }

        /// <summary>
        /// Gets connection string for database VALIDACatalogoSeries access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
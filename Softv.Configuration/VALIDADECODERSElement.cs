﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class VALIDADECODERSElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for VALIDADECODERS class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for VALIDADECODERS
        ///</summary>
        [ConfigurationProperty("DataClassVALIDADECODERS", DefaultValue = "Softv.DAO.VALIDADECODERSData")]
        public String DataClass
        {
          get { return (string)base["DataClassVALIDADECODERS"]; }
        }

        /// <summary>
        /// Gets connection string for database VALIDADECODERS access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
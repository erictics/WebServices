﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class VALIDAOrdenQuejaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for VALIDAOrdenQueja class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for VALIDAOrdenQueja
        ///</summary>
        [ConfigurationProperty("DataClassVALIDAOrdenQueja", DefaultValue = "Softv.DAO.VALIDAOrdenQuejaData")]
        public String DataClass
        {
          get { return (string)base["DataClassVALIDAOrdenQueja"]; }
        }

        /// <summary>
        /// Gets connection string for database VALIDAOrdenQueja access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
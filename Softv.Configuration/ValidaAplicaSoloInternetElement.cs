﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaAplicaSoloInternetElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaAplicaSoloInternet class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaAplicaSoloInternet
        ///</summary>
        [ConfigurationProperty("DataClassValidaAplicaSoloInternet", DefaultValue = "Softv.DAO.ValidaAplicaSoloInternetData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaAplicaSoloInternet"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaAplicaSoloInternet access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaCambioDClvtxtServElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaCambioDClvtxtServ class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaCambioDClvtxtServ
        ///</summary>
        [ConfigurationProperty("DataClassValidaCambioDClvtxtServ", DefaultValue = "Softv.DAO.ValidaCambioDClvtxtServData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaCambioDClvtxtServ"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaCambioDClvtxtServ access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
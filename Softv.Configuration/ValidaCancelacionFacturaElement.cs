﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaCancelacionFacturaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaCancelacionFactura class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaCancelacionFactura
        ///</summary>
        [ConfigurationProperty("DataClassValidaCancelacionFactura", DefaultValue = "Softv.DAO.ValidaCancelacionFacturaData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaCancelacionFactura"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaCancelacionFactura access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
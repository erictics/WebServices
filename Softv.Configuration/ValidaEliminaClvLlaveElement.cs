﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaEliminaClvLlaveElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaEliminaClvLlave class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaEliminaClvLlave
        ///</summary>
        [ConfigurationProperty("DataClassValidaEliminaClvLlave", DefaultValue = "Softv.DAO.ValidaEliminaClvLlaveData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaEliminaClvLlave"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaEliminaClvLlave access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaEliminaRelLocalidadCiudadElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaEliminaRelLocalidadCiudad class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaEliminaRelLocalidadCiudad
        ///</summary>
        [ConfigurationProperty("DataClassValidaEliminaRelLocalidadCiudad", DefaultValue = "Softv.DAO.ValidaEliminaRelLocalidadCiudadData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaEliminaRelLocalidadCiudad"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaEliminaRelLocalidadCiudad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaFacturaFiscalElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaFacturaFiscal class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaFacturaFiscal
        ///</summary>
        [ConfigurationProperty("DataClassValidaFacturaFiscal", DefaultValue = "Softv.DAO.ValidaFacturaFiscalData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaFacturaFiscal"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaFacturaFiscal access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
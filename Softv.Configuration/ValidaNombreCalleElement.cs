﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaNombreCalleElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaNombreCalle class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaNombreCalle
        ///</summary>
        [ConfigurationProperty("DataClassValidaNombreCalle", DefaultValue = "Softv.DAO.ValidaNombreCalleData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaNombreCalle"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaNombreCalle access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
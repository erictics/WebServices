﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaNombreColoniaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaNombreColonia class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaNombreColonia
        ///</summary>
        [ConfigurationProperty("DataClassValidaNombreColonia", DefaultValue = "Softv.DAO.ValidaNombreColoniaData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaNombreColonia"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaNombreColonia access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
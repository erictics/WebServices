﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaOrdSerManualesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaOrdSerManuales class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaOrdSerManuales
        ///</summary>
        [ConfigurationProperty("DataClassValidaOrdSerManuales", DefaultValue = "Softv.DAO.ValidaOrdSerManualesData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaOrdSerManuales"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaOrdSerManuales access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
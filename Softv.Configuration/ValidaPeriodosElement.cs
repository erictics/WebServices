﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaPeriodosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaPeriodos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaPeriodos
        ///</summary>
        [ConfigurationProperty("DataClassValidaPeriodos", DefaultValue = "Softv.DAO.ValidaPeriodosData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaPeriodos"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaPeriodos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
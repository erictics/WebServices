﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaPideAparatosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaPideAparatos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaPideAparatos
        ///</summary>
        [ConfigurationProperty("DataClassValidaPideAparatos", DefaultValue = "Softv.DAO.ValidaPideAparatosData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaPideAparatos"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaPideAparatos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaPideSiTieneMasAparatosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaPideSiTieneMasAparatos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaPideSiTieneMasAparatos
        ///</summary>
        [ConfigurationProperty("DataClassValidaPideSiTieneMasAparatos", DefaultValue = "Softv.DAO.ValidaPideSiTieneMasAparatosData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaPideSiTieneMasAparatos"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaPideSiTieneMasAparatos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
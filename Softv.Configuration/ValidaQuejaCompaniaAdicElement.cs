﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaQuejaCompaniaAdicElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaQuejaCompaniaAdic class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaQuejaCompaniaAdic
        ///</summary>
        [ConfigurationProperty("DataClassValidaQuejaCompaniaAdic", DefaultValue = "Softv.DAO.ValidaQuejaCompaniaAdicData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaQuejaCompaniaAdic"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaQuejaCompaniaAdic access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
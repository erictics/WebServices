﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaReprocesoPorContratoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaReprocesoPorContrato class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaReprocesoPorContrato
        ///</summary>
        [ConfigurationProperty("DataClassValidaReprocesoPorContrato", DefaultValue = "Softv.DAO.ValidaReprocesoPorContratoData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaReprocesoPorContrato"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaReprocesoPorContrato access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaSaldoContratoElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaSaldoContrato class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaSaldoContrato
        ///</summary>
        [ConfigurationProperty("DataClassValidaSaldoContrato", DefaultValue = "Softv.DAO.ValidaSaldoContratoData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaSaldoContrato"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaSaldoContrato access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
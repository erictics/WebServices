﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaSiContratoExiste_CMElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaSiContratoExiste_CM class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaSiContratoExiste_CM
        ///</summary>
        [ConfigurationProperty("DataClassValidaSiContratoExiste_CM", DefaultValue = "Softv.DAO.ValidaSiContratoExiste_CMData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaSiContratoExiste_CM"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaSiContratoExiste_CM access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidaTrabajosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidaTrabajos class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidaTrabajos
        ///</summary>
        [ConfigurationProperty("DataClassValidaTrabajos", DefaultValue = "Softv.DAO.ValidaTrabajosData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidaTrabajos"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidaTrabajos access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
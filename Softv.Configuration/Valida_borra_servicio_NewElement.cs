﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Valida_borra_servicio_NewElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Valida_borra_servicio_New class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Valida_borra_servicio_New
        ///</summary>
        [ConfigurationProperty("DataClassValida_borra_servicio_New", DefaultValue = "Softv.DAO.Valida_borra_servicio_NewData")]
        public String DataClass
        {
          get { return (string)base["DataClassValida_borra_servicio_New"]; }
        }

        /// <summary>
        /// Gets connection string for database Valida_borra_servicio_New access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
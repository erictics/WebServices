﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class ValidacionLoginCajeraElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for ValidacionLoginCajera class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for ValidacionLoginCajera
        ///</summary>
        [ConfigurationProperty("DataClassValidacionLoginCajera", DefaultValue = "Softv.DAO.ValidacionLoginCajeraData")]
        public String DataClass
        {
          get { return (string)base["DataClassValidacionLoginCajera"]; }
        }

        /// <summary>
        /// Gets connection string for database ValidacionLoginCajera access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
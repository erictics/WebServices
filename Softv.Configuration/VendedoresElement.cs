﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class VendedoresElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Vendedores class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Vendedores
        ///</summary>
        [ConfigurationProperty("DataClassVendedores", DefaultValue = "Softv.DAO.VendedoresData")]
        public String DataClass
        {
          get { return (string)base["DataClassVendedores"]; }
        }

        /// <summary>
        /// Gets connection string for database Vendedores access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
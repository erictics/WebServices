﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class Vendores_ReportesVentasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for Vendores_ReportesVentas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for Vendores_ReportesVentas
        ///</summary>
        [ConfigurationProperty("DataClassVendores_ReportesVentas", DefaultValue = "Softv.DAO.Vendores_ReportesVentasData")]
        public String DataClass
        {
          get { return (string)base["DataClassVendores_ReportesVentas"]; }
        }

        /// <summary>
        /// Gets connection string for database Vendores_ReportesVentas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
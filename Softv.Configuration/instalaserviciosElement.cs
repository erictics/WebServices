﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class instalaserviciosElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for instalaservicios class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for instalaservicios
        ///</summary>
        [ConfigurationProperty("DataClassinstalaservicios", DefaultValue = "Softv.DAO.instalaserviciosData")]
        public String DataClass
        {
          get { return (string)base["DataClassinstalaservicios"]; }
        }

        /// <summary>
        /// Gets connection string for database instalaservicios access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
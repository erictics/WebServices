﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class muestraCP_ColoniaLocalidadElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for muestraCP_ColoniaLocalidad class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for muestraCP_ColoniaLocalidad
        ///</summary>
        [ConfigurationProperty("DataClassmuestraCP_ColoniaLocalidad", DefaultValue = "Softv.DAO.muestraCP_ColoniaLocalidadData")]
        public String DataClass
        {
          get { return (string)base["DataClassmuestraCP_ColoniaLocalidad"]; }
        }

        /// <summary>
        /// Gets connection string for database muestraCP_ColoniaLocalidad access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
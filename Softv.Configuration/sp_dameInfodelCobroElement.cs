﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class sp_dameInfodelCobroElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for sp_dameInfodelCobro class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for sp_dameInfodelCobro
        ///</summary>
        [ConfigurationProperty("DataClasssp_dameInfodelCobro", DefaultValue = "Softv.DAO.sp_dameInfodelCobroData")]
        public String DataClass
        {
          get { return (string)base["DataClasssp_dameInfodelCobro"]; }
        }

        /// <summary>
        /// Gets connection string for database sp_dameInfodelCobro access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
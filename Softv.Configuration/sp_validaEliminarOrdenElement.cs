﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class sp_validaEliminarOrdenElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for sp_validaEliminarOrden class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for sp_validaEliminarOrden
        ///</summary>
        [ConfigurationProperty("DataClasssp_validaEliminarOrden", DefaultValue = "Softv.DAO.sp_validaEliminarOrdenData")]
        public String DataClass
        {
          get { return (string)base["DataClasssp_validaEliminarOrden"]; }
        }

        /// <summary>
        /// Gets connection string for database sp_validaEliminarOrden access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
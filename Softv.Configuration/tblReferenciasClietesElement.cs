﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class tblReferenciasClietesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for tblReferenciasClietes class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for tblReferenciasClietes
        ///</summary>
        [ConfigurationProperty("DataClasstblReferenciasClietes", DefaultValue = "Softv.DAO.tblReferenciasClietesData")]
        public String DataClass
        {
          get { return (string)base["DataClasstblReferenciasClietes"]; }
        }

        /// <summary>
        /// Gets connection string for database tblReferenciasClietes access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
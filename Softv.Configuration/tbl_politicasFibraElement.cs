﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class tbl_politicasFibraElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for tbl_politicasFibra class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for tbl_politicasFibra
        ///</summary>
        [ConfigurationProperty("DataClasstbl_politicasFibra", DefaultValue = "Softv.DAO.tbl_politicasFibraData")]
        public String DataClass
        {
          get { return (string)base["DataClasstbl_politicasFibra"]; }
        }

        /// <summary>
        /// Gets connection string for database tbl_politicasFibra access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
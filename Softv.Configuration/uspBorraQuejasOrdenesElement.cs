﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class uspBorraQuejasOrdenesElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for uspBorraQuejasOrdenes class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for uspBorraQuejasOrdenes
        ///</summary>
        [ConfigurationProperty("DataClassuspBorraQuejasOrdenes", DefaultValue = "Softv.DAO.uspBorraQuejasOrdenesData")]
        public String DataClass
        {
          get { return (string)base["DataClassuspBorraQuejasOrdenes"]; }
        }

        /// <summary>
        /// Gets connection string for database uspBorraQuejasOrdenes access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
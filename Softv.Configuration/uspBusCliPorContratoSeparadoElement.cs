﻿
using System;
using System.Configuration;

namespace SoftvConfiguration
{
    public class uspBusCliPorContratoSeparadoElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for uspBusCliPorContratoSeparado class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                SoftvSettings.Settings.Assembly :
                (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for uspBusCliPorContratoSeparado
        ///</summary>
        [ConfigurationProperty("DataClassuspBusCliPorContratoSeparado", DefaultValue = "Softv.DAO.uspBusCliPorContratoSeparadoData")]
        public String DataClass
        {
            get { return (string)base["DataClassuspBusCliPorContratoSeparado"]; }
        }

        /// <summary>
        /// Gets connection string for database uspBusCliPorContratoSeparado access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString : (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}


﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class uspBuscaOrdSer_BuscaOrdSerSeparado2Element: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for uspBuscaOrdSer_BuscaOrdSerSeparado2 class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for uspBuscaOrdSer_BuscaOrdSerSeparado2
        ///</summary>
        [ConfigurationProperty("DataClassuspBuscaOrdSer_BuscaOrdSerSeparado2", DefaultValue = "Softv.DAO.uspBuscaOrdSer_BuscaOrdSerSeparado2Data")]
        public String DataClass
        {
          get { return (string)base["DataClassuspBuscaOrdSer_BuscaOrdSerSeparado2"]; }
        }

        /// <summary>
        /// Gets connection string for database uspBuscaOrdSer_BuscaOrdSerSeparado2 access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
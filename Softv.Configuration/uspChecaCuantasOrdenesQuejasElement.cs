﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class uspChecaCuantasOrdenesQuejasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for uspChecaCuantasOrdenesQuejas class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for uspChecaCuantasOrdenesQuejas
        ///</summary>
        [ConfigurationProperty("DataClassuspChecaCuantasOrdenesQuejas", DefaultValue = "Softv.DAO.uspChecaCuantasOrdenesQuejasData")]
        public String DataClass
        {
          get { return (string)base["DataClassuspChecaCuantasOrdenesQuejas"]; }
        }

        /// <summary>
        /// Gets connection string for database uspChecaCuantasOrdenesQuejas access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
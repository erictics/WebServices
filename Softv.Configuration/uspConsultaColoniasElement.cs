﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class uspConsultaColoniasElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for uspConsultaColonias class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for uspConsultaColonias
        ///</summary>
        [ConfigurationProperty("DataClassuspConsultaColonias", DefaultValue = "Softv.DAO.uspConsultaColoniasData")]
        public String DataClass
        {
          get { return (string)base["DataClassuspConsultaColonias"]; }
        }

        /// <summary>
        /// Gets connection string for database uspConsultaColonias access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class uspConsultaColoniasPorUsuarioElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for uspConsultaColoniasPorUsuario class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for uspConsultaColoniasPorUsuario
        ///</summary>
        [ConfigurationProperty("DataClassuspConsultaColoniasPorUsuario", DefaultValue = "Softv.DAO.uspConsultaColoniasPorUsuarioData")]
        public String DataClass
        {
          get { return (string)base["DataClassuspConsultaColoniasPorUsuario"]; }
        }

        /// <summary>
        /// Gets connection string for database uspConsultaColoniasPorUsuario access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
﻿
    using System;
    using System.Configuration;

    namespace SoftvConfiguration
    {
      public class uspHaz_PreguntaElement: ConfigurationElement
      {
        /// <summary>
        /// Gets assembly name for uspHaz_Pregunta class
        /// </summary>
        [ConfigurationProperty( "Assembly")]
        public String Assembly
        {
          get
          {
            string assembly = (string)base["Assembly"];
            assembly = String.IsNullOrEmpty(assembly) ?
            SoftvSettings.Settings.Assembly :
            (string)base["Assembly"];
            return assembly;
          }
        }

        /// <summary>
        /// Gets class name for uspHaz_Pregunta
        ///</summary>
        [ConfigurationProperty("DataClassuspHaz_Pregunta", DefaultValue = "Softv.DAO.uspHaz_PreguntaData")]
        public String DataClass
        {
          get { return (string)base["DataClassuspHaz_Pregunta"]; }
        }

        /// <summary>
        /// Gets connection string for database uspHaz_Pregunta access
        ///</summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
          get
          {
            string connectionString = (string)base["ConnectionString"];
            connectionString = String.IsNullOrEmpty(connectionString) ? SoftvSettings.Settings.ConnectionString :  (string)base["ConnectionString"];
            return connectionString;
          }
        }
      }
    }

  
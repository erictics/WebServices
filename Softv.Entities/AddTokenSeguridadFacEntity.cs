﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.AddTokenSeguridadFacEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : AddTokenSeguridadFac entity
    /// File                    : AddTokenSeguridadFacEntity.cs
    /// Creation date           : 16/11/2016
    /// Creation time           : 06:33 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class AddTokenSeguridadFacEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Token1
        /// </summary>
        [DataMember]
        public String Token1 { get; set; }
        /// <summary>
        /// Property Token2
        /// </summary>
        [DataMember]
        public String Token2 { get; set; }
        /// <summary>
        /// Property IdUsuario
        /// </summary>
        [DataMember]
        public int? IdUsuario { get; set; }
        /// <summary>
        /// Property Usuario
        /// </summary>
        [DataMember]
        public String Usuario { get; set; }
        /// <summary>
        /// Property IdSucursal
        /// </summary>
        [DataMember]
        public int? IdSucursal { get; set; }
        /// <summary>
        /// Property IpMaquina
        /// </summary>
        [DataMember]
        public String IpMaquina { get; set; }
        /// <summary>
        /// Property Clv_Factura
        /// </summary>
        //[DataMember]
        //public long? Clv_Factura { get; set; }
        ///// <summary>
        ///// Property Ticket
        ///// </summary>
        //[DataMember]
        //public bool? Ticket { get; set; }
        ///// <summary>
        ///// Property Mizar
        ///// </summary>
        //[DataMember]
        //public bool? Mizar { get; set; }

        /// <summary>
        /// Property ClvSession
        /// </summary>
        [DataMember]
        public long? ClvSession { get; set; }
        #endregion
    }
}


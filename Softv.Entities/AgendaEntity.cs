﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class AgendaEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public String Tipo { get; set; }

        [DataMember]
        public String Estado { get; set; }

        [DataMember]
        public int? Clv_OrdRep { get; set; }

        [DataMember]
        public String Contrato { get; set; }

        [DataMember]
        public String FECHA { get; set; }

        [DataMember]
        public String FechaOrdRep { get; set; }

        [DataMember]
        public String Turno { get; set; }

        [DataMember]
        public int? No_Cita { get; set; }

        [DataMember]
        public int? CLV_TECNICO { get; set; }

        [DataMember]
        public String NOMBRE { get; set; }

        [DataMember]
        public String ApellidoPaterno { get; set; }

        [DataMember]
        public String ApellidoMaterno { get; set; }

        [DataMember]
        public String Hora { get; set; }

        [DataMember]
        public String Status { get; set; }

        [DataMember]
        public int? Sector { get; set; }

        [DataMember]
        public int? idcompania { get; set; }

        [DataMember]
        public int? ClvUsuario { get; set; }

        [DataMember]
        public int? opSetupBoxTarjeta { get; set; }

        [DataMember]
        public String SetUpBox { get; set; }

        [DataMember]
        public int? CONTRATO { get; set; }

        [DataMember]
        public long CLV_CITA { get; set; }

        [DataMember]
        public long clv_queja { get; set; }

        [DataMember]
        public long clv_orden { get; set; }
        #endregion
    }

    public class IdentificacionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public string TipoIdentificacion { get; set; }

        [DataMember]
        public long? Contrato { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int? Op { get; set; }

        #endregion
    }
}

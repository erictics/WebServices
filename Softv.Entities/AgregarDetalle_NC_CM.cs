﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Softv.Entities
{
    public class AgregarDetalle_NC_CM
    {

        public int? Clv_Detalle { get; set; }

        public decimal? Importe { get; set; }

        public List<AgregarDetalle_NC_CM> Lista { get; set; }

    }
}

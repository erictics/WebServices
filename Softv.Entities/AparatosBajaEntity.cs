﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class AparatosBajaEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? ContratoNet { get; set; }

        [DataMember]
        public int? Clv_UnicaNet { get; set; }

        [DataMember]
        public int? Clv_CableModem { get; set; }

        [DataMember]
        public String MacCableModem { get; set; }

        [DataMember]
        public int? ClvCatSerie { get; set; }

        [DataMember]
        public int? IdInventario { get; set; }

        [DataMember]
        public int? IdArticulo { get; set; }

        [DataMember]
        public String Descripcion { get; set; }

        [DataMember]
        public int? IdServicio { get; set; }

        [DataMember]
        public int? IdMedio { get; set; }

        #endregion
    }
}

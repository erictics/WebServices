﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    public class ArbolAparatosRecontratacionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? IdReconAparato { get; set; }

        [DataMember]
        public long? Clv_UnicaNet { get; set; }
        
        [DataMember]
        public String Nombre { get; set; }
        
        [DataMember]
        public long? ContratoNet { get; set; }
        
        [DataMember]
        public String Detalle { get; set; }
        
        [DataMember]
        public String Type { get; set; }
        
        [DataMember]
        public String Tipo { get; set; }
        
        [DataMember]
        public long? Clv_Aparato { get; set; }

        [DataMember]
        public int? Clv_TipSer { get; set; }

        [DataMember]
        public String Status { get; set; }

        #endregion
    }
}

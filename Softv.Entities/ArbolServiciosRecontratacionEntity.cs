﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    public class ArbolServiciosRecontratacionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? IdRecon { get; set; }

        [DataMember]
        public long? Contrato { get; set; }
        
        [DataMember]
        public String Nombre { get; set; }
        
        [DataMember]
        public long? Clv_UnicaNet { get; set; }
        
        [DataMember]
        public int? IdMedio { get; set; }
        
        [DataMember]
        public String Type { get; set; }

        [DataMember]
        public String Detalle { get; set; }

        [DataMember]
        public int? Expanded { get; set; }
        
        [DataMember]
        public String Tipo { get; set; }


        [DataMember]
        public String Status { get; set; }

        [DataMember]
        public int? Clv_TipSer { get; set; }

        [DataMember]
        public List<ArbolAparatosRecontratacionEntity> children { get; set; }

        #endregion
    }

    public class DetalleRecontratacionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? IdRecon { get; set; }

        [DataMember]
        public long? Clv_UnicaNet { get; set; }

        [DataMember]
        public String Servicio { get; set; }

        [DataMember]
        public String Medio { get; set; }

        [DataMember]
        public String Status { get; set; }

        [DataMember]
        public int? TvConPago { get; set; }

        [DataMember]
        public int? TvSinPago { get; set; }

        #endregion
    }
}

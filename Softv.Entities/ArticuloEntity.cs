﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ArticuloEntity : BaseEntity
    {
        [DataMember]
        public long? NoArticulo { get; set; }

        [DataMember]
        public long? Clave { get; set; }

        [DataMember]
        public string Concepto { get; set; }

        [DataMember]
        public decimal? Precio { get; set; }

        [DataMember]
        public int? Cantidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ArticuloRentaEntity:BaseEntity
    {
        [DataMember]
        public int  Id { get; set; }

        [DataMember]
        public string Articulo { get; set; }

        [DataMember]
        public string servicio { get; set; }

        [DataMember]
        public string TipoServicio { get; set; }
    }

    [DataContract]
    [Serializable]
    public class GuardaCostoPagareEntity : BaseEntity
    {
        
        [DataMember]
        public long  ? ID { get; set; }

        [DataMember]
        public int  ? TIPSER { get; set; }

        [DataMember]
        public string OPCION { get; set; }
        [DataMember]
        public string MSJ { get; set; }

        [DataMember]
        public int ? Clv_Servicio { get; set; }

        [DataMember]
        public long ?  IdCostoAp { get; set; }
    }


    [DataContract]
    [Serializable]
    public class ArticuloRentaAparatosEntity : BaseEntity
    {

        [DataMember]
        public long? NoArticulo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public long ? IdCostoAP { get; set; }

        [DataMember]
        public bool Bnd { get; set; }
    }


    [DataContract]
    [Serializable]
    public class CostoArticuloPagareVigenciasEntity : BaseEntity
    {
        [DataMember]
        public bool Vigente { get; set; }

        [DataMember]
        public int ? IdRelVigencia { get; set; }

        [DataMember]
        public long  IdCostoAP { get; set; }

        [DataMember]
        public int DIA_INICIAL { get; set; }

        [DataMember]
        public int DIA_FINAL { get; set; }

        [DataMember]
        public String Periodo_Inicial { get; set; }

        [DataMember]
        public String Periodo_Final { get; set; }

        [DataMember]
        public Decimal CostoArticulo { get; set;  }

        [DataMember]
        public Decimal RentaPrincipal { get; set; }
        [DataMember]
        public Decimal RentaAdicional { get; set; }
        [DataMember]
        public Decimal RentaAdicional2 { get; set; }
        [DataMember]
        public Decimal RentaAdicional3 { get; set; }
        [DataMember]
        public string Msj { get; set; }
        


    }
}

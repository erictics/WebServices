﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.BUSCADetOrdSerEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : BUSCADetOrdSer entity
    /// File                    : BUSCADetOrdSerEntity.cs
    /// Creation date           : 15/03/2017
    /// Creation time           : 11:45 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class BUSCADetOrdSerEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clave
        /// </summary>
        [DataMember]
        public long? Clave { get; set; }
        /// <summary>
        /// Property Clv_Orden
        /// </summary>
        [DataMember]
        public long? Clv_Orden { get; set; }
        /// <summary>
        /// Property Clv_Trabajo
        /// </summary>
        [DataMember]
        public int? Clv_Trabajo { get; set; }
        /// <summary>
        /// Property Descripcion
        /// </summary>
        [DataMember]
        public String Descripcion { get; set; }
        /// <summary>
        /// Property Accion
        /// </summary>
        [DataMember]
        public String Accion { get; set; }
        /// <summary>
        /// Property Obs
        /// </summary>
        [DataMember]
        public String Obs { get; set; }
        /// <summary>
        /// Property SeRealiza
        /// </summary>
        [DataMember]
        public bool? SeRealiza { get; set; }
        #endregion
    }
}


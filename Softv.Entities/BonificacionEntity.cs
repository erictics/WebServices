﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class BonificacionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? IdBonificacion { get; set; }

        [DataMember]
        public decimal? Bonificacion { get; set; }

        [DataMember]
        public int? Clv_TipoUsuario { get; set; }

        [DataMember]
        public String Nombre { get; set; }

        [DataMember]
        public int? Op { get; set; }

        #endregion
    }
}

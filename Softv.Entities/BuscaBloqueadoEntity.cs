﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.BuscaBloqueadoEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : BuscaBloqueado entity
    /// File                    : BuscaBloqueadoEntity.cs
    /// Creation date           : 13/02/2017
    /// Creation time           : 01:30 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class BuscaBloqueadoEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public long? Contrato { get; set; }
        /// <summary>
        /// Property Num
        /// </summary>
        [DataMember]
        public int? Num { get; set; }
        /// <summary>
        /// Property Bloqueado
        /// </summary>
        [DataMember]
        public int? Bloqueado { get; set; }
        #endregion
    }
}


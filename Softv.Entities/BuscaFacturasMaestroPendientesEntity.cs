﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.BuscaFacturasMaestroPendientesEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : BuscaFacturasMaestroPendientes entity
    /// File                    : BuscaFacturasMaestroPendientesEntity.cs
    /// Creation date           : 30/05/2017
    /// Creation time           : 12:54 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class BuscaFacturasMaestroPendientesEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Id
        /// </summary>
        [DataMember]
        public int? Id { get; set; }
        /// <summary>
        /// Property Clv_FacturaMaestro
        /// </summary>
        [DataMember]
        public long? Clv_FacturaMaestro { get; set; }
        /// <summary>
        /// Property ContratoMaestro
        /// </summary>
        [DataMember]
        public long? ContratoMaestro { get; set; }
        /// <summary>
        /// Property Ticket
        /// </summary>
        [DataMember]
        public String Ticket { get; set; }
        /// <summary>
        /// Property Tipo
        /// </summary>
        [DataMember]
        public String Tipo { get; set; }
        /// <summary>
        /// Property Cliente
        /// </summary>
        [DataMember]
        public String Cliente { get; set; }
        /// <summary>
        /// Property ImporteFactura
        /// </summary>
        [DataMember]
        public Decimal? ImporteFactura { get; set; }
        /// <summary>
        /// Property FechaFacturacion
        /// </summary>
        [DataMember]
        public String FechaFacturacion { get; set; }
        /// <summary>
        /// Property TotalAbonado
        /// </summary>
        [DataMember]
        public Decimal? TotalAbonado { get; set; }
        /// <summary>
        /// Property IdCompania
        /// </summary>
        [DataMember]
        public int? IdCompania { get; set; }
        /// <summary>
        /// Property IdDistribuidor
        /// </summary>
        [DataMember]
        public int? IdDistribuidor { get; set; }
        /// <summary>
        /// Property ACuantosPagos
        /// </summary>
        [DataMember]
        public String ACuantosPagos { get; set; }
        /// <summary>
        /// Property PagoInicial
        /// </summary>
        [DataMember]
        public Decimal? PagoInicial { get; set; }
        /// <summary>
        /// Property FechaVencimiento
        /// </summary>
        [DataMember]
        public String FechaVencimiento { get; set; }
        /// <summary>
        /// Property Status
        /// </summary>
        [DataMember]
        public String Status { get; set; }
        /// <summary>
        /// Property Clv_FacturaCli
        /// </summary>
        [DataMember]
        public long? Clv_FacturaCli { get; set; }
        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public String Contrato { get; set; }
        /// <summary>
        /// Property ImporteFacCliente
        /// </summary>
        [DataMember]
        public Decimal? ImporteFacCliente { get; set; }
        /// <summary>
        /// Property MontoAbono
        /// </summary>
        [DataMember]
        public Decimal? MontoAbono { get; set; }
        /// <summary>
        /// Property PorPagar
        /// </summary>
        [DataMember]
        public Decimal? PorPagar { get; set; }
        /// <summary>
        /// Property Saldada
        /// </summary>
        [DataMember]
        public bool? Saldada { get; set; }










        [DataMember]
        public String Fecha { get; set; }


        [DataMember]
        public int? Op { get; set; }


        [DataMember]
        public String ContratoCompania { get; set; }



        #endregion
    }
}


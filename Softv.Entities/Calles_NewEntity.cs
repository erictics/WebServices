﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class Calles_NewEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_Calle { get; set; }

        [DataMember]
        public String NOMBRE { get; set; }

        [DataMember]
        public int? Op { get; set; }

        [DataMember]
        public int? Idcompania { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class QMCalleEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? Clv_Calle { get; set; }

        [DataMember]
        public string NOMBRE { get; set; }

        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
     public class CambioAparatoEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? AparatoCliente { get; set; }

        [DataMember]
        public String StatusEntrega { get; set; }

        [DataMember]
        public long? AparatoAsignar { get; set; }

        [DataMember]
        public long? TipoAparatoAsignar { get; set; }

        #endregion
    }
}

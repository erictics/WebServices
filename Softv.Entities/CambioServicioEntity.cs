﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
   public  class CambioServicioEntity : BaseEntity
    {
        [DataMember]
        public long? Clave { get; set; }

        [DataMember]
        public long? Contrato { get; set; }


        [DataMember]
        public string NumContrato { get; set; }

        [DataMember]
        public long ? ContratoNet { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public int ? Clv_TipSer { get; set; }

        [DataMember]
        public string Concepto { get; set; }

        [DataMember]
        public int? Clv_ServOld { get; set; }

        [DataMember]
        public string Servicio_Anterior { get; set; }

        [DataMember]
        public int? Clv_ServNew { get; set; }

        [DataMember]
        public string Servicio_Actual { get; set; }

        [DataMember]
        public DateTime Fecha_Sol { get; set; }


        [DataMember]
        public int ? Realizar { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Clv_Txt { get; set; }

        [DataMember]
        public decimal ? Monto { get; set; }

        [DataMember]
        public long ? Clv_Factura { get; set; }

        [DataMember]
        public string fecha { get; set; }

        [DataMember]
        public string hora { get; set; }
    }
}

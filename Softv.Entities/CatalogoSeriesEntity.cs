﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.CatalogoSeriesEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : CatalogoSeries entity
    /// File                    : CatalogoSeriesEntity.cs
    /// Creation date           : 22/11/2017
    /// Creation time           : 01:10 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class CatalogoSeriesEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Serie
        /// </summary>
        [DataMember]
        public String Serie { get; set; }
        /// <summary>
        /// Property Folios_Impresos
        /// </summary>
        [DataMember]
        public int? Folios_Impresos { get; set; }
        /// <summary>
        /// Property UltimoFolio_Usado
        /// </summary>
        [DataMember]
        public int? UltimoFolio_Usado { get; set; }
        /// <summary>
        /// Property Clv_Vendedor
        /// </summary>
        [DataMember]
        public int? Clv_Vendedor { get; set; }
        /// <summary>
        /// Property Clave
        /// </summary>
        [DataMember]
        public int? Clave { get; set; }


        [DataMember]
        public int? Tipo { get; set; }


        [DataMember]
        public String NOMBRE { get; set; }

        [DataMember]
        public int? Op { get; set; }

        [DataMember]
        public int? ClvUsuario { get; set; }

        [DataMember]
        public int? IdCompania { get; set; }

        [DataMember]
        public int? Folio { get; set; }



        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ChecaFolioUsadoEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ChecaFolioUsado entity
    /// File                    : ChecaFolioUsadoEntity.cs
    /// Creation date           : 27/10/2016
    /// Creation time           : 06:41 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ChecaFolioUsadoEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Vendedor
        /// </summary>
        [DataMember]
        public int? Clv_Vendedor { get; set; }
        /// <summary>
        /// Property Serie
        /// </summary>
        [DataMember]
        public String Serie { get; set; }
        /// <summary>
        /// Property Id
        /// </summary>
        [DataMember]
        public int? Id { get; set; }



        [DataMember]
        public int? Folio { get; set; }

        #endregion
    }
}


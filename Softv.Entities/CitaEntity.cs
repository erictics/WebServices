﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{

    [DataContract]
    [Serializable]
    public class CitaEntity : BaseEntity
    {

        #region Attributes

        [DataMember]
        public int? Clv_Cita { get; set; }

        [DataMember]
        public int? Clv_Tecnico { get; set; }

        [DataMember]
        public String Fecha { get; set; }

        [DataMember]
        public int? Contrato { get; set; }

        [DataMember]
        public String Descripcion_corta { get; set; }

        [DataMember]
        public String Descripcion { get; set; }

        [DataMember]
        public String QuejaOrden { get; set; }

        [DataMember]
        public int? Clv_ORDEN { get; set; }

        [DataMember]
        public int? Clv_TipSer { get; set; }

        [DataMember]
        public String Obs { get; set; }

        [DataMember]
        public String Status { get; set; }

        [DataMember]
        public int? Clave { get; set; }

        [DataMember]
        public int? Clv_Hora { get; set; }

        [DataMember]
        public String HORA { get; set; }

        #endregion

    }
}

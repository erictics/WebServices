﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class CitaQuejaEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_Queja { get; set; }

        [DataMember]
        public int? Clv_TipSer { get; set; }

        [DataMember]
        public int? Contrato { get; set; }

        [DataMember]
        public String Fecha_Solicitud { get; set; }

        [DataMember]
        public String Fecha_Ejecucion { get; set; }

        [DataMember]
        public String Observaciones { get; set; }

        [DataMember]
        public String Status { get; set; }

        [DataMember]
        public int? Clave_Tecnico { get; set; }

        [DataMember]
        public String Problema { get; set; }

        [DataMember]
        public String Solucion { get; set; }

        [DataMember]
        public String Fecha_Actualizacion { get; set; }

        [DataMember]
        public String Fecha_Captura { get; set; }

        [DataMember]
        public String Clv_TipoQueja { get; set; }

        [DataMember]
        public String Imp { get; set; }

        [DataMember]
        public String Clasificacion { get; set; }

        [DataMember]
        public String Hora_Ejecucion { get; set; }

        [DataMember]
        public int? Clv_Trabajo { get; set; }

        [DataMember]
        public Boolean Impresa { get; set; }
        #endregion
    }
}

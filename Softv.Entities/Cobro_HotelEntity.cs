﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    public class Cobro_HotelEntity : BaseEntity
    {

        #region Attributes

        [DataMember]
        public int? Clv_servicio { get; set; }

        [DataMember]
        public string Clv_txt { get; set; }

        [DataMember]
        public int? Clv_tipser { get; set; }

        [DataMember]
        public decimal? Contratacion { get; set; }

        [DataMember]
        public decimal? Mensualidad { get; set; }

        [DataMember]
        public int? Contrato { get; set; }

        [DataMember]
        public int? Clv_UnicaNet { get; set; }

        [DataMember]
        public decimal? Renta { get; set; }

        [DataMember]
        public int? result { get; set; }

        [DataMember]
        public string MSJ { get; set; }

        #endregion
    }
}

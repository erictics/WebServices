﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{

    [DataContract]
    [Serializable]
    public class Colonias_NewEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_ServicioTipo { get; set; }

        [DataMember]
        public String CP { get; set; }

        [DataMember]
        public int? Clv_Tipo { get; set; }

        [DataMember]
        public String FechaEntrega { get; set; }

        [DataMember]
        public String Nombre { get; set; }

        [DataMember]
        public int? Clv_Colonia { get; set; }

        [DataMember]
        public int? Op { get; set; }

        [DataMember]
        public int? IdCompania { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class QMColoniaEntity : BaseEntity
    {
        #region Attributes
        [DataMember]
        public long? clv_colonia { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        #endregion
    }
}


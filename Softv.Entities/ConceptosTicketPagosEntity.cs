﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ConceptosTicketPagosEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ConceptosTicketPagos entity
    /// File                    : ConceptosTicketPagosEntity.cs
    /// Creation date           : 03/11/2016
    /// Creation time           : 11:06 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ConceptosTicketPagosEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Descripcion
        /// </summary>
        [DataMember]
        public String Descripcion { get; set; }
        /// <summary>
        /// Property importe
        /// </summary>
        [DataMember]
        public Decimal? importe { get; set; }


        
        [DataMember]
        public long? Clv_Factura { get; set; }



        [DataMember]
        public Decimal? importeBonifica { get; set; }


        [DataMember]
        public Decimal? bonificacion { get; set; }

        [DataMember]
        public String PeriodoPagado { get; set; }

        [DataMember]
        public Decimal? PuntosCombo { get; set; }

        [DataMember]
        public Decimal? ImporteDiasProp { get; set; }

        [DataMember]
        public Decimal? puntosAplicadosAnt { get; set; }

        [DataMember]
        public int? PuntosAplicadosPagoAdelantado { get; set; }

        [DataMember]
        public int? PuntosAplicadosOtros { get; set; }


        #endregion
    }
}


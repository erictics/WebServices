﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ContratoSimilarEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public string ContratoCom { get; set; }

        [DataMember]
        public long? CONTRATO { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string SegundoNombre { get; set; }

        [DataMember]
        public string Apellido_Paterno { get; set; }

        [DataMember]
        public string Apellido_Materno { get; set; }

        [DataMember]
        public long? IdCompania { get; set; }

        [DataMember]
        public long? Clv_Estado { get; set; }

        [DataMember]
        public long? Clv_Ciudad { get; set; }

        [DataMember]
        public long? Clv_Localidad { get; set; }

        [DataMember]
        public long? Clv_Colonia { get; set; }

        [DataMember]
        public long? Clv_Calle { get; set; }

        [DataMember]
        public string Calle { get; set; }

        [DataMember]
        public string Colonia { get; set; }

        [DataMember]
        public string NUMERO { get; set; }

        [DataMember]
        public string Num_Int { get; set; }

        [DataMember]
        public long? IdIdentificacion { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int? NombreSimilar { get; set; }

        [DataMember]
        public int? DomicilioSimilar { get; set; }

        [DataMember]
        public int? IdentificacionSimilar { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class BitacoraDireccionSimilarEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? Contrato { get; set; }

        [DataMember]
        public string Coincidencia { get; set; }

        [DataMember]
        public long? ContratoSimilar { get; set; }

        #endregion
    }
}

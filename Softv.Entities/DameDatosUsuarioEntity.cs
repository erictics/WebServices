﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.DameDatosUsuarioEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameDatosUsuario entity
    /// File                    : DameDatosUsuarioEntity.cs
    /// Creation date           : 14/10/2016
    /// Creation time           : 10:52 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class DameDatosUsuarioEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clave
        /// </summary>
        [DataMember]
        public long? Clave { get; set; }
        /// <summary>
        /// Property Clv_Usuario
        /// </summary>
        [DataMember]
        public long? Clv_Usuario { get; set; }
        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        /// <summary>
        /// Property Domicilio
        /// </summary>
        [DataMember]
        public String Domicilio { get; set; }
        /// <summary>
        /// Property Colonia
        /// </summary>
        [DataMember]
        public String Colonia { get; set; }
        /// <summary>
        /// Property FechaIngreso
        /// </summary>
        [DataMember]
        public String FechaIngreso { get; set; }
        /// <summary>
        /// Property FechaSalida
        /// </summary>
        [DataMember]
        public String FechaSalida { get; set; }
        /// <summary>
        /// Property Activo
        /// </summary>
        [DataMember]
        public bool? Activo { get; set; }
        /// <summary>
        /// Property Pasaporte
        /// </summary>
        [DataMember]
        public String Pasaporte { get; set; }
        /// <summary>
        /// Property Clv_TipoUsuario
        /// </summary>
        [DataMember]
        public int? Clv_TipoUsuario { get; set; }
        /// <summary>
        /// Property CATV
        /// </summary>
        [DataMember]
        public bool? CATV { get; set; }
        /// <summary>
        /// Property Facturacion
        /// </summary>
        [DataMember]
        public bool? Facturacion { get; set; }
        /// <summary>
        /// Property Boletos
        /// </summary>
        [DataMember]
        public bool? Boletos { get; set; }
        /// <summary>
        /// Property Mizar_AN
        /// </summary>
        [DataMember]
        public bool? Mizar_AN { get; set; }
        #endregion
    }
}


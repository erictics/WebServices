﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.DameNombreSucursalEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameNombreSucursal entity
    /// File                    : DameNombreSucursalEntity.cs
    /// Creation date           : 14/10/2016
    /// Creation time           : 11:04 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class DameNombreSucursalEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Sucursal
        /// </summary>
        [DataMember]
        public long? Clv_Sucursal { get; set; }
        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        /// <summary>
        /// Property IP
        /// </summary>
        [DataMember]
        public String IP { get; set; }
        /// <summary>
        /// Property Impresora
        /// </summary>
        [DataMember]
        public String Impresora { get; set; }
        /// <summary>
        /// Property Clv_Equivalente
        /// </summary>
        [DataMember]
        public String Clv_Equivalente { get; set; }
        /// <summary>
        /// Property Serie
        /// </summary>
        [DataMember]
        public String Serie { get; set; }
        /// <summary>
        /// Property UltimoFolioUsado
        /// </summary>
        [DataMember]
        public int? UltimoFolioUsado { get; set; }
        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    public class DatosDetalleCM
    {

        public int? Posicion { get; set; }
        public int? Nivel { get; set; }
        public string Descripcion { get; set; }
        public decimal? Total { get; set; }


        public List<DatosDetalleCM> ListaC { get; set; }

    }
}

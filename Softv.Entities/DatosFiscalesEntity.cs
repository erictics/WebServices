﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.DatosFiscalesEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DatosFiscales entity
    /// File                    : DatosFiscalesEntity.cs
    /// Creation date           : 07/09/2017
    /// Creation time           : 11:44 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class DatosFiscalesEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public long? Contrato { get; set; }
        /// <summary>
        /// Property IVADESGLOSADO
        /// </summary>
        [DataMember]
        public byte? IVADESGLOSADO { get; set; }
        /// <summary>
        /// Property RAZON_SOCIAL
        /// </summary>
        [DataMember]
        public String RAZON_SOCIAL { get; set; }
        /// <summary>
        /// Property RFC
        /// </summary>
        [DataMember]
        public String RFC { get; set; }
        /// <summary>
        /// Property CALLE_RS
        /// </summary>
        [DataMember]
        public String CALLE_RS { get; set; }
        /// <summary>
        /// Property NUMERO_RS
        /// </summary>
        [DataMember]
        public String NUMERO_RS { get; set; }
        /// <summary>
        /// Property ENTRECALLES
        /// </summary>
        [DataMember]
        public String ENTRECALLES { get; set; }
        /// <summary>
        /// Property COLONIA_RS
        /// </summary>
        [DataMember]
        public String COLONIA_RS { get; set; }
        /// <summary>
        /// Property CIUDAD_RS
        /// </summary>
        [DataMember]
        public String CIUDAD_RS { get; set; }
        /// <summary>
        /// Property ESTADO_RS
        /// </summary>
        [DataMember]
        public String ESTADO_RS { get; set; }
        /// <summary>
        /// Property CP_RS
        /// </summary>
        [DataMember]
        public String CP_RS { get; set; }
        /// <summary>
        /// Property TELEFONO_RS
        /// </summary>
        [DataMember]
        public String TELEFONO_RS { get; set; }
        /// <summary>
        /// Property FAX_RS
        /// </summary>
        [DataMember]
        public String FAX_RS { get; set; }
        /// <summary>
        /// Property TIPO
        /// </summary>
        [DataMember]
        public String TIPO { get; set; }
        /// <summary>
        /// Property IDENTIFICADOR
        /// </summary>
        [DataMember]
        public Decimal? IDENTIFICADOR { get; set; }
        /// <summary>
        /// Property CURP
        /// </summary>
        [DataMember]
        public String CURP { get; set; }


        [DataMember]
        public String PAIS { get; set; }


        [DataMember]
        public String Email { get; set; }

        #endregion
    }
}


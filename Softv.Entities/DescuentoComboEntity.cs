﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DescuentoComboEntity
    {
        [DataMember]
        public int? Clv_Session { get; set; }
        [DataMember]
        public int? Clv_TipSer { get; set; }
        [DataMember]
        public int? Clv_Servicio { get; set; }
        [DataMember]
        public decimal? Cont { get; set; }
        [DataMember]
        public decimal? Mens { get; set; }
        [DataMember]
        public decimal? Reco { get; set; }
        [DataMember]
        public decimal ? Puntos { get; set; }
        [DataMember]
        public decimal? Llamada { get; set; }
        [DataMember]
        public int ? Op { get; set; }

        [DataMember]
        public int? Clave { get; set; }

        [DataMember]
        public int? Clv_Descuento { get; set; }

        [DataMember]
        public string Descripcion { get; set; }
    }
}

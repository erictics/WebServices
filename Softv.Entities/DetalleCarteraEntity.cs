﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DetalleCarteraEntity
    {
        [DataMember]
        public int ? Tipservicio { get; set; }

        [DataMember]
        public string  Fecha { get; set; }

         [DataMember]
        public string TipoReporte { get; set; }
    }

    [DataContract]
    [Serializable]
    public class GenCarteraEntity
    {
        [DataMember]
        public long? clv_session2 { get; set; }

        [DataMember]
        public int? Clv_TipSer { get; set; }

        [DataMember]
        public int? TipoCartera { get; set; }

        [DataMember]
        public string Clv_Usuario { get; set; }

        [DataMember]
        public long? Clv_Cartera { get; set; }
    }
}

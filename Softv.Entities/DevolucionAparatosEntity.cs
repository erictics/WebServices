﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DevolucionAparatosEntity : BaseEntity
    {

        [DataMember]
        public long ? CLV_ORDEN { get; set; }
        [DataMember]
        public string  TIPOAPARATO { get; set; }
        [DataMember]
        public string TIPO { get; set; }
        [DataMember]
        public long ? CLV_CABLEMODEM { get; set; }
        [DataMember]
        public string MACCABLEMODEM { get; set; }
        [DataMember]
        public bool ? ESTADOAPARATO { get; set; }
        [DataMember]
        public string PROVIENEDE { get; set; }
        [DataMember]
        public string  RECIBIO { get; set; }
        [DataMember]
        public string MARCAACTUAL { get; set; }
        [DataMember]
        public bool ? REGRESARALALMACEN { get; set; }
        [DataMember]
        public string MARCA { get; set; }

        [DataMember]
        public long? clv_unicanet { get; set; }

        [DataMember]
        public string CONTRATO { get; set; }
    }


    [DataContract]
    [Serializable]
    public class AparatosPruebaEntity : BaseEntity
    {

        [DataMember]
        public long? contrato { get; set; }

        [DataMember]
        public string clv_unicanet { get; set; }

        [DataMember]
        public string clv_cablemodem { get; set; }

        [DataMember]
        public string maccablemodem { get; set; }

        [DataMember]
        public string clv_servicio { get; set; }

        [DataMember]
        public string descripcion { get; set; }
        
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.DimeSiYaGrabeUnaFacMaestroEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DimeSiYaGrabeUnaFacMaestro entity
    /// File                    : DimeSiYaGrabeUnaFacMaestroEntity.cs
    /// Creation date           : 27/03/2017
    /// Creation time           : 12:04 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class DimeSiYaGrabeUnaFacMaestroEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property ContratoMae
        /// </summary>
        [DataMember]
        public long? ContratoMae { get; set; }
        /// <summary>
        /// Property Valida
        /// </summary>
        [DataMember]
        public int? Valida { get; set; }


        #endregion
    }
}


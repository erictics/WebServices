﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.Dime_Que_servicio_Tiene_clienteEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Dime_Que_servicio_Tiene_cliente entity
    /// File                    : Dime_Que_servicio_Tiene_clienteEntity.cs
    /// Creation date           : 08/02/2017
    /// Creation time           : 06:59 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class Dime_Que_servicio_Tiene_clienteEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property clv_tipser
        /// </summary>
        [DataMember]
        public int? clv_tipser { get; set; }
        /// <summary>
        /// Property Concepto
        /// </summary>
        [DataMember]
        public String Concepto { get; set; }
        /// <summary>
        /// Property Principal
        /// </summary>
        [DataMember]
        public int? Principal { get; set; }


        [DataMember]
        public long? Contrato { get; set; }




        #endregion
    }
}


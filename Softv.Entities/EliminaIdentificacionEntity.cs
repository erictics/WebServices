﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    class EliminaIdentificacionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public string TipoIdentificacion { get; set; }

        #endregion
    }
}

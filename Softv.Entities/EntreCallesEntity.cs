﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class EntreCallesEntity : BaseEntity
    {
        [DataMember]
        public long? contrato { get; set; }

        [DataMember]
        public int? CalleNorte { get; set; }

        [DataMember]
        public int? CalleSur { get; set; }

        [DataMember]
        public int? CalleEste { get; set; }

        [DataMember]
        public int? CalleOeste { get; set; }

        [DataMember]
        public string Norte { get; set; }

        [DataMember]
        public string Sur { get; set; }

        [DataMember]
        public string Este { get; set; }

        [DataMember]
        public string Oeste { get; set; }

        [DataMember]
        public string Casa { get; set; }

        [DataMember]
        public string referencia { get; set; }
    }
}

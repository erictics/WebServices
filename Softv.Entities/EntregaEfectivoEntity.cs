﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]

    public class EntregaEfectivoEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? IdEntrega { get; set; }

        [DataMember]
        public string Fecha { get; set; }

        [DataMember]
        public long? Clv_plaza { get; set; }

        [DataMember]
        public long? Clv_sucursal { get; set; }

        [DataMember]
        public string Sucursal { get; set; }

        [DataMember]
        public decimal? EfectivoDisponible { get; set; }

        [DataMember]
        public decimal? Diferencia { get; set; }

        [DataMember]
        public decimal? ImporteCometra { get; set; }

        [DataMember]
        public decimal? ImporteDeoBancario { get; set; }

        [DataMember]
        public decimal? ImporteCorporativo { get; set; }

        [DataMember]
        public string FechaCometra { get; set; }

        [DataMember]
        public string FechaDepBancario { get; set; }

        [DataMember]
        public string FechaCorporativo { get; set; }

        [DataMember]
        public string Usuario { get; set; }

        [DataMember]
        public int? op { get; set; }

        [DataMember]
        public bool? existe { get; set; }

        [DataMember]
        public string RecibeCorporativo { get; set; }

        [DataMember]
        public string razon_social { get; set; }

        [DataMember]
        public string FechaHoy { get; set; }

        [DataMember]
        public string FechaCorporativoFin { get; set; }

        [DataMember]
        public string FechaFin { get; set; }

        #endregion
    }
}

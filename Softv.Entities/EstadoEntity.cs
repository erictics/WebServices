﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.EstadoEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Estado entity
    /// File                    : EstadoEntity.cs
    /// Creation date           : 07/02/2017
    /// Creation time           : 06:08 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class EstadoEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Estado
        /// </summary>
        [DataMember]
        public int? Clv_Estado { get; set; }
        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        /// <summary>
        /// Property id_compania
        /// </summary>
        [DataMember]
        public int? id_compania { get; set; }


        [DataMember]
        public int? IdPlaza { get; set; }



        [DataMember]
        public int? IdEstado { get; set; }

        //[DataMember]
        //public List<ClienteEntity> Cliente { get; set; }

        #endregion
    }
}


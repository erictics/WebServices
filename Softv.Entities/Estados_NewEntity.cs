﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{

    [DataContract]
    [Serializable]
    public class Estados_NewEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_Estado { get; set; }

        [DataMember]
        public String Nombre { get; set; }

        [DataMember]
        public int? opcion { get; set; }

        [DataMember]
        public int? clv_estadomod { get; set; }

        [DataMember]
        public int? mismoNombre { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class MQEstadosEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? Clv_Estado { get; set; }

        [DataMember]
        public String Nombre { get; set; }

        [DataMember]
        public long? IdCompania { get; set; }

        #endregion
    }

}


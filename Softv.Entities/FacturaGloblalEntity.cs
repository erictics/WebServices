﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class FacturaGloblalEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public string Serie { get; set; }

        [DataMember]
        public string factura { get; set; }

        [DataMember]
        public string fecha { get; set; }

        [DataMember]
        public string sucursal { get; set; }

        [DataMember]
        public int? Op { get; set; }

        [DataMember]
        public string tipo { get; set; }

        [DataMember]
        public long? idDistribuidor { get; set; }

        [DataMember]
        public long? ClvUsuario { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string fechai { get; set; }

        [DataMember]
        public string fechaf { get; set; }

        [DataMember]
        public string cajera { get; set; }

        [DataMember]
        public decimal? importe { get; set; }

        [DataMember]
        public decimal? cancelada { get; set; }

        [DataMember]
        public decimal? IdFactura { get; set; }

        [DataMember]
        public decimal? TOTAL { get; set; }

        [DataMember]
        public decimal? SUBTOTAL { get; set; }

        [DataMember]
        public decimal? IVA { get; set; }

        [DataMember]
        public decimal? IEPS { get; set; }

        [DataMember]
        public int? Cont { get; set; }

        [DataMember]
        public int? idcompania { get; set; }

        [DataMember]
        public long? Clv_Facturaglobal { get; set; }

        [DataMember]
        public string ID_Compania_Mizart { get; set; }

        [DataMember]
        public string ID_Sucursal_Mizart { get; set; }

        [DataMember]
        public string MSG { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class TipoNotaEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? clv { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class CantidadLetraEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public decimal? Numero { get; set; }

        [DataMember]
        public int? moneda { get; set; }

        [DataMember]
        public string letra { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class FacturaDigitalEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? Clv_Factura { get; set; }

        [DataMember]
        public string TipoFact { get; set; }

        [DataMember]
        public long? Clv_FacturaCFD { get; set; }

        [DataMember]
        public decimal? TotalConPuntos { get; set; }

        [DataMember]
        public decimal? SubTotal { get; set; }

        [DataMember]
        public decimal? TotalSinPuntos { get; set; }

        [DataMember]
        public decimal? Descuento { get; set; }

        [DataMember]
        public decimal? iva { get; set; }

        [DataMember]
        public decimal? ieps { get; set; }

        [DataMember]
        public decimal? TasaIva { get; set; }

        [DataMember]
        public decimal? TasaIeps { get; set; }

        [DataMember]
        public string Fecha { get; set; }

        [DataMember]
        public decimal? TotalImpuestos { get; set; }

        [DataMember]
        public int? IDCOMPANIA { get; set; }

        [DataMember]
        public string FORMADEPAGO { get; set; }

        [DataMember]
        public string CONDICIONESDEPAGO { get; set; }

        [DataMember]
        public string MOTIVODESCUENTO { get; set; }

        [DataMember]
        public string METODODEPAGO { get; set; }

        [DataMember]
        public string TIPODECOMPROBANTE { get; set; }

        [DataMember]
        public string PAGOENPARCIALIDADES { get; set; }

        [DataMember]
        public string LUGAREXPEDICION { get; set; }

        [DataMember]
        public string REGIMEN { get; set; }

        [DataMember]
        public string MONEDA { get; set; }

        [DataMember]
        public string VERSON { get; set; }

        [DataMember]
        public string numCtaPago { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class ReFacturaGlobalEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? Clv_Factura { get; set; }

        [DataMember]
        public int? Res { get; set; }

        [DataMember]
        public string Msg { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class FacDigCDFEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? Clv_Factura { get; set; }

        [DataMember]
        public decimal? Clv_FacturaCDF { get; set; }

        [DataMember]
        public string tipo { get; set; }

        #endregion
    }

}
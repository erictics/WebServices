﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    public class GraficasEncuestas2
    {

        public int? IdPregunta { get; set; }
        public int? IdTipoPregunta { get; set; }
        public string Pregunta { get; set; }
        public string RespAbi { get; set; }
        public string Respuesta { get; set; }
        public int? Cuantos { get; set; }

        public List<GraficasEncuestas2> LstCliente { get; set; }

    }
}

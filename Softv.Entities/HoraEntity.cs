﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class HoraEntity : BaseEntity
    {
        #region Attributes
        [DataMember]
        public int? Clv_Hora { get; set; }

        [DataMember]
        public String Hora { get; set; }
        #endregion
    }

    [DataContract]
    [Serializable]
    public class HoraOrdQueEntity : BaseEntity
    {
        #region Attributes
        [DataMember]
        public string HoraIni { get; set; }

        [DataMember]
        public string HoraFin { get; set; }
        #endregion
    }
}

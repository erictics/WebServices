﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;


namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class InterfazCablemodemEntity : BaseEntity
    {
        [DataMember]
        public long ? Clv_Orden { get; set; }

        [DataMember]
        public string fecha_ejecucion { get; set; }

        [DataMember]
        public string fecha_habilitar { get; set; }

        [DataMember]
        public string Comando { get; set; }

        [DataMember]
        public string SERVICIO { get; set; }

        [DataMember]
        public int ? CLVTIPSER { get; set; }

        [DataMember]
        public string TIPOSERVICIO { get; set; }

        [DataMember]
        public string MEDIO { get; set; }

        [DataMember]
        public int ? IdMedio { get; set; }


        [DataMember]
        public int ? CLVPLAZA { get; set; }

        [DataMember]
        public string NOMBREPLAZA { get; set; }

        [DataMember]
        public string MacAddress { get; set; }

        [DataMember]
        public int? resultado { get; set; }

        [DataMember]
        public int ? CLVDISTRIBUIDOR { get; set;  }

        [DataMember]
        public string NOMBREDISTRIBUIDOR { get; set; }

        [DataMember]
        public string contratocompuesto { get; set; }

        [DataMember]
        public int ? Op { get; set; }
    }
}

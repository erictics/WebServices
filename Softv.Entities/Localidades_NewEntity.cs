﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{

    [DataContract]
    [Serializable]
    public class Localidades_NewEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_Localidad { get; set; }

        [DataMember]
        public String Nombre { get; set; }

        [DataMember]
        public int? opcion { get; set; }

        [DataMember]
        public int? clvnuevo { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class QMLocalidadEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_Localidad { get; set; }

        [DataMember]
        public String Nombre { get; set; }

        #endregion
    }
}


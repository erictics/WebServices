﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ModuleEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? IdModule { get; set; }

        [DataMember]
        public String Description { get; set; }

        [DataMember]
        public String ModulePath { get; set; }

        [DataMember]
        public String ModuleView { get; set; }

        [DataMember]
        public int? ParentId { get; set; }

        [DataMember]
        public int? SortOrder { get; set; }

        [DataMember]
        public bool? OptAdd { get; set; }

        [DataMember]
        public bool? OptSelect { get; set; }

        [DataMember]
        public bool? OptUpdate { get; set; }
 
        [DataMember]
        public bool? OptDelete { get; set; }

        [DataMember]
        public PermisoEntity Permiso { get; set; }

        [DataMember]
        public int tipo { get; set; }

        [DataMember]
        public int? IdRol { get; set; }

        [DataMember]
        public List<PermisoEntity> lstPermisos { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class ModuleWebEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? IdModule { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ModulePath { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class LogClasificacionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? IdClassLog { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public long? IdModule { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class LogSistemaEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public string Contrato { get; set; }

        [DataMember]
        public string Modulo { get; set; }

        [DataMember]
        public string Submodulo { get; set; }

        [DataMember]
        public string Pricipal { get; set; }

        [DataMember]
        public long? IdClassLog { get; set; }

        [DataMember]
        public long? ContratoCom { get; set; }

        [DataMember]
        public long? IdCompania { get; set; }

        [DataMember]
        public long? Id { get; set; }

        [DataMember]
        public string FechaIni { get; set; }

        [DataMember]
        public string FechaFin { get; set; }

        [DataMember]
        public bool? FechaOp { get; set; }

        [DataMember]
        public string Fecha { get; set; }

        [DataMember]
        public string Hora { get; set; }

        [DataMember]
        public long? Clv_usuario { get; set; }

        [DataMember]
        public string Usuario { get; set; }

        [DataMember]
        public string Observaciones { get; set; }

        [DataMember]
        public string Clv_afectada { get; set; }

        [DataMember]
        public string Comando { get; set; }

        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.MovSistEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MovSist entity
    /// File                    : MovSistEntity.cs
    /// Creation date           : 18/04/2017
    /// Creation time           : 01:56 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class MovSistEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property consecutivo
        /// </summary>
        [DataMember]
        public long? Clv_usuario { get; set; }
        /// <summary>
        /// Property Fecha
        /// </summary>
        [DataMember]
        public String Modulo { get; set; }
        /// <summary>
        /// Property usuario
        /// </summary>
        [DataMember]
        public String Submodulo { get; set; }
        /// <summary>
        /// Property contrato
        /// </summary>
        [DataMember]
        public String Observaciones { get; set; }
        /// <summary>
        /// Property Sistema
        /// </summary>
        [DataMember]
        public String Usuario { get; set; }
        /// <summary>
        /// Property Pantalla
        /// </summary>
        [DataMember]
        public String Comando { get; set; }
        /// <summary>
        /// Property control
        /// </summary>
        [DataMember]
        public long ? Clv_afectada { get; set; }

        [DataMember]
        public long? op { get; set; }


        [DataMember]
        public string fecha { get; set; }

        [DataMember]
        public string hora { get; set; }

        [DataMember]
        public long ?  id { get; set; }

        [DataMember]
        public long? IdClassLog { get; set; }
        #endregion

    }
}


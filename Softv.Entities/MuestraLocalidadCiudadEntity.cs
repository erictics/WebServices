﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.MuestraLocalidadCiudadEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraLocalidadCiudad entity
    /// File                    : MuestraLocalidadCiudadEntity.cs
    /// Creation date           : 05/09/2017
    /// Creation time           : 05:01 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class MuestraLocalidadCiudadEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property clv_ciudad
        /// </summary>
        [DataMember]
        public int? clv_ciudad { get; set; }
        /// <summary>
        /// Property Clv_Localidad
        /// </summary>
        [DataMember]
        public int? Clv_Localidad { get; set; }
        /// <summary>
        /// Property NOMBRE
        /// </summary>
        [DataMember]
        public String NOMBRE { get; set; }
        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{

    [DataContract]
    [Serializable]
    public class MuestraTipoPromocionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_TipSer { get; set; }

        [DataMember]
        public int? Clave { get; set; }

        [DataMember]
        public String Descripcion { get; set; }
        #endregion
    }

    [DataContract]
    [Serializable]
    public class PromocionEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? IdCompania { get; set; }

        [DataMember]
        public bool docexcatorce { get; set; }

        [DataMember]
        public bool seisxsiete { get; set; }
        #endregion
    }
}


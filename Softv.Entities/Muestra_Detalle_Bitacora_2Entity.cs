﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.Muestra_Detalle_Bitacora_2Entity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Muestra_Detalle_Bitacora_2 entity
    /// File                    : Muestra_Detalle_Bitacora_2Entity.cs
    /// Creation date           : 21/06/2017
    /// Creation time           : 12:43 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class Muestra_Detalle_Bitacora_2Entity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property catTipoArticuloClave
        /// </summary>
        [DataMember]
        public int? catTipoArticuloClave { get; set; }
        /// <summary>
        /// Property Descripcion
        /// </summary>
        [DataMember]
        public String Descripcion { get; set; }


        [DataMember]
        public long? ClvTecnico { get; set; }

        [DataMember]
        public int? IdAlmacen { get; set; }


        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.Muestra_PlazasPorUsuarioEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Muestra_PlazasPorUsuario entity
    /// File                    : Muestra_PlazasPorUsuarioEntity.cs
    /// Creation date           : 26/07/2017
    /// Creation time           : 01:19 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class Muestra_PlazasPorUsuarioEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Plaza
        /// </summary>
        [DataMember]
        public int? Clv_Plaza { get; set; }
        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }


        [DataMember]
        public int? Clv_Usuario{ get; set; }


        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class NotaCreditoEntity
    {
        [DataMember]
        public long ? Clv_NotadeCredito { get; set; }
        [DataMember]
        public long? Contrato { get; set; }
        [DataMember]
        public string ContratoComp { get; set; }
        [DataMember]
        public string SerieFolio { get; set; }
        [DataMember]
        public long? Factura { get; set; }
        [DataMember]
        public string FechaGeneracion { get; set; }
        [DataMember]
        public string FechaCaducidad { get; set; }
        [DataMember]
        public long? Clv_sucursal { get; set; }
        [DataMember]
        public string UsuarioCaptura { get; set; }
        [DataMember]
        public long ? Clv_UsuarioAutorizo { get; set; }
        [DataMember]
        public decimal ? Monto { get; set; }
        [DataMember]
        public decimal? Saldo { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        [DataMember]
        public int ? NotaAFactura { get; set; }
        [DataMember]
        public long? Clv_caja { get; set; }
        [DataMember]
        public int? tipo { get; set; }
        [DataMember]
        public List<DetalleNotaEntity> DetalleNota { get; set; }

    }

    [DataContract]
    [Serializable]
    public class DetalleNotaEntity
    {
        [DataMember]
        public long? Clv_detalle { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public decimal Importe { get; set; }
        [DataMember]
        public decimal ImporteNota { get; set; }
        [DataMember]
        public long? Clv_servicio { get; set; }
        [DataMember]
        public bool seCobra { get; set; }

    }

    [DataContract]
    [Serializable]
    public class StatusNotaEntity
    {
        [DataMember]
        public string Clv_Status { get; set; }
        [DataMember]
        public string Status { get; set; }       

    }

    [DataContract]
    [Serializable]
    public class FacturaClienteEntity
    {
        [DataMember]
        public long? Clv_factura { get; set; }
        [DataMember]
        public string Serie { get; set; }

        [DataMember]
        public decimal Monto { get; set; }

    }


    [DataContract]
    [Serializable]
    public class DatosTicket
    {

        [DataMember]
        public int ? Clv_sucursal { get; set; }


        [DataMember]
        public string sucursal { get; set; }


        [DataMember]
        public int? Clv_caja { get; set; }


        [DataMember]
        public string caja { get; set; }
    }
}

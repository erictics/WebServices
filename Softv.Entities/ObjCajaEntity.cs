﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    public class ObjCajaEntity
    {

        public string NOMBRE { get; set; }
        public int? CLV_USUARIO { get; set; }

        public List<ObjCajaEntity> ListaCaja { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    public class ObjCajeroEntity
    {

        public string NOMBRE { get; set; }
        public string CLV_USUARIO { get; set; }

        public List<ObjCajeroEntity> ListaC { get; set; }

    }
}

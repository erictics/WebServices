﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ObjCitaEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public String Clv_Cita { get; set; }

        [DataMember]
        public String ClaveTecnico { get; set; }

        [DataMember]
        public String Comentario { get; set; }

        [DataMember]
        public String Fecha { get; set; }

        [DataMember]
        public int? ClaveHora { get; set; }

        [DataMember]
        public String TURNO { get; set; }
        
        #endregion
    }
}

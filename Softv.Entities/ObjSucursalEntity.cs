﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    public class ObjSucursalEntity
    {

        public string NOMBRE { get; set; }
        public int? CLV_SUCURSAL { get; set; }

        public List<ObjSucursalEntity> ListaSuc { get; set; }

    }
}

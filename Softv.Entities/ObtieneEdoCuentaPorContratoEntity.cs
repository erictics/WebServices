﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ObtieneEdoCuentaPorContratoEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ObtieneEdoCuentaPorContrato entity
    /// File                    : ObtieneEdoCuentaPorContratoEntity.cs
    /// Creation date           : 07/03/2017
    /// Creation time           : 04:51 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ObtieneEdoCuentaPorContratoEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property IdEstadoCuenta
        /// </summary>
        [DataMember]
        public long? IdEstadoCuenta { get; set; }
        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public String Contrato { get; set; }
        /// <summary>
        /// Property Cliente
        /// </summary>
        [DataMember]
        public String Cliente { get; set; }
        /// <summary>
        /// Property Saldo
        /// </summary>
        [DataMember]
        public Decimal? Saldo { get; set; }
        /// <summary>
        /// Property status
        /// </summary>
        [DataMember]
        public String status { get; set; }
        /// <summary>
        /// Property Ticket
        /// </summary>
        [DataMember]
        public String Ticket { get; set; }
        /// <summary>
        /// Property Id
        /// </summary>
        [DataMember]
        public long? Id { get; set; }
        /// <summary>
        /// Property ContratoBueno
        /// </summary>
        [DataMember]
        public long? ContratoBueno { get; set; }
        /// <summary>
        /// Property Fecha
        /// </summary>
        [DataMember]
        public String Fecha { get; set; }
        /// <summary>
        /// Property Factura
        /// </summary>
        [DataMember]
        public long? Factura { get; set; }

        [DataMember]
        public int CONT { get; set; }

        [DataMember]
        public string Periodo { get; set; }

        #endregion
    }
}


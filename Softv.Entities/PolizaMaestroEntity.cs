﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;


namespace Softv.Entities
{
    [DataContract]
    [Serializable]

    public class PolizasMaestroEntity : BaseEntity
    {
        [DataMember]
        public long? Clv_Poliza { get; set; }
        [DataMember]
        public string Plaza { get; set; }
        [DataMember]
        public int? Clv_Plaza { get; set; }
        [DataMember]
        public string FechaGeneracion { get; set; }
        [DataMember]
        public string FechaPoliza { get; set; }
        [DataMember]
        public string Dolares { get; set; }
    }

    public class FiltroPolizaEntity
    {
        [DataMember]
        public int? Op { get; set; }
        [DataMember]
        public int? Clv_Poliza { get; set; }
        [DataMember]
        public int? Clv_Plaza { get; set; }
        [DataMember]
        public string FechaPoliza { get; set; }

        public int Dolares { get; set; }
    }

    public class DetallePolizaEntity
    {
        [DataMember]
        public string Cuenta { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public decimal Debe { get; set; }
        [DataMember]
        public decimal Haber { get; set; }
        [DataMember]
        public int Orden { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ProcesosEncuestasEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ProcesosEncuestas entity
    /// File                    : ProcesosEncuestasEntity.cs
    /// Creation date           : 17/06/2017
    /// Creation time           : 11:22 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ProcesosEncuestasEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property IdProcesoEnc
        /// </summary>
        [DataMember]
        public int? IdProcesoEnc { get; set; }
        /// <summary>
        /// Property NombreProceso
        /// </summary>
        [DataMember]
        public String NombreProceso { get; set; }
        /// <summary>
        /// Property TipSer
        /// </summary>
        [DataMember]
        public String TipSer { get; set; }
        /// <summary>
        /// Property TipoBusqueda
        /// </summary>
        [DataMember]
        public String TipoBusqueda { get; set; }
        /// <summary>
        /// Property StatusTipBusq
        /// </summary>
        [DataMember]
        public String StatusTipBusq { get; set; }
        /// <summary>
        /// Property TipoFecha
        /// </summary>
        [DataMember]
        public String TipoFecha { get; set; }
        /// <summary>
        /// Property FechaInicio
        /// </summary>
        [DataMember]
        public String FechaInicio { get; set; }
        /// <summary>
        /// Property FechaFin
        /// </summary>
        [DataMember]
        public String FechaFin { get; set; }
        /// <summary>
        /// Property Encuesta
        /// </summary>
        [DataMember]
        public String Encuesta { get; set; }
        /// <summary>
        /// Property StatusEncuesta
        /// </summary>
        [DataMember]
        public String StatusEncuesta { get; set; }
        /// <summary>
        /// Property Usuario
        /// </summary>
        [DataMember]
        public String Usuario { get; set; }
        /// <summary>
        /// Property IdEncuesta
        /// </summary>
        [DataMember]
        public int? IdEncuesta { get; set; }
        /// <summary>
        /// Property Total
        /// </summary>
        [DataMember]
        public int? Total { get; set; }
        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.QuitarDetalleEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : QuitarDetalle entity
    /// File                    : QuitarDetalleEntity.cs
    /// Creation date           : 26/10/2016
    /// Creation time           : 05:14 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class QuitarDetalleEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property IdSession
        /// </summary>
        [DataMember]
        public long? IdSession { get; set; }
        /// <summary>
        /// Property ClvDetalle
        /// </summary>
        [DataMember]
        public long? ClvDetalle { get; set; }
        /// <summary>
        /// Property IdSistema
        /// </summary>
        //[DataMember]
        //public String IdSistema { get; set; }
        /// <summary>
        /// Property IdTipoCli
        /// </summary>
        [DataMember]
        public int? IdTipoCli { get; set; }
        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public long? Contrato { get; set; }
        /// <summary>
        /// Property Error
        /// </summary>
        [DataMember]
        public int? Error { get; set; }
        /// <summary>
        /// Property Msg
        /// </summary>
        [DataMember]
        public String Msg { get; set; }
        #endregion
    }
}


﻿    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    namespace Softv.Entities
    {
    [DataContract]
    [Serializable]
    public class RecontratacionEntity : BaseEntity
    {
    #region Attributes

      [DataMember]
      public int? IdRecon { get; set; }

      [DataMember]
      public int? IdReconAparato { get; set; }

      [DataMember]
      public int? ClvSession { get; set; }

      [DataMember]
      public int? Clv_Cablemodem { get; set; }

      [DataMember]
      public int? ClvOrden { get; set; }

      [DataMember]
      public int? ClvDetalleOrd { get; set; }

      [DataMember]
      public int? IdContrato { get; set; }

      [DataMember]
      public int? Clv_Unicanet { get; set; }

      [DataMember]
      public int? ClvTipoServ { get; set; }

      [DataMember]
      public int? Clv_Servicio { get; set; }

      [DataMember]
      public String Staus { get; set; }

      [DataMember]
      public int? IdMedio { get; set; }

      [DataMember]
      public int? ContratoNet { get; set; }

      [DataMember]
      public int? TvConPago { get; set; }

      [DataMember]
      public int? TvSinPago { get; set; }

    #endregion
    }
    }

  
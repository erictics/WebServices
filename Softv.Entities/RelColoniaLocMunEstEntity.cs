﻿
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    namespace Softv.Entities
    {
    /// <summary>
    /// Class                   : Softv.Entities.RelColoniaLocMunEstEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelColoniaLocMunEst entity
    /// File                    : RelColoniaLocMunEstEntity.cs
    /// Creation date           : 03/02/2016
    /// Creation time           : 06:55 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class RelColoniaLocMunEstEntity : BaseEntity
    {
    #region Attributes
    
      /// <summary>
      /// Property IdColonia
      /// </summary>
      [DataMember]
      public int? IdColonia { get; set; }
      /// <summary>
      /// Property IdLocalidad
      /// </summary>
      [DataMember]
      public int? IdLocalidad { get; set; }
      /// <summary>
      /// Property IdMunicipio
      /// </summary>
      [DataMember]
      public int? IdMunicipio { get; set; }
      /// <summary>
      /// Property IdEstado
      /// </summary>
      [DataMember]
      public int? IdEstado { get; set; }
      /// <summary>
      /// Property CP
      /// </summary>
      [DataMember]
      public String CP { get; set; }
      /// <summary>
      /// Property IdTipoColonia
      /// </summary>
      [DataMember]
      public int? IdTipoColonia { get; set; }
          [DataMember]
          public TipoColoniaEntity TipoColonia { get; set; }
        
          [DataMember]
          public ColoniaEntity Colonia { get; set; }
        
          [DataMember]
          public EstadoEntity Estado { get; set; }
        
          [DataMember]
          public LocalidadEntity Localidad { get; set; }
        
          [DataMember]
          public MunicipioEntity Municipio { get; set; }
        

        
    #endregion
    }
    }

  
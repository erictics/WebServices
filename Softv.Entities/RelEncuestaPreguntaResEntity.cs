﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.RelEncuestaPreguntaResEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelEncuestaPreguntaRes entity
    /// File                    : RelEncuestaPreguntaResEntity.cs
    /// Creation date           : 17/06/2017
    /// Creation time           : 11:14 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class RelEncuestaPreguntaResEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Id
        /// </summary>
        [DataMember]
        public int? Id { get; set; }
        /// <summary>
        /// Property IdEncuesta
        /// </summary>
        [DataMember]
        public int? IdEncuesta { get; set; }
        /// <summary>
        /// Property IdPregunta
        /// </summary>
        [DataMember]
        public int? IdPregunta { get; set; }
        /// <summary>
        /// Property Id_ResOpcMult
        /// </summary>
        [DataMember]
        public int? Id_ResOpcMult { get; set; }
        [DataMember]
        public EncuestasEntity Encuestas { get; set; }

        [DataMember]
        public PreguntasEntity Preguntas { get; set; }

        [DataMember]
        public ResOpcMultsEntity ResOpcMults { get; set; }

        #endregion
    }
}


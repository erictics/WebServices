﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{

    [DataContract]
    [Serializable]
    public class RelRedMedioEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? IdRed { get; set; }

        [DataMember]
        public int? IdMedio { get; set; }

        [DataMember]
        public int? Op { get; set; }

        [DataMember]
        public String Descripcion { get; set; }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class IpEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public long? Clv_UnicaNet { get; set; }

        [DataMember]
        public long? IdIP { get; set; }

        [DataMember]
        public string IP_ALL { get; set; }

        [DataMember]
        public long? IdCompania { get; set; }

        [DataMember]
        public long? Clv_Estado { get; set; }

        [DataMember]
        public long? Clv_Ciudad { get; set; }

        [DataMember]
        public long? Clv_Localidad { get; set; }

        [DataMember]
        public long? IdMedio { get; set; }

        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.RelTipoServClienteEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelTipoServCliente entity
    /// File                    : RelTipoServClienteEntity.cs
    /// Creation date           : 07/10/2017
    /// Creation time           : 10:58 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class RelTipoServClienteEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Servicio
        /// </summary>
        [DataMember]
        public int? Clv_Servicio { get; set; }
        /// <summary>
        /// Property Clv_TipSer
        /// </summary>
        [DataMember]
        public int? Clv_TipSer { get; set; }
        /// <summary>
        /// Property Descripcion
        /// </summary>
        [DataMember]
        public String Descripcion { get; set; }
        /// <summary>
        /// Property Clv_Txt
        /// </summary>
        [DataMember]
        public String Clv_Txt { get; set; }
        /// <summary>
        /// Property AplicanCom
        /// </summary>
        [DataMember]
        public bool? AplicanCom { get; set; }
        /// <summary>
        /// Property Sale_en_Cartera
        /// </summary>
        [DataMember]
        public bool? Sale_en_Cartera { get; set; }
        /// <summary>
        /// Property Precio
        /// </summary>
        [DataMember]
        public Decimal? Precio { get; set; }
        /// <summary>
        /// Property Genera_Orden
        /// </summary>
        [DataMember]
        public bool? Genera_Orden { get; set; }
        /// <summary>
        /// Property Es_Principal
        /// </summary>
        [DataMember]
        public bool? Es_Principal { get; set; }
        /// <summary>
        /// Property Clv_Trabajo
        /// </summary>
        [DataMember]
        public int? Clv_Trabajo { get; set; }
        #endregion
    }
}


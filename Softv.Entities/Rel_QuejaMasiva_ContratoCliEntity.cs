﻿
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    namespace Softv.Entities
    {
    /// <summary>
    /// Class                   : Softv.Entities.Rel_QuejaMasiva_ContratoCliEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Rel_QuejaMasiva_ContratoCli entity
    /// File                    : Rel_QuejaMasiva_ContratoCliEntity.cs
    /// Creation date           : 17/07/2018
    /// Creation time           : 12:17 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class Rel_QuejaMasiva_ContratoCliEntity : BaseEntity
    {
    #region Attributes
    
      /// <summary>
      /// Property IdQuejaMasiva
      /// </summary>
      [DataMember]
      public long? IdQuejaMasiva { get; set; }
      /// <summary>
      /// Property Contrato
      /// </summary>
      [DataMember]
      public long? Contrato { get; set; }
    #endregion
    }
    }

  
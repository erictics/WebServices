﻿
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    namespace Softv.Entities
    {
    /// <summary>
    /// Class                   : Softv.Entities.Rel_QuejaMasiva_LocEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Rel_QuejaMasiva_Loc entity
    /// File                    : Rel_QuejaMasiva_LocEntity.cs
    /// Creation date           : 17/07/2018
    /// Creation time           : 12:20 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class Rel_QuejaMasiva_LocEntity : BaseEntity
    {
    #region Attributes
    
      /// <summary>
      /// Property IdQuejaMasiva
      /// </summary>
      [DataMember]
      public long? IdQuejaMasiva { get; set; }
      /// <summary>
      /// Property Loc
      /// </summary>
      [DataMember]
      public int? Loc { get; set; }
    #endregion
    }
    }

  
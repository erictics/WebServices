﻿
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    namespace Softv.Entities
    {
    /// <summary>
    /// Class                   : Softv.Entities.ReporteEdoC2Entity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ReporteEdoC2 entity
    /// File                    : ReporteEdoC2Entity.cs
    /// Creation date           : 08/03/2017
    /// Creation time           : 01:17 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ReporteEdoC2Entity : BaseEntity
    {
    #region Attributes
    
      /// <summary>
      /// Property IdDetalle
      /// </summary>
      [DataMember]
      public int? IdDetalle { get; set; }
      /// <summary>
      /// Property IdEstadoCuenta
      /// </summary>
      [DataMember]
      public int? IdEstadoCuenta { get; set; }
      /// <summary>
      /// Property Posicion
      /// </summary>
      [DataMember]
      public int? Posicion { get; set; }
      /// <summary>
      /// Property SubPosicion
      /// </summary>
      [DataMember]
      public int? SubPosicion { get; set; }
      /// <summary>
      /// Property Tipo
      /// </summary>
      [DataMember]
      public String Tipo { get; set; }
      /// <summary>
      /// Property Formato
      /// </summary>
      [DataMember]
      public String Formato { get; set; }
      /// <summary>
      /// Property Descripcion
      /// </summary>
      [DataMember]
      public String Descripcion { get; set; }
      /// <summary>
      /// Property Importe
      /// </summary>
      [DataMember]
      public Decimal? Importe { get; set; }
    #endregion
    }
    }

  
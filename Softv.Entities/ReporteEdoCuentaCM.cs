﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{

    [DataContract]
    [Serializable]
    public class ReporteEdoCuentaCM
    {
        #region Attributes

        /// <summary>
        /// Property IdEstadoCuenta
        /// </summary>
        [DataMember]
        public int? X { get; set; }
        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public long? Contrato { get; set; }
        /// <summary>
        /// Property TarifaRegular
        /// </summary>
        [DataMember]
        public Decimal? TarifaRegular { get; set; }
        /// <summary>
        /// Property TarifaProntoPago
        /// </summary>
        [DataMember]
        public Decimal? TarifaProntoPago { get; set; }
        /// <summary>
        /// Property FechaRegular
        /// </summary>
        [DataMember]
        public String FechaRegular { get; set; }
        /// <summary>
        /// Property FechaProntoPago
        /// </summary>
        [DataMember]
        public String FechaProntoPago { get; set; }
        /// <summary>
        /// Property TotalRegular
        /// </summary>
        [DataMember]
        public Decimal? TotalRegular { get; set; }
        /// <summary>
        /// Property TotalProntoPago
        /// </summary>
        [DataMember]
        public Decimal? TotalProntoPago { get; set; }
        /// <summary>
        /// Property Paquete
        /// </summary>
        [DataMember]
        public String Paquete { get; set; }
        /// <summary>
        /// Property Bancomer
        /// </summary>
        [DataMember]
        public String Bancomer { get; set; }
        /// <summary>
        /// Property Oxxo
        /// </summary>
        [DataMember]
        public String Oxxo { get; set; }
        /// <summary>
        /// Property CodigoDeBarrasOxxo
        /// </summary>
        [DataMember]
        public byte[] CodigoDeBarrasOxxo { get; set; }
        /// <summary>
        /// Property NOMBRE
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        /// <summary>
        /// Property CALLE
        /// </summary>
        [DataMember]
        public int? CALLE { get; set; }
        /// <summary>
        /// Property NUMERO
        /// </summary>
        [DataMember]
        public String NUMERO { get; set; }
        /// <summary>
        /// Property COLONIA
        /// </summary>
        [DataMember]
        public int? COLONIA { get; set; }
        /// <summary>
        /// Property CIUDAD
        /// </summary>
        [DataMember]
        public int? CIUDAD { get; set; }
        /// <summary>
        /// Property ENTRECALLES
        /// </summary>
        [DataMember]
        public String ENTRECALLES { get; set; }
        /// <summary>
        /// Property CP
        /// </summary>
        [DataMember]
        public int? CP { get; set; }
        /// <summary>
        /// Property LOCALIDAD
        /// </summary>
        [DataMember]
        public int? LOCALIDAD { get; set; }
        /// <summary>
        /// Property Estado
        /// </summary>
        [DataMember]
        public int? Estado { get; set; }
        /// <summary>
        /// Property FechaGeneracion
        /// </summary>
        [DataMember]
        public String FechaGeneracion { get; set; }
        /// <summary>
        /// Property LetraTotalRegular
        /// </summary>
        [DataMember]
        public String LetraTotalRegular { get; set; }

        [DataMember]
        public String LetraTotalProntoPago { get; set; }
        

        [DataMember]
        public byte[] imgB { get; set; }






        [DataMember]
        public long? ClvSessionPadre { get; set; }

        [DataMember]
        public long? ContratoMaestro { get; set; }

        [DataMember]
        public String Referencia { get; set; }



        /// <summary>
        /// Property LetraTotalProntoPago
        /// </summary>
        //[DataMember]
        //public String LetraTotalProntoPago { get; set; }
        ///// <summary>
        ///// Property imgPP
        ///// </summary>
        //[DataMember]
        //public byte[] imgPP { get; set; }
        ///// <summary>
        ///// Property imgTR
        ///// </summary>
        //[DataMember]
        //public byte[] imgTR { get; set; }
        ///// <summary>
        ///// Property imgC
        ///// </summary>
        //[DataMember]
        //public byte[] imgC { get; set; }
        ///// <summary>
        ///// Property imgO
        ///// </summary>
        //[DataMember]
        //public byte[] imgO { get; set; }


        #endregion
    }
}

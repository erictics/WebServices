﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{


    [DataContract]
    [Serializable]
    public class ReportecarteraEntity : BaseEntity
    {
        [DataMember]
        public int  Clv_cartera { get; set; }
        [DataMember]
        public string Distribuidor { get; set; }
        [DataMember]
        public string Region { get; set; }

        [DataMember]
        public int Clv_tipser { get; set; }

        [DataMember]
        public int identificador { get; set; }

        [DataMember]
        public string Periodo { get; set; }

        [DataMember]
        public long? Clv_SessionBancos { get; set; }
    }


    [DataContract]
    [Serializable]
    public class ReporteMantenimientoEntity : BaseEntity
    {
        [DataMember]
        public int ? Clv_QuejaIni { get; set; }

        [DataMember]
        public int? Clv_QuejaFin { get; set; }

        [DataMember]
        public string FechaIni { get; set; }

        [DataMember]
        public string FechaFin { get; set; }

        [DataMember]
        public string StatusE { get; set; }

        [DataMember]
        public string StatusP { get; set; }

        [DataMember]
        public string StatusS { get; set; }

        [DataMember]
        public int ? Clv_Trabajo { get; set; }

        [DataMember]
        public int? ClvProblema { get; set; }

        [DataMember]
        public int? Op1 { get; set; }

        [DataMember]
        public int? Op2 { get; set; }

        [DataMember]
        public int? Op4 { get; set; }
        [DataMember]
        public int? Op5 { get; set; }
        [DataMember]
        public int? OpF { get; set; }

        [DataMember]
        public bool? TipoReporte { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReporteEntity : BaseEntity
    {
        [DataMember]
        public List<PlazaRepVariosEntity> distribuidores { get; set; }
        [DataMember]
        public List<companiaEntity> plazas { get; set; }
        [DataMember]
        public List<CiudadCAMDOEntity> ciudades { get; set; }
        [DataMember]
        public List<LocalidadCAMDOEntity> localidades { get; set; }
        [DataMember]
        public List<ColoniaCAMDOEntity> colonias { get; set; }
        [DataMember]
        public List<PeriodosEntity> periodos { get; set; }
        [DataMember]
        public List<TiposClienteEntity> tiposcliente { get; set; }

        [DataMember]
        public List<ServiciosInternetEntity> servicios { get; set; }
        [DataMember]
        public string contrato { get; set; }
        [DataMember]
        public List<CalleCAMDOEntity> calles { get; set; }
        [DataMember]
        public List<EstadoEntity> estados { get; set; }
        [DataMember]
        public String estatus { get; set; }
        [DataMember]
        public int? Clv_inicio { get; set; }
        [DataMember]
        public int? Clv_Orden { get; set; }
        [DataMember]
        public int? Clv_fin { get; set; }
        [DataMember]
        public string fechasolInicial { get; set; }
        [DataMember]
        public string fechasolFinal { get; set; }
        [DataMember]
        public string fechaejeInicial { get; set; }
        [DataMember]
        public string fechaejeFinal { get; set; }
        [DataMember]
        public int? Clv_trabajo { get; set; }

        [DataMember]
        public int? clv_Depto { get; set; }

        [DataMember]
        public int? clvProblema { get; set; }
        [DataMember]
        public int? clvQueja { get; set; }

        [DataMember]
        public int? Op { get; set; }
        [DataMember]
        public int? OpOrdenar { get; set; }
        [DataMember]
        public int? Clv_usuario { get; set; }

        [DataMember]
        public int? Clv_TipSer { get; set; }
        [DataMember]
        public bool? ejecutados { get; set; }
        [DataMember]
        public bool? pendientes { get; set; }
        [DataMember]
        public bool? visitados { get; set; }
        [DataMember]
        public bool? enproceso { get; set; }
        [DataMember]
        public bool? op0 { get; set; }
        [DataMember]
        public bool? op1 { get; set; }
        [DataMember]
        public bool? op2 { get; set; }
        [DataMember]
        public bool? op3 { get; set; }
        [DataMember]
        public bool? op4 { get; set; }
        [DataMember]
        public bool? op5 { get; set; }
        [DataMember]
        public bool? op6 { get; set; }
        [DataMember]
        public bool? op7 { get; set; }

        [DataMember]
        public bool? conQueja { get; set; }

        [DataMember]
        public bool? sinqueja { get; set; }

        [DataMember]
        public bool? ambas { get; set; }

        [DataMember]
        public bool? conectado { get; set; }

        [DataMember]
        public bool? baja { get; set; }

        [DataMember]
        public bool? instalacion { get; set; }

        [DataMember]
        public bool? desconectado { get; set; }

        [DataMember]
        public bool? suspendido { get; set; }

        [DataMember]
        public bool? fueraArea { get; set; }
        [DataMember]
        public bool? DescTmp { get; set; }

        [DataMember]
        public int? MotCan { get; set; }


        [DataMember]
        public int? ultimoMes { get; set; }

        [DataMember]
        public int? ultimoAnio { get; set; }

        [DataMember]
        public bool? soloInternet { get; set; }

        [DataMember]
        public int opCaracter { get; set; }


    }
}

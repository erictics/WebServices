﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ReporteEstadoCuentaNuevoEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ReporteEstadoCuentaNuevo entity
    /// File                    : ReporteEstadoCuentaNuevoEntity.cs
    /// Creation date           : 08/03/2017
    /// Creation time           : 10:37 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ReporteEstadoCuentaNuevoEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Id
        /// </summary>
        [DataMember]
        public int? Id { get; set; }
        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public String Contrato { get; set; }

        


        [DataMember]
        public long? IdEstadoCuenta { get; set; }

        [DataMember]
        public String lineaPP { get; set; }

        [DataMember]
        public String lineaTR { get; set; }

        [DataMember]
        public String lineaC { get; set; }

        [DataMember]
        public String lineaO { get; set; }



        [DataMember]
        public long? ClvSessionPadre { get; set; }

        [DataMember]
        public long? ContratoMaestro { get; set; }

        [DataMember]
        public String Referencia { get; set; }



        #endregion
    }
}


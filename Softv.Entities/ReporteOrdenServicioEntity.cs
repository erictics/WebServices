﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ReporteOrdenServicioEntity
    {
        [DataMember]
        public int ? Clv_TipSer { get; set; }
        [DataMember]
        public int ? op1 { get; set;  }
        [DataMember]
        public int? op2 { get; set; }
        [DataMember]
        public int? op3 { get; set; }
        [DataMember]
        public int? op4 { get; set; }
        [DataMember]
        public int? op5 { get; set; }
        [DataMember]
        public bool StatusPen { get; set; }
        [DataMember]
        public bool StatusEje { get; set; }
        [DataMember]
        public bool StatusVis { get; set; }
        [DataMember]
        public int? Clv_OrdenIni { get; set; }
        [DataMember]
        public int? Clv_OrdenFin { get; set; }
        [DataMember]
        public String Fec1Ini { get; set; }
        [DataMember]
        public String Fec1Fin { get; set; }
        [DataMember]
        public String Fec2Ini { get; set; }
        [DataMember]
        public String Fec2Fin { get; set; }
        [DataMember]
        public int ? Clv_Trabajo { get; set; }
        [DataMember]
        public int ? Clv_Colonia { get; set; }
        [DataMember]
        public int? OpOrden { get; set; }
        [DataMember]
        public int ? IdCompania { get; set; }
        [DataMember]
        public int ? clv_ciudad { get; set; }
        [DataMember]
        public int ? clv_usuario { get; set; }
     
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
   public  class ResponseEntity : BaseEntity
    {
        [DataMember]
        public bool? Bnd { get; set; }

        [DataMember]
        public int? Res { get; set; }

        [DataMember]
        public string Msj { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ServicioDescuentoEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clv_UnicaNet { get; set; }

        [DataMember]
        public int? Clv_TipServ { get; set; }

        [DataMember]
        public int? Descuento { get; set; }

        #endregion
    }
}

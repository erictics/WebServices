﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.SessionWebDosEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : SessionWebDos entity
    /// File                    : SessionWebDosEntity.cs
    /// Creation date           : 12/01/2017
    /// Creation time           : 06:11 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class SessionWebDosEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Usuario
        /// </summary>
        [DataMember]
        public String Usuario { get; set; }

        /// <summary>
        /// Property NombreUser
        /// </summary>
        [DataMember]
        public String NombreUser { get; set; }

        /// <summary>
        /// Property TipoUser
        /// </summary>
        [DataMember]
        public int? TipoUser { get; set; }

        /// <summary>
        /// Property Token
        /// </summary>
        [DataMember]
        public String Token { get; set; }


        [DataMember]
        public String Pass { get; set; }



        [DataMember]
        public int? Id { get; set; }


        #endregion
    }
}


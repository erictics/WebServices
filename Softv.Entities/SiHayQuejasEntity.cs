﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.QuejasEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Quejas entity
    /// File                    : QuejasEntity.cs
    /// Creation date           : 25/01/2017
    /// Creation time           : 12:14 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class SiHayQuejasEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Queja
        /// </summary>
        [DataMember]
        public long? Clv_Queja { get; set; }

        /// <summary>
        /// Property Concepto
        /// </summary>
        [DataMember]
        public String Concepto { get; set; }
        
        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class StatusEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Clave { get; set; }

        [DataMember]
        public String Clv_StatusNet { get; set; }

        [DataMember]
        public String Concepto { get; set; }

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.SucursalReturnEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : SucursalReturn entity
    /// File                    : SucursalReturnEntity.cs
    /// Creation date           : 25/11/2016
    /// Creation time           : 10:52 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class SucursalReturnEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property NOMBRE
        /// </summary>
        [DataMember]
        public String NOMBRE { get; set; }
        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.Sucursales_ReportesVentasEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Sucursales_ReportesVentas entity
    /// File                    : Sucursales_ReportesVentasEntity.cs
    /// Creation date           : 26/07/2017
    /// Creation time           : 04:53 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class Sucursales_ReportesVentasEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Sucursal
        /// </summary>
        [DataMember]
        public int? Clv_Sucursal { get; set; }
        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        




        [DataMember]
        public List<Plaza_ReportesVentasEntity> LstPlaza { get; set; }



        #endregion
    }
}


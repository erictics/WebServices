﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.SuspencionTemporalEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : SuspencionTemporal entity
    /// File                    : SuspencionTemporalEntity.cs
    /// Creation date           : 22/10/2016
    /// Creation time           : 11:29 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class SuspencionTemporalEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Meg
        /// </summary>
        [DataMember]
        public String Meg { get; set; }



        [DataMember]
        public long? Contrato { get; set; }

        [DataMember]
        public long? IdSession { get; set; }

        #endregion
    }
}


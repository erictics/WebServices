﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.T1RepArqueoEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : T1RepArqueo entity
    /// File                    : T1RepArqueoEntity.cs
    /// Creation date           : 15/12/2016
    /// Creation time           : 01:01 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class T1RepArqueoEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Fecha
        /// </summary>
        [DataMember]
        public String Fecha { get; set; }
        /// <summary>
        /// Property Cajera
        /// </summary>
        [DataMember]
        public String Cajera { get; set; }
        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }



        [DataMember]
        public String ClvUsuario { get; set; }


        [DataMember]
        public int? Id { get; set; }

        #endregion
    }
}


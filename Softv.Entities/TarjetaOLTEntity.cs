﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class TarjetaOLTEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? Calve { get; set; }

        [DataMember]
        public String descripcion { get; set; }

        [DataMember]
        public int? PUERTOS { get; set; }

        [DataMember]
        public int? IdOlt { get; set; }

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.TblNetEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : TblNet entity
    /// File                    : TblNetEntity.cs
    /// Creation date           : 31/10/2017
    /// Creation time           : 05:47 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class TblNetEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public int? ClvServicio { get; set; }

        [DataMember]
        public String Clv_Txt { get; set; }

        [DataMember]
        public String Clv_Eq { get; set; }

        [DataMember]
        public String Msg { get; set; }

        [DataMember]
        public int? Error { get; set; }

        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public int? IdMedio { get; set; }

        [DataMember]
        public String Descripcion { get; set; }

        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.TblNotasMaestraOpcionesEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : TblNotasMaestraOpciones entity
    /// File                    : TblNotasMaestraOpcionesEntity.cs
    /// Creation date           : 16/05/2017
    /// Creation time           : 11:32 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class TblNotasMaestraOpcionesEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_NotaMaestra
        /// </summary>
        [DataMember]
        public long? Clv_NotaMaestra { get; set; }
        /// <summary>
        /// Property OpCancelar
        /// </summary>
        [DataMember]
        public bool? OpCancelar { get; set; }
        /// <summary>
        /// Property OpReimprimir
        /// </summary>
        [DataMember]
        public bool? OpReimprimir { get; set; }
        /// <summary>
        /// Property OpCorreo
        /// </summary>
        [DataMember]
        public bool? OpCorreo { get; set; }


        [DataMember]
        public bool? OpRefacturar { get; set; }




        #endregion
    }
}


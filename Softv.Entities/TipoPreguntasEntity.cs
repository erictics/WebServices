﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.TipoPreguntasEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : TipoPreguntas entity
    /// File                    : TipoPreguntasEntity.cs
    /// Creation date           : 17/06/2017
    /// Creation time           : 11:01 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class TipoPreguntasEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property IdTipoPregunta
        /// </summary>
        [DataMember]
        public int? IdTipoPregunta { get; set; }
        /// <summary>
        /// Property Descripcion
        /// </summary>
        [DataMember]
        public String Descripcion { get; set; }
        [DataMember]
        public PreguntasEntity Preguntas { get; set; }

        #endregion
    }
}


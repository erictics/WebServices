﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class TrabajosOrdenesTecnicoDiaEntity : BaseEntity
    {
        #region Attributes
        [DataMember]
        public int? ID { get; set; }

        [DataMember]
        public int? Clv_Cita { get; set; }

        [DataMember]
        public int? Contrato { get; set; }

        [DataMember]
        public String QuejaOrden { get; set; }

        [DataMember]
        public String Fec_Sol { get; set; }

        [DataMember]
        public String hora { get; set; }

        [DataMember]
        public String DESCRIPCION { get; set; }

        [DataMember]
        public String Obs { get; set; }

        [DataMember]
        public int? CONTQEUJAS { get; set; }

        [DataMember]
        public int? CONTORDENES { get; set; }

        [DataMember]
        public int? CONTQEUJA { get; set; }

        [DataMember]
        public int? CONTORDENE { get; set; }

        [DataMember]
        public int? Clv_OrdenQueja { get; set; }

        [DataMember]
        public String Estatus { get; set; }

        [DataMember]
        public String Turno { get; set; }

        [DataMember]
        public int? TOTALQUEJAS { get; set; }

        [DataMember]
        public int? TOTALORDENES { get; set; }
        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.UltimoSerieYFolioEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : UltimoSerieYFolio entity
    /// File                    : UltimoSerieYFolioEntity.cs
    /// Creation date           : 27/10/2016
    /// Creation time           : 05:56 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class UltimoSerieYFolioEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Clv_Vendedor
        /// </summary>
        [DataMember]
        public int? Clv_Vendedor { get; set; }
        /// <summary>
        /// Property SERIE
        /// </summary>
        [DataMember]
        public String SERIE { get; set; }
        /// <summary>
        /// Property ULTIMOFOLIO_USADO
        /// </summary>
        [DataMember]
        public int? ULTIMOFOLIO_USADO { get; set; }






        [DataMember]
        public long? Contrato { get; set; }




        #endregion
    }
}


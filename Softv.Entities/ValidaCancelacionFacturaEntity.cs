﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ValidaCancelacionFacturaEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidaCancelacionFactura entity
    /// File                    : ValidaCancelacionFacturaEntity.cs
    /// Creation date           : 03/02/2017
    /// Creation time           : 05:19 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ValidaCancelacionFacturaEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Res
        /// </summary>
        [DataMember]
        public int? Res { get; set; }
        /// <summary>
        /// Property Msg
        /// </summary>
        [DataMember]
        public String Msg { get; set; }





        [DataMember]
        public long? ClvFactura { get; set; }


        #endregion
    }
}


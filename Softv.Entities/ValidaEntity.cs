﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ValidaEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public bool Resultado { get; set; }

        [DataMember]
        public String MSJ { get; set; }

        #endregion
    }
}

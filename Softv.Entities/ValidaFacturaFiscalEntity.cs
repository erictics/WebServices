﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ValidaFacturaFiscalEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidaFacturaFiscal entity
    /// File                    : ValidaFacturaFiscalEntity.cs
    /// Creation date           : 07/02/2017
    /// Creation time           : 11:15 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ValidaFacturaFiscalEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property ClvFactura
        /// </summary>
        [DataMember]
        public long? ClvFactura { get; set; }
        /// <summary>
        /// Property fiscal
        /// </summary>
        [DataMember]
        public int? fiscal { get; set; }
        #endregion
    }
}


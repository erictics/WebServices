﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.ValidaPideSiTieneMasAparatosEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidaPideSiTieneMasAparatos entity
    /// File                    : ValidaPideSiTieneMasAparatosEntity.cs
    /// Creation date           : 22/10/2016
    /// Creation time           : 12:38 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class ValidaPideSiTieneMasAparatosEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Valor
        /// </summary>
        [DataMember]
        public bool? Valor { get; set; }








        [DataMember]
        public long? IdSession { get; set; }


        [DataMember]
        public long? IdDetalle { get; set; }


        [DataMember]
        public long? Contrato { get; set; }


        #endregion
    }
}


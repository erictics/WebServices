﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class ValidaServicoEntity : BaseEntity
    {
        #region Attributes

        [DataMember]
        public bool? Resultado { get; set; }

        [DataMember]
        public String Mensaje { get; set; }

        #endregion
    }



    [DataContract]
    [Serializable]
    public class DetalleCobroFacEntity : BaseEntity
    {
        #region Attributes
        [DataMember]
        public string titulo { get; set; }

        [DataMember]
        public string Articulo { get; set; }

        [DataMember]
        public int cantidad { get; set; }

        [DataMember]
        public decimal monto { get; set; }
        #endregion
    }


    [DataContract]
    [Serializable]
    public class ConvenioCobroEntity : BaseEntity
    {
        #region Attributes
        [DataMember]
        public long  Contrato { get; set; }

        [DataMember]
        public decimal Suma { get; set; }

        [DataMember]
        public decimal ImporteMes { get; set; }

        [DataMember]
        public decimal NumPagos { get; set; }

        [DataMember]
        public long  Clv_UnicaNet { get; set; }
        #endregion
    }
}

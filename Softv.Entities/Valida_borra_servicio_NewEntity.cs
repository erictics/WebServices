﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.Valida_borra_servicio_NewEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Valida_borra_servicio_New entity
    /// File                    : Valida_borra_servicio_NewEntity.cs
    /// Creation date           : 18/09/2017
    /// Creation time           : 04:24 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class Valida_borra_servicio_NewEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property clv_txt
        /// </summary>
        [DataMember]
        public String clv_txt { get; set; }
        /// <summary>
        /// Property error
        /// </summary>
        [DataMember]
        public int? error { get; set; }
        /// <summary>
        /// Property mensaje
        /// </summary>
        [DataMember]
        public String mensaje { get; set; }


        [DataMember]
        public int? Id { get; set; }

        #endregion
    }
}


﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.VendedoresEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Vendedores entity
    /// File                    : VendedoresEntity.cs
    /// Creation date           : 22/11/2017
    /// Creation time           : 10:24 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class VendedoresEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        /// <summary>
        /// Property Domicilio
        /// </summary>
        [DataMember]
        public String Domicilio { get; set; }
        /// <summary>
        /// Property Colonia
        /// </summary>
        [DataMember]
        public String Colonia { get; set; }
        /// <summary>
        /// Property FechaIngreso
        /// </summary>
        [DataMember]
        public String FechaIngreso { get; set; }
        /// <summary>
        /// Property FechaSalida
        /// </summary>
        [DataMember]
        public String FechaSalida { get; set; }
        /// <summary>
        /// Property Activo
        /// </summary>
        [DataMember]
        public bool? Activo { get; set; }
        /// <summary>
        /// Property Clv_TipPro
        /// </summary>
        [DataMember]
        public int? Clv_TipPro { get; set; }
        /// <summary>
        /// Property Clv_Grupo
        /// </summary>
        [DataMember]
        public int? Clv_Grupo { get; set; }
        /// <summary>
        /// Property idcompania
        /// </summary>
        [DataMember]
        public int? idcompania { get; set; }
        /// <summary>
        /// Property Clv_Vendedor
        /// </summary>
        [DataMember]
        public int? Clv_Vendedor { get; set; }




        [DataMember]
        public bool? Capacitacion { get; set; }


        [DataMember]
        public int? ClvUsuario { get; set; }

        [DataMember]
        public int? Op { get; set; }


        #endregion
    }
}


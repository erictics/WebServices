﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.sp_verificaCoordenadasEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : sp_verificaCoordenadas entity
    /// File                    : sp_verificaCoordenadasEntity.cs
    /// Creation date           : 24/07/2017
    /// Creation time           : 05:40 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class sp_verificaCoordenadasEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property Contrato
        /// </summary>
        [DataMember]
        public long? Contrato { get; set; }
        /// <summary>
        /// Property Clv_orden
        /// </summary>
        [DataMember]
        public long? Clv_orden { get; set; }
        /// <summary>
        /// Property TieneCoordenadas
        /// </summary>
        [DataMember]
        public int? TieneCoordenadas { get; set; }
        #endregion
    }
}


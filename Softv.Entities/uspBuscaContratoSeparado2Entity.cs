﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.uspBuscaContratoSeparado2Entity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : uspBuscaContratoSeparado2 entity
    /// File                    : uspBuscaContratoSeparado2Entity.cs
    /// Creation date           : 24/01/2017
    /// Creation time           : 12:15 p. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class uspBuscaContratoSeparado2Entity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property CONTRATO
        /// </summary>
        [DataMember]
        public String CONTRATO { get; set; }
        /// <summary>
        /// Property Nombre
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        /// <summary>
        /// Property Apellido_Paterno
        /// </summary>
        [DataMember]
        public String Apellido_Paterno { get; set; }
        /// <summary>
        /// Property Apellido_Materno
        /// </summary>
        [DataMember]
        public String Apellido_Materno { get; set; }
        /// <summary>
        /// Property CALLE
        /// </summary>
        [DataMember]
        public String CALLE { get; set; }
        /// <summary>
        /// Property COLONIA
        /// </summary>
        [DataMember]
        public String COLONIA { get; set; }
        /// <summary>
        /// Property NUMERO
        /// </summary>
        [DataMember]
        public String NUMERO { get; set; }
        /// <summary>
        /// Property CIUDAD
        /// </summary>
        [DataMember]
        public String CIUDAD { get; set; }
        /// <summary>
        /// Property SOLOINTERNET
        /// </summary>
        [DataMember]
        public bool? SOLOINTERNET { get; set; }
        /// <summary>
        /// Property ESHOTEL
        /// </summary>
        [DataMember]
        public bool? ESHOTEL { get; set; }
        /// <summary>
        /// Property ContratoBueno
        /// </summary>
        [DataMember]
        public long? ContratoBueno { get; set; }

        [DataMember]
        public int? Activo { get; set; }





        [DataMember]
        public String ContratoCom { get; set; }

        [DataMember]
        public int? ClvColonia { get; set; }

        [DataMember]
        public String SetupBox { get; set; }

        [DataMember]
        public int? IdUsuario { get; set; }

        [DataMember]
        public int? TipoSer { get; set; }

        [DataMember]
        public int? Op { get; set; }


        [DataMember]
        public int? IdDistribuidor { get; set; }





        #endregion
    }
}


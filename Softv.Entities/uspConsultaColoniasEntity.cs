﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    /// <summary>
    /// Class                   : Softv.Entities.uspConsultaColoniasEntity.cs
    /// Generated by            : Class Generator (c) 2014
    /// Description             : uspConsultaColonias entity
    /// File                    : uspConsultaColoniasEntity.cs
    /// Creation date           : 14/01/2017
    /// Creation time           : 11:46 a. m.
    ///</summary>
    [DataContract]
    [Serializable]
    public class uspConsultaColoniasEntity : BaseEntity
    {
        #region Attributes

        /// <summary>
        /// Property clvColonia
        /// </summary>
        [DataMember]
        public int? clvColonia { get; set; }

        /// <summary>
        /// Property nombre
        /// </summary>
        [DataMember]
        public String nombre { get; set; }



        [DataMember]
        public int? idcompania { get; set; }


        #endregion
    }
}


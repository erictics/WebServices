﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softv.Providers
{
    public class ProviderSoftv
    {
        public static UsuarioProvider Usuario
        {
            get { return UsuarioProvider.Instance; }
        }
        public static RoleProvider Role
        {
            get { return RoleProvider.Instance; }
        }
        public static PermisoProvider Permiso
        {
            get { return PermisoProvider.Instance; }
        }
        public static ModuleProvider Module
        {
            get { return ModuleProvider.Instance; }
        }
        public static SecutityProvider Secutity
        {
            get { return SecutityProvider.Instance; }
        }

        public static SessionProvider Session
        {
            get { return SessionProvider.Instance; }
        }






        public static DameClv_SessionProvider DameClv_Session
        {
            get { return DameClv_SessionProvider.Instance; }
        }

        public static BusCliPorContrato_FacProvider BusCliPorContrato_Fac
        {
            get { return BusCliPorContrato_FacProvider.Instance; }
        }

        public static uspBusCliPorContratoSeparadoProvider uspBusCliPorContratoSeparado
        {
            get { return uspBusCliPorContratoSeparadoProvider.Instance; }
        }

        public static DameDatosUsuarioProvider DameDatosUsuario
        {
            get { return DameDatosUsuarioProvider.Instance; }
        }

        public static DameNombreSucursalProvider DameNombreSucursal
        {
            get { return DameNombreSucursalProvider.Instance; }
        }

        public static DameDistribuidorUsuarioProvider DameDistribuidorUsuario
        {
            get { return DameDistribuidorUsuarioProvider.Instance; }
        }

        public static DameSerDelCliFacProvider DameSerDelCliFac
        {
            get { return DameSerDelCliFacProvider.Instance; }
        }

        public static DameTiposClientesProvider DameTiposClientes
        {
            get { return DameTiposClientesProvider.Instance; }
        }

        public static DameDetalleProvider DameDetalle
        {
            get { return DameDetalleProvider.Instance; }
        }

        public static SumaDetalleProvider SumaDetalle
        {
            get { return SumaDetalleProvider.Instance; }
        }



        public static DameDatosUpProvider DameDatosUp
        {
            get { return DameDatosUpProvider.Instance; }
        }

        public static SessionWebProvider SessionWeb
        {
            get { return SessionWebProvider.Instance; }
        }

        public static DameSessionWProvider DameSessionW
        {
            get { return DameSessionWProvider.Instance; }
        }

        public static MuestraTipoFacturaHistorialProvider MuestraTipoFacturaHistorial
        {
            get { return MuestraTipoFacturaHistorialProvider.Instance; }
        }


        public static BuscaFacturasHistorialProvider BuscaFacturasHistorial
        {
            get { return BuscaFacturasHistorialProvider.Instance; }
        }

        public static BuscaOrdenServicioProvider BuscaOrdenServicio
        {
            get { return BuscaOrdenServicioProvider.Instance; }
        }

        public static MuestraTipSerPrincipalProvider MuestraTipSerPrincipal
        {
            get { return MuestraTipSerPrincipalProvider.Instance; }
        }

        public static BuscaQuejasLProvider BuscaQuejasL
        {
            get { return BuscaQuejasLProvider.Instance; }
        }

        public static InformacionClientePeriodosProvider InformacionClientePeriodos
        {
            get { return InformacionClientePeriodosProvider.Instance; }
        }

        public static BotonClabeProvider BotonClabe
        {
            get { return BotonClabeProvider.Instance; }
        }

        public static SuspencionTemporalProvider SuspencionTemporal
        {
            get { return SuspencionTemporalProvider.Instance; }
        }

        public static ValidaPideAparatosProvider ValidaPideAparatos
        {
            get { return ValidaPideAparatosProvider.Instance; }
        }

        public static ValidaPideSiTieneMasAparatosProvider ValidaPideSiTieneMasAparatos
        {
            get { return ValidaPideSiTieneMasAparatosProvider.Instance; }
        }

        public static MuestraAparatosCobroAdeudoProvider MuestraAparatosCobroAdeudo
        {
            get { return MuestraAparatosCobroAdeudoProvider.Instance; }
        }

        public static GrabaDevolucionAparatoProvider GrabaDevolucionAparato
        {
            get { return GrabaDevolucionAparatoProvider.Instance; }
        }

        public static CobraAdeudoProvider CobraAdeudo
        {
            get { return CobraAdeudoProvider.Instance; }
        }

        public static QuitarAntenaDevolucionProvider QuitarAntenaDevolucion
        {
            get { return QuitarAntenaDevolucionProvider.Instance; }
        }

        public static MotCanProvider MotCan
        {
            get { return MotCanProvider.Instance; }
        }

        public static ChecarServiciosCanceladosProvider ChecarServiciosCancelados
        {
            get { return ChecarServiciosCanceladosProvider.Instance; }
        }

        public static MuestraColoniasProvider MuestraColonias
        {
            get { return MuestraColoniasProvider.Instance; }
        }

        public static MuestraServiciosFACProvider MuestraServiciosFAC
        {
            get { return MuestraServiciosFACProvider.Instance; }
        }

        public static CiudadCAMDOProvider CiudadCAMDO
        {
            get { return CiudadCAMDOProvider.Instance; }
        }

        public static LocalidadCAMDOProvider LocalidadCAMDO
        {
            get { return LocalidadCAMDOProvider.Instance; }
        }

        public static ColoniaCAMDOProvider ColoniaCAMDO
        {
            get { return ColoniaCAMDOProvider.Instance; }
        }

        public static CalleCAMDOProvider CalleCAMDO
        {
            get { return CalleCAMDOProvider.Instance; }
        }

        public static CAMDOFACProvider CAMDOFAC
        {
            get { return CAMDOFACProvider.Instance; }
        }

        public static QuitarDetalleProvider QuitarDetalle
        {
            get { return QuitarDetalleProvider.Instance; }
        }

        public static BitacoraProvider Bitacora
        {
            get { return BitacoraProvider.Instance; }
        }

        public static DameRelSucursalCompaProvider DameRelSucursalCompa
        {
            get { return DameRelSucursalCompaProvider.Instance; }
        }

        public static SumaTotalDetalleProvider SumaTotalDetalle
        {
            get { return SumaTotalDetalleProvider.Instance; }
        }

        public static MuestraBancosProvider MuestraBancos
        {
            get { return MuestraBancosProvider.Instance; }
        }

        public static MuestraTiposTarjetasProvider MuestraTiposTarjetas
        {
            get { return MuestraTiposTarjetasProvider.Instance; }
        }

        public static MontoNotaCreditoProvider MontoNotaCredito
        {
            get { return MontoNotaCreditoProvider.Instance; }
        }

        public static NuevoPagoProvider NuevoPago
        {
            get { return NuevoPagoProvider.Instance; }
        }

        public static VendedoresLProvider VendedoresL
        {
            get { return VendedoresLProvider.Instance; }
        }

        public static UltimoSerieYFolioProvider UltimoSerieYFolio
        {
            get { return UltimoSerieYFolioProvider.Instance; }
        }

        public static FolioDisponibleProvider FolioDisponible
        {
            get { return FolioDisponibleProvider.Instance; }
        }

        public static ChecaFolioUsadoProvider ChecaFolioUsado
        {
            get { return ChecaFolioUsadoProvider.Instance; }
        }

        public static AdelantarProvider Adelantar
        {
            get { return AdelantarProvider.Instance; }
        }

        public static PagoAdelantadoProvider PagoAdelantado
        {
            get { return PagoAdelantadoProvider.Instance; }
        }

        public static GrabaFacturas2Provider GrabaFacturas2
        {
            get { return GrabaFacturas2Provider.Instance; }
        }

        public static AddServicioAdicionalesProvider AddServicioAdicionales
        {
            get { return AddServicioAdicionalesProvider.Instance; }
        }

        public static CobraBajasOPrecobraAdeudoProvider CobraBajasOPrecobraAdeudo
        {
            get { return CobraBajasOPrecobraAdeudoProvider.Instance; }
        }

        public static CrearTicketTableProvider CrearTicketTable
        {
            get { return CrearTicketTableProvider.Instance; }
        }

        public static ConceptosTicketPagosProvider ConceptosTicketPagos
        {
            get { return ConceptosTicketPagosProvider.Instance; }
        }

        public static ConsultaOrsSerProvider ConsultaOrsSer
        {
            get { return ConsultaOrsSerProvider.Instance; }
        }

        public static ConsultarQuejasTableProvider ConsultarQuejasTable
        {
            get { return ConsultarQuejasTableProvider.Instance; }
        }
        public static DistribuidorReporteProvider DistribuidorReporte
        {
            get { return DistribuidorReporteProvider.Instance; }
        }

        public static RepDistribuidoresProvider RepDistribuidores
        {
            get { return RepDistribuidoresProvider.Instance; }
        }

        public static PlazaReporteProvider PlazaReporte
        {
            get { return PlazaReporteProvider.Instance; }
        }

        public static RepPlazaProvider RepPlaza
        {
            get { return RepPlazaProvider.Instance; }
        }

        public static Muestra_SucursalesCortesProvider Muestra_SucursalesCortes
        {
            get { return Muestra_SucursalesCortesProvider.Instance; }
        }

        public static Muestra_UsuariosCortesProvider Muestra_UsuariosCortes
        {
            get { return Muestra_UsuariosCortesProvider.Instance; }
        }

        public static Muestra_VendedoresCortesProvider Muestra_VendedoresCortes
        {
            get { return Muestra_VendedoresCortesProvider.Instance; }
        }

        public static Muestra_CajasCortesProvider Muestra_CajasCortes
        {
            get { return Muestra_CajasCortesProvider.Instance; }
        }

        public static OpcionesCortesFacturasProvider OpcionesCortesFacturas
        {
            get { return OpcionesCortesFacturasProvider.Instance; }
        }

        public static CatalogoReportesFacProvider CatalogoReportesFac
        {
            get { return CatalogoReportesFacProvider.Instance; }
        }

        public static AddTokenSeguridadFacProvider AddTokenSeguridadFac
        {
            get { return AddTokenSeguridadFacProvider.Instance; }
        }

        public static SeguridadTokenProvider SeguridadToken
        {
            get { return SeguridadTokenProvider.Instance; }
        }

        public static PlazasXmlProvider PlazasXml
        {
            get { return PlazasXmlProvider.Instance; }
        }

        public static ReporteCortesProvider ReporteCortes
        {
            get { return ReporteCortesProvider.Instance; }
        }

        public static DistribuidoresXmlProvider DistribuidoresXml
        {
            get { return DistribuidoresXmlProvider.Instance; }
        }

        public static ReporteCortesFacProvider ReporteCortesFac
        {
            get { return ReporteCortesFacProvider.Instance; }
        }

        public static ReporteFacFisProvider ReporteFacFis
        {
            get { return ReporteFacFisProvider.Instance; }
        }

        public static ReportesCortesPlazaProvider ReportesCortesPlaza
        {
            get { return ReportesCortesPlazaProvider.Instance; }
        }

        public static SucursalesEspecialesProvider SucursalesEspeciales
        {
            get { return SucursalesEspecialesProvider.Instance; }
        }


        public static ReporteCortesEspecialesProvider ReporteCortesEspeciales
        {
            get { return ReporteCortesEspecialesProvider.Instance; }
        }

        public static RepFacFisEspecialesProvider RepFacFisEspeciales
        {
            get { return RepFacFisEspecialesProvider.Instance; }
        }


        public static MuestraPlazasProcesosProvider MuestraPlazasProcesos
        {
            get { return MuestraPlazasProcesosProvider.Instance; }
        }

        public static MuestraCajerosProcesosProvider MuestraCajerosProcesos
        {
            get { return MuestraCajerosProcesosProvider.Instance; }
        }

        public static EntregaParcialProvider EntregaParcial
        {
            get { return EntregaParcialProvider.Instance; }
        }

        public static BuscaParcialesProvider BuscaParciales
        {
            get { return BuscaParcialesProvider.Instance; }
        }

        public static uspChecaSiTieneDesgloseProvider uspChecaSiTieneDesglose
        {
            get { return uspChecaSiTieneDesgloseProvider.Instance; }
        }

        public static DesgloseDeMonedaProvider DesgloseDeMoneda
        {
            get { return DesgloseDeMonedaProvider.Instance; }
        }


        public static BuscaDesgloseDeMonedaProvider BuscaDesgloseDeMoneda
        {
            get { return BuscaDesgloseDeMonedaProvider.Instance; }
        }

        public static T1RepArqueoProvider T1RepArqueo
        {
            get { return T1RepArqueoProvider.Instance; }
        }


        public static ValidacionLoginCajeraProvider ValidacionLoginCajera
        {
            get { return ValidacionLoginCajeraProvider.Instance; }
        }


        public static uspHaz_PreguntaProvider uspHaz_Pregunta
        {
            get { return uspHaz_PreguntaProvider.Instance; }
        }

        public static ChecaOrdenRetiroProvider ChecaOrdenRetiro
        {
            get { return ChecaOrdenRetiroProvider.Instance; }
        }

        public static ConRelClienteObsProvider ConRelClienteObs
        {
            get { return ConRelClienteObsProvider.Instance; }
        }


        public static SessionWebDosProvider SessionWebDos
        {
            get { return SessionWebDosProvider.Instance; }
        }


        public static Muestra_Compania_RelUsuarioProvider Muestra_Compania_RelUsuario
        {
            get { return Muestra_Compania_RelUsuarioProvider.Instance; }
        }

        public static MuestraTipSerPrincipal2Provider MuestraTipSerPrincipal2
        {
            get { return MuestraTipSerPrincipal2Provider.Instance; }
        }

        public static MUESTRAUSUARIOSProvider MUESTRAUSUARIOS
        {
            get { return MUESTRAUSUARIOSProvider.Instance; }
        }

        public static uspConsultaColoniasProvider uspConsultaColonias
        {
            get { return uspConsultaColoniasProvider.Instance; }
        }

        public static uspBuscaLLamadasDeInternetProvider uspBuscaLLamadasDeInternet
        {
            get { return uspBuscaLLamadasDeInternetProvider.Instance; }
        }
        public static uspConsultaColoniasPorUsuarioProvider uspConsultaColoniasPorUsuario
        {
            get { return uspConsultaColoniasPorUsuarioProvider.Instance; }
        }

        public static uspBuscaContratoSeparado2Provider uspBuscaContratoSeparado2
        {
            get { return uspBuscaContratoSeparado2Provider.Instance; }
        }

        public static uspConsultaTblClasificacionProblemasProvider uspConsultaTblClasificacionProblemas
        {
            get { return uspConsultaTblClasificacionProblemasProvider.Instance; }
        }

        public static MUESTRATRABAJOSQUEJASProvider MUESTRATRABAJOSQUEJAS
        {
            get { return MUESTRATRABAJOSQUEJASProvider.Instance; }
        }

        public static uspContratoServProvider uspContratoServ
        {
            get { return uspContratoServProvider.Instance; }
        }

        public static MUESTRACLASIFICACIONQUEJASProvider MUESTRACLASIFICACIONQUEJAS
        {
            get { return MUESTRACLASIFICACIONQUEJASProvider.Instance; }
        }

        public static Softv_GetPrioridadQuejaProvider Softv_GetPrioridadQueja
        {
            get { return Softv_GetPrioridadQuejaProvider.Instance; }
        }

        public static VALIDAOrdenQuejaProvider VALIDAOrdenQueja
        {
            get { return VALIDAOrdenQuejaProvider.Instance; }
        }

        public static ObtenerUsarioPorClaveProvider ObtenerUsarioPorClave
        {
            get { return ObtenerUsarioPorClaveProvider.Instance; }
        }

        public static Muestra_Tecnicos_AlmacenProvider Muestra_Tecnicos_Almacen
        {
            get { return Muestra_Tecnicos_AlmacenProvider.Instance; }
        }

        public static spConsultaTurnosProvider spConsultaTurnos
        {
            get { return spConsultaTurnosProvider.Instance; }
        }

        public static LLamadasdeInternetProvider LLamadasdeInternet
        {
            get { return LLamadasdeInternetProvider.Instance; }
        }

        public static QuejasProvider Quejas
        {
            get { return QuejasProvider.Instance; }
        }

        public static Actualizar_quejasCallCenterProvider Actualizar_quejasCallCenter
        {
            get { return Actualizar_quejasCallCenterProvider.Instance; }
        }

        public static BotonEscalarQuejasProvider BotonEscalarQuejas
        {
            get { return BotonEscalarQuejasProvider.Instance; }
        }


        public static DameBonificacionProvider DameBonificacion
        {
            get { return DameBonificacionProvider.Instance; }
        }

        public static sp_dameContratoCompaniaAdicProvider sp_dameContratoCompaniaAdic
        {
            get { return sp_dameContratoCompaniaAdicProvider.Instance; }
        }

        public static MUESTRATIPOFACTURAProvider MUESTRATIPOFACTURA
        {
            get { return MUESTRATIPOFACTURAProvider.Instance; }
        }

        public static BUSCAFACTURASProvider BUSCAFACTURAS
        {
            get { return BUSCAFACTURASProvider.Instance; }
        }

        public static ValidaCancelacionFacturaProvider ValidaCancelacionFactura
        {
            get { return ValidaCancelacionFacturaProvider.Instance; }
        }

        public static MUESTRAMOTIVOSProvider MUESTRAMOTIVOS
        {
            get { return MUESTRAMOTIVOSProvider.Instance; }
        }

        public static GuardaMotivosProvider GuardaMotivos
        {
            get { return GuardaMotivosProvider.Instance; }
        }

        public static ObtieneSucursalesEspeciales_ReimpresionProvider ObtieneSucursalesEspeciales_Reimpresion
        {
            get { return ObtieneSucursalesEspeciales_ReimpresionProvider.Instance; }
        }

        public static MUESTRATIPOFACTURA_ReimpresionProvider MUESTRATIPOFACTURA_Reimpresion
        {
            get { return MUESTRATIPOFACTURA_ReimpresionProvider.Instance; }
        }

        public static BuscaFacturasEspecialesProvider BuscaFacturasEspeciales
        {
            get { return BuscaFacturasEspecialesProvider.Instance; }
        }

        public static ValidaFacturaFiscalProvider ValidaFacturaFiscal
        {
            get { return ValidaFacturaFiscalProvider.Instance; }
        }

        public static CANCELACIONFACTURASProvider CANCELACIONFACTURAS
        {
            get { return CANCELACIONFACTURASProvider.Instance; }
        }

        public static companiaProvider compania
        {
            get { return companiaProvider.Instance; }
        }

        public static EstadoProvider Estado
        {
            get { return EstadoProvider.Instance; }
        }

        public static PeriodosProvider Periodos
        {
            get { return PeriodosProvider.Instance; }
        }

        public static PlazaRepVariosProvider PlazaRepVarios
        {
            get { return PlazaRepVariosProvider.Instance; }
        }

        public static ServiciosDigitalProvider ServiciosDigital
        {
            get { return ServiciosDigitalProvider.Instance; }
        }

        public static ServiciosInternetProvider ServiciosInternet
        {
            get { return ServiciosInternetProvider.Instance; }
        }

        public static TipServProvider TipServ
        {
            get { return TipServProvider.Instance; }
        }

        public static TiposClienteProvider TiposCliente
        {
            get { return TiposClienteProvider.Instance; }
        }

        public static uspBuscaOrdSer_BuscaOrdSerSeparado2Provider uspBuscaOrdSer_BuscaOrdSerSeparado2
        {
            get { return uspBuscaOrdSer_BuscaOrdSerSeparado2Provider.Instance; }
        }

        public static uspChecaCuantasOrdenesQuejasProvider uspChecaCuantasOrdenesQuejas
        {
            get { return uspChecaCuantasOrdenesQuejasProvider.Instance; }
        }

        public static MUESTRATRABAJOSPorTipoUsuarioProvider MUESTRATRABAJOSPorTipoUsuario
        {
            get { return MUESTRATRABAJOSPorTipoUsuarioProvider.Instance; }
        }

        public static Dime_Que_servicio_Tiene_clienteProvider Dime_Que_servicio_Tiene_cliente
        {
            get { return Dime_Que_servicio_Tiene_clienteProvider.Instance; }
        }

        public static Dimesihay_ConexProvider Dimesihay_Conex
        {
            get { return Dimesihay_ConexProvider.Instance; }
        }

        public static BuscaQuejasSeparado2Provider BuscaQuejasSeparado2
        {
            get { return BuscaQuejasSeparado2Provider.Instance; }
        }

        public static uspBorraQuejasOrdenesProvider uspBorraQuejasOrdenes
        {
            get { return uspBorraQuejasOrdenesProvider.Instance; }
        }

        public static ValidaQuejaCompaniaAdicProvider ValidaQuejaCompaniaAdic
        {
            get { return ValidaQuejaCompaniaAdicProvider.Instance; }
        }

        public static BuscaBloqueadoProvider BuscaBloqueado
        {
            get { return BuscaBloqueadoProvider.Instance; }
        }


        public static OrdSerProvider OrdSer
        {
            get { return OrdSerProvider.Instance; }
        }

        public static DetOrdSerProvider DetOrdSer
        {
            get { return DetOrdSerProvider.Instance; }
        }

        public static CAMDOProvider CAMDO
        {
            get { return CAMDOProvider.Instance; }
        }

        public static IPAQUProvider IPAQU
        {
            get { return IPAQUProvider.Instance; }
        }

        public static MuestraGuaBorProvider MuestraGuaBor
        {
            get { return MuestraGuaBorProvider.Instance; }
        }

        public static TblFacturasOpcionesProvider TblFacturasOpciones
        {
            get { return TblFacturasOpcionesProvider.Instance; }
        }

        public static ContratoMaestroFacProvider ContratoMaestroFac
        {
            get { return ContratoMaestroFacProvider.Instance; }
        }

        public static TiposCortesClientesProvider TiposCortesClientes
        {
            get { return TiposCortesClientesProvider.Instance; }
        }

        public static TipoPagosFacturasProvider TipoPagosFacturas
        {
            get { return TipoPagosFacturasProvider.Instance; }
        }

        public static DomicilioFiscalProvider DomicilioFiscal
        {
            get { return DomicilioFiscalProvider.Instance; }
        }

        public static ValidaSiContratoExiste_CMProvider ValidaSiContratoExiste_CM
        {
            get { return ValidaSiContratoExiste_CMProvider.Instance; }
        }


        public static RelContratoMaestro_ContratoSoftvProvider RelContratoMaestro_ContratoSoftv
        {
            get { return RelContratoMaestro_ContratoSoftvProvider.Instance; }
        }



        public static tieneEdoCuentaProvider tieneEdoCuenta
        {
            get { return tieneEdoCuentaProvider.Instance; }
        }

        public static CrearNotaCreditoProvider CrearNotaCredito
        {
            get { return CrearNotaCreditoProvider.Instance; }
        }

        public static ConceptosTicketNotasCreditoProvider ConceptosTicketNotasCredito
        {
            get { return ConceptosTicketNotasCreditoProvider.Instance; }
        }


        public static sp_dameInfodelCobroProvider sp_dameInfodelCobro
        {
            get { return sp_dameInfodelCobroProvider.Instance; }
        }

        public static DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider DamelasOrdenesque_GeneroFacturaAgendaOrdser
        {
            get { return DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider.Instance; }
        }

        public static Genera_Cita_OrdserFacProvider Genera_Cita_OrdserFac
        {
            get { return Genera_Cita_OrdserFacProvider.Instance; }
        }


        public static ValidaSaldoContratoProvider ValidaSaldoContrato
        {
            get { return ValidaSaldoContratoProvider.Instance; }
        }

        public static CobraSaldoProvider CobraSaldo
        {
            get { return CobraSaldoProvider.Instance; }
        }

        public static ObtieneEdoCuentaSinSaldarProvider ObtieneEdoCuentaSinSaldar
        {
            get { return ObtieneEdoCuentaSinSaldarProvider.Instance; }
        }

        public static ValidaHistorialContratoProvider ValidaHistorialContrato
        {
            get { return ValidaHistorialContratoProvider.Instance; }
        }

        public static ObtieneEdoCuentaPorContratoProvider ObtieneEdoCuentaPorContrato
        {
            get { return ObtieneEdoCuentaPorContratoProvider.Instance; }
        }

        public static DameLineasCapturaProvider DameLineasCaptura
        {
            get { return DameLineasCapturaProvider.Instance; }
        }

        public static ReenviaEstadosCuentaPorContratoProvider ReenviaEstadosCuentaPorContrato
        {
            get { return ReenviaEstadosCuentaPorContratoProvider.Instance; }
        }

        public static ObtieneInformacionEnvioCorreoProvider ObtieneInformacionEnvioCorreo
        {
            get { return ObtieneInformacionEnvioCorreoProvider.Instance; }
        }

        public static ValidaReprocesoPorContratoProvider ValidaReprocesoPorContrato
        {
            get { return ValidaReprocesoPorContratoProvider.Instance; }
        }

        public static ReprocesaEdoCuentaContratoProvider ReprocesaEdoCuentaContrato
        {
            get { return ReprocesaEdoCuentaContratoProvider.Instance; }
        }

        public static ReporteEstadoCuentaNuevoProvider ReporteEstadoCuentaNuevo
        {
            get { return ReporteEstadoCuentaNuevoProvider.Instance; }
        }

        public static GuardaMotivoCanServProvider GuardaMotivoCanServ
        {
            get { return GuardaMotivoCanServProvider.Instance; }
        }

        public static BUSCLIPORCONTRATO_OrdSerProvider BUSCLIPORCONTRATO_OrdSer
        {
            get { return BUSCLIPORCONTRATO_OrdSerProvider.Instance; }
        }

        public static BUSCADetOrdSerProvider BUSCADetOrdSer
        {
            get { return BUSCADetOrdSerProvider.Instance; }
        }

        public static MUESTRAAPARATOS_DISCPONIBLESProvider MUESTRAAPARATOS_DISCPONIBLES
        {
            get { return MUESTRAAPARATOS_DISCPONIBLESProvider.Instance; }
        }

        public static SP_GuardaIAPARATOSProvider SP_GuardaIAPARATOS
        {
            get { return SP_GuardaIAPARATOSProvider.Instance; }
        }

        public static ActualizaFacturaMaestroProvider ActualizaFacturaMaestro
        {
            get { return ActualizaFacturaMaestroProvider.Instance; }
        }

        public static GrabaFacturaCMaestroProvider GrabaFacturaCMaestro
        {
            get { return GrabaFacturaCMaestroProvider.Instance; }
        }

        public static CobraSaldoContratoMaestroProvider CobraSaldoContratoMaestro
        {
            get { return CobraSaldoContratoMaestroProvider.Instance; }
        }

        public static DimeSiGrabaOrdProvider DimeSiGrabaOrd
        {
            get { return DimeSiGrabaOrdProvider.Instance; }
        }

        public static Valida_DetOrdenProvider Valida_DetOrden
        {
            get { return Valida_DetOrdenProvider.Instance; }
        }

        public static Checa_si_tiene_camdoProvider Checa_si_tiene_camdo
        {
            get { return Checa_si_tiene_camdoProvider.Instance; }
        }

        public static Cambia_Tipo_cablemodemProvider Cambia_Tipo_cablemodem
        {
            get { return Cambia_Tipo_cablemodemProvider.Instance; }
        }

        public static ChecaMotivoCanServProvider ChecaMotivoCanServ
        {
            get { return ChecaMotivoCanServProvider.Instance; }
        }

        public static VALIDADECODERSProvider VALIDADECODERS
        {
            get { return VALIDADECODERSProvider.Instance; }
        }


        public static NueRelOrdenUsuarioProvider NueRelOrdenUsuario
        {
            get { return NueRelOrdenUsuarioProvider.Instance; }
        }

        public static DimeSiYaGrabeUnaFacMaestroProvider DimeSiYaGrabeUnaFacMaestro
        {
            get { return DimeSiYaGrabeUnaFacMaestroProvider.Instance; }
        }

        public static GuardaPagoFacturaMaestroProvider GuardaPagoFacturaMaestro
        {
            get { return GuardaPagoFacturaMaestroProvider.Instance; }
        }

        public static MuestraFacturasMaestroProvider MuestraFacturasMaestro
        {
            get { return MuestraFacturasMaestroProvider.Instance; }
        }

        public static BuscaFacturasMaestroProvider BuscaFacturasMaestro
        {
            get { return BuscaFacturasMaestroProvider.Instance; }
        }

        public static ConsultaContratoNetCAdeudoProvider ConsultaContratoNetCAdeudo
        {
            get { return ConsultaContratoNetCAdeudoProvider.Instance; }
        }

        public static NUEPago_En_EfectivoDetPagoProvider NUEPago_En_EfectivoDetPago
        {
            get { return NUEPago_En_EfectivoDetPagoProvider.Instance; }
        }

        public static NUEPago_En_EfectivoDetMaestroProvider NUEPago_En_EfectivoDetMaestro
        {
            get { return NUEPago_En_EfectivoDetMaestroProvider.Instance; }
        }

        public static SP_GuardaOrdSerAparatosProvider SP_GuardaOrdSerAparatos
        {
            get { return SP_GuardaOrdSerAparatosProvider.Instance; }
        }

        public static MODORDSERProvider MODORDSER
        {
            get { return MODORDSERProvider.Instance; }
        }

        public static ChecaOrdSerRetiroProvider ChecaOrdSerRetiro
        {
            get { return ChecaOrdSerRetiroProvider.Instance; }
        }

        public static SP_LLena_Bitacora_OrdenesProvider SP_LLena_Bitacora_Ordenes
        {
            get { return SP_LLena_Bitacora_OrdenesProvider.Instance; }
        }

        public static Imprime_OrdenProvider Imprime_Orden
        {
            get { return Imprime_OrdenProvider.Instance; }
        }

        public static ConsultaOrdSerProvider ConsultaOrdSer
        {
            get { return ConsultaOrdSerProvider.Instance; }
        }

        public static GuardaMovSistProvider GuardaMovSist
        {
            get { return GuardaMovSistProvider.Instance; }
        }

        public static MovSistProvider MovSist
        {
            get { return MovSistProvider.Instance; }
        }

        public static ObtieneMediosPagoProvider ObtieneMediosPago
        {
            get { return ObtieneMediosPagoProvider.Instance; }
        }

        public static sp_validaEliminarOrdenProvider sp_validaEliminarOrden
        {
            get { return sp_validaEliminarOrdenProvider.Instance; }
        }

        public static DameDetalle_FacturaMaestroProvider DameDetalle_FacturaMaestro
        {
            get { return DameDetalle_FacturaMaestroProvider.Instance; }
        }

        public static ObtieneHistorialPagosFacturaMaestroProvider ObtieneHistorialPagosFacturaMaestro
        {
            get { return ObtieneHistorialPagosFacturaMaestroProvider.Instance; }
        }

        public static BuscaFacturasMaestroConsultaProvider BuscaFacturasMaestroConsulta
        {
            get { return BuscaFacturasMaestroConsultaProvider.Instance; }
        }

        public static CobraContratoMaestroProvider CobraContratoMaestro
        {
            get { return CobraContratoMaestroProvider.Instance; }
        }

        public static DetalleContratosMaestrosProvider DetalleContratosMaestros
        {
            get { return DetalleContratosMaestrosProvider.Instance; }
        }

        public static NotasDeCredito_ContraMaeFacProvider NotasDeCredito_ContraMaeFac
        {
            get { return NotasDeCredito_ContraMaeFacProvider.Instance; }
        }

        public static StatusNotadeCreditoProvider StatusNotadeCredito
        {
            get { return StatusNotadeCreditoProvider.Instance; }
        }

        public static DAME_FACTURASDECLIENTEProvider DAME_FACTURASDECLIENTE
        {
            get { return DAME_FACTURASDECLIENTEProvider.Instance; }
        }

        public static Detalle_NotasdeCreditoProvider Detalle_NotasdeCredito
        {
            get { return Detalle_NotasdeCreditoProvider.Instance; }
        }

        public static Calcula_montoProvider Calcula_monto
        {
            get { return Calcula_montoProvider.Instance; }
        }

        public static GeneraRefBanamexMaestroProvider GeneraRefBanamexMaestro
        {
            get { return GeneraRefBanamexMaestroProvider.Instance; }
        }

        public static TblNotasMaestraOpcionesProvider TblNotasMaestraOpciones
        {
            get { return TblNotasMaestraOpcionesProvider.Instance; }
        }

        public static Sp_DameDetalleFacturaMaestraProvider Sp_DameDetalleFacturaMaestra
        {
            get { return Sp_DameDetalleFacturaMaestraProvider.Instance; }
        }

        public static MuestraFacturasMaestroConsultaProvider MuestraFacturasMaestroConsulta
        {
            get { return MuestraFacturasMaestroConsultaProvider.Instance; }
        }

        public static DetalleContratosFMProvider DetalleContratosFM
        {
            get { return DetalleContratosFMProvider.Instance; }
        }

        public static DameDetalle_FacturaporCliProvider DameDetalle_FacturaporCli
        {
            get { return DameDetalle_FacturaporCliProvider.Instance; }
        }

        public static AgregaDetalleNotaDeCreditoMaestroProvider AgregaDetalleNotaDeCreditoMaestro
        {
            get { return AgregaDetalleNotaDeCreditoMaestroProvider.Instance; }
        }

        public static BuscaFacturasMaestroPendientesProvider BuscaFacturasMaestroPendientes
        {
            get { return BuscaFacturasMaestroPendientesProvider.Instance; }
        }

        public static MuestraRelOrdenesTecnicosProvider MuestraRelOrdenesTecnicos
        {
            get { return MuestraRelOrdenesTecnicosProvider.Instance; }
        }

        public static ValidaOrdSerManualesProvider ValidaOrdSerManuales
        {
            get { return ValidaOrdSerManualesProvider.Instance; }
        }

        public static InsertMotCanServProvider InsertMotCanServ
        {
            get { return InsertMotCanServProvider.Instance; }
        }






        public static EncuestasProvider Encuestas
        {
            get { return EncuestasProvider.Instance; }
        }

        public static PreguntasProvider Preguntas
        {
            get { return PreguntasProvider.Instance; }
        }

        public static ResOpcMultsProvider ResOpcMults
        {
            get { return ResOpcMultsProvider.Instance; }
        }

        public static TipoPreguntasProvider TipoPreguntas
        {
            get { return TipoPreguntasProvider.Instance; }
        }

        public static ProcesosEncuestasProvider ProcesosEncuestas
        {
            get { return ProcesosEncuestasProvider.Instance; }
        }

        public static UniversoEncuestaProvider UniversoEncuesta
        {
            get { return UniversoEncuestaProvider.Instance; }
        }

        public static LlamadasProvider Llamadas
        {
            get { return LlamadasProvider.Instance; }
        }

        public static ClientesNoContestoProvider ClientesNoContesto
        {
            get { return ClientesNoContestoProvider.Instance; }
        }

        public static RelEncuestaClientesProvider RelEncuestaClientes
        {
            get { return RelEncuestaClientesProvider.Instance; }
        }

        public static RelPreguntaOpcMultsProvider RelPreguntaOpcMults
        {
            get { return RelPreguntaOpcMultsProvider.Instance; }
        }

        public static RelPreguntaEncuestasProvider RelPreguntaEncuestas
        {
            get { return RelPreguntaEncuestasProvider.Instance; }
        }

        public static RelEnProcesosProvider RelEnProcesos
        {
            get { return RelEnProcesosProvider.Instance; }
        }

        public static RelEncuestaPreguntaResProvider RelEncuestaPreguntaRes
        {
            get { return RelEncuestaPreguntaResProvider.Instance; }
        }

        public static Muestra_Detalle_Bitacora_2Provider Muestra_Detalle_Bitacora_2
        {
            get { return Muestra_Detalle_Bitacora_2Provider.Instance; }
        }

        public static Muestra_Descripcion_Articulo_2Provider Muestra_Descripcion_Articulo_2
        {
            get { return Muestra_Descripcion_Articulo_2Provider.Instance; }
        }

        public static Softv_DimeSiTieneBitacoraProvider Softv_DimeSiTieneBitacora
        {
            get { return Softv_DimeSiTieneBitacoraProvider.Instance; }
        }

        public static Softv_GetDescargaMaterialEstaPagadoProvider Softv_GetDescargaMaterialEstaPagado
        {
            get { return Softv_GetDescargaMaterialEstaPagadoProvider.Instance; }
        }

        public static Softv_ExistenciasTecnicoProvider Softv_ExistenciasTecnico
        {
            get { return Softv_ExistenciasTecnicoProvider.Instance; }
        }

        public static TblDescargaMaterialProvider TblDescargaMaterial
        {
            get { return TblDescargaMaterialProvider.Instance; }
        }


        public static GetDescargaMaterialArticulosByIdClvOrdenProvider GetDescargaMaterialArticulosByIdClvOrden
        {
            get { return GetDescargaMaterialArticulosByIdClvOrdenProvider.Instance; }
        }

        public static MuestraPreguntas_EncuestasProvider MuestraPreguntas_Encuestas
        {
            get { return MuestraPreguntas_EncuestasProvider.Instance; }
        }

        public static MuestraRespuestas_EncuestasProvider MuestraRespuestas_Encuestas
        {
            get { return MuestraRespuestas_EncuestasProvider.Instance; }
        }

        public static Muestra_DistribuidoresEncProvider Muestra_DistribuidoresEnc
        {
            get { return Muestra_DistribuidoresEncProvider.Instance; }
        }

        public static Muestra_PlazaEncProvider Muestra_PlazaEnc
        {
            get { return Muestra_PlazaEncProvider.Instance; }
        }

        public static Get_UniversoEncuestaProvider Get_UniversoEncuesta
        {
            get { return Get_UniversoEncuestaProvider.Instance; }
        }


        public static GraficasPreguntasProvider GraficasPreguntas
        {
            get { return GraficasPreguntasProvider.Instance; }
        }

        public static ConAtenTelCteProvider ConAtenTelCte
        {
            get { return ConAtenTelCteProvider.Instance; }
        }

        public static SP_InsertaTbl_NoEntregadosProvider SP_InsertaTbl_NoEntregados
        {
            get { return SP_InsertaTbl_NoEntregadosProvider.Instance; }
        }


        public static TipoDeCambioProvider TipoDeCambio
        {
            get { return TipoDeCambioProvider.Instance; }
        }

        public static ContratosPorSaldarMaestroProvider ContratosPorSaldarMaestro
        {
            get { return ContratosPorSaldarMaestroProvider.Instance; }
        }

        public static ContratosSaldadosMaestroProvider ContratosSaldadosMaestro
        {
            get { return ContratosSaldadosMaestroProvider.Instance; }
        }

        public static ResumenFacturasDolaresProvider ResumenFacturasDolares
        {
            get { return ResumenFacturasDolaresProvider.Instance; }
        }

        public static ValidarNuevoProvider ValidarNuevo
        {
            get { return ValidarNuevoProvider.Instance; }
        }

        public static BuscaSiTieneQuejaProvider BuscaSiTieneQueja
        {
            get { return BuscaSiTieneQuejaProvider.Instance; }
        }

        public static ComisionesTecnicosWebProvider ComisionesTecnicosWeb
        {
            get { return ComisionesTecnicosWebProvider.Instance; }
        }

        public static ComisionesVendedoresWebProvider ComisionesVendedoresWeb
        {
            get { return ComisionesVendedoresWebProvider.Instance; }
        }

        public static sp_verificaCoordenadasProvider sp_verificaCoordenadas
        {
            get { return sp_verificaCoordenadasProvider.Instance; }
        }

        public static sp_verificaBimProveedorProvider sp_verificaBimProveedor
        {
            get { return sp_verificaBimProveedorProvider.Instance; }
        }

        public static NueRelClienteTabProvider NueRelClienteTab
        {
            get { return NueRelClienteTabProvider.Instance; }
        }

        public static ServiciosWebProvider ServiciosWeb
        {
            get { return ServiciosWebProvider.Instance; }
        }

        public static ValidaTrabajosProvider ValidaTrabajos
        {
            get { return ValidaTrabajosProvider.Instance; }
        }

        public static ValidaInstalacionCorrectaProvider ValidaInstalacionCorrecta
        {
            get { return ValidaInstalacionCorrectaProvider.Instance; }
        }

        public static Plaza_ReportesVentasProvider Plaza_ReportesVentas
        {
            get { return Plaza_ReportesVentasProvider.Instance; }
        }

        public static Muestra_PlazasPorUsuarioProvider Muestra_PlazasPorUsuario
        {
            get { return Muestra_PlazasPorUsuarioProvider.Instance; }
        }

        public static Vendores_ReportesVentasProvider Vendores_ReportesVentas
        {
            get { return Vendores_ReportesVentasProvider.Instance; }
        }

        public static Sucursales_ReportesVentasProvider Sucursales_ReportesVentas
        {
            get { return Sucursales_ReportesVentasProvider.Instance; }
        }

        public static Graficas_RepVentasProvider Graficas_RepVentas
        {
            get { return Graficas_RepVentasProvider.Instance; }
        }

        public static RepGralComisiones_RepVentasProvider RepGralComisiones_RepVentas
        {
            get { return RepGralComisiones_RepVentasProvider.Instance; }
        }

        public static RepGral_StatusVentasProvider RepGral_StatusVentas
        {
            get { return RepGral_StatusVentasProvider.Instance; }
        }

        public static CalleProvider Calle
        {
            get { return CalleProvider.Instance; }
        }

        public static ColoniaProvider Colonia
        {
            get { return ColoniaProvider.Instance; }
        }

        public static LocalidadProvider Localidad
        {
            get { return LocalidadProvider.Instance; }
        }

        public static MunicipioProvider Municipio
        {
            get { return MunicipioProvider.Instance; }
        }

        public static RelCalleColoniaProvider RelCalleColonia
        {
            get { return RelCalleColoniaProvider.Instance; }
        }

        public static RelColoniaLocMunEstProvider RelColoniaLocMunEst
        {
            get { return RelColoniaLocMunEstProvider.Instance; }
        }

        public static RelLocalidadMunEstProvider RelLocalidadMunEst
        {
            get { return RelLocalidadMunEstProvider.Instance; }
        }

        public static RelMunicipioEstProvider RelMunicipioEst
        {
            get { return RelMunicipioEstProvider.Instance; }
        }

        public static ClienteProvider Cliente
        {
            get { return ClienteProvider.Instance; }
        }

        public static TipoColoniaProvider TipoColonia
        {
            get { return TipoColoniaProvider.Instance; }
        }

        public static DistribuidorProvider Distribuidor
        {
            get { return DistribuidorProvider.Instance; }
        }

        public static PlazaProvider Plaza
        {
            get { return PlazaProvider.Instance; }
        }

        public static TipoClienteProvider TipoCliente
        {
            get { return TipoClienteProvider.Instance; }
        }

        public static RelColoniaServicioProvider RelColoniaServicio
        {
            get { return RelColoniaServicioProvider.Instance; }
        }

        public static ReferenciaClienteProvider ReferenciaCliente
        {
            get { return ReferenciaClienteProvider.Instance; }
        }

        public static RoboDeSenalProvider RoboDeSenal
        {
            get { return RoboDeSenalProvider.Instance; }
        }

        public static ObservacionClienteProvider ObservacionCliente
        {
            get { return ObservacionClienteProvider.Instance; }
        }

        public static DatoBancarioProvider DatoBancario
        {
            get { return DatoBancarioProvider.Instance; }
        }

        public static DatoFiscalProvider DatoFiscal
        {
            get { return DatoFiscalProvider.Instance; }
        }
        public static TipoTarjetaProvider TipoTarjeta
        {
            get { return TipoTarjetaProvider.Instance; }
        }

        public static RelTarifadosServiciosProvider RelTarifadosServicios
        {
            get { return RelTarifadosServiciosProvider.Instance; }
        }

        public static RelPlazaEstMunProvider RelPlazaEstMun
        {
            get { return RelPlazaEstMunProvider.Instance; }
        }

        public static PeriodoCobroProvider PeriodoCobro
        {
            get { return PeriodoCobroProvider.Instance; }
        }

        public static BancoProvider Banco
        {
            get { return BancoProvider.Instance; }
        }


        public static ListadoNotasProvider ListadoNotas
        {
            get { return ListadoNotasProvider.Instance; }
        }


        public static MuestraMedioPorServicoContratadoProvider MuestraMedioPorServicoContratado
        {
            get { return MuestraMedioPorServicoContratadoProvider.Instance; }
        }

        public static Medio_SoftvProvider Medio_Softv
        {
            get { return Medio_SoftvProvider.Instance; }
        }

        public static RelUnicanetMedioProvider RelUnicanetMedio
        {
            get { return RelUnicanetMedioProvider.Instance; }
        }

        public static MuestraArbolServiciosAparatosPorinstalarProvider MuestraArbolServiciosAparatosPorinstalar
        {
            get { return MuestraArbolServiciosAparatosPorinstalarProvider.Instance; }
        }

        public static MuestraTipoAparatoProvider MuestraTipoAparato
        {
            get { return MuestraTipoAparatoProvider.Instance; }
        }

        public static MuestraServiciosRelTipoAparatoProvider MuestraServiciosRelTipoAparato
        {
            get { return MuestraServiciosRelTipoAparatoProvider.Instance; }
        }

        public static MuestraAparatosDisponiblesProvider MuestraAparatosDisponibles
        {
            get { return MuestraAparatosDisponiblesProvider.Instance; }
        }

        public static MuestraEstadosCompaniaProvider MuestraEstadosCompania
        {
            get { return MuestraEstadosCompaniaProvider.Instance; }
        }

        public static MuestraCiudadesEstadoProvider MuestraCiudadesEstado
        {
            get { return MuestraCiudadesEstadoProvider.Instance; }
        }

        public static MuestraLocalidadCiudadProvider MuestraLocalidadCiudad
        {
            get { return MuestraLocalidadCiudadProvider.Instance; }
        }

        public static MuestraColoniaLocalidadProvider MuestraColoniaLocalidad
        {
            get { return MuestraColoniaLocalidadProvider.Instance; }
        }

        public static MuestraCalleColoniaProvider MuestraCalleColonia
        {
            get { return MuestraCalleColoniaProvider.Instance; }
        }

        public static muestraCP_ColoniaLocalidadProvider muestraCP_ColoniaLocalidad
        {
            get { return muestraCP_ColoniaLocalidadProvider.Instance; }
        }

        public static AsignaAparatosAlServicioProvider AsignaAparatosAlServicio
        {
            get { return AsignaAparatosAlServicioProvider.Instance; }
        }

        public static CatalogoCajasProvider CatalogoCajas
        {
            get { return CatalogoCajasProvider.Instance; }
        }

        public static SUCURSALESProvider SUCURSALES
        {
            get { return SUCURSALESProvider.Instance; }
        }

        public static CLIENTES_NewProvider CLIENTES_New
        {
            get { return CLIENTES_NewProvider.Instance; }
        }

        public static DatosFiscalesProvider DatosFiscales
        {
            get { return DatosFiscalesProvider.Instance; }
        }

        public static RELCLIBANCOProvider RELCLIBANCO
        {
            get { return RELCLIBANCOProvider.Instance; }
        }

        public static MUESTRATIPOSDECUENTAProvider MUESTRATIPOSDECUENTA
        {
            get { return MUESTRATIPOSDECUENTAProvider.Instance; }
        }

        public static MUESTRABANCOS_DatosBancariosProvider MUESTRABANCOS_DatosBancarios
        {
            get { return MUESTRABANCOS_DatosBancariosProvider.Instance; }
        }

        public static tblReferenciasClietesProvider tblReferenciasClietes
        {
            get { return tblReferenciasClietesProvider.Instance; }
        }

        public static RELCLIENTEOBSProvider RELCLIENTEOBS
        {
            get { return RELCLIENTEOBSProvider.Instance; }
        }

        public static RoboDeSeñal_NewProvider RoboDeSeñal_New
        {
            get { return RoboDeSeñal_NewProvider.Instance; }
        }

        public static TipServ_NewProvider TipServ_New
        {
            get { return TipServ_NewProvider.Instance; }
        }

        public static Tipo_Colonias1_NewProvider Tipo_Colonias1_New
        {
            get { return Tipo_Colonias1_NewProvider.Instance; }
        }

        public static Estados_NewProvider Estados_New
        {
            get { return Estados_NewProvider.Instance; }
        }

        public static Ciudades_NewProvider Ciudades_New
        {
            get { return Ciudades_NewProvider.Instance; }
        }

        public static RelEstadoCiudad_NewProvider RelEstadoCiudad_New
        {
            get { return RelEstadoCiudad_NewProvider.Instance; }
        }


        public static ConfiguracionProvider ConfiguracionProvider
        {
            get { return ConfiguracionProvider.Instance; }
        }

        public static ProcesosProvider Procesos
        {
            get { return ProcesosProvider.Instance; }
        }

        public static MuestraTipSerPrincipal_SERProvider MuestraTipSerPrincipal_SER
        {
            get { return MuestraTipSerPrincipal_SERProvider.Instance; }
        }

        public static Servicios_NewProvider Servicios_New
        {
            get { return Servicios_NewProvider.Instance; }
        }

        public static BORRel_Trabajos_NoCobroMensualProvider BORRel_Trabajos_NoCobroMensual
        {
            get { return BORRel_Trabajos_NoCobroMensualProvider.Instance; }
        }

        public static GUARDARel_Trabajos_NoCobroMensualProvider GUARDARel_Trabajos_NoCobroMensual
        {
            get { return GUARDARel_Trabajos_NoCobroMensualProvider.Instance; }
        }

        public static NUEVOClv_EquiProvider NUEVOClv_Equi
        {
            get { return NUEVOClv_EquiProvider.Instance; }
        }

        public static ValidaAplicaSoloInternetProvider ValidaAplicaSoloInternet
        {
            get { return ValidaAplicaSoloInternetProvider.Instance; }
        }

        public static NueAplicaSoloInternetProvider NueAplicaSoloInternet
        {
            get { return NueAplicaSoloInternetProvider.Instance; }
        }

        public static BorAplicaSoloInternetProvider BorAplicaSoloInternet
        {
            get { return BorAplicaSoloInternetProvider.Instance; }
        }

        public static Valida_borra_servicio_NewProvider Valida_borra_servicio_New
        {
            get { return Valida_borra_servicio_NewProvider.Instance; }
        }

        public static ValidaCambioDClvtxtServProvider ValidaCambioDClvtxtServ
        {
            get { return ValidaCambioDClvtxtServProvider.Instance; }
        }


        public static MUESTRASOLOTARIFADOSProvider MUESTRASOLOTARIFADOS
        {
            get { return MUESTRASOLOTARIFADOSProvider.Instance; }
        }

        public static MUESTRATRABAJOS_NewProvider MUESTRATRABAJOS_New
        {
            get { return MUESTRATRABAJOS_NewProvider.Instance; }
        }

        public static MuestraTipoPromocionProvider MuestraTipoPromocion
        {
            get { return MuestraTipoPromocionProvider.Instance; }
        }

        public static ValidaPeriodosProvider ValidaPeriodos
        {
            get { return ValidaPeriodosProvider.Instance; }
        }

        public static REL_TARIFADOS_SERVICIOS_NewProvider REL_TARIFADOS_SERVICIOS_New
        {
            get { return REL_TARIFADOS_SERVICIOS_NewProvider.Instance; }
        }

        public static RelTarifadosServiciosCostoPorCaja_NewProvider RelTarifadosServiciosCostoPorCaja_New
        {
            get { return RelTarifadosServiciosCostoPorCaja_NewProvider.Instance; }
        }

        public static NUEPuntos_Pago_AdelantadoProvider NUEPuntos_Pago_Adelantado
        {
            get { return NUEPuntos_Pago_AdelantadoProvider.Instance; }
        }

        public static ModRentaAparatoProvider ModRentaAparato
        {
            get { return ModRentaAparatoProvider.Instance; }
        }

        public static Actualiza_InstalacionProvider Actualiza_Instalacion
        {
            get { return Actualiza_InstalacionProvider.Instance; }
        }

        public static ValidaEliminaClvLlaveProvider ValidaEliminaClvLlave
        {
            get { return ValidaEliminaClvLlaveProvider.Instance; }
        }

        public static Muestra_Plazas_ConfiguracionServiciosProvider Muestra_Plazas_ConfiguracionServicios
        {
            get { return Muestra_Plazas_ConfiguracionServiciosProvider.Instance; }
        }

        public static DameRelCompaniaEstadoCiudadProvider DameRelCompaniaEstadoCiudad
        {
            get { return DameRelCompaniaEstadoCiudadProvider.Instance; }
        }

        public static DameServiciosRelComEdoCd_PorServicio1_NewProvider DameServiciosRelComEdoCd_PorServicio1_New
        {
            get { return DameServiciosRelComEdoCd_PorServicio1_NewProvider.Instance; }
        }

        public static insertaServiciosRelCompaniaEstadoCiudadProvider insertaServiciosRelCompaniaEstadoCiudad
        {
            get { return insertaServiciosRelCompaniaEstadoCiudadProvider.Instance; }
        }

        public static insertaServiciosRelCompaniaEstadoCiudadATodosProvider insertaServiciosRelCompaniaEstadoCiudadATodos
        {
            get { return insertaServiciosRelCompaniaEstadoCiudadATodosProvider.Instance; }
        }

        public static ValidaNombreLocalidadProvider ValidaNombreLocalidad
        {
            get { return ValidaNombreLocalidadProvider.Instance; }
        }

        public static Localidades_NewProvider Localidades_New
        {
            get { return Localidades_NewProvider.Instance; }
        }

        public static SPRelCiudadLocalidadProvider SPRelCiudadLocalidad
        {
            get { return SPRelCiudadLocalidadProvider.Instance; }
        }

        public static ValidaEliminaRelLocalidadCiudadProvider ValidaEliminaRelLocalidadCiudad
        {
            get { return ValidaEliminaRelLocalidadCiudadProvider.Instance; }
        }

        public static Plaza_DistribuidoresNewProvider Plaza_DistribuidoresNew
        {
            get { return Plaza_DistribuidoresNewProvider.Instance; }
        }

        public static MuestraServiciosDelCli_porOpcionProvider MuestraServiciosDelCli_porOpcion
        {
            get { return MuestraServiciosDelCli_porOpcionProvider.Instance; }
        }

        public static Colonias_NewProvider Colonias_New
        {
            get { return Colonias_NewProvider.Instance; }
        }

        public static ValidaNombreColoniaProvider ValidaNombreColonia
        {
            get { return ValidaNombreColoniaProvider.Instance; }
        }

        public static MuestraEstados_RelColProvider MuestraEstados_RelCol
        {
            get { return MuestraEstados_RelColProvider.Instance; }
        }

        public static MuestraCdsEdo_RelColoniaProvider MuestraCdsEdo_RelColonia
        {
            get { return MuestraCdsEdo_RelColoniaProvider.Instance; }
        }

        public static MuestraLoc_RelColoniaProvider MuestraLoc_RelColonia
        {
            get { return MuestraLoc_RelColoniaProvider.Instance; }
        }

        public static InsertaRelColoniaLocalidadProvider InsertaRelColoniaLocalidad
        {
            get { return InsertaRelColoniaLocalidadProvider.Instance; }
        }

        public static ValidaCVELOCCOLProvider ValidaCVELOCCOL
        {
            get { return ValidaCVELOCCOLProvider.Instance; }
        }

        public static RelColoniasSerProvider RelColoniasSer
        {
            get { return RelColoniasSerProvider.Instance; }
        }

        public static RelColoniaMedioProvider RelColoniaMedio
        {
            get { return RelColoniaMedioProvider.Instance; }
        }

        public static AreaTecnicaProvider AreaTecnica
        {
            get { return AreaTecnicaProvider.Instance; }
        }

        public static ValidaNombreCalleProvider ValidaNombreCalle
        {
            get { return ValidaNombreCalleProvider.Instance; }
        }

        public static Calles_NewProvider Calles_New
        {
            get { return Calles_NewProvider.Instance; }
        }

        public static MuestraLocalidades_CalleProvider MuestraLocalidades_Calle
        {
            get { return MuestraLocalidades_CalleProvider.Instance; }
        }

        public static MuestraColonias_CalleProvider MuestraColonias_Calle
        {
            get { return MuestraColonias_CalleProvider.Instance; }
        }

        public static RelColoniasCalles_NewProvider RelColoniasCalles_New
        {
            get { return RelColoniasCalles_NewProvider.Instance; }
        }

        public static MuestraMedios_NewProvider MuestraMedios_New
        {
            get { return MuestraMedios_NewProvider.Instance; }
        }

        public static ValidaEliminarRelColoniaCalleProvider ValidaEliminarRelColoniaCalle
        {
            get { return ValidaEliminarRelColoniaCalleProvider.Instance; }
        }

        public static CatalogoMotivosProvider CatalogoMotivos
        {
            get { return CatalogoMotivosProvider.Instance; }
        }

        public static CatalogoAgendaProvider CatalogoAgenda
        {
            get { return CatalogoAgendaProvider.Instance; }
        }

        public static MuestraArbolServicios_ClientesProvider MuestraArbolServicios_Clientes
        {
            get { return MuestraArbolServicios_ClientesProvider.Instance; }
        }

        public static ContratacionServicioProvider ContratacionServicio
        {
            get { return ContratacionServicioProvider.Instance; }
        }

        public static ClientesServicioProvider ClientesServicio
        {
            get { return ClientesServicioProvider.Instance; }
        }

        public static ClientesAparatoProvider ClientesAparato
        {
            get { return ClientesAparatoProvider.Instance; }
        }

        public static ModeloAparatoProvider ModeloAparato
        {
            get { return ModeloAparatoProvider.Instance; }
        }

        public static RelTipoServClienteProvider RelTipoServCliente
        {
            get { return RelTipoServClienteProvider.Instance; }
        }

        public static InfoTvsProvider InfoTvs
        {
            get { return InfoTvsProvider.Instance; }
        }

        public static MuestraContratoRealProvider MuestraContratoReal
        {
            get { return MuestraContratoRealProvider.Instance; }
        }

        public static instalaserviciosProvider instalaservicios
        {
            get { return instalaserviciosProvider.Instance; }
        }

        public static tbl_politicasFibraProvider tbl_politicasFibra
        {
            get { return tbl_politicasFibraProvider.Instance; }
        }

        public static DameUnidadesMedidasDeVelocidadProvider DameUnidadesMedidasDeVelocidad
        {
            get { return DameUnidadesMedidasDeVelocidadProvider.Instance; }
        }

        public static TblNetProvider TblNet
        {
            get { return TblNetProvider.Instance; }
        }

        public static Rel_Trabajos_NoCobroMensualProvider Rel_Trabajos_NoCobroMensual
        {
            get { return Rel_Trabajos_NoCobroMensualProvider.Instance; }
        }

        public static Catalogo_IpsProvider Catalogo_Ips
        {
            get { return Catalogo_IpsProvider.Instance; }
        }

        public static catalogoIps_dosProvider catalogoIps_dos
        {
            get { return catalogoIps_dosProvider.Instance; }
        }

        public static RelRedPlazaProvider RelRedPlaza
        {
            get { return RelRedPlazaProvider.Instance; }
        }

        public static General_Sistema_IIProvider General_Sistema_II
        {
            get { return General_Sistema_IIProvider.Instance; }
        }

        public static RelRedCompaniaProvider RelRedCompania
        {
            get { return RelRedCompaniaProvider.Instance; }
        }

        public static RelRedEstadoProvider RelRedEstado
        {
            get { return RelRedEstadoProvider.Instance; }
        }

        public static RelRedCiudadProvider RelRedCiudad
        {
            get { return RelRedCiudadProvider.Instance; }
        }

        public static RelRedLocalidadProvider RelRedLocalidad
        {
            get { return RelRedLocalidadProvider.Instance; }
        }

        public static RelRedMedioProvider RelRedMedio
        {
            get { return RelRedMedioProvider.Instance; }
        }

        public static CatMediosProvider CatMedios
        {
            get { return CatMediosProvider.Instance; }
        }

        public static BUSCAVENDEDORESProvider BUSCAVENDEDORES
        {
            get { return BUSCAVENDEDORESProvider.Instance; }
        }

        public static VendedoresProvider Vendedores
        {
            get { return VendedoresProvider.Instance; }
        }

        public static CatalogoSeriesProvider CatalogoSeries
        {
            get { return CatalogoSeriesProvider.Instance; }
        }

        public static VALIDACatalogoSeriesProvider VALIDACatalogoSeries
        {
            get { return VALIDACatalogoSeriesProvider.Instance; }
        }

        public static Ultimo_SERIEYFOLIOProvider Ultimo_SERIEYFOLIO
        {
            get { return Ultimo_SERIEYFOLIOProvider.Instance; }
        }

        public static Folio_DisponibleProvider Folio_Disponible
        {
            get { return Folio_DisponibleProvider.Instance; }
        }

        public static Cancela_FoliosProvider Cancela_Folios
        {
            get { return Cancela_FoliosProvider.Instance; }
        }

        public static GuardaEvidenciaCancelacionFolioProvider GuardaEvidenciaCancelacionFolio
        {
            get { return GuardaEvidenciaCancelacionFolioProvider.Instance; }
        }

        public static SP_SerieFolioProvider SP_SerieFolio
        {
            get { return SP_SerieFolioProvider.Instance; }
        }

        public static DameTipoSerieProvider DameTipoSerie
        {
            get { return DameTipoSerieProvider.Instance; }
        }

        public static ValidaFoliosImprimirProvider ValidaFoliosImprimir
        {
            get { return ValidaFoliosImprimirProvider.Instance; }
        }

        public static ReimprimirFoliosProvider ReimprimirFolios
        {
            get { return ReimprimirFoliosProvider.Instance; }
        }


        public static PolizaMaestroProvider PolizaMaestro
        {
            get { return PolizaMaestroProvider.Instance; }
        }

        public static RelacionIngresosMaestroProvider RelacionIngresosMaestro
        {
            get { return RelacionIngresosMaestroProvider.Instance; }
        }

        public static BuscaFacturasFiscaProvider BuscaFacturasFisca
        {
            get { return BuscaFacturasFiscaProvider.Instance; }
        }

        public static DameDetalle_FacturaMaestroFiscalProvider DameDetalle_FacturaMaestroFiscal
        {
            get { return DameDetalle_FacturaMaestroFiscalProvider.Instance; }
        }


        public static ActualizaFacturaGeneraFiscalProvider ActualizaFacturaGeneraFiscal
        {
            get { return ActualizaFacturaGeneraFiscalProvider.Instance; }
        }
        public static ReporteFacturasMaestrasVencidasProvider ReporteFacturasMaestrasVencidas
        {
            get { return ReporteFacturasMaestrasVencidasProvider.Instance; }
        }

        public static ReporteServiciosInstaladosProvider ReporteServiciosInstalados
        {
            get { return ReporteServiciosInstaladosProvider.Instance; }
        }

        public static ReporteServiciosPorInstalarProvider ReporteServiciosPorInstalar
        {
            get { return ReporteServiciosPorInstalarProvider.Instance; }
        }


        public static FoliosCanceladosProvider FoliosCancelados
        {
            get { return FoliosCanceladosProvider.Instance; }
        }


        public static EvidenciasDeCanceladosProvider EvidenciasDeCancelados
        {
            get { return EvidenciasDeCanceladosProvider.Instance; }
        }

        public static RangosProvider Rangos
        {
            get { return RangosProvider.Instance; }
        }

        public static ComisionesPorServicioProvider ComisionesPorServicio
        {
            get { return ComisionesPorServicioProvider.Instance; }
        }

        public static GrupoVentasProvider GrupoVentas
        {
            get { return GrupoVentasProvider.Instance; }
        }

        public static DocumentosVendedoresClientesProvider DocumentosVendedoresClientes
        {
            get { return DocumentosVendedoresClientesProvider.Instance; }
        }

        public static ReportesProvider Reportes
        {
            get { return ReportesProvider.Instance; }
        }

        public static RecontratacionProvider Recontratacion
        {
            get { return RecontratacionProvider.Instance; }
        }







        public static GastosProvider Gastos
        {
            get { return GastosProvider.Instance; }
        }

        public static QuejasMasivasProvider QuejasMasivas
        {
            get { return QuejasMasivasProvider.Instance; }
        }


        public static BusquedaQuejasMasivasProvider BusquedaQuejasMasivas
        {
            get { return BusquedaQuejasMasivasProvider.Instance; }
        }


        public static UspChecaSiTieneExtensionesProvider UspChecaSiTieneExtensiones
        {
            get { return UspChecaSiTieneExtensionesProvider.Instance; }
        }

        public static UspLlenaComboExtensionesProvider UspLlenaComboExtensiones
        {
            get { return UspLlenaComboExtensionesProvider.Instance; }
        }

        public static CargoAutomaticoProvider CargoAutomatico
        {
            get { return CargoAutomaticoProvider.Instance; }
        }
  
  

        public static FacturaGloblalProvider FacturaGloblal
        {
            get { return FacturaGloblalProvider.Instance; }
        }




        public static NotaCreditoProvider NotaCreditoProvider
        {
            get { return NotaCreditoProvider.Instance; }
        }

        


    }
}

﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{ 
    /// <summary>
    /// Class                   : Softv.Providers.BitacoraProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Bitacora Provider
    /// File                    : BitacoraProvider.cs
    /// Creation date           : 26/10/2016
    /// Creation time           : 06:30 p. m.
    /// </summary>
    public abstract class BitacoraProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Bitacora from DB
        /// </summary>
        private static BitacoraProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Bitacora instance
        /// </summary>
        public static BitacoraProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Bitacora.Assembly,
                    SoftvSettings.Settings.Bitacora.DataClass);
                    _Instance = (BitacoraProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public BitacoraProvider()
        {
        }



        /// <summary>
        /// Abstract method to add Bitacora
        ///  /summary>
        /// <param name="Bitacora"></param>
        /// <returns></returns>
        public abstract int AddBitacora(BitacoraEntity entity_Bitacora);

        public abstract int AddTickets(BitacoraEntity entity_Bitacora);

        public abstract int AddReenviarEdoCuenta(BitacoraEntity entity_Bitacora);

        public abstract int AddReprocesarEdoCuenta(BitacoraEntity entity_Bitacora);










        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual BitacoraEntity GetBitacoraFromReader(IDataReader reader)
        {
            BitacoraEntity entity_Bitacora = null;
            try
            {
                entity_Bitacora = new BitacoraEntity();
                entity_Bitacora.Usuario = (String)(GetFromReader(reader, "Usuario", IsString: true));
                entity_Bitacora.Contrato = (long?)(GetFromReader(reader, "Contrato"));
                entity_Bitacora.Id = (int?)(GetFromReader(reader, "Id"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Bitacora data to entity", ex);
            }
            return entity_Bitacora;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.BorAplicaSoloInternetProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : BorAplicaSoloInternet Provider
    /// File                    : BorAplicaSoloInternetProvider.cs
    /// Creation date           : 18/09/2017
    /// Creation time           : 12:40 p. m.
    /// </summary>
    public abstract class BorAplicaSoloInternetProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of BorAplicaSoloInternet from DB
        /// </summary>
        private static BorAplicaSoloInternetProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a BorAplicaSoloInternet instance
        /// </summary>
        public static BorAplicaSoloInternetProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.BorAplicaSoloInternet.Assembly,
                    SoftvSettings.Settings.BorAplicaSoloInternet.DataClass);
                    _Instance = (BorAplicaSoloInternetProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public BorAplicaSoloInternetProvider()
        {
        }
       

        /// <summary>
        /// Abstract method to delete BorAplicaSoloInternet
        /// </summary>
        public abstract int DeleteBorAplicaSoloInternet(long? Clv_Servicio);
        

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual BorAplicaSoloInternetEntity GetBorAplicaSoloInternetFromReader(IDataReader reader)
        {
            BorAplicaSoloInternetEntity entity_BorAplicaSoloInternet = null;
            try
            {
                entity_BorAplicaSoloInternet = new BorAplicaSoloInternetEntity();
                entity_BorAplicaSoloInternet.Clv_Servicio = (long?)(GetFromReader(reader, "Clv_Servicio"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting BorAplicaSoloInternet data to entity", ex);
            }
            return entity_BorAplicaSoloInternet;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.BusCliPorContrato_FacProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : BusCliPorContrato_Fac Provider
    /// File                    : BusCliPorContrato_FacProvider.cs
    /// Creation date           : 13/10/2016
    /// Creation time           : 06:52 p. m.
    /// </summary>
    public abstract class BusCliPorContrato_FacProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of BusCliPorContrato_Fac from DB
        /// </summary>
        private static BusCliPorContrato_FacProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a BusCliPorContrato_Fac instance
        /// </summary>
        public static BusCliPorContrato_FacProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.BusCliPorContrato_Fac.Assembly,
                    SoftvSettings.Settings.BusCliPorContrato_Fac.DataClass);
                    _Instance = (BusCliPorContrato_FacProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public BusCliPorContrato_FacProvider()
        {
        }
      
        public abstract List<BusCliPorContrato_FacEntity> GetBusCliPorContrato_Fac(int? Id, String ContratoC);

      
        protected virtual BusCliPorContrato_FacEntity GetBusCliPorContrato_FacFromReader(IDataReader reader)
        {
            BusCliPorContrato_FacEntity entity_BusCliPorContrato_Fac = null;
            try
            {
                entity_BusCliPorContrato_Fac = new BusCliPorContrato_FacEntity();
                //entity_BusCliPorContrato_Fac.Id = (int?)(GetFromReader(reader, "Id"));
                entity_BusCliPorContrato_Fac.Contrato = (long?)(GetFromReader(reader, "Contrato"));
                entity_BusCliPorContrato_Fac.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));
                entity_BusCliPorContrato_Fac.Calle = (String)(GetFromReader(reader, "Calle", IsString: true));
                entity_BusCliPorContrato_Fac.Colonia = (String)(GetFromReader(reader, "Colonia", IsString: true));
                entity_BusCliPorContrato_Fac.Numero = (String)(GetFromReader(reader, "Numero", IsString: true));
                entity_BusCliPorContrato_Fac.Ciudad = (String)(GetFromReader(reader, "Ciudad", IsString: true));
                entity_BusCliPorContrato_Fac.SoloInternet = (bool?)(GetFromReader(reader, "SoloInternet"));
                entity_BusCliPorContrato_Fac.EsHotel = (bool?)(GetFromReader(reader, "EsHotel"));
                entity_BusCliPorContrato_Fac.Telefono = (String)(GetFromReader(reader, "Telefono", IsString: true));
                entity_BusCliPorContrato_Fac.Celular = (String)(GetFromReader(reader, "Celular", IsString: true));
                entity_BusCliPorContrato_Fac.Desglosa_IVA = (bool?)(GetFromReader(reader, "Desglosa_IVA"));
                entity_BusCliPorContrato_Fac.ContratoC = (String)(GetFromReader(reader, "ContratoC", IsString: true));                

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting BusCliPorContrato_Fac data to entity", ex);
            }
            return entity_BusCliPorContrato_Fac;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.BuscaDesgloseDeMonedaProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : BuscaDesgloseDeMoneda Provider
    /// File                    : BuscaDesgloseDeMonedaProvider.cs
    /// Creation date           : 14/12/2016
    /// Creation time           : 06:07 p. m.
    /// </summary>
    public abstract class BuscaDesgloseDeMonedaProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of BuscaDesgloseDeMoneda from DB
        /// </summary>
        private static BuscaDesgloseDeMonedaProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a BuscaDesgloseDeMoneda instance
        /// </summary>
        public static BuscaDesgloseDeMonedaProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.BuscaDesgloseDeMoneda.Assembly,
                    SoftvSettings.Settings.BuscaDesgloseDeMoneda.DataClass);
                    _Instance = (BuscaDesgloseDeMonedaProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public BuscaDesgloseDeMonedaProvider()
        {
        }
       
        /// <summary>
        /// Abstract method to get all BuscaDesgloseDeMoneda
        /// </summary>
        public abstract List<BuscaDesgloseDeMonedaEntity> GetBuscaDesgloseDeMoneda(int? Op, String ClvUsuario, String Fecha);


        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual BuscaDesgloseDeMonedaEntity GetBuscaDesgloseDeMonedaFromReader(IDataReader reader)
        {
            BuscaDesgloseDeMonedaEntity entity_BuscaDesgloseDeMoneda = null;
            try
            {
                entity_BuscaDesgloseDeMoneda = new BuscaDesgloseDeMonedaEntity();
                entity_BuscaDesgloseDeMoneda.Consecutivo = (long?)(GetFromReader(reader, "Consecutivo"));
                entity_BuscaDesgloseDeMoneda.Fecha = (String)(GetFromReader(reader, "Fecha", IsString: true));
                entity_BuscaDesgloseDeMoneda.NomCajera = (String)(GetFromReader(reader, "NomCajera", IsString: true));
                entity_BuscaDesgloseDeMoneda.Total = (Decimal?)(GetFromReader(reader, "Total"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting BuscaDesgloseDeMoneda data to entity", ex);
            }
            return entity_BuscaDesgloseDeMoneda;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals; 

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.CAMDOProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : CAMDO Provider
    /// File                    : CAMDOProvider.cs
    /// Creation date           : 17/02/2017
    /// Creation time           : 01:35 p. m.
    /// </summary>
    public abstract class CAMDOProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of CAMDO from DB
        /// </summary>
        private static CAMDOProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a CAMDO instance
        /// </summary>
        public static CAMDOProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.CAMDO.Assembly,
                    SoftvSettings.Settings.CAMDO.DataClass);
                    _Instance = (CAMDOProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public CAMDOProvider()
        {
        }
        /// <summary>
        /// Abstract method to add CAMDO
        ///  /summary>
        /// <param name="CAMDO"></param>
        /// <returns></returns>
        public abstract int AddCAMDO(CAMDOEntity entity_CAMDO);

        /// <summary>
        /// Abstract method to delete CAMDO
        /// </summary>
        public abstract int DeleteCAMDO();

        /// <summary>
        /// Abstract method to update CAMDO
        /// </summary>
        public abstract int EditCAMDO(CAMDOEntity entity_CAMDO);

        /// <summary>
        /// Abstract method to get all CAMDO
        /// </summary>
        public abstract List<CAMDOEntity> GetCAMDO();

        public abstract List<CAMDOEntity> CiudadCamdo(long? CONTRATO);

        public abstract List<CAMDOEntity> LocalidadCamdo(long? CONTRATO, int? Clv_Ciudad);

        public abstract List<CAMDOEntity> ColoniaCamdo(long? CONTRATO, int? Clv_Localidad);

        public abstract List<CAMDOEntity> CalleCamdo(long? CONTRATO, int? Clv_Colonia);





        /// <summary>
        /// Abstract method to get all CAMDO List<int> lid
        /// </summary>
        public abstract List<CAMDOEntity> GetCAMDO(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract CAMDOEntity GetConCAMDO(long? CLAVE, long? Clv_Orden, long? CONTRATO);

        public abstract CAMDOEntity GetCAMDOById(long? CLAVE, long? Clv_Orden, long? CONTRATO);



        /// <summary>
        ///Get CAMDO
        ///</summary>
        public abstract SoftvList<CAMDOEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get CAMDO
        ///</summary>
        public abstract SoftvList<CAMDOEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual CAMDOEntity GetCAMDOFromReader(IDataReader reader)
        {
            CAMDOEntity entity_CAMDO = null;
            try
            {
                entity_CAMDO = new CAMDOEntity();
                entity_CAMDO.CLAVE = (long?)(GetFromReader(reader, "CLAVE"));
                entity_CAMDO.Clv_Orden = (long?)(GetFromReader(reader, "Clv_Orden"));
                entity_CAMDO.CONTRATO = (long?)(GetFromReader(reader, "CONTRATO"));
                entity_CAMDO.Clv_Calle = (int?)(GetFromReader(reader, "Clv_Calle"));
                entity_CAMDO.NUMERO = (String)(GetFromReader(reader, "NUMERO", IsString: true));
                entity_CAMDO.ENTRECALLES = (String)(GetFromReader(reader, "ENTRECALLES", IsString: true));
                entity_CAMDO.Clv_Colonia = (int?)(GetFromReader(reader, "Clv_Colonia"));
                entity_CAMDO.TELEFONO = (String)(GetFromReader(reader, "TELEFONO", IsString: true));
                try
                {
                    entity_CAMDO.ClvTecnica = (int?)(GetFromReader(reader, "ClvTecnica"));

                }
                catch{ }
               
                entity_CAMDO.Clv_Ciudad = (int?)(GetFromReader(reader, "Clv_Ciudad"));
                try
                {
                    entity_CAMDO.Num_int = (String)(GetFromReader(reader, "Num_int", IsString: true));
                }
                catch (Exception ex) { }
                
                //entity_CAMDO.Clv_Sector = (int?)(GetFromReader(reader, "Clv_Sector"));
                entity_CAMDO.Clv_Localidad = (int?)(GetFromReader(reader, "Clv_Localidad"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting CAMDO data to entity", ex);
            }
            return entity_CAMDO;
        }




        protected virtual CAMDOEntity GetConCAMDOFromReader(IDataReader reader)
        {
            CAMDOEntity entity_CAMDO = null;
            try
            {
                entity_CAMDO = new CAMDOEntity();
                entity_CAMDO.existe = (int?)(GetFromReader(reader, "existe"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting CAMDO data to entity", ex);
            }
            return entity_CAMDO;
        }



        protected virtual CAMDOEntity GetCiudadCamdoFromReader(IDataReader reader)
        {
            CAMDOEntity entity_CAMDO = null;
            try
            {
                entity_CAMDO = new CAMDOEntity();
                entity_CAMDO.Clv_Ciudad = (int?)(GetFromReader(reader, "Clv_Ciudad"));
                entity_CAMDO.nombre = (String)(GetFromReader(reader, "nombre", IsString: true));


            }
            catch (Exception ex)
            {
                throw new Exception("Error converting CAMDO data to entity", ex);
            }
            return entity_CAMDO;
        }


        protected virtual CAMDOEntity GetLocalidadCamdoFromReader(IDataReader reader)
        {
            CAMDOEntity entity_CAMDO = null;
            try
            {
                entity_CAMDO = new CAMDOEntity();
                entity_CAMDO.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));
                entity_CAMDO.Clv_Localidad = (int?)(GetFromReader(reader, "Clv_Localidad"));           

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting CAMDO data to entity", ex);
            }
            return entity_CAMDO;
        }


        protected virtual CAMDOEntity GetColoniaCamdoFromReader(IDataReader reader)
        {
            CAMDOEntity entity_CAMDO = null;
            try
            {
                entity_CAMDO = new CAMDOEntity();
                entity_CAMDO.COLONIA = (String)(GetFromReader(reader, "COLONIA", IsString: true));
                entity_CAMDO.CLV_COLONIA = (int?)(GetFromReader(reader, "CLV_COLONIA"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting CAMDO data to entity", ex);
            }
            return entity_CAMDO;
        }


        protected virtual CAMDOEntity GetCalleCamdoFromReader(IDataReader reader)
        {
            CAMDOEntity entity_CAMDO = null;
            try
            {
                entity_CAMDO = new CAMDOEntity();
                entity_CAMDO.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));
                entity_CAMDO.Clv_Calle = (int?)(GetFromReader(reader, "Clv_Calle"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting CAMDO data to entity", ex);
            }
            return entity_CAMDO;
        }







    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ChecaOrdenRetiroProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ChecaOrdenRetiro Provider
    /// File                    : ChecaOrdenRetiroProvider.cs
    /// Creation date           : 20/01/2017
    /// Creation time           : 12:31 p. m.
    /// </summary>
    public abstract class ChecaOrdenRetiroProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ChecaOrdenRetiro from DB
        /// </summary>
        private static ChecaOrdenRetiroProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ChecaOrdenRetiro instance
        /// </summary>
        public static ChecaOrdenRetiroProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ChecaOrdenRetiro.Assembly,
                    SoftvSettings.Settings.ChecaOrdenRetiro.DataClass);
                    _Instance = (ChecaOrdenRetiroProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ChecaOrdenRetiroProvider()
        {
        }
        
        public abstract List<ChecaOrdenRetiroEntity> GetChecaOrdenRetiro(long? Contrato);

        protected virtual ChecaOrdenRetiroEntity GetChecaOrdenRetiroFromReader(IDataReader reader)
        {
            ChecaOrdenRetiroEntity entity_ChecaOrdenRetiro = null;
            try
            {
                entity_ChecaOrdenRetiro = new ChecaOrdenRetiroEntity();
                entity_ChecaOrdenRetiro.Resultado = (int?)(GetFromReader(reader, "Resultado"));
                entity_ChecaOrdenRetiro.Msg = (String)(GetFromReader(reader, "Msg", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ChecaOrdenRetiro data to entity", ex);
            }
            return entity_ChecaOrdenRetiro;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.Checa_si_tiene_camdoProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Checa_si_tiene_camdo Provider
    /// File                    : Checa_si_tiene_camdoProvider.cs
    /// Creation date           : 25/03/2017
    /// Creation time           : 12:03 p. m.
    /// </summary>
    public abstract class Checa_si_tiene_camdoProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Checa_si_tiene_camdo from DB
        /// </summary>
        private static Checa_si_tiene_camdoProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Checa_si_tiene_camdo instance
        /// </summary>
        public static Checa_si_tiene_camdoProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Checa_si_tiene_camdo.Assembly,
                    SoftvSettings.Settings.Checa_si_tiene_camdo.DataClass);
                    _Instance = (Checa_si_tiene_camdoProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public Checa_si_tiene_camdoProvider()
        {
        }
        
        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract Checa_si_tiene_camdoEntity GetCheca_si_tiene_camdoById(long? ClvOrden);


        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual Checa_si_tiene_camdoEntity GetCheca_si_tiene_camdoFromReader(IDataReader reader)
        {
            Checa_si_tiene_camdoEntity entity_Checa_si_tiene_camdo = null;
            try
            {
                entity_Checa_si_tiene_camdo = new Checa_si_tiene_camdoEntity();
                //entity_Checa_si_tiene_camdo.ClvOrden = (long?)(GetFromReader(reader, "ClvOrden"));
                entity_Checa_si_tiene_camdo.Error = (int?)(GetFromReader(reader, "Error"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Checa_si_tiene_camdo data to entity", ex);
            }
            return entity_Checa_si_tiene_camdo;
        }

    }

    #region Customs Methods

    #endregion
}


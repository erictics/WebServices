﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ChecarServiciosCanceladosProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ChecarServiciosCancelados Provider
    /// File                    : ChecarServiciosCanceladosProvider.cs
    /// Creation date           : 25/10/2016
    /// Creation time           : 10:14 a. m.
    /// </summary> 
    public abstract class ChecarServiciosCanceladosProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ChecarServiciosCancelados from DB
        /// </summary>
        private static ChecarServiciosCanceladosProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ChecarServiciosCancelados instance
        /// </summary>
        public static ChecarServiciosCanceladosProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ChecarServiciosCancelados.Assembly,
                    SoftvSettings.Settings.ChecarServiciosCancelados.DataClass);
                    _Instance = (ChecarServiciosCanceladosProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ChecarServiciosCanceladosProvider()
        {
        }


        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ChecarServiciosCanceladosEntity GetChecarServiciosCanceladosById(long? Contrato);





        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ChecarServiciosCanceladosEntity GetChecarServiciosCanceladosFromReader(IDataReader reader)
        {
            ChecarServiciosCanceladosEntity entity_ChecarServiciosCancelados = null;
            try
            {
                entity_ChecarServiciosCancelados = new ChecarServiciosCanceladosEntity();
                entity_ChecarServiciosCancelados.Valor = (bool)(GetFromReader(reader, "Valor"));


            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ChecarServiciosCancelados data to entity", ex);
            }
            return entity_ChecarServiciosCancelados;
        }

    }

    #region Customs Methods

    #endregion
}


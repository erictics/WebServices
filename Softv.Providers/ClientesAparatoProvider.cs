﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ClientesAparatoProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ClientesAparato Provider
    /// File                    : ClientesAparatoProvider.cs
    /// Creation date           : 06/10/2017
    /// Creation time           : 11:25 a. m.
    /// </summary>
    public abstract class ClientesAparatoProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ClientesAparato from DB
        /// </summary> 
        private static ClientesAparatoProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ClientesAparato instance
        /// </summary>
        public static ClientesAparatoProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ClientesAparato.Assembly,
                    SoftvSettings.Settings.ClientesAparato.DataClass);
                    _Instance = (ClientesAparatoProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }
        
        public ClientesAparatoProvider()
        {
        }
        
        public abstract int AddClientesAparato(ClientesAparatoEntity entity_ClientesAparato);

       
        public abstract int EditClientesAparato(ClientesAparatoEntity entity_ClientesAparato);
        
        public abstract List<ClientesAparatoEntity> GetClientesAparato(long? ContratoNet);

        public abstract List<ArticuloRentaEntity> GetCostoArticuloPagare(int? op, int? tipser, int? id);

        public abstract List<TipServEntity> GetMuestraTipoServicioPagare(long? Opcion);

        public abstract int? GetEliCostosPagare(int? IdCosto);

        public abstract List<Servicios_NewEntity> GetMuestraServiciosRentas(int? Clv_tipser);

        public abstract List<ArticuloRentaAparatosEntity> GetConsultaEquipoMarcado(int? TipoServicio, int? TipoPaquete, long? IdCostoAP);

        public abstract List<ArticuloRentaAparatosEntity> GetMuestraMarcasAparatosPagareParaList(int? TipoServicio, int? TipoPaquete, long? IdCostoAP);

        public abstract List<CostoArticuloPagareVigenciasEntity> GetSP_ConsultaRel_CostoArticuloPagareConVigencia(long? IdCostoAP);

        public abstract string GetSP_InsertaRel_CostoArticuloPagareConVigencias(CostoArticuloPagareVigenciasEntity obj);

        public abstract string GetSP_ModificarRel_CostoArticuloPagareConVigencias(CostoArticuloPagareVigenciasEntity obj);

        public abstract int? SP_EliminarRel_CostoArticuloPagareConVigenciasIndividual(long? IdCostoAP, long? IdRelVigencia);

        public abstract GuardaCostoPagareEntity GetSP_GuardaCostosPagareNew(GuardaCostoPagareEntity obj);

        public abstract string GetSp_ValidaVigencias(CostoArticuloPagareVigenciasEntity obj);

        public abstract int? GetSP_LimpiaInsertaRel_CostoArticuloPagareConAparatos(long? IdCostoAP);

        public abstract int? GetSP_InsertaRel_CostoArticuloPagareConAparatos(int? op, List<ArticuloRentaAparatosEntity> obj);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ClientesAparatoEntity GetClientesAparatoFromReader(IDataReader reader)
        {
            ClientesAparatoEntity entity_ClientesAparato = null;
            try
            {
                entity_ClientesAparato = new ClientesAparatoEntity();
                entity_ClientesAparato.ContratoNet = (long?)(GetFromReader(reader, "ContratoNet"));
                entity_ClientesAparato.Status = (String)(GetFromReader(reader, "Status", IsString: true));
                entity_ClientesAparato.Clv_CableModem = (long?)(GetFromReader(reader, "Clv_CableModem"));
                entity_ClientesAparato.Clv_Usuario = (long?)(GetFromReader(reader, "Clv_Usuario"));
                entity_ClientesAparato.Fecha_Activacion = (String)(GetFromReader(reader, "Fecha_Activacion", IsString: true));
                entity_ClientesAparato.Fecha_Suspension = (String)(GetFromReader(reader, "Fecha_Suspension", IsString: true));
                entity_ClientesAparato.Fecha_Baja = (String)(GetFromReader(reader, "Fecha_Baja", IsString: true));
                entity_ClientesAparato.Fecha_Traspaso = (String)(GetFromReader(reader, "Fecha_Traspaso", IsString: true));
                entity_ClientesAparato.Obs = (String)(GetFromReader(reader, "Obs", IsString: true));
                entity_ClientesAparato.SeRenta = (bool?)(GetFromReader(reader, "SeRenta"));
                entity_ClientesAparato.no_extensiones = (int?)(GetFromReader(reader, "no_extensiones"));
                entity_ClientesAparato.NoCaja = (int?)(GetFromReader(reader, "NoCaja"));
                entity_ClientesAparato.ventacablemodem1 = (bool?)(GetFromReader(reader, "ventacablemodem1"));
                entity_ClientesAparato.ventacablemodem2 = (bool?)(GetFromReader(reader, "ventacablemodem2"));
                entity_ClientesAparato.Tipo_Cablemodem = (int?)(GetFromReader(reader, "Tipo_Cablemodem"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ClientesAparato data to entity", ex);
            }
            return entity_ClientesAparato;
        }

    }

    #region Customs Methods

    #endregion
}


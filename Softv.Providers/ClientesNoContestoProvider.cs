﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ClientesNoContestoProvider 
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ClientesNoContesto Provider
    /// File                    : ClientesNoContestoProvider.cs
    /// Creation date           : 17/06/2017
    /// Creation time           : 11:24 a. m.
    /// </summary>
    public abstract class ClientesNoContestoProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ClientesNoContesto from DB
        /// </summary>
        private static ClientesNoContestoProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ClientesNoContesto instance
        /// </summary>
        public static ClientesNoContestoProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ClientesNoContesto.Assembly,
                    SoftvSettings.Settings.ClientesNoContesto.DataClass);
                    _Instance = (ClientesNoContestoProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ClientesNoContestoProvider()
        {
        }
        /// <summary>
        /// Abstract method to add ClientesNoContesto
        ///  /summary>
        /// <param name="ClientesNoContesto"></param>
        /// <returns></returns>
        public abstract int AddClientesNoContesto(ClientesNoContestoEntity entity_ClientesNoContesto);

        /// <summary>
        /// Abstract method to delete ClientesNoContesto
        /// </summary>
        public abstract int DeleteClientesNoContesto(int? IdNoContesto);

        /// <summary>
        /// Abstract method to update ClientesNoContesto
        /// </summary>
        public abstract int EditClientesNoContesto(ClientesNoContestoEntity entity_ClientesNoContesto);

        /// <summary>
        /// Abstract method to get all ClientesNoContesto
        /// </summary>
        public abstract List<ClientesNoContestoEntity> GetClientesNoContesto();

        /// <summary>
        /// Abstract method to get all ClientesNoContesto List<int> lid
        /// </summary>
        public abstract List<ClientesNoContestoEntity> GetClientesNoContesto(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ClientesNoContestoEntity GetClientesNoContestoById(int? IdNoContesto);



        /// <summary>
        ///Get ClientesNoContesto
        ///</summary>
        public abstract SoftvList<ClientesNoContestoEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get ClientesNoContesto
        ///</summary>
        public abstract SoftvList<ClientesNoContestoEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ClientesNoContestoEntity GetClientesNoContestoFromReader(IDataReader reader)
        {
            ClientesNoContestoEntity entity_ClientesNoContesto = null;
            try
            {
                entity_ClientesNoContesto = new ClientesNoContestoEntity();
                entity_ClientesNoContesto.IdNoContesto = (int?)(GetFromReader(reader, "IdNoContesto"));
                entity_ClientesNoContesto.IdProcesoEnc = (int?)(GetFromReader(reader, "IdProcesoEnc"));
                entity_ClientesNoContesto.IdEncuesta = (int?)(GetFromReader(reader, "IdEncuesta"));
                entity_ClientesNoContesto.Contrato = (long?)(GetFromReader(reader, "Contrato"));
                entity_ClientesNoContesto.FechaApli = (DateTime?)(GetFromReader(reader, "FechaApli"));
                entity_ClientesNoContesto.IdPlaza = (int?)(GetFromReader(reader, "IdPlaza"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ClientesNoContesto data to entity", ex);
            }
            return entity_ClientesNoContesto;
        }

    }

    #region Customs Methods

    #endregion
}


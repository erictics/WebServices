﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ColoniaCAMDOProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ColoniaCAMDO Provider
    /// File                    : ColoniaCAMDOProvider.cs
    /// Creation date           : 25/10/2016
    /// Creation time           : 06:36 p. m.
    /// </summary>
    public abstract class ColoniaCAMDOProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ColoniaCAMDO from DB
        /// </summary>
        private static ColoniaCAMDOProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ColoniaCAMDO instance
        /// </summary>
        public static ColoniaCAMDOProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ColoniaCAMDO.Assembly,
                    SoftvSettings.Settings.ColoniaCAMDO.DataClass);
                    _Instance = (ColoniaCAMDOProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ColoniaCAMDOProvider()
        {
        }


        /// <summary>
        /// Abstract method to get all ColoniaCAMDO
        /// </summary>
        public abstract List<ColoniaCAMDOEntity> GetColoniaCAMDO(long? Contrato, int? localidad);

        public abstract List<PosteEntity> GetMuestraDescPoste(long? op);

        public abstract int AddInsertaNueDescPoste(long? clave, String descripcion);



        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ColoniaCAMDOEntity GetColoniaCAMDOFromReader(IDataReader reader)
        {
            ColoniaCAMDOEntity entity_ColoniaCAMDO = null;
            try
            {
                entity_ColoniaCAMDO = new ColoniaCAMDOEntity();
                entity_ColoniaCAMDO.COLONIA = (String)(GetFromReader(reader, "COLONIA", IsString: true));
                entity_ColoniaCAMDO.CLV_COLONIA = (long?)(GetFromReader(reader, "CLV_COLONIA"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ColoniaCAMDO data to entity", ex);
            }
            return entity_ColoniaCAMDO;
        }

    }

    #region Customs Methods

    #endregion
}


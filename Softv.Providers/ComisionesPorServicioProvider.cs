﻿
    using System;
    using System.Text;
    using System.Data;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Runtime.Remoting;
    using Softv.Entities;
    using SoftvConfiguration;
    using Globals;

    namespace Softv.Providers
    {
    /// <summary>
    /// Class                   : Softv.Providers.ComisionesPorServicioProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ComisionesPorServicio Provider
    /// File                    : ComisionesPorServicioProvider.cs
    /// Creation date           : 12/12/2017
    /// Creation time           : 10:02 a. m.
    /// </summary>
    public abstract class ComisionesPorServicioProvider : Globals.DataAccess
    {

    /// <summary>
    /// Instance of ComisionesPorServicio from DB
    /// </summary>
    private static ComisionesPorServicioProvider _Instance = null;

    private static ObjectHandle obj;
    /// <summary>
    /// Generates a ComisionesPorServicio instance
    /// </summary>
    public static ComisionesPorServicioProvider Instance
    {
    get
    {
    if (_Instance == null)
    {
    obj = Activator.CreateInstance(
    SoftvSettings.Settings.ComisionesPorServicio.Assembly,
    SoftvSettings.Settings.ComisionesPorServicio.DataClass);
    _Instance = (ComisionesPorServicioProvider)obj.Unwrap();
    }
    return _Instance;
    }
    }

    /// <summary>
    /// Provider's default constructor
    /// </summary>
    public ComisionesPorServicioProvider()
    {
    }

    public abstract List<ComisionesPorServicioEntity> GetMuestraTipServ(ComisionesPorServicioEntity ObjTipoServicio);

    public abstract List<ComisionesPorServicioEntity> GetMuestraServicios(ComisionesPorServicioEntity ObjServicio);

    public abstract List<ComisionesPorServicioEntity> GetCONCOMISION(ComisionesPorServicioEntity ObjComision);

    public abstract List<ComisionesPorServicioEntity> GetCONRANGOS(ComisionesPorServicioEntity ObjRango);

    public abstract int? GetNUECOMISION(ComisionesPorServicioEntity ObjComision);

    public abstract int? GetBORCOMISION(ComisionesPorServicioEntity ObjComision);
    
    }

    #region Customs Methods
    
    #endregion
    }

  
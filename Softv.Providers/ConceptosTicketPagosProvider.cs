﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ConceptosTicketPagosProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ConceptosTicketPagos Provider
    /// File                    : ConceptosTicketPagosProvider.cs
    /// Creation date           : 03/11/2016
    /// Creation time           : 11:06 a. m.
    /// </summary>
    public abstract class ConceptosTicketPagosProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ConceptosTicketPagos from DB
        /// </summary>
        private static ConceptosTicketPagosProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ConceptosTicketPagos instance
        /// </summary>
        public static ConceptosTicketPagosProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ConceptosTicketPagos.Assembly,
                    SoftvSettings.Settings.ConceptosTicketPagos.DataClass);
                    _Instance = (ConceptosTicketPagosProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ConceptosTicketPagosProvider()
        {
        }
       
        public abstract List<ConceptosTicketPagosEntity> GetConceptosTicketPagos(long? Clv_Factura);

       
        protected virtual ConceptosTicketPagosEntity GetConceptosTicketPagosFromReader(IDataReader reader)
        {
            ConceptosTicketPagosEntity entity_ConceptosTicketPagos = null;
            try
            {
                entity_ConceptosTicketPagos = new ConceptosTicketPagosEntity();
                entity_ConceptosTicketPagos.Descripcion = (String)(GetFromReader(reader, "Descripcion", IsString: true));
                entity_ConceptosTicketPagos.importe = (Decimal?)(GetFromReader(reader, "importe"));
                entity_ConceptosTicketPagos.importeBonifica = (Decimal?)(GetFromReader(reader, "importeBonifica"));
                entity_ConceptosTicketPagos.bonificacion = (Decimal?)(GetFromReader(reader, "bonificacion"));
                entity_ConceptosTicketPagos.PeriodoPagado = (String)(GetFromReader(reader, "PeriodoPagado", IsString: true));
                entity_ConceptosTicketPagos.PuntosCombo = (Decimal?)(GetFromReader(reader, "PuntosCombo"));
                entity_ConceptosTicketPagos.ImporteDiasProp = (Decimal?)(GetFromReader(reader, "ImporteDiasProp"));
                entity_ConceptosTicketPagos.puntosAplicadosAnt = (Decimal?)(GetFromReader(reader, "puntosAplicadosAnt"));
                entity_ConceptosTicketPagos.PuntosAplicadosPagoAdelantado = (int?)(GetFromReader(reader, "PuntosAplicadosPagoAdelantado"));
                entity_ConceptosTicketPagos.PuntosAplicadosOtros = (int?)(GetFromReader(reader, "PuntosAplicadosOtros"));


            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ConceptosTicketPagos data to entity", ex);
            }
            return entity_ConceptosTicketPagos;
        }

    }

    #region Customs Methods

    #endregion
}


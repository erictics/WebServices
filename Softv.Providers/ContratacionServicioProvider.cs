﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ContratacionServicioProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ContratacionServicio Provider
    /// File                    : ContratacionServicioProvider.cs
    /// Creation date           : 05/10/2017
    /// Creation time           : 09:35 a. m.
    /// </summary>
    public abstract class ContratacionServicioProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ContratacionServicio from DB
        /// </summary>
        private static ContratacionServicioProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ContratacionServicio instance
        /// </summary>
        public static ContratacionServicioProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ContratacionServicio.Assembly,
                    SoftvSettings.Settings.ContratacionServicio.DataClass);
                    _Instance = (ContratacionServicioProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ContratacionServicioProvider()
        {
        }
        /// <summary>
        /// Abstract method to add ContratacionServicio
        ///  /summary>
        /// <param name="ContratacionServicio"></param>
        /// <returns></returns>
        public abstract int AddContratacionServicio(ContratacionServicioEntity entity_ContratacionServicio);

        /// <summary>
        /// Abstract method to delete ContratacionServicio
        /// </summary>
        public abstract int DeleteContratacionServicio();

        /// <summary>
        /// Abstract method to update ContratacionServicio
        /// </summary>
        public abstract int EditContratacionServicio(ContratacionServicioEntity entity_ContratacionServicio);

        /// <summary>
        /// Abstract method to get all ContratacionServicio
        /// </summary>
        public abstract List<ContratacionServicioEntity> GetContratacionServicio();

        /// <summary>
        /// Abstract method to get all ContratacionServicio List<int> lid
        /// </summary>
        public abstract List<ContratacionServicioEntity> GetContratacionServicio(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ContratacionServicioEntity GetContratacionServicioById();



        /// <summary>
        ///Get ContratacionServicio
        ///</summary>
        public abstract SoftvList<ContratacionServicioEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get ContratacionServicio
        ///</summary>
        public abstract SoftvList<ContratacionServicioEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ContratacionServicioEntity GetContratacionServicioFromReader(IDataReader reader)
        {
            ContratacionServicioEntity entity_ContratacionServicio = null;
            try
            {
                entity_ContratacionServicio = new ContratacionServicioEntity();
                entity_ContratacionServicio.Id = (int?)(GetFromReader(reader, "Id"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratacionServicio data to entity", ex);
            }
            return entity_ContratacionServicio;
        }

        public abstract List<StatusEntity> GetStatusNet();
        public abstract List<StatusEntity> GetStatusCableModem();
        public abstract List<UsuarioSoftvEntity> GetMuestra_Usuarios(int? CLV_UNICANET, int? tipo_serv);
        public abstract UsuarioSoftvEntity GetCONRel_ContNet_Usuarios(int? Clv_UnicaNet);
        public abstract List<VendedoresLEntity> GetMuestraPromotoresNet();
        public abstract ServicioDescuentoEntity GetConRelCteDescuento(int? Clv_UnicaNet, int? Clv_TipSer);
        public abstract int? GetNueRelCteDescuento(int? Clv_UnicaNet, int? Clv_TipSer, int? Descuento);
        public abstract int? GetBorRelCteDescuento(int? Clv_UnicaNet, int? Clv_TipSer);
        public abstract List<Servicios_NewEntity> GetListServicioAdicTvDig();
        public abstract List<Servicios_NewEntity> GetRelTipoServClientePorContrato(int? Clv_TipSer, long? contrato);
    }

    #region Customs Methods

    #endregion
}


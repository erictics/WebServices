﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ContratosSaldadosMaestroProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ContratosSaldadosMaestro Provider
    /// File                    : ContratosSaldadosMaestroProvider.cs
    /// Creation date           : 05/07/2017
    /// Creation time           : 06:11 p. m.
    /// </summary>
    public abstract class ContratosSaldadosMaestroProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ContratosSaldadosMaestro from DB
        /// </summary>
        private static ContratosSaldadosMaestroProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ContratosSaldadosMaestro instance
        /// </summary>
        public static ContratosSaldadosMaestroProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ContratosSaldadosMaestro.Assembly,
                    SoftvSettings.Settings.ContratosSaldadosMaestro.DataClass);
                    _Instance = (ContratosSaldadosMaestroProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ContratosSaldadosMaestroProvider()
        {
        }
       
        public abstract List<ContratosSaldadosMaestroEntity> GetContratosSaldadosMaestro(String FechaFacturacion);

        protected virtual ContratosSaldadosMaestroEntity GetContratosSaldadosMaestroFromReader(IDataReader reader)
        {
            ContratosSaldadosMaestroEntity entity_ContratosSaldadosMaestro = null;
            try
            {
                entity_ContratosSaldadosMaestro = new ContratosSaldadosMaestroEntity();
                entity_ContratosSaldadosMaestro.FechaFacturacion = (String)(GetFromReader(reader, "FechaFacturacion", IsString: true));
                entity_ContratosSaldadosMaestro.Factura = (String)(GetFromReader(reader, "Factura", IsString: true));
                entity_ContratosSaldadosMaestro.ContratoMaestro = (long?)(GetFromReader(reader, "ContratoMaestro"));
                entity_ContratosSaldadosMaestro.Contrato = (String)(GetFromReader(reader, "Contrato", IsString: true));
                entity_ContratosSaldadosMaestro.Cliente = (String)(GetFromReader(reader, "Cliente", IsString: true));
                entity_ContratosSaldadosMaestro.Importe = (Decimal?)(GetFromReader(reader, "Importe"));
                entity_ContratosSaldadosMaestro.FechaPago = (String)(GetFromReader(reader, "FechaPago", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratosSaldadosMaestro data to entity", ex);
            }
            return entity_ContratosSaldadosMaestro;
        }

    }

    #region Customs Methods

    #endregion
}


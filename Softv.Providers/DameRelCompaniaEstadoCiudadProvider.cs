﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.DameRelCompaniaEstadoCiudadProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameRelCompaniaEstadoCiudad Provider
    /// File                    : DameRelCompaniaEstadoCiudadProvider.cs
    /// Creation date           : 21/09/2017
    /// Creation time           : 10:43 a. m.
    /// </summary>
    public abstract class DameRelCompaniaEstadoCiudadProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of DameRelCompaniaEstadoCiudad from DB
        /// </summary>
        private static DameRelCompaniaEstadoCiudadProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a DameRelCompaniaEstadoCiudad instance
        /// </summary>
        public static DameRelCompaniaEstadoCiudadProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.DameRelCompaniaEstadoCiudad.Assembly,
                    SoftvSettings.Settings.DameRelCompaniaEstadoCiudad.DataClass);
                    _Instance = (DameRelCompaniaEstadoCiudadProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public DameRelCompaniaEstadoCiudadProvider()
        {
        }
        /// <summary>
        /// Abstract method to add DameRelCompaniaEstadoCiudad
        ///  /summary>
        /// <param name="DameRelCompaniaEstadoCiudad"></param>
        /// <returns></returns>
        public abstract int AddDameRelCompaniaEstadoCiudad(DameRelCompaniaEstadoCiudadEntity entity_DameRelCompaniaEstadoCiudad);

        /// <summary>
        /// Abstract method to delete DameRelCompaniaEstadoCiudad
        /// </summary>
        public abstract int DeleteDameRelCompaniaEstadoCiudad();

        /// <summary>
        /// Abstract method to update DameRelCompaniaEstadoCiudad
        /// </summary>
        public abstract int EditDameRelCompaniaEstadoCiudad(DameRelCompaniaEstadoCiudadEntity entity_DameRelCompaniaEstadoCiudad);

        /// <summary>
        /// Abstract method to get all DameRelCompaniaEstadoCiudad
        /// </summary>
        public abstract List<DameRelCompaniaEstadoCiudadEntity> GetDameRelCompaniaEstadoCiudad(long? clv_plaza);

        /// <summary>
        /// Abstract method to get all DameRelCompaniaEstadoCiudad List<int> lid
        /// </summary>
        public abstract List<DameRelCompaniaEstadoCiudadEntity> GetDameRelCompaniaEstadoCiudad(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract DameRelCompaniaEstadoCiudadEntity GetDameRelCompaniaEstadoCiudadById();



        /// <summary>
        ///Get DameRelCompaniaEstadoCiudad
        ///</summary>
        public abstract SoftvList<DameRelCompaniaEstadoCiudadEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get DameRelCompaniaEstadoCiudad
        ///</summary>
        public abstract SoftvList<DameRelCompaniaEstadoCiudadEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual DameRelCompaniaEstadoCiudadEntity GetDameRelCompaniaEstadoCiudadFromReader(IDataReader reader)
        {
            DameRelCompaniaEstadoCiudadEntity entity_DameRelCompaniaEstadoCiudad = null;
            try
            {
                entity_DameRelCompaniaEstadoCiudad = new DameRelCompaniaEstadoCiudadEntity();
                entity_DameRelCompaniaEstadoCiudad.id_compania = (int?)(GetFromReader(reader, "id_compania"));
                entity_DameRelCompaniaEstadoCiudad.NomCompania = (String)(GetFromReader(reader, "NomCompania", IsString: true));
                entity_DameRelCompaniaEstadoCiudad.Clv_Estado = (int?)(GetFromReader(reader, "Clv_Estado"));
                entity_DameRelCompaniaEstadoCiudad.NomEstado = (String)(GetFromReader(reader, "NomEstado", IsString: true));
                entity_DameRelCompaniaEstadoCiudad.Clv_Ciudad = (int?)(GetFromReader(reader, "Clv_Ciudad"));
                entity_DameRelCompaniaEstadoCiudad.NomCiudad = (String)(GetFromReader(reader, "NomCiudad", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting DameRelCompaniaEstadoCiudad data to entity", ex);
            }
            return entity_DameRelCompaniaEstadoCiudad;
        }

    }

    #region Customs Methods

    #endregion
}


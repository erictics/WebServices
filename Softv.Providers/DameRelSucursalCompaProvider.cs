﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.DameRelSucursalCompaProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameRelSucursalCompa Provider
    /// File                    : DameRelSucursalCompaProvider.cs
    /// Creation date           : 27/10/2016
    /// Creation time           : 11:28 a. m.
    /// </summary>
    public abstract class DameRelSucursalCompaProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of DameRelSucursalCompa from DB
        /// </summary>
        private static DameRelSucursalCompaProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a DameRelSucursalCompa instance
        /// </summary>
        public static DameRelSucursalCompaProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.DameRelSucursalCompa.Assembly,
                    SoftvSettings.Settings.DameRelSucursalCompa.DataClass);
                    _Instance = (DameRelSucursalCompaProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public DameRelSucursalCompaProvider()
        {
        }


        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract DameRelSucursalCompaEntity GetDameRelSucursalCompaById(int? IdSucursal, long? Contrato);

        public abstract DameRelSucursalCompaEntity GetDeepDimeSiYaGrabeFac(long? Contrato);


        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual DameRelSucursalCompaEntity GetDameRelSucursalCompaFromReader(IDataReader reader)
        {
            DameRelSucursalCompaEntity entity_DameRelSucursalCompa = null;
            try
            {
                entity_DameRelSucursalCompa = new DameRelSucursalCompaEntity();
                //entity_DameRelSucursalCompa.IdSucursal = (int?)(GetFromReader(reader, "IdSucursal"));
                //entity_DameRelSucursalCompa.Contrato = (long?)(GetFromReader(reader, "Contrato"));
                entity_DameRelSucursalCompa.Id = (int?)(GetFromReader(reader, "Id"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting DameRelSucursalCompa data to entity", ex);
            }
            return entity_DameRelSucursalCompa;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DamelasOrdenesque_GeneroFacturaAgendaOrdser Provider
    /// File                    : DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider.cs
    /// Creation date           : 02/03/2017
    /// Creation time           : 06:50 p. m.
    /// </summary>
    public abstract class DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of DamelasOrdenesque_GeneroFacturaAgendaOrdser from DB
        /// </summary>
        private static DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a DamelasOrdenesque_GeneroFacturaAgendaOrdser instance
        /// </summary>
        public static DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.DamelasOrdenesque_GeneroFacturaAgendaOrdser.Assembly,
                    SoftvSettings.Settings.DamelasOrdenesque_GeneroFacturaAgendaOrdser.DataClass);
                    _Instance = (DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public DamelasOrdenesque_GeneroFacturaAgendaOrdserProvider()
        {
        }
        /// <summary>
        /// Abstract method to add DamelasOrdenesque_GeneroFacturaAgendaOrdser
        ///  /summary>
        /// <param name="DamelasOrdenesque_GeneroFacturaAgendaOrdser"></param>
        /// <returns></returns>
        public abstract int AddDamelasOrdenesque_GeneroFacturaAgendaOrdser(DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser);

        /// <summary>
        /// Abstract method to delete DamelasOrdenesque_GeneroFacturaAgendaOrdser
        /// </summary>
        public abstract int DeleteDamelasOrdenesque_GeneroFacturaAgendaOrdser();

        /// <summary>
        /// Abstract method to update DamelasOrdenesque_GeneroFacturaAgendaOrdser
        /// </summary>
        public abstract int EditDamelasOrdenesque_GeneroFacturaAgendaOrdser(DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser);

        /// <summary>
        /// Abstract method to get all DamelasOrdenesque_GeneroFacturaAgendaOrdser
        /// </summary>
        public abstract List<DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity> GetDamelasOrdenesque_GeneroFacturaAgendaOrdser();

        /// <summary>
        /// Abstract method to get all DamelasOrdenesque_GeneroFacturaAgendaOrdser List<int> lid
        /// </summary>
        public abstract List<DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity> GetDamelasOrdenesque_GeneroFacturaAgendaOrdser(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity GetDamelasOrdenesque_GeneroFacturaAgendaOrdserById(long? ClvFactura);


        public abstract DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity GetValidaExistenciaOrdenPorFactura(long? ClvFactura);





        /// <summary>
        ///Get DamelasOrdenesque_GeneroFacturaAgendaOrdser
        ///</summary>
        public abstract SoftvList<DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get DamelasOrdenesque_GeneroFacturaAgendaOrdser
        ///</summary>
        public abstract SoftvList<DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity GetDamelasOrdenesque_GeneroFacturaAgendaOrdserFromReader(IDataReader reader)
        {
            DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser = null;
            try
            {
                entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser = new DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity();
                //entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser.ClvFactura = (long?)(GetFromReader(reader, "ClvFactura"));
                entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser.Orden = (long?)(GetFromReader(reader, "Orden"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting DamelasOrdenesque_GeneroFacturaAgendaOrdser data to entity", ex);
            }
            return entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser;
        }


        protected virtual DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity GetValidaExistenciaOrdenPorFacturaFromReader(IDataReader reader)
        {
            DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser = null;
            try
            {
                entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser = new DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity();
                //entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser.ClvFactura = (long?)(GetFromReader(reader, "ClvFactura"));
                entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser.valida = (int?)(GetFromReader(reader, "valida"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting DamelasOrdenesque_GeneroFacturaAgendaOrdser data to entity", ex);
            }
            return entity_DamelasOrdenesque_GeneroFacturaAgendaOrdser;
        }









    }

    #region Customs Methods

    #endregion
}


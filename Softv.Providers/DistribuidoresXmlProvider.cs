﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.DistribuidoresXmlProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DistribuidoresXml Provider
    /// File                    : DistribuidoresXmlProvider.cs
    /// Creation date           : 18/11/2016
    /// Creation time           : 10:53 a. m.
    /// </summary>
    public abstract class DistribuidoresXmlProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of DistribuidoresXml from DB
        /// </summary>
        private static DistribuidoresXmlProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a DistribuidoresXml instance
        /// </summary>
        public static DistribuidoresXmlProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.DistribuidoresXml.Assembly,
                    SoftvSettings.Settings.DistribuidoresXml.DataClass);
                    _Instance = (DistribuidoresXmlProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public DistribuidoresXmlProvider()
        {
        }
        /// <summary>
        /// Abstract method to add DistribuidoresXml
        ///  /summary>
        /// <param name="DistribuidoresXml"></param>
        /// <returns></returns>
        public abstract int AddDistribuidoresXml(DistribuidoresXmlEntity entity_DistribuidoresXml);

        /// <summary>
        /// Abstract method to delete DistribuidoresXml
        /// </summary>
        public abstract int DeleteDistribuidoresXml();

        /// <summary>
        /// Abstract method to update DistribuidoresXml
        /// </summary>
        public abstract int EditDistribuidoresXml(DistribuidoresXmlEntity entity_DistribuidoresXml);

        /// <summary>
        /// Abstract method to get all DistribuidoresXml
        /// </summary>
        public abstract List<DistribuidoresXmlEntity> GetDistribuidoresXml();

        /// <summary>
        /// Abstract method to get all DistribuidoresXml List<int> lid
        /// </summary>
        public abstract List<DistribuidoresXmlEntity> GetDistribuidoresXml(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract DistribuidoresXmlEntity GetDistribuidoresXmlById();



        /// <summary>
        ///Get DistribuidoresXml
        ///</summary>
        public abstract SoftvList<DistribuidoresXmlEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get DistribuidoresXml
        ///</summary>
        public abstract SoftvList<DistribuidoresXmlEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual DistribuidoresXmlEntity GetDistribuidoresXmlFromReader(IDataReader reader)
        {
            DistribuidoresXmlEntity entity_DistribuidoresXml = null;
            try
            {
                entity_DistribuidoresXml = new DistribuidoresXmlEntity();
                //entity_DistribuidoresXml.IdDis = (long?)(GetFromReader(reader, "IdDis"));
                entity_DistribuidoresXml.Distribuidores = (String)(GetFromReader(reader, "Distribuidores", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting DistribuidoresXml data to entity", ex);
            }
            return entity_DistribuidoresXml;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.EncuestasProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Encuestas Provider
    /// File                    : EncuestasProvider.cs
    /// Creation date           : 17/06/2017
    /// Creation time           : 10:59 a. m.
    /// </summary>
    public abstract class EncuestasProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Encuestas from DB
        /// </summary>
        private static EncuestasProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Encuestas instance
        /// </summary>
        public static EncuestasProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Encuestas.Assembly,
                    SoftvSettings.Settings.Encuestas.DataClass);
                    _Instance = (EncuestasProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public EncuestasProvider()
        {
        }
        /// <summary>
        /// Abstract method to add Encuestas
        ///  /summary>
        /// <param name="Encuestas"></param>
        /// <returns></returns>
        public abstract int AddEncuestas(EncuestasEntity entity_Encuestas);

        /// <summary>
        /// Abstract method to delete Encuestas
        /// </summary>
        public abstract int DeleteEncuestas(int? IdEncuesta);

        /// <summary>
        /// Abstract method to update Encuestas
        /// </summary>
        public abstract int EditEncuestas(EncuestasEntity entity_Encuestas);

        /// <summary>
        /// Abstract method to get all Encuestas
        /// </summary>
        public abstract List<EncuestasEntity> GetEncuestas();

        /// <summary>
        /// Abstract method to get all Encuestas List<int> lid
        /// </summary>
        public abstract List<EncuestasEntity> GetEncuestas(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract EncuestasEntity GetEncuestasById(int? IdEncuesta);


        public abstract List<EncuestasEntity> GetAddEncuesta(String xml);

        public abstract List<EncuestasEntity> GetEditEncuesta(String xml);

        public abstract EncuestasEntity GetEncuestaDetalle(int? idencuesta);
        public abstract string ImprimeEncuesta(int? idencuesta);

        /// <summary>
        ///Get Encuestas
        ///</summary>
        public abstract SoftvList<EncuestasEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get Encuestas
        ///</summary>
        public abstract SoftvList<EncuestasEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual EncuestasEntity GetEncuestasFromReader(IDataReader reader)
        {
            EncuestasEntity entity_Encuestas = null;
            try
            {
                entity_Encuestas = new EncuestasEntity();
                entity_Encuestas.IdEncuesta = (int?)(GetFromReader(reader, "IdEncuesta"));
                entity_Encuestas.TituloEncuesta = (String)(GetFromReader(reader, "TituloEncuesta", IsString: true));
                entity_Encuestas.Descripcion = (String)(GetFromReader(reader, "Descripcion", IsString: true));
                entity_Encuestas.FechaCreacion = (String)(GetFromReader(reader, "FechaCreacion", IsString: true));
                entity_Encuestas.IdUsuario = (int?)(GetFromReader(reader, "IdUsuario"));
                entity_Encuestas.Aplicada = (bool?)(GetFromReader(reader, "Aplicada"));
                entity_Encuestas.Activa = (bool?)(GetFromReader(reader, "Activa"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Encuestas data to entity", ex);
            }
            return entity_Encuestas;
        }


        protected virtual EncuestasEntity GetEncuestasDosFromReader(IDataReader reader)
        {
            EncuestasEntity entity_Encuestas = null;
            try
            {
                entity_Encuestas = new EncuestasEntity();
                entity_Encuestas.IdEncuesta = (int?)(GetFromReader(reader, "IdEncuesta"));
            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Encuestas data to entity", ex);
            }
            return entity_Encuestas;
        }




    }

    #region Customs Methods

    #endregion
}


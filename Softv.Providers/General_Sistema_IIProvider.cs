﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.General_Sistema_IIProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : General_Sistema_II Provider
    /// File                    : General_Sistema_IIProvider.cs
    /// Creation date           : 10/11/2017
    /// Creation time           : 01:15 p. m.
    /// </summary>
    public abstract class General_Sistema_IIProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of General_Sistema_II from DB
        /// </summary>
        private static General_Sistema_IIProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a General_Sistema_II instance
        /// </summary>
        public static General_Sistema_IIProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.General_Sistema_II.Assembly,
                    SoftvSettings.Settings.General_Sistema_II.DataClass);
                    _Instance = (General_Sistema_IIProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public General_Sistema_IIProvider()
        {
        }
        /// <summary>
        /// Abstract method to add General_Sistema_II
        ///  /summary>
        /// <param name="General_Sistema_II"></param>
        /// <returns></returns>
        public abstract int AddGeneral_Sistema_II(General_Sistema_IIEntity entity_General_Sistema_II);

        /// <summary>
        /// Abstract method to delete General_Sistema_II
        /// </summary>
        public abstract int DeleteGeneral_Sistema_II();

        /// <summary>
        /// Abstract method to update General_Sistema_II
        /// </summary>
        public abstract int EditGeneral_Sistema_II(General_Sistema_IIEntity entity_General_Sistema_II);

        /// <summary>
        /// Abstract method to get all General_Sistema_II
        /// </summary>
        public abstract List<General_Sistema_IIEntity> GetGeneral_Sistema_II();

        /// <summary>
        /// Abstract method to get all General_Sistema_II List<int> lid
        /// </summary>
        public abstract List<General_Sistema_IIEntity> GetGeneral_Sistema_II(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract General_Sistema_IIEntity GetGeneral_Sistema_IIById();



        /// <summary>
        ///Get General_Sistema_II
        ///</summary>
        public abstract SoftvList<General_Sistema_IIEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get General_Sistema_II
        ///</summary>
        public abstract SoftvList<General_Sistema_IIEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual General_Sistema_IIEntity GetGeneral_Sistema_IIFromReader(IDataReader reader)
        {
            General_Sistema_IIEntity entity_General_Sistema_II = null;
            try
            {
                entity_General_Sistema_II = new General_Sistema_IIEntity();
                entity_General_Sistema_II.AlcanceRedIp = (Int16?)(GetFromReader(reader, "AlcanceRedIp"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting General_Sistema_II data to entity", ex);
            }
            return entity_General_Sistema_II;
        }

        public abstract General_Sistema_IIEntity Get_ActivaIP();

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.GuardaPagoFacturaMaestroProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : GuardaPagoFacturaMaestro Provider
    /// File                    : GuardaPagoFacturaMaestroProvider.cs
    /// Creation date           : 29/03/2017
    /// Creation time           : 06:55 p. m.
    /// </summary>
    public abstract class GuardaPagoFacturaMaestroProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of GuardaPagoFacturaMaestro from DB
        /// </summary>
        private static GuardaPagoFacturaMaestroProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a GuardaPagoFacturaMaestro instance
        /// </summary>
        public static GuardaPagoFacturaMaestroProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.GuardaPagoFacturaMaestro.Assembly,
                    SoftvSettings.Settings.GuardaPagoFacturaMaestro.DataClass);
                    _Instance = (GuardaPagoFacturaMaestroProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public GuardaPagoFacturaMaestroProvider()
        {
        }
        /// <summary>
        /// Abstract method to add GuardaPagoFacturaMaestro
        ///  /summary>
        /// <param name="GuardaPagoFacturaMaestro"></param>
        /// <returns></returns>
        public abstract int AddGuardaPagoFacturaMaestro(GuardaPagoFacturaMaestroEntity entity_GuardaPagoFacturaMaestro);

        /// <summary>
        /// Abstract method to delete GuardaPagoFacturaMaestro
        /// </summary>
        public abstract int DeleteGuardaPagoFacturaMaestro();

        /// <summary>
        /// Abstract method to update GuardaPagoFacturaMaestro
        /// </summary>
        public abstract int EditGuardaPagoFacturaMaestro(GuardaPagoFacturaMaestroEntity entity_GuardaPagoFacturaMaestro);

        /// <summary>
        /// Abstract method to get all GuardaPagoFacturaMaestro
        /// </summary>
        public abstract List<GuardaPagoFacturaMaestroEntity> GetGuardaPagoFacturaMaestro(long? Clv_FacturaMaestro, long? ContratoMaestro, String Cajera, int? Caja, String IpMaquina, int? Sucursal, Decimal? Monto, Decimal? GLOEFECTIVO2, Decimal? GLOCHEQUE2, int? GLOCLV_BANCOCHEQUE2, String NUMEROCHEQUE2, Decimal? GLOTARJETA2, int? GLOCLV_BANCOTARJETA2, String NUMEROTARJETA2, String TARJETAAUTORIZACION2, long? CLV_Nota3, Decimal? GLONOTA3, int? IdMedioPago, int? IdCompania, int? IdDistribuidor);

        /// <summary>
        /// Abstract method to get all GuardaPagoFacturaMaestro List<int> lid
        /// </summary>
        public abstract List<GuardaPagoFacturaMaestroEntity> GetGuardaPagoFacturaMaestro(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract GuardaPagoFacturaMaestroEntity GetGuardaPagoFacturaMaestroById();



        /// <summary>
        ///Get GuardaPagoFacturaMaestro
        ///</summary>
        public abstract SoftvList<GuardaPagoFacturaMaestroEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get GuardaPagoFacturaMaestro
        ///</summary>
        public abstract SoftvList<GuardaPagoFacturaMaestroEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        //protected virtual GuardaPagoFacturaMaestroEntity GetGuardaPagoFacturaMaestroFromReader(IDataReader reader)
        //{
        //    GuardaPagoFacturaMaestroEntity entity_GuardaPagoFacturaMaestro = null;
        //    try
        //    {
        //        entity_GuardaPagoFacturaMaestro = new GuardaPagoFacturaMaestroEntity();
        //        entity_GuardaPagoFacturaMaestro.Clv_Pago = (long?)(GetFromReader(reader, "Clv_Pago"));

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error converting GuardaPagoFacturaMaestro data to entity", ex);
        //    }
        //    return entity_GuardaPagoFacturaMaestro;
        //}

    }

    #region Customs Methods

    #endregion
}


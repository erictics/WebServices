﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ListadoNotasProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ListadoNotas Provider
    /// File                    : ListadoNotasProvider.cs
    /// Creation date           : 30/03/2016
    /// Creation time           : 06:59 p. m.
    /// </summary>
    public abstract class ListadoNotasProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ListadoNotas from DB
        /// </summary>
        private static ListadoNotasProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ListadoNotas instance
        /// </summary>
        public static ListadoNotasProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ListadoNotas.Assembly,
                    SoftvSettings.Settings.ListadoNotas.DataClass);
                    _Instance = (ListadoNotasProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ListadoNotasProvider()
        {
        }
        /// <summary>
        /// Abstract method to add ListadoNotas
        ///  /summary>
        /// <param name="ListadoNotas"></param>
        /// <returns></returns>


        /// <summary>
        /// Abstract method to get all ListadoNotas
        /// </summary>
        public abstract List<ListadoNotasEntity> GetListadoNotas(long? IdContrato);

        /// <summary>
        /// Abstract method to get all ListadoNotas List<int> lid
        /// </summary>
        public abstract List<ListadoNotasEntity> GetListadoNotas(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ListadoNotasEntity GetListadoNotasById(long? IdContrato);



        /// <summary>
        ///Get ListadoNotas
        ///</summary>
        public abstract SoftvList<ListadoNotasEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get ListadoNotas
        ///</summary>
        public abstract SoftvList<ListadoNotasEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ListadoNotasEntity GetListadoNotasFromReader(IDataReader reader)
        {
            ListadoNotasEntity entity_ListadoNotas = null;
            try
            {
                entity_ListadoNotas = new ListadoNotasEntity();
               
                entity_ListadoNotas.Observacion = (String)(GetFromReader(reader, "Observacion", IsString: true));
                entity_ListadoNotas.DescripcionRobo = (String)(GetFromReader(reader, "DescripcionRobo", IsString: true));
                entity_ListadoNotas.FechaRobo = (String)(GetFromReader(reader, "FechaRobo", IsString: true));
                entity_ListadoNotas.IdContrato = (int?)(GetFromReader(reader, "IdContrato", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ListadoNotas data to entity", ex);
            }
            return entity_ListadoNotas;
        }

    }

    #region Customs Methods

    #endregion
}


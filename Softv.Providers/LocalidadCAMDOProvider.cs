﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.LocalidadCAMDOProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : LocalidadCAMDO Provider
    /// File                    : LocalidadCAMDOProvider.cs
    /// Creation date           : 25/10/2016
    /// Creation time           : 06:28 p. m.
    /// </summary>
    public abstract class LocalidadCAMDOProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of LocalidadCAMDO from DB
        /// </summary>
        private static LocalidadCAMDOProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a LocalidadCAMDO instance
        /// </summary>
        public static LocalidadCAMDOProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.LocalidadCAMDO.Assembly,
                    SoftvSettings.Settings.LocalidadCAMDO.DataClass);
                    _Instance = (LocalidadCAMDOProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public LocalidadCAMDOProvider()
        {
        }


        /// <summary>
        /// Abstract method to get all LocalidadCAMDO
        /// </summary>
        public abstract List<LocalidadCAMDOEntity> GetLocalidadCAMDO(long? Contrato, int? Ciudad);



        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual LocalidadCAMDOEntity GetLocalidadCAMDOFromReader(IDataReader reader)
        {
            LocalidadCAMDOEntity entity_LocalidadCAMDO = null;
            try
            {
                entity_LocalidadCAMDO = new LocalidadCAMDOEntity();
                entity_LocalidadCAMDO.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));
                entity_LocalidadCAMDO.Clv_Localidad = (long?)(GetFromReader(reader, "Clv_Localidad"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting LocalidadCAMDO data to entity", ex);
            }
            return entity_LocalidadCAMDO;
        }

    }

    #region Customs Methods

    #endregion
}


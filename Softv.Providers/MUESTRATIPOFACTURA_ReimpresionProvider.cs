﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.MUESTRATIPOFACTURA_ReimpresionProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MUESTRATIPOFACTURA_Reimpresion Provider
    /// File                    : MUESTRATIPOFACTURA_ReimpresionProvider.cs
    /// Creation date           : 03/02/2017
    /// Creation time           : 06:16 p. m.
    /// </summary>
    public abstract class MUESTRATIPOFACTURA_ReimpresionProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of MUESTRATIPOFACTURA_Reimpresion from DB
        /// </summary>
        private static MUESTRATIPOFACTURA_ReimpresionProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a MUESTRATIPOFACTURA_Reimpresion instance
        /// </summary>
        public static MUESTRATIPOFACTURA_ReimpresionProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.MUESTRATIPOFACTURA_Reimpresion.Assembly,
                    SoftvSettings.Settings.MUESTRATIPOFACTURA_Reimpresion.DataClass);
                    _Instance = (MUESTRATIPOFACTURA_ReimpresionProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public MUESTRATIPOFACTURA_ReimpresionProvider()
        {
        }
        /// <summary>
        /// Abstract method to add MUESTRATIPOFACTURA_Reimpresion
        ///  /summary>
        /// <param name="MUESTRATIPOFACTURA_Reimpresion"></param>
        /// <returns></returns>
        public abstract int AddMUESTRATIPOFACTURA_Reimpresion(MUESTRATIPOFACTURA_ReimpresionEntity entity_MUESTRATIPOFACTURA_Reimpresion);

        /// <summary>
        /// Abstract method to delete MUESTRATIPOFACTURA_Reimpresion
        /// </summary>
        public abstract int DeleteMUESTRATIPOFACTURA_Reimpresion();

        /// <summary>
        /// Abstract method to update MUESTRATIPOFACTURA_Reimpresion
        /// </summary>
        public abstract int EditMUESTRATIPOFACTURA_Reimpresion(MUESTRATIPOFACTURA_ReimpresionEntity entity_MUESTRATIPOFACTURA_Reimpresion);

        /// <summary>
        /// Abstract method to get all MUESTRATIPOFACTURA_Reimpresion
        /// </summary>
        public abstract List<MUESTRATIPOFACTURA_ReimpresionEntity> GetMUESTRATIPOFACTURA_Reimpresion();

        /// <summary>
        /// Abstract method to get all MUESTRATIPOFACTURA_Reimpresion List<int> lid
        /// </summary>
        public abstract List<MUESTRATIPOFACTURA_ReimpresionEntity> GetMUESTRATIPOFACTURA_Reimpresion(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract MUESTRATIPOFACTURA_ReimpresionEntity GetMUESTRATIPOFACTURA_ReimpresionById();



        /// <summary>
        ///Get MUESTRATIPOFACTURA_Reimpresion
        ///</summary>
        public abstract SoftvList<MUESTRATIPOFACTURA_ReimpresionEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get MUESTRATIPOFACTURA_Reimpresion
        ///</summary>
        public abstract SoftvList<MUESTRATIPOFACTURA_ReimpresionEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual MUESTRATIPOFACTURA_ReimpresionEntity GetMUESTRATIPOFACTURA_ReimpresionFromReader(IDataReader reader)
        {
            MUESTRATIPOFACTURA_ReimpresionEntity entity_MUESTRATIPOFACTURA_Reimpresion = null;
            try
            {
                entity_MUESTRATIPOFACTURA_Reimpresion = new MUESTRATIPOFACTURA_ReimpresionEntity();
                entity_MUESTRATIPOFACTURA_Reimpresion.CLAVE = (String)(GetFromReader(reader, "CLAVE", IsString: true));
                entity_MUESTRATIPOFACTURA_Reimpresion.CONCEPTO = (String)(GetFromReader(reader, "CONCEPTO", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting MUESTRATIPOFACTURA_Reimpresion data to entity", ex);
            }
            return entity_MUESTRATIPOFACTURA_Reimpresion;
        }

    }

    #region Customs Methods

    #endregion
}


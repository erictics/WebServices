﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.MUESTRATRABAJOSQUEJASProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MUESTRATRABAJOSQUEJAS Provider
    /// File                    : MUESTRATRABAJOSQUEJASProvider.cs
    /// Creation date           : 24/01/2017
    /// Creation time           : 01:06 p. m.
    /// </summary>
    public abstract class MUESTRATRABAJOSQUEJASProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of MUESTRATRABAJOSQUEJAS from DB
        /// </summary>
        private static MUESTRATRABAJOSQUEJASProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a MUESTRATRABAJOSQUEJAS instance
        /// </summary>
        public static MUESTRATRABAJOSQUEJASProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.MUESTRATRABAJOSQUEJAS.Assembly,
                    SoftvSettings.Settings.MUESTRATRABAJOSQUEJAS.DataClass);
                    _Instance = (MUESTRATRABAJOSQUEJASProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public MUESTRATRABAJOSQUEJASProvider()
        {
        }
        /// <summary>
        /// Abstract method to add MUESTRATRABAJOSQUEJAS
        ///  /summary>
        /// <param name="MUESTRATRABAJOSQUEJAS"></param>
        /// <returns></returns>
        public abstract int AddMUESTRATRABAJOSQUEJAS(MUESTRATRABAJOSQUEJASEntity entity_MUESTRATRABAJOSQUEJAS);

        /// <summary>
        /// Abstract method to delete MUESTRATRABAJOSQUEJAS
        /// </summary>
        public abstract int DeleteMUESTRATRABAJOSQUEJAS();

        /// <summary>
        /// Abstract method to update MUESTRATRABAJOSQUEJAS
        /// </summary>
        public abstract int EditMUESTRATRABAJOSQUEJAS(MUESTRATRABAJOSQUEJASEntity entity_MUESTRATRABAJOSQUEJAS);

        /// <summary>
        /// Abstract method to get all MUESTRATRABAJOSQUEJAS
        /// </summary>
        public abstract List<MUESTRATRABAJOSQUEJASEntity> GetMUESTRATRABAJOSQUEJAS(int? TipSer);

        public abstract List<MUESTRATRABAJOSQUEJASEntity> GetMUESTRATRABAJOSQUEJAS_MasivosList();

        /// <summary>
        /// Abstract method to get all MUESTRATRABAJOSQUEJAS List<int> lid
        /// </summary>
        public abstract List<MUESTRATRABAJOSQUEJASEntity> GetMUESTRATRABAJOSQUEJAS(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract MUESTRATRABAJOSQUEJASEntity GetMUESTRATRABAJOSQUEJASById();



        /// <summary>
        ///Get MUESTRATRABAJOSQUEJAS
        ///</summary>
        public abstract SoftvList<MUESTRATRABAJOSQUEJASEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get MUESTRATRABAJOSQUEJAS
        ///</summary>
        public abstract SoftvList<MUESTRATRABAJOSQUEJASEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual MUESTRATRABAJOSQUEJASEntity GetMUESTRATRABAJOSQUEJASFromReader(IDataReader reader)
        {
            MUESTRATRABAJOSQUEJASEntity entity_MUESTRATRABAJOSQUEJAS = null;
            try
            {
                entity_MUESTRATRABAJOSQUEJAS = new MUESTRATRABAJOSQUEJASEntity();
                entity_MUESTRATRABAJOSQUEJAS.CLV_TRABAJO = (int?)(GetFromReader(reader, "CLV_TRABAJO"));
                entity_MUESTRATRABAJOSQUEJAS.DESCRIPCION = (String)(GetFromReader(reader, "DESCRIPCION", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting MUESTRATRABAJOSQUEJAS data to entity", ex);
            }
            return entity_MUESTRATRABAJOSQUEJAS;
        }

    }

    #region Customs Methods

    #endregion
}


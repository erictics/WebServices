﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.Medio_SoftvProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Medio_Softv Provider
    /// File                    : Medio_SoftvProvider.cs
    /// Creation date           : 22/08/2017
    /// Creation time           : 12:49 p. m.
    /// </summary>
    public abstract class Medio_SoftvProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Medio_Softv from DB
        /// </summary>
        private static Medio_SoftvProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Medio_Softv instance
        /// </summary>
        public static Medio_SoftvProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Medio_Softv.Assembly,
                    SoftvSettings.Settings.Medio_Softv.DataClass);
                    _Instance = (Medio_SoftvProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public Medio_SoftvProvider()
        {
        }
        /// <summary>
        /// Abstract method to add Medio_Softv
        ///  /summary>
        /// <param name="Medio_Softv"></param>
        /// <returns></returns>
        public abstract int AddMedio_Softv(Medio_SoftvEntity entity_Medio_Softv);

        /// <summary>
        /// Abstract method to delete Medio_Softv
        /// </summary>
        public abstract int DeleteMedio_Softv();

        /// <summary>
        /// Abstract method to update Medio_Softv
        /// </summary>
        public abstract int EditMedio_Softv(Medio_SoftvEntity entity_Medio_Softv);

        /// <summary>
        /// Abstract method to get all Medio_Softv
        /// </summary>
        public abstract List<Medio_SoftvEntity> GetMedio_Softv();


        public abstract List<Medio_SoftvEntity> GetMedio_SoftvList(String xml);




        /// <summary>
        /// Abstract method to get all Medio_Softv List<int> lid
        /// </summary>
        public abstract List<Medio_SoftvEntity> GetMedio_Softv(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract Medio_SoftvEntity GetMedio_SoftvById();



        /// <summary>
        ///Get Medio_Softv
        ///</summary>
        public abstract SoftvList<Medio_SoftvEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get Medio_Softv
        ///</summary>
        public abstract SoftvList<Medio_SoftvEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual Medio_SoftvEntity GetMedio_SoftvFromReader(IDataReader reader)
        {
            Medio_SoftvEntity entity_Medio_Softv = null;
            try
            {
                entity_Medio_Softv = new Medio_SoftvEntity();
                //entity_Medio_Softv.Id = (int?)(GetFromReader(reader, "Id"));
                entity_Medio_Softv.IdArticulo = (long?)(GetFromReader(reader, "IdArticulo"));
                entity_Medio_Softv.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));
                entity_Medio_Softv.Letra = (String)(GetFromReader(reader, "Letra", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Medio_Softv data to entity", ex);
            }
            return entity_Medio_Softv;
        }





    }

    #region Customs Methods

    #endregion
}


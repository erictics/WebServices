﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.MuestraAparatosDisponiblesProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraAparatosDisponibles Provider
    /// File                    : MuestraAparatosDisponiblesProvider.cs
    /// Creation date           : 04/09/2017
    /// Creation time           : 05:32 p. m.
    /// </summary>
    public abstract class MuestraAparatosDisponiblesProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of MuestraAparatosDisponibles from DB
        /// </summary>
        private static MuestraAparatosDisponiblesProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a MuestraAparatosDisponibles instance
        /// </summary>
        public static MuestraAparatosDisponiblesProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.MuestraAparatosDisponibles.Assembly,
                    SoftvSettings.Settings.MuestraAparatosDisponibles.DataClass);
                    _Instance = (MuestraAparatosDisponiblesProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public MuestraAparatosDisponiblesProvider()
        {
        }
        /// <summary>
        /// Abstract method to add MuestraAparatosDisponibles
        ///  /summary>
        /// <param name="MuestraAparatosDisponibles"></param>
        /// <returns></returns>
        /// 
        public abstract List<MuestraAparatosDisponiblesEntity> GetListClienteAparatos(long? Contrato);

        public abstract List<MuestraAparatosDisponiblesEntity> GetListAparatosDisponiblesByIdArticulo(long? Clv_Tecnico, long? Id_Articulo);

        public abstract List<StatusAparatosEntity> GetSP_StatusAparatosList();

        public abstract List<MuestraTipoAparatoEntity> GetListTipoAparatosByIdArticulo(long? IdArticulo, long? ContratoNet);

        public abstract int? GetSetCambioAparato(SP_GuardaIAPARATOSEntity ObjCambioAparato);

        public abstract CambioAparatoEntity GetCambioAparatoDeep(long? Clv_Orden, long? Clave);

        public abstract int AddMuestraAparatosDisponibles(MuestraAparatosDisponiblesEntity entity_MuestraAparatosDisponibles);

        /// <summary>
        /// Abstract method to delete MuestraAparatosDisponibles
        /// </summary>
        public abstract int DeleteMuestraAparatosDisponibles();

        /// <summary>
        /// Abstract method to update MuestraAparatosDisponibles
        /// </summary>
        public abstract int EditMuestraAparatosDisponibles(MuestraAparatosDisponiblesEntity entity_MuestraAparatosDisponibles);

        /// <summary>
        /// Abstract method to get all MuestraAparatosDisponibles
        /// </summary>
        public abstract List<MuestraAparatosDisponiblesEntity> GetMuestraAparatosDisponibles(long? clv_orden, long? idArticulo, long? Clv_Tecnico);

        /// <summary>
        /// Abstract method to get all MuestraAparatosDisponibles List<int> lid
        /// </summary>
        public abstract List<MuestraAparatosDisponiblesEntity> GetMuestraAparatosDisponibles(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract MuestraAparatosDisponiblesEntity GetMuestraAparatosDisponiblesById();



        /// <summary>
        ///Get MuestraAparatosDisponibles
        ///</summary>
        public abstract SoftvList<MuestraAparatosDisponiblesEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get MuestraAparatosDisponibles
        ///</summary>
        public abstract SoftvList<MuestraAparatosDisponiblesEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual MuestraAparatosDisponiblesEntity GetMuestraAparatosDisponiblesFromReader(IDataReader reader)
        {
            MuestraAparatosDisponiblesEntity entity_MuestraAparatosDisponibles = null;
            try
            {
                entity_MuestraAparatosDisponibles = new MuestraAparatosDisponiblesEntity();
                //entity_MuestraAparatosDisponibles.clv_orden = (long?)(GetFromReader(reader, "clv_orden"));
                //entity_MuestraAparatosDisponibles.idArticulo = (long?)(GetFromReader(reader, "idArticulo"));
                //entity_MuestraAparatosDisponibles.Clv_Tecnico = (long?)(GetFromReader(reader, "Clv_Tecnico"));
                entity_MuestraAparatosDisponibles.Clv_Aparato = (long?)(GetFromReader(reader, "Clv_Aparato"));
                entity_MuestraAparatosDisponibles.Descripcion = (String)(GetFromReader(reader, "Descripcion", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting MuestraAparatosDisponibles data to entity", ex);
            }
            return entity_MuestraAparatosDisponibles;
        }

    }

    #region Customs Methods

    #endregion
}


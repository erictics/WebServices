﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.MuestraCalleColoniaProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraCalleColonia Provider
    /// File                    : MuestraCalleColoniaProvider.cs
    /// Creation date           : 05/09/2017
    /// Creation time           : 05:21 p. m.
    /// </summary>
    public abstract class MuestraCalleColoniaProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of MuestraCalleColonia from DB
        /// </summary>
        private static MuestraCalleColoniaProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a MuestraCalleColonia instance
        /// </summary>
        public static MuestraCalleColoniaProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.MuestraCalleColonia.Assembly,
                    SoftvSettings.Settings.MuestraCalleColonia.DataClass);
                    _Instance = (MuestraCalleColoniaProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public MuestraCalleColoniaProvider()
        {
        }
        /// <summary>
        /// Abstract method to add MuestraCalleColonia
        ///  /summary>
        /// <param name="MuestraCalleColonia"></param>
        /// <returns></returns>
        public abstract int AddMuestraCalleColonia(MuestraCalleColoniaEntity entity_MuestraCalleColonia);

        /// <summary>
        /// Abstract method to delete MuestraCalleColonia
        /// </summary>
        public abstract int DeleteMuestraCalleColonia();

        /// <summary>
        /// Abstract method to update MuestraCalleColonia
        /// </summary>
        public abstract int EditMuestraCalleColonia(MuestraCalleColoniaEntity entity_MuestraCalleColonia);

        /// <summary>
        /// Abstract method to get all MuestraCalleColonia
        /// </summary>
        public abstract List<MuestraCalleColoniaEntity> GetMuestraCalleColonia(int? clv_colonia);

        /// <summary>
        /// Abstract method to get all MuestraCalleColonia List<int> lid
        /// </summary>
        public abstract List<MuestraCalleColoniaEntity> GetMuestraCalleColonia(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract MuestraCalleColoniaEntity GetMuestraCalleColoniaById();



        /// <summary>
        ///Get MuestraCalleColonia
        ///</summary>
        public abstract SoftvList<MuestraCalleColoniaEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get MuestraCalleColonia
        ///</summary>
        public abstract SoftvList<MuestraCalleColoniaEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual MuestraCalleColoniaEntity GetMuestraCalleColoniaFromReader(IDataReader reader)
        {
            MuestraCalleColoniaEntity entity_MuestraCalleColonia = null;
            try
            {
                entity_MuestraCalleColonia = new MuestraCalleColoniaEntity();
                //entity_MuestraCalleColonia.clv_colonia = (int?)(GetFromReader(reader, "clv_colonia"));
                entity_MuestraCalleColonia.Clv_Calle = (int?)(GetFromReader(reader, "Clv_Calle"));
                entity_MuestraCalleColonia.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting MuestraCalleColonia data to entity", ex);
            }
            return entity_MuestraCalleColonia;
        }

    }

    #region Customs Methods

    #endregion
}


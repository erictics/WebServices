﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.MuestraLocalidadCiudadProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraLocalidadCiudad Provider
    /// File                    : MuestraLocalidadCiudadProvider.cs
    /// Creation date           : 05/09/2017
    /// Creation time           : 05:01 p. m.
    /// </summary>
    public abstract class MuestraLocalidadCiudadProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of MuestraLocalidadCiudad from DB
        /// </summary>
        private static MuestraLocalidadCiudadProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a MuestraLocalidadCiudad instance
        /// </summary>
        public static MuestraLocalidadCiudadProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.MuestraLocalidadCiudad.Assembly,
                    SoftvSettings.Settings.MuestraLocalidadCiudad.DataClass);
                    _Instance = (MuestraLocalidadCiudadProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public MuestraLocalidadCiudadProvider()
        {
        }
        /// <summary>
        /// Abstract method to add MuestraLocalidadCiudad
        ///  /summary>
        /// <param name="MuestraLocalidadCiudad"></param>
        /// <returns></returns>
        public abstract int AddMuestraLocalidadCiudad(MuestraLocalidadCiudadEntity entity_MuestraLocalidadCiudad);

        /// <summary>
        /// Abstract method to delete MuestraLocalidadCiudad
        /// </summary>
        public abstract int DeleteMuestraLocalidadCiudad();

        /// <summary>
        /// Abstract method to update MuestraLocalidadCiudad
        /// </summary>
        public abstract int EditMuestraLocalidadCiudad(MuestraLocalidadCiudadEntity entity_MuestraLocalidadCiudad);

        /// <summary>
        /// Abstract method to get all MuestraLocalidadCiudad
        /// </summary>
        public abstract List<MuestraLocalidadCiudadEntity> GetMuestraLocalidadCiudad(int? clv_ciudad);

        /// <summary>
        /// Abstract method to get all MuestraLocalidadCiudad List<int> lid
        /// </summary>
        public abstract List<MuestraLocalidadCiudadEntity> GetMuestraLocalidadCiudad(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract MuestraLocalidadCiudadEntity GetMuestraLocalidadCiudadById();



        /// <summary>
        ///Get MuestraLocalidadCiudad
        ///</summary>
        public abstract SoftvList<MuestraLocalidadCiudadEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get MuestraLocalidadCiudad
        ///</summary>
        public abstract SoftvList<MuestraLocalidadCiudadEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual MuestraLocalidadCiudadEntity GetMuestraLocalidadCiudadFromReader(IDataReader reader)
        {
            MuestraLocalidadCiudadEntity entity_MuestraLocalidadCiudad = null;
            try
            {
                entity_MuestraLocalidadCiudad = new MuestraLocalidadCiudadEntity();
                //entity_MuestraLocalidadCiudad.clv_ciudad = (int?)(GetFromReader(reader, "clv_ciudad"));
                entity_MuestraLocalidadCiudad.Clv_Localidad = (int?)(GetFromReader(reader, "Clv_Localidad"));
                entity_MuestraLocalidadCiudad.NOMBRE = (String)(GetFromReader(reader, "NOMBRE", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting MuestraLocalidadCiudad data to entity", ex);
            }
            return entity_MuestraLocalidadCiudad;
        }

    }

    #region Customs Methods

    #endregion
}


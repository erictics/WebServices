﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.MuestraRelOrdenesTecnicosProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraRelOrdenesTecnicos Provider
    /// File                    : MuestraRelOrdenesTecnicosProvider.cs
    /// Creation date           : 12/06/2017
    /// Creation time           : 05:42 p. m.
    /// </summary>
    public abstract class MuestraRelOrdenesTecnicosProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of MuestraRelOrdenesTecnicos from DB
        /// </summary>
        private static MuestraRelOrdenesTecnicosProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a MuestraRelOrdenesTecnicos instance
        /// </summary>
        public static MuestraRelOrdenesTecnicosProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.MuestraRelOrdenesTecnicos.Assembly,
                    SoftvSettings.Settings.MuestraRelOrdenesTecnicos.DataClass);
                    _Instance = (MuestraRelOrdenesTecnicosProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public MuestraRelOrdenesTecnicosProvider()
        {
        }
        /// <summary>
        /// Abstract method to add MuestraRelOrdenesTecnicos
        ///  /summary>
        /// <param name="MuestraRelOrdenesTecnicos"></param>
        /// <returns></returns>
        public abstract int AddMuestraRelOrdenesTecnicos(MuestraRelOrdenesTecnicosEntity entity_MuestraRelOrdenesTecnicos);

        /// <summary>
        /// Abstract method to delete MuestraRelOrdenesTecnicos
        /// </summary>
        public abstract int DeleteMuestraRelOrdenesTecnicos();

        /// <summary>
        /// Abstract method to update MuestraRelOrdenesTecnicos
        /// </summary>
        public abstract int EditMuestraRelOrdenesTecnicos(MuestraRelOrdenesTecnicosEntity entity_MuestraRelOrdenesTecnicos);

        /// <summary>
        /// Abstract method to get all MuestraRelOrdenesTecnicos
        /// </summary>
        public abstract List<MuestraRelOrdenesTecnicosEntity> GetMuestraRelOrdenesTecnicos(long? ClvOrdSer);

        /// <summary>
        /// Abstract method to get all MuestraRelOrdenesTecnicos List<int> lid
        /// </summary>
        public abstract List<MuestraRelOrdenesTecnicosEntity> GetMuestraRelOrdenesTecnicos(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract MuestraRelOrdenesTecnicosEntity GetMuestraRelOrdenesTecnicosById();



        /// <summary>
        ///Get MuestraRelOrdenesTecnicos
        ///</summary>
        public abstract SoftvList<MuestraRelOrdenesTecnicosEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get MuestraRelOrdenesTecnicos
        ///</summary>
        public abstract SoftvList<MuestraRelOrdenesTecnicosEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual MuestraRelOrdenesTecnicosEntity GetMuestraRelOrdenesTecnicosFromReader(IDataReader reader)
        {
            MuestraRelOrdenesTecnicosEntity entity_MuestraRelOrdenesTecnicos = null;
            try
            {
                entity_MuestraRelOrdenesTecnicos = new MuestraRelOrdenesTecnicosEntity();
                entity_MuestraRelOrdenesTecnicos.CLV_TECNICO = (long?)(GetFromReader(reader, "CLV_TECNICO"));
                entity_MuestraRelOrdenesTecnicos.NOMBRE = (String)(GetFromReader(reader, "NOMBRE", IsString: true));
                //entity_MuestraRelOrdenesTecnicos.ClvOrdSer = (long?)(GetFromReader(reader, "ClvOrdSer"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting MuestraRelOrdenesTecnicos data to entity", ex);
            }
            return entity_MuestraRelOrdenesTecnicos;
        }

    }

    #region Customs Methods

    #endregion
}


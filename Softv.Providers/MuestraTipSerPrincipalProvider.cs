﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.MuestraTipSerPrincipalProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraTipSerPrincipal Provider
    /// File                    : MuestraTipSerPrincipalProvider.cs
    /// Creation date           : 21/10/2016
    /// Creation time           : 06:23 p. m.
    /// </summary>
    public abstract class MuestraTipSerPrincipalProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of MuestraTipSerPrincipal from DB
        /// </summary>
        private static MuestraTipSerPrincipalProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a MuestraTipSerPrincipal instance
        /// </summary>
        public static MuestraTipSerPrincipalProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.MuestraTipSerPrincipal.Assembly,
                    SoftvSettings.Settings.MuestraTipSerPrincipal.DataClass);
                    _Instance = (MuestraTipSerPrincipalProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public MuestraTipSerPrincipalProvider()
        {
        }
        /// <summary>
        /// Abstract method to add MuestraTipSerPrincipal
        ///  /summary>
        /// <param name="MuestraTipSerPrincipal"></param>
        /// <returns></returns>
        public abstract int AddMuestraTipSerPrincipal(MuestraTipSerPrincipalEntity entity_MuestraTipSerPrincipal);

        /// <summary>
        /// Abstract method to delete MuestraTipSerPrincipal
        /// </summary>
        public abstract int DeleteMuestraTipSerPrincipal();

        /// <summary>
        /// Abstract method to update MuestraTipSerPrincipal
        /// </summary>
        public abstract int EditMuestraTipSerPrincipal(MuestraTipSerPrincipalEntity entity_MuestraTipSerPrincipal);

        /// <summary>
        /// Abstract method to get all MuestraTipSerPrincipal
        /// </summary>
        public abstract List<MuestraTipSerPrincipalEntity> GetMuestraTipSerPrincipal();

        /// <summary>
        /// Abstract method to get all MuestraTipSerPrincipal List<int> lid
        /// </summary>
        public abstract List<MuestraTipSerPrincipalEntity> GetMuestraTipSerPrincipal(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract MuestraTipSerPrincipalEntity GetMuestraTipSerPrincipalById();



        /// <summary>
        ///Get MuestraTipSerPrincipal
        ///</summary>
        public abstract SoftvList<MuestraTipSerPrincipalEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get MuestraTipSerPrincipal
        ///</summary>
        public abstract SoftvList<MuestraTipSerPrincipalEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual MuestraTipSerPrincipalEntity GetMuestraTipSerPrincipalFromReader(IDataReader reader)
        {
            MuestraTipSerPrincipalEntity entity_MuestraTipSerPrincipal = null;
            try
            {
                entity_MuestraTipSerPrincipal = new MuestraTipSerPrincipalEntity();
                entity_MuestraTipSerPrincipal.Clv_TipSerPrincipal = (int?)(GetFromReader(reader, "Clv_TipSerPrincipal"));
                entity_MuestraTipSerPrincipal.Concepto = (String)(GetFromReader(reader, "Concepto", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting MuestraTipSerPrincipal data to entity", ex);
            }
            return entity_MuestraTipSerPrincipal;
        }

    }

    #region Customs Methods

    #endregion
}


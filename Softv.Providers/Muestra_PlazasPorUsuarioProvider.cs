﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.Muestra_PlazasPorUsuarioProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Muestra_PlazasPorUsuario Provider
    /// File                    : Muestra_PlazasPorUsuarioProvider.cs
    /// Creation date           : 26/07/2017
    /// Creation time           : 01:19 p. m.
    /// </summary>
    public abstract class Muestra_PlazasPorUsuarioProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Muestra_PlazasPorUsuario from DB
        /// </summary>
        private static Muestra_PlazasPorUsuarioProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Muestra_PlazasPorUsuario instance
        /// </summary>
        public static Muestra_PlazasPorUsuarioProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Muestra_PlazasPorUsuario.Assembly,
                    SoftvSettings.Settings.Muestra_PlazasPorUsuario.DataClass);
                    _Instance = (Muestra_PlazasPorUsuarioProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public Muestra_PlazasPorUsuarioProvider()
        {
        }
        /// <summary>
        /// Abstract method to add Muestra_PlazasPorUsuario
        ///  /summary>
        /// <param name="Muestra_PlazasPorUsuario"></param>
        /// <returns></returns>
        public abstract int AddMuestra_PlazasPorUsuario(Muestra_PlazasPorUsuarioEntity entity_Muestra_PlazasPorUsuario);

        /// <summary>
        /// Abstract method to delete Muestra_PlazasPorUsuario
        /// </summary>
        public abstract int DeleteMuestra_PlazasPorUsuario();

        /// <summary>
        /// Abstract method to update Muestra_PlazasPorUsuario
        /// </summary>
        public abstract int EditMuestra_PlazasPorUsuario(Muestra_PlazasPorUsuarioEntity entity_Muestra_PlazasPorUsuario);

        /// <summary>
        /// Abstract method to get all Muestra_PlazasPorUsuario
        /// </summary>
        public abstract List<Muestra_PlazasPorUsuarioEntity> GetMuestra_PlazasPorUsuario(int? Clv_Usuario);

        /// <summary>
        /// Abstract method to get all Muestra_PlazasPorUsuario List<int> lid
        /// </summary>
        public abstract List<Muestra_PlazasPorUsuarioEntity> GetMuestra_PlazasPorUsuario(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract Muestra_PlazasPorUsuarioEntity GetMuestra_PlazasPorUsuarioById();



        /// <summary>
        ///Get Muestra_PlazasPorUsuario
        ///</summary>
        public abstract SoftvList<Muestra_PlazasPorUsuarioEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get Muestra_PlazasPorUsuario
        ///</summary>
        public abstract SoftvList<Muestra_PlazasPorUsuarioEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual Muestra_PlazasPorUsuarioEntity GetMuestra_PlazasPorUsuarioFromReader(IDataReader reader)
        {
            Muestra_PlazasPorUsuarioEntity entity_Muestra_PlazasPorUsuario = null;
            try
            {
                entity_Muestra_PlazasPorUsuario = new Muestra_PlazasPorUsuarioEntity();
                entity_Muestra_PlazasPorUsuario.Clv_Plaza = (int?)(GetFromReader(reader, "Clv_Plaza"));
                entity_Muestra_PlazasPorUsuario.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Muestra_PlazasPorUsuario data to entity", ex);
            }
            return entity_Muestra_PlazasPorUsuario;
        }

    }

    #region Customs Methods

    #endregion
}


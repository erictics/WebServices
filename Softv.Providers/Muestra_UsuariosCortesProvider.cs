﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.Muestra_UsuariosCortesProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Muestra_UsuariosCortes Provider
    /// File                    : Muestra_UsuariosCortesProvider.cs
    /// Creation date           : 10/11/2016
    /// Creation time           : 06:19 p. m.
    /// </summary>
    public abstract class Muestra_UsuariosCortesProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Muestra_UsuariosCortes from DB
        /// </summary>
        private static Muestra_UsuariosCortesProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Muestra_UsuariosCortes instance
        /// </summary>
        public static Muestra_UsuariosCortesProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Muestra_UsuariosCortes.Assembly,
                    SoftvSettings.Settings.Muestra_UsuariosCortes.DataClass);
                    _Instance = (Muestra_UsuariosCortesProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public Muestra_UsuariosCortesProvider()
        {
        }
        /// <summary>
        /// Abstract method to add Muestra_UsuariosCortes
        ///  /summary>
        /// <param name="Muestra_UsuariosCortes"></param>
        /// <returns></returns>
        public abstract int AddMuestra_UsuariosCortes(Muestra_UsuariosCortesEntity entity_Muestra_UsuariosCortes);

        /// <summary>
        /// Abstract method to delete Muestra_UsuariosCortes
        /// </summary>
        public abstract int DeleteMuestra_UsuariosCortes();

        /// <summary>
        /// Abstract method to update Muestra_UsuariosCortes
        /// </summary>
        public abstract int EditMuestra_UsuariosCortes(Muestra_UsuariosCortesEntity entity_Muestra_UsuariosCortes);

        /// <summary>
        /// Abstract method to get all Muestra_UsuariosCortes
        /// </summary>
        public abstract List<Muestra_UsuariosCortesEntity> GetMuestra_UsuariosCortes(long? IdUsuario);

        /// <summary>
        /// Abstract method to get all Muestra_UsuariosCortes List<int> lid
        /// </summary>
        public abstract List<Muestra_UsuariosCortesEntity> GetMuestra_UsuariosCortes(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract Muestra_UsuariosCortesEntity GetMuestra_UsuariosCortesById();



        /// <summary>
        ///Get Muestra_UsuariosCortes
        ///</summary>
        public abstract SoftvList<Muestra_UsuariosCortesEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get Muestra_UsuariosCortes
        ///</summary>
        public abstract SoftvList<Muestra_UsuariosCortesEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual Muestra_UsuariosCortesEntity GetMuestra_UsuariosCortesFromReader(IDataReader reader)
        {
            Muestra_UsuariosCortesEntity entity_Muestra_UsuariosCortes = null;
            try
            {
                entity_Muestra_UsuariosCortes = new Muestra_UsuariosCortesEntity();
                entity_Muestra_UsuariosCortes.CLV_USUARIO = (String)(GetFromReader(reader, "CLV_USUARIO", IsString: true));
                entity_Muestra_UsuariosCortes.NOMBRE = (String)(GetFromReader(reader, "NOMBRE", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Muestra_UsuariosCortes data to entity", ex);
            }
            return entity_Muestra_UsuariosCortes;
        }

    }

    #region Customs Methods

    #endregion
}


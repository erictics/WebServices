﻿using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;


namespace Softv.Providers
{
    public abstract class NotaCreditoProvider : Globals.DataAccess
    {
        private static NotaCreditoProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Municipio instance
        /// </summary>
        public static NotaCreditoProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.NotaCreditoElement.Assembly,
                    SoftvSettings.Settings.NotaCreditoElement.DataClass);
                    _Instance = (NotaCreditoProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public NotaCreditoProvider()
        {
        }

        public abstract List<NotaCreditoEntity> GetBUSCANOTASDECREDITO(int? op, int? Clv_NotadeCredito, string Fecha, string contrato, int? sucursal, string Nombre, int? Idcompania);

        public abstract List<StatusNotaEntity> GetStatusNotadeCredito();

        public abstract List<DetalleNotaEntity> GetDetalle_NotasdeCredito(long Factura, long? Clv_NotadeCredito);

        public abstract List<FacturaClienteEntity> GetDAME_FACTURASDECLIENTE(long? contrato, long clv_nota);

        public abstract NotaCreditoEntity GetConsulta_NotaCredito(long? Clv_NotadeCredito);

        public abstract DatosTicket GetObtieneDatosTicket(long? Clv_NotadeCredito);

        public abstract int? GetNueva_NotadeCredito(NotaCreditoEntity nota);

        public abstract int? GetGuarda_DetalleNota(long? Clv_Factura, long? Clv_Nota);

        public abstract string GetReportesNotasDeCredito(long? Clv_Nota);

        public abstract int? GetBorrar_Session_Notas(long Clv_factura, long? Clv_Nota);


        public abstract int? GetModifica_DetalleNotas(long? Clv_detalle, long? Clv_factura, bool? secobra, decimal? importe);

        public abstract long? GetCANCELACIONFACTURAS_Notas(long? Clv_Factura, int? op);

        public abstract decimal? GetDameMonto_NotadeCredito(long? Clv_Nota, long? contrato);


    }
}

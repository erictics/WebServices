﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.PlazaRepVariosProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : PlazaRepVarios Provider
    /// File                    : PlazaRepVariosProvider.cs
    /// Creation date           : 07/02/2017
    /// Creation time           : 06:18 p. m.
    /// </summary>
    public abstract class PlazaRepVariosProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of PlazaRepVarios from DB
        /// </summary>
        private static PlazaRepVariosProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a PlazaRepVarios instance
        /// </summary>
        public static PlazaRepVariosProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.PlazaRepVarios.Assembly,
                    SoftvSettings.Settings.PlazaRepVarios.DataClass);
                    _Instance = (PlazaRepVariosProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public PlazaRepVariosProvider()
        {
        }
        /// <summary>
        /// Abstract method to add PlazaRepVarios
        ///  /summary>
        /// <param name="PlazaRepVarios"></param>
        /// <returns></returns>
        public abstract int AddPlazaRepVarios(PlazaRepVariosEntity entity_PlazaRepVarios);

        /// <summary>
        /// Abstract method to delete PlazaRepVarios
        /// </summary>
        public abstract int DeletePlazaRepVarios();

        /// <summary>
        /// Abstract method to update PlazaRepVarios
        /// </summary>
        public abstract int EditPlazaRepVarios(PlazaRepVariosEntity entity_PlazaRepVarios);

        /// <summary>
        /// Abstract method to get all PlazaRepVarios
        /// </summary>
        public abstract List<PlazaRepVariosEntity> GetPlazaRepVarios();

        /// <summary>
        /// Abstract method to get all PlazaRepVarios List<int> lid
        /// </summary>
        public abstract List<PlazaRepVariosEntity> GetPlazaRepVarios(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract PlazaRepVariosEntity GetPlazaRepVariosById();


        /// <summary>
        /// Abstract method to Change State PlazaRepVarios
        /// </summary>
        //public abstract int ChangeStatePlazaRepVarios(,bool State );


        /// <summary>
        ///Get PlazaRepVarios
        ///</summary>
        public abstract SoftvList<PlazaRepVariosEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get PlazaRepVarios
        ///</summary>
        public abstract SoftvList<PlazaRepVariosEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual PlazaRepVariosEntity GetPlazaRepVariosFromReader(IDataReader reader)
        {
            PlazaRepVariosEntity entity_PlazaRepVarios = null;
            try
            {
                entity_PlazaRepVarios = new PlazaRepVariosEntity();
                entity_PlazaRepVarios.Clv_Plaza = (int?)(GetFromReader(reader, "Clv_Plaza"));
                entity_PlazaRepVarios.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));
                entity_PlazaRepVarios.RFC = (String)(GetFromReader(reader, "RFC", IsString: true));
                entity_PlazaRepVarios.Calle = (String)(GetFromReader(reader, "Calle", IsString: true));
                entity_PlazaRepVarios.NumEx = (String)(GetFromReader(reader, "NumEx", IsString: true));
                entity_PlazaRepVarios.NumIn = (String)(GetFromReader(reader, "NumIn", IsString: true));
                entity_PlazaRepVarios.Colonia = (String)(GetFromReader(reader, "Colonia", IsString: true));
                entity_PlazaRepVarios.CP = (String)(GetFromReader(reader, "CP", IsString: true));
                entity_PlazaRepVarios.Localidad = (String)(GetFromReader(reader, "Localidad", IsString: true));
                entity_PlazaRepVarios.Estado = (String)(GetFromReader(reader, "Estado", IsString: true));
                entity_PlazaRepVarios.EntreCalles = (String)(GetFromReader(reader, "EntreCalles", IsString: true));
                entity_PlazaRepVarios.Telefono = (String)(GetFromReader(reader, "Telefono", IsString: true));
                entity_PlazaRepVarios.Fax = (String)(GetFromReader(reader, "Fax", IsString: true));
                entity_PlazaRepVarios.Email = (String)(GetFromReader(reader, "Email", IsString: true));
                entity_PlazaRepVarios.Municipio = (String)(GetFromReader(reader, "Municipio", IsString: true));
                entity_PlazaRepVarios.Pais = (String)(GetFromReader(reader, "Pais", IsString: true));
                entity_PlazaRepVarios.lada1 = (String)(GetFromReader(reader, "lada1", IsString: true));
                entity_PlazaRepVarios.lada2 = (String)(GetFromReader(reader, "lada2", IsString: true));
                entity_PlazaRepVarios.Telefono2 = (String)(GetFromReader(reader, "Telefono2", IsString: true));
                entity_PlazaRepVarios.NombreContacto = (String)(GetFromReader(reader, "NombreContacto", IsString: true));
                entity_PlazaRepVarios.ImportePagare = (Decimal?)(GetFromReader(reader, "ImportePagare"));
                entity_PlazaRepVarios.TiposDistribuidor = (int?)(GetFromReader(reader, "TiposDistribuidor"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting PlazaRepVarios data to entity", ex);
            }
            return entity_PlazaRepVarios;
        }

    }

    #region Customs Methods

    #endregion
}


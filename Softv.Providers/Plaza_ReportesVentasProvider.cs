﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.Plaza_ReportesVentasProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Plaza_ReportesVentas Provider
    /// File                    : Plaza_ReportesVentasProvider.cs
    /// Creation date           : 26/07/2017
    /// Creation time           : 12:26 p. m.
    /// </summary>
    public abstract class Plaza_ReportesVentasProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Plaza_ReportesVentas from DB
        /// </summary>
        private static Plaza_ReportesVentasProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Plaza_ReportesVentas instance
        /// </summary>
        public static Plaza_ReportesVentasProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Plaza_ReportesVentas.Assembly,
                    SoftvSettings.Settings.Plaza_ReportesVentas.DataClass);
                    _Instance = (Plaza_ReportesVentasProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public Plaza_ReportesVentasProvider()
        {
        }
        /// <summary>
        /// Abstract method to add Plaza_ReportesVentas
        ///  /summary>
        /// <param name="Plaza_ReportesVentas"></param>
        /// <returns></returns>
        public abstract int AddPlaza_ReportesVentas(Plaza_ReportesVentasEntity entity_Plaza_ReportesVentas);

        /// <summary>
        /// Abstract method to delete Plaza_ReportesVentas
        /// </summary>
        public abstract int DeletePlaza_ReportesVentas();

        /// <summary>
        /// Abstract method to update Plaza_ReportesVentas
        /// </summary>
        public abstract int EditPlaza_ReportesVentas(Plaza_ReportesVentasEntity entity_Plaza_ReportesVentas);

        /// <summary>
        /// Abstract method to get all Plaza_ReportesVentas
        /// </summary>
        public abstract List<Plaza_ReportesVentasEntity> GetPlaza_ReportesVentas();

        public abstract List<Plaza_ReportesVentasEntity> GetPlaza_ReportesVentasXmlList(String xml);







        /// <summary>
        /// Abstract method to get all Plaza_ReportesVentas List<int> lid
        /// </summary>
        public abstract List<Plaza_ReportesVentasEntity> GetPlaza_ReportesVentas(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract Plaza_ReportesVentasEntity GetPlaza_ReportesVentasById();



        /// <summary>
        ///Get Plaza_ReportesVentas
        ///</summary>
        public abstract SoftvList<Plaza_ReportesVentasEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get Plaza_ReportesVentas
        ///</summary>
        public abstract SoftvList<Plaza_ReportesVentasEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual Plaza_ReportesVentasEntity GetPlaza_ReportesVentasFromReader(IDataReader reader)
        {
            Plaza_ReportesVentasEntity entity_Plaza_ReportesVentas = null;
            try
            {
                entity_Plaza_ReportesVentas = new Plaza_ReportesVentasEntity();
                entity_Plaza_ReportesVentas.id_compania = (int?)(GetFromReader(reader, "id_compania"));
                entity_Plaza_ReportesVentas.razon_social = (String)(GetFromReader(reader, "razon_social", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Plaza_ReportesVentas data to entity", ex);
            }
            return entity_Plaza_ReportesVentas;
        }

    }

    #region Customs Methods

    #endregion
}


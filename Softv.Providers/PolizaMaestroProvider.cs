﻿using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;
using System.IO;

namespace Softv.Providers
{
    public abstract class PolizaMaestroProvider : Globals.DataAccess
    {
        /// <summary>
        /// Instance of ContratoMaestroFac from DB
        /// </summary>
        private static PolizaMaestroProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ContratoMaestroFac instance
        /// </summary>
        public static PolizaMaestroProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.PolizaMaestro.Assembly,
                    SoftvSettings.Settings.PolizaMaestro.DataClass);
                    _Instance = (PolizaMaestroProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public PolizaMaestroProvider()
        {
        }

        /// <summary>
        /// Abstract method to Change State ContratoMaestroFac
        /// </summary>
        public abstract List<PolizasMaestroEntity> GetObtienePolizasMaestro(FiltroPolizaEntity filtros);

        public abstract int? GetEliminaPoliza(FiltroPolizaEntity filtros);

        public abstract List<DetallePolizaEntity> GetDetallesPolizaMaestro(FiltroPolizaEntity filtros);

        public abstract PolizasMaestroEntity GetGeneraNuevaPolizaMaestro(FiltroPolizaEntity filtros);

        public abstract string GetPolizaTxt(FiltroPolizaEntity filtros);

        /// <summary>
        /// Obtiene las polizas de maestro para llenar el browse
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected virtual List<PolizasMaestroEntity> GetObtienePolizasMaestro(IDataReader reader)
        {
            List<PolizasMaestroEntity> list_Polizas = null;
            try
            {
                list_Polizas = new List<PolizasMaestroEntity>();

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratoMaestroFac data to entity", ex);
            }
            return list_Polizas;
        }

        /// <summary>
        /// Elimina póliza
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected virtual int? GetEliminaPoliza(IDataReader reader)
        {
            List<PolizasMaestroEntity> list_Polizas = null;
            try
            {
                list_Polizas = new List<PolizasMaestroEntity>();

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratoMaestroFac data to entity", ex);
            }
            return 0;
        }

        /// <summary>
        /// Obtiene las polizas de maestro para llenar el browse
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected virtual List<DetallePolizaEntity> GetDetallesPolizaMaestro(IDataReader reader)
        {
            List<DetallePolizaEntity> Detalles = null;
            try
            {
                Detalles = new List<DetallePolizaEntity>();

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratoMaestroFac data to entity", ex);
            }
            return Detalles;
        }

        /// <summary>
        /// Genera una nueva póliza
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected virtual PolizasMaestroEntity GetGeneraNuevaPolizaMaestro(IDataReader reader)
        {
            PolizasMaestroEntity Poliza = null;
            try
            {
                Poliza = new PolizasMaestroEntity();

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratoMaestroFac data to entity", ex);
            }
            return Poliza;
        }

        /// <summary>
        /// Genera una nueva póliza
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected virtual string GetPolizaTxt(IDataReader reader)
        {
            string Poliza = null;
            try
            {
                
            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratoMaestroFac data to entity", ex);
            }
            return Poliza;
        }
        
        /// <summary>
        /// Mapea un reader a una entidad
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected virtual PolizasMaestroEntity GetObtienePolizasMaestroFromReader(IDataReader reader)
        {
            PolizasMaestroEntity entity_PolizasMaestro = null;
            try
            {
                entity_PolizasMaestro = new PolizasMaestroEntity();
                entity_PolizasMaestro.Clv_Plaza = (int?)(GetFromReader(reader, "Clv_Plaza"));
                entity_PolizasMaestro.FechaGeneracion = (String)(GetFromReader(reader, "FechaGeneracion", IsString: true));
                entity_PolizasMaestro.FechaPoliza = (String)(GetFromReader(reader, "FechaPoliza", IsString: true));
                entity_PolizasMaestro.Plaza = (String)(GetFromReader(reader, "Plaza", IsString: true));
                entity_PolizasMaestro.Clv_Poliza = (long?)(GetFromReader(reader, "Clv_Poliza"));
                entity_PolizasMaestro.Dolares = (String)(GetFromReader(reader, "Dolares", IsString: true));
            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratoMaestroFac data to entity", ex);
            }
            return entity_PolizasMaestro;
        }
    }
}

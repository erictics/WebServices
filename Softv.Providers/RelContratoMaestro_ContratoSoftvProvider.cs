﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.RelContratoMaestro_ContratoSoftvProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelContratoMaestro_ContratoSoftv Provider
    /// File                    : RelContratoMaestro_ContratoSoftvProvider.cs
    /// Creation date           : 23/02/2017
    /// Creation time           : 11:27 a. m.
    /// </summary>
    public abstract class RelContratoMaestro_ContratoSoftvProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of RelContratoMaestro_ContratoSoftv from DB
        /// </summary>
        private static RelContratoMaestro_ContratoSoftvProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a RelContratoMaestro_ContratoSoftv instance
        /// </summary>
        public static RelContratoMaestro_ContratoSoftvProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.RelContratoMaestro_ContratoSoftv.Assembly,
                    SoftvSettings.Settings.RelContratoMaestro_ContratoSoftv.DataClass);
                    _Instance = (RelContratoMaestro_ContratoSoftvProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public RelContratoMaestro_ContratoSoftvProvider()
        {
        }
        /// <summary>
        /// Abstract method to add RelContratoMaestro_ContratoSoftv
        ///  /summary>
        /// <param name="RelContratoMaestro_ContratoSoftv"></param>
        /// <returns></returns>
        public abstract int AddRelContratoMaestro_ContratoSoftv(RelContratoMaestro_ContratoSoftvEntity entity_RelContratoMaestro_ContratoSoftv);

        /// <summary>
        /// Abstract method to delete RelContratoMaestro_ContratoSoftv
        /// </summary>
        public abstract int DeleteRelContratoMaestro_ContratoSoftv();

        /// <summary>
        /// Abstract method to update RelContratoMaestro_ContratoSoftv
        /// </summary>
        public abstract int EditRelContratoMaestro_ContratoSoftv(RelContratoMaestro_ContratoSoftvEntity entity_RelContratoMaestro_ContratoSoftv);

        /// <summary>
        /// Abstract method to get all RelContratoMaestro_ContratoSoftv
        /// </summary>
        public abstract List<RelContratoMaestro_ContratoSoftvEntity> GetRelContratoMaestro_ContratoSoftv();



    


        /// <summary>
        /// Abstract method to get all RelContratoMaestro_ContratoSoftv List<int> lid
        /// </summary>
        public abstract List<RelContratoMaestro_ContratoSoftvEntity> GetRelContratoMaestro_ContratoSoftv(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract RelContratoMaestro_ContratoSoftvEntity GetRelContratoMaestro_ContratoSoftvById();



        /// <summary>
        ///Get RelContratoMaestro_ContratoSoftv
        ///</summary>
        public abstract SoftvList<RelContratoMaestro_ContratoSoftvEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get RelContratoMaestro_ContratoSoftv
        ///</summary>
        public abstract SoftvList<RelContratoMaestro_ContratoSoftvEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual RelContratoMaestro_ContratoSoftvEntity GetRelContratoMaestro_ContratoSoftvFromReader(IDataReader reader)
        {
            RelContratoMaestro_ContratoSoftvEntity entity_RelContratoMaestro_ContratoSoftv = null;
            try
            {
                entity_RelContratoMaestro_ContratoSoftv = new RelContratoMaestro_ContratoSoftvEntity();
                entity_RelContratoMaestro_ContratoSoftv.IdContratoMaestro = (long?)(GetFromReader(reader, "IdContratoMaestro"));
                entity_RelContratoMaestro_ContratoSoftv.Contrato = (long?)(GetFromReader(reader, "Contrato"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting RelContratoMaestro_ContratoSoftv data to entity", ex);
            }
            return entity_RelContratoMaestro_ContratoSoftv;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.RelRedMedioProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelRedMedio Provider
    /// File                    : RelRedMedioProvider.cs
    /// Creation date           : 10/11/2017
    /// Creation time           : 06:44 p. m.
    /// </summary>
    public abstract class RelRedMedioProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of RelRedMedio from DB
        /// </summary>
        private static RelRedMedioProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a RelRedMedio instance
        /// </summary>
        public static RelRedMedioProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.RelRedMedio.Assembly,
                    SoftvSettings.Settings.RelRedMedio.DataClass);
                    _Instance = (RelRedMedioProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public RelRedMedioProvider()
        {
        }
        /// <summary>
        /// Abstract method to add RelRedMedio
        ///  /summary>
        /// <param name="RelRedMedio"></param>
        /// <returns></returns>
        public abstract int AddRelRedMedio(RelRedMedioEntity entity_RelRedMedio);

        /// <summary>
        /// Abstract method to delete RelRedMedio
        /// </summary>
        public abstract int DeleteRelRedMedio();

        /// <summary>
        /// Abstract method to update RelRedMedio
        /// </summary>
        public abstract int EditRelRedMedio(RelRedMedioEntity entity_RelRedMedio);

        /// <summary>
        /// Abstract method to get all RelRedMedio
        /// </summary>
        public abstract List<RelRedMedioEntity> GetRelRedMedio(int? IdRed);

        public abstract List<RelRedMedioEntity> GetRelRedMedio_Inc(int? IdRed);

        /// <summary>
        /// Abstract method to get all RelRedMedio List<int> lid
        /// </summary>
        public abstract List<RelRedMedioEntity> GetRelRedMedio(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract RelRedMedioEntity GetRelRedMedioById();



        /// <summary>
        ///Get RelRedMedio
        ///</summary>
        public abstract SoftvList<RelRedMedioEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get RelRedMedio
        ///</summary>
        public abstract SoftvList<RelRedMedioEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual RelRedMedioEntity GetRelRedMedioFromReader(IDataReader reader)
        {
            RelRedMedioEntity entity_RelRedMedio = null;
            try
            {
                entity_RelRedMedio = new RelRedMedioEntity();
                entity_RelRedMedio.IdRed = (int?)(GetFromReader(reader, "IdRed"));
                entity_RelRedMedio.IdMedio = (int?)(GetFromReader(reader, "IdMedio"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting RelRedMedio data to entity", ex);
            }
            return entity_RelRedMedio;
        }

    }

    #region Customs Methods

    #endregion
}


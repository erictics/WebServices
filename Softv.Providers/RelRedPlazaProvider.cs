﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.RelRedPlazaProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelRedPlaza Provider
    /// File                    : RelRedPlazaProvider.cs
    /// Creation date           : 10/11/2017
    /// Creation time           : 12:27 p. m.
    /// </summary>
    public abstract class RelRedPlazaProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of RelRedPlaza from DB
        /// </summary>
        private static RelRedPlazaProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a RelRedPlaza instance
        /// </summary>
        public static RelRedPlazaProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.RelRedPlaza.Assembly,
                    SoftvSettings.Settings.RelRedPlaza.DataClass);
                    _Instance = (RelRedPlazaProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public RelRedPlazaProvider()
        {
        }
        /// <summary>
        /// Abstract method to add RelRedPlaza
        ///  /summary>
        /// <param name="RelRedPlaza"></param>
        /// <returns></returns>
        public abstract int AddRelRedPlaza(RelRedPlazaEntity entity_RelRedPlaza);

        /// <summary>
        /// Abstract method to delete RelRedPlaza
        /// </summary>
        public abstract int DeleteRelRedPlaza();

        /// <summary>
        /// Abstract method to update RelRedPlaza
        /// </summary>
        public abstract int EditRelRedPlaza(RelRedPlazaEntity entity_RelRedPlaza);

        /// <summary>
        /// Abstract method to get all RelRedPlaza
        /// </summary>
        public abstract List<RelRedPlazaEntity> GetRelRedPlaza(long? IdRed);

        public abstract List<RelRedPlazaEntity> GetRelRedPlaza_Inc(long? IdRed);

        /// <summary>
        /// Abstract method to get all RelRedPlaza List<int> lid
        /// </summary>
        public abstract List<RelRedPlazaEntity> GetRelRedPlaza(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract RelRedPlazaEntity GetRelRedPlazaById();



        /// <summary>
        ///Get RelRedPlaza
        ///</summary>
        public abstract SoftvList<RelRedPlazaEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get RelRedPlaza
        ///</summary>
        public abstract SoftvList<RelRedPlazaEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual RelRedPlazaEntity GetRelRedPlazaFromReader(IDataReader reader)
        {
            RelRedPlazaEntity entity_RelRedPlaza = null;
            try
            {
                entity_RelRedPlaza = new RelRedPlazaEntity();
                entity_RelRedPlaza.IdRed = (long?)(GetFromReader(reader, "IdRed"));
                entity_RelRedPlaza.Clv_Plaza = (int?)(GetFromReader(reader, "Clv_Plaza"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting RelRedPlaza data to entity", ex);
            }
            return entity_RelRedPlaza;
        }


        protected virtual RelRedPlazaEntity GetRelRedPlaza_dosFromReader(IDataReader reader)
        {
            RelRedPlazaEntity entity_RelRedPlaza = null;
            try
            {
                entity_RelRedPlaza = new RelRedPlazaEntity();
                entity_RelRedPlaza.Clv_Plaza = (int?)(GetFromReader(reader, "Clv_Plaza"));
                entity_RelRedPlaza.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting RelRedPlaza data to entity", ex);
            }
            return entity_RelRedPlaza;
        }


        protected virtual RelRedPlazaEntity GetRelRedPlaza_tresFromReader(IDataReader reader)
        {
            RelRedPlazaEntity entity_RelRedPlaza = null;
            try
            {
                entity_RelRedPlaza = new RelRedPlazaEntity();
                entity_RelRedPlaza.IdRed = (long?)(GetFromReader(reader, "IdRed"));
                entity_RelRedPlaza.ClvPlaza = (long?)(GetFromReader(reader, "Clv_Plaza"));
                entity_RelRedPlaza.Nombre = (String)(GetFromReader(reader, "Nombre", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting RelRedPlaza data to entity", ex);
            }
            return entity_RelRedPlaza;
        }


    }

    #region Customs Methods

    #endregion
}


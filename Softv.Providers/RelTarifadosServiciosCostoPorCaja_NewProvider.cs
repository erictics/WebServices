﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.RelTarifadosServiciosCostoPorCaja_NewProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelTarifadosServiciosCostoPorCaja_New Provider
    /// File                    : RelTarifadosServiciosCostoPorCaja_NewProvider.cs
    /// Creation date           : 19/09/2017
    /// Creation time           : 06:17 p. m.
    /// </summary>
    public abstract class RelTarifadosServiciosCostoPorCaja_NewProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of RelTarifadosServiciosCostoPorCaja_New from DB
        /// </summary>
        private static RelTarifadosServiciosCostoPorCaja_NewProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a RelTarifadosServiciosCostoPorCaja_New instance
        /// </summary>
        public static RelTarifadosServiciosCostoPorCaja_NewProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.RelTarifadosServiciosCostoPorCaja_New.Assembly,
                    SoftvSettings.Settings.RelTarifadosServiciosCostoPorCaja_New.DataClass);
                    _Instance = (RelTarifadosServiciosCostoPorCaja_NewProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public RelTarifadosServiciosCostoPorCaja_NewProvider()
        {
        }
        /// <summary>
        /// Abstract method to add RelTarifadosServiciosCostoPorCaja_New
        ///  /summary>
        /// <param name="RelTarifadosServiciosCostoPorCaja_New"></param>
        /// <returns></returns>
        public abstract int AddRelTarifadosServiciosCostoPorCaja_New(RelTarifadosServiciosCostoPorCaja_NewEntity entity_RelTarifadosServiciosCostoPorCaja_New);

        /// <summary>
        /// Abstract method to delete RelTarifadosServiciosCostoPorCaja_New
        /// </summary>
        public abstract int DeleteRelTarifadosServiciosCostoPorCaja_New();

        /// <summary>
        /// Abstract method to update RelTarifadosServiciosCostoPorCaja_New
        /// </summary>
        public abstract int EditRelTarifadosServiciosCostoPorCaja_New(RelTarifadosServiciosCostoPorCaja_NewEntity entity_RelTarifadosServiciosCostoPorCaja_New);

        /// <summary>
        /// Abstract method to get all RelTarifadosServiciosCostoPorCaja_New
        /// </summary>
        public abstract List<RelTarifadosServiciosCostoPorCaja_NewEntity> GetRelTarifadosServiciosCostoPorCaja_New();

        /// <summary>
        /// Abstract method to get all RelTarifadosServiciosCostoPorCaja_New List<int> lid
        /// </summary>
        public abstract List<RelTarifadosServiciosCostoPorCaja_NewEntity> GetRelTarifadosServiciosCostoPorCaja_New(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract RelTarifadosServiciosCostoPorCaja_NewEntity GetRelTarifadosServiciosCostoPorCaja_NewById(long? Clv_Llave);



        /// <summary>
        ///Get RelTarifadosServiciosCostoPorCaja_New
        ///</summary>
        public abstract SoftvList<RelTarifadosServiciosCostoPorCaja_NewEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get RelTarifadosServiciosCostoPorCaja_New
        ///</summary>
        public abstract SoftvList<RelTarifadosServiciosCostoPorCaja_NewEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual RelTarifadosServiciosCostoPorCaja_NewEntity GetRelTarifadosServiciosCostoPorCaja_NewFromReader(IDataReader reader)
        {
            RelTarifadosServiciosCostoPorCaja_NewEntity entity_RelTarifadosServiciosCostoPorCaja_New = null;
            try
            {
                entity_RelTarifadosServiciosCostoPorCaja_New = new RelTarifadosServiciosCostoPorCaja_NewEntity();
                entity_RelTarifadosServiciosCostoPorCaja_New.CostoPrincipal = (Decimal?)(GetFromReader(reader, "CostoPrincipal"));
                entity_RelTarifadosServiciosCostoPorCaja_New.Costo1ra = (Decimal?)(GetFromReader(reader, "Costo1ra"));
                entity_RelTarifadosServiciosCostoPorCaja_New.Costo2da = (Decimal?)(GetFromReader(reader, "Costo2da"));
                entity_RelTarifadosServiciosCostoPorCaja_New.Costo3ra = (Decimal?)(GetFromReader(reader, "Costo3ra"));
                entity_RelTarifadosServiciosCostoPorCaja_New.CostoPrincipal2 = (Decimal?)(GetFromReader(reader, "CostoPrincipal2"));
                entity_RelTarifadosServiciosCostoPorCaja_New.Costo1ra2 = (Decimal?)(GetFromReader(reader, "Costo1ra2"));
                entity_RelTarifadosServiciosCostoPorCaja_New.Costo2da2 = (Decimal?)(GetFromReader(reader, "Costo2da2"));
                entity_RelTarifadosServiciosCostoPorCaja_New.Costo3ra2 = (Decimal?)(GetFromReader(reader, "Costo3ra2"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting RelTarifadosServiciosCostoPorCaja_New data to entity", ex);
            }
            return entity_RelTarifadosServiciosCostoPorCaja_New;
        }

    }

    #region Customs Methods

    #endregion
}


﻿using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;
using System.IO;

namespace Softv.Providers
{
    public abstract class RelacionIngresosMaestroProvider : Globals.DataAccess
    {
        /// <summary>
        /// Instance of ContratoMaestroFac from DB
        /// </summary>
        private static RelacionIngresosMaestroProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ContratoMaestroFac instance
        /// </summary>
        public static RelacionIngresosMaestroProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.RelacionIngresosMaestro.Assembly,
                    SoftvSettings.Settings.RelacionIngresosMaestro.DataClass);
                    _Instance = (RelacionIngresosMaestroProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public RelacionIngresosMaestroProvider()
        {
        }

        public abstract string GetRelacionIngresosMaestro(List<int> Distribuidores, string FechaInicial, string FechaFinal, int Dolares);

        protected virtual string GetRelacionIngresosMaestro(IDataReader reader)
        {
            string list_Relacion = null;
            try
            {
                list_Relacion = "";

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ContratoMaestroFac data to entity", ex);
            }
            return list_Relacion;
        }
    }
}

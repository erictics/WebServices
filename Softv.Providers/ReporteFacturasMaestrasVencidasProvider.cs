﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ReporteFacturasMaestrasVencidasProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ReporteFacturasMaestrasVencidas Provider
    /// File                    : ReporteFacturasMaestrasVencidasProvider.cs
    /// Creation date           : 24/07/2017
    /// Creation time           : 10:34 a. m.
    /// </summary>
    public abstract class ReporteFacturasMaestrasVencidasProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ReporteFacturasMaestrasVencidas from DB
        /// </summary>
        private static ReporteFacturasMaestrasVencidasProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ReporteFacturasMaestrasVencidas instance
        /// </summary>
        public static ReporteFacturasMaestrasVencidasProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ReporteFacturasMaestrasVencidas.Assembly,
                    SoftvSettings.Settings.ReporteFacturasMaestrasVencidas.DataClass);
                    _Instance = (ReporteFacturasMaestrasVencidasProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ReporteFacturasMaestrasVencidasProvider()
        {
        }
        /// <summary>
        /// Abstract method to add ReporteFacturasMaestrasVencidas
        ///  /summary>
        /// <param name="ReporteFacturasMaestrasVencidas"></param>
        /// <returns></returns>
        public abstract int AddReporteFacturasMaestrasVencidas(ReporteFacturasMaestrasVencidasEntity entity_ReporteFacturasMaestrasVencidas);

        /// <summary>
        /// Abstract method to delete ReporteFacturasMaestrasVencidas
        /// </summary>
        public abstract int DeleteReporteFacturasMaestrasVencidas();

        /// <summary>
        /// Abstract method to update ReporteFacturasMaestrasVencidas
        /// </summary>
        public abstract int EditReporteFacturasMaestrasVencidas(ReporteFacturasMaestrasVencidasEntity entity_ReporteFacturasMaestrasVencidas);

        /// <summary>
        /// Abstract method to get all ReporteFacturasMaestrasVencidas
        /// </summary>
        public abstract List<ReporteFacturasMaestrasVencidasEntity> GetReporteFacturasMaestrasVencidas(String Fecha, int? Vencidas, int? PorVencer);

        /// <summary>
        /// Abstract method to get all ReporteFacturasMaestrasVencidas List<int> lid
        /// </summary>
        public abstract List<ReporteFacturasMaestrasVencidasEntity> GetReporteFacturasMaestrasVencidas(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ReporteFacturasMaestrasVencidasEntity GetReporteFacturasMaestrasVencidasById();



        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas
        ///</summary>
        public abstract SoftvList<ReporteFacturasMaestrasVencidasEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get ReporteFacturasMaestrasVencidas
        ///</summary>
        public abstract SoftvList<ReporteFacturasMaestrasVencidasEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ReporteFacturasMaestrasVencidasEntity GetReporteFacturasMaestrasVencidasFromReader(IDataReader reader)
        {
            ReporteFacturasMaestrasVencidasEntity entity_ReporteFacturasMaestrasVencidas = null;
            try
            {
                entity_ReporteFacturasMaestrasVencidas = new ReporteFacturasMaestrasVencidasEntity();
                entity_ReporteFacturasMaestrasVencidas.Contacto = (String)(GetFromReader(reader, "Contacto", IsString: true));
                entity_ReporteFacturasMaestrasVencidas.Telefono = (String)(GetFromReader(reader, "Telefono", IsString: true));
                entity_ReporteFacturasMaestrasVencidas.SaldoVencido = (Decimal?)(GetFromReader(reader, "SaldoVencido"));
                try
                {
                   DateTime fecha= (DateTime)(GetFromReader(reader, "FechaVencimiento"));
                    entity_ReporteFacturasMaestrasVencidas.FechaVencimiento = fecha.ToShortDateString();

                }
                catch
                {}
                try
                {
                    DateTime fecha= (DateTime)(GetFromReader(reader, "FechaEmision"));
                    entity_ReporteFacturasMaestrasVencidas.FechaEmision = fecha.ToShortDateString();
                }
                catch
                { }
                
                
                entity_ReporteFacturasMaestrasVencidas.DiasCredito = (int?)(GetFromReader(reader, "DiasCredito"));
                entity_ReporteFacturasMaestrasVencidas.ImporteFactura = (Decimal?)(GetFromReader(reader, "ImporteFactura"));
                entity_ReporteFacturasMaestrasVencidas.TotalAbonado = (Decimal?)(GetFromReader(reader, "TotalAbonado"));
                //entity_ReporteFacturasMaestrasVencidas.Fecha = (String)(GetFromReader(reader, "Fecha", IsString: true));
                //entity_ReporteFacturasMaestrasVencidas.Vencidas = (int?)(GetFromReader(reader, "Vencidas"));
                //entity_ReporteFacturasMaestrasVencidas.PorVencer = (int?)(GetFromReader(reader, "PorVencer"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ReporteFacturasMaestrasVencidas data to entity", ex);
            }
            return entity_ReporteFacturasMaestrasVencidas;
        }

    }

    #region Customs Methods

    #endregion
}


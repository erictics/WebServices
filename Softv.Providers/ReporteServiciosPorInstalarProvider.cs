﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ReporteServiciosPorInstalarProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ReporteServiciosPorInstalar Provider
    /// File                    : ReporteServiciosPorInstalarProvider.cs
    /// Creation date           : 24/07/2017
    /// Creation time           : 11:12 a. m.
    /// </summary>
    public abstract class ReporteServiciosPorInstalarProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ReporteServiciosPorInstalar from DB
        /// </summary>
        private static ReporteServiciosPorInstalarProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ReporteServiciosPorInstalar instance
        /// </summary>
        public static ReporteServiciosPorInstalarProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ReporteServiciosPorInstalar.Assembly,
                    SoftvSettings.Settings.ReporteServiciosPorInstalar.DataClass);
                    _Instance = (ReporteServiciosPorInstalarProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ReporteServiciosPorInstalarProvider()
        {
        }
        /// <summary>
        /// Abstract method to add ReporteServiciosPorInstalar
        ///  /summary>
        /// <param name="ReporteServiciosPorInstalar"></param>
        /// <returns></returns>
        public abstract int AddReporteServiciosPorInstalar(ReporteServiciosPorInstalarEntity entity_ReporteServiciosPorInstalar);

        /// <summary>
        /// Abstract method to delete ReporteServiciosPorInstalar
        /// </summary>
        public abstract int DeleteReporteServiciosPorInstalar();

        /// <summary>
        /// Abstract method to update ReporteServiciosPorInstalar
        /// </summary>
        public abstract int EditReporteServiciosPorInstalar(ReporteServiciosPorInstalarEntity entity_ReporteServiciosPorInstalar);

        /// <summary>
        /// Abstract method to get all ReporteServiciosPorInstalar
        /// </summary>
        public abstract RepServXInstEntity GetReporteServiciosPorInstalar(long? ContratoMaestro);

        /// <summary>
        /// Abstract method to get all ReporteServiciosPorInstalar List<int> lid
        /// </summary>
        public abstract List<ReporteServiciosPorInstalarEntity> GetReporteServiciosPorInstalar(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ReporteServiciosPorInstalarEntity GetReporteServiciosPorInstalarById();



        /// <summary>
        ///Get ReporteServiciosPorInstalar
        ///</summary>
        public abstract SoftvList<ReporteServiciosPorInstalarEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get ReporteServiciosPorInstalar
        ///</summary>
        public abstract SoftvList<ReporteServiciosPorInstalarEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ReporteServiciosPorInstalarEntity GetReporteServiciosPorInstalarFromReader(IDataReader reader)
        {
            ReporteServiciosPorInstalarEntity entity_ReporteServiciosPorInstalar = null;
            try
            {
                entity_ReporteServiciosPorInstalar = new ReporteServiciosPorInstalarEntity();
                entity_ReporteServiciosPorInstalar.Contrato = (String)(GetFromReader(reader, "Contrato", IsString: true));
                entity_ReporteServiciosPorInstalar.Servicio = (String)(GetFromReader(reader, "Servicio", IsString: true));
                entity_ReporteServiciosPorInstalar.Direccion = (String)(GetFromReader(reader, "Direccion", IsString: true));
                entity_ReporteServiciosPorInstalar.OrdenGenerada = (String)(GetFromReader(reader, "OrdenGenerada", IsString: true));
                entity_ReporteServiciosPorInstalar.FechaOrden = (String)(GetFromReader(reader, "FechaOrden", IsString: true));
                entity_ReporteServiciosPorInstalar.Clv_Orden = (long?)(GetFromReader(reader, "Clv_Orden"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ReporteServiciosPorInstalar data to entity", ex);
            }
            return entity_ReporteServiciosPorInstalar;
        }



        protected virtual ReporteServiciosPorInstalar_TempEntity GetReporteServiciosPorInstalar_TempFromReader(IDataReader reader)
        {
            ReporteServiciosPorInstalar_TempEntity entity_ReporteServiciosPorInstalar_Temp = null;
            try
            {
                entity_ReporteServiciosPorInstalar_Temp = new ReporteServiciosPorInstalar_TempEntity();
                entity_ReporteServiciosPorInstalar_Temp.Status = (String)(GetFromReader(reader, "Status", IsString: true));
                entity_ReporteServiciosPorInstalar_Temp.Total = (int?)(GetFromReader(reader, "Total"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ReporteServiciosPorInstalar_Temp data to entity", ex);
            }
            return entity_ReporteServiciosPorInstalar_Temp;
        }










    }

    #region Customs Methods

    #endregion
}


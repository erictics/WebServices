﻿
    using System;
    using System.Text;
    using System.Data;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Runtime.Remoting;
    using Softv.Entities;
    using SoftvConfiguration;
    using Globals;

    namespace Softv.Providers
    {
    /// <summary>
    /// Class                   : Softv.Providers.ServiciosInternetProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ServiciosInternet Provider
    /// File                    : ServiciosInternetProvider.cs
    /// Creation date           : 07/02/2017
    /// Creation time           : 06:29 p. m.
    /// </summary>
    public abstract class ServiciosInternetProvider : Globals.DataAccess
    {

    /// <summary>
    /// Instance of ServiciosInternet from DB
    /// </summary>
    private static ServiciosInternetProvider _Instance = null;

    private static ObjectHandle obj;
    /// <summary>
    /// Generates a ServiciosInternet instance
    /// </summary>
    public static ServiciosInternetProvider Instance
    {
    get
    {
    if (_Instance == null)
    {
    obj = Activator.CreateInstance(
    SoftvSettings.Settings.ServiciosInternet.Assembly,
    SoftvSettings.Settings.ServiciosInternet.DataClass);
    _Instance = (ServiciosInternetProvider)obj.Unwrap();
    }
    return _Instance;
    }
    }

    /// <summary>
    /// Provider's default constructor
    /// </summary>
    public ServiciosInternetProvider()
    {
    }
    /// <summary>
    /// Abstract method to add ServiciosInternet
    ///  /summary>
    /// <param name="ServiciosInternet"></param>
    /// <returns></returns>
    public abstract int AddServiciosInternet(ServiciosInternetEntity entity_ServiciosInternet);

    /// <summary>
    /// Abstract method to delete ServiciosInternet
    /// </summary>
    public abstract int DeleteServiciosInternet();

    /// <summary>
    /// Abstract method to update ServiciosInternet
    /// </summary>
    public abstract int EditServiciosInternet(ServiciosInternetEntity entity_ServiciosInternet);

    /// <summary>
    /// Abstract method to get all ServiciosInternet
    /// </summary>
    public abstract List<ServiciosInternetEntity> GetServiciosInternet();

    /// <summary>
    /// Abstract method to get all ServiciosInternet List<int> lid
    /// </summary>
    public abstract List<ServiciosInternetEntity> GetServiciosInternet(List<int> lid);

    /// <summary>
    /// Abstract method to get by id
    /// </summary>
    public abstract ServiciosInternetEntity GetServiciosInternetById();

    

    /// <summary>
    ///Get ServiciosInternet
    ///</summary>
    public abstract SoftvList<ServiciosInternetEntity> GetPagedList(int pageIndex, int pageSize);

    /// <summary>
    ///Get ServiciosInternet
    ///</summary>
    public abstract SoftvList<ServiciosInternetEntity> GetPagedList(int pageIndex, int pageSize,String xml);

    /// <summary>
    /// Converts data from reader to entity
    /// </summary>
    protected virtual ServiciosInternetEntity GetServiciosInternetFromReader(IDataReader reader)
    {
    ServiciosInternetEntity entity_ServiciosInternet = null;
    try
    {
      entity_ServiciosInternet = new ServiciosInternetEntity();
    entity_ServiciosInternet.Clv_Servicio = (int?)(GetFromReader(reader,"Clv_Servicio"));
          entity_ServiciosInternet.Descripcion = (String)(GetFromReader(reader,"Descripcion", IsString : true));
        
    }
    catch (Exception ex)
    {
    throw new Exception("Error converting ServiciosInternet data to entity", ex);
    }
    return entity_ServiciosInternet;
    }
    
    }

    #region Customs Methods
    
    #endregion
    }

  
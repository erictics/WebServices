﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.Sp_DameDetalleFacturaMaestraProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Sp_DameDetalleFacturaMaestra Provider
    /// File                    : Sp_DameDetalleFacturaMaestraProvider.cs
    /// Creation date           : 19/05/2017
    /// Creation time           : 10:05 a. m.
    /// </summary>
    public abstract class Sp_DameDetalleFacturaMaestraProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Sp_DameDetalleFacturaMaestra from DB
        /// </summary>
        private static Sp_DameDetalleFacturaMaestraProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Sp_DameDetalleFacturaMaestra instance
        /// </summary>
        public static Sp_DameDetalleFacturaMaestraProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Sp_DameDetalleFacturaMaestra.Assembly,
                    SoftvSettings.Settings.Sp_DameDetalleFacturaMaestra.DataClass);
                    _Instance = (Sp_DameDetalleFacturaMaestraProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public Sp_DameDetalleFacturaMaestraProvider()
        {
        }
        /// <summary>
        /// Abstract method to add Sp_DameDetalleFacturaMaestra
        ///  /summary>
        /// <param name="Sp_DameDetalleFacturaMaestra"></param>
        /// <returns></returns>
        public abstract int AddSp_DameDetalleFacturaMaestra(Sp_DameDetalleFacturaMaestraEntity entity_Sp_DameDetalleFacturaMaestra);

        /// <summary>
        /// Abstract method to delete Sp_DameDetalleFacturaMaestra
        /// </summary>
        public abstract int DeleteSp_DameDetalleFacturaMaestra();

        /// <summary>
        /// Abstract method to update Sp_DameDetalleFacturaMaestra
        /// </summary>
        public abstract int EditSp_DameDetalleFacturaMaestra(Sp_DameDetalleFacturaMaestraEntity entity_Sp_DameDetalleFacturaMaestra);

        /// <summary>
        /// Abstract method to get all Sp_DameDetalleFacturaMaestra
        /// </summary>
        public abstract List<Sp_DameDetalleFacturaMaestraEntity> GetSp_DameDetalleFacturaMaestra(long? ClvSessionPadre);

        /// <summary>
        /// Abstract method to get all Sp_DameDetalleFacturaMaestra List<int> lid
        /// </summary>
        public abstract List<Sp_DameDetalleFacturaMaestraEntity> GetSp_DameDetalleFacturaMaestra(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract Sp_DameDetalleFacturaMaestraEntity GetSp_DameDetalleFacturaMaestraById();



        /// <summary>
        ///Get Sp_DameDetalleFacturaMaestra
        ///</summary>
        public abstract SoftvList<Sp_DameDetalleFacturaMaestraEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get Sp_DameDetalleFacturaMaestra
        ///</summary>
        public abstract SoftvList<Sp_DameDetalleFacturaMaestraEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual Sp_DameDetalleFacturaMaestraEntity GetSp_DameDetalleFacturaMaestraFromReader(IDataReader reader)
        {
            Sp_DameDetalleFacturaMaestraEntity entity_Sp_DameDetalleFacturaMaestra = null;
            try
            {
                entity_Sp_DameDetalleFacturaMaestra = new Sp_DameDetalleFacturaMaestraEntity();
                entity_Sp_DameDetalleFacturaMaestra.Descripcion = (String)(GetFromReader(reader, "Descripcion", IsString: true));
                entity_Sp_DameDetalleFacturaMaestra.Importe = (Decimal?)(GetFromReader(reader, "Importe"));
                entity_Sp_DameDetalleFacturaMaestra.PuntosAplicadosPagoAdelantado = (int?)(GetFromReader(reader, "PuntosAplicadosPagoAdelantado"));
                entity_Sp_DameDetalleFacturaMaestra.PuntosAplicadosOtros = (int?)(GetFromReader(reader, "PuntosAplicadosOtros"));
                entity_Sp_DameDetalleFacturaMaestra.DescuentoNet = (Decimal?)(GetFromReader(reader, "DescuentoNet"));
                entity_Sp_DameDetalleFacturaMaestra.importeBonifica = (Decimal?)(GetFromReader(reader, "importeBonifica"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Sp_DameDetalleFacturaMaestra data to entity", ex);
            }
            return entity_Sp_DameDetalleFacturaMaestra;
        }

    }

    #region Customs Methods

    #endregion
}


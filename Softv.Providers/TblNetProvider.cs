﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.TblNetProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : TblNet Provider
    /// File                    : TblNetProvider.cs
    /// Creation date           : 31/10/2017
    /// Creation time           : 05:47 p. m.
    /// </summary>
    public abstract class TblNetProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of TblNet from DB
        /// </summary>
        private static TblNetProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a TblNet instance
        /// </summary>
        public static TblNetProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.TblNet.Assembly,
                    SoftvSettings.Settings.TblNet.DataClass);
                    _Instance = (TblNetProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public TblNetProvider()
        {
        }

        public abstract List<TblNetEntity> GetServicioClvEqMedioList(int? ClvServicio);

        public abstract TblNetEntity GetAddServicioClvEqMedio(List<TblNetEntity> ObjClvEquivalente);

        public abstract TblNetEntity GetUpdateServicioClvEqMedio(List<TblNetEntity> ObjClvEquivalente);

    }

    #region Customs Methods

    #endregion
}
﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.UspChecaSiTieneExtensionesProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : UspChecaSiTieneExtensiones Provider
    /// File                    : UspChecaSiTieneExtensionesProvider.cs
    /// Creation date           : 28/07/2018
    /// Creation time           : 11:52 a. m.
    /// </summary>
    public abstract class UspChecaSiTieneExtensionesProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of UspChecaSiTieneExtensiones from DB
        /// </summary>
        private static UspChecaSiTieneExtensionesProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a UspChecaSiTieneExtensiones instance
        /// </summary>
        public static UspChecaSiTieneExtensionesProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.UspChecaSiTieneExtensiones.Assembly,
                    SoftvSettings.Settings.UspChecaSiTieneExtensiones.DataClass);
                    _Instance = (UspChecaSiTieneExtensionesProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public UspChecaSiTieneExtensionesProvider()
        {
        }
        /// <summary>
        /// Abstract method to add UspChecaSiTieneExtensiones
        ///  /summary>
        /// <param name="UspChecaSiTieneExtensiones"></param>
        /// <returns></returns>
        public abstract int AddUspChecaSiTieneExtensiones(UspChecaSiTieneExtensionesEntity entity_UspChecaSiTieneExtensiones);

        /// <summary>
        /// Abstract method to delete UspChecaSiTieneExtensiones
        /// </summary>
        public abstract int DeleteUspChecaSiTieneExtensiones();

        /// <summary>
        /// Abstract method to update UspChecaSiTieneExtensiones
        /// </summary>
        public abstract int EditUspChecaSiTieneExtensiones(UspChecaSiTieneExtensionesEntity entity_UspChecaSiTieneExtensiones);

        /// <summary>
        /// Abstract method to get all UspChecaSiTieneExtensiones
        /// </summary>
        public abstract List<UspChecaSiTieneExtensionesEntity> GetUspChecaSiTieneExtensiones();

        /// <summary>
        /// Abstract method to get all UspChecaSiTieneExtensiones List<int> lid
        /// </summary>
        public abstract List<UspChecaSiTieneExtensionesEntity> GetUspChecaSiTieneExtensiones(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract UspChecaSiTieneExtensionesEntity GetUspChecaSiTieneExtensionesById(long? CLVORDEN);



        /// <summary>
        ///Get UspChecaSiTieneExtensiones
        ///</summary>
        public abstract SoftvList<UspChecaSiTieneExtensionesEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get UspChecaSiTieneExtensiones
        ///</summary>
        public abstract SoftvList<UspChecaSiTieneExtensionesEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual UspChecaSiTieneExtensionesEntity GetUspChecaSiTieneExtensionesFromReader(IDataReader reader)
        {
            UspChecaSiTieneExtensionesEntity entity_UspChecaSiTieneExtensiones = null;
            try
            {
                entity_UspChecaSiTieneExtensiones = new UspChecaSiTieneExtensionesEntity();
                entity_UspChecaSiTieneExtensiones.BND = (int?)(GetFromReader(reader, "BND"));
                entity_UspChecaSiTieneExtensiones.NUMEXT = (int?)(GetFromReader(reader, "NUMEXT"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting UspChecaSiTieneExtensiones data to entity", ex);
            }
            return entity_UspChecaSiTieneExtensiones;
        }

    }

    #region Customs Methods

    #endregion
}


﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ValidaEliminaRelLocalidadCiudadProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidaEliminaRelLocalidadCiudad Provider
    /// File                    : ValidaEliminaRelLocalidadCiudadProvider.cs
    /// Creation date           : 21/09/2017
    /// Creation time           : 05:38 p. m.
    /// </summary>
    public abstract class ValidaEliminaRelLocalidadCiudadProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ValidaEliminaRelLocalidadCiudad from DB
        /// </summary>
        private static ValidaEliminaRelLocalidadCiudadProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ValidaEliminaRelLocalidadCiudad instance
        /// </summary>
        public static ValidaEliminaRelLocalidadCiudadProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ValidaEliminaRelLocalidadCiudad.Assembly,
                    SoftvSettings.Settings.ValidaEliminaRelLocalidadCiudad.DataClass);
                    _Instance = (ValidaEliminaRelLocalidadCiudadProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ValidaEliminaRelLocalidadCiudadProvider()
        {
        }
        /// <summary>
        /// Abstract method to add ValidaEliminaRelLocalidadCiudad
        ///  /summary>
        /// <param name="ValidaEliminaRelLocalidadCiudad"></param>
        /// <returns></returns>
        public abstract int AddValidaEliminaRelLocalidadCiudad(ValidaEliminaRelLocalidadCiudadEntity entity_ValidaEliminaRelLocalidadCiudad);

        /// <summary>
        /// Abstract method to delete ValidaEliminaRelLocalidadCiudad
        /// </summary>
        public abstract int DeleteValidaEliminaRelLocalidadCiudad();

        /// <summary>
        /// Abstract method to update ValidaEliminaRelLocalidadCiudad
        /// </summary>
        public abstract int EditValidaEliminaRelLocalidadCiudad(ValidaEliminaRelLocalidadCiudadEntity entity_ValidaEliminaRelLocalidadCiudad);

        /// <summary>
        /// Abstract method to get all ValidaEliminaRelLocalidadCiudad
        /// </summary>
        public abstract List<ValidaEliminaRelLocalidadCiudadEntity> GetValidaEliminaRelLocalidadCiudad();

        /// <summary>
        /// Abstract method to get all ValidaEliminaRelLocalidadCiudad List<int> lid
        /// </summary>
        public abstract List<ValidaEliminaRelLocalidadCiudadEntity> GetValidaEliminaRelLocalidadCiudad(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ValidaEliminaRelLocalidadCiudadEntity GetValidaEliminaRelLocalidadCiudadById(int? clv_localidad, int? clv_ciudad);



        /// <summary>
        ///Get ValidaEliminaRelLocalidadCiudad
        ///</summary>
        public abstract SoftvList<ValidaEliminaRelLocalidadCiudadEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get ValidaEliminaRelLocalidadCiudad
        ///</summary>
        public abstract SoftvList<ValidaEliminaRelLocalidadCiudadEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ValidaEliminaRelLocalidadCiudadEntity GetValidaEliminaRelLocalidadCiudadFromReader(IDataReader reader)
        {
            ValidaEliminaRelLocalidadCiudadEntity entity_ValidaEliminaRelLocalidadCiudad = null;
            try
            {
                entity_ValidaEliminaRelLocalidadCiudad = new ValidaEliminaRelLocalidadCiudadEntity();
                entity_ValidaEliminaRelLocalidadCiudad.error = (String)(GetFromReader(reader, "error", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ValidaEliminaRelLocalidadCiudad data to entity", ex);
            }
            return entity_ValidaEliminaRelLocalidadCiudad;
        }

    }

    #region Customs Methods

    #endregion
}


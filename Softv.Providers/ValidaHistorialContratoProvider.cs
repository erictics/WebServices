﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.ValidaHistorialContratoProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidaHistorialContrato Provider
    /// File                    : ValidaHistorialContratoProvider.cs
    /// Creation date           : 07/03/2017
    /// Creation time           : 04:37 p. m.
    /// </summary>
    public abstract class ValidaHistorialContratoProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of ValidaHistorialContrato from DB
        /// </summary>
        private static ValidaHistorialContratoProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a ValidaHistorialContrato instance
        /// </summary>
        public static ValidaHistorialContratoProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.ValidaHistorialContrato.Assembly,
                    SoftvSettings.Settings.ValidaHistorialContrato.DataClass);
                    _Instance = (ValidaHistorialContratoProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public ValidaHistorialContratoProvider()
        {
        }
        /// <summary>
        /// Abstract method to add ValidaHistorialContrato
        ///  /summary>
        /// <param name="ValidaHistorialContrato"></param>
        /// <returns></returns>
        public abstract int AddValidaHistorialContrato(ValidaHistorialContratoEntity entity_ValidaHistorialContrato);

        /// <summary>
        /// Abstract method to delete ValidaHistorialContrato
        /// </summary>
        public abstract int DeleteValidaHistorialContrato();

        /// <summary>
        /// Abstract method to update ValidaHistorialContrato
        /// </summary>
        public abstract int EditValidaHistorialContrato(ValidaHistorialContratoEntity entity_ValidaHistorialContrato);

        /// <summary>
        /// Abstract method to get all ValidaHistorialContrato
        /// </summary>
        public abstract List<ValidaHistorialContratoEntity> GetValidaHistorialContrato();

        /// <summary>
        /// Abstract method to get all ValidaHistorialContrato List<int> lid
        /// </summary>
        public abstract List<ValidaHistorialContratoEntity> GetValidaHistorialContrato(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract ValidaHistorialContratoEntity GetValidaHistorialContratoById(long? Contrato);



        /// <summary>
        ///Get ValidaHistorialContrato
        ///</summary>
        public abstract SoftvList<ValidaHistorialContratoEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get ValidaHistorialContrato
        ///</summary>
        public abstract SoftvList<ValidaHistorialContratoEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual ValidaHistorialContratoEntity GetValidaHistorialContratoFromReader(IDataReader reader)
        {
            ValidaHistorialContratoEntity entity_ValidaHistorialContrato = null;
            try
            {
                entity_ValidaHistorialContrato = new ValidaHistorialContratoEntity();
                //entity_ValidaHistorialContrato.Contrato = (long?)(GetFromReader(reader, "Contrato"));
                entity_ValidaHistorialContrato.tieneSaldo = (int?)(GetFromReader(reader, "tieneSaldo"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting ValidaHistorialContrato data to entity", ex);
            }
            return entity_ValidaHistorialContrato;
        }

    }

    #region Customs Methods

    #endregion
}


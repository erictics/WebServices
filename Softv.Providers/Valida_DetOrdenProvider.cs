﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.Valida_DetOrdenProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Valida_DetOrden Provider
    /// File                    : Valida_DetOrdenProvider.cs
    /// Creation date           : 25/03/2017
    /// Creation time           : 11:36 a. m.
    /// </summary>
    public abstract class Valida_DetOrdenProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of Valida_DetOrden from DB
        /// </summary>
        private static Valida_DetOrdenProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a Valida_DetOrden instance
        /// </summary>
        public static Valida_DetOrdenProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.Valida_DetOrden.Assembly,
                    SoftvSettings.Settings.Valida_DetOrden.DataClass);
                    _Instance = (Valida_DetOrdenProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public Valida_DetOrdenProvider()
        {
        }
        /// <summary>
        /// Abstract method to add Valida_DetOrden
        ///  /summary>
        /// <param name="Valida_DetOrden"></param>
        /// <returns></returns>
        public abstract int AddValida_DetOrden(Valida_DetOrdenEntity entity_Valida_DetOrden);

        /// <summary>
        /// Abstract method to delete Valida_DetOrden
        /// </summary>
        public abstract int DeleteValida_DetOrden();

        /// <summary>
        /// Abstract method to update Valida_DetOrden
        /// </summary>
        public abstract int EditValida_DetOrden(Valida_DetOrdenEntity entity_Valida_DetOrden);

        /// <summary>
        /// Abstract method to get all Valida_DetOrden
        /// </summary>
        public abstract List<Valida_DetOrdenEntity> GetValida_DetOrden();

        /// <summary>
        /// Abstract method to get all Valida_DetOrden List<int> lid
        /// </summary>
        public abstract List<Valida_DetOrdenEntity> GetValida_DetOrden(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract Valida_DetOrdenEntity GetValida_DetOrdenById(long? ClvOrden);



        /// <summary>
        ///Get Valida_DetOrden
        ///</summary>
        public abstract SoftvList<Valida_DetOrdenEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get Valida_DetOrden
        ///</summary>
        public abstract SoftvList<Valida_DetOrdenEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual Valida_DetOrdenEntity GetValida_DetOrdenFromReader(IDataReader reader)
        {
            Valida_DetOrdenEntity entity_Valida_DetOrden = null;
            try
            {
                entity_Valida_DetOrden = new Valida_DetOrdenEntity();
                //entity_Valida_DetOrden.ClvOrden = (long?)(GetFromReader(reader, "ClvOrden"));
                entity_Valida_DetOrden.Validacion = (int?)(GetFromReader(reader, "Validacion"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting Valida_DetOrden data to entity", ex);
            }
            return entity_Valida_DetOrden;
        }

    }

    #region Customs Methods

    #endregion
}


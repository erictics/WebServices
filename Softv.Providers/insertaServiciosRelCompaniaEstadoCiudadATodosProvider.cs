﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.insertaServiciosRelCompaniaEstadoCiudadATodosProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : insertaServiciosRelCompaniaEstadoCiudadATodos Provider
    /// File                    : insertaServiciosRelCompaniaEstadoCiudadATodosProvider.cs
    /// Creation date           : 21/09/2017
    /// Creation time           : 12:07 p. m.
    /// </summary>
    public abstract class insertaServiciosRelCompaniaEstadoCiudadATodosProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of insertaServiciosRelCompaniaEstadoCiudadATodos from DB
        /// </summary>
        private static insertaServiciosRelCompaniaEstadoCiudadATodosProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a insertaServiciosRelCompaniaEstadoCiudadATodos instance
        /// </summary>
        public static insertaServiciosRelCompaniaEstadoCiudadATodosProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.insertaServiciosRelCompaniaEstadoCiudadATodos.Assembly,
                    SoftvSettings.Settings.insertaServiciosRelCompaniaEstadoCiudadATodos.DataClass);
                    _Instance = (insertaServiciosRelCompaniaEstadoCiudadATodosProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public insertaServiciosRelCompaniaEstadoCiudadATodosProvider()
        {
        }
        /// <summary>
        /// Abstract method to add insertaServiciosRelCompaniaEstadoCiudadATodos
        ///  /summary>
        /// <param name="insertaServiciosRelCompaniaEstadoCiudadATodos"></param>
        /// <returns></returns>
        public abstract int AddinsertaServiciosRelCompaniaEstadoCiudadATodos(insertaServiciosRelCompaniaEstadoCiudadATodosEntity entity_insertaServiciosRelCompaniaEstadoCiudadATodos);

        /// <summary>
        /// Abstract method to delete insertaServiciosRelCompaniaEstadoCiudadATodos
        /// </summary>
        public abstract int DeleteinsertaServiciosRelCompaniaEstadoCiudadATodos(long? clv_plaza, long? clv_servicio);

        /// <summary>
        /// Abstract method to update insertaServiciosRelCompaniaEstadoCiudadATodos
        /// </summary>
        public abstract int EditinsertaServiciosRelCompaniaEstadoCiudadATodos(insertaServiciosRelCompaniaEstadoCiudadATodosEntity entity_insertaServiciosRelCompaniaEstadoCiudadATodos);

        /// <summary>
        /// Abstract method to get all insertaServiciosRelCompaniaEstadoCiudadATodos
        /// </summary>
        public abstract List<insertaServiciosRelCompaniaEstadoCiudadATodosEntity> GetinsertaServiciosRelCompaniaEstadoCiudadATodos();

        /// <summary>
        /// Abstract method to get all insertaServiciosRelCompaniaEstadoCiudadATodos List<int> lid
        /// </summary>
        public abstract List<insertaServiciosRelCompaniaEstadoCiudadATodosEntity> GetinsertaServiciosRelCompaniaEstadoCiudadATodos(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract insertaServiciosRelCompaniaEstadoCiudadATodosEntity GetinsertaServiciosRelCompaniaEstadoCiudadATodosById();



        /// <summary>
        ///Get insertaServiciosRelCompaniaEstadoCiudadATodos
        ///</summary>
        public abstract SoftvList<insertaServiciosRelCompaniaEstadoCiudadATodosEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get insertaServiciosRelCompaniaEstadoCiudadATodos
        ///</summary>
        public abstract SoftvList<insertaServiciosRelCompaniaEstadoCiudadATodosEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual insertaServiciosRelCompaniaEstadoCiudadATodosEntity GetinsertaServiciosRelCompaniaEstadoCiudadATodosFromReader(IDataReader reader)
        {
            insertaServiciosRelCompaniaEstadoCiudadATodosEntity entity_insertaServiciosRelCompaniaEstadoCiudadATodos = null;
            try
            {
                entity_insertaServiciosRelCompaniaEstadoCiudadATodos = new insertaServiciosRelCompaniaEstadoCiudadATodosEntity();
                entity_insertaServiciosRelCompaniaEstadoCiudadATodos.clv_plaza = (long?)(GetFromReader(reader, "clv_plaza"));
                entity_insertaServiciosRelCompaniaEstadoCiudadATodos.clv_servicio = (long?)(GetFromReader(reader, "clv_servicio"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting insertaServiciosRelCompaniaEstadoCiudadATodos data to entity", ex);
            }
            return entity_insertaServiciosRelCompaniaEstadoCiudadATodos;
        }

    }

    #region Customs Methods

    #endregion
}


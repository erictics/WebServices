﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.sp_dameInfodelCobroProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : sp_dameInfodelCobro Provider
    /// File                    : sp_dameInfodelCobroProvider.cs
    /// Creation date           : 02/03/2017
    /// Creation time           : 06:36 p. m.
    /// </summary>
    public abstract class sp_dameInfodelCobroProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of sp_dameInfodelCobro from DB
        /// </summary>
        private static sp_dameInfodelCobroProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a sp_dameInfodelCobro instance
        /// </summary>
        public static sp_dameInfodelCobroProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.sp_dameInfodelCobro.Assembly,
                    SoftvSettings.Settings.sp_dameInfodelCobro.DataClass);
                    _Instance = (sp_dameInfodelCobroProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public sp_dameInfodelCobroProvider()
        {
        }
        /// <summary>
        /// Abstract method to add sp_dameInfodelCobro
        ///  /summary>
        /// <param name="sp_dameInfodelCobro"></param>
        /// <returns></returns>
        public abstract int Addsp_dameInfodelCobro(sp_dameInfodelCobroEntity entity_sp_dameInfodelCobro);

        /// <summary>
        /// Abstract method to delete sp_dameInfodelCobro
        /// </summary>
        public abstract int Deletesp_dameInfodelCobro();

        /// <summary>
        /// Abstract method to update sp_dameInfodelCobro
        /// </summary>
        public abstract int Editsp_dameInfodelCobro(sp_dameInfodelCobroEntity entity_sp_dameInfodelCobro);

        /// <summary>
        /// Abstract method to get all sp_dameInfodelCobro
        /// </summary>
        public abstract List<sp_dameInfodelCobroEntity> Getsp_dameInfodelCobro();

        /// <summary>
        /// Abstract method to get all sp_dameInfodelCobro List<int> lid
        /// </summary>
        public abstract List<sp_dameInfodelCobroEntity> Getsp_dameInfodelCobro(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract sp_dameInfodelCobroEntity Getsp_dameInfodelCobroById(long? ClvSession, long? ClvDetalle);



        /// <summary>
        ///Get sp_dameInfodelCobro
        ///</summary>
        public abstract SoftvList<sp_dameInfodelCobroEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get sp_dameInfodelCobro
        ///</summary>
        public abstract SoftvList<sp_dameInfodelCobroEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual sp_dameInfodelCobroEntity Getsp_dameInfodelCobroFromReader(IDataReader reader)
        {
            sp_dameInfodelCobroEntity entity_sp_dameInfodelCobro = null;
            try
            {
                entity_sp_dameInfodelCobro = new sp_dameInfodelCobroEntity();
                //entity_sp_dameInfodelCobro.ClvSession = (long?)(GetFromReader(reader, "ClvSession"));
                //entity_sp_dameInfodelCobro.ClvDetalle = (long?)(GetFromReader(reader, "ClvDetalle"));
                entity_sp_dameInfodelCobro.Msg = (String)(GetFromReader(reader, "Msg", IsString: true));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting sp_dameInfodelCobro data to entity", ex);
            }
            return entity_sp_dameInfodelCobro;
        }

    }

    #region Customs Methods

    #endregion
}


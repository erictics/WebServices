﻿
using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;
using Globals;

namespace Softv.Providers
{
    /// <summary>
    /// Class                   : Softv.Providers.tieneEdoCuentaProvider
    /// Generated by            : Class Generator (c) 2014
    /// Description             : tieneEdoCuenta Provider
    /// File                    : tieneEdoCuentaProvider.cs
    /// Creation date           : 28/02/2017
    /// Creation time           : 05:16 p. m.
    /// </summary>
    public abstract class tieneEdoCuentaProvider : Globals.DataAccess
    {

        /// <summary>
        /// Instance of tieneEdoCuenta from DB
        /// </summary>
        private static tieneEdoCuentaProvider _Instance = null;

        private static ObjectHandle obj;
        /// <summary>
        /// Generates a tieneEdoCuenta instance
        /// </summary>
        public static tieneEdoCuentaProvider Instance
        {
            get
            {
                if (_Instance == null)
                {
                    obj = Activator.CreateInstance(
                    SoftvSettings.Settings.tieneEdoCuenta.Assembly,
                    SoftvSettings.Settings.tieneEdoCuenta.DataClass);
                    _Instance = (tieneEdoCuentaProvider)obj.Unwrap();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Provider's default constructor
        /// </summary>
        public tieneEdoCuentaProvider()
        {
        }
        /// <summary>
        /// Abstract method to add tieneEdoCuenta
        ///  /summary>
        /// <param name="tieneEdoCuenta"></param>
        /// <returns></returns>
        public abstract int AddtieneEdoCuenta(tieneEdoCuentaEntity entity_tieneEdoCuenta);

        /// <summary>
        /// Abstract method to delete tieneEdoCuenta
        /// </summary>
        public abstract int DeletetieneEdoCuenta();

        /// <summary>
        /// Abstract method to update tieneEdoCuenta
        /// </summary>
        public abstract int EdittieneEdoCuenta(tieneEdoCuentaEntity entity_tieneEdoCuenta);

        /// <summary>
        /// Abstract method to get all tieneEdoCuenta
        /// </summary>
        public abstract List<tieneEdoCuentaEntity> GettieneEdoCuenta();

        /// <summary>
        /// Abstract method to get all tieneEdoCuenta List<int> lid
        /// </summary>
        public abstract List<tieneEdoCuentaEntity> GettieneEdoCuenta(List<int> lid);

        /// <summary>
        /// Abstract method to get by id
        /// </summary>
        public abstract tieneEdoCuentaEntity GettieneEdoCuentaById(long? Contrato);



        /// <summary>
        ///Get tieneEdoCuenta
        ///</summary>
        public abstract SoftvList<tieneEdoCuentaEntity> GetPagedList(int pageIndex, int pageSize);

        /// <summary>
        ///Get tieneEdoCuenta
        ///</summary>
        public abstract SoftvList<tieneEdoCuentaEntity> GetPagedList(int pageIndex, int pageSize, String xml);

        /// <summary>
        /// Converts data from reader to entity
        /// </summary>
        protected virtual tieneEdoCuentaEntity GettieneEdoCuentaFromReader(IDataReader reader)
        {
            tieneEdoCuentaEntity entity_tieneEdoCuenta = null;
            try
            {
                entity_tieneEdoCuenta = new tieneEdoCuentaEntity();
                //entity_tieneEdoCuenta.Contrato = (long?)(GetFromReader(reader, "Contrato")); , IsString: true));
                entity_tieneEdoCuenta.tieneEdoCuenta = (bool)(GetFromReader(reader, "tieneEdoCuenta"));
                entity_tieneEdoCuenta.idEstadoCuenta = (long?)(GetFromReader(reader, "idEstadoCuenta"));

            }
            catch (Exception ex)
            {
                throw new Exception("Error converting tieneEdoCuenta data to entity", ex);
            }
            return entity_tieneEdoCuenta;
        }

    }

    #region Customs Methods

    #endregion
}


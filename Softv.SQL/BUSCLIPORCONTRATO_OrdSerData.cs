﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.BUSCLIPORCONTRATO_OrdSerData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : BUSCLIPORCONTRATO_OrdSer Data Access Object
    /// File                    : BUSCLIPORCONTRATO_OrdSerDAO.cs
    /// Creation date           : 13/03/2017
    /// Creation time           : 06:11 p. m.
    ///</summary>
    public class BUSCLIPORCONTRATO_OrdSerData : BUSCLIPORCONTRATO_OrdSerProvider
    {
       
        /// <summary>
        /// Gets BUSCLIPORCONTRATO_OrdSer by
        ///</summary>
        public override BUSCLIPORCONTRATO_OrdSerEntity GetBUSCLIPORCONTRATO_OrdSerById(long? CONTRATO)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.BUSCLIPORCONTRATO_OrdSer.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("softvweb_BUSCLIPORCONTRATO", connection);
                BUSCLIPORCONTRATO_OrdSerEntity entity_BUSCLIPORCONTRATO_OrdSer = null;

                AssingParameter(comandoSql, "@CONTRATO", CONTRATO);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_BUSCLIPORCONTRATO_OrdSer = GetBUSCLIPORCONTRATO_OrdSerFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data BUSCLIPORCONTRATO_OrdSer " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_BUSCLIPORCONTRATO_OrdSer;
            }

        }



      

        #region Customs Methods

        #endregion
    }
}

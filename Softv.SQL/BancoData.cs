﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.BancoData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Banco Data Access Object
    /// File                    : BancoDAO.cs
    /// Creation date           : 26/02/2016
    /// Creation time           : 04:23 p. m.
    ///</summary>
    public class BancoData : BancoProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Banco"> Object Banco added to List</param>
        public override int AddBanco(BancoEntity entity_Banco)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Banco.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("NUEBANCOS", connection);
                           
                AssingParameter(comandoSql, "@nombre", entity_Banco.Nombre);
                AssingParameter(comandoSql, "@ClaveRel", entity_Banco.ClaveRel);
                AssingParameter(comandoSql, "@Clv_Txt", entity_Banco.ClaveTxt);
                AssingParameter(comandoSql, "@clave", null, pd: ParameterDirection.Output, IsKey: true);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Banco " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@clave"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Banco
        ///</summary>
        /// <param name="">  IdBanco to delete </param>
        public override int DeleteBanco(int? IdBanco)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Banco.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("BORBANCOS", connection);

                AssingParameter(comandoSql, "@clave", IdBanco);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Banco " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Banco
        ///</summary>
        /// <param name="Banco"> Objeto Banco a editar </param>
        public override int EditBanco(BancoEntity entity_Banco)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Banco.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MODBANCOS", connection);

                AssingParameter(comandoSql, "@clave", entity_Banco.IdBanco);

                AssingParameter(comandoSql, "@nombre", entity_Banco.Nombre);

                AssingParameter(comandoSql, "@ClaveRel", entity_Banco.ClaveRel);

                AssingParameter(comandoSql, "@Clv_Txt", entity_Banco.ClaveTxt);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Banco " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Banco
        ///</summary>
        public override List<BancoEntity> GetBanco(int? IdBanco, String Nombre, int? Op)
        {
            List<BancoEntity> BancoList = new List<BancoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Banco.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("BUSCABANCOS", connection);

                AssingParameter(comandoSql, "@clave", IdBanco);
                AssingParameter(comandoSql, "@nombre", Nombre);
                AssingParameter(comandoSql, "@OP", Op);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        BancoList.Add(GetBancoAllFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Banco " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return BancoList;
        }

        /// <summary>
        /// Gets all Banco by List<int>
        ///</summary>
        public override List<BancoEntity> GetBanco(List<int> lid)
        {
            List<BancoEntity> BancoList = new List<BancoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Banco.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_BancoGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        BancoList.Add(GetBancoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Banco " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return BancoList;
        }

        /// <summary>
        /// Gets Banco by
        ///</summary>
        public override BancoEntity GetBancoById(int? IdBanco)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Banco.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("CONBANCOS", connection);
                BancoEntity entity_Banco = null;


                AssingParameter(comandoSql, "@clave", IdBanco);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Banco = GetBancoFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Banco " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Banco;
            }

        }


        public override List<BancoEntity> GetBancoByIdBanco(int? IdBanco)
        {
            List<BancoEntity> BancoList = new List<BancoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Banco.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_BancoGetByIdBanco", connection);

                AssingParameter(comandoSql, "@IdBanco", IdBanco);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        BancoList.Add(GetBancoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Banco " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return BancoList;
        }

        
        #region Customs Methods

        #endregion
    }
}

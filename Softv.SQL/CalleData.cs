﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals; 

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.CalleData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Calle Data Access Object
    /// File                    : CalleDAO.cs
    /// Creation date           : 30/12/2015
    /// Creation time           : 10:00 a. m.
    ///</summary>
    public class CalleData : CalleProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Calle"> Object Calle added to List</param>
        public override int AddCalle(CalleEntity entity_Calle)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleAdd", connection);

                AssingParameter(comandoSql, "@IdCalle", null, pd: ParameterDirection.Output, IsKey: true);

                AssingParameter(comandoSql, "@Nombre", entity_Calle.Nombre);

                //AssingParameter(comandoSql, "@IdColonia", entity_Calle.IdColonia);
                //AssingParameter(comandoSql, "@IdLocalidad", entity_Calle.IdLocalidad);
                //AssingParameter(comandoSql, "@IdMunicipio", entity_Calle.IdMunicipio);
                //AssingParameter(comandoSql, "@IdEstado", entity_Calle.IdEstado);


                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Calle " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdCalle"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Calle
        ///</summary>
        /// <param name="">  IdCalle to delete </param>
        public override int DeleteCalle(int? IdCalle)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleDelete", connection);

                AssingParameter(comandoSql, "@IdCalle", IdCalle);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Calle
        ///</summary>
        /// <param name="Calle"> Objeto Calle a editar </param>
        public override int EditCalle(CalleEntity entity_Calle)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleEdit", connection);

                AssingParameter(comandoSql, "@IdCalle", entity_Calle.IdCalle);

                AssingParameter(comandoSql, "@Nombre", entity_Calle.Nombre);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdCalle"].Value;
            }
            return result;
        }















        public override int AddRelCalle(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("SP_AddCalleL", connection);
                AssingParameter(comandoSql, "@IdCalle", null, pd: ParameterDirection.Output, IsKey: true);
                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Calle " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdCalle"].Value;
            }
            return result;
        }


        
        public override int UpdateRelCalle(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("SP_UpdateCalleL", connection);
                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Calle " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }
































        /// <summary>
        /// Gets all Calle
        ///</summary>
        public override List<CalleEntity> GetCalle()
        {
            List<CalleEntity> CalleList = new List<CalleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        CalleList.Add(GetCalleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return CalleList;
        }

        /// <summary>
        /// Gets all Calle by List<int>
        ///</summary>
        public override List<CalleEntity> GetCalle(List<int> lid)
        {
            List<CalleEntity> CalleList = new List<CalleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_CalleGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        CalleList.Add(GetCalleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return CalleList;
        }

        /// <summary>
        /// Gets Calle by
        ///</summary>
        public override CalleEntity GetCalleById(int? IdCalle)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleGetById", connection);
                CalleEntity entity_Calle = null;


                AssingParameter(comandoSql, "@IdCalle", IdCalle);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Calle = GetCalleFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Calle;
            }

        }



        /// <summary>
        ///Get Calle
        ///</summary>
        public override SoftvList<CalleEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<CalleEntity> entities = new SoftvList<CalleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetCalleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetCalleCount();
                return entities ?? new SoftvList<CalleEntity>();
            }
        }

        /// <summary>
        ///Get Calle
        ///</summary>
        public override SoftvList<CalleEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<CalleEntity> entities = new SoftvList<CalleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetCalleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetCalleCount(xml);
                return entities ?? new SoftvList<CalleEntity>();
            }
        }

        /// <summary>
        ///Get Count Calle
        ///</summary>
        public int GetCalleCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Calle
        ///</summary>
        public int GetCalleCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_CalleGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Calle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

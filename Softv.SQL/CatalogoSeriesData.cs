﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.CatalogoSeriesData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : CatalogoSeries Data Access Object
    /// File                    : CatalogoSeriesDAO.cs
    /// Creation date           : 22/11/2017
    /// Creation time           : 01:10 p. m.
    ///</summary>
    public class CatalogoSeriesData : CatalogoSeriesProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="CatalogoSeries"> Object CatalogoSeries added to List</param>
        public override int AddCatalogoSeries(CatalogoSeriesEntity entity_CatalogoSeries)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.CatalogoSeries.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("NUECatalogoSeries", connection);

                AssingParameter(comandoSql, "@Serie", entity_CatalogoSeries.Serie);

                AssingParameter(comandoSql, "@Folios_Impresos", entity_CatalogoSeries.Folios_Impresos);

                AssingParameter(comandoSql, "@UltimoFolio_Usado", entity_CatalogoSeries.UltimoFolio_Usado);

                AssingParameter(comandoSql, "@Clv_Vendedor", entity_CatalogoSeries.Clv_Vendedor);

                AssingParameter(comandoSql, "@Tipo", entity_CatalogoSeries.Tipo);

                AssingParameter(comandoSql, "@Clave", null, pd: ParameterDirection.Output, IsKey: true);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding CatalogoSeries " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@Clave"].Value;
            }
            return result;
        }



        public override int AddFolios(CatalogoSeriesEntity entity_CatalogoSeries)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.CatalogoSeries.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("SpInsertNewTbl_Folios", connection);

                AssingParameter(comandoSql, "@serie", entity_CatalogoSeries.Serie);

                AssingParameter(comandoSql, "@folio", entity_CatalogoSeries.Folio);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding CatalogoSeries " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = 1;
            }
            return result;
        }


        public override int AddSerieFolios(CatalogoSeriesEntity entity_CatalogoSeries)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.CatalogoSeries.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("SP_InsertarSerieFolio", connection);

                AssingParameter(comandoSql, "@serie", entity_CatalogoSeries.Serie);

                AssingParameter(comandoSql, "@folio", entity_CatalogoSeries.Folio);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding CatalogoSeries " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = 1;
            }
            return result;
        }






        /// <summary>
        /// Deletes a CatalogoSeries
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteCatalogoSeries(int? Clave)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.CatalogoSeries.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("BORCatalogoSeries", connection);

                AssingParameter(comandoSql, "@Clave", Clave);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting CatalogoSeries " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a CatalogoSeries
        ///</summary>
        /// <param name="CatalogoSeries"> Objeto CatalogoSeries a editar </param>
        public override int EditCatalogoSeries(CatalogoSeriesEntity entity_CatalogoSeries)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.CatalogoSeries.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MODCatalogoSeries", connection);

                AssingParameter(comandoSql, "@Clave", entity_CatalogoSeries.Clave);

                AssingParameter(comandoSql, "@Serie", entity_CatalogoSeries.Serie);

                AssingParameter(comandoSql, "@Folios_Impresos", entity_CatalogoSeries.Folios_Impresos);

                AssingParameter(comandoSql, "@UltimoFolio_Usado", entity_CatalogoSeries.UltimoFolio_Usado);

                AssingParameter(comandoSql, "@Clv_Vendedor", entity_CatalogoSeries.Clv_Vendedor);

                AssingParameter(comandoSql, "@Tipo", entity_CatalogoSeries.Tipo);


                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating CatalogoSeries " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all CatalogoSeries
        ///</summary>
        public override List<CatalogoSeriesEntity> GetCatalogoSeries(String Serie, int? Clv_Vendedor, String NOMBRE, int? Op, int? ClvUsuario, int? IdCompania, int? Tipo)
        {
            List<CatalogoSeriesEntity> CatalogoSeriesList = new List<CatalogoSeriesEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.CatalogoSeries.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("BUSCACatalogoSeries", connection);

                AssingParameter(comandoSql, "@SERIE", Serie);
                AssingParameter(comandoSql, "@Clv_Vendedor", Clv_Vendedor);
                AssingParameter(comandoSql, "@NOMBRE", NOMBRE);
                AssingParameter(comandoSql, "@OP", Op);
                AssingParameter(comandoSql, "@ClvUsuario", ClvUsuario);
                AssingParameter(comandoSql, "@idcompania", IdCompania);
                AssingParameter(comandoSql, "@tipo", Tipo);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        CatalogoSeriesList.Add(GetCatalogoSeriesAllFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data CatalogoSeries " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return CatalogoSeriesList;
        }

        
        /// <summary>
        /// Gets CatalogoSeries by
        ///</summary>
        public override CatalogoSeriesEntity GetCatalogoSeriesById(int? Clave)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.CatalogoSeries.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("CONCatalogoSeries", connection);
                CatalogoSeriesEntity entity_CatalogoSeries = null;

                AssingParameter(comandoSql, "@Clave", Clave);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_CatalogoSeries = GetCatalogoSeriesFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data CatalogoSeries " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_CatalogoSeries;
            }

        }



      

        

        #region Customs Methods

        #endregion
    }
}

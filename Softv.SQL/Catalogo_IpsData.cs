﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Catalogo_IpsData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Catalogo_Ips Data Access Object
    /// File                    : Catalogo_IpsDAO.cs
    /// Creation date           : 07/11/2017
    /// Creation time           : 12:34 p. m.
    ///</summary>
    public class Catalogo_IpsData : Catalogo_IpsProvider
    {

        public override List<Catalogo_IpsEntity> GetCatalogo_Ips(long? a, long? b, long? c, long? d, int? mask, String status, string NombreRed)
        {
            List<Catalogo_IpsEntity> Catalogo_IpsList = new List<Catalogo_IpsEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Catalogo_Ips.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("Add_Catalogo_Ips", connection);
                AssingParameter(comandoSql, "@a", a);
                AssingParameter(comandoSql, "@b", b);
                AssingParameter(comandoSql, "@c", c);
                AssingParameter(comandoSql, "@d", d);
                AssingParameter(comandoSql, "@mask", mask);
                AssingParameter(comandoSql, "@status", status);
                AssingParameter(comandoSql, "@NombreRed", NombreRed);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Catalogo_IpsList.Add(GetCatalogo_IpsFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Catalogo_Ips " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Catalogo_IpsList;
        }
        

        public override List<Catalogo_IpsEntity> GetListCatalogo_Ips(long? IdRed, int? Op)
        {
            List<Catalogo_IpsEntity> Catalogo_IpsList = new List<Catalogo_IpsEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Catalogo_Ips.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("GetList_CatalogoRed", connection);

                AssingParameter(comandoSql, "@IdRed", IdRed);
                AssingParameter(comandoSql, "@Op", Op);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Catalogo_IpsList.Add(GetListCatalogo_IpsFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Catalogo_Ips " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Catalogo_IpsList;
        }
        
        public override Catalogo_IpsEntity GetCatalogo_IpsById(long? IdRed)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Catalogo_Ips.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("GetOneRed", connection);
                Catalogo_IpsEntity entity_Catalogo_Ips = null;

                AssingParameter(comandoSql, "@IdRed", IdRed);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Catalogo_Ips = GetListCatalogo_IpsFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Catalogo_Ips " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Catalogo_Ips;
            }

        }

        public override int? GetUpateNombreRed(Catalogo_IpsEntity ObjRed)
        {
            int? result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Catalogo_Ips.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("UpateNombreRed", connection);
                AssingParameter(comandoSql, "@IdRed", ObjRed.IdRed);
                AssingParameter(comandoSql, "@NombreRed", ObjRed.NombreRed);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Catalogo_Ips " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
                return result;
            }
        }

        #region Customs Methods

        #endregion
    }
}

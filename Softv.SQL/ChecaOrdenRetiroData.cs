﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ChecaOrdenRetiroData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ChecaOrdenRetiro Data Access Object
    /// File                    : ChecaOrdenRetiroDAO.cs
    /// Creation date           : 20/01/2017
    /// Creation time           : 12:31 p. m.
    ///</summary>
    public class ChecaOrdenRetiroData : ChecaOrdenRetiroProvider
    {
       
        public override List<ChecaOrdenRetiroEntity> GetChecaOrdenRetiro(long? Contrato)
        {
            List<ChecaOrdenRetiroEntity> ChecaOrdenRetiroList = new List<ChecaOrdenRetiroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ChecaOrdenRetiro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("softvweb_ChecaOrdenRetiro", connection);
                AssingParameter(comandoSql, "@Contrato", Contrato);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ChecaOrdenRetiroList.Add(GetChecaOrdenRetiroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ChecaOrdenRetiro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ChecaOrdenRetiroList;
        }        

        #region Customs Methods

        #endregion
    }
}

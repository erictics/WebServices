﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Ciudades_NewData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Ciudades_New Data Access Object
    /// File                    : Ciudades_NewDAO.cs
    /// Creation date           : 12/09/2017
    /// Creation time           : 05:01 p. m.
    ///</summary>
    public class Ciudades_NewData : Ciudades_NewProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Ciudades_New"> Object Ciudades_New added to List</param>
        public override int AddCiudades_New(Ciudades_NewEntity entity_Ciudades_New)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewAdd", connection);

                AssingParameter(comandoSql, "@Clv_Ciudad", entity_Ciudades_New.Clv_Ciudad);

                AssingParameter(comandoSql, "@Nombre", entity_Ciudades_New.Nombre);

                AssingParameter(comandoSql, "@CobroEspecial", entity_Ciudades_New.CobroEspecial);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdCiudades_New"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Ciudades_New
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteCiudades_New(int? Clv_Ciudad)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("BORCIUDADES", connection);
                AssingParameter(comandoSql, "@Clv_Ciudad", Clv_Ciudad);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Ciudades_New
        ///</summary>
        /// <param name="Ciudades_New"> Objeto Ciudades_New a editar </param>
        public override int EditCiudades_New(Ciudades_NewEntity entity_Ciudades_New)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MODCIUDADES", connection);

                AssingParameter(comandoSql, "@CLV_CIUDAD", entity_Ciudades_New.Clv_Ciudad);
                AssingParameter(comandoSql, "@NOMBRE", entity_Ciudades_New.Nombre);


                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Ciudades_New
        ///</summary>
        public override List<Ciudades_NewEntity> GetCiudades_New()
        {
            List<Ciudades_NewEntity> Ciudades_NewList = new List<Ciudades_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Ciudades_NewList.Add(GetCiudades_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Ciudades_NewList;
        }




        public override List<Ciudades_NewEntity> GetMuestraCiudad(int? Clv_Ciudad)
        {
            List<Ciudades_NewEntity> Ciudades_NewList = new List<Ciudades_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("CONCIUDADES", connection);

                AssingParameter(comandoSql, "Clv_Ciudad", Clv_Ciudad);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Ciudades_NewList.Add(GetCiudades_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Ciudades_NewList;
        }





        public override List<Ciudades_NewEntity> GetAddCiudades(String Nombre, int? Id)
        {
            List<Ciudades_NewEntity> Ciudades_NewList = new List<Ciudades_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Add_Ciudad_New", connection);

                AssingParameter(comandoSql, "@Nombre", Nombre);
  

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Ciudades_NewList.Add(GetAddCiudades_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Ciudades_NewList;
        }














        /// <summary>
        /// Gets all Ciudades_New by List<int>
        ///</summary>
        public override List<Ciudades_NewEntity> GetCiudades_New(List<int> lid)
        {
            List<Ciudades_NewEntity> Ciudades_NewList = new List<Ciudades_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Ciudades_NewList.Add(GetCiudades_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Ciudades_NewList;
        }

        /// <summary>
        /// Gets Ciudades_New by
        ///</summary>
        public override Ciudades_NewEntity GetCiudades_NewById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewGetById", connection);
                Ciudades_NewEntity entity_Ciudades_New = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Ciudades_New = GetCiudades_NewFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Ciudades_New;
            }

        }



        /// <summary>
        ///Get Ciudades_New
        ///</summary>
        public override SoftvList<Ciudades_NewEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<Ciudades_NewEntity> entities = new SoftvList<Ciudades_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetCiudades_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetCiudades_NewCount();
                return entities ?? new SoftvList<Ciudades_NewEntity>();
            }
        }

        /// <summary>
        ///Get Ciudades_New
        ///</summary>
        public override SoftvList<Ciudades_NewEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<Ciudades_NewEntity> entities = new SoftvList<Ciudades_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetCiudades_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetCiudades_NewCount(xml);
                return entities ?? new SoftvList<Ciudades_NewEntity>();
            }
        }

        /// <summary>
        ///Get Count Ciudades_New
        ///</summary>
        public int GetCiudades_NewCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Ciudades_New
        ///</summary>
        public int GetCiudades_NewCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Ciudades_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Ciudades_NewGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql); 


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Ciudades_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

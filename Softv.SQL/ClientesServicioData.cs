﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;
 
namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ClientesServicioData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ClientesServicio Data Access Object
    /// File                    : ClientesServicioDAO.cs
    /// Creation date           : 06/10/2017
    /// Creation time           : 11:19 a. m.
    ///</summary>
    public class ClientesServicioData : ClientesServicioProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="ClientesServicio"> Object ClientesServicio added to List</param>

        public override int? GetValidaPapoClienteServicio(int? Clv_UnicaNet){
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand comando = new SqlCommand("ValidaPapoClienteServicio");
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandTimeout = 0;
                    comando.Connection = connection;

                    SqlParameter par1 = new SqlParameter("@CantPAgo", SqlDbType.Int);
                    par1.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par1);

                    SqlParameter par2 = new SqlParameter("@Clv_UnicaNet", Clv_UnicaNet);
                    par2.Value = Clv_UnicaNet;
                    par2.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par2);

                    comando.ExecuteNonQuery();

                    result = Int32.Parse(par1.Value.ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }

            }
            return result;
        }

        public override int? GetEliminaClienteServicio(int? Clv_UnicaNet)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("EliminaClienteServicio", connection);
                AssingParameter(comandoSql, "@Clv_UnicaNet", Clv_UnicaNet);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("deleting ClientesServicio  " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }

        public override List<Servicios_NewEntity> GetValidaTVDigCliente(int? Contrato)
        {
            List<Servicios_NewEntity> ServicioList = new List<Servicios_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rangos.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("ValidaTVDigCliente", connection);
                AssingParameter(comandoSql, "@Contrato", Contrato);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        Servicios_NewEntity result = new Servicios_NewEntity();
                        result.Clv_Servicio = Int32.Parse(rd[0].ToString());
                        result.Descripcion = rd[1].ToString();
                        ServicioList.Add(result);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return ServicioList;
            }
        }

        public override int? GetAddPqueteAdic(ClientesServicioEntity ObjPqueteAdic)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("AddPqueteAdic", connection);
                AssingParameter(comandoSql, "@Clv_UnicaNet", null, pd: ParameterDirection.Output, IsKey: true);
                AssingParameter(comandoSql, "@Contrato", ObjPqueteAdic.Contrato);
                AssingParameter(comandoSql, "@Clv_Servicio", ObjPqueteAdic.Clv_Servicio);
                AssingParameter(comandoSql, "@status", ObjPqueteAdic.status);
                AssingParameter(comandoSql, "@fecha_solicitud", ObjPqueteAdic.fecha_solicitud);
                AssingParameter(comandoSql, "@fecha_instalacio", ObjPqueteAdic.fecha_instalacio);
                AssingParameter(comandoSql, "@fecha_suspension", ObjPqueteAdic.fecha_suspension);
                AssingParameter(comandoSql, "@fecha_baja", ObjPqueteAdic.fecha_baja);
                AssingParameter(comandoSql, "@fecha_Fuera_Area", ObjPqueteAdic.fecha_Fuera_Area);
                AssingParameter(comandoSql, "@FECHA_ULT_PAGO", ObjPqueteAdic.FECHA_ULT_PAGO);
                AssingParameter(comandoSql, "@PrimerMensualidad", ObjPqueteAdic.PrimerMensualidad);
                AssingParameter(comandoSql, "@ultimo_mes", ObjPqueteAdic.ultimo_mes);
                AssingParameter(comandoSql, "@ultimo_anio", ObjPqueteAdic.ultimo_anio);
                AssingParameter(comandoSql, "@primerMesAnt", ObjPqueteAdic.primerMesAnt);
                AssingParameter(comandoSql, "@statusAnt", ObjPqueteAdic.statusAnt);
                AssingParameter(comandoSql, "@facturaAnt", ObjPqueteAdic.facturaAnt);
                AssingParameter(comandoSql, "@GENERAOSINSTA", ObjPqueteAdic.GENERAOSINSTA);
                AssingParameter(comandoSql, "@factura", ObjPqueteAdic.factura);
                AssingParameter(comandoSql, "@Clv_Vendedor", ObjPqueteAdic.Clv_Vendedor);
                AssingParameter(comandoSql, "@Clv_Promocion", ObjPqueteAdic.Clv_Promocion);
                AssingParameter(comandoSql, "@Email", ObjPqueteAdic.Email);
                AssingParameter(comandoSql, "@Obs", ObjPqueteAdic.Obs);
                AssingParameter(comandoSql, "@CLV_MOTCAN", ObjPqueteAdic.CLV_MOTCAN);
                AssingParameter(comandoSql, "@Cortesia", ObjPqueteAdic.Cortesia);
                AssingParameter(comandoSql, "@Adic", ObjPqueteAdic.Adic);
                AssingParameter(comandoSql, "@TVSINPAGO", ObjPqueteAdic.TVSINPAGO);
                AssingParameter(comandoSql, "@TVCONPAGO", ObjPqueteAdic.TVCONPAGO);
                AssingParameter(comandoSql, "@IdMedio", ObjPqueteAdic.IdMedio);
                AssingParameter(comandoSql, "@Clv_usuarioCapturo", ObjPqueteAdic.Clv_usuarioCapturo);
                AssingParameter(comandoSql, "@ParentClv_UnicaNet", ObjPqueteAdic.ParentClv_UnicaNet);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@Clv_UnicaNet"].Value;
            }
            return result;
        }

        public override int AddClientesServicio(ClientesServicioEntity entity_ClientesServicio)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("NuevoClientesServicio", connection);

                AssingParameter(comandoSql, "@Clv_UnicaNet", null, pd: ParameterDirection.Output, IsKey: true);

                AssingParameter(comandoSql, "@Contrato", entity_ClientesServicio.Contrato);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_ClientesServicio.Clv_Servicio);

                AssingParameter(comandoSql, "@status", entity_ClientesServicio.status);

                AssingParameter(comandoSql, "@fecha_solicitud", entity_ClientesServicio.fecha_solicitud);

                AssingParameter(comandoSql, "@fecha_instalacio", entity_ClientesServicio.fecha_instalacio);

                AssingParameter(comandoSql, "@fecha_suspension", entity_ClientesServicio.fecha_suspension);

                AssingParameter(comandoSql, "@fecha_baja", entity_ClientesServicio.fecha_baja);

                AssingParameter(comandoSql, "@fecha_Fuera_Area", entity_ClientesServicio.fecha_Fuera_Area);

                AssingParameter(comandoSql, "@FECHA_ULT_PAGO", entity_ClientesServicio.FECHA_ULT_PAGO);

                AssingParameter(comandoSql, "@PrimerMensualidad", entity_ClientesServicio.PrimerMensualidad);

                AssingParameter(comandoSql, "@ultimo_mes", entity_ClientesServicio.ultimo_mes);

                AssingParameter(comandoSql, "@ultimo_anio", entity_ClientesServicio.ultimo_anio);

                AssingParameter(comandoSql, "@primerMesAnt", entity_ClientesServicio.primerMesAnt);

                AssingParameter(comandoSql, "@statusAnt", entity_ClientesServicio.statusAnt);

                AssingParameter(comandoSql, "@facturaAnt", entity_ClientesServicio.facturaAnt);

                AssingParameter(comandoSql, "@GENERAOSINSTA", entity_ClientesServicio.GENERAOSINSTA);

                AssingParameter(comandoSql, "@factura", entity_ClientesServicio.factura);

                AssingParameter(comandoSql, "@Clv_Vendedor", entity_ClientesServicio.Clv_Vendedor);

                AssingParameter(comandoSql, "@Clv_Promocion", entity_ClientesServicio.Clv_Promocion);

                AssingParameter(comandoSql, "@Email", entity_ClientesServicio.Email);

                AssingParameter(comandoSql, "@Obs", entity_ClientesServicio.Obs);

                AssingParameter(comandoSql, "@CLV_MOTCAN", entity_ClientesServicio.CLV_MOTCAN);

                AssingParameter(comandoSql, "@Cortesia", entity_ClientesServicio.Cortesia);

                AssingParameter(comandoSql, "@Adic", entity_ClientesServicio.Adic);

                AssingParameter(comandoSql, "@TVSINPAGO", entity_ClientesServicio.TVSINPAGO);

                AssingParameter(comandoSql, "@TVCONPAGO", entity_ClientesServicio.TVCONPAGO);

                AssingParameter(comandoSql, "@IdMedio", entity_ClientesServicio.IdMedio);

                AssingParameter(comandoSql, "@Clv_usuarioCapturo", entity_ClientesServicio.Clv_usuarioCapturo);

                AssingParameter(comandoSql, "@TipServ", entity_ClientesServicio.TipServ);

                AssingParameter(comandoSql, "@ParentClv_UnicaNet", entity_ClientesServicio.ParentClv_UnicaNet);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@Clv_UnicaNet"].Value;
            }
            return result;
        }

        public override Cobro_HotelEntity GetSaveCobroHoteles(Cobro_HotelEntity ObjCobro)
        {
            Cobro_HotelEntity result = new Cobro_HotelEntity();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("SaveCobroHoteles", connection);
                AssingParameter(comandoSql, "@Clv_servicio", ObjCobro.Clv_servicio);
                AssingParameter(comandoSql, "@Clv_tipser", ObjCobro.Clv_tipser);
                AssingParameter(comandoSql, "@Contratacion", ObjCobro.Contratacion);
                AssingParameter(comandoSql, "@Mensualidad", ObjCobro.Mensualidad);
                AssingParameter(comandoSql, "@Contrato", ObjCobro.Contrato);
                AssingParameter(comandoSql, "@Clv_UnicaNet", ObjCobro.Clv_UnicaNet);
                AssingParameter(comandoSql, "@Renta", ObjCobro.Renta);

                IDataReader rd = null;

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        result.result = Int32.Parse(rd[0].ToString());
                        result.MSJ = rd[1].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }

        public override Cobro_HotelEntity GetCobroHotelesByContratoClvUnicanet(Cobro_HotelEntity ObjCobro)
        {
            Cobro_HotelEntity List = new Cobro_HotelEntity();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rangos.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("CobroHotelesByContratoClvUnicanet", connection);
                AssingParameter(comandoSql, "@Contrato", ObjCobro.Contrato);
                AssingParameter(comandoSql, "@Clv_UnicaNet", ObjCobro.Clv_UnicaNet);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        List.Clv_servicio = Int32.Parse(rd[0].ToString());
                        List.Clv_txt = rd[1].ToString();
                        List.Clv_tipser = Int32.Parse(rd[2].ToString());
                        List.Contratacion = decimal.Parse(rd[3].ToString());
                        List.Mensualidad = decimal.Parse(rd[4].ToString());
                        List.Contrato = Int32.Parse(rd[5].ToString());
                        List.Clv_UnicaNet = Int32.Parse(rd[6].ToString());
                        List.Renta = decimal.Parse(rd[7].ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return List;
            }
        }

        public override int? GetDeleteCobroHoteles(Cobro_HotelEntity ObjCobro)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("DeleteCobroHoteles", connection);
                AssingParameter(comandoSql, "@Contrato", ObjCobro.Contrato);
                AssingParameter(comandoSql, "@Clv_UnicaNet", ObjCobro.Clv_UnicaNet);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding GetDeleteCobroHoteles " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }

        public override ValidaEntity GetValidaIPServicio(long? Clv_UnicaNet)
        {
            ValidaEntity List = new ValidaEntity();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rangos.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("ValidaIPServicio", connection);
                AssingParameter(comandoSql, "@Clv_UnicaNet", Clv_UnicaNet);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        List.Resultado = bool.Parse(rd[0].ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GetValidaIPServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return List;
            }
        }

        public override IpEntity GetConIPServicio(long? Clv_UnicaNet)
        {
            IpEntity List = new IpEntity();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rangos.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("ConIPServicio", connection);
                AssingParameter(comandoSql, "@Clv_UnicaNet", Clv_UnicaNet);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        List.Clv_UnicaNet = Int32.Parse(rd[0].ToString());
                        List.IdIP = Int32.Parse(rd[1].ToString());
                        List.IP_ALL = rd[2].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GetConIPServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return List;
            }
        }

        public override List<IpEntity> GetListaIPDisp(IpEntity ObjIP)
        {
            List<IpEntity> List = new List<IpEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rangos.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("ListaIPDisp", connection);
                AssingParameter(comandoSql, "@IdCompania", ObjIP.IdCompania);
                AssingParameter(comandoSql, "@Clv_Estado", ObjIP.Clv_Estado);
                AssingParameter(comandoSql, "@Clv_Ciudad", ObjIP.Clv_Ciudad);
                AssingParameter(comandoSql, "@Clv_Localidad", ObjIP.Clv_Localidad);
                AssingParameter(comandoSql, "@IdMedio", ObjIP.IdMedio);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        IpEntity result = new IpEntity();
                        result.IdIP = Int32.Parse(rd[0].ToString());
                        result.IP_ALL = rd[1].ToString();
                        List.Add(result);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GetListaIPDisp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return List;
            }
        }






















        public override int DeleteClientesServicio()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ClientesServicioDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a ClientesServicio
        ///</summary>
        /// <param name="ClientesServicio"> Objeto ClientesServicio a editar </param>
        public override int EditClientesServicio(ClientesServicioEntity entity_ClientesServicio)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Update_ClientesServicios", connection);

                AssingParameter(comandoSql, "@Clv_UnicaNet", entity_ClientesServicio.Clv_UnicaNet);

                AssingParameter(comandoSql, "@Contrato", entity_ClientesServicio.Contrato);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_ClientesServicio.Clv_Servicio);

                AssingParameter(comandoSql, "@status", entity_ClientesServicio.status);

                AssingParameter(comandoSql, "@fecha_solicitud", entity_ClientesServicio.fecha_solicitud);

                AssingParameter(comandoSql, "@fecha_instalacio", entity_ClientesServicio.fecha_instalacio);

                AssingParameter(comandoSql, "@fecha_suspension", entity_ClientesServicio.fecha_suspension);

                AssingParameter(comandoSql, "@fecha_baja", entity_ClientesServicio.fecha_baja);

                AssingParameter(comandoSql, "@fecha_Fuera_Area", entity_ClientesServicio.fecha_Fuera_Area);

                AssingParameter(comandoSql, "@FECHA_ULT_PAGO", entity_ClientesServicio.FECHA_ULT_PAGO);

                AssingParameter(comandoSql, "@PrimerMensualidad", entity_ClientesServicio.PrimerMensualidad);

                AssingParameter(comandoSql, "@ultimo_mes", entity_ClientesServicio.ultimo_mes);

                AssingParameter(comandoSql, "@ultimo_anio", entity_ClientesServicio.ultimo_anio);

                AssingParameter(comandoSql, "@primerMesAnt", entity_ClientesServicio.primerMesAnt);

                AssingParameter(comandoSql, "@statusAnt", entity_ClientesServicio.statusAnt);

                AssingParameter(comandoSql, "@facturaAnt", entity_ClientesServicio.facturaAnt);

                AssingParameter(comandoSql, "@GENERAOSINSTA", entity_ClientesServicio.GENERAOSINSTA);

                AssingParameter(comandoSql, "@factura", entity_ClientesServicio.factura);

                AssingParameter(comandoSql, "@Clv_Vendedor", entity_ClientesServicio.Clv_Vendedor);

                AssingParameter(comandoSql, "@Clv_Promocion", entity_ClientesServicio.Clv_Promocion);

                AssingParameter(comandoSql, "@Email", entity_ClientesServicio.Email);

                AssingParameter(comandoSql, "@Obs", entity_ClientesServicio.Obs);

                AssingParameter(comandoSql, "@CLV_MOTCAN", entity_ClientesServicio.CLV_MOTCAN);

                AssingParameter(comandoSql, "@Cortesia", entity_ClientesServicio.Cortesia);

                AssingParameter(comandoSql, "@Adic", entity_ClientesServicio.Adic);

                AssingParameter(comandoSql, "@TVSINPAGO", entity_ClientesServicio.TVSINPAGO);

                AssingParameter(comandoSql, "@TVCONPAGO", entity_ClientesServicio.TVCONPAGO);

                AssingParameter(comandoSql, "@IdMedio", entity_ClientesServicio.IdMedio);

                AssingParameter(comandoSql, "@Clv_usuarioCapturo", entity_ClientesServicio.Clv_usuarioCapturo);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all ClientesServicio
        ///</summary>
        public override List<ClientesServicioEntity> GetClientesServicio(long? Clv_UnicaNet)
        {
            List<ClientesServicioEntity> ClientesServicioList = new List<ClientesServicioEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MuestraInfo_ClientesServicios", connection);

                AssingParameter(comandoSql, "@Clv_UnicaNet", Clv_UnicaNet);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ClientesServicioList.Add(GetClientesServicioFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ClientesServicioList;
        }

        /// <summary>
        /// Gets all ClientesServicio by List<int>
        ///</summary>
        public override List<ClientesServicioEntity> GetClientesServicio(List<int> lid)
        {
            List<ClientesServicioEntity> ClientesServicioList = new List<ClientesServicioEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_ClientesServicioGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ClientesServicioList.Add(GetClientesServicioFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ClientesServicioList;
        }

        /// <summary>
        /// Gets ClientesServicio by
        ///</summary>
        public override ClientesServicioEntity GetClientesServicioById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ClientesServicioGetById", connection);
                ClientesServicioEntity entity_ClientesServicio = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_ClientesServicio = GetClientesServicioFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_ClientesServicio;
            }

        }



        /// <summary>
        ///Get ClientesServicio
        ///</summary>
        public override SoftvList<ClientesServicioEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ClientesServicioEntity> entities = new SoftvList<ClientesServicioEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ClientesServicioGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetClientesServicioFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetClientesServicioCount();
                return entities ?? new SoftvList<ClientesServicioEntity>();
            }
        }

        /// <summary>
        ///Get ClientesServicio
        ///</summary>
        public override SoftvList<ClientesServicioEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ClientesServicioEntity> entities = new SoftvList<ClientesServicioEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ClientesServicioGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetClientesServicioFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetClientesServicioCount(xml);
                return entities ?? new SoftvList<ClientesServicioEntity>();
            }
        }

        /// <summary>
        ///Get Count ClientesServicio
        ///</summary>
        public int GetClientesServicioCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ClientesServicioGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count ClientesServicio
        ///</summary>
        public int GetClientesServicioCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ClientesServicio.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ClientesServicioGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ClientesServicio " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

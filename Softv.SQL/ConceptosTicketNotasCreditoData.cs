﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ConceptosTicketNotasCreditoData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ConceptosTicketNotasCredito Data Access Object
    /// File                    : ConceptosTicketNotasCreditoDAO.cs
    /// Creation date           : 01/03/2017
    /// Creation time           : 01:50 p. m.
    ///</summary>
    public class ConceptosTicketNotasCreditoData : ConceptosTicketNotasCreditoProvider
    {
        
        public override List<ConceptosTicketNotasCreditoEntity> GetConceptosTicketNotasCredito(long? Clv_Factura)
        {
            List<ConceptosTicketNotasCreditoEntity> ConceptosTicketNotasCreditoList = new List<ConceptosTicketNotasCreditoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ConceptosTicketNotasCredito.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ConceptosNotaCredito", connection);

                AssingParameter(comandoSql, "@Clv_Factura", Clv_Factura);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ConceptosTicketNotasCreditoList.Add(GetConceptosTicketNotasCreditoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ConceptosTicketNotasCredito " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ConceptosTicketNotasCreditoList;
        }
        
        public override List<ConceptosTicketNotasCreditoEntity> GetConceptosTicketNotasCreditoCM(long? Clv_Factura)
        {
            List<ConceptosTicketNotasCreditoEntity> ConceptosTicketNotasCreditoList = new List<ConceptosTicketNotasCreditoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ConceptosTicketNotasCredito.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ConceptosNotaCredito_CM", connection);

                AssingParameter(comandoSql, "@Clv_Factura", Clv_Factura);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ConceptosTicketNotasCreditoList.Add(GetConceptosTicketNotasCreditoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ConceptosTicketNotasCredito " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ConceptosTicketNotasCreditoList;
        }
        

        #region Customs Methods

        #endregion
    }
}

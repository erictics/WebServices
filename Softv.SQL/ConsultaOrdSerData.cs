﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ConsultaOrdSerData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ConsultaOrdSer Data Access Object
    /// File                    : ConsultaOrdSerDAO.cs
    /// Creation date           : 13/04/2017
    /// Creation time           : 11:05 a. m.
    ///</summary>
    public class ConsultaOrdSerData : ConsultaOrdSerProvider
    {
       
        public override ConsultaOrdSerEntity GetConsultaOrdSerById(long? Clv_Orden)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ConsultaOrdSer.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("softvweb_ConsultaOrdSer", connection);
                ConsultaOrdSerEntity entity_ConsultaOrdSer = null;

                AssingParameter(comandoSql, "@ClvOrdSer", Clv_Orden);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_ConsultaOrdSer = GetConsultaOrdSerFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ConsultaOrdSer " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_ConsultaOrdSer;
            }

        }



        #region Customs Methods

        #endregion
    }
}

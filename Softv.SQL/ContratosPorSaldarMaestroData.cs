﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ContratosPorSaldarMaestroData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ContratosPorSaldarMaestro Data Access Object
    /// File                    : ContratosPorSaldarMaestroDAO.cs
    /// Creation date           : 05/07/2017
    /// Creation time           : 05:47 p. m.
    ///</summary>
    public class ContratosPorSaldarMaestroData : ContratosPorSaldarMaestroProvider
    {
        
        public override List<ContratosPorSaldarMaestroEntity> GetContratosPorSaldarMaestro(String FechaFacturacion)
        {
            List<ContratosPorSaldarMaestroEntity> ContratosPorSaldarMaestroList = new List<ContratosPorSaldarMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ContratosPorSaldarMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ContratosPorSaldarMaestro", connection);

                AssingParameter(comandoSql, "@FechaFacturacion", FechaFacturacion);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ContratosPorSaldarMaestroList.Add(GetContratosPorSaldarMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ContratosPorSaldarMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ContratosPorSaldarMaestroList;
        }


        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.DameBonificacionData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameBonificacion Data Access Object
    /// File                    : DameBonificacionDAO.cs
    /// Creation date           : 30/01/2017
    /// Creation time           : 12:35 p. m.
    ///</summary>
    public class DameBonificacionData : DameBonificacionProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="DameBonificacion"> Object DameBonificacion added to List</param>
        public override int AddDameBonificacion(DameBonificacionEntity entity_DameBonificacion)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("InsertaBonificacion", connection);

                AssingParameter(comandoSql, "@Clv_queja", entity_DameBonificacion.Clv_queja);

                AssingParameter(comandoSql, "@dias", entity_DameBonificacion.dias);

                AssingParameter(comandoSql, "@comentario", entity_DameBonificacion.comentario);

                AssingParameter(comandoSql, "@ClvUsuario", entity_DameBonificacion.ClvUsuario);

                AssingParameter(comandoSql, "@BndPorMonto", entity_DameBonificacion.BndPorMonto);

                AssingParameter(comandoSql, "@Monto", entity_DameBonificacion.Monto);

                AssingParameter(comandoSql, "@Montofac", entity_DameBonificacion.Montofac);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                //result = (int)comandoSql.Parameters["@IdDameBonificacion"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a DameBonificacion
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteDameBonificacion(long? Clv_queja, int? ClvUsuario)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("EliminaBonificacion", connection);

                AssingParameter(comandoSql, "@Clv_queja", Clv_queja);
                AssingParameter(comandoSql, "@ClvUsuario", ClvUsuario);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a DameBonificacion
        ///</summary>
        /// <param name="DameBonificacion"> Objeto DameBonificacion a editar </param>
        public override int EditDameBonificacion(DameBonificacionEntity entity_DameBonificacion)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameBonificacionEdit", connection);

                AssingParameter(comandoSql, "@dias", entity_DameBonificacion.dias);

                AssingParameter(comandoSql, "@comentario", entity_DameBonificacion.comentario);

                AssingParameter(comandoSql, "@tieneBonificacion", entity_DameBonificacion.tieneBonificacion);

                AssingParameter(comandoSql, "@aplicada", entity_DameBonificacion.aplicada);

                AssingParameter(comandoSql, "@clv_factura", entity_DameBonificacion.clv_factura);

                AssingParameter(comandoSql, "@fecha", entity_DameBonificacion.fecha);

                AssingParameter(comandoSql, "@cantidad", entity_DameBonificacion.cantidad);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all DameBonificacion
        ///</summary>
        public override List<DameBonificacionEntity> GetDameBonificacion(long? Clv_queja)
        {
            List<DameBonificacionEntity> DameBonificacionList = new List<DameBonificacionEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("DameBonificacionweb", connection);

                AssingParameter(comandoSql, "@Clv_queja", Clv_queja);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DameBonificacionList.Add(GetDameBonificacionFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameBonificacionList;
        }

        //public override List<DameBonificacionEntity> GetDamePrecioBonificaciob(long? Clv_queja, int? dias)
        //{
        //    List<DameBonificacionEntity> DameBonificacionList = new List<DameBonificacionEntity>();
        //    using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
        //    {

        //        SqlCommand comandoSql = CreateCommand("sp_PrecioDiasBonificacion", connection);

        //        AssingParameter(comandoSql, "@Clv_queja", Clv_queja);
        //        AssingParameter(comandoSql, "@dias", dias);

        //        IDataReader rd = null;
        //        try
        //        {
        //            if (connection.State == ConnectionState.Closed)
        //                connection.Open();
        //            rd = ExecuteReader(comandoSql);

        //            while (rd.Read())
        //            {
        //                DameBonificacionList.Add(GetDamePDBonificacionFromReader(rd));
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
        //        }
        //        finally
        //        {
        //            if (connection != null)
        //                connection.Close();
        //            if (rd != null)
        //                rd.Close();
        //        }
        //    }
        //    return DameBonificacionList;
        //}


        /// <summary>
        /// Gets all DameBonificacion by List<int>
        ///</summary>
        public override List<DameBonificacionEntity> GetDameBonificacion(List<int> lid)
        {
            List<DameBonificacionEntity> DameBonificacionList = new List<DameBonificacionEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_DameBonificacionGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DameBonificacionList.Add(GetDameBonificacionFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameBonificacionList;
        }

        /// <summary>
        /// Gets DameBonificacion by
        ///</summary>
        public override DameBonificacionEntity GetDameBonificacionById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameBonificacionGetById", connection);
                DameBonificacionEntity entity_DameBonificacion = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_DameBonificacion = GetDameBonificacionFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_DameBonificacion;
            }

        }


        // Tabla 2
        public override List<DameBonificacionEntity> GetDamePrecioBonificaciob(long? Clv_queja, int? dias)
        {
            List<DameBonificacionEntity> DameBonificacionList = new List<DameBonificacionEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("sp_PrecioDiasBonificacion", connection);

                AssingParameter(comandoSql, "@Clv_queja", Clv_queja);
                AssingParameter(comandoSql, "@dias", dias);

                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                SqlDataReader rd = comandoSql.ExecuteReader(); //obtiene todas las tablas del query
                try
                {
                    if (rd.HasRows)
                    {
                        while (rd.Read()) //1 NO TOMAR
                        {
                        }
                        if (rd.NextResult()) //2
                        {
                            while (rd.Read())
                            {
                                DameBonificacionList.Add(GetDamePDBonificacionFromReader(rd));
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameBonificacionList;
        }

        //Tabla 2
        public override List<DameBonificacionEntity> GetImporteFactura(long? Clv_queja, bool? BndPorMonto)
        {
            List<DameBonificacionEntity> DameBonificacionList = new List<DameBonificacionEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("sp_ImporteFacturacion", connection);

                AssingParameter(comandoSql, "@clvQueja", Clv_queja);
                AssingParameter(comandoSql, "@BndPorMonto", BndPorMonto);

                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                SqlDataReader rd = comandoSql.ExecuteReader(); //obtiene todas las tablas del query
                try
                {
                    if (rd.HasRows)
                    {
                        while (rd.Read()) //1 NO TOMAR
                        {
                        }
                        if (rd.NextResult()) //2
                        {
                            while (rd.Read())
                            {
                                DameBonificacionList.Add(GetDamePDBonificacionFromReader(rd));
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameBonificacionList;
        }


        /// <summary>
        ///Get DameBonificacion
        ///</summary>
        public override SoftvList<DameBonificacionEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<DameBonificacionEntity> entities = new SoftvList<DameBonificacionEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameBonificacionGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDameBonificacionFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDameBonificacionCount();
                return entities ?? new SoftvList<DameBonificacionEntity>();
            }
        }

        /// <summary>
        ///Get DameBonificacion
        ///</summary>
        public override SoftvList<DameBonificacionEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<DameBonificacionEntity> entities = new SoftvList<DameBonificacionEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameBonificacionGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDameBonificacionFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDameBonificacionCount(xml);
                return entities ?? new SoftvList<DameBonificacionEntity>();
            }
        }

        /// <summary>
        ///Get Count DameBonificacion
        ///</summary>
        public int GetDameBonificacionCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameBonificacionGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count DameBonificacion
        ///</summary>
        public int GetDameBonificacionCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameBonificacionGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

      //  public int GetDAMEUTLIMODIA()
      //  {
      //      int result = 0;
      //      using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameBonificacion.ConnectionString))
      //      {
              
      //          try
      //          {
      //              if (connection.State == ConnectionState.Closed)
      //                  connection.Open();
      //              SqlCommand strSQL = new SqlCommand("DAMEUTLIMODIA");
      //              strSQL.Connection = connection;
      //              strSQL.CommandType = CommandType.StoredProcedure;
      //              strSQL.CommandTimeout = 0;

      //              SqlParameter par3 = new SqlParameter("@DIA", SqlDbType.Int);
      //              par3.Direction = ParameterDirection.Output;
      //              strSQL.Parameters.Add(par3);
      //              result = int.Parse(par3.Value.ToString());

      //          }
      //          catch (Exception ex)
      //          {
      //              throw new Exception("Error getting data DameBonificacion " + ex.Message, ex);
      //          }
      //          finally
      //          {
      //              if (connection != null)
      //                  connection.Close();
      //          }
      //      }
      //      return result;
      //  }

      //public   DameFilaPreDetFacturas_2

        #region Customs Methods

        #endregion
    }
}

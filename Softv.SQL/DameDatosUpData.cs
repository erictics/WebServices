﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.DameDatosUpData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameDatosUp Data Access Object
    /// File                    : DameDatosUpDAO.cs
    /// Creation date           : 17/10/2016
    /// Creation time           : 12:17 p. m.
    ///</summary>
    public class DameDatosUpData : DameDatosUpProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="DameDatosUp"> Object DameDatosUp added to List</param>
        public override int AddDameDatosUp(DameDatosUpEntity entity_DameDatosUp)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpAdd", connection);

                //AssingParameter(comandoSql, "@nombreD", entity_DameDatosUp.nombreD);

                //AssingParameter(comandoSql, "@Descripcion", entity_DameDatosUp.Descripcion);

                //AssingParameter(comandoSql, "@Clv_Sucursal", entity_DameDatosUp.Clv_Sucursal);

                //AssingParameter(comandoSql, "@NombreS", entity_DameDatosUp.NombreS);

                //AssingParameter(comandoSql, "@IP", entity_DameDatosUp.IP);

                //AssingParameter(comandoSql, "@Impresora", entity_DameDatosUp.Impresora);

                //AssingParameter(comandoSql, "@Clv_Equivalente", entity_DameDatosUp.Clv_Equivalente);

                //AssingParameter(comandoSql, "@Serie", entity_DameDatosUp.Serie);

                //AssingParameter(comandoSql, "@UltimoFolioUsado", entity_DameDatosUp.UltimoFolioUsado);

                //AssingParameter(comandoSql, "@Clave", entity_DameDatosUp.Clave);

                //AssingParameter(comandoSql, "@Clv_Usuario", entity_DameDatosUp.Clv_Usuario);

                //AssingParameter(comandoSql, "@Nombre", entity_DameDatosUp.Nombre);

                //AssingParameter(comandoSql, "@Domicilio", entity_DameDatosUp.Domicilio);

                //AssingParameter(comandoSql, "@Colonia", entity_DameDatosUp.Colonia);

                //AssingParameter(comandoSql, "@FechaIngreso", entity_DameDatosUp.FechaIngreso);

                //AssingParameter(comandoSql, "@FechaSalida", entity_DameDatosUp.FechaSalida);

                //AssingParameter(comandoSql, "@Activo", entity_DameDatosUp.Activo);

                //AssingParameter(comandoSql, "@Pasaporte", entity_DameDatosUp.Pasaporte);

                //AssingParameter(comandoSql, "@Clv_TipoUsuario", entity_DameDatosUp.Clv_TipoUsuario);

                //AssingParameter(comandoSql, "@CATV", entity_DameDatosUp.CATV);

                //AssingParameter(comandoSql, "@Facturacion", entity_DameDatosUp.Facturacion);

                //AssingParameter(comandoSql, "@Boletos", entity_DameDatosUp.Boletos);

                //AssingParameter(comandoSql, "@Mizar_AN", entity_DameDatosUp.Mizar_AN);

                //AssingParameter(comandoSql, "@Fecha", entity_DameDatosUp.Fecha);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdDameDatosUp"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a DameDatosUp
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteDameDatosUp()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a DameDatosUp
        ///</summary>
        /// <param name="DameDatosUp"> Objeto DameDatosUp a editar </param>
        public override int EditDameDatosUp(DameDatosUpEntity entity_DameDatosUp)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpEdit", connection);

                //AssingParameter(comandoSql, "@nombreD", entity_DameDatosUp.nombreD);

                //AssingParameter(comandoSql, "@Descripcion", entity_DameDatosUp.Descripcion);

                //AssingParameter(comandoSql, "@Clv_Sucursal", entity_DameDatosUp.Clv_Sucursal);

                //AssingParameter(comandoSql, "@NombreS", entity_DameDatosUp.NombreS);

                //AssingParameter(comandoSql, "@IP", entity_DameDatosUp.IP);

                //AssingParameter(comandoSql, "@Impresora", entity_DameDatosUp.Impresora);

                //AssingParameter(comandoSql, "@Clv_Equivalente", entity_DameDatosUp.Clv_Equivalente);

                //AssingParameter(comandoSql, "@Serie", entity_DameDatosUp.Serie);

                //AssingParameter(comandoSql, "@UltimoFolioUsado", entity_DameDatosUp.UltimoFolioUsado);

                //AssingParameter(comandoSql, "@Clave", entity_DameDatosUp.Clave);

                //AssingParameter(comandoSql, "@Clv_Usuario", entity_DameDatosUp.Clv_Usuario);

                //AssingParameter(comandoSql, "@Nombre", entity_DameDatosUp.Nombre);

                //AssingParameter(comandoSql, "@Domicilio", entity_DameDatosUp.Domicilio);

                //AssingParameter(comandoSql, "@Colonia", entity_DameDatosUp.Colonia);

                //AssingParameter(comandoSql, "@FechaIngreso", entity_DameDatosUp.FechaIngreso);

                //AssingParameter(comandoSql, "@FechaSalida", entity_DameDatosUp.FechaSalida);

                //AssingParameter(comandoSql, "@Activo", entity_DameDatosUp.Activo);

                //AssingParameter(comandoSql, "@Pasaporte", entity_DameDatosUp.Pasaporte);

                //AssingParameter(comandoSql, "@Clv_TipoUsuario", entity_DameDatosUp.Clv_TipoUsuario);

                //AssingParameter(comandoSql, "@CATV", entity_DameDatosUp.CATV);

                //AssingParameter(comandoSql, "@Facturacion", entity_DameDatosUp.Facturacion);

                //AssingParameter(comandoSql, "@Boletos", entity_DameDatosUp.Boletos);

                //AssingParameter(comandoSql, "@Mizar_AN", entity_DameDatosUp.Mizar_AN);

                //AssingParameter(comandoSql, "@Fecha", entity_DameDatosUp.Fecha);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all DameDatosUp
        ///</summary>
        public override List<DameDatosUpEntity> GetDameDatosUp(long? Clave, String Clv_Usuario, long? Clv_Sucursal, String IpMaquina)
        {
            List<DameDatosUpEntity> DameDatosUpList = new List<DameDatosUpEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("DameDatosUp", connection);

                AssingParameter(comandoSql, "@Clave", Clave);
                AssingParameter(comandoSql, "@Clv_Usuario", Clv_Usuario);
                AssingParameter(comandoSql, "@Clv_Sucursal", Clv_Sucursal);
                AssingParameter(comandoSql, "@IpMaquina", IpMaquina);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DameDatosUpList.Add(GetDameDatosUpFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameDatosUpList;
        }

        /// <summary>
        /// Gets all DameDatosUp by List<int>
        ///</summary>
        public override List<DameDatosUpEntity> GetDameDatosUp(List<int> lid)
        {
            List<DameDatosUpEntity> DameDatosUpList = new List<DameDatosUpEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DameDatosUpList.Add(GetDameDatosUpFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameDatosUpList;
        }

        /// <summary>
        /// Gets DameDatosUp by
        ///</summary>
        public override DameDatosUpEntity GetDameDatosUpById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpGetById", connection);
                DameDatosUpEntity entity_DameDatosUp = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_DameDatosUp = GetDameDatosUpFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_DameDatosUp;
            }

        }



        /// <summary>
        ///Get DameDatosUp
        ///</summary>
        public override SoftvList<DameDatosUpEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<DameDatosUpEntity> entities = new SoftvList<DameDatosUpEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDameDatosUpFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDameDatosUpCount();
                return entities ?? new SoftvList<DameDatosUpEntity>();
            }
        }

        /// <summary>
        ///Get DameDatosUp
        ///</summary>
        public override SoftvList<DameDatosUpEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<DameDatosUpEntity> entities = new SoftvList<DameDatosUpEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDameDatosUpFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDameDatosUpCount(xml);
                return entities ?? new SoftvList<DameDatosUpEntity>();
            }
        }

        /// <summary>
        ///Get Count DameDatosUp
        ///</summary>
        public int GetDameDatosUpCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count DameDatosUp
        ///</summary>
        public int GetDameDatosUpCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDatosUp.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDatosUpGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDatosUp " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

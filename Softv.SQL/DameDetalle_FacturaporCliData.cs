﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.DameDetalle_FacturaporCliData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameDetalle_FacturaporCli Data Access Object
    /// File                    : DameDetalle_FacturaporCliDAO.cs
    /// Creation date           : 22/05/2017
    /// Creation time           : 01:43 p. m.
    ///</summary>
    public class DameDetalle_FacturaporCliData : DameDetalle_FacturaporCliProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="DameDetalle_FacturaporCli"> Object DameDetalle_FacturaporCli added to List</param>
        public override int AddDameDetalle_FacturaporCli(DameDetalle_FacturaporCliEntity entity_DameDetalle_FacturaporCli)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliAdd", connection);

                AssingParameter(comandoSql, "@CLV_DETALLE", entity_DameDetalle_FacturaporCli.CLV_DETALLE);

                AssingParameter(comandoSql, "@CLAVE", entity_DameDetalle_FacturaporCli.CLAVE);

                AssingParameter(comandoSql, "@DESCORTA", entity_DameDetalle_FacturaporCli.DESCORTA);

                AssingParameter(comandoSql, "@Pagos_Adelantados", entity_DameDetalle_FacturaporCli.Pagos_Adelantados);

                AssingParameter(comandoSql, "@tvAdic", entity_DameDetalle_FacturaporCli.tvAdic);

                AssingParameter(comandoSql, "@Meses_Cortesia", entity_DameDetalle_FacturaporCli.Meses_Cortesia);

                AssingParameter(comandoSql, "@mesesApagar", entity_DameDetalle_FacturaporCli.mesesApagar);

                AssingParameter(comandoSql, "@importe", entity_DameDetalle_FacturaporCli.importe);

                AssingParameter(comandoSql, "@periodoPagadoIni", entity_DameDetalle_FacturaporCli.periodoPagadoIni);

                AssingParameter(comandoSql, "@periodoPagadoFin", entity_DameDetalle_FacturaporCli.periodoPagadoFin);

                AssingParameter(comandoSql, "@PuntosAplicadosOtros", entity_DameDetalle_FacturaporCli.PuntosAplicadosOtros);

                AssingParameter(comandoSql, "@puntosAplicadosAnt", entity_DameDetalle_FacturaporCli.puntosAplicadosAnt);

                AssingParameter(comandoSql, "@PuntosAplicadosPagoAdelantado", entity_DameDetalle_FacturaporCli.PuntosAplicadosPagoAdelantado);

                AssingParameter(comandoSql, "@DescuentoNet", entity_DameDetalle_FacturaporCli.DescuentoNet);

                AssingParameter(comandoSql, "@Des_Otr_Ser_Misma_Categoria", entity_DameDetalle_FacturaporCli.Des_Otr_Ser_Misma_Categoria);

                AssingParameter(comandoSql, "@bonificacion", entity_DameDetalle_FacturaporCli.bonificacion);

                AssingParameter(comandoSql, "@importeAdicional", entity_DameDetalle_FacturaporCli.importeAdicional);

                AssingParameter(comandoSql, "@columnaDetalle", entity_DameDetalle_FacturaporCli.columnaDetalle);

                AssingParameter(comandoSql, "@DiasBonifica", entity_DameDetalle_FacturaporCli.DiasBonifica);

                AssingParameter(comandoSql, "@mesesBonificar", entity_DameDetalle_FacturaporCli.mesesBonificar);

                AssingParameter(comandoSql, "@importeBonifica", entity_DameDetalle_FacturaporCli.importeBonifica);

                AssingParameter(comandoSql, "@Ultimo_Mes", entity_DameDetalle_FacturaporCli.Ultimo_Mes);

                AssingParameter(comandoSql, "@Ultimo_anio", entity_DameDetalle_FacturaporCli.Ultimo_anio);

                AssingParameter(comandoSql, "@Adelantado", entity_DameDetalle_FacturaporCli.Adelantado);

                AssingParameter(comandoSql, "@DESCRIPCION", entity_DameDetalle_FacturaporCli.DESCRIPCION);

                AssingParameter(comandoSql, "@MacCableModem", entity_DameDetalle_FacturaporCli.MacCableModem);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdDameDetalle_FacturaporCli"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a DameDetalle_FacturaporCli
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteDameDetalle_FacturaporCli()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a DameDetalle_FacturaporCli
        ///</summary>
        /// <param name="DameDetalle_FacturaporCli"> Objeto DameDetalle_FacturaporCli a editar </param>
        public override int EditDameDetalle_FacturaporCli(DameDetalle_FacturaporCliEntity entity_DameDetalle_FacturaporCli)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliEdit", connection);

                AssingParameter(comandoSql, "@CLV_DETALLE", entity_DameDetalle_FacturaporCli.CLV_DETALLE);

                AssingParameter(comandoSql, "@CLAVE", entity_DameDetalle_FacturaporCli.CLAVE);

                AssingParameter(comandoSql, "@DESCORTA", entity_DameDetalle_FacturaporCli.DESCORTA);

                AssingParameter(comandoSql, "@Pagos_Adelantados", entity_DameDetalle_FacturaporCli.Pagos_Adelantados);

                AssingParameter(comandoSql, "@tvAdic", entity_DameDetalle_FacturaporCli.tvAdic);

                AssingParameter(comandoSql, "@Meses_Cortesia", entity_DameDetalle_FacturaporCli.Meses_Cortesia);

                AssingParameter(comandoSql, "@mesesApagar", entity_DameDetalle_FacturaporCli.mesesApagar);

                AssingParameter(comandoSql, "@importe", entity_DameDetalle_FacturaporCli.importe);

                AssingParameter(comandoSql, "@periodoPagadoIni", entity_DameDetalle_FacturaporCli.periodoPagadoIni);

                AssingParameter(comandoSql, "@periodoPagadoFin", entity_DameDetalle_FacturaporCli.periodoPagadoFin);

                AssingParameter(comandoSql, "@PuntosAplicadosOtros", entity_DameDetalle_FacturaporCli.PuntosAplicadosOtros);

                AssingParameter(comandoSql, "@puntosAplicadosAnt", entity_DameDetalle_FacturaporCli.puntosAplicadosAnt);

                AssingParameter(comandoSql, "@PuntosAplicadosPagoAdelantado", entity_DameDetalle_FacturaporCli.PuntosAplicadosPagoAdelantado);

                AssingParameter(comandoSql, "@DescuentoNet", entity_DameDetalle_FacturaporCli.DescuentoNet);

                AssingParameter(comandoSql, "@Des_Otr_Ser_Misma_Categoria", entity_DameDetalle_FacturaporCli.Des_Otr_Ser_Misma_Categoria);

                AssingParameter(comandoSql, "@bonificacion", entity_DameDetalle_FacturaporCli.bonificacion);

                AssingParameter(comandoSql, "@importeAdicional", entity_DameDetalle_FacturaporCli.importeAdicional);

                AssingParameter(comandoSql, "@columnaDetalle", entity_DameDetalle_FacturaporCli.columnaDetalle);

                AssingParameter(comandoSql, "@DiasBonifica", entity_DameDetalle_FacturaporCli.DiasBonifica);

                AssingParameter(comandoSql, "@mesesBonificar", entity_DameDetalle_FacturaporCli.mesesBonificar);

                AssingParameter(comandoSql, "@importeBonifica", entity_DameDetalle_FacturaporCli.importeBonifica);

                AssingParameter(comandoSql, "@Ultimo_Mes", entity_DameDetalle_FacturaporCli.Ultimo_Mes);

                AssingParameter(comandoSql, "@Ultimo_anio", entity_DameDetalle_FacturaporCli.Ultimo_anio);

                AssingParameter(comandoSql, "@Adelantado", entity_DameDetalle_FacturaporCli.Adelantado);

                AssingParameter(comandoSql, "@DESCRIPCION", entity_DameDetalle_FacturaporCli.DESCRIPCION);

                AssingParameter(comandoSql, "@MacCableModem", entity_DameDetalle_FacturaporCli.MacCableModem);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all DameDetalle_FacturaporCli
        ///</summary>
        public override List<DameDetalle_FacturaporCliEntity> GetDameDetalle_FacturaporCli(long? Clv_FacturaCli, long? Clv_Session)
        {
            List<DameDetalle_FacturaporCliEntity> DameDetalle_FacturaporCliList = new List<DameDetalle_FacturaporCliEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("DameDetalle_FacturaporCli", connection);

                AssingParameter(comandoSql, "@Clv_FacturaCli", Clv_FacturaCli);
                AssingParameter(comandoSql, "@Clv_Session", Clv_Session);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DameDetalle_FacturaporCliList.Add(GetDameDetalle_FacturaporCliFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameDetalle_FacturaporCliList;
        }

        /// <summary>
        /// Gets all DameDetalle_FacturaporCli by List<int>
        ///</summary>
        public override List<DameDetalle_FacturaporCliEntity> GetDameDetalle_FacturaporCli(List<int> lid)
        {
            List<DameDetalle_FacturaporCliEntity> DameDetalle_FacturaporCliList = new List<DameDetalle_FacturaporCliEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DameDetalle_FacturaporCliList.Add(GetDameDetalle_FacturaporCliFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameDetalle_FacturaporCliList;
        }

        /// <summary>
        /// Gets DameDetalle_FacturaporCli by
        ///</summary>
        public override DameDetalle_FacturaporCliEntity GetDameDetalle_FacturaporCliById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliGetById", connection);
                DameDetalle_FacturaporCliEntity entity_DameDetalle_FacturaporCli = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_DameDetalle_FacturaporCli = GetDameDetalle_FacturaporCliFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_DameDetalle_FacturaporCli;
            }

        }



        /// <summary>
        ///Get DameDetalle_FacturaporCli
        ///</summary>
        public override SoftvList<DameDetalle_FacturaporCliEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<DameDetalle_FacturaporCliEntity> entities = new SoftvList<DameDetalle_FacturaporCliEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDameDetalle_FacturaporCliFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDameDetalle_FacturaporCliCount();
                return entities ?? new SoftvList<DameDetalle_FacturaporCliEntity>();
            }
        }

        /// <summary>
        ///Get DameDetalle_FacturaporCli
        ///</summary>
        public override SoftvList<DameDetalle_FacturaporCliEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<DameDetalle_FacturaporCliEntity> entities = new SoftvList<DameDetalle_FacturaporCliEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDameDetalle_FacturaporCliFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDameDetalle_FacturaporCliCount(xml);
                return entities ?? new SoftvList<DameDetalle_FacturaporCliEntity>();
            }
        }

        /// <summary>
        ///Get Count DameDetalle_FacturaporCli
        ///</summary>
        public int GetDameDetalle_FacturaporCliCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count DameDetalle_FacturaporCli
        ///</summary>
        public int GetDameDetalle_FacturaporCliCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameDetalle_FacturaporCli.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DameDetalle_FacturaporCliGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameDetalle_FacturaporCli " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.DameTiposClientesData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DameTiposClientes Data Access Object
    /// File                    : DameTiposClientesDAO.cs
    /// Creation date           : 14/10/2016
    /// Creation time           : 11:17 a. m.
    ///</summary>
    public class DameTiposClientesData : DameTiposClientesProvider
    {


        /// <summary>
        /// Gets all DameTiposClientes
        ///</summary>
        public override List<DameTiposClientesEntity> GetDameTiposClientes(long? Contrato)
        {
            List<DameTiposClientesEntity> DameTiposClientesList = new List<DameTiposClientesEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DameTiposClientes.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("DameTiposClientes", connection);

                AssingParameter(comandoSql, "@Contrato", Contrato);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DameTiposClientesList.Add(GetDameTiposClientesFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameTiposClientes " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DameTiposClientesList;
        }


        #region Customs Methods

        #endregion
    }
}

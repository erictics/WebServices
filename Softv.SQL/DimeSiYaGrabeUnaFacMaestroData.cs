﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.DimeSiYaGrabeUnaFacMaestroData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : DimeSiYaGrabeUnaFacMaestro Data Access Object
    /// File                    : DimeSiYaGrabeUnaFacMaestroDAO.cs
    /// Creation date           : 27/03/2017
    /// Creation time           : 12:04 p. m.
    ///</summary>
    public class DimeSiYaGrabeUnaFacMaestroData : DimeSiYaGrabeUnaFacMaestroProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="DimeSiYaGrabeUnaFacMaestro"> Object DimeSiYaGrabeUnaFacMaestro added to List</param>
        public override int AddDimeSiYaGrabeUnaFacMaestro(DimeSiYaGrabeUnaFacMaestroEntity entity_DimeSiYaGrabeUnaFacMaestro)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroAdd", connection);

                AssingParameter(comandoSql, "@ContratoMae", entity_DimeSiYaGrabeUnaFacMaestro.ContratoMae);

                AssingParameter(comandoSql, "@Valida", entity_DimeSiYaGrabeUnaFacMaestro.Valida);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdDimeSiYaGrabeUnaFacMaestro"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteDimeSiYaGrabeUnaFacMaestro()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        /// <param name="DimeSiYaGrabeUnaFacMaestro"> Objeto DimeSiYaGrabeUnaFacMaestro a editar </param>
        public override int EditDimeSiYaGrabeUnaFacMaestro(DimeSiYaGrabeUnaFacMaestroEntity entity_DimeSiYaGrabeUnaFacMaestro)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroEdit", connection);

                AssingParameter(comandoSql, "@ContratoMae", entity_DimeSiYaGrabeUnaFacMaestro.ContratoMae);

                AssingParameter(comandoSql, "@Valida", entity_DimeSiYaGrabeUnaFacMaestro.Valida);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        public override List<DimeSiYaGrabeUnaFacMaestroEntity> GetDimeSiYaGrabeUnaFacMaestro()
        {
            List<DimeSiYaGrabeUnaFacMaestroEntity> DimeSiYaGrabeUnaFacMaestroList = new List<DimeSiYaGrabeUnaFacMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DimeSiYaGrabeUnaFacMaestroList.Add(GetDimeSiYaGrabeUnaFacMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DimeSiYaGrabeUnaFacMaestroList;
        }

        /// <summary>
        /// Gets all DimeSiYaGrabeUnaFacMaestro by List<int>
        ///</summary>
        public override List<DimeSiYaGrabeUnaFacMaestroEntity> GetDimeSiYaGrabeUnaFacMaestro(List<int> lid)
        {
            List<DimeSiYaGrabeUnaFacMaestroEntity> DimeSiYaGrabeUnaFacMaestroList = new List<DimeSiYaGrabeUnaFacMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DimeSiYaGrabeUnaFacMaestroList.Add(GetDimeSiYaGrabeUnaFacMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return DimeSiYaGrabeUnaFacMaestroList;
        }

        /// <summary>
        /// Gets DimeSiYaGrabeUnaFacMaestro by
        ///</summary>
        public override DimeSiYaGrabeUnaFacMaestroEntity GetDimeSiYaGrabeUnaFacMaestroById(long? ContratoMae)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("DIME_SI_YA_GRABE_UNA_FACTURAMaestro", connection);
                DimeSiYaGrabeUnaFacMaestroEntity entity_DimeSiYaGrabeUnaFacMaestro = null;

                AssingParameter(comandoSql, "@ContratoMae", ContratoMae);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_DimeSiYaGrabeUnaFacMaestro = GetDimeSiYaGrabeUnaFacMaestroFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_DimeSiYaGrabeUnaFacMaestro;
            }

        }



        /// <summary>
        ///Get DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        public override SoftvList<DimeSiYaGrabeUnaFacMaestroEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<DimeSiYaGrabeUnaFacMaestroEntity> entities = new SoftvList<DimeSiYaGrabeUnaFacMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDimeSiYaGrabeUnaFacMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDimeSiYaGrabeUnaFacMaestroCount();
                return entities ?? new SoftvList<DimeSiYaGrabeUnaFacMaestroEntity>();
            }
        }

        /// <summary>
        ///Get DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        public override SoftvList<DimeSiYaGrabeUnaFacMaestroEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<DimeSiYaGrabeUnaFacMaestroEntity> entities = new SoftvList<DimeSiYaGrabeUnaFacMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDimeSiYaGrabeUnaFacMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDimeSiYaGrabeUnaFacMaestroCount(xml);
                return entities ?? new SoftvList<DimeSiYaGrabeUnaFacMaestroEntity>();
            }
        }

        /// <summary>
        ///Get Count DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        public int GetDimeSiYaGrabeUnaFacMaestroCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count DimeSiYaGrabeUnaFacMaestro
        ///</summary>
        public int GetDimeSiYaGrabeUnaFacMaestroCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DimeSiYaGrabeUnaFacMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_DimeSiYaGrabeUnaFacMaestroGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DimeSiYaGrabeUnaFacMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

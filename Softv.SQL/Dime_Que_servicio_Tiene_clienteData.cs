﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Dime_Que_servicio_Tiene_clienteData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Dime_Que_servicio_Tiene_cliente Data Access Object
    /// File                    : Dime_Que_servicio_Tiene_clienteDAO.cs
    /// Creation date           : 08/02/2017
    /// Creation time           : 06:59 p. m.
    ///</summary>
    public class Dime_Que_servicio_Tiene_clienteData : Dime_Que_servicio_Tiene_clienteProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Dime_Que_servicio_Tiene_cliente"> Object Dime_Que_servicio_Tiene_cliente added to List</param>
        public override int AddDime_Que_servicio_Tiene_cliente(Dime_Que_servicio_Tiene_clienteEntity entity_Dime_Que_servicio_Tiene_cliente)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteAdd", connection);

                AssingParameter(comandoSql, "@clv_tipser", entity_Dime_Que_servicio_Tiene_cliente.clv_tipser);

                AssingParameter(comandoSql, "@Concepto", entity_Dime_Que_servicio_Tiene_cliente.Concepto);

                AssingParameter(comandoSql, "@Principal", entity_Dime_Que_servicio_Tiene_cliente.Principal);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdDime_Que_servicio_Tiene_cliente"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Dime_Que_servicio_Tiene_cliente
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteDime_Que_servicio_Tiene_cliente()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Dime_Que_servicio_Tiene_cliente
        ///</summary>
        /// <param name="Dime_Que_servicio_Tiene_cliente"> Objeto Dime_Que_servicio_Tiene_cliente a editar </param>
        public override int EditDime_Que_servicio_Tiene_cliente(Dime_Que_servicio_Tiene_clienteEntity entity_Dime_Que_servicio_Tiene_cliente)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteEdit", connection);

                AssingParameter(comandoSql, "@clv_tipser", entity_Dime_Que_servicio_Tiene_cliente.clv_tipser);

                AssingParameter(comandoSql, "@Concepto", entity_Dime_Que_servicio_Tiene_cliente.Concepto);

                AssingParameter(comandoSql, "@Principal", entity_Dime_Que_servicio_Tiene_cliente.Principal);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Dime_Que_servicio_Tiene_cliente
        ///</summary>
        public override List<Dime_Que_servicio_Tiene_clienteEntity> GetDime_Que_servicio_Tiene_cliente(long? Contrato)
        {
            List<Dime_Que_servicio_Tiene_clienteEntity> Dime_Que_servicio_Tiene_clienteList = new List<Dime_Que_servicio_Tiene_clienteEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Dime_Que_servicio_Tiene_cliente", connection);

                AssingParameter(comandoSql, "@Contrato", Contrato);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Dime_Que_servicio_Tiene_clienteList.Add(GetDime_Que_servicio_Tiene_clienteFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Dime_Que_servicio_Tiene_clienteList;
        }

        /// <summary>
        /// Gets all Dime_Que_servicio_Tiene_cliente by List<int>
        ///</summary>
        public override List<Dime_Que_servicio_Tiene_clienteEntity> GetDime_Que_servicio_Tiene_cliente(List<int> lid)
        {
            List<Dime_Que_servicio_Tiene_clienteEntity> Dime_Que_servicio_Tiene_clienteList = new List<Dime_Que_servicio_Tiene_clienteEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Dime_Que_servicio_Tiene_clienteList.Add(GetDime_Que_servicio_Tiene_clienteFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Dime_Que_servicio_Tiene_clienteList;
        }

        /// <summary>
        /// Gets Dime_Que_servicio_Tiene_cliente by
        ///</summary>
        public override Dime_Que_servicio_Tiene_clienteEntity GetDime_Que_servicio_Tiene_clienteById(long? Contrato)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Dime_Que_servicio_Tiene_cliente", connection);
                Dime_Que_servicio_Tiene_clienteEntity entity_Dime_Que_servicio_Tiene_cliente = null;

                AssingParameter(comandoSql, "@Contrato", Contrato);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Dime_Que_servicio_Tiene_cliente = GetDime_Que_servicio_Tiene_clienteFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Dime_Que_servicio_Tiene_cliente;
            }

        }



        /// <summary>
        ///Get Dime_Que_servicio_Tiene_cliente
        ///</summary>
        public override SoftvList<Dime_Que_servicio_Tiene_clienteEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<Dime_Que_servicio_Tiene_clienteEntity> entities = new SoftvList<Dime_Que_servicio_Tiene_clienteEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDime_Que_servicio_Tiene_clienteFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDime_Que_servicio_Tiene_clienteCount();
                return entities ?? new SoftvList<Dime_Que_servicio_Tiene_clienteEntity>();
            }
        }

        /// <summary>
        ///Get Dime_Que_servicio_Tiene_cliente
        ///</summary>
        public override SoftvList<Dime_Que_servicio_Tiene_clienteEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<Dime_Que_servicio_Tiene_clienteEntity> entities = new SoftvList<Dime_Que_servicio_Tiene_clienteEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetDime_Que_servicio_Tiene_clienteFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetDime_Que_servicio_Tiene_clienteCount(xml);
                return entities ?? new SoftvList<Dime_Que_servicio_Tiene_clienteEntity>();
            }
        }

        /// <summary>
        ///Get Count Dime_Que_servicio_Tiene_cliente
        ///</summary>
        public int GetDime_Que_servicio_Tiene_clienteCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Dime_Que_servicio_Tiene_cliente
        ///</summary>
        public int GetDime_Que_servicio_Tiene_clienteCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Dime_Que_servicio_Tiene_cliente.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Dime_Que_servicio_Tiene_clienteGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Dime_Que_servicio_Tiene_cliente " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

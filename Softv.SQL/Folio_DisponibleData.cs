﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Folio_DisponibleData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Folio_Disponible Data Access Object
    /// File                    : Folio_DisponibleDAO.cs
    /// Creation date           : 23/11/2017
    /// Creation time           : 11:38 a. m.
    ///</summary>
    public class Folio_DisponibleData : Folio_DisponibleProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Folio_Disponible"> Object Folio_Disponible added to List</param>
        public override int AddFolio_Disponible(Folio_DisponibleEntity entity_Folio_Disponible)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleAdd", connection);

                AssingParameter(comandoSql, "@Folio", entity_Folio_Disponible.Folio);

                AssingParameter(comandoSql, "@CLV_VENDEDOR", entity_Folio_Disponible.CLV_VENDEDOR);

                AssingParameter(comandoSql, "@SERIE", entity_Folio_Disponible.SERIE);

                AssingParameter(comandoSql, "@CONTRATO", entity_Folio_Disponible.CONTRATO);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdFolio_Disponible"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Folio_Disponible
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteFolio_Disponible()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Folio_Disponible
        ///</summary>
        /// <param name="Folio_Disponible"> Objeto Folio_Disponible a editar </param>
        public override int EditFolio_Disponible(Folio_DisponibleEntity entity_Folio_Disponible)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleEdit", connection);

                AssingParameter(comandoSql, "@Folio", entity_Folio_Disponible.Folio);

                AssingParameter(comandoSql, "@CLV_VENDEDOR", entity_Folio_Disponible.CLV_VENDEDOR);

                AssingParameter(comandoSql, "@SERIE", entity_Folio_Disponible.SERIE);

                AssingParameter(comandoSql, "@CONTRATO", entity_Folio_Disponible.CONTRATO);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Folio_Disponible
        ///</summary>
        public override List<Folio_DisponibleEntity> GetFolio_Disponible(int? CLV_VENDEDOR, String SERIE, long? CONTRATO)
        {
            List<Folio_DisponibleEntity> Folio_DisponibleList = new List<Folio_DisponibleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Folio_Disponible", connection);

                AssingParameter(comandoSql, "@CLV_VENDEDOR", CLV_VENDEDOR);
                AssingParameter(comandoSql, "@SERIE", SERIE);
                AssingParameter(comandoSql, "@CONTRATO", CONTRATO);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Folio_DisponibleList.Add(GetFolio_DisponibleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Folio_DisponibleList;
        }

        /// <summary>
        /// Gets all Folio_Disponible by List<int>
        ///</summary>
        public override List<Folio_DisponibleEntity> GetFolio_Disponible(List<int> lid)
        {
            List<Folio_DisponibleEntity> Folio_DisponibleList = new List<Folio_DisponibleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Folio_DisponibleList.Add(GetFolio_DisponibleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Folio_DisponibleList;
        }

        /// <summary>
        /// Gets Folio_Disponible by
        ///</summary>
        public override Folio_DisponibleEntity GetFolio_DisponibleById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleGetById", connection);
                Folio_DisponibleEntity entity_Folio_Disponible = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Folio_Disponible = GetFolio_DisponibleFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Folio_Disponible;
            }

        }



        /// <summary>
        ///Get Folio_Disponible
        ///</summary>
        public override SoftvList<Folio_DisponibleEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<Folio_DisponibleEntity> entities = new SoftvList<Folio_DisponibleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetFolio_DisponibleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetFolio_DisponibleCount();
                return entities ?? new SoftvList<Folio_DisponibleEntity>();
            }
        }

        /// <summary>
        ///Get Folio_Disponible
        ///</summary>
        public override SoftvList<Folio_DisponibleEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<Folio_DisponibleEntity> entities = new SoftvList<Folio_DisponibleEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetFolio_DisponibleFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetFolio_DisponibleCount(xml);
                return entities ?? new SoftvList<Folio_DisponibleEntity>();
            }
        }

        /// <summary>
        ///Get Count Folio_Disponible
        ///</summary>
        public int GetFolio_DisponibleCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Folio_Disponible
        ///</summary>
        public int GetFolio_DisponibleCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Folio_Disponible.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Folio_DisponibleGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Folio_Disponible " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.GeneraRefBanamexMaestroData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : GeneraRefBanamexMaestro Data Access Object
    /// File                    : GeneraRefBanamexMaestroDAO.cs
    /// Creation date           : 15/05/2017
    /// Creation time           : 06:01 p. m.
    ///</summary>
    public class GeneraRefBanamexMaestroData : GeneraRefBanamexMaestroProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="GeneraRefBanamexMaestro"> Object GeneraRefBanamexMaestro added to List</param>
        public override int AddGeneraRefBanamexMaestro(GeneraRefBanamexMaestroEntity entity_GeneraRefBanamexMaestro)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroAdd", connection);

                AssingParameter(comandoSql, "@ContratoMaestro", entity_GeneraRefBanamexMaestro.ContratoMaestro);

                AssingParameter(comandoSql, "@Importe", entity_GeneraRefBanamexMaestro.Importe);

                AssingParameter(comandoSql, "@lineaCaptura", entity_GeneraRefBanamexMaestro.lineaCaptura);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdGeneraRefBanamexMaestro"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a GeneraRefBanamexMaestro
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteGeneraRefBanamexMaestro()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a GeneraRefBanamexMaestro
        ///</summary>
        /// <param name="GeneraRefBanamexMaestro"> Objeto GeneraRefBanamexMaestro a editar </param>
        public override int EditGeneraRefBanamexMaestro(GeneraRefBanamexMaestroEntity entity_GeneraRefBanamexMaestro)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroEdit", connection);

                AssingParameter(comandoSql, "@ContratoMaestro", entity_GeneraRefBanamexMaestro.ContratoMaestro);

                AssingParameter(comandoSql, "@Importe", entity_GeneraRefBanamexMaestro.Importe);

                AssingParameter(comandoSql, "@lineaCaptura", entity_GeneraRefBanamexMaestro.lineaCaptura);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all GeneraRefBanamexMaestro
        ///</summary>
        public override List<GeneraRefBanamexMaestroEntity> GetGeneraRefBanamexMaestro()
        {
            List<GeneraRefBanamexMaestroEntity> GeneraRefBanamexMaestroList = new List<GeneraRefBanamexMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        GeneraRefBanamexMaestroList.Add(GetGeneraRefBanamexMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return GeneraRefBanamexMaestroList;
        }

        /// <summary>
        /// Gets all GeneraRefBanamexMaestro by List<int>
        ///</summary>
        public override List<GeneraRefBanamexMaestroEntity> GetGeneraRefBanamexMaestro(List<int> lid)
        {
            List<GeneraRefBanamexMaestroEntity> GeneraRefBanamexMaestroList = new List<GeneraRefBanamexMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        GeneraRefBanamexMaestroList.Add(GetGeneraRefBanamexMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return GeneraRefBanamexMaestroList;
        }

        /// <summary>
        /// Gets GeneraRefBanamexMaestro by
        ///</summary>
        public override GeneraRefBanamexMaestroEntity GetGeneraRefBanamexMaestroById(long? ContratoMaestro, Decimal? Importe)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("GeneraRefBanamexMaestro", connection);
                GeneraRefBanamexMaestroEntity entity_GeneraRefBanamexMaestro = null;

                AssingParameter(comandoSql, "@ContratoMaestro", ContratoMaestro);
                AssingParameter(comandoSql, "@Importe", Importe);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_GeneraRefBanamexMaestro = GetGeneraRefBanamexMaestroFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_GeneraRefBanamexMaestro;
            }

        }



        /// <summary>
        ///Get GeneraRefBanamexMaestro
        ///</summary>
        public override SoftvList<GeneraRefBanamexMaestroEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<GeneraRefBanamexMaestroEntity> entities = new SoftvList<GeneraRefBanamexMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetGeneraRefBanamexMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetGeneraRefBanamexMaestroCount();
                return entities ?? new SoftvList<GeneraRefBanamexMaestroEntity>();
            }
        }

        /// <summary>
        ///Get GeneraRefBanamexMaestro
        ///</summary>
        public override SoftvList<GeneraRefBanamexMaestroEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<GeneraRefBanamexMaestroEntity> entities = new SoftvList<GeneraRefBanamexMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetGeneraRefBanamexMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetGeneraRefBanamexMaestroCount(xml);
                return entities ?? new SoftvList<GeneraRefBanamexMaestroEntity>();
            }
        }

        /// <summary>
        ///Get Count GeneraRefBanamexMaestro
        ///</summary>
        public int GetGeneraRefBanamexMaestroCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count GeneraRefBanamexMaestro
        ///</summary>
        public int GetGeneraRefBanamexMaestroCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GeneraRefBanamexMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_GeneraRefBanamexMaestroGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GeneraRefBanamexMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.GrabaFacturaCMaestroData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : GrabaFacturaCMaestro Data Access Object
    /// File                    : GrabaFacturaCMaestroDAO.cs
    /// Creation date           : 22/03/2017
    /// Creation time           : 06:05 p. m.
    ///</summary>
    public class GrabaFacturaCMaestroData : GrabaFacturaCMaestroProvider
    {

        /// <summary>
        /// Gets GrabaFacturaCMaestro by
        ///</summary>
        public override GrabaFacturaCMaestroEntity GetGrabaFacturaCMaestroById(long? ContratoMaestro, bool? Credito, String Cajera, String IpMaquina, int? Sucursal, int? IdCompania, int? IdDistribuidor, long? ClvSessionPadre, int? Tipo, String ToKen2, int? NoPagos, Decimal? PagoInicial, int? IdCaja)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GrabaFacturaCMaestro.ConnectionString))
            {
                GrabaFacturaCMaestroEntity entity_GrabaFacturaCMaestro  = new GrabaFacturaCMaestroEntity();

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    ;
                    SqlCommand comando = new SqlCommand("GrabaFacturaCMaestro");
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandTimeout = 0;
                    comando.Connection = connection;


                    SqlParameter par1 = new SqlParameter("@ContratoMaestro", SqlDbType.BigInt);
                    par1.Value = ContratoMaestro;
                    par1.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par1);

                    SqlParameter par2 = new SqlParameter("@Credito", SqlDbType.Bit);
                    par2.Value = Credito;
                    par2.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par2);

                    SqlParameter par3 = new SqlParameter("@cajera", SqlDbType.VarChar);
                    par3.Value = Cajera;
                    par3.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par3);

                    SqlParameter par4 = new SqlParameter("@Caja", SqlDbType.Int);
                    par4.Value = IdCaja;
                    par4.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par4);

                    SqlParameter par5 = new SqlParameter("@sucursal", SqlDbType.Int);
                    par5.Value = Sucursal;
                    par5.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par5);

                    SqlParameter par7 = new SqlParameter("@IdCompania", SqlDbType.Int);
                    par7.Value = IdCompania;
                    par7.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par7);


                    SqlParameter par8 = new SqlParameter("@IdDistribuidor", SqlDbType.Int);
                    par8.Value = IdDistribuidor;
                    par8.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par8);

                    SqlParameter par9 = new SqlParameter("@ClvSessionPadre", SqlDbType.BigInt);
                    par9.Value = ClvSessionPadre;
                    par9.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par9);

                    SqlParameter par10 = new SqlParameter("@Clv_FacturaMaestro", SqlDbType.BigInt);
                    par10.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par10);


                    SqlParameter par11 = new SqlParameter("@BndError", SqlDbType.Int);
                    par11.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par11);

                    SqlParameter par12 = new SqlParameter("@Msg", SqlDbType.VarChar, 400);
                    par12.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par12);

                    SqlParameter par13 = new SqlParameter("@1Tipo", SqlDbType.Int);
                    par13.Value = Tipo;
                    par13.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par13);

                    SqlParameter par14 = new SqlParameter("@ToKen2", SqlDbType.VarChar,50);
                    par14.Value = ToKen2;
                    par14.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par14);


                    SqlParameter par15 = new SqlParameter("@NoPagos", SqlDbType.Int);
                    par15.Value = NoPagos;
                    par15.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par15);

                    SqlParameter par16 = new SqlParameter("@PagoInicial", SqlDbType.Decimal);
                    par16.Value = NoPagos;
                    par16.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par16);


                    comando.ExecuteNonQuery();


                    entity_GrabaFacturaCMaestro.ClvFacturaMaestro = long.Parse(par10.Value.ToString());
                    try
                    {
                        entity_GrabaFacturaCMaestro.BndError = Int32.Parse(par11.Value.ToString());
                    }
                    catch
                    {
                        entity_GrabaFacturaCMaestro.BndError = 0;
                    }

                    try
                    {
                        entity_GrabaFacturaCMaestro.Msg = par12.Value.ToString();
                    }
                    catch
                    {
                        entity_GrabaFacturaCMaestro.Msg = "";
                    }
                   


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data GrabaFacturaCMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_GrabaFacturaCMaestro;
            }

        }

        

        #region Customs Methods

        #endregion
    }
}

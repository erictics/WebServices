﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.LlamadasData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Llamadas Data Access Object
    /// File                    : LlamadasDAO.cs
    /// Creation date           : 17/06/2017
    /// Creation time           : 11:09 a. m.
    ///</summary>
    public class LlamadasData : LlamadasProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Llamadas"> Object Llamadas added to List</param>
        public override int AddLlamadas(LlamadasEntity entity_Llamadas)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasAdd", connection);

                AssingParameter(comandoSql, "@IdLlamada", null, pd: ParameterDirection.Output, IsKey: true);

                AssingParameter(comandoSql, "@IdUsuario", entity_Llamadas.IdUsuario);

                AssingParameter(comandoSql, "@Tipo_Llamada", entity_Llamadas.Tipo_Llamada);

                AssingParameter(comandoSql, "@Contrato", entity_Llamadas.Contrato);

                AssingParameter(comandoSql, "@Detalle", entity_Llamadas.Detalle);

                AssingParameter(comandoSql, "@Solucion", entity_Llamadas.Solucion);

                AssingParameter(comandoSql, "@Fecha", entity_Llamadas.Fecha);

                AssingParameter(comandoSql, "@HoraInicio", entity_Llamadas.HoraInicio);

                AssingParameter(comandoSql, "@HoraFin", entity_Llamadas.HoraFin);

                AssingParameter(comandoSql, "@IdTurno", entity_Llamadas.IdTurno);

                AssingParameter(comandoSql, "@Clv_Queja", entity_Llamadas.Clv_Queja);

                AssingParameter(comandoSql, "@IdConexion", entity_Llamadas.IdConexion);

                AssingParameter(comandoSql, "@Clv_Trabajo", entity_Llamadas.Clv_Trabajo);

                AssingParameter(comandoSql, "@Clv_TipSer", entity_Llamadas.Clv_TipSer);

                AssingParameter(comandoSql, "@Clv_Problema", entity_Llamadas.Clv_Problema);

                AssingParameter(comandoSql, "@Clv_Motivo", entity_Llamadas.Clv_Motivo);

                AssingParameter(comandoSql, "@SiEsCliente", entity_Llamadas.SiEsCliente);

                AssingParameter(comandoSql, "@Ciudad", entity_Llamadas.Ciudad);

                AssingParameter(comandoSql, "@NombreCliente", entity_Llamadas.NombreCliente);

                AssingParameter(comandoSql, "@Telefono", entity_Llamadas.Telefono);

                AssingParameter(comandoSql, "@Cel", entity_Llamadas.Cel);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Llamadas " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdLlamadas"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Llamadas
        ///</summary>
        /// <param name="">  IdLlamada to delete </param>
        public override int DeleteLlamadas(int? IdLlamada)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasDelete", connection);

                AssingParameter(comandoSql, "@IdLlamada", IdLlamada);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Llamadas
        ///</summary>
        /// <param name="Llamadas"> Objeto Llamadas a editar </param>
        public override int EditLlamadas(LlamadasEntity entity_Llamadas)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasEdit", connection);

                AssingParameter(comandoSql, "@IdLlamada", entity_Llamadas.IdLlamada);

                AssingParameter(comandoSql, "@IdUsuario", entity_Llamadas.IdUsuario);

                AssingParameter(comandoSql, "@Tipo_Llamada", entity_Llamadas.Tipo_Llamada);

                AssingParameter(comandoSql, "@Contrato", entity_Llamadas.Contrato);

                AssingParameter(comandoSql, "@Detalle", entity_Llamadas.Detalle);

                AssingParameter(comandoSql, "@Solucion", entity_Llamadas.Solucion);

                AssingParameter(comandoSql, "@Fecha", entity_Llamadas.Fecha);

                AssingParameter(comandoSql, "@HoraInicio", entity_Llamadas.HoraInicio);

                AssingParameter(comandoSql, "@HoraFin", entity_Llamadas.HoraFin);

                AssingParameter(comandoSql, "@IdTurno", entity_Llamadas.IdTurno);

                AssingParameter(comandoSql, "@Clv_Queja", entity_Llamadas.Clv_Queja);

                AssingParameter(comandoSql, "@IdConexion", entity_Llamadas.IdConexion);

                AssingParameter(comandoSql, "@Clv_Trabajo", entity_Llamadas.Clv_Trabajo);

                AssingParameter(comandoSql, "@Clv_TipSer", entity_Llamadas.Clv_TipSer);

                AssingParameter(comandoSql, "@Clv_Problema", entity_Llamadas.Clv_Problema);

                AssingParameter(comandoSql, "@Clv_Motivo", entity_Llamadas.Clv_Motivo);

                AssingParameter(comandoSql, "@SiEsCliente", entity_Llamadas.SiEsCliente);

                AssingParameter(comandoSql, "@Ciudad", entity_Llamadas.Ciudad);

                AssingParameter(comandoSql, "@NombreCliente", entity_Llamadas.NombreCliente);

                AssingParameter(comandoSql, "@Telefono", entity_Llamadas.Telefono);

                AssingParameter(comandoSql, "@Cel", entity_Llamadas.Cel);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Llamadas
        ///</summary>
        public override List<LlamadasEntity> GetLlamadas()
        {
            List<LlamadasEntity> LlamadasList = new List<LlamadasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        LlamadasList.Add(GetLlamadasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return LlamadasList;
        }

        /// <summary>
        /// Gets all Llamadas by List<int>
        ///</summary>
        public override List<LlamadasEntity> GetLlamadas(List<int> lid)
        {
            List<LlamadasEntity> LlamadasList = new List<LlamadasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        LlamadasList.Add(GetLlamadasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return LlamadasList;
        }

        /// <summary>
        /// Gets Llamadas by
        ///</summary>
        public override LlamadasEntity GetLlamadasById(int? IdLlamada)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasGetById", connection);
                LlamadasEntity entity_Llamadas = null;


                AssingParameter(comandoSql, "@IdLlamada", IdLlamada);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Llamadas = GetLlamadasFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Llamadas;
            }

        }



        /// <summary>
        ///Get Llamadas
        ///</summary>
        public override SoftvList<LlamadasEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<LlamadasEntity> entities = new SoftvList<LlamadasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetLlamadasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetLlamadasCount();
                return entities ?? new SoftvList<LlamadasEntity>();
            }
        }

        /// <summary>
        ///Get Llamadas
        ///</summary>
        public override SoftvList<LlamadasEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<LlamadasEntity> entities = new SoftvList<LlamadasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetLlamadasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetLlamadasCount(xml);
                return entities ?? new SoftvList<LlamadasEntity>();
            }
        }

        /// <summary>
        ///Get Count Llamadas
        ///</summary>
        public int GetLlamadasCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Llamadas
        ///</summary>
        public int GetLlamadasCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Llamadas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_LlamadasGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Llamadas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

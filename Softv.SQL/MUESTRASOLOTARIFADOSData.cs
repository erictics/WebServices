﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.MUESTRASOLOTARIFADOSData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MUESTRASOLOTARIFADOS Data Access Object
    /// File                    : MUESTRASOLOTARIFADOSDAO.cs
    /// Creation date           : 19/09/2017
    /// Creation time           : 09:55 a. m.
    ///</summary>
    public class MUESTRASOLOTARIFADOSData : MUESTRASOLOTARIFADOSProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="MUESTRASOLOTARIFADOS"> Object MUESTRASOLOTARIFADOS added to List</param>
        public override int AddMUESTRASOLOTARIFADOS(MUESTRASOLOTARIFADOSEntity entity_MUESTRASOLOTARIFADOS)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSAdd", connection);

                AssingParameter(comandoSql, "@Clave", entity_MUESTRASOLOTARIFADOS.Clave);

                AssingParameter(comandoSql, "@Concepto", entity_MUESTRASOLOTARIFADOS.Concepto);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdMUESTRASOLOTARIFADOS"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a MUESTRASOLOTARIFADOS
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteMUESTRASOLOTARIFADOS()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a MUESTRASOLOTARIFADOS
        ///</summary>
        /// <param name="MUESTRASOLOTARIFADOS"> Objeto MUESTRASOLOTARIFADOS a editar </param>
        public override int EditMUESTRASOLOTARIFADOS(MUESTRASOLOTARIFADOSEntity entity_MUESTRASOLOTARIFADOS)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSEdit", connection);

                AssingParameter(comandoSql, "@Clave", entity_MUESTRASOLOTARIFADOS.Clave);

                AssingParameter(comandoSql, "@Concepto", entity_MUESTRASOLOTARIFADOS.Concepto);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all MUESTRASOLOTARIFADOS
        ///</summary>
        public override List<MUESTRASOLOTARIFADOSEntity> GetMUESTRASOLOTARIFADOS()
        {
            List<MUESTRASOLOTARIFADOSEntity> MUESTRASOLOTARIFADOSList = new List<MUESTRASOLOTARIFADOSEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MUESTRASOLOTARIFADOS", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MUESTRASOLOTARIFADOSList.Add(GetMUESTRASOLOTARIFADOSFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MUESTRASOLOTARIFADOSList;
        }

        /// <summary>
        /// Gets all MUESTRASOLOTARIFADOS by List<int>
        ///</summary>
        public override List<MUESTRASOLOTARIFADOSEntity> GetMUESTRASOLOTARIFADOS(List<int> lid)
        {
            List<MUESTRASOLOTARIFADOSEntity> MUESTRASOLOTARIFADOSList = new List<MUESTRASOLOTARIFADOSEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MUESTRASOLOTARIFADOSList.Add(GetMUESTRASOLOTARIFADOSFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MUESTRASOLOTARIFADOSList;
        }

        /// <summary>
        /// Gets MUESTRASOLOTARIFADOS by
        ///</summary>
        public override MUESTRASOLOTARIFADOSEntity GetMUESTRASOLOTARIFADOSById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSGetById", connection);
                MUESTRASOLOTARIFADOSEntity entity_MUESTRASOLOTARIFADOS = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_MUESTRASOLOTARIFADOS = GetMUESTRASOLOTARIFADOSFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_MUESTRASOLOTARIFADOS;
            }

        }



        /// <summary>
        ///Get MUESTRASOLOTARIFADOS
        ///</summary>
        public override SoftvList<MUESTRASOLOTARIFADOSEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MUESTRASOLOTARIFADOSEntity> entities = new SoftvList<MUESTRASOLOTARIFADOSEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMUESTRASOLOTARIFADOSFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMUESTRASOLOTARIFADOSCount();
                return entities ?? new SoftvList<MUESTRASOLOTARIFADOSEntity>();
            }
        }

        /// <summary>
        ///Get MUESTRASOLOTARIFADOS
        ///</summary>
        public override SoftvList<MUESTRASOLOTARIFADOSEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MUESTRASOLOTARIFADOSEntity> entities = new SoftvList<MUESTRASOLOTARIFADOSEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMUESTRASOLOTARIFADOSFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMUESTRASOLOTARIFADOSCount(xml);
                return entities ?? new SoftvList<MUESTRASOLOTARIFADOSEntity>();
            }
        }

        /// <summary>
        ///Get Count MUESTRASOLOTARIFADOS
        ///</summary>
        public int GetMUESTRASOLOTARIFADOSCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count MUESTRASOLOTARIFADOS
        ///</summary>
        public int GetMUESTRASOLOTARIFADOSCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRASOLOTARIFADOS.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MUESTRASOLOTARIFADOSGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MUESTRASOLOTARIFADOS " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

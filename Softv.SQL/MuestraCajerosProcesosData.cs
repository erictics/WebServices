﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.MuestraCajerosProcesosData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraCajerosProcesos Data Access Object
    /// File                    : MuestraCajerosProcesosDAO.cs
    /// Creation date           : 13/12/2016
    /// Creation time           : 12:25 p. m.
    ///</summary>
    public class MuestraCajerosProcesosData : MuestraCajerosProcesosProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="MuestraCajerosProcesos"> Object MuestraCajerosProcesos added to List</param>
        public override int AddMuestraCajerosProcesos(MuestraCajerosProcesosEntity entity_MuestraCajerosProcesos)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosAdd", connection);

                AssingParameter(comandoSql, "@Clv_Usuario", entity_MuestraCajerosProcesos.Clv_Usuario);

                AssingParameter(comandoSql, "@Nombre", entity_MuestraCajerosProcesos.Nombre);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdMuestraCajerosProcesos"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a MuestraCajerosProcesos
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteMuestraCajerosProcesos()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a MuestraCajerosProcesos
        ///</summary>
        /// <param name="MuestraCajerosProcesos"> Objeto MuestraCajerosProcesos a editar </param>
        public override int EditMuestraCajerosProcesos(MuestraCajerosProcesosEntity entity_MuestraCajerosProcesos)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosEdit", connection);

                AssingParameter(comandoSql, "@Clv_Usuario", entity_MuestraCajerosProcesos.Clv_Usuario);

                AssingParameter(comandoSql, "@Nombre", entity_MuestraCajerosProcesos.Nombre);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all MuestraCajerosProcesos
        ///</summary>
        public override List<MuestraCajerosProcesosEntity> GetMuestraCajerosProcesos(int? IdUsuario)
        {
            List<MuestraCajerosProcesosEntity> MuestraCajerosProcesosList = new List<MuestraCajerosProcesosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("softvweb_MuestraCajerosProcesos", connection);
                AssingParameter(comandoSql, "@IdUsuario", IdUsuario);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraCajerosProcesosList.Add(GetMuestraCajerosProcesosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraCajerosProcesosList;
        }



        public override List<MuestraCajerosProcesosEntity> GetCajeros(int? IdUsuario, int? IdPlaza)
        {
            List<MuestraCajerosProcesosEntity> MuestraCajerosProcesosList = new List<MuestraCajerosProcesosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MUESTRACAJERAS", connection);
                AssingParameter(comandoSql, "@ClvUsuario", IdUsuario);
                AssingParameter(comandoSql, "@idcompania", IdPlaza);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraCajerosProcesosList.Add(GetMuestraCajerosProcesosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraCajerosProcesosList;
        }

















        /// <summary>
        /// Gets all MuestraCajerosProcesos by List<int>
        ///</summary>
        public override List<MuestraCajerosProcesosEntity> GetMuestraCajerosProcesos(List<int> lid)
        {
            List<MuestraCajerosProcesosEntity> MuestraCajerosProcesosList = new List<MuestraCajerosProcesosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraCajerosProcesosList.Add(GetMuestraCajerosProcesosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraCajerosProcesosList;
        }

        /// <summary>
        /// Gets MuestraCajerosProcesos by
        ///</summary>
        public override MuestraCajerosProcesosEntity GetMuestraCajerosProcesosById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosGetById", connection);
                MuestraCajerosProcesosEntity entity_MuestraCajerosProcesos = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_MuestraCajerosProcesos = GetMuestraCajerosProcesosFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_MuestraCajerosProcesos;
            }

        }



        /// <summary>
        ///Get MuestraCajerosProcesos
        ///</summary>
        public override SoftvList<MuestraCajerosProcesosEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraCajerosProcesosEntity> entities = new SoftvList<MuestraCajerosProcesosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraCajerosProcesosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraCajerosProcesosCount();
                return entities ?? new SoftvList<MuestraCajerosProcesosEntity>();
            }
        }

        /// <summary>
        ///Get MuestraCajerosProcesos
        ///</summary>
        public override SoftvList<MuestraCajerosProcesosEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraCajerosProcesosEntity> entities = new SoftvList<MuestraCajerosProcesosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraCajerosProcesosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraCajerosProcesosCount(xml);
                return entities ?? new SoftvList<MuestraCajerosProcesosEntity>();
            }
        }

        /// <summary>
        ///Get Count MuestraCajerosProcesos
        ///</summary>
        public int GetMuestraCajerosProcesosCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count MuestraCajerosProcesos
        ///</summary>
        public int GetMuestraCajerosProcesosCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCajerosProcesos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCajerosProcesosGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCajerosProcesos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

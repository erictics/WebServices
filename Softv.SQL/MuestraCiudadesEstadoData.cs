﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.MuestraCiudadesEstadoData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraCiudadesEstado Data Access Object
    /// File                    : MuestraCiudadesEstadoDAO.cs
    /// Creation date           : 05/09/2017
    /// Creation time           : 04:51 p. m.
    ///</summary>
    public class MuestraCiudadesEstadoData : MuestraCiudadesEstadoProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="MuestraCiudadesEstado"> Object MuestraCiudadesEstado added to List</param>
        public override int AddMuestraCiudadesEstado(MuestraCiudadesEstadoEntity entity_MuestraCiudadesEstado)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoAdd", connection);

                AssingParameter(comandoSql, "@clv_estado", entity_MuestraCiudadesEstado.clv_estado);

                AssingParameter(comandoSql, "@idcompania", entity_MuestraCiudadesEstado.idcompania);

                AssingParameter(comandoSql, "@Clv_Ciudad", entity_MuestraCiudadesEstado.Clv_Ciudad);

                AssingParameter(comandoSql, "@Nombre", entity_MuestraCiudadesEstado.Nombre);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdMuestraCiudadesEstado"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a MuestraCiudadesEstado
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteMuestraCiudadesEstado()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a MuestraCiudadesEstado
        ///</summary>
        /// <param name="MuestraCiudadesEstado"> Objeto MuestraCiudadesEstado a editar </param>
        public override int EditMuestraCiudadesEstado(MuestraCiudadesEstadoEntity entity_MuestraCiudadesEstado)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoEdit", connection);

                AssingParameter(comandoSql, "@clv_estado", entity_MuestraCiudadesEstado.clv_estado);

                AssingParameter(comandoSql, "@idcompania", entity_MuestraCiudadesEstado.idcompania);

                AssingParameter(comandoSql, "@Clv_Ciudad", entity_MuestraCiudadesEstado.Clv_Ciudad);

                AssingParameter(comandoSql, "@Nombre", entity_MuestraCiudadesEstado.Nombre);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all MuestraCiudadesEstado
        ///</summary>
        public override List<MuestraCiudadesEstadoEntity> GetMuestraCiudadesEstado(int? clv_estado, int? idcompania)
        {
            List<MuestraCiudadesEstadoEntity> MuestraCiudadesEstadoList = new List<MuestraCiudadesEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MuestraCiudadesEstado", connection);

                AssingParameter(comandoSql, "@clv_estado", clv_estado);
                AssingParameter(comandoSql, "@idcompania", idcompania);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraCiudadesEstadoList.Add(GetMuestraCiudadesEstadoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraCiudadesEstadoList;
        }

        /// <summary>
        /// Gets all MuestraCiudadesEstado by List<int>
        ///</summary>
        public override List<MuestraCiudadesEstadoEntity> GetMuestraCiudadesEstado(List<int> lid)
        {
            List<MuestraCiudadesEstadoEntity> MuestraCiudadesEstadoList = new List<MuestraCiudadesEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraCiudadesEstadoList.Add(GetMuestraCiudadesEstadoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraCiudadesEstadoList;
        }

        /// <summary>
        /// Gets MuestraCiudadesEstado by
        ///</summary>
        public override MuestraCiudadesEstadoEntity GetMuestraCiudadesEstadoById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoGetById", connection);
                MuestraCiudadesEstadoEntity entity_MuestraCiudadesEstado = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_MuestraCiudadesEstado = GetMuestraCiudadesEstadoFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_MuestraCiudadesEstado;
            }

        }



        /// <summary>
        ///Get MuestraCiudadesEstado
        ///</summary>
        public override SoftvList<MuestraCiudadesEstadoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraCiudadesEstadoEntity> entities = new SoftvList<MuestraCiudadesEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraCiudadesEstadoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraCiudadesEstadoCount();
                return entities ?? new SoftvList<MuestraCiudadesEstadoEntity>();
            }
        }

        /// <summary>
        ///Get MuestraCiudadesEstado
        ///</summary>
        public override SoftvList<MuestraCiudadesEstadoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraCiudadesEstadoEntity> entities = new SoftvList<MuestraCiudadesEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraCiudadesEstadoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraCiudadesEstadoCount(xml);
                return entities ?? new SoftvList<MuestraCiudadesEstadoEntity>();
            }
        }

        /// <summary>
        ///Get Count MuestraCiudadesEstado
        ///</summary>
        public int GetMuestraCiudadesEstadoCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count MuestraCiudadesEstado
        ///</summary>
        public int GetMuestraCiudadesEstadoCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraCiudadesEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraCiudadesEstadoGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraCiudadesEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.MuestraContratoRealData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraContratoReal Data Access Object
    /// File                    : MuestraContratoRealDAO.cs
    /// Creation date           : 18/10/2017
    /// Creation time           : 12:22 p. m.
    ///</summary>
    public class MuestraContratoRealData : MuestraContratoRealProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="MuestraContratoReal"> Object MuestraContratoReal added to List</param>
        public override int AddMuestraContratoReal(MuestraContratoRealEntity entity_MuestraContratoReal)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealAdd", connection);

                AssingParameter(comandoSql, "@ContratoCom", entity_MuestraContratoReal.ContratoCom);

                AssingParameter(comandoSql, "@Contrato", entity_MuestraContratoReal.Contrato);

                AssingParameter(comandoSql, "@Id", entity_MuestraContratoReal.Id);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdMuestraContratoReal"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a MuestraContratoReal
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteMuestraContratoReal()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a MuestraContratoReal
        ///</summary>
        /// <param name="MuestraContratoReal"> Objeto MuestraContratoReal a editar </param>
        public override int EditMuestraContratoReal(MuestraContratoRealEntity entity_MuestraContratoReal)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealEdit", connection);

                AssingParameter(comandoSql, "@ContratoCom", entity_MuestraContratoReal.ContratoCom);

                AssingParameter(comandoSql, "@Contrato", entity_MuestraContratoReal.Contrato);

                AssingParameter(comandoSql, "@Id", entity_MuestraContratoReal.Id);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all MuestraContratoReal
        ///</summary>
        public override List<MuestraContratoRealEntity> GetMuestraContratoReal()
        {
            List<MuestraContratoRealEntity> MuestraContratoRealList = new List<MuestraContratoRealEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraContratoRealList.Add(GetMuestraContratoRealFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraContratoRealList;
        }

        /// <summary>
        /// Gets all MuestraContratoReal by List<int>
        ///</summary>
        public override List<MuestraContratoRealEntity> GetMuestraContratoReal(List<int> lid)
        {
            List<MuestraContratoRealEntity> MuestraContratoRealList = new List<MuestraContratoRealEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraContratoRealList.Add(GetMuestraContratoRealFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraContratoRealList;
        }

        /// <summary>
        /// Gets MuestraContratoReal by
        ///</summary>
        public override MuestraContratoRealEntity GetMuestraContratoRealById(String ContratoCom, int? Id)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Get_MuestraContratoReal", connection);
                MuestraContratoRealEntity entity_MuestraContratoReal = null;

                AssingParameter(comandoSql, "@ContratoCom", ContratoCom);


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_MuestraContratoReal = GetMuestraContratoRealFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_MuestraContratoReal;
            }

        }



        /// <summary>
        ///Get MuestraContratoReal
        ///</summary>
        public override SoftvList<MuestraContratoRealEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraContratoRealEntity> entities = new SoftvList<MuestraContratoRealEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraContratoRealFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraContratoRealCount();
                return entities ?? new SoftvList<MuestraContratoRealEntity>();
            }
        }

        /// <summary>
        ///Get MuestraContratoReal
        ///</summary>
        public override SoftvList<MuestraContratoRealEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraContratoRealEntity> entities = new SoftvList<MuestraContratoRealEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraContratoRealFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraContratoRealCount(xml);
                return entities ?? new SoftvList<MuestraContratoRealEntity>();
            }
        }

        /// <summary>
        ///Get Count MuestraContratoReal
        ///</summary>
        public int GetMuestraContratoRealCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count MuestraContratoReal
        ///</summary>
        public int GetMuestraContratoRealCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraContratoReal.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraContratoRealGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraContratoReal " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

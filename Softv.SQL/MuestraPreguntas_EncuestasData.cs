﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.MuestraPreguntas_EncuestasData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraPreguntas_Encuestas Data Access Object
    /// File                    : MuestraPreguntas_EncuestasDAO.cs
    /// Creation date           : 28/06/2017
    /// Creation time           : 06:33 p. m.
    ///</summary>
    public class MuestraPreguntas_EncuestasData : MuestraPreguntas_EncuestasProvider
    {

        string ConnectionStringEncuestas = System.Configuration.ConfigurationManager.
ConnectionStrings["ConnectionStringEncuestas"].ConnectionString;
        /// <summary>
        ///</summary>
        /// <param name="MuestraPreguntas_Encuestas"> Object MuestraPreguntas_Encuestas added to List</param>
        public override int AddMuestraPreguntas_Encuestas(MuestraPreguntas_EncuestasEntity entity_MuestraPreguntas_Encuestas)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasAdd", connection);

                AssingParameter(comandoSql, "@IdPregunta", entity_MuestraPreguntas_Encuestas.IdPregunta);

                AssingParameter(comandoSql, "@Pregunta", entity_MuestraPreguntas_Encuestas.Pregunta);

                AssingParameter(comandoSql, "@Descripcion", entity_MuestraPreguntas_Encuestas.Descripcion);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdMuestraPreguntas_Encuestas"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a MuestraPreguntas_Encuestas
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteMuestraPreguntas_Encuestas()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a MuestraPreguntas_Encuestas
        ///</summary>
        /// <param name="MuestraPreguntas_Encuestas"> Objeto MuestraPreguntas_Encuestas a editar </param>
        public override int EditMuestraPreguntas_Encuestas(MuestraPreguntas_EncuestasEntity entity_MuestraPreguntas_Encuestas)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasEdit", connection);

                AssingParameter(comandoSql, "@IdPregunta", entity_MuestraPreguntas_Encuestas.IdPregunta);

                AssingParameter(comandoSql, "@Pregunta", entity_MuestraPreguntas_Encuestas.Pregunta);

                AssingParameter(comandoSql, "@Descripcion", entity_MuestraPreguntas_Encuestas.Descripcion);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all MuestraPreguntas_Encuestas
        ///</summary>
        public override List<MuestraPreguntas_EncuestasEntity> GetMuestraPreguntas_Encuestas(int? IdEncuesta)
        {
            List<MuestraPreguntas_EncuestasEntity> MuestraPreguntas_EncuestasList = new List<MuestraPreguntas_EncuestasEntity>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("MuestraPreguntas_Encuestas", connection);

                AssingParameter(comandoSql, "@IdEncuesta", IdEncuesta);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraPreguntas_EncuestasList.Add(GetMuestraPreguntas_EncuestasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraPreguntas_EncuestasList;
        }

        /// <summary>
        /// Gets all MuestraPreguntas_Encuestas by List<int>
        ///</summary>
        public override List<MuestraPreguntas_EncuestasEntity> GetMuestraPreguntas_Encuestas(List<int> lid)
        {
            List<MuestraPreguntas_EncuestasEntity> MuestraPreguntas_EncuestasList = new List<MuestraPreguntas_EncuestasEntity>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraPreguntas_EncuestasList.Add(GetMuestraPreguntas_EncuestasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraPreguntas_EncuestasList;
        }

        /// <summary>
        /// Gets MuestraPreguntas_Encuestas by
        ///</summary>
        public override MuestraPreguntas_EncuestasEntity GetMuestraPreguntas_EncuestasById()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasGetById", connection);
                MuestraPreguntas_EncuestasEntity entity_MuestraPreguntas_Encuestas = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_MuestraPreguntas_Encuestas = GetMuestraPreguntas_EncuestasFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_MuestraPreguntas_Encuestas;
            }

        }



        /// <summary>
        ///Get MuestraPreguntas_Encuestas
        ///</summary>
        public override SoftvList<MuestraPreguntas_EncuestasEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraPreguntas_EncuestasEntity> entities = new SoftvList<MuestraPreguntas_EncuestasEntity>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraPreguntas_EncuestasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraPreguntas_EncuestasCount();
                return entities ?? new SoftvList<MuestraPreguntas_EncuestasEntity>();
            }
        }

        /// <summary>
        ///Get MuestraPreguntas_Encuestas
        ///</summary>
        public override SoftvList<MuestraPreguntas_EncuestasEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraPreguntas_EncuestasEntity> entities = new SoftvList<MuestraPreguntas_EncuestasEntity>();
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraPreguntas_EncuestasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraPreguntas_EncuestasCount(xml);
                return entities ?? new SoftvList<MuestraPreguntas_EncuestasEntity>();
            }
        }

        /// <summary>
        ///Get Count MuestraPreguntas_Encuestas
        ///</summary>
        public int GetMuestraPreguntas_EncuestasCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count MuestraPreguntas_Encuestas
        ///</summary>
        public int GetMuestraPreguntas_EncuestasCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionStringEncuestas))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraPreguntas_EncuestasGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraPreguntas_Encuestas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

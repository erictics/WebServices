﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.MuestraTipoFacturaHistorialData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : MuestraTipoFacturaHistorial Data Access Object
    /// File                    : MuestraTipoFacturaHistorialDAO.cs
    /// Creation date           : 21/10/2016
    /// Creation time           : 12:35 p. m.
    ///</summary>
    public class MuestraTipoFacturaHistorialData : MuestraTipoFacturaHistorialProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="MuestraTipoFacturaHistorial"> Object MuestraTipoFacturaHistorial added to List</param>
        public override int AddMuestraTipoFacturaHistorial(MuestraTipoFacturaHistorialEntity entity_MuestraTipoFacturaHistorial)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialAdd", connection);

                AssingParameter(comandoSql, "@CLAVE", entity_MuestraTipoFacturaHistorial.CLAVE);

                AssingParameter(comandoSql, "@CONCEPTO", entity_MuestraTipoFacturaHistorial.CONCEPTO);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdMuestraTipoFacturaHistorial"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a MuestraTipoFacturaHistorial
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteMuestraTipoFacturaHistorial()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a MuestraTipoFacturaHistorial
        ///</summary>
        /// <param name="MuestraTipoFacturaHistorial"> Objeto MuestraTipoFacturaHistorial a editar </param>
        public override int EditMuestraTipoFacturaHistorial(MuestraTipoFacturaHistorialEntity entity_MuestraTipoFacturaHistorial)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialEdit", connection);

                AssingParameter(comandoSql, "@CLAVE", entity_MuestraTipoFacturaHistorial.CLAVE);

                AssingParameter(comandoSql, "@CONCEPTO", entity_MuestraTipoFacturaHistorial.CONCEPTO);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all MuestraTipoFacturaHistorial
        ///</summary>
        public override List<MuestraTipoFacturaHistorialEntity> GetMuestraTipoFacturaHistorial()
        {
            List<MuestraTipoFacturaHistorialEntity> MuestraTipoFacturaHistorialList = new List<MuestraTipoFacturaHistorialEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MuestraTipoFactura_Historial", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraTipoFacturaHistorialList.Add(GetMuestraTipoFacturaHistorialFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraTipoFacturaHistorialList;
        }

        /// <summary>
        /// Gets all MuestraTipoFacturaHistorial by List<int>
        ///</summary>
        public override List<MuestraTipoFacturaHistorialEntity> GetMuestraTipoFacturaHistorial(List<int> lid)
        {
            List<MuestraTipoFacturaHistorialEntity> MuestraTipoFacturaHistorialList = new List<MuestraTipoFacturaHistorialEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        MuestraTipoFacturaHistorialList.Add(GetMuestraTipoFacturaHistorialFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return MuestraTipoFacturaHistorialList;
        }

        /// <summary>
        /// Gets MuestraTipoFacturaHistorial by
        ///</summary>
        public override MuestraTipoFacturaHistorialEntity GetMuestraTipoFacturaHistorialById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialGetById", connection);
                MuestraTipoFacturaHistorialEntity entity_MuestraTipoFacturaHistorial = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_MuestraTipoFacturaHistorial = GetMuestraTipoFacturaHistorialFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_MuestraTipoFacturaHistorial;
            }

        }



        /// <summary>
        ///Get MuestraTipoFacturaHistorial
        ///</summary>
        public override SoftvList<MuestraTipoFacturaHistorialEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<MuestraTipoFacturaHistorialEntity> entities = new SoftvList<MuestraTipoFacturaHistorialEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraTipoFacturaHistorialFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraTipoFacturaHistorialCount();
                return entities ?? new SoftvList<MuestraTipoFacturaHistorialEntity>();
            }
        }

        /// <summary>
        ///Get MuestraTipoFacturaHistorial
        ///</summary>
        public override SoftvList<MuestraTipoFacturaHistorialEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<MuestraTipoFacturaHistorialEntity> entities = new SoftvList<MuestraTipoFacturaHistorialEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetMuestraTipoFacturaHistorialFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetMuestraTipoFacturaHistorialCount(xml);
                return entities ?? new SoftvList<MuestraTipoFacturaHistorialEntity>();
            }
        }

        /// <summary>
        ///Get Count MuestraTipoFacturaHistorial
        ///</summary>
        public int GetMuestraTipoFacturaHistorialCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count MuestraTipoFacturaHistorial
        ///</summary>
        public int GetMuestraTipoFacturaHistorialCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MuestraTipoFacturaHistorial.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_MuestraTipoFacturaHistorialGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data MuestraTipoFacturaHistorial " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

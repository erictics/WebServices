﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Muestra_Detalle_Bitacora_2Data
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Muestra_Detalle_Bitacora_2 Data Access Object
    /// File                    : Muestra_Detalle_Bitacora_2DAO.cs
    /// Creation date           : 21/06/2017
    /// Creation time           : 12:43 p. m.
    ///</summary>
    public class Muestra_Detalle_Bitacora_2Data : Muestra_Detalle_Bitacora_2Provider
    {
      

     
        public override List<Muestra_Detalle_Bitacora_2Entity> GetMuestra_Detalle_Bitacora_2(long? ClvTecnico, int? IdAlmacen)
        {
            List<Muestra_Detalle_Bitacora_2Entity> Muestra_Detalle_Bitacora_2List = new List<Muestra_Detalle_Bitacora_2Entity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Muestra_Detalle_Bitacora_2.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Muestra_Detalle_Bitacora_2", connection);

                AssingParameter(comandoSql, "@clv_tecnico", ClvTecnico);
                AssingParameter(comandoSql, "@idAlmacen", IdAlmacen);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Muestra_Detalle_Bitacora_2List.Add(GetMuestra_Detalle_Bitacora_2FromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Muestra_Detalle_Bitacora_2 " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Muestra_Detalle_Bitacora_2List;
        }


        


        public override List<Muestra_Detalle_Bitacora_2Entity> GetTipoArticuloTecnicoSinSerie_Mantenimiento(long? ClvTecnico)
        {
            List<Muestra_Detalle_Bitacora_2Entity> Muestra_Detalle_Bitacora_2List = new List<Muestra_Detalle_Bitacora_2Entity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Muestra_Detalle_Bitacora_2.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("getTipoArticuloTecnicoSinSerie_Mantenimiento", connection);
                AssingParameter(comandoSql, "@IdTecnico", ClvTecnico);               
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Muestra_Detalle_Bitacora_2List.Add(GetMuestra_Detalle_Bitacora_2FromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Muestra_Detalle_Bitacora_2 " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Muestra_Detalle_Bitacora_2List;
        }


        #region Customs Methods

        #endregion
    }
}

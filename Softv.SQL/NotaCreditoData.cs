﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using CrystalDecisions.CrystalReports.Engine;

namespace Softv.DAO
{
    public class NotaCreditoData : NotaCreditoProvider
    {
        public override List<NotaCreditoEntity> GetBUSCANOTASDECREDITO(int ? op,int ? Clv_NotadeCredito,string Fecha, string contrato,int ? sucursal,string Nombre,int ? Idcompania)
        {
            List<NotaCreditoEntity> result = new List<NotaCreditoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                long contratocompania = 0;
                if (op == 3)
                {
                    contratocompania = long.Parse(contrato.Split('-')[0]);
                    Idcompania = int.Parse(contrato.Split('-')[1]);
                }

                SqlCommand comandoSql = CreateCommand("BUSCANOTASDECREDITO", connection);
                AssingParameter(comandoSql, "@OP", op);
                AssingParameter(comandoSql, "@Clv_NotadeCredito", Clv_NotadeCredito);
                AssingParameter(comandoSql, "@FECHA", Fecha.Replace("/",""));
                AssingParameter(comandoSql, "@CONTRATO", contratocompania);
                AssingParameter(comandoSql, "@Sucursal", sucursal);
                AssingParameter(comandoSql, "@NOMBRE", Nombre);
                AssingParameter(comandoSql, "@idcompania", Idcompania);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        NotaCreditoEntity nc = new NotaCreditoEntity();
                        nc.Clv_NotadeCredito = long.Parse(rd[0].ToString());
                        nc.FechaGeneracion =   DateTime.Parse(rd[1].ToString()).ToShortDateString();
                        nc.ContratoComp = rd[2].ToString();
                        nc.Contrato = long.Parse(rd[13].ToString());
                        nc.SerieFolio = rd[3].ToString();
                        nc.Monto = decimal.Parse(rd[4].ToString());
                        nc.Status = rd[5].ToString();
                        nc.Saldo = decimal.Parse(rd[12].ToString());
                        nc.Factura = long.Parse(rd[14].ToString());
                        result.Add(nc);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data BUSCANOTASDECREDITO " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return result;
        }

        public long Obtencontrato(string contratocompuesto)
        {
            long result = 0;
         
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.FoliosCancelados.ConnectionString))
            {
                try
                {
                    long contratocompania = long.Parse(contratocompuesto.Split('-')[0]);
                    long compania = long.Parse(contratocompuesto.Split('-')[1]);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    SqlCommand comando = new SqlCommand("obtencontratoCliente");
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandTimeout = 0;
                    comando.Connection = connection;

                    SqlParameter par1 = new SqlParameter("@contratocompania", SqlDbType.BigInt);
                    par1.Direction = ParameterDirection.Input;
                    par1.Value = contratocompania;
                    comando.Parameters.Add(par1);

                    SqlParameter par2 = new SqlParameter("@compania", SqlDbType.BigInt);
                    par2.Direction = ParameterDirection.Input;
                    par2.Value = compania;
                    comando.Parameters.Add(par2);

                    SqlParameter par0 = new SqlParameter("@contrato", SqlDbType.BigInt);
                    par0.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par0);
                    comando.ExecuteNonQuery();
                    result = long.Parse(par0.Value.ToString());

                }
                catch (Exception ex)
                {
                    connection.Close();
                   // throw new Exception("Error getting data DameMonto_NotadeCredito " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }

        public override List<StatusNotaEntity> GetStatusNotadeCredito()
        {
            List<StatusNotaEntity> result = new List<StatusNotaEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("StatusNotadeCredito", connection);              
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        StatusNotaEntity nc = new StatusNotaEntity();
                        nc.Clv_Status = rd[0].ToString();
                        nc.Status =rd[1].ToString();                       
                        result.Add(nc);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data StatusNotadeCredito " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return result;
        }

        public override List<DetalleNotaEntity> GetDetalle_NotasdeCredito(long Factura ,long ? Clv_NotadeCredito)
        {
            List<DetalleNotaEntity> result = new List<DetalleNotaEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("Detalle_NotasdeCredito", connection);
                AssingParameter(comandoSql, "@Factura", Factura);
                AssingParameter(comandoSql, "@Clv_NotadeCredito", Clv_NotadeCredito);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DetalleNotaEntity nc = new DetalleNotaEntity();
                        nc.Clv_detalle = long.Parse(rd[0].ToString());
                        nc.Descripcion = rd[1].ToString();
                        nc.Importe = decimal.Parse(rd[2].ToString());
                        nc.Clv_servicio = Int32.Parse(rd[3].ToString());
                        nc.seCobra = bool.Parse(rd[4].ToString());
                        result.Add(nc);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data StatusNotadeCredito " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return result;

        }

        public override List<FacturaClienteEntity> GetDAME_FACTURASDECLIENTE(long ? contrato, long clv_nota)
        {
            List<FacturaClienteEntity> result = new List<FacturaClienteEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("DAME_FACTURASDECLIENTE", connection);
                AssingParameter(comandoSql, "@CONTRATO", contrato);
                AssingParameter(comandoSql, "@clv_nota", clv_nota);
                
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        FacturaClienteEntity nc = new FacturaClienteEntity();
                        nc.Serie =rd[0].ToString();
                        nc.Clv_factura = long.Parse(rd[1].ToString());       
                        nc.Monto = decimal.Parse(rd[2].ToString());
                        result.Add(nc);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DAME_FACTURASDECLIENTE " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return result;
        }

        public override NotaCreditoEntity GetConsulta_NotaCredito(long ? Clv_NotadeCredito)
        {

            NotaCreditoEntity nc = new NotaCreditoEntity();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("Consulta_NotaCredito", connection);               
                AssingParameter(comandoSql, "@Clv_NotadeCredito", Clv_NotadeCredito);              
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        
                        nc.Clv_NotadeCredito = long.Parse(rd[0].ToString());
                        nc.Contrato = long.Parse(rd[1].ToString());
                        nc.ContratoComp = rd[1].ToString();
                        nc.SerieFolio = rd[2].ToString();
                        nc.Factura = long.Parse(rd[3].ToString());
                        nc.FechaGeneracion = DateTime.Parse(rd[4].ToString()).ToShortDateString();
                        nc.Clv_sucursal = long.Parse(rd[5].ToString());
                        nc.UsuarioCaptura = rd[6].ToString();
                        nc.Clv_UsuarioAutorizo = Int32.Parse(rd[7].ToString());
                        nc.FechaCaducidad = DateTime.Parse(rd[8].ToString()).ToShortDateString();
                        nc.Monto = decimal.Parse(rd[9].ToString());
                        nc.Status = rd[10].ToString();
                        nc.Observaciones = rd[11].ToString();
                        nc.Saldo = decimal.Parse(rd[12].ToString());
                        nc.NotaAFactura = int.Parse(rd[14].ToString());
                        nc.Clv_caja = int.Parse(rd[15].ToString());                       
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Consulta_NotaCredito " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return nc;
        }

        public override DatosTicket GetObtieneDatosTicket(long? clv_factura)
        {

            DatosTicket nc = new DatosTicket();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("ObtieneDatosTicket", connection);
                AssingParameter(comandoSql, "@clv_factura", clv_factura);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        nc.Clv_caja = int.Parse(rd[1].ToString());
                        nc.caja = rd[0].ToString();                      
                    }
                    rd.NextResult();
                    while (rd.Read())
                    {
                        nc.Clv_sucursal = int.Parse(rd[1].ToString());
                        nc.sucursal = rd[0].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneDatosTicket " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return nc;
        }

        public override int ? GetNueva_NotadeCredito(NotaCreditoEntity nota)
        {

            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("Nueva_NotadeCredito", connection);               
                AssingParameter(comandoSql, "@Contrato", nota.Contrato);
                AssingParameter(comandoSql, "@Factura", nota.Factura);
                AssingParameter(comandoSql, "@Fecha_deGeneracion", nota.FechaGeneracion);
                AssingParameter(comandoSql, "@Usuario_Captura", nota.UsuarioCaptura);
                AssingParameter(comandoSql, "@Usuario_Autorizo", nota.Clv_UsuarioAutorizo);
                AssingParameter(comandoSql, "@Fecha_Caducidad", nota.FechaCaducidad);
                AssingParameter(comandoSql, "@Monto", nota.Monto);
                AssingParameter(comandoSql, "@Status", nota.Status);
                AssingParameter(comandoSql, "@Observaciones", nota.Observaciones);
                AssingParameter(comandoSql, "@Sucursal", nota.Clv_sucursal);
                AssingParameter(comandoSql, "@suc_aplica", nota.Clv_sucursal);
                AssingParameter(comandoSql, "@tipo", nota.tipo);
                AssingParameter(comandoSql, "@caja", nota.Clv_caja);
                AssingParameter(comandoSql, "@contratoAplicar", nota.Contrato);
                AssingParameter(comandoSql, "@Clv_Notadecredito", null, pd: ParameterDirection.Output, IsKey: true);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                        comandoSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Nueva_NotadeCredito " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                result = (int)comandoSql.Parameters["@Clv_Notadecredito"].Value;
            }
            return result;
        }


        public override int? GetGuarda_DetalleNota(long ? Clv_Factura, long ? Clv_Nota)
        {

            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("Guarda_DetalleNota", connection);
                AssingParameter(comandoSql, "@Clv_Factura", Clv_Factura);
                AssingParameter(comandoSql, "@Clv_Nota", Clv_Nota);               
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                        result= comandoSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Guarda_DetalleNota " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                
            }
            return result;
        }


        public override string GetReportesNotasDeCredito(long? Clv_Nota)
        {

            string name = Guid.NewGuid().ToString() + ".pdf";
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {

                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string ruta = apppath + "/ReportesSistema/";
                string titulo = "Reporte de Permanencia";                
                string fileName = apppath + "/Reportes/" + name;
                StringBuilder sb = new StringBuilder();
                ReportDocument reportDocument = new ReportDocument();
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlCommand comandoSql = CreateCommand("ReportesNotasDeCreditoNew", connection);
                    AssingParameter(comandoSql, "@Clave", Clv_Nota);
                    AssingParameter(comandoSql, "@Clv_nota_Ini", 0);
                    AssingParameter(comandoSql, "@Clv_nota_Fin", 0);
                    AssingParameter(comandoSql, "@Fecha_Ini", "19000101");
                    AssingParameter(comandoSql, "@Fecha_Fin", "19000101");
                    AssingParameter(comandoSql, "@op", 0);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(comandoSql);
                    DataSet dataSet = new DataSet();
                    dataAdapter.Fill(dataSet);

                    dataSet.Tables[0].TableName = "ReportesNotasDeCredito;1";
                    dataSet.Tables[1].TableName = "DetFacturas_NotadeCredito";
                    dataSet.Tables[2].TableName = "Notas_de_Credito";
                    dataSet.Tables[3].TableName = "Rel_NotaCredito_ConceptosServ";
                    dataSet.Tables[4].TableName = "General";
                    reportDocument.Load(ruta + "ReporteNotasdeCredito.rpt");
                    reportDocument.SetDataSource(dataSet);


                    reportDocument.DataDefinition.FormulaFields["Titulo"].Text = "'" + "Nota de Crédito" + "'";
                    reportDocument.DataDefinition.FormulaFields["Clave"].Text = "'" + "Nota de Crédito:" + "'";
                    reportDocument.DataDefinition.FormulaFields["Empresa"].Text = "'" + GetNombreSistema() + "'";                    
                    reportDocument.DataDefinition.FormulaFields["DireccionEmpresa"].Text = "'" + "" + "'";
                    reportDocument.DataDefinition.FormulaFields["Colonia_CpEmpresa"].Text = "'" + "" + "'";
                    reportDocument.DataDefinition.FormulaFields["CiudadEmpresa"].Text = "'" + "" + "'";
                    reportDocument.DataDefinition.FormulaFields["RfcEmpresa"].Text = "'" + "" + "'";
                    reportDocument.DataDefinition.FormulaFields["TelefonoEmpresa"].Text = "'" + "" + "'";
                    reportDocument.DataDefinition.FormulaFields["Copia"].Text = "'Copia'";
                   // reportDocument.DataDefinition.FormulaFields["Copia"].Text = "'Original'";
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ReportesNotasDeCreditoNew " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return name;
        }

        public string GetNombreSistema()
        {
            ConfiguracionData config = new ConfiguracionData();
            PreferenciasEntity pref = config.GetDetallePreferencias();
            return pref.NombreSistema;
        }

        public override int ? GetBorrar_Session_Notas(long Clv_factura,long? Clv_Nota)
        {

            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("Borrar_Session_Notas", connection);
                AssingParameter(comandoSql, "@Clv_factura", Clv_factura);
                AssingParameter(comandoSql, "@clv_notadecredito", Clv_Nota);                         
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = comandoSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Borrar_Session_Notas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return result;
        }


        public override int ? GetModifica_DetalleNotas(long ? Clv_detalle,long ? Clv_factura,bool ? secobra, decimal ? importe)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.MUESTRAUSUARIOS.ConnectionString))
            {
                SqlCommand comandoSql = CreateCommand("Modifica_DetalleNotas", connection);
                AssingParameter(comandoSql, "@Clv_detalle", Clv_detalle);
                AssingParameter(comandoSql, "@Clv_factura", Clv_factura);
                AssingParameter(comandoSql, "@Value", secobra);
                AssingParameter(comandoSql, "@importe", importe);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = comandoSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Modifica_DetalleNotas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return result;
        }


        public override long ? GetCANCELACIONFACTURAS_Notas(long? Clv_Factura, int  ? op)
        {
            long? result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.FoliosCancelados.ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    SqlCommand comando = new SqlCommand("CANCELACIONFACTURAS_Notas");
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandTimeout = 0;
                    comando.Connection = connection;

                    SqlParameter par1 = new SqlParameter("@Clv_Factura", SqlDbType.BigInt);
                    par1.Direction = ParameterDirection.Input;
                    par1.Value = Clv_Factura;
                    comando.Parameters.Add(par1);

                    SqlParameter par0 = new SqlParameter("@MSG", SqlDbType.VarChar, 250);
                    par0.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par0);

                    SqlParameter par2 = new SqlParameter("@BNDERROR", SqlDbType.BigInt);
                    par2.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par2);

                    SqlParameter par3 = new SqlParameter("@OP", SqlDbType.BigInt);
                    par3.Direction = ParameterDirection.Input;
                    par3.Value = op;
                    comando.Parameters.Add(par3);
                    comando.ExecuteNonQuery();
                    result = long.Parse(par2.Value.ToString());

                }
                catch (Exception ex)
                {
                    connection.Close();
                    throw new Exception("Error getting data CANCELACIONFACTURAS_Notas " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }

            }
            return result;
        }


        public override decimal ? GetDameMonto_NotadeCredito(long ? Clv_Nota, long ? contrato)
        {

            decimal result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.FoliosCancelados.ConnectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    SqlCommand comando = new SqlCommand("DameMonto_NotadeCredito");
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandTimeout = 0;
                    comando.Connection = connection;

                    SqlParameter par1 = new SqlParameter("@Clv_Nota", SqlDbType.BigInt);
                    par1.Direction = ParameterDirection.Input;
                    par1.Value = Clv_Nota;
                    comando.Parameters.Add(par1);

                    SqlParameter par2 = new SqlParameter("@contrato", SqlDbType.BigInt);
                    par2.Direction = ParameterDirection.Input;
                    par2.Value = contrato;
                    comando.Parameters.Add(par2);

                    SqlParameter par0 = new SqlParameter("@Monto", SqlDbType.Decimal);
                    par0.Direction = ParameterDirection.Output;
                    comando.Parameters.Add(par0);                    
                    comando.ExecuteNonQuery();
                    result = decimal.Parse(par0.Value.ToString());

                }
                catch (Exception ex)
                {
                    connection.Close();
                    throw new Exception("Error getting data DameMonto_NotadeCredito " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }

            }
            return result;

        }




    }
}

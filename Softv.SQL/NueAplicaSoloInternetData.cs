﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.NueAplicaSoloInternetData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : NueAplicaSoloInternet Data Access Object
    /// File                    : NueAplicaSoloInternetDAO.cs
    /// Creation date           : 18/09/2017
    /// Creation time           : 12:29 p. m.
    ///</summary>
    public class NueAplicaSoloInternetData : NueAplicaSoloInternetProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="NueAplicaSoloInternet"> Object NueAplicaSoloInternet added to List</param>
        public override int AddNueAplicaSoloInternet(NueAplicaSoloInternetEntity entity_NueAplicaSoloInternet)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("NueAplicaSoloInternet", connection);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_NueAplicaSoloInternet.Clv_Servicio);


                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                //result = (int)comandoSql.Parameters["@IdNueAplicaSoloInternet"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a NueAplicaSoloInternet
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteNueAplicaSoloInternet()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a NueAplicaSoloInternet
        ///</summary>
        /// <param name="NueAplicaSoloInternet"> Objeto NueAplicaSoloInternet a editar </param>
        public override int EditNueAplicaSoloInternet(NueAplicaSoloInternetEntity entity_NueAplicaSoloInternet)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetEdit", connection);

                //AssingParameter(comandoSql, "@Clv_Servicio", entity_NueAplicaSoloInternet.Clv_Servicio);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all NueAplicaSoloInternet
        ///</summary>
        public override List<NueAplicaSoloInternetEntity> GetNueAplicaSoloInternet()
        {
            List<NueAplicaSoloInternetEntity> NueAplicaSoloInternetList = new List<NueAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        NueAplicaSoloInternetList.Add(GetNueAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return NueAplicaSoloInternetList;
        }

        /// <summary>
        /// Gets all NueAplicaSoloInternet by List<int>
        ///</summary>
        public override List<NueAplicaSoloInternetEntity> GetNueAplicaSoloInternet(List<int> lid)
        {
            List<NueAplicaSoloInternetEntity> NueAplicaSoloInternetList = new List<NueAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        NueAplicaSoloInternetList.Add(GetNueAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return NueAplicaSoloInternetList;
        }

        /// <summary>
        /// Gets NueAplicaSoloInternet by
        ///</summary>
        public override NueAplicaSoloInternetEntity GetNueAplicaSoloInternetById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetGetById", connection);
                NueAplicaSoloInternetEntity entity_NueAplicaSoloInternet = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_NueAplicaSoloInternet = GetNueAplicaSoloInternetFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_NueAplicaSoloInternet;
            }

        }



        /// <summary>
        ///Get NueAplicaSoloInternet
        ///</summary>
        public override SoftvList<NueAplicaSoloInternetEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<NueAplicaSoloInternetEntity> entities = new SoftvList<NueAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetNueAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetNueAplicaSoloInternetCount();
                return entities ?? new SoftvList<NueAplicaSoloInternetEntity>();
            }
        }

        /// <summary>
        ///Get NueAplicaSoloInternet
        ///</summary>
        public override SoftvList<NueAplicaSoloInternetEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<NueAplicaSoloInternetEntity> entities = new SoftvList<NueAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetNueAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetNueAplicaSoloInternetCount(xml);
                return entities ?? new SoftvList<NueAplicaSoloInternetEntity>();
            }
        }

        /// <summary>
        ///Get Count NueAplicaSoloInternet
        ///</summary>
        public int GetNueAplicaSoloInternetCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count NueAplicaSoloInternet
        ///</summary>
        public int GetNueAplicaSoloInternetCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.NueAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_NueAplicaSoloInternetGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data NueAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

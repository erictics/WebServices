﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ObtieneHistorialPagosFacturaMaestroData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ObtieneHistorialPagosFacturaMaestro Data Access Object
    /// File                    : ObtieneHistorialPagosFacturaMaestroDAO.cs
    /// Creation date           : 19/04/2017
    /// Creation time           : 05:40 p. m.
    ///</summary>
    public class ObtieneHistorialPagosFacturaMaestroData : ObtieneHistorialPagosFacturaMaestroProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="ObtieneHistorialPagosFacturaMaestro"> Object ObtieneHistorialPagosFacturaMaestro added to List</param>
        public override int AddObtieneHistorialPagosFacturaMaestro(ObtieneHistorialPagosFacturaMaestroEntity entity_ObtieneHistorialPagosFacturaMaestro)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroAdd", connection);

                AssingParameter(comandoSql, "@Clv_Pago", entity_ObtieneHistorialPagosFacturaMaestro.Clv_Pago);

                AssingParameter(comandoSql, "@Hora", entity_ObtieneHistorialPagosFacturaMaestro.Hora);

                AssingParameter(comandoSql, "@Cajera", entity_ObtieneHistorialPagosFacturaMaestro.Cajera);

                AssingParameter(comandoSql, "@Importe", entity_ObtieneHistorialPagosFacturaMaestro.Importe);

                AssingParameter(comandoSql, "@Caja", entity_ObtieneHistorialPagosFacturaMaestro.Caja);

                AssingParameter(comandoSql, "@Sucursal", entity_ObtieneHistorialPagosFacturaMaestro.Sucursal);

                AssingParameter(comandoSql, "@MedioPago", entity_ObtieneHistorialPagosFacturaMaestro.MedioPago);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdObtieneHistorialPagosFacturaMaestro"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteObtieneHistorialPagosFacturaMaestro()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        /// <param name="ObtieneHistorialPagosFacturaMaestro"> Objeto ObtieneHistorialPagosFacturaMaestro a editar </param>
        public override int EditObtieneHistorialPagosFacturaMaestro(ObtieneHistorialPagosFacturaMaestroEntity entity_ObtieneHistorialPagosFacturaMaestro)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroEdit", connection);

                AssingParameter(comandoSql, "@Clv_Pago", entity_ObtieneHistorialPagosFacturaMaestro.Clv_Pago);

                AssingParameter(comandoSql, "@Hora", entity_ObtieneHistorialPagosFacturaMaestro.Hora);

                AssingParameter(comandoSql, "@Cajera", entity_ObtieneHistorialPagosFacturaMaestro.Cajera);

                AssingParameter(comandoSql, "@Importe", entity_ObtieneHistorialPagosFacturaMaestro.Importe);

                AssingParameter(comandoSql, "@Caja", entity_ObtieneHistorialPagosFacturaMaestro.Caja);

                AssingParameter(comandoSql, "@Sucursal", entity_ObtieneHistorialPagosFacturaMaestro.Sucursal);

                AssingParameter(comandoSql, "@MedioPago", entity_ObtieneHistorialPagosFacturaMaestro.MedioPago);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        public override List<ObtieneHistorialPagosFacturaMaestroEntity> GetObtieneHistorialPagosFacturaMaestro(long? ClvFacturaMaestro)
        {
            List<ObtieneHistorialPagosFacturaMaestroEntity> ObtieneHistorialPagosFacturaMaestroList = new List<ObtieneHistorialPagosFacturaMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ObtieneHistorialPagosFacturaMaestro", connection);
                IDataReader rd = null;

                AssingParameter(comandoSql, "@Clv_FacturaMaestro", ClvFacturaMaestro);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ObtieneHistorialPagosFacturaMaestroList.Add(GetObtieneHistorialPagosFacturaMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ObtieneHistorialPagosFacturaMaestroList;
        }

        /// <summary>
        /// Gets all ObtieneHistorialPagosFacturaMaestro by List<int>
        ///</summary>
        public override List<ObtieneHistorialPagosFacturaMaestroEntity> GetObtieneHistorialPagosFacturaMaestro(List<int> lid)
        {
            List<ObtieneHistorialPagosFacturaMaestroEntity> ObtieneHistorialPagosFacturaMaestroList = new List<ObtieneHistorialPagosFacturaMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ObtieneHistorialPagosFacturaMaestroList.Add(GetObtieneHistorialPagosFacturaMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ObtieneHistorialPagosFacturaMaestroList;
        }

        /// <summary>
        /// Gets ObtieneHistorialPagosFacturaMaestro by
        ///</summary>
        public override ObtieneHistorialPagosFacturaMaestroEntity GetObtieneHistorialPagosFacturaMaestroById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroGetById", connection);
                ObtieneHistorialPagosFacturaMaestroEntity entity_ObtieneHistorialPagosFacturaMaestro = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_ObtieneHistorialPagosFacturaMaestro = GetObtieneHistorialPagosFacturaMaestroFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_ObtieneHistorialPagosFacturaMaestro;
            }

        }



        /// <summary>
        ///Get ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        public override SoftvList<ObtieneHistorialPagosFacturaMaestroEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ObtieneHistorialPagosFacturaMaestroEntity> entities = new SoftvList<ObtieneHistorialPagosFacturaMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetObtieneHistorialPagosFacturaMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetObtieneHistorialPagosFacturaMaestroCount();
                return entities ?? new SoftvList<ObtieneHistorialPagosFacturaMaestroEntity>();
            }
        }

        /// <summary>
        ///Get ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        public override SoftvList<ObtieneHistorialPagosFacturaMaestroEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ObtieneHistorialPagosFacturaMaestroEntity> entities = new SoftvList<ObtieneHistorialPagosFacturaMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetObtieneHistorialPagosFacturaMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetObtieneHistorialPagosFacturaMaestroCount(xml);
                return entities ?? new SoftvList<ObtieneHistorialPagosFacturaMaestroEntity>();
            }
        }

        /// <summary>
        ///Get Count ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        public int GetObtieneHistorialPagosFacturaMaestroCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count ObtieneHistorialPagosFacturaMaestro
        ///</summary>
        public int GetObtieneHistorialPagosFacturaMaestroCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ObtieneHistorialPagosFacturaMaestro.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ObtieneHistorialPagosFacturaMaestroGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ObtieneHistorialPagosFacturaMaestro " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;
using OfficeOpenXml;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Xml.Linq;
//------------
using System.Xml;
using System.Configuration;
using System.Xml.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web;
using System.Net;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using System.Drawing;

namespace Softv.DAO
{
    public class PolizaMaestroData: PolizaMaestroProvider
    {

        public override List<PolizasMaestroEntity> GetObtienePolizasMaestro(FiltroPolizaEntity filtros)
        {
            List<PolizasMaestroEntity> GetObtienePolizasMaestro = new List<PolizasMaestroEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ContratoMaestroFac.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ObtienePolizasMaestro", connection);

                AssingParameter(comandoSql, "@Op", filtros.Op);

                AssingParameter(comandoSql, "@Clv_Plaza", filtros.Clv_Plaza);

                AssingParameter(comandoSql, "@Fecha", filtros.FechaPoliza);

                AssingParameter(comandoSql, "@Dolares", filtros.Dolares);

                IDataReader rd = null;

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        GetObtienePolizasMaestro.Add(GetObtienePolizasMaestroFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ContratoMaestroFac " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return GetObtienePolizasMaestro;
        }

        public override int? GetEliminaPoliza(FiltroPolizaEntity filtros)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ContratoMaestroFac.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("EliminaPolizaMaestro", connection);

                AssingParameter(comandoSql, "@Clv_Poliza", filtros.Clv_Poliza);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    ExecuteNonQuery(comandoSql);

                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ContratoMaestroFac " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return 0;
        }

        public override List<DetallePolizaEntity> GetDetallesPolizaMaestro(FiltroPolizaEntity filtros)
        {
            List<DetallePolizaEntity> GetDetallesPolizaMaestro = new List<DetallePolizaEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ContratoMaestroFac.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ObtieneDetallePolizaMaestro", connection);

                AssingParameter(comandoSql, "@Clv_Poliza", filtros.Clv_Poliza);

                IDataReader rd = null;

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        DetallePolizaEntity entity_Detalle = new DetallePolizaEntity();
                        entity_Detalle.Cuenta = (String)(GetFromReader(rd, "Cuenta", IsString: true));
                        entity_Detalle.Descripcion = (String)(GetFromReader(rd, "Descripcion", IsString: true));
                        entity_Detalle.Debe = (decimal)(GetFromReader(rd, "Debe"));
                        entity_Detalle.Haber = (decimal)(GetFromReader(rd, "Haber"));
                        entity_Detalle.Orden = (int)(GetFromReader(rd, "Orden"));
                        GetDetallesPolizaMaestro.Add(entity_Detalle);
                    }
                    GetDetallesPolizaMaestro = GetDetallesPolizaMaestro.OrderBy(x => x.Orden).ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ContratoMaestroFac " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return GetDetallesPolizaMaestro;
        }

        public override PolizasMaestroEntity GetGeneraNuevaPolizaMaestro(FiltroPolizaEntity filtros)
        {
            PolizasMaestroEntity GetGeneraNuevaPolizaMaestro = new PolizasMaestroEntity();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ContratoMaestroFac.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("GeneraPolizaMaestro", connection);

                AssingParameter(comandoSql, "@FechaFin", filtros.FechaPoliza);

                AssingParameter(comandoSql, "@FEchaIni", filtros.FechaPoliza);

                AssingParameter(comandoSql, "@Clv_Plaza", filtros.Clv_Plaza);

                AssingParameter(comandoSql, "@Dolares", filtros.Dolares);

                IDataReader rd = null;

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    if (rd.Read())
                    {
                        GetGeneraNuevaPolizaMaestro.Clv_Plaza = (int?)(GetFromReader(rd, "Clv_Plaza"));
                        GetGeneraNuevaPolizaMaestro.FechaGeneracion = (String)(GetFromReader(rd, "FechaGeneracion", IsString: true));
                        GetGeneraNuevaPolizaMaestro.FechaPoliza = (String)(GetFromReader(rd, "FechaPoliza", IsString: true));
                        GetGeneraNuevaPolizaMaestro.Plaza = (String)(GetFromReader(rd, "Plaza", IsString: true));
                        GetGeneraNuevaPolizaMaestro.Clv_Poliza = (long?)(GetFromReader(rd, "Clv_Poliza"));
                        GetGeneraNuevaPolizaMaestro.Dolares = (String)(GetFromReader(rd, "Dolares", IsString: true));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ContratoMaestroFac " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return GetGeneraNuevaPolizaMaestro;
        }

        public override string GetPolizaTxt(FiltroPolizaEntity filtros)
        {
            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "/";
            string archivo = Guid.NewGuid() + ".txt";
            string ruta = folder + archivo;

            string Referencia_Servicio = null;
            try
            {
                string Nom_Archivo = null;
                string Encabezado = null;
                string imp1 = null;
                string Rutatxt = null;
                string fecha = "";
                string fecha2 = "";
                int DiaEntero = 0;
                int DiaActual = 0;
                string MesEntero = "";
                int contaX = 0;
                string[] tabla = null;
                string aux_cuenta = "";
                string cuenta_Buena = "";
                string cuenta_Aux = "";

                DBFuncion db1 = new DBFuncion();
                db1.agregarParametro("@Clv_Poliza", SqlDbType.BigInt, filtros.Clv_Poliza);
                IDataReader reader = db1.consultaReader("DameFechaActualMaestro");
                reader.Read();
                string ano = (String)(GetFromReader(reader, "Año", IsString: true));
                string mes = (String)(GetFromReader(reader, "mes", IsString: true));
                string dia = (String)(GetFromReader(reader, "dia", IsString: true));
                string ano2 = (String)(GetFromReader(reader, "ano", IsString: true));
                string dia2 = (String)(GetFromReader(reader, "dia2", IsString: true));
                string mes2 = (String)(GetFromReader(reader, "mes2", IsString: true));

                fecha = ano + mes + dia;
                fecha2 = ano2 + mes2 + dia2;
                DiaActual = Convert.ToInt32(dia2);
                
                DiaEntero = Convert.ToInt32(mes2);
                if (DiaEntero == 1)
                {
                    MesEntero = "Enero";
                }
                if (DiaEntero == 2)
                {
                    MesEntero = "Febrero";
                }
                if (DiaEntero == 3)
                {
                    MesEntero = "Marzo";
                }
                if (DiaEntero == 4)
                {
                    MesEntero = "Abril";
                }
                if (DiaEntero == 5)
                {
                    MesEntero = "Mayo";
                }
                if (DiaEntero == 6)
                {
                    MesEntero = "Junio";
                }
                if (DiaEntero == 7)
                {
                    MesEntero = "Julio";
                }
                if (DiaEntero == 8)
                {
                    MesEntero = "Agosto";
                }
                if (DiaEntero == 9)
                {
                    MesEntero = "Septiembre";
                }
                if (DiaEntero == 10)
                {
                    MesEntero = "Octubre";
                }
                if (DiaEntero == 11)
                {
                    MesEntero = "Noviembre";
                }
                if (DiaEntero == 12)
                {
                    MesEntero = "Diciembre";
                }
                
                Rutatxt = ruta;
                Nom_Archivo = ruta;

                using (StreamWriter sw = File.CreateText(Nom_Archivo))
                {
                    string indice = "";

                    string id = "P";
                    string cuenta = "";
                    string descripcion = "";
                    string importe = "";
                    string tipo = "";
                    string espacio = "";
                    string espacio2 = "";
                    string espacio3 = "";
                    string espacio4 = "";
                    string num_poliza = "";
                    string importe_aux = "";
                    string Titulo = "";
                    string Titulo_Bueno = "";
                    int polizaa = 0;
                    int contador = 0;
                    string dia_aux = "";

                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@CLV_POLIZA", SqlDbType.Int, filtros.Clv_Poliza);
                    db2.agregarParametro("@CONCEPTO", SqlDbType.VarChar, ParameterDirection.Output, 100);
                    db2.consultaOutput("Concepto_polizaMaestro");
                    string conceptoPoliza = db2.diccionarioOutput["@CONCEPTO"].ToString();
                    //=== INDICE ===
                    dia_aux = Rellena_Text2((Convert.ToString(DiaActual)), 2, " ");
                    Titulo_Bueno = Rellena_Espacios(conceptoPoliza.Trim(), 100);
                    num_poliza = Rellena_Text(filtros.Clv_Poliza.ToString(), 3, "0");
                    indice = id + "  " + fecha2 + "    " + "1" + "        " + dia_aux + " " + "1" + " " + "0" + "          " + Titulo_Bueno + " " + "11" + " " + "0 0 ";
                    sw.Write(indice + Environment.NewLine);

                    DBFuncion db3 = new DBFuncion();
                    db3.agregarParametro("@num_poliza", SqlDbType.BigInt, filtros.Clv_Poliza);
                    SqlDataReader reader3 = db3.consultaReader("Muestra_Detalle_PolizaMaestro");
                    
                    while (reader3.Read())
                    {
                        if (reader3.GetValue(0) == null)
                        {
                            break; // TODO: might not be correct. Was : Exit While
                        }

                        importe_aux = Convert.ToString(Convert.ToDecimal(reader3.GetValue(3)));
                        cuenta_Aux = reader3.GetValue(1).ToString().Trim();
                        cuenta_Buena = cuenta_Aux.Replace("-", "");

                        cuenta = Rellena_Espacios(cuenta_Buena, 20);// Strings.Mid(Strings.Trim(cuenta_Buena), 1, 20) + Microsoft.VisualBasic.Strings.Space(20 - Strings.Len(Strings.Mid(Strings.Trim(cuenta_Buena), 1, 20)));
                        espacio = "         ";
                        tipo = Convert.ToString(reader3.GetValue(5));
                        //importe = Mid(Trim(reader.GetValue(3)), 1, 16) & Microsoft.VisualBasic.Strings.Space(16 - Len(Mid(Trim(reader.GetValue(3)), 16, 16)))
                        importe_aux = Rellena_Text2((importe_aux), 20, " ");
                        espacio2 = "0";
                        espacio3 = "          ";
                        
                        espacio4 = "                  ";

                        descripcion = Rellena_Espacios(reader3.GetValue(2).ToString(), 100);
                        //sw.Write("M" + "  " + cuenta + " " + espacio + RellenaEspaciosDerecha(facturaGlobal, 10) + tipo + " " + importe_aux + " " + espacio2 + espacio3 + "0.0" + espacio4 + descripcion + " " + "   0 ")
                        sw.Write("M" + "  " + cuenta + " " + espacio + "I-" + dia_aux + "       " + tipo + " " + importe_aux + " " + espacio2 + espacio3 + "0.0" + espacio4 + descripcion + " " + "   0 ");

                        sw.Write(Environment.NewLine);

                        contador = contador + 1;
                    }
                    sw.Close();
                }
            }
            catch (System.Exception ex)
            {

            }

            return archivo;
            
        }
        public string Rellena_Text2(string Valor, int Longitud, string Relleno)
        {
            string functionReturnValue = null;
            int Contador = 0;
            int Total = 0;
            Contador = Valor.Length;
            functionReturnValue = "";
            Total = Longitud - Contador;
            int i = 0;
            i = 1;
            for (i = 1; i <= Total; i++)
            {
                functionReturnValue = functionReturnValue + Relleno;
                //i = i + 1
            }
            functionReturnValue = Valor + functionReturnValue;
            return functionReturnValue;
        }

        public string Rellena_Text(string Valor, int Longitud, string Relleno)
        {
            string functionReturnValue = null;
            int Contador = 0;
            int Total = 0;
            Contador = Valor.Length;
            functionReturnValue = "";
            Total = Longitud - Contador;
            int i = 0;
            i = 1;
            for (i = 1; i <= Total; i++)
            {
                functionReturnValue = functionReturnValue + Relleno;
                //i = i + 1
            }
            functionReturnValue = functionReturnValue + Valor;
            return functionReturnValue;
        }

        public string Rellena_Espacios(string Valor, int Longitud)
        {
            while (Valor.Length < Longitud)
            {
                Valor = Valor + " ";
            }
            return Valor;
        }
    }
}

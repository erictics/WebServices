﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.RelLocalidadMunEstData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelLocalidadMunEst Data Access Object
    /// File                    : RelLocalidadMunEstDAO.cs
    /// Creation date           : 03/02/2016
    /// Creation time           : 06:54 p. m.
    ///</summary>
    public class RelLocalidadMunEstData : RelLocalidadMunEstProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="RelLocalidadMunEst"> Object RelLocalidadMunEst added to List</param>
        public override int AddRelLocalidadMunEst(RelLocalidadMunEstEntity entity_RelLocalidadMunEst)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstAdd", connection);

                //AssingParameter(comandoSql, "@IdLocalidad", null, pd: ParameterDirection.Output, IsKey: true);

                //AssingParameter(comandoSql, "@IdMunicipio", null, pd: ParameterDirection.Output, IsKey: true);

                //AssingParameter(comandoSql, "@IdEstado", null, pd: ParameterDirection.Output, IsKey: true);

                AssingParameter(comandoSql, "@IdLocalidad", entity_RelLocalidadMunEst.IdLocalidad);

                AssingParameter(comandoSql, "@IdMunicipio", entity_RelLocalidadMunEst.IdMunicipio);

                AssingParameter(comandoSql, "@IdEstado", entity_RelLocalidadMunEst.IdEstado);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                //result = (int)comandoSql.Parameters["@IdRelLocalidadMunEst"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a RelLocalidadMunEst
        ///</summary>
        /// <param name="">  IdLocalidad,IdMunicipio,IdEstado to delete </param>
        public override int DeleteRelLocalidadMunEst(int? IdLocalidad, int? IdMunicipio, int? IdEstado)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstDelete", connection);

                AssingParameter(comandoSql, "@IdLocalidad", IdLocalidad);

                AssingParameter(comandoSql, "@IdMunicipio", IdMunicipio);

                AssingParameter(comandoSql, "@IdEstado", IdEstado);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a RelLocalidadMunEst
        ///</summary>
        /// <param name="RelLocalidadMunEst"> Objeto RelLocalidadMunEst a editar </param>
        public override int EditRelLocalidadMunEst(RelLocalidadMunEstEntity entity_RelLocalidadMunEst)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstEdit", connection);

                AssingParameter(comandoSql, "@IdLocalidad", entity_RelLocalidadMunEst.IdLocalidad);

                AssingParameter(comandoSql, "@IdMunicipio", entity_RelLocalidadMunEst.IdMunicipio);

                AssingParameter(comandoSql, "@IdEstado", entity_RelLocalidadMunEst.IdEstado);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all RelLocalidadMunEst
        ///</summary>
        public override List<RelLocalidadMunEstEntity> GetRelLocalidadMunEst()
        {
            List<RelLocalidadMunEstEntity> RelLocalidadMunEstList = new List<RelLocalidadMunEstEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelLocalidadMunEstList.Add(GetRelLocalidadMunEstFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelLocalidadMunEstList;
        }

        /// <summary>
        /// Gets all RelLocalidadMunEst by List<int>
        ///</summary>
        public override List<RelLocalidadMunEstEntity> GetRelLocalidadMunEst(List<int> lid)
        {
            List<RelLocalidadMunEstEntity> RelLocalidadMunEstList = new List<RelLocalidadMunEstEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelLocalidadMunEstList.Add(GetRelLocalidadMunEstFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelLocalidadMunEstList;
        }

        /// <summary>
        /// Gets RelLocalidadMunEst by
        ///</summary>
        public override RelLocalidadMunEstEntity GetRelLocalidadMunEstById(int? IdLocalidad, int? IdMunicipio, int? IdEstado)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetById", connection);
                RelLocalidadMunEstEntity entity_RelLocalidadMunEst = null;


                AssingParameter(comandoSql, "@IdLocalidad", IdLocalidad);

                AssingParameter(comandoSql, "@IdMunicipio", IdMunicipio);

                AssingParameter(comandoSql, "@IdEstado", IdEstado);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_RelLocalidadMunEst = GetRelLocalidadMunEstFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_RelLocalidadMunEst;
            }

        }


        public override List<RelLocalidadMunEstEntity> GetRelLocalidadMunEstByIdEstado(int? IdEstado)
        {
            List<RelLocalidadMunEstEntity> RelLocalidadMunEstList = new List<RelLocalidadMunEstEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetByIdEstado", connection);

                AssingParameter(comandoSql, "@IdEstado", IdEstado);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelLocalidadMunEstList.Add(GetRelLocalidadMunEstFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelLocalidadMunEstList;
        }

        public override List<RelLocalidadMunEstEntity> GetRelLocalidadMunEstByIdLocalidad(int? IdLocalidad)
        {
            List<RelLocalidadMunEstEntity> RelLocalidadMunEstList = new List<RelLocalidadMunEstEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetByIdLocalidad", connection);

                AssingParameter(comandoSql, "@IdLocalidad", IdLocalidad);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelLocalidadMunEstList.Add(GetRelLocalidadMunEstFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelLocalidadMunEstList;
        }

        public override List<RelLocalidadMunEstEntity> GetRelLocalidadMunEstByIdMunicipio(int? IdMunicipio)
        {
            List<RelLocalidadMunEstEntity> RelLocalidadMunEstList = new List<RelLocalidadMunEstEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetByIdMunicipio", connection);

                AssingParameter(comandoSql, "@IdMunicipio", IdMunicipio);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelLocalidadMunEstList.Add(GetRelLocalidadMunEst_dosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelLocalidadMunEstList;
        }


        /// <summary>
        ///Get RelLocalidadMunEst
        ///</summary>
        public override SoftvList<RelLocalidadMunEstEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<RelLocalidadMunEstEntity> entities = new SoftvList<RelLocalidadMunEstEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRelLocalidadMunEstFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRelLocalidadMunEstCount();
                return entities ?? new SoftvList<RelLocalidadMunEstEntity>();
            }
        }

        /// <summary>
        ///Get RelLocalidadMunEst
        ///</summary>
        public override SoftvList<RelLocalidadMunEstEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<RelLocalidadMunEstEntity> entities = new SoftvList<RelLocalidadMunEstEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRelLocalidadMunEstFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRelLocalidadMunEstCount(xml);
                return entities ?? new SoftvList<RelLocalidadMunEstEntity>();
            }
        }

        /// <summary>
        ///Get Count RelLocalidadMunEst
        ///</summary>
        public int GetRelLocalidadMunEstCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count RelLocalidadMunEst
        ///</summary>
        public int GetRelLocalidadMunEstCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelLocalidadMunEst.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelLocalidadMunEstGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelLocalidadMunEst " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

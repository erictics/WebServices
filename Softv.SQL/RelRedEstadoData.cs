﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.RelRedEstadoData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelRedEstado Data Access Object
    /// File                    : RelRedEstadoDAO.cs
    /// Creation date           : 10/11/2017
    /// Creation time           : 05:10 p. m.
    ///</summary>
    public class RelRedEstadoData : RelRedEstadoProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="RelRedEstado"> Object RelRedEstado added to List</param>
        public override int AddRelRedEstado(RelRedEstadoEntity entity_RelRedEstado)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoAdd", connection);

                AssingParameter(comandoSql, "@IdRed", entity_RelRedEstado.IdRed);

                AssingParameter(comandoSql, "@Clv_Estado", entity_RelRedEstado.Clv_Estado);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdRelRedEstado"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a RelRedEstado
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteRelRedEstado()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a RelRedEstado
        ///</summary>
        /// <param name="RelRedEstado"> Objeto RelRedEstado a editar </param>
        public override int EditRelRedEstado(RelRedEstadoEntity entity_RelRedEstado)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Add_RelRedEstado", connection);

                AssingParameter(comandoSql, "@IdRed", entity_RelRedEstado.IdRed);
                AssingParameter(comandoSql, "@Clv_Estado", entity_RelRedEstado.Clv_Estado);
                AssingParameter(comandoSql, "@Op", entity_RelRedEstado.Op);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all RelRedEstado
        ///</summary>
        public override List<RelRedEstadoEntity> GetRelRedEstado(int? IdRed)
        {
            List<RelRedEstadoEntity> RelRedEstadoList = new List<RelRedEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Get_RelRedEstado_Inc", connection);

                AssingParameter(comandoSql, "@IdRed", IdRed);

                IDataReader rd = null;
                try
                 {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelRedEstadoEntity a = new RelRedEstadoEntity();
                        a.IdRed = Int32.Parse(rd[0].ToString());
                        a.Clv_Estado = Int32.Parse(rd[1].ToString());
                        a.Nombre = rd[2].ToString();
                        RelRedEstadoList.Add(a);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelRedEstadoList;
        }



        public override List<RelRedEstadoEntity> GetRelRedEstado_Dis(int? IdRed)
        {
            List<RelRedEstadoEntity> RelRedEstadoList = new List<RelRedEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Get_RelRedEstado_Dis", connection);

                AssingParameter(comandoSql, "@IdRed", IdRed);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelRedEstadoList.Add(GetRelRedEstado_tresFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelRedEstadoList;
        }




        /// <summary>
        /// Gets all RelRedEstado by List<int>
        ///</summary>
        public override List<RelRedEstadoEntity> GetRelRedEstado(List<int> lid)
        {
            List<RelRedEstadoEntity> RelRedEstadoList = new List<RelRedEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RelRedEstadoList.Add(GetRelRedEstadoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RelRedEstadoList;
        }

        /// <summary>
        /// Gets RelRedEstado by
        ///</summary>
        public override RelRedEstadoEntity GetRelRedEstadoById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoGetById", connection);
                RelRedEstadoEntity entity_RelRedEstado = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_RelRedEstado = GetRelRedEstadoFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_RelRedEstado;
            }

        }



        /// <summary>
        ///Get RelRedEstado
        ///</summary>
        public override SoftvList<RelRedEstadoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<RelRedEstadoEntity> entities = new SoftvList<RelRedEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRelRedEstadoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRelRedEstadoCount();
                return entities ?? new SoftvList<RelRedEstadoEntity>();
            }
        }

        /// <summary>
        ///Get RelRedEstado
        ///</summary>
        public override SoftvList<RelRedEstadoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<RelRedEstadoEntity> entities = new SoftvList<RelRedEstadoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRelRedEstadoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRelRedEstadoCount(xml);
                return entities ?? new SoftvList<RelRedEstadoEntity>();
            }
        }

        /// <summary>
        ///Get Count RelRedEstado
        ///</summary>
        public int GetRelRedEstadoCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count RelRedEstado
        ///</summary>
        public int GetRelRedEstadoCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelRedEstado.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RelRedEstadoGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RelRedEstado " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

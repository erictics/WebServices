﻿
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Data;
    using System.Linq;
    using System.Data.SqlClient;
    using Softv.Entities;
    using Softv.Providers;
    using SoftvConfiguration;
    using Globals;

    namespace Softv.DAO
    {
    /// <summary>
    /// Class                   : Softv.DAO.RelUnicanetMedioData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RelUnicanetMedio Data Access Object
    /// File                    : RelUnicanetMedioDAO.cs
    /// Creation date           : 22/08/2017
    /// Creation time           : 12:55 p. m.
    ///</summary>
    public class RelUnicanetMedioData : RelUnicanetMedioProvider
    {
    /// <summary>
    ///</summary>
    /// <param name="RelUnicanetMedio"> Object RelUnicanetMedio added to List</param>
    public override int AddRelUnicanetMedio(RelUnicanetMedioEntity entity_RelUnicanetMedio)
    {
    int result=0;
    using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioAdd", connection);
    
            AssingParameter(comandoSql,"@Clv_UnicaNet",entity_RelUnicanetMedio.Clv_UnicaNet);
          
            AssingParameter(comandoSql,"@idMedio",entity_RelUnicanetMedio.idMedio);
          
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    result = ExecuteNonQuery(comandoSql);
    }
    catch (Exception ex)
    {
    throw new Exception("Error adding RelUnicanetMedio " + ex.Message, ex);
    }
    finally
    {
    connection.Close();
    }
    result = (int)comandoSql.Parameters["@IdRelUnicanetMedio"].Value;
    }
    return result;
    }

    /// <summary>
    /// Deletes a RelUnicanetMedio
    ///</summary>
    /// <param name="">   to delete </param>
    public override int DeleteRelUnicanetMedio()
    {
    int result=0;
    using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioDelete", connection);
    
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    result = ExecuteNonQuery(comandoSql);
    }
    catch (Exception ex)
    {
    throw new Exception("Error deleting RelUnicanetMedio " + ex.Message, ex);
    }
    finally
    {
    if(connection != null)
    connection.Close();
    }
    }
    return result;
    }

    /// <summary>
    /// Edits a RelUnicanetMedio
    ///</summary>
    /// <param name="RelUnicanetMedio"> Objeto RelUnicanetMedio a editar </param>
    public override int EditRelUnicanetMedio(RelUnicanetMedioEntity entity_RelUnicanetMedio)
    {
    int result=0;
    using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioEdit", connection);
    
            AssingParameter(comandoSql,"@Clv_UnicaNet",entity_RelUnicanetMedio.Clv_UnicaNet);
          
            AssingParameter(comandoSql,"@idMedio",entity_RelUnicanetMedio.idMedio);
          
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    
        result = int.Parse(ExecuteNonQuery(comandoSql).ToString());
          
    }
    catch (Exception ex)
    {
    throw new Exception("Error updating RelUnicanetMedio " + ex.Message, ex);
    }
    finally
    {
    if(connection != null)
    connection.Close();
    }
    }
    return result;
    }

    /// <summary>
    /// Gets all RelUnicanetMedio
    ///</summary>
    public override List<RelUnicanetMedioEntity> GetRelUnicanetMedio()
    {
    List<RelUnicanetMedioEntity> RelUnicanetMedioList = new List<RelUnicanetMedioEntity>();
    using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioGet", connection);
    IDataReader rd = null;
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    rd = ExecuteReader(comandoSql);

    while (rd.Read())
    {
    RelUnicanetMedioList.Add(GetRelUnicanetMedioFromReader(rd));
    }
    }
    catch (Exception ex)
    {
    throw new Exception("Error getting data RelUnicanetMedio "  + ex.Message, ex);
    }
    finally
    {
    if(connection!=null)
    connection.Close();
    if(rd != null)
    rd.Close();
    }
    }
    return RelUnicanetMedioList;
    }

    /// <summary>
    /// Gets all RelUnicanetMedio by List<int>
    ///</summary>
    public override List<RelUnicanetMedioEntity> GetRelUnicanetMedio(List<int> lid)
    {
    List<RelUnicanetMedioEntity> RelUnicanetMedioList = new List<RelUnicanetMedioEntity>();
    using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    DataTable IdDT = BuildTableID(lid);
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioGetByIds", connection);
    AssingParameter(comandoSql, "@IdTable", IdDT);
    
    IDataReader rd = null;
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    rd = ExecuteReader(comandoSql);

    while (rd.Read())
    {
    RelUnicanetMedioList.Add(GetRelUnicanetMedioFromReader(rd));
    }
    }
    catch (Exception ex)
    {
    throw new Exception("Error getting data RelUnicanetMedio "  + ex.Message, ex);
    }
    finally
    {
    if(connection!=null)
    connection.Close();
    if(rd != null)
    rd.Close();
    }
    }
    return RelUnicanetMedioList;
    }

    /// <summary>
    /// Gets RelUnicanetMedio by
    ///</summary>
    public override RelUnicanetMedioEntity GetRelUnicanetMedioById()
    {
    using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioGetById", connection);
    RelUnicanetMedioEntity entity_RelUnicanetMedio = null;

    
    IDataReader rd = null;
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
    if (rd.Read())
    entity_RelUnicanetMedio = GetRelUnicanetMedioFromReader(rd);
    }
    catch (Exception ex)
    {
    throw new Exception("Error getting data RelUnicanetMedio "  + ex.Message, ex);
    }
    finally
    {
    if(connection!=null)
    connection.Close();
    if(rd != null)
    rd.Close();
    }
    return entity_RelUnicanetMedio;
    }

    }

    

    /// <summary>
    ///Get RelUnicanetMedio
    ///</summary>
    public override SoftvList<RelUnicanetMedioEntity> GetPagedList(int pageIndex, int pageSize)
    {
    SoftvList<RelUnicanetMedioEntity> entities = new SoftvList<RelUnicanetMedioEntity>();
    using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioGetPaged", connection);
    
    AssingParameter(comandoSql,"@pageIndex", pageIndex);
    AssingParameter(comandoSql,"@pageSize", pageSize);
    IDataReader rd = null;
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    rd = ExecuteReader(comandoSql);
    while (rd.Read())
    {
    entities.Add(GetRelUnicanetMedioFromReader(rd));
    }
    }
    catch (Exception ex)
    {
    throw new Exception("Error getting data RelUnicanetMedio " + ex.Message, ex);
    }
    finally
    {
    if (connection != null)
    connection.Close();
    if (rd != null)
    rd.Close();
    }
    entities.totalCount = GetRelUnicanetMedioCount();
    return entities ?? new SoftvList<RelUnicanetMedioEntity>();
    }
    }

    /// <summary>
    ///Get RelUnicanetMedio
    ///</summary>
    public override SoftvList<RelUnicanetMedioEntity> GetPagedList(int pageIndex, int pageSize,String xml)
    {
    SoftvList<RelUnicanetMedioEntity> entities = new SoftvList<RelUnicanetMedioEntity>();
    using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioGetPagedXml", connection);
    
    AssingParameter(comandoSql,"@pageSize", pageSize);
    AssingParameter(comandoSql,"@pageIndex", pageIndex);
    AssingParameter(comandoSql,"@xml", xml);
    IDataReader rd = null;
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    rd = ExecuteReader(comandoSql);
    while (rd.Read())
    {
    entities.Add(GetRelUnicanetMedioFromReader(rd));
    }
    }
    catch (Exception ex)
    {
    throw new Exception("Error getting data RelUnicanetMedio " + ex.Message, ex);
    }
    finally
    {
    if (connection != null)
    connection.Close();
    if (rd != null)
    rd.Close();
    }
    entities.totalCount = GetRelUnicanetMedioCount(xml);
    return entities ?? new SoftvList<RelUnicanetMedioEntity>();
    }
    }

    /// <summary>
    ///Get Count RelUnicanetMedio
    ///</summary>
    public int GetRelUnicanetMedioCount()
    {
    int result = 0;
    using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioGetCount", connection);
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    result = (int)ExecuteScalar(comandoSql);


    }
    catch (Exception ex)
    {
    throw new Exception("Error getting data RelUnicanetMedio " + ex.Message, ex);
    }
    finally
    {
    if (connection != null)
    connection.Close();
    }
    }
    return result;
    }


    /// <summary>
    ///Get Count RelUnicanetMedio
    ///</summary>
    public int GetRelUnicanetMedioCount(String xml)
    {
    int result = 0;
    using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelUnicanetMedio.ConnectionString))
    {
    
    SqlCommand comandoSql = CreateCommand("Softv_RelUnicanetMedioGetCountXml", connection);
    
    AssingParameter(comandoSql,"@xml", xml);
    try
    {
    if (connection.State == ConnectionState.Closed)
    connection.Open();
    result = (int)ExecuteScalar(comandoSql);


    }
    catch (Exception ex)
    {
    throw new Exception("Error getting data RelUnicanetMedio " + ex.Message, ex);
    }
    finally
    {
    if (connection != null)
    connection.Close();
    }
    }
    return result;
    }

    #region Customs Methods
    
    #endregion
    }
    }
  
﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Rel_Trabajos_NoCobroMensualData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Rel_Trabajos_NoCobroMensual Data Access Object
    /// File                    : Rel_Trabajos_NoCobroMensualDAO.cs
    /// Creation date           : 01/11/2017
    /// Creation time           : 05:46 p. m.
    ///</summary>
    public class Rel_Trabajos_NoCobroMensualData : Rel_Trabajos_NoCobroMensualProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Rel_Trabajos_NoCobroMensual"> Object Rel_Trabajos_NoCobroMensual added to List</param>
        public override int AddRel_Trabajos_NoCobroMensual(Rel_Trabajos_NoCobroMensualEntity entity_Rel_Trabajos_NoCobroMensual)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualAdd", connection);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_Rel_Trabajos_NoCobroMensual.Clv_Servicio);

                AssingParameter(comandoSql, "@Clv_Trabajo", entity_Rel_Trabajos_NoCobroMensual.Clv_Trabajo);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdRel_Trabajos_NoCobroMensual"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Rel_Trabajos_NoCobroMensual
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteRel_Trabajos_NoCobroMensual()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Rel_Trabajos_NoCobroMensual
        ///</summary>
        /// <param name="Rel_Trabajos_NoCobroMensual"> Objeto Rel_Trabajos_NoCobroMensual a editar </param>
        public override int EditRel_Trabajos_NoCobroMensual(Rel_Trabajos_NoCobroMensualEntity entity_Rel_Trabajos_NoCobroMensual)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualEdit", connection);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_Rel_Trabajos_NoCobroMensual.Clv_Servicio);

                AssingParameter(comandoSql, "@Clv_Trabajo", entity_Rel_Trabajos_NoCobroMensual.Clv_Trabajo);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Rel_Trabajos_NoCobroMensual
        ///</summary>
        public override List<Rel_Trabajos_NoCobroMensualEntity> GetRel_Trabajos_NoCobroMensual()
        {
            List<Rel_Trabajos_NoCobroMensualEntity> Rel_Trabajos_NoCobroMensualList = new List<Rel_Trabajos_NoCobroMensualEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Rel_Trabajos_NoCobroMensualList.Add(GetRel_Trabajos_NoCobroMensualFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Rel_Trabajos_NoCobroMensualList;
        }

        /// <summary>
        /// Gets all Rel_Trabajos_NoCobroMensual by List<int>
        ///</summary>
        public override List<Rel_Trabajos_NoCobroMensualEntity> GetRel_Trabajos_NoCobroMensual(List<int> lid)
        {
            List<Rel_Trabajos_NoCobroMensualEntity> Rel_Trabajos_NoCobroMensualList = new List<Rel_Trabajos_NoCobroMensualEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Rel_Trabajos_NoCobroMensualList.Add(GetRel_Trabajos_NoCobroMensualFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Rel_Trabajos_NoCobroMensualList;
        }

        /// <summary>
        /// Gets Rel_Trabajos_NoCobroMensual by
        ///</summary>
        public override Rel_Trabajos_NoCobroMensualEntity GetRel_Trabajos_NoCobroMensualById(int? Clv_Servicio)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Get_RelTrabajosNoCobroMensual", connection);
                Rel_Trabajos_NoCobroMensualEntity entity_Rel_Trabajos_NoCobroMensual = null;

                AssingParameter(comandoSql, "@Clv_Servicio", Clv_Servicio);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Rel_Trabajos_NoCobroMensual = GetRel_Trabajos_NoCobroMensualFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Rel_Trabajos_NoCobroMensual;
            }

        }



        /// <summary>
        ///Get Rel_Trabajos_NoCobroMensual
        ///</summary>
        public override SoftvList<Rel_Trabajos_NoCobroMensualEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<Rel_Trabajos_NoCobroMensualEntity> entities = new SoftvList<Rel_Trabajos_NoCobroMensualEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRel_Trabajos_NoCobroMensualFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRel_Trabajos_NoCobroMensualCount();
                return entities ?? new SoftvList<Rel_Trabajos_NoCobroMensualEntity>();
            }
        }

        /// <summary>
        ///Get Rel_Trabajos_NoCobroMensual
        ///</summary>
        public override SoftvList<Rel_Trabajos_NoCobroMensualEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<Rel_Trabajos_NoCobroMensualEntity> entities = new SoftvList<Rel_Trabajos_NoCobroMensualEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRel_Trabajos_NoCobroMensualFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRel_Trabajos_NoCobroMensualCount(xml);
                return entities ?? new SoftvList<Rel_Trabajos_NoCobroMensualEntity>();
            }
        }

        /// <summary>
        ///Get Count Rel_Trabajos_NoCobroMensual
        ///</summary>
        public int GetRel_Trabajos_NoCobroMensualCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Rel_Trabajos_NoCobroMensual
        ///</summary>
        public int GetRel_Trabajos_NoCobroMensualCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Rel_Trabajos_NoCobroMensual.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Rel_Trabajos_NoCobroMensualGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Rel_Trabajos_NoCobroMensual " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

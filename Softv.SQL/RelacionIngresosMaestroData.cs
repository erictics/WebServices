﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;
using OfficeOpenXml;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Xml.Linq;
//------------
using System.Xml;
using System.Configuration;
using System.Xml.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web;
using System.Net;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using System.Drawing;
using CrystalDecisions.CrystalReports.Engine;

namespace Softv.DAO
{
    public class RelacionIngresosMaestroData : RelacionIngresosMaestroProvider
    {
        //public override List<RelacionIngresosMaestroEntity> GetRelacionIngresosMaestro(List<int> Distribuidores, string FechaInicial, string FechaFinal, int Dolares)
        //{
        //    List<RelacionIngresosMaestroEntity> GetRelacionIngresosMaestro = new List<RelacionIngresosMaestroEntity>();

        //    XmlSerializer xmlSerializer = new XmlSerializer(Distribuidores.GetType());

        //    using (StringWriter textWriter = new StringWriter())
        //    {
        //        xmlSerializer.Serialize(textWriter, Distribuidores);

        //        DBFuncion db1 = new DBFuncion();
        //        db1.agregarParametro("@DistribuidoresXml", SqlDbType.Xml, textWriter.ToString());
        //        db1.agregarParametro("@Fecha_Ini", SqlDbType.VarChar, FechaInicial);
        //        db1.agregarParametro("@Fecha_Fin", SqlDbType.VarChar, FechaFinal);
        //        db1.agregarParametro("@Dolares", SqlDbType.Int, Dolares);
        //        SqlDataReader reader = db1.consultaReader("RelacionIngresosMaestro");
        //        GetRelacionIngresosMaestro = db1.MapDataToEntityCollection<RelacionIngresosMaestroEntity>(reader).ToList();
        //    }

        //    return GetRelacionIngresosMaestro;
        //}

        public override string GetRelacionIngresosMaestro(List<int> Distribuidores, string FechaInicial, string FechaFinal, int Dolares)
        {
            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "/Reportes/";
            string archivo = Guid.NewGuid() + ".pdf";
            string ruta = folder + archivo;

            XmlSerializer xmlSerializer = new XmlSerializer(Distribuidores.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, Distribuidores);

                DBFuncion db1 = new DBFuncion();
                db1.agregarParametro("@DistribuidoresXml", SqlDbType.Xml, textWriter.ToString());
                db1.agregarParametro("@Fecha_Ini", SqlDbType.VarChar, FechaInicial.Replace("/", ""));
                db1.agregarParametro("@Fecha_Fin", SqlDbType.VarChar, FechaFinal.Replace("/", ""));
                db1.agregarParametro("@Dolares", SqlDbType.Int, Dolares);
                DataSet ds = db1.consultaDS("RelacionIngresosMaestro");
                ds.Tables[0].TableName = "Desgloce_de_Prueba";
                ds.Tables[1].TableName = "TipServ";

                //Nos traemos la ruta de reportes
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@Clv_Sistema", SqlDbType.Int, 2);
                db2.agregarParametro("@Ruta", SqlDbType.VarChar, ParameterDirection.Output, 800);
                db2.consultaOutput("DameRuta");

                //Lo vamos a crear en crystal y luego lo exportamos a pdf
                ReportDocument reporte = new ReportDocument();
                reporte.Load(db2.diccionarioOutput["@Ruta"].ToString() + @"\DesgloceporConceptos.rpt");
                reporte.SetDataSource(ds);
                reporte.DataDefinition.FormulaFields["Empresa"].Text = "'Facturación Corporativa'";
                reporte.DataDefinition.FormulaFields["Titulo"].Text = "'Relación de Ingresos por Concepto'";
                reporte.DataDefinition.FormulaFields["Fecha"].Text = "'De la Fecha " + FechaInicial + " a la Fecha " + FechaFinal + "'";
                reporte.DataDefinition.FormulaFields["Subtitulo"].Text = "''";
                if (Dolares == 1)
                {
                    reporte.DataDefinition.FormulaFields["Compania"].Text = "'Tipo - Dólares'";
                }
                else
                {
                    reporte.DataDefinition.FormulaFields["Compania"].Text = "'Tipo - Pesos'";
                }
                reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, ruta);
            }

            return archivo;
        }
        
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;
using System.Collections;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.RepGral_StatusVentasData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : RepGral_StatusVentas Data Access Object
    /// File                    : RepGral_StatusVentasDAO.cs
    /// Creation date           : 28/07/2017
    /// Creation time           : 10:27 a. m.
    ///</summary>
    public class RepGral_StatusVentasData : RepGral_StatusVentasProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="RepGral_StatusVentas"> Object RepGral_StatusVentas added to List</param>
        public override int AddRepGral_StatusVentas(RepGral_StatusVentasEntity entity_RepGral_StatusVentas)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasAdd", connection);

                AssingParameter(comandoSql, "@Serie", entity_RepGral_StatusVentas.Serie);

                AssingParameter(comandoSql, "@Folio", entity_RepGral_StatusVentas.Folio);

                AssingParameter(comandoSql, "@Contrato", entity_RepGral_StatusVentas.Contrato);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_RepGral_StatusVentas.Clv_Servicio);

                AssingParameter(comandoSql, "@Descripcion", entity_RepGral_StatusVentas.Descripcion);

                AssingParameter(comandoSql, "@STATUS", entity_RepGral_StatusVentas.STATUS);

                AssingParameter(comandoSql, "@Clv_Grupo", entity_RepGral_StatusVentas.Clv_Grupo);

                AssingParameter(comandoSql, "@Grupo", entity_RepGral_StatusVentas.Grupo);

                AssingParameter(comandoSql, "@Clave", entity_RepGral_StatusVentas.Clave);

                AssingParameter(comandoSql, "@Nombre", entity_RepGral_StatusVentas.Nombre);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdRepGral_StatusVentas"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a RepGral_StatusVentas
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteRepGral_StatusVentas()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a RepGral_StatusVentas
        ///</summary>
        /// <param name="RepGral_StatusVentas"> Objeto RepGral_StatusVentas a editar </param>
        public override int EditRepGral_StatusVentas(RepGral_StatusVentasEntity entity_RepGral_StatusVentas)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasEdit", connection);

                AssingParameter(comandoSql, "@Serie", entity_RepGral_StatusVentas.Serie);

                AssingParameter(comandoSql, "@Folio", entity_RepGral_StatusVentas.Folio);

                AssingParameter(comandoSql, "@Contrato", entity_RepGral_StatusVentas.Contrato);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_RepGral_StatusVentas.Clv_Servicio);

                AssingParameter(comandoSql, "@Descripcion", entity_RepGral_StatusVentas.Descripcion);

                AssingParameter(comandoSql, "@STATUS", entity_RepGral_StatusVentas.STATUS);

                AssingParameter(comandoSql, "@Clv_Grupo", entity_RepGral_StatusVentas.Clv_Grupo);

                AssingParameter(comandoSql, "@Grupo", entity_RepGral_StatusVentas.Grupo);

                AssingParameter(comandoSql, "@Clave", entity_RepGral_StatusVentas.Clave);

                AssingParameter(comandoSql, "@Nombre", entity_RepGral_StatusVentas.Nombre);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all RepGral_StatusVentas
        ///</summary>
        public override List<RepGral_StatusVentasEntity> GetRepGral_StatusVentas()
        {
            List<RepGral_StatusVentasEntity> RepGral_StatusVentasList = new List<RepGral_StatusVentasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RepGral_StatusVentasList.Add(GetRepGral_StatusVentasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RepGral_StatusVentasList;
        }






        public override List<RepGral_StatusVentasEntity> GetRepGral_StatusVentasXmlList(String xml)
        {

            List<RepGral_StatusVentasEntity> List = new List<RepGral_StatusVentasEntity>();
            List<Plaza_ReportesVentasEntity> Plaza = new List<Plaza_ReportesVentasEntity>();
            List<Muestra_PlazasPorUsuarioEntity> Dist = new List<Muestra_PlazasPorUsuarioEntity>();
            ArrayList list = new ArrayList();

            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("StatusDeVentas_Web", connection);
                //comandoSql.CommandTimeout = 820;
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Dist.Add(GetRepGral_StatusVentasDisFromReader(rd));
                    }

                    if (rd.NextResult())
                    {
                        while (rd.Read())
                        {
                            Plaza.Add(GetRepGral_StatusVentasPlazaFromReader(rd));
                        }
                    }

                    if (rd.NextResult())
                    {
                        while (rd.Read())
                        {
                            List.Add(GetRepGral_StatusVentasFromReader(rd));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Plaza_ReportesVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return List;
        }










        /// <summary>
        /// Gets all RepGral_StatusVentas by List<int>
        ///</summary>
        public override List<RepGral_StatusVentasEntity> GetRepGral_StatusVentas(List<int> lid)
        {
            List<RepGral_StatusVentasEntity> RepGral_StatusVentasList = new List<RepGral_StatusVentasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        RepGral_StatusVentasList.Add(GetRepGral_StatusVentasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return RepGral_StatusVentasList;
        }

        /// <summary>
        /// Gets RepGral_StatusVentas by
        ///</summary>
        public override RepGral_StatusVentasEntity GetRepGral_StatusVentasById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasGetById", connection);
                RepGral_StatusVentasEntity entity_RepGral_StatusVentas = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_RepGral_StatusVentas = GetRepGral_StatusVentasFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_RepGral_StatusVentas;
            }

        }



        /// <summary>
        ///Get RepGral_StatusVentas
        ///</summary>
        public override SoftvList<RepGral_StatusVentasEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<RepGral_StatusVentasEntity> entities = new SoftvList<RepGral_StatusVentasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRepGral_StatusVentasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRepGral_StatusVentasCount();
                return entities ?? new SoftvList<RepGral_StatusVentasEntity>();
            }
        }

        /// <summary>
        ///Get RepGral_StatusVentas
        ///</summary>
        public override SoftvList<RepGral_StatusVentasEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<RepGral_StatusVentasEntity> entities = new SoftvList<RepGral_StatusVentasEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetRepGral_StatusVentasFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetRepGral_StatusVentasCount(xml);
                return entities ?? new SoftvList<RepGral_StatusVentasEntity>();
            }
        }

        /// <summary>
        ///Get Count RepGral_StatusVentas
        ///</summary>
        public int GetRepGral_StatusVentasCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count RepGral_StatusVentas
        ///</summary>
        public int GetRepGral_StatusVentasCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RepGral_StatusVentas.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_RepGral_StatusVentasGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data RepGral_StatusVentas " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Servicios_NewData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Servicios_New Data Access Object
    /// File                    : Servicios_NewDAO.cs
    /// Creation date           : 18/09/2017
    /// Creation time           : 10:48 a. m.
    ///</summary>
    public class Servicios_NewData : Servicios_NewProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Servicios_New"> Object Servicios_New added to List</param>
        public override int AddServicios_New(Servicios_NewEntity entity_Servicios_New)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("NUESERVICIOS", connection);
                
                AssingParameter(comandoSql, "@Clv_TipSer", entity_Servicios_New.Clv_TipSer);
                AssingParameter(comandoSql, "@Descripcion", entity_Servicios_New.Descripcion);
                AssingParameter(comandoSql, "@Clv_Txt", entity_Servicios_New.Clv_Txt);
                AssingParameter(comandoSql, "@AplicanCom", entity_Servicios_New.AplicanCom);
                AssingParameter(comandoSql, "@Sale_en_Cartera", entity_Servicios_New.Sale_en_Cartera);
                AssingParameter(comandoSql, "@Precio", entity_Servicios_New.Precio);
                AssingParameter(comandoSql, "@Genera_Orden", entity_Servicios_New.Genera_Orden);
                AssingParameter(comandoSql, "@Es_Principal", entity_Servicios_New.Es_Principal);
                AssingParameter(comandoSql, "@Clv_Servicio", null, pd: ParameterDirection.Output, IsKey: true);
                AssingParameter(comandoSql, "@idcompania", entity_Servicios_New.idcompania);
                AssingParameter(comandoSql, "@EsToken", entity_Servicios_New.EsToken);
                AssingParameter(comandoSql, "@Gb", entity_Servicios_New.Gb);
                AssingParameter(comandoSql, "@dolares", entity_Servicios_New.dolares);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@Clv_Servicio"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Servicios_New
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteServicios_New(int? Clv_Servicio)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("BORSERVICIOS", connection);

                AssingParameter(comandoSql, "@clv_servicio", Clv_Servicio);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Servicios_New
        ///</summary>
        /// <param name="Servicios_New"> Objeto Servicios_New a editar </param>
        public override int EditServicios_New(Servicios_NewEntity entity_Servicios_New)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("MODSERVICIOS", connection);
                AssingParameter(comandoSql, "@Clv_Servicio", entity_Servicios_New.Clv_Servicio);
                AssingParameter(comandoSql, "@Clv_TipSer", entity_Servicios_New.Clv_TipSer);
                AssingParameter(comandoSql, "@Descripcion", entity_Servicios_New.Descripcion);
                AssingParameter(comandoSql, "@Clv_Txt", entity_Servicios_New.Clv_Txt);
                AssingParameter(comandoSql, "@AplicanCom", entity_Servicios_New.AplicanCom);
                AssingParameter(comandoSql, "@Sale_en_Cartera", entity_Servicios_New.Sale_en_Cartera);
                AssingParameter(comandoSql, "@Precio", entity_Servicios_New.Precio);
                AssingParameter(comandoSql, "@Genera_Orden", entity_Servicios_New.Genera_Orden);
                AssingParameter(comandoSql, "@Es_Principal", entity_Servicios_New.Es_Principal);
                AssingParameter(comandoSql, "@Concepto", entity_Servicios_New.Concepto);
                AssingParameter(comandoSql, "@EsToken", entity_Servicios_New.EsToken);
                AssingParameter(comandoSql, "@Gb", entity_Servicios_New.Gb);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Servicios_New
        ///</summary>
        public override List<Servicios_NewEntity> GetServicios_New(int? Clv_TipSer, int? Clv_Servicio, String Descripcion, String Clv_Txt, int? Op, int? idcompania)
        {
            List<Servicios_NewEntity> Servicios_NewList = new List<Servicios_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("BUSCASERVICIOS", connection);
                IDataReader rd = null;

                AssingParameter(comandoSql, "@CLV_TIPSER", Clv_TipSer);
                AssingParameter(comandoSql, "@CLV_SERVICIO", Clv_Servicio);
                AssingParameter(comandoSql, "@DESCRIPCION", Descripcion);
                AssingParameter(comandoSql, "@CLV_TXT", Clv_Txt);
                AssingParameter(comandoSql, "@OP", Op);
                AssingParameter(comandoSql, "@idcompania", idcompania);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Servicios_NewList.Add(GetServicios_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Servicios_NewList;
        }

        /// <summary>
        /// Gets all Servicios_New by List<int>
        ///</summary>
        public override List<Servicios_NewEntity> GetServicios_New(List<int> lid)
        {
            List<Servicios_NewEntity> Servicios_NewList = new List<Servicios_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_Servicios_NewGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Servicios_NewList.Add(GetServicios_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Servicios_NewList;
        }

        /// <summary>
        /// Gets Servicios_New by
        ///</summary>
        public override Servicios_NewEntity GetServicios_NewById(int? Clv_Servicio)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("CONSERVICIOS", connection);
                Servicios_NewEntity entity_Servicios_New = null;

                AssingParameter(comandoSql, "@clv_servicio", Clv_Servicio);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Servicios_New = GetServiciosBY_NewFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Servicios_New;
            }

        }



        /// <summary>
        ///Get Servicios_New
        ///</summary>
        public override SoftvList<Servicios_NewEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<Servicios_NewEntity> entities = new SoftvList<Servicios_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Servicios_NewGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetServicios_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetServicios_NewCount();
                return entities ?? new SoftvList<Servicios_NewEntity>();
            }
        }

        /// <summary>
        ///Get Servicios_New
        ///</summary>
        public override SoftvList<Servicios_NewEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<Servicios_NewEntity> entities = new SoftvList<Servicios_NewEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Servicios_NewGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetServicios_NewFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetServicios_NewCount(xml);
                return entities ?? new SoftvList<Servicios_NewEntity>();
            }
        }

        /// <summary>
        ///Get Count Servicios_New
        ///</summary>
        public int GetServicios_NewCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Servicios_NewGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Servicios_New
        ///</summary>
        public int GetServicios_NewCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Servicios_New.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Servicios_NewGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Servicios_New " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.Softv_ExistenciasTecnicoData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : Softv_ExistenciasTecnico Data Access Object
    /// File                    : Softv_ExistenciasTecnicoDAO.cs
    /// Creation date           : 21/06/2017
    /// Creation time           : 04:51 p. m.
    ///</summary>
    public class Softv_ExistenciasTecnicoData : Softv_ExistenciasTecnicoProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="Softv_ExistenciasTecnico"> Object Softv_ExistenciasTecnico added to List</param>
        public override int AddSoftv_ExistenciasTecnico(Softv_ExistenciasTecnicoEntity entity_Softv_ExistenciasTecnico)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoAdd", connection);

                AssingParameter(comandoSql, "@ClvArticulo", entity_Softv_ExistenciasTecnico.ClvArticulo);

                AssingParameter(comandoSql, "@ClvTecnico", entity_Softv_ExistenciasTecnico.ClvTecnico);

                AssingParameter(comandoSql, "@IdAlmacen", entity_Softv_ExistenciasTecnico.IdAlmacen);

                AssingParameter(comandoSql, "@Existe", entity_Softv_ExistenciasTecnico.Existe);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdSoftv_ExistenciasTecnico"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a Softv_ExistenciasTecnico
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteSoftv_ExistenciasTecnico()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a Softv_ExistenciasTecnico
        ///</summary>
        /// <param name="Softv_ExistenciasTecnico"> Objeto Softv_ExistenciasTecnico a editar </param>
        public override int EditSoftv_ExistenciasTecnico(Softv_ExistenciasTecnicoEntity entity_Softv_ExistenciasTecnico)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoEdit", connection);

                AssingParameter(comandoSql, "@ClvArticulo", entity_Softv_ExistenciasTecnico.ClvArticulo);

                AssingParameter(comandoSql, "@ClvTecnico", entity_Softv_ExistenciasTecnico.ClvTecnico);

                AssingParameter(comandoSql, "@IdAlmacen", entity_Softv_ExistenciasTecnico.IdAlmacen);

                AssingParameter(comandoSql, "@Existe", entity_Softv_ExistenciasTecnico.Existe);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all Softv_ExistenciasTecnico
        ///</summary>
        public override List<Softv_ExistenciasTecnicoEntity> GetSoftv_ExistenciasTecnico()
        {
            List<Softv_ExistenciasTecnicoEntity> Softv_ExistenciasTecnicoList = new List<Softv_ExistenciasTecnicoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Softv_ExistenciasTecnicoList.Add(GetSoftv_ExistenciasTecnicoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Softv_ExistenciasTecnicoList;
        }

        /// <summary>
        /// Gets all Softv_ExistenciasTecnico by List<int>
        ///</summary>
        public override List<Softv_ExistenciasTecnicoEntity> GetSoftv_ExistenciasTecnico(List<int> lid)
        {
            List<Softv_ExistenciasTecnicoEntity> Softv_ExistenciasTecnicoList = new List<Softv_ExistenciasTecnicoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        Softv_ExistenciasTecnicoList.Add(GetSoftv_ExistenciasTecnicoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return Softv_ExistenciasTecnicoList;
        }

        /// <summary>
        /// Gets Softv_ExistenciasTecnico by
        ///</summary>
        public override Softv_ExistenciasTecnicoEntity GetSoftv_ExistenciasTecnicoById(long? ClvArticulo, long? ClvTecnico, int? IdAlmacen)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softvweb_ExistenciasTecnico", connection);
                Softv_ExistenciasTecnicoEntity entity_Softv_ExistenciasTecnico = null;

                AssingParameter(comandoSql, "@ClvArticulo", ClvArticulo);
                AssingParameter(comandoSql, "@ClvTecnico", ClvTecnico);
                AssingParameter(comandoSql, "@IdAlmacen", IdAlmacen);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_Softv_ExistenciasTecnico = GetSoftv_ExistenciasTecnicoFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_Softv_ExistenciasTecnico;
            }

        }



        /// <summary>
        ///Get Softv_ExistenciasTecnico
        ///</summary>
        public override SoftvList<Softv_ExistenciasTecnicoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<Softv_ExistenciasTecnicoEntity> entities = new SoftvList<Softv_ExistenciasTecnicoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetSoftv_ExistenciasTecnicoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetSoftv_ExistenciasTecnicoCount();
                return entities ?? new SoftvList<Softv_ExistenciasTecnicoEntity>();
            }
        }

        /// <summary>
        ///Get Softv_ExistenciasTecnico
        ///</summary>
        public override SoftvList<Softv_ExistenciasTecnicoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<Softv_ExistenciasTecnicoEntity> entities = new SoftvList<Softv_ExistenciasTecnicoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetSoftv_ExistenciasTecnicoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetSoftv_ExistenciasTecnicoCount(xml);
                return entities ?? new SoftvList<Softv_ExistenciasTecnicoEntity>();
            }
        }

        /// <summary>
        ///Get Count Softv_ExistenciasTecnico
        ///</summary>
        public int GetSoftv_ExistenciasTecnicoCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count Softv_ExistenciasTecnico
        ///</summary>
        public int GetSoftv_ExistenciasTecnicoCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Softv_ExistenciasTecnico.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_Softv_ExistenciasTecnicoGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data Softv_ExistenciasTecnico " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

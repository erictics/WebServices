﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.SumaTotalDetalleData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : SumaTotalDetalle Data Access Object
    /// File                    : SumaTotalDetalleDAO.cs
    /// Creation date           : 27/10/2016
    /// Creation time           : 01:09 p. m.
    ///</summary>
    public class SumaTotalDetalleData : SumaTotalDetalleProvider
    {

        /// <summary>
        /// Gets SumaTotalDetalle by
        ///</summary>
        public override SumaTotalDetalleEntity GetSumaTotalDetalleById(long? IdSession)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.SumaTotalDetalle.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("softvweb_DAMETOTALSumaDetalle", connection);
                SumaTotalDetalleEntity entity_SumaTotalDetalle = null;

                AssingParameter(comandoSql, "@IdSession", IdSession);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_SumaTotalDetalle = GetSumaTotalDetalleFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data SumaTotalDetalle " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_SumaTotalDetalle;
            }

        }



        #region Customs Methods

        #endregion
    }
}

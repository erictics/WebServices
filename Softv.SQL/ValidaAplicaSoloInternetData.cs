﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ValidaAplicaSoloInternetData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidaAplicaSoloInternet Data Access Object
    /// File                    : ValidaAplicaSoloInternetDAO.cs
    /// Creation date           : 18/09/2017
    /// Creation time           : 12:06 p. m.
    ///</summary>
    public class ValidaAplicaSoloInternetData : ValidaAplicaSoloInternetProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="ValidaAplicaSoloInternet"> Object ValidaAplicaSoloInternet added to List</param>
        public override int AddValidaAplicaSoloInternet(ValidaAplicaSoloInternetEntity entity_ValidaAplicaSoloInternet)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ValidaAplicaSoloInternet", connection);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_ValidaAplicaSoloInternet.Clv_Servicio);

                AssingParameter(comandoSql, "@Res", null, pd: ParameterDirection.Output, IsKey: true);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@Res"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a ValidaAplicaSoloInternet
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteValidaAplicaSoloInternet()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a ValidaAplicaSoloInternet
        ///</summary>
        /// <param name="ValidaAplicaSoloInternet"> Objeto ValidaAplicaSoloInternet a editar </param>
        public override int EditValidaAplicaSoloInternet(ValidaAplicaSoloInternetEntity entity_ValidaAplicaSoloInternet)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetEdit", connection);

                AssingParameter(comandoSql, "@Clv_Servicio", entity_ValidaAplicaSoloInternet.Clv_Servicio);

                AssingParameter(comandoSql, "@Res", entity_ValidaAplicaSoloInternet.Res);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all ValidaAplicaSoloInternet
        ///</summary>
        public override List<ValidaAplicaSoloInternetEntity> GetValidaAplicaSoloInternet()
        {
            List<ValidaAplicaSoloInternetEntity> ValidaAplicaSoloInternetList = new List<ValidaAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ValidaAplicaSoloInternetList.Add(GetValidaAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ValidaAplicaSoloInternetList;
        }

        /// <summary>
        /// Gets all ValidaAplicaSoloInternet by List<int>
        ///</summary>
        public override List<ValidaAplicaSoloInternetEntity> GetValidaAplicaSoloInternet(List<int> lid)
        {
            List<ValidaAplicaSoloInternetEntity> ValidaAplicaSoloInternetList = new List<ValidaAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ValidaAplicaSoloInternetList.Add(GetValidaAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ValidaAplicaSoloInternetList;
        }

        /// <summary>
        /// Gets ValidaAplicaSoloInternet by
        ///</summary>
        public override ValidaAplicaSoloInternetEntity GetValidaAplicaSoloInternetById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetGetById", connection);
                ValidaAplicaSoloInternetEntity entity_ValidaAplicaSoloInternet = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_ValidaAplicaSoloInternet = GetValidaAplicaSoloInternetFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_ValidaAplicaSoloInternet;
            }

        }



        /// <summary>
        ///Get ValidaAplicaSoloInternet
        ///</summary>
        public override SoftvList<ValidaAplicaSoloInternetEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ValidaAplicaSoloInternetEntity> entities = new SoftvList<ValidaAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetValidaAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetValidaAplicaSoloInternetCount();
                return entities ?? new SoftvList<ValidaAplicaSoloInternetEntity>();
            }
        }

        /// <summary>
        ///Get ValidaAplicaSoloInternet
        ///</summary>
        public override SoftvList<ValidaAplicaSoloInternetEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ValidaAplicaSoloInternetEntity> entities = new SoftvList<ValidaAplicaSoloInternetEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetValidaAplicaSoloInternetFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetValidaAplicaSoloInternetCount(xml);
                return entities ?? new SoftvList<ValidaAplicaSoloInternetEntity>();
            }
        }

        /// <summary>
        ///Get Count ValidaAplicaSoloInternet
        ///</summary>
        public int GetValidaAplicaSoloInternetCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count ValidaAplicaSoloInternet
        ///</summary>
        public int GetValidaAplicaSoloInternetCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaAplicaSoloInternet.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaAplicaSoloInternetGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaAplicaSoloInternet " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

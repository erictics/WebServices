﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ValidaOrdSerManualesData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidaOrdSerManuales Data Access Object
    /// File                    : ValidaOrdSerManualesDAO.cs
    /// Creation date           : 13/06/2017
    /// Creation time           : 11:56 a. m.
    ///</summary>
    public class ValidaOrdSerManualesData : ValidaOrdSerManualesProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="ValidaOrdSerManuales"> Object ValidaOrdSerManuales added to List</param>
        public override int AddValidaOrdSerManuales(ValidaOrdSerManualesEntity entity_ValidaOrdSerManuales)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesAdd", connection);

                AssingParameter(comandoSql, "@ClvOrdSer", entity_ValidaOrdSerManuales.ClvOrdSer);

                AssingParameter(comandoSql, "@Error", entity_ValidaOrdSerManuales.Error);

                AssingParameter(comandoSql, "@Msg", entity_ValidaOrdSerManuales.Msg);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdValidaOrdSerManuales"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a ValidaOrdSerManuales
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteValidaOrdSerManuales()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a ValidaOrdSerManuales
        ///</summary>
        /// <param name="ValidaOrdSerManuales"> Objeto ValidaOrdSerManuales a editar </param>
        public override int EditValidaOrdSerManuales(ValidaOrdSerManualesEntity entity_ValidaOrdSerManuales)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesEdit", connection);

                AssingParameter(comandoSql, "@ClvOrdSer", entity_ValidaOrdSerManuales.ClvOrdSer);

                AssingParameter(comandoSql, "@Error", entity_ValidaOrdSerManuales.Error);

                AssingParameter(comandoSql, "@Msg", entity_ValidaOrdSerManuales.Msg);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all ValidaOrdSerManuales
        ///</summary>
        public override List<ValidaOrdSerManualesEntity> GetValidaOrdSerManuales()
        {
            List<ValidaOrdSerManualesEntity> ValidaOrdSerManualesList = new List<ValidaOrdSerManualesEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ValidaOrdSerManualesList.Add(GetValidaOrdSerManualesFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ValidaOrdSerManualesList;
        }

        /// <summary>
        /// Gets all ValidaOrdSerManuales by List<int>
        ///</summary>
        public override List<ValidaOrdSerManualesEntity> GetValidaOrdSerManuales(List<int> lid)
        {
            List<ValidaOrdSerManualesEntity> ValidaOrdSerManualesList = new List<ValidaOrdSerManualesEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ValidaOrdSerManualesList.Add(GetValidaOrdSerManualesFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ValidaOrdSerManualesList;
        }

        /// <summary>
        /// Gets ValidaOrdSerManuales by
        ///</summary>
        public override ValidaOrdSerManualesEntity GetValidaOrdSerManualesById(long? ClvOrdSer)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("ValidaOrdSerManuales", connection);
                ValidaOrdSerManualesEntity entity_ValidaOrdSerManuales = null;

                AssingParameter(comandoSql, "@ClvOrdSer", ClvOrdSer);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_ValidaOrdSerManuales = GetValidaOrdSerManualesFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_ValidaOrdSerManuales;
            }

        }



        /// <summary>
        ///Get ValidaOrdSerManuales
        ///</summary>
        public override SoftvList<ValidaOrdSerManualesEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ValidaOrdSerManualesEntity> entities = new SoftvList<ValidaOrdSerManualesEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetValidaOrdSerManualesFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetValidaOrdSerManualesCount();
                return entities ?? new SoftvList<ValidaOrdSerManualesEntity>();
            }
        }

        /// <summary>
        ///Get ValidaOrdSerManuales
        ///</summary>
        public override SoftvList<ValidaOrdSerManualesEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ValidaOrdSerManualesEntity> entities = new SoftvList<ValidaOrdSerManualesEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetValidaOrdSerManualesFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetValidaOrdSerManualesCount(xml);
                return entities ?? new SoftvList<ValidaOrdSerManualesEntity>();
            }
        }

        /// <summary>
        ///Get Count ValidaOrdSerManuales
        ///</summary>
        public int GetValidaOrdSerManualesCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count ValidaOrdSerManuales
        ///</summary>
        public int GetValidaOrdSerManualesCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidaOrdSerManuales.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidaOrdSerManualesGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidaOrdSerManuales " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

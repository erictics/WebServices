﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.ValidarNuevoData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : ValidarNuevo Data Access Object
    /// File                    : ValidarNuevoDAO.cs
    /// Creation date           : 06/07/2017
    /// Creation time           : 12:26 p. m.
    ///</summary>
    public class ValidarNuevoData : ValidarNuevoProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="ValidarNuevo"> Object ValidarNuevo added to List</param>
        public override int AddValidarNuevo(ValidarNuevoEntity entity_ValidarNuevo)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoAdd", connection);

                AssingParameter(comandoSql, "@ClvOrden", entity_ValidarNuevo.ClvOrden);

                AssingParameter(comandoSql, "@Op", entity_ValidarNuevo.Op);

                AssingParameter(comandoSql, "@Valor", entity_ValidarNuevo.Valor);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdValidarNuevo"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a ValidarNuevo
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteValidarNuevo()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a ValidarNuevo
        ///</summary>
        /// <param name="ValidarNuevo"> Objeto ValidarNuevo a editar </param>
        public override int EditValidarNuevo(ValidarNuevoEntity entity_ValidarNuevo)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoEdit", connection);

                AssingParameter(comandoSql, "@ClvOrden", entity_ValidarNuevo.ClvOrden);

                AssingParameter(comandoSql, "@Op", entity_ValidarNuevo.Op);

                AssingParameter(comandoSql, "@Valor", entity_ValidarNuevo.Valor);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all ValidarNuevo
        ///</summary>
        public override List<ValidarNuevoEntity> GetValidarNuevo()
        {
            List<ValidarNuevoEntity> ValidarNuevoList = new List<ValidarNuevoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ValidarNuevoList.Add(GetValidarNuevoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ValidarNuevoList;
        }

        /// <summary>
        /// Gets all ValidarNuevo by List<int>
        ///</summary>
        public override List<ValidarNuevoEntity> GetValidarNuevo(List<int> lid)
        {
            List<ValidarNuevoEntity> ValidarNuevoList = new List<ValidarNuevoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        ValidarNuevoList.Add(GetValidarNuevoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return ValidarNuevoList;
        }

        /// <summary>
        /// Gets ValidarNuevo by
        ///</summary>
        public override ValidarNuevoEntity GetValidarNuevoById(long? ClvOrden, int? Op)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("softvweb_ValidarNuevo", connection);
                ValidarNuevoEntity entity_ValidarNuevo = null;

                AssingParameter(comandoSql, "@ClvOrden", ClvOrden);
                AssingParameter(comandoSql, "@Op", Op);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_ValidarNuevo = GetValidarNuevoFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_ValidarNuevo;
            }

        }



        /// <summary>
        ///Get ValidarNuevo
        ///</summary>
        public override SoftvList<ValidarNuevoEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<ValidarNuevoEntity> entities = new SoftvList<ValidarNuevoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetValidarNuevoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetValidarNuevoCount();
                return entities ?? new SoftvList<ValidarNuevoEntity>();
            }
        }

        /// <summary>
        ///Get ValidarNuevo
        ///</summary>
        public override SoftvList<ValidarNuevoEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<ValidarNuevoEntity> entities = new SoftvList<ValidarNuevoEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetValidarNuevoFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetValidarNuevoCount(xml);
                return entities ?? new SoftvList<ValidarNuevoEntity>();
            }
        }

        /// <summary>
        ///Get Count ValidarNuevo
        ///</summary>
        public int GetValidarNuevoCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count ValidarNuevo
        ///</summary>
        public int GetValidarNuevoCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.ValidarNuevo.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_ValidarNuevoGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data ValidarNuevo " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.spConsultaTurnosData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : spConsultaTurnos Data Access Object
    /// File                    : spConsultaTurnosDAO.cs
    /// Creation date           : 24/01/2017
    /// Creation time           : 06:37 p. m.
    ///</summary>
    public class spConsultaTurnosData : spConsultaTurnosProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="spConsultaTurnos"> Object spConsultaTurnos added to List</param>
        public override int AddspConsultaTurnos(spConsultaTurnosEntity entity_spConsultaTurnos)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosAdd", connection);

                AssingParameter(comandoSql, "@ID", entity_spConsultaTurnos.ID);

                AssingParameter(comandoSql, "@TURNO", entity_spConsultaTurnos.TURNO);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IdspConsultaTurnos"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a spConsultaTurnos
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeletespConsultaTurnos()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a spConsultaTurnos
        ///</summary>
        /// <param name="spConsultaTurnos"> Objeto spConsultaTurnos a editar </param>
        public override int EditspConsultaTurnos(spConsultaTurnosEntity entity_spConsultaTurnos)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosEdit", connection);

                AssingParameter(comandoSql, "@ID", entity_spConsultaTurnos.ID);

                AssingParameter(comandoSql, "@TURNO", entity_spConsultaTurnos.TURNO);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all spConsultaTurnos
        ///</summary>
        public override List<spConsultaTurnosEntity> GetspConsultaTurnos()
        {
            List<spConsultaTurnosEntity> spConsultaTurnosList = new List<spConsultaTurnosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("spConsultaTurnos", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        spConsultaTurnosList.Add(GetspConsultaTurnosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return spConsultaTurnosList;
        }

        /// <summary>
        /// Gets all spConsultaTurnos by List<int>
        ///</summary>
        public override List<spConsultaTurnosEntity> GetspConsultaTurnos(List<int> lid)
        {
            List<spConsultaTurnosEntity> spConsultaTurnosList = new List<spConsultaTurnosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        spConsultaTurnosList.Add(GetspConsultaTurnosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return spConsultaTurnosList;
        }

        /// <summary>
        /// Gets spConsultaTurnos by
        ///</summary>
        public override spConsultaTurnosEntity GetspConsultaTurnosById()
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosGetById", connection);
                spConsultaTurnosEntity entity_spConsultaTurnos = null;


                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_spConsultaTurnos = GetspConsultaTurnosFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_spConsultaTurnos;
            }

        }



        /// <summary>
        ///Get spConsultaTurnos
        ///</summary>
        public override SoftvList<spConsultaTurnosEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<spConsultaTurnosEntity> entities = new SoftvList<spConsultaTurnosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetspConsultaTurnosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetspConsultaTurnosCount();
                return entities ?? new SoftvList<spConsultaTurnosEntity>();
            }
        }

        /// <summary>
        ///Get spConsultaTurnos
        ///</summary>
        public override SoftvList<spConsultaTurnosEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<spConsultaTurnosEntity> entities = new SoftvList<spConsultaTurnosEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetspConsultaTurnosFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetspConsultaTurnosCount(xml);
                return entities ?? new SoftvList<spConsultaTurnosEntity>();
            }
        }

        /// <summary>
        ///Get Count spConsultaTurnos
        ///</summary>
        public int GetspConsultaTurnosCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count spConsultaTurnos
        ///</summary>
        public int GetspConsultaTurnosCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.spConsultaTurnos.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_spConsultaTurnosGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data spConsultaTurnos " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

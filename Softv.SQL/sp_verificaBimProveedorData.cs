﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.sp_verificaBimProveedorData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : sp_verificaBimProveedor Data Access Object
    /// File                    : sp_verificaBimProveedorDAO.cs
    /// Creation date           : 24/07/2017
    /// Creation time           : 05:58 p. m.
    ///</summary>
    public class sp_verificaBimProveedorData : sp_verificaBimProveedorProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="sp_verificaBimProveedor"> Object sp_verificaBimProveedor added to List</param>
        public override int Addsp_verificaBimProveedor(sp_verificaBimProveedorEntity entity_sp_verificaBimProveedor)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorAdd", connection);

                AssingParameter(comandoSql, "@Clv_orden", entity_sp_verificaBimProveedor.Clv_orden);

                AssingParameter(comandoSql, "@TieneBimProveedor", entity_sp_verificaBimProveedor.TieneBimProveedor);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@Idsp_verificaBimProveedor"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a sp_verificaBimProveedor
        ///</summary>
        /// <param name="">   to delete </param>
        public override int Deletesp_verificaBimProveedor()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a sp_verificaBimProveedor
        ///</summary>
        /// <param name="sp_verificaBimProveedor"> Objeto sp_verificaBimProveedor a editar </param>
        public override int Editsp_verificaBimProveedor(sp_verificaBimProveedorEntity entity_sp_verificaBimProveedor)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorEdit", connection);

                AssingParameter(comandoSql, "@Clv_orden", entity_sp_verificaBimProveedor.Clv_orden);

                AssingParameter(comandoSql, "@TieneBimProveedor", entity_sp_verificaBimProveedor.TieneBimProveedor);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all sp_verificaBimProveedor
        ///</summary>
        public override List<sp_verificaBimProveedorEntity> Getsp_verificaBimProveedor()
        {
            List<sp_verificaBimProveedorEntity> sp_verificaBimProveedorList = new List<sp_verificaBimProveedorEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        sp_verificaBimProveedorList.Add(Getsp_verificaBimProveedorFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return sp_verificaBimProveedorList;
        }

        /// <summary>
        /// Gets all sp_verificaBimProveedor by List<int>
        ///</summary>
        public override List<sp_verificaBimProveedorEntity> Getsp_verificaBimProveedor(List<int> lid)
        {
            List<sp_verificaBimProveedorEntity> sp_verificaBimProveedorList = new List<sp_verificaBimProveedorEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        sp_verificaBimProveedorList.Add(Getsp_verificaBimProveedorFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return sp_verificaBimProveedorList;
        }

        /// <summary>
        /// Gets sp_verificaBimProveedor by
        ///</summary>
        public override sp_verificaBimProveedorEntity Getsp_verificaBimProveedorById(long? Clv_orden)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("sp_verificaBimProveedor", connection);
                sp_verificaBimProveedorEntity entity_sp_verificaBimProveedor = null;

                AssingParameter(comandoSql, "@Clv_orden", Clv_orden);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_sp_verificaBimProveedor = Getsp_verificaBimProveedorFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_sp_verificaBimProveedor;
            }

        }



        /// <summary>
        ///Get sp_verificaBimProveedor
        ///</summary>
        public override SoftvList<sp_verificaBimProveedorEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<sp_verificaBimProveedorEntity> entities = new SoftvList<sp_verificaBimProveedorEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(Getsp_verificaBimProveedorFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = Getsp_verificaBimProveedorCount();
                return entities ?? new SoftvList<sp_verificaBimProveedorEntity>();
            }
        }

        /// <summary>
        ///Get sp_verificaBimProveedor
        ///</summary>
        public override SoftvList<sp_verificaBimProveedorEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<sp_verificaBimProveedorEntity> entities = new SoftvList<sp_verificaBimProveedorEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(Getsp_verificaBimProveedorFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = Getsp_verificaBimProveedorCount(xml);
                return entities ?? new SoftvList<sp_verificaBimProveedorEntity>();
            }
        }

        /// <summary>
        ///Get Count sp_verificaBimProveedor
        ///</summary>
        public int Getsp_verificaBimProveedorCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count sp_verificaBimProveedor
        ///</summary>
        public int Getsp_verificaBimProveedorCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.sp_verificaBimProveedor.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_sp_verificaBimProveedorGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data sp_verificaBimProveedor " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;
using Globals;

namespace Softv.DAO
{
    /// <summary>
    /// Class                   : Softv.DAO.uspHaz_PreguntaData
    /// Generated by            : Class Generator (c) 2014
    /// Description             : uspHaz_Pregunta Data Access Object
    /// File                    : uspHaz_PreguntaDAO.cs
    /// Creation date           : 19/01/2017
    /// Creation time           : 06:40 p. m.
    ///</summary>
    public class uspHaz_PreguntaData : uspHaz_PreguntaProvider
    {
        /// <summary>
        ///</summary>
        /// <param name="uspHaz_Pregunta"> Object uspHaz_Pregunta added to List</param>
        public override int AdduspHaz_Pregunta(uspHaz_PreguntaEntity entity_uspHaz_Pregunta)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaAdd", connection);

                AssingParameter(comandoSql, "@Pregunta", entity_uspHaz_Pregunta.Pregunta);

                AssingParameter(comandoSql, "@MesesAdelantados", entity_uspHaz_Pregunta.MesesAdelantados);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error adding uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                }
                result = (int)comandoSql.Parameters["@IduspHaz_Pregunta"].Value;
            }
            return result;
        }

        /// <summary>
        /// Deletes a uspHaz_Pregunta
        ///</summary>
        /// <param name="">   to delete </param>
        public override int DeleteuspHaz_Pregunta()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaDelete", connection);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = ExecuteNonQuery(comandoSql);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error deleting uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Edits a uspHaz_Pregunta
        ///</summary>
        /// <param name="uspHaz_Pregunta"> Objeto uspHaz_Pregunta a editar </param>
        public override int EdituspHaz_Pregunta(uspHaz_PreguntaEntity entity_uspHaz_Pregunta)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaEdit", connection);

                AssingParameter(comandoSql, "@Pregunta", entity_uspHaz_Pregunta.Pregunta);

                AssingParameter(comandoSql, "@MesesAdelantados", entity_uspHaz_Pregunta.MesesAdelantados);

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    result = int.Parse(ExecuteNonQuery(comandoSql).ToString());

                }
                catch (Exception ex)
                {
                    throw new Exception("Error updating uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all uspHaz_Pregunta
        ///</summary>
        public override List<uspHaz_PreguntaEntity> GetuspHaz_Pregunta()
        {
            List<uspHaz_PreguntaEntity> uspHaz_PreguntaList = new List<uspHaz_PreguntaEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaGet", connection);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        uspHaz_PreguntaList.Add(GetuspHaz_PreguntaFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return uspHaz_PreguntaList;
        }

        /// <summary>
        /// Gets all uspHaz_Pregunta by List<int>
        ///</summary>
        public override List<uspHaz_PreguntaEntity> GetuspHaz_Pregunta(List<int> lid)
        {
            List<uspHaz_PreguntaEntity> uspHaz_PreguntaList = new List<uspHaz_PreguntaEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {
                DataTable IdDT = BuildTableID(lid);

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaGetByIds", connection);
                AssingParameter(comandoSql, "@IdTable", IdDT);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);

                    while (rd.Read())
                    {
                        uspHaz_PreguntaList.Add(GetuspHaz_PreguntaFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return uspHaz_PreguntaList;
        }

        /// <summary>
        /// Gets uspHaz_Pregunta by
        ///</summary>
        public override uspHaz_PreguntaEntity GetuspHaz_PreguntaById(long? Contrato, int? Op)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("softvweb_uspHaz_Pregunta", connection);
                uspHaz_PreguntaEntity entity_uspHaz_Pregunta = null;

                AssingParameter(comandoSql, "@Contrato", Contrato);
                AssingParameter(comandoSql, "@Op", Op);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_uspHaz_Pregunta = GetuspHaz_PreguntaFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_uspHaz_Pregunta;
            }

        }



        public override uspHaz_PreguntaEntity GetAfirmacionPre(long? Contrato, int? MesesAdelantados, int? Op, long? ClvSession, int? Op2)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("AfirmacionPregunta", connection);
                uspHaz_PreguntaEntity entity_uspHaz_Pregunta = null;

                AssingParameter(comandoSql, "@Contrato", Contrato);
                AssingParameter(comandoSql, "@MesesAdelantados", MesesAdelantados);
                AssingParameter(comandoSql, "@Op", Op);
                AssingParameter(comandoSql, "@ClvSession", ClvSession);
                AssingParameter(comandoSql, "@Op2", Op2);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_uspHaz_Pregunta = GetAfirmacionReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_uspHaz_Pregunta;
            }

        }



        public override uspHaz_PreguntaEntity GetDeepuspHaz_Pregunta_CM(long? Contrato, int? Op)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("uspHaz_Pregunta_CM", connection);
                uspHaz_PreguntaEntity entity_uspHaz_Pregunta = null;

                AssingParameter(comandoSql, "@Contrato", Contrato);
                AssingParameter(comandoSql, "@Op", Op);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_uspHaz_Pregunta = GetuspHaz_PreguntaFromReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_uspHaz_Pregunta;
            }

        }


        public override uspHaz_PreguntaEntity GetDeeAfirmacionPregunta_CM(long? Contrato, int? MesesAdelantados, int? Op, long? ClvSession, int? Op2)
        {
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("AfirmacionPreguntaMaestro", connection);
                uspHaz_PreguntaEntity entity_uspHaz_Pregunta = null;

                AssingParameter(comandoSql, "@ContratoMaestro", Contrato);
                AssingParameter(comandoSql, "@MesesAdelantados", MesesAdelantados);
                AssingParameter(comandoSql, "@Op", Op);
                AssingParameter(comandoSql, "@ClvSession", ClvSession);
                AssingParameter(comandoSql, "@Op2", Op2);

                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                    if (rd.Read())
                        entity_uspHaz_Pregunta = GetAfirmacionReader(rd);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                return entity_uspHaz_Pregunta;
            }

        }


































        /// <summary>
        ///Get uspHaz_Pregunta
        ///</summary>
        public override SoftvList<uspHaz_PreguntaEntity> GetPagedList(int pageIndex, int pageSize)
        {
            SoftvList<uspHaz_PreguntaEntity> entities = new SoftvList<uspHaz_PreguntaEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaGetPaged", connection);

                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@pageSize", pageSize);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetuspHaz_PreguntaFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetuspHaz_PreguntaCount();
                return entities ?? new SoftvList<uspHaz_PreguntaEntity>();
            }
        }

        /// <summary>
        ///Get uspHaz_Pregunta
        ///</summary>
        public override SoftvList<uspHaz_PreguntaEntity> GetPagedList(int pageIndex, int pageSize, String xml)
        {
            SoftvList<uspHaz_PreguntaEntity> entities = new SoftvList<uspHaz_PreguntaEntity>();
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaGetPagedXml", connection);

                AssingParameter(comandoSql, "@pageSize", pageSize);
                AssingParameter(comandoSql, "@pageIndex", pageIndex);
                AssingParameter(comandoSql, "@xml", xml);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = ExecuteReader(comandoSql);
                    while (rd.Read())
                    {
                        entities.Add(GetuspHaz_PreguntaFromReader(rd));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
                entities.totalCount = GetuspHaz_PreguntaCount(xml);
                return entities ?? new SoftvList<uspHaz_PreguntaEntity>();
            }
        }

        /// <summary>
        ///Get Count uspHaz_Pregunta
        ///</summary>
        public int GetuspHaz_PreguntaCount()
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaGetCount", connection);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }


        /// <summary>
        ///Get Count uspHaz_Pregunta
        ///</summary>
        public int GetuspHaz_PreguntaCount(String xml)
        {
            int result = 0;
            using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.uspHaz_Pregunta.ConnectionString))
            {

                SqlCommand comandoSql = CreateCommand("Softv_uspHaz_PreguntaGetCountXml", connection);

                AssingParameter(comandoSql, "@xml", xml);
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    result = (int)ExecuteScalar(comandoSql);


                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data uspHaz_Pregunta " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                }
            }
            return result;
        }

        #region Customs Methods

        #endregion
    }
}

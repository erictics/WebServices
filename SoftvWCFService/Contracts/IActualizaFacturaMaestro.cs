﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IActualizaFacturaMaestro
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetActualizaFacturaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ActualizaFacturaMaestroEntity GetActualizaFacturaMaestro(long? ClvFacturaMaestro, int? NoPago, Decimal? PagoInicial);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddActualizaFacturaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddActualizaFacturaMaestro(ActualizaFacturaMaestroEntity objActualizaFacturaMaestro);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IActualiza_Instalacion
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetActualiza_InstalacionList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Actualiza_InstalacionEntity> GetActualiza_InstalacionList(int? CLV_LLAVE, int? Clv_TipoCliente, int? opc);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddActualiza_Instalacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddActualiza_Instalacion(Actualiza_InstalacionEntity objActualiza_Instalacion);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IActualizar_quejasCallCenter
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepActualizar_quejasCallCenter", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Actualizar_quejasCallCenterEntity GetDeepActualizar_quejasCallCenter(int? Clv_queja, int? CallCenter);

    }
}


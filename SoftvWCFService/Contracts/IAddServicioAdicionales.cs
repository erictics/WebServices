﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IAddServicioAdicionales
    {
         
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepAddServicioAdicionales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        AddServicioAdicionalesEntity GetDeepAddServicioAdicionales(long? Clv_Session, String Clv_Txt, long? Clv_UnicaNet, long? Clave, int? MesesAdelantados, int? TvAdic, int? op , int? Clv_TipoCliente);


    }
}


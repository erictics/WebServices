﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;


namespace SoftvWCFService.Contracts 
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IAgregaDetalleNotaDeCreditoMaestro
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgregaDetalleNotaDeCreditoMaestroList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<AgregaDetalleNotaDeCreditoMaestroEntity> GetAgregaDetalleNotaDeCreditoMaestroList(AgregaDetalleNotaDeCreditoMaestroEntity ObjDetalle, List<AgregarDetalle_NC_CM> lstDetalle);


    }
}


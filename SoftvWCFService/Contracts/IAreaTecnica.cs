﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.ComponentModel;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IAreaTecnica
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConSector", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SectorEntity> GetConSector(int? clvsector, string clv_txt, string descripcion, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraColoniaSec", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ColoniaEntity> GetMuestraColoniaSec(int? Clv_Colonia, string Clv_Sector, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConRelSectorColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         List<ColoniaEntity> GetConRelSectorColonia(int? Clv_Sector);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueSector", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueSector(int? op, string Clv_Txt, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelSectorColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueRelSectorColonia(int? Clv_Sector, int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorRelSectorColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorRelSectorColonia(int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModSector", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModSector(int? Clv_Sector, string Clv_Txt,string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCluster", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ClusterEntity> GetMuestraCluster(int? opcion, string clave, string descripcion, int? clv_cluster);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInsertUpdateCluster", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetInsertUpdateCluster(int? opcion, string clave, string descripcion, int? clv_cluster);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraRelClusterSector", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SectorEntity> GetMuestraRelClusterSector(int? clv_cluster, int? opcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuitarEliminarRelClusterSector", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetQuitarEliminarRelClusterSector(int? clv_cluster, int? opcion, int? clv_sector);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConSector2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SectorEntity> GetConSector2(int? Clv_Sector, string Clv_Txt, string Descripcion, int? Op, int? clv_cluster);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONSULTATap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TapEntity> GetCONSULTATap(TapEntity TapEntity);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAPostes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PosteEntity> GetMUESTRAPostes(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCalleSec", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CalleEntity> GetMuestraCalleSec(int? Clv_Sector, int? Clv_Colonia, int? Clv_Calle, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetINSERTATap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetINSERTATap(TapEntity TapEntity);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODIFICATap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetMODIFICATap(TapEntity TapEntity);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_GetTrabajoByClv_TipSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TrabajoEntity> GetSoftv_GetTrabajoByClv_TipSer(int? Clv_TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_GetTrabajoByClv_TipSerTipo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TrabajoEntity> GetSoftv_GetTrabajoByClv_TipSerTipo(int? Clv_TipSer, string Tipo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Imputables", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ImputableEntity> GetMuestra_Imputables();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Articulos_Acometida", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ArticulosAcometidaEntity> GetMuestra_Articulos_Acometida();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Articulos_Clasificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ArticulosclasificacionEntity> GetMuestra_Articulos_Clasificacion(int? clvtipo,int ? Clv_tipser);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaRelMaterialTrabajos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MaterialtrabajonEntity> GetConsultaRelMaterialTrabajos(int? clvtrabajo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_GetTrabajoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TrabajoEntity GetSoftv_GetTrabajoById(int? clvtrabajo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_AddTrabajo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftv_AddTrabajo(TrabajoEntity trabajo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_AddRelMaterialTrabajo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftv_AddRelMaterialTrabajo(int? Clv_TipoArticulo, int? Clv_Articulo, int? Cantidad, int? Clv_Trabajo, int? Clv_TipSer);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_DeleteRelMaterialTrabajo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftv_DeleteRelMaterialTrabajo(int? Clv_Material);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_EditTrabajo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftv_EditTrabajo(TrabajoEntity trabajo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuarda_imputablePorTrabajo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGuarda_imputablePorTrabajo(int? Clv_Trabajo, int? imputable);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorSector", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorSector(int? Clv_Sector);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConHub", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SectorEntity> GetConHub(int? Clv_Sector, string Clv_Txt, string Descripcion, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraColoniaHub", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ColoniaEntity> GetMuestraColoniaHub(int? Clv_Colonia, int? Clv_Sector, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConRelHubColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<hubcoloniaEntity> GetConRelHubColonia(int? Clv_Sector);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueHub", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueHub(int? op, string Clv_Txt, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelHubColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueRelHubColonia(int? Clv_Sector, int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorRelHubColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorRelHubColonia(int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModHub", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModHub(int? Clv_Sector, string Clv_Txt, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorHub", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorHub(int? Clv_Sector);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraDescOlt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PosteEntity> GetMuestraDescOlt(int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInsertaNueDescOlt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetInsertaNueDescOlt(int? Clave, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaNueDescOLT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetValidaNueDescOLT(int? Clave, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONSULTAnap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TapEntity> GetCONSULTAnap(int? Op, int? IdTap, string Clave, string Sector, string Poste, string Colonia, string Calle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCalleHub", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CalleEntity> GetMuestraCalleHub(int? Clv_Sector, int? Clv_Colonia, int? Clv_Calle, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAOlt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PosteEntity> GetMUESTRAOlt(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetINSERTAnap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TapEntity GetINSERTAnap(TapEntity ObjNAP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODIFICAnap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TapEntity GetMODIFICAnap(TapEntity ObjNAP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogoTarjetasOlt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TarjetaOLTEntity> GetCatalogoTarjetasOlt(int? ClaveOlt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInsertaNueCatalogoTarjetasOlt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetInsertaNueCatalogoTarjetasOlt(TarjetaOLTEntity ObjTarjetaOLT);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspInsertaTblClasificacionProblemas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetuspInsertaTblClasificacionProblemas(int? clvProblema, string descripcion, bool activo, int opAccion);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspConsultaTblClasificacionProblemas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<clasproblemasEntity> GetuspConsultaTblClasificacionProblemas(int? clvProblema, string descripcion, int? opBusqueda);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTrabajosResumen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MUESTRATRABAJOS_NewEntity> GetMuestraTrabajosResumen(int? clv_usuario);

        

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IAsignaAparatosAlServicio
    {
         

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAsignaAparatosAlServicioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<AsignaAparatosAlServicioEntity> GetAsignaAparatosAlServicioList(AsignaAparatosAlServicioEntity obj, List<MuestraArbolServiciosAparatosPorinstalar_dosEntity> Lst);

    }
}


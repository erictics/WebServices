﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBORRel_Trabajos_NoCobroMensual
    {
         
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteBORRel_Trabajos_NoCobroMensual", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteBORRel_Trabajos_NoCobroMensual(int? Clv_Servicio);

    }
}


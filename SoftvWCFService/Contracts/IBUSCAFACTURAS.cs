﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract] 
    public interface IBUSCAFACTURAS
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBUSCAFACTURASList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BUSCAFACTURASEntity> GetBUSCAFACTURASList(int? Op, String Serie, int? Folio, String FechaOp, String ContratoCom, String tipo, long? IdCompania, String NombreOp, int? IdUsuario);


    }
}


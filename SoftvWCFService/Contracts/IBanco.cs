﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBanco
    {
         
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        BancoEntity GetBanco(int? IdBanco);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        BancoEntity GetDeepBanco(int? IdBanco);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBancoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BancoEntity> GetBancoList(int? IdBanco, String Nombre, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddBanco(BancoEntity objBanco);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateBanco(BancoEntity objBanco);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteBanco(int? IdBanco);

    }
}


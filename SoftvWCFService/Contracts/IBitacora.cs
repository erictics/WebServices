﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBitacora
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddBitacora", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddBitacora(BitacoraEntity objBitacora);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddBitacoraTickets", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddBitacoraTickets (BitacoraEntity objBitacora);
         

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddReenviarEdoCuenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddReenviarEdoCuenta(BitacoraEntity objReenviarEdoCuenta);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddReprocesarEdoCuenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddReprocesarEdoCuenta(BitacoraEntity objReprocesarEdoCuenta);


    }
}


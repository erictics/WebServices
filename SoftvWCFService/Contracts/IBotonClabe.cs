﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBotonClabe 
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBotonClabe", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        BotonClabeEntity GetBotonClabe();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepBotonClabe", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        BotonClabeEntity GetDeepBotonClabe();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBotonClabeList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BotonClabeEntity> GetBotonClabeList(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBotonImprimeClabe", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BotonClabeEntity> GetBotonImprimeClabe(long? Contrato);


    }
}


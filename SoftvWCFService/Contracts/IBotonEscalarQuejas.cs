﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBotonEscalarQuejas
    {
         

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameNoTicketQuejaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BotonEscalarQuejasEntity> GetDameNoTicketQuejaList(long? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEscalarQuejasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BotonEscalarQuejasEntity> GetEscalarQuejasList(long? Clv_Queja, int? IdUsuario, int? NoTicket);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBusCliPorContrato_Fac
    {
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetBusCliPorContrato_Fac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //BusCliPorContrato_FacEntity GetBusCliPorContrato_Fac();

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetDeepBusCliPorContrato_Fac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //BusCliPorContrato_FacEntity GetDeepBusCliPorContrato_Fac();

        [OperationContract] 
        [WebInvoke(Method = "*", UriTemplate = "GetBusCliPorContrato_FacList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BusCliPorContrato_FacEntity> GetBusCliPorContrato_FacList(int? Id, String ContratoC);



    }
}


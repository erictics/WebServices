﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBuscaFacturasFisca
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaFacturasFiscaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BuscaFacturasFiscaEntity> GetBuscaFacturasFiscaList(String Factura, String Fecha, int? Todas, long? ContratoMaestro, int? Opcion);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddDetalleFacFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BuscaFacturasFiscaEntity> GetAddDetalleFacFiscal(BuscaFacturasFiscaEntity Obj, List<DameDetalle_FacturaMaestroFiscalEntity> Lst);

         
    }
}


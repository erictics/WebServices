﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBuscaFacturasMaestroPendientes
    {
         
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaFacturasMaestroPendientesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BuscaFacturasMaestroPendientesEntity> GetBuscaFacturasMaestroPendientesList(String Fecha, String Ticket, long? ContratoMaestro, String Cliente, int? Op, String ContratoCompania);


    }
}


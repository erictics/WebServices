﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBuscaOrdenServicio
    {
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetBuscaOrdenServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //BuscaOrdenServicioEntity GetBuscaOrdenServicio();

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetDeepBuscaOrdenServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //BuscaOrdenServicioEntity GetDeepBuscaOrdenServicio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaOrdenServicioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BuscaOrdenServicioEntity> GetBuscaOrdenServicioList(String STATUS, long? ContratoO);
         

    }
}


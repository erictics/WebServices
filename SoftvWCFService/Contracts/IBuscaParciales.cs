﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBuscaParciales
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaParcialesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BuscaParcialesEntity> GetBuscaParcialesList(int? Op, String Referencia, String NomCajera, String Fecha, int? IdUsuario, int? IdCompania);
        
    } 
}


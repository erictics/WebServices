﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBuscaQuejasL
    {
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetBuscaQuejasL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //BuscaQuejasLEntity GetBuscaQuejasL();

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetDeepBuscaQuejasL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //BuscaQuejasLEntity GetDeepBuscaQuejasL();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaQuejasLList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BuscaQuejasLEntity> GetBuscaQuejasLList(int? Clv_TipSer, long? ContratoO, String Status);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBuscaQuejasSeparado2
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaQuejasSeparado2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BuscaQuejasSeparado2Entity> GetBuscaQuejasSeparado2List(int? Clv_TipSer, long? Clv_Queja, String Contrato, String NOMBRE, String AP, String AM, String CALLE, String NUMERO, String SetupBox, String Status, int? Op, int? ClvColonia, int? IdCompania, int? ClvUsuario, bool SoloNivel2, int? NoTicket);


         
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBuscaSiTieneQueja
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaSiTieneQueja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        BuscaSiTieneQuejaEntity GetBuscaSiTieneQueja(int? ClvTipSer, long? Contrato);

    } 
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBusquedaQuejasMasivas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBusquedaQuejasMasivasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<BusquedaQuejasMasivasEntity> GetBusquedaQuejasMasivasList(int? Op, int? Clv_Queja, String Status, int? Edo, int? Cd, int? Loc, int? Col, int? Calle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_MQClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMClientesEntity> GetSoftvWeb_MQClientes(QMClientesEntity ObjCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_QMEstados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MQEstadosEntity> GetSoftvWeb_QMEstados(long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_QMCiudades", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMCiudadesEntity> GetSoftvWeb_QMCiudades(List<MQEstadosEntity> ListEstado, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_QMLocalidades", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMLocalidadEntity> GetSoftvWeb_QMLocalidades(List<QMCiudadesEntity> ListCiudad, List<MQEstadosEntity> ListEstado, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GEtSoftvWeb_QMColonias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMColoniaEntity> GEtSoftvWeb_QMColonias(List<QMLocalidadEntity> ListLocalidad, List<QMCiudadesEntity> ListCiudad, List<MQEstadosEntity> ListEstado, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_QMCalles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMCalleEntity> GetSoftvWeb_QMCalles(List<QMColoniaEntity> ListColonia, List<QMLocalidadEntity> ListLocalidad, List<QMCiudadesEntity> ListCiudad, List<MQEstadosEntity> ListEstado, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQMTecnicos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TecnicosEntity> GetQMTecnicos(long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ValidaQMArea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetSoftvWeb_ValidaQMArea(List<QMCalleEntity> ListCalles, List<QMColoniaEntity> ListColonia, List<QMLocalidadEntity> ListLocalidad, List<QMCiudadesEntity> ListCiudad, List<MQEstadosEntity> ListEstado, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ValidaQMContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetSoftvWeb_ValidaQMContrato(List<QMClientesEntity> ListCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_UpdateEstadoQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftvWeb_UpdateEstadoQM(List<MQEstadosEntity> ListEstado, long? Clv_Queja, int? OP, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_UpdateCiudadQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftvWeb_UpdateCiudadQM(List<QMCiudadesEntity> ListCiudad, long? Clv_Queja, int? OP, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_UpdateLocalidadQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftvWeb_UpdateLocalidadQM(List<QMLocalidadEntity> ListLocalidad, long? Clv_Queja, int? OP, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_UpdateColoniaQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftvWeb_UpdateColoniaQM(List<QMColoniaEntity> ListColonia, long? Clv_Queja, int? OP, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_UpdateCalleQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftvWeb_UpdateCalleQM(List<QMCalleEntity> ListCalle, long? Clv_Queja, int? OP, long? Clave);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICAMDO
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConCAMDO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CAMDOEntity GetConCAMDO(long? CLAVE, long? Clv_Orden, long? CONTRATO);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCAMDO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CAMDOEntity GetDeepCAMDO(long? CLAVE, long? Clv_Orden, long? CONTRATO);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCAMDOList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CAMDOEntity> GetCAMDOList();
         

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCAMDO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCAMDO(CAMDOEntity objCAMDO);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCAMDO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCAMDO(CAMDOEntity objCAMDO);


        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteCAMDO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteCAMDO(long? CLAVE, long? Clv_Orden, long? CONTRATO);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetllenaCiudadCamdoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CAMDOEntity> GetllenaCiudadCamdoList(long? CONTRATO);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetllenaLocalidadCamdoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CAMDOEntity> GetllenaLocalidadCamdoList(long? CONTRATO, int? Clv_Ciudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetllenaColoniaCamdoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CAMDOEntity> GetllenaColoniaCamdoList(long? CONTRATO, int? Clv_Localidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetllenaCalleCamdoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CAMDOEntity> GetllenaCalleCamdoList(long? CONTRATO, int? Clv_Colonia);

    }
}


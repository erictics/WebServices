﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICAMDOFAC
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCAMDOFAC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CAMDOFACEntity GetCAMDOFAC(long? Clv_Sesion, long? CONTRATO);
         

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCAMDOFACList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CAMDOFACEntity> GetCAMDOFACList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCAMDOFAC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCAMDOFAC(CAMDOFACEntity objCAMDOFAC);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCAMDOFAC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCAMDOFAC(long? Clv_Sesion, long? CONTRATO);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICLIENTES_New
    { 


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCLIENTES_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CLIENTES_NewEntity> GetCLIENTES_NewList(String Nombre, String SegundoNombre, String Apellido_Paterno, String Apellido_Materno, String FechaNacimiento, bool? EsFisica, String TELEFONO, String CELULAR, String Email, String ClaveElector, int? IdCompania, int? Clv_Estado, int? Clv_Ciudad, int? Clv_Localidad, int? Clv_Colonia, int? Clv_Calle, String ENTRECALLES, String NUMERO, String NumInt, String CodigoPostal, long? IdUsuario, int? TipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCLIENTES_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCLIENTES_New(CLIENTES_NewEntity objCLIENTES_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaClientesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CLIENTES_NewEntity> GetConsultaClientesList(long? CONTRATO);

        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesFiltosNew", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<CLIENTES_NewEntity> GetClientesFiltosNew(CLIENTES_NewEntity lstCliente);
        
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaClientes_Filtros_List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CLIENTES_NewEntity> GetConsultaClientes_Filtros_List(CLIENTES_NewEntity lstCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddRelClienteIDentificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddRelClienteIDentificacion(IdentificacionEntity ObjIden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscarClienteIdentficacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IdentificacionEntity GetBuscarClienteIdentficacion(int? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInsertaEntrecalles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetInsertaEntrecalles(EntreCallesEntity ObjEntrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWEb_DameEntrecalles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EntreCallesEntity GetSoftvWEb_DameEntrecalles(long? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvweb_DameContratoAnt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClienteContrtoAnterior GetSoftvweb_DameContratoAnt(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConCPByColoniaLocalidadCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        muestraCP_ColoniaLocalidadEntity GetConCPByColoniaLocalidadCiudad(long? Clv_Colonia, long? Clv_Localidad, long? Clv_Ciudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlacaCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetPlacaCliente(long? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaDFCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaDFCliente(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListaContratoSimilar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ContratoSimilarEntity> GetListaContratoSimilar(ContratoSimilarEntity ObjContratoSimilar);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddBitacoraDireccionSimilar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddBitacoraDireccionSimilar(List<BitacoraDireccionSimilarEntity> ObjBitacora);

    }
}


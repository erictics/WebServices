﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICalcula_monto
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalcula_monto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Calcula_montoEntity GetCalcula_monto(long? ClvFactura, int? Op);
    }
} 


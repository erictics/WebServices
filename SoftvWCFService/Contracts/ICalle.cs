﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICalle
    {
        [OperationContract] 
        [WebInvoke(Method = "*", UriTemplate = "GetCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CalleEntity GetCalle(int? IdCalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CalleEntity GetDeepCalle(int? IdCalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalleList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CalleEntity> GetCalleList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCallePagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<CalleEntity> GetCallePagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCallePagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<CalleEntity> GetCallePagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCalle(CalleEntity objCalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCalle(CalleEntity objCalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCalle(String BaseRemoteIp, int BaseIdUser, int? IdCalle);





        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCalleL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCalleL(CalleEntity lstRelCalle, List<RelCalleColoniaEntity> RelCalleAdd);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCalleL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCalleL(CalleEntity lstRelCalle, List<RelCalleColoniaEntity> RelCalleAdd);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "UpdateCalleL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? UpdateCalleL(CalleEntity lstRelCalle, List<RelCalleColoniaEntity> RelCalleAdd, List<RelCalleColoniaEntity> RelCalleDel);





    }
}


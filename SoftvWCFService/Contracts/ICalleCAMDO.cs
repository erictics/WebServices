﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICalleCAMDO
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalleCAMDOList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CalleCAMDOEntity> GetCalleCAMDOList(long? Contrato, int? colonia);
         

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICalles_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Calles_NewEntity GetCalles_New(int? Clv_Calle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Calles_NewEntity GetDeepCalles_New(int? Clv_Calle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalles_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Calles_NewEntity> GetCalles_NewList(int? Clv_Calle, String NOMBRE, int? Op, int? Idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCalles_New(Calles_NewEntity objCalles_New);

        [OperationContract] 
        [WebInvoke(Method = "*", UriTemplate = "UpdateCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCalles_New(Calles_NewEntity objCalles_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCalles_New();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaColServCli", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaColServCli(long? IdCompania, long? Clv_Estado, long? Clv_Ciudad, long? Clv_Localidad, long? Clv_Colonia);

    }
}


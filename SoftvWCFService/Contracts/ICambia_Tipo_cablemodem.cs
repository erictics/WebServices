﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICambia_Tipo_cablemodem
    { 

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCambia_Tipo_cablemodem", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCambia_Tipo_cablemodem(Cambia_Tipo_cablemodemEntity objCambia_Tipo_cablemodem);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{ 
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICancela_Folios
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCancela_Folios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCancela_Folios(Cancela_FoliosEntity objCancela_Folios);


    }
}


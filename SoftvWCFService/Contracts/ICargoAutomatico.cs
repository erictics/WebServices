﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICargoAutomatico
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCargoAutomatico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetCargoAutomatico();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCargoAutomatico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetDeepCargoAutomatico();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAllCargoAutomaticoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CargoAutomaticoEntity> GetCargoAutomaticoList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCargoAutomatico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCargoAutomatico(CargoAutomaticoEntity objCargoAutomatico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCargoAutomatico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCargoAutomatico(CargoAutomaticoEntity objCargoAutomatico);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteCargoAutomatico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteCargoAutomatico(String BaseRemoteIp, int BaseIdUser,);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAllMUESTRAPERIODOS_Seleccionar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CargoAutomaticoEntity> GetAllMUESTRAPERIODOS_Seleccionar(int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteBorraTablasArchivos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteBorraTablasArchivos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DamedatosUsuario_CA GetDameDatosUsuario(string Clv_Usuario, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDAMENOMBRESUCURSAL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DAMENOMBRESUCURSAL_CA GetDAMENOMBRESUCURSAL(int? Clv_Sucursal);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosGenerales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDatosGenerales_CA GetDameDatosGenerales();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConPreliminarBancosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ConPreliminarBancos_CA> GetConPreliminarBancosList(int? Clv_Periodo, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONDETFACTURASBANCOSList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CONDETFACTURASBANCOS_CA> GetCONDETFACTURASBANCOSList(long? Clv_SessionBancos, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDame_PreRecibo_oFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Dame_PreRecibo_oFactura_CA GetDame_PreRecibo_oFactura(long? Clv_Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameGeneralesBancos_Total", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameGeneralesBancos_Total_CA GetDameGeneralesBancos_Total(long? Clv_SessionBancos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddGeneraBancos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddGeneraBancos(CargoAutomaticoEntity objCargoAutomatico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameRangoFacturas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetDameRangoFacturas(long? Clv_SessionBancos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosGeneralesBanamex", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetDameDatosGeneralesBanamex(long? Clv_SessionBancos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameGeneralesBancos_Total_Detalle_Banamex", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetDameGeneralesBancos_Total_Detalle_Banamex(long? Clv_SessionBancos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMODDetFacturasBancos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMODDetFacturasBancos(CONDETFACTURASBANCOS_CA objCargoAutomatico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaAfectacionBancos_WEB", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetValidaAfectacionBancos_WEB(long? Clv_SessionBancos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateGUARDAFACTURASBANCOS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateGUARDAFACTURASBANCOS(CONDETFACTURASBANCOS_CA objCargoAutomatico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCANCELALISTADOPRELIMINAR_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetCANCELALISTADOPRELIMINAR_web(long? Clv_SessionBancos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddInserta_Proceso_archivo_bancomer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddInserta_Proceso_archivo_bancomer(CargoAutomaticoEntity objCargoAutomatico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddProcesa_archivo_bancomer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddProcesa_archivo_bancomer(CargoAutomaticoEntity objCargoAutomatico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteBorra_tabla_afectacion_bancomer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteBorra_tabla_afectacion_bancomer(long? Clv_SessionBancos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDame_archivo_bancomer_WEB", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetDame_archivo_bancomer_WEB();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsulta_Grales_Prosa_bancomer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoAutomaticoEntity GetConsulta_Grales_Prosa_bancomer(long? Clv_SessionBancos, String Ruta);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaDocumentoTxt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int? GetGuardaDocumentoTxt();
        
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract] 
    public interface ICatMedios
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatMediosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CatMediosEntity> GetCatMediosList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatMedioByCiuLocCol", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CatMediosEntity> GetCatMedioByCiuLocCol(int? Clv_Ciudad, int? Clv_Localidad, int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConDescuentoCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<string> GetConDescuentoCombo(string Descripcion, int? Op, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getsp_muestraServiciosCombos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ServiciosInternetEntity> Getsp_muestraServiciosCombos(int? Clv_TipSer, int? Clv_Servicio, int? Op, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Get_clv_session_Reportes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Get_clv_session_Reportes();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConDetDescuentoCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DescuentoComboEntity> GetConDetDescuentoCombo(long? Clv_Descuento, long? Clv_TipoCliente, string Descripcion, long? Clv_Session, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgrPreDetDescuentoCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAgrPreDetDescuentoCombo(DescuentoComboEntity DescuentoCombo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEliPreDetDescuentoCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetEliPreDetDescuentoCombo(long? Clv_Session, long? Clv_Servicio, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModDescuentoCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModDescuentoCombo(long? Clv_Descuento, int? Clv_TipoCliente, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueDescuentoCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetNueDescuentoCombo(int? Clv_TipoCliente, string Descripcion, long? idcompania);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueDetDescuentoCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetNueDetDescuentoCombo(int? Clv_Descuento, long? Clv_Session, long? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameClv_Descuento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetDameClv_Descuento(int? Clv_TipoCliente, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConMedioByCiuLocColTpServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CatMediosEntity> GetConMedioByCiuLocColTpServ(int? Clv_Ciudad, int? Clv_Localidad, int? Clv_Colonia, int? IdServicio);
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICatalogoAgenda
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_MuestraSectores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SectorEntity> GetSoftv_MuestraSectores();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Tecnicos_Agenda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TecnicosEntity> GetMuestra_Tecnicos_Agenda(int ? clv_usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDesplegarAgenda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<AgendaEntity> GetDesplegarAgenda(AgendaEntity ObjAgenda);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Tecnicos_Almacen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TecnicosEntity> GetMuestra_Tecnicos_Almacen(int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONCITAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CitaEntity GetCONCITAS(int? Clv_Cita);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVERORDENES_CITAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CitaEntity GetVERORDENES_CITAS(int? CLV_CITA);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBUSCLIPORCONTRATO2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClienteEntity GetBUSCLIPORCONTRATO2(int? CONTRATO, String NOMBRE, String CALLE, String NUMERO, String CIUDAD, String Telefono, int? OP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDame_DetOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MUESTRATRABAJOS_NewEntity> GetDame_DetOrdSer(int? Clv_Orden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBUSCADetCitas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CitaEntity GetBUSCADetCitas(int? CLV_CITA);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTrabajosOrdenesTecnicoDia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TrabajosOrdenesTecnicoDiaEntity> GetTrabajosOrdenesTecnicoDia(String fecha, int? Clv_Tecnico);

        [OperationContract] 
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAHORAS_CITAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<HoraEntity> GetMUESTRAHORAS_CITAS(String FECHA, int? CLV_TECNICO);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONSULTARREL_CITAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetCONSULTARREL_CITAS(int? Clv_Cita);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODIFICA_REL_CITAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetMODIFICA_REL_CITAS(ObjCitaEntity ObjCita);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBOR_CITAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBOR_CITAS(int? CLV_CITA);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVERQUEJAS_CITAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CitaQuejaEntity GetVERQUEJAS_CITAS(int? CLV_CITA);
    }
}


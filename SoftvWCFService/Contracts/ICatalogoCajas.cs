﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICatalogoCajas
    {
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetCatalogoCajas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    CatalogoCajasEntity GetCatalogoCajas(int? Clave);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetDeepCatalogoCajas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    CatalogoCajasEntity GetDeepCatalogoCajas(int? Clave);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetCatalogoCajasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    IEnumerable<CatalogoCajasEntity> GetCatalogoCajasList(int? Clave, string Descripcion, int? OP, long? idcompania, int? clv_usuario);
   
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "AddCatalogoCajas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? AddCatalogoCajas(CatalogoCajasEntity objCatalogoCajas);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "UpdateCatalogoCajas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? UpdateCatalogoCajas(CatalogoCajasEntity objCatalogoCajas);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "DeleteCatalogoCajas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? DeleteCatalogoCajas(String BaseRemoteIp, int BaseIdUser,int? Clave);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetValidaIPMaquina", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    ValidaEntity GetValidaIPMaquina(String IpMAquina, int Clave, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneCajasSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CatalogoCajasEntity> GetObtieneCajasSucursal(int? Clv_sucursal);

    }
    }

  
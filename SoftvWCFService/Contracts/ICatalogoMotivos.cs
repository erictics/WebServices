﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts 
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICatalogoMotivos
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaMotivoCancelacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CatalogoMotivosEntity> GetBuscaMotivoCancelacion(int? Clv_MOTCAN, String MOTCAN, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUEMotivoCancelacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetNUEMotivoCancelacion(int? op, string MOTCAN);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODMotivoCancelacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetMODMotivoCancelacion(int? Clv_MOTCAN, string MOTCAN);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBORMotivoCancelacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetBORMotivoCancelacion(int? Clv_MOTCAN);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaMotivosFacturaCancelada", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CatalogoMotivosEntity> GetBuscaMotivosFacturaCancelada(int? Clv_Motivo, String Descripcion, int? Bandera, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUEMOTIVOSFACTURACANCELACION", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetNUEMOTIVOSFACTURACANCELACION(int? Bandera, string Descripcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODMOTIVOSFACTURACANCELACION", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetMODMOTIVOSFACTURACANCELACION(int? Clv_Motivo, string Descripcion, int? Bandera);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBORMOTIVOSFACTURACANCELACION", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetBORMOTIVOSFACTURACANCELACION(int? Clv_Motivo);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts 
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICatalogoReportesFac
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogoReportesFacList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CatalogoReportesFacEntity> GetCatalogoReportesFacList(int? Op);





        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogoRepCorteEspecialFacList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CatalogoReportesFacEntity> GetCatalogoRepCorteEspecialFacList(int? Op);


    }
}


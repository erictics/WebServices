﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICatalogoSeries
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCatalogoSeries", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CatalogoSeriesEntity GetDeepCatalogoSeries(int? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogoSeriesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CatalogoSeriesEntity> GetCatalogoSeriesList(String Serie, int? Clv_Vendedor, String NOMBRE, int? Op, int? ClvUsuario, int? IdCompania, int? Tipo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCatalogoSeries", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCatalogoSeries(CatalogoSeriesEntity objCatalogoSeries);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCatalogoSeries", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCatalogoSeries(CatalogoSeriesEntity objCatalogoSeries);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCatalogoSeries", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCatalogoSeries(int? Clave);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddFolios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddFolios(CatalogoSeriesEntity objCatalogoSeries);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddSerieFolios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddSerieFolios(CatalogoSeriesEntity objCatalogoSeries); 

    }
}


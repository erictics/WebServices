﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICatalogo_Ips
    {
         
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogo_IpsList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Catalogo_IpsEntity> GetCatalogo_IpsList(long? a, long? b, long? c, long? d, int? mask, String status, string NombreRed);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListCatalogo_Ips", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Catalogo_IpsEntity> GetListCatalogo_Ips(long? IdRed, int? Op);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCatalogo_Ips", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Catalogo_IpsEntity GetDeepCatalogo_Ips(long? IdRed);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUpateNombreRed", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetUpateNombreRed(Catalogo_IpsEntity ObjRed);

    }
}


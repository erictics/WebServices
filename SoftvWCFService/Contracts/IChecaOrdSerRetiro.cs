﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IChecaOrdSerRetiro 
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepChecaOrdSerRetiro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ChecaOrdSerRetiroEntity GetDeepChecaOrdSerRetiro(long? ClvOrden);


    }
}


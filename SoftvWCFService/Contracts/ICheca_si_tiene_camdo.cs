﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICheca_si_tiene_camdo 
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCheca_si_tiene_camdo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Checa_si_tiene_camdoEntity GetCheca_si_tiene_camdo(long? ClvOrden);


    }
}


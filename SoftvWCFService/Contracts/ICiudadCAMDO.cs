﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICiudadCAMDO
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCiudadCAMDOList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CiudadCAMDOEntity> GetCiudadCAMDOList(long? Contrato);

         
    }
}


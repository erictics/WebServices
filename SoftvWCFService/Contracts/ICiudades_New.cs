﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICiudades_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCiudades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Ciudades_NewEntity GetCiudades_New();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCiudades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Ciudades_NewEntity GetDeepCiudades_New();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCiudades_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Ciudades_NewEntity> GetCiudades_NewList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCiudades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCiudades_New(Ciudades_NewEntity objCiudades_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCiudades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCiudades_New(Ciudades_NewEntity objCiudades_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCiudades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCiudades_New(int? Clv_Ciudad);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddCiudades", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Ciudades_NewEntity> GetAddCiudades(String Nombre, int? Id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCiudadById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Ciudades_NewEntity> GetMuestraCiudadById(int? Clv_Ciudad);
         
    }
}


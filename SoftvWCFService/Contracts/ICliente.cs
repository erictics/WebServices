﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract] 
    public interface ICliente
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClienteEntity GetCliente(long? IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClienteEntity GetDeepCliente(long? IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ClienteEntity> GetClienteList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientePagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClientePagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientePagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClientePagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCliente(ClienteEntity objCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCliente(ClienteEntity objCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCliente(String BaseRemoteIp, int BaseIdUser, long? IdContrato);










        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddClienteL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddClienteL(ClienteEntity lstCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClienteL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateClienteL(ClienteEntity lstCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClienteDPos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateClienteDPos(ClienteEntity lstClienteDP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddReferenciaClienteL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddReferenciaClienteL(ClienteEntity lstCliente, List<ReferenciaClienteEntity> ReferenciaClienteAdd);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNotasClienteL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNotasClienteL(ClienteEntity lstCliente, List<ObservacionClienteEntity> ObservacionClienteAdd, List<RoboDeSenalEntity> RoboDeSenalAdd);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDatoFiscalCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDatoFiscalCliente(ClienteEntity lstCliente, List<DatoFiscalEntity> DatoFiscalAdd);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDatoBancarioCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDatoBancarioCliente(ClienteEntity lstCliente, List<DatoBancarioEntity> DatoBancarioAdd);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClienteContrato(string IdContrato, int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteDireccion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClienteDireccion(string Calle, string NumExt, string NumInt, int IdEstado, int IdMunicipio, int IdLocalidad, int IdColonia, int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteDP", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClienteDP(string Nombre, string ApePaterno, string ApeMaterno, string Telefono, int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientePlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClientePlaza(int IdPlaza, int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClientes(ClienteEntity lstCliente);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteIdCompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> GetClienteIdCompania(ClienteEntity lstIdCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClienteT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClienteEntity> UpdateClienteT(ClienteEntity lstCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRATIPOFACTURA_Historial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MuestraTipoFacturaHistorialEntity> GetMUESTRATIPOFACTURA_Historial();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaFacturasHistorial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BuscaFacturasHistorialEntity> GetBuscaFacturasHistorial(BuscaFacturasHistorialEntity ObjFacHis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBUSCAORDSER", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BuscaOrdenServicioEntity> GetBUSCAORDSER(BuscaOrdenServicioEntity ObjOrdSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipSerPrincipal2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipServ_NewEntity> GetMuestraTipSerPrincipal2();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBUSCAQUEJAS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BuscaQuejasLEntity> GetBUSCAQUEJAS(BuscaQuejasLEntity ObjQueja);

    }
}


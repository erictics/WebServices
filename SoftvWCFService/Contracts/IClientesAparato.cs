﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IClientesAparato
    {
         
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesAparatoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ClientesAparatoEntity> GetClientesAparatoList(long? ContratoNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClientesAparato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateClientesAparato(ClientesAparatoEntity objClientesAparato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCostoArticuloPagare", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       List<ArticuloRentaEntity> GetCostoArticuloPagare(int? op, int? tipser, int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipoServicioPagare", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipServEntity> GetMuestraTipoServicioPagare(long? Opcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEliCostosPagare", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetEliCostosPagare(int? IdCosto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraServiciosRentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Servicios_NewEntity> GetMuestraServiciosRentas(int? Clv_tipser);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaEquipoMarcado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ArticuloRentaAparatosEntity> GetConsultaEquipoMarcado(int? TipoServicio, int? TipoPaquete, long? IdCostoAP);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraMarcasAparatosPagareParaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ArticuloRentaAparatosEntity> GetMuestraMarcasAparatosPagareParaList(int? TipoServicio, int? TipoPaquete, long? IdCostoAP);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_ConsultaRel_CostoArticuloPagareConVigencia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CostoArticuloPagareVigenciasEntity> GetSP_ConsultaRel_CostoArticuloPagareConVigencia(long? IdCostoAP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_InsertaRel_CostoArticuloPagareConVigencias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       string GetSP_InsertaRel_CostoArticuloPagareConVigencias(CostoArticuloPagareVigenciasEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_ModificarRel_CostoArticuloPagareConVigencias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetSP_ModificarRel_CostoArticuloPagareConVigencias(CostoArticuloPagareVigenciasEntity obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_EliminarRel_CostoArticuloPagareConVigenciasIndividual", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_EliminarRel_CostoArticuloPagareConVigenciasIndividual(long? IdCostoAP, long? IdRelVigencia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_GuardaCostosPagareNew", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       GuardaCostoPagareEntity GetSP_GuardaCostosPagareNew(GuardaCostoPagareEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_ValidaVigencias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       string GetSp_ValidaVigencias(CostoArticuloPagareVigenciasEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_LimpiaInsertaRel_CostoArticuloPagareConAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_LimpiaInsertaRel_CostoArticuloPagareConAparatos(long? IdCostoAP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_InsertaRel_CostoArticuloPagareConAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_InsertaRel_CostoArticuloPagareConAparatos(int? op, List<ArticuloRentaAparatosEntity> obj);

    }
}


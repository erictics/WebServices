﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IClientesNoContesto
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesNoContesto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClientesNoContestoEntity GetClientesNoContesto(int? IdNoContesto);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepClientesNoContesto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClientesNoContestoEntity GetDeepClientesNoContesto(int? IdNoContesto);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesNoContestoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ClientesNoContestoEntity> GetClientesNoContestoList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesNoContestoPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClientesNoContestoEntity> GetClientesNoContestoPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesNoContestoPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ClientesNoContestoEntity> GetClientesNoContestoPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddClientesNoContesto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddClientesNoContesto(ClientesNoContestoEntity objClientesNoContesto);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClientesNoContesto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateClientesNoContesto(ClientesNoContestoEntity objClientesNoContesto);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteClientesNoContesto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteClientesNoContesto(String BaseRemoteIp, int BaseIdUser, int? IdNoContesto); 

    }
}


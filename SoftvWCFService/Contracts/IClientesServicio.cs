﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IClientesServicio
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaPapoClienteServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetValidaPapoClienteServicio(int? Clv_UnicaNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEliminaClienteServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetEliminaClienteServicio(int? Clv_UnicaNet);
         
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaTVDigCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Servicios_NewEntity> GetValidaTVDigCliente(int? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesServicioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ClientesServicioEntity> GetClientesServicioList(long? Clv_UnicaNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddClientesServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddClientesServicio(ClientesServicioEntity objClientesServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClientesServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateClientesServicio(ClientesServicioEntity objClientesServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddPqueteAdic", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddPqueteAdic(ClientesServicioEntity ObjPqueteAdic);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSaveCobroHoteles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Cobro_HotelEntity GetSaveCobroHoteles(Cobro_HotelEntity ObjCobro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCobroHotelesByContratoClvUnicanet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Cobro_HotelEntity GetCobroHotelesByContratoClvUnicanet(Cobro_HotelEntity ObjCobro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeleteCobroHoteles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetDeleteCobroHoteles(Cobro_HotelEntity ObjCobro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaIPServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaIPServicio(long? Clv_UnicaNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConIPServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IpEntity GetConIPServicio(long? Clv_UnicaNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListaIPDisp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<IpEntity> GetListaIPDisp(IpEntity ObjIP);
    }
}


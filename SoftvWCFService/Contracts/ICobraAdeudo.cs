﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICobraAdeudo
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaServioPaqueteAdic", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaServicoEntity GetValidaServioPaqueteAdic(int? Clv_UnicaNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddGrabaDevolucionAparatoL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddGrabaDevolucionAparatoL(CobraAdeudoEntity lstCobraAdeudo, List<GrabaDevolucionAparatoEntity> lstDevolucionApa);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalleCobroFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DetalleCobroFacEntity> GetDetalleCobroFac(int? Clv_UnicaNet);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetspCalculaConvenioCobroMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ConvenioCobroEntity GetspCalculaConvenioCobroMaterial(long? Clv_UnicaNet);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetspInsertaParcialidadesCobroMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetspInsertaParcialidadesCobroMaterial(ConvenioCobroEntity obj);
        

    } 
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICobraBajasOPrecobraAdeudo
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCobraBajasOPrecobraAdeudo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CobraBajasOPrecobraAdeudoEntity GetDeepCobraBajasOPrecobraAdeudo(long? IdSession, long? Contrato, int? IdMotCan);

         

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPrecobraAdeudo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CobraBajasOPrecobraAdeudoEntity GetDeepPrecobraAdeudo(long? IdSession, long? IdDetalle, int? IdMotCan);


    }
}


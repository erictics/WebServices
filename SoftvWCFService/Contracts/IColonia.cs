﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IColonia
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ColoniaEntity GetColonia(int? IdColonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ColoniaEntity GetDeepColonia(int? IdColonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ColoniaEntity> GetColoniaList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ColoniaEntity> GetColoniaPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ColoniaEntity> GetColoniaPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddColonia(ColoniaEntity objColonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateColonia(ColoniaEntity objColonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteColonia(String BaseRemoteIp, int BaseIdUser, int? IdColonia);


         





        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddColoniaL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddColoniaL(ColoniaEntity lstRelColonia, List<RelColoniaLocMunEstEntity> RelColoniaLocMunEstAdd, List<RelColoniaServicioEntity> RelColoniaServicioAdd);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateColoniaL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateColoniaL(ColoniaEntity lstRelColonia, List<RelColoniaLocMunEstEntity> RelColoniaLocMunEstAdd, List<RelColoniaServicioEntity> RelColoniaServicioAdd);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "UpdateColoniaL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? UpdateColoniaL(ColoniaEntity lstRelColonia, List<RelColoniaServicioEntity> RelColoniaServicioAdd, List<RelColoniaServicioEntity> TRelColoniaServicioDel);



    }
}


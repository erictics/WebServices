﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IColoniaCAMDO
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaCAMDOList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ColoniaCAMDOEntity> GetColoniaCAMDOList(long? Contrato, int? localidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraDescPoste", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PosteEntity> GetMuestraDescPoste(long? op);
                
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddInsertaNueDescPoste", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddInsertaNueDescPoste(long? clave, String descripcion);
         
    }
}


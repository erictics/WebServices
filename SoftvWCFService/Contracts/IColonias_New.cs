﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IColonias_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColonias_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Colonias_NewEntity GetColonias_New(int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepColonias_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Colonias_NewEntity GetDeepColonias_New(int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColonias_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Colonias_NewEntity> GetColonias_NewList(int? Clv_Colonia, String Nombre, int? Op, int? IdCompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddColonias_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddColonias_New(Colonias_NewEntity objColonias_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateColonias_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateColonias_New(Colonias_NewEntity objColonias_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteColonias_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteColonias_New();

    }
} 


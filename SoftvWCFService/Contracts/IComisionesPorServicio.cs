﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IComisionesPorServicio
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ComisionesPorServicioEntity> GetMuestraTipServ(ComisionesPorServicioEntity ObjTipoServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ComisionesPorServicioEntity> GetMuestraServicios(ComisionesPorServicioEntity ObjServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONCOMISION", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ComisionesPorServicioEntity> GetCONCOMISION(ComisionesPorServicioEntity ObjComision);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONRANGOS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ComisionesPorServicioEntity> GetCONRANGOS(ComisionesPorServicioEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUECOMISION", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNUECOMISION(ComisionesPorServicioEntity ObjComision);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBORCOMISION", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBORCOMISION(ComisionesPorServicioEntity ObjComision);
     
    }
    }

  
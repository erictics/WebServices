﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IComisionesTecnicosWeb
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesTecnicosWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ComisionesTecnicosWebEntity GetComisionesTecnicosWeb();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepComisionesTecnicosWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ComisionesTecnicosWebEntity GetDeepComisionesTecnicosWeb();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesTecnicosWebList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ComisionesTecnicosWebEntity> GetComisionesTecnicosWebList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesTecnicosWebPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ComisionesTecnicosWebEntity> GetComisionesTecnicosWebPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesTecnicosWebPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ComisionesTecnicosWebEntity> GetComisionesTecnicosWebPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddComisionesTecnicosWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddComisionesTecnicosWeb(ComisionesTecnicosWebEntity objComisionesTecnicosWeb);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateComisionesTecnicosWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateComisionesTecnicosWeb(ComisionesTecnicosWebEntity objComisionesTecnicosWeb);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteComisionesTecnicosWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteComisionesTecnicosWeb(); 

    }
}


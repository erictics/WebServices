﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IComisionesVendedoresWeb
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesVendedoresWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ComisionesVendedoresWebEntity GetComisionesVendedoresWeb();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepComisionesVendedoresWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ComisionesVendedoresWebEntity GetDeepComisionesVendedoresWeb();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesVendedoresWebList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ComisionesVendedoresWebEntity> GetComisionesVendedoresWebList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesVendedoresWebPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ComisionesVendedoresWebEntity> GetComisionesVendedoresWebPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetComisionesVendedoresWebPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ComisionesVendedoresWebEntity> GetComisionesVendedoresWebPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddComisionesVendedoresWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddComisionesVendedoresWeb(ComisionesVendedoresWebEntity objComisionesVendedoresWeb);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateComisionesVendedoresWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateComisionesVendedoresWeb(ComisionesVendedoresWebEntity objComisionesVendedoresWeb);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteComisionesVendedoresWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteComisionesVendedoresWeb(int? IdComision);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddComisionesVendedoresWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ComisionesVendedoresWebEntity> GetAddComisionesVendedoresWeb(int? Clv_Tipservicios, int? Clv_Servicio, int? RangoInicial, int? RangoFinal, Decimal? Comsion);
         
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IConfiguracion
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodoscorte", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ConfiguracionEntity> GetPeriodoscorte( int? clv_periodo, int ? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGeneralesPrincipal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         GeneralesPrincipalEntity GetGeneralesPrincipal();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetspConsultaRangosCobroMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CobroMaterialEntity> GetspConsultaRangosCobroMaterial(int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONSULTAGENERALESDESC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CONSULTAGENERALESDESCEntity GetCONSULTAGENERALESDESC(int? Clv_Periodo, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImpuestos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ImpuestosEntity GetImpuestos(int? id, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueGeneralDesconexionPagosDif", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetNueGeneralDesconexionPagosDif(int? Clv_Periodo, int? DiaCorte, int? cobra, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODIFCAPeriodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetMODIFCAPeriodos(int? Clv_Periodo, int? Habilitar, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODIFCAGENERALESDESC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetMODIFCAGENERALESDESC(CONSULTAGENERALESDESCEntity CONSULTAGENERALESDESC);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueTabla_Impuestos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetNueTabla_Impuestos(ImpuestosEntity ImpuestosEntity);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetspAgregaRangosCobroMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetspAgregaRangosCobroMaterial(CobroMaterialEntity CobroMaterial);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetspEliminaRangosCobroMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetspEliminaRangosCobroMaterial(int Idrango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModificaPromocionesGeneral", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int ? GetModificaPromocionesGeneral(int? docexcatorce, int? seisxsiete, string usuario, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGeneralesPrincipalGuardar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGeneralesPrincipalGuardar(GeneralesPrincipalEntity GeneralesPrincipalEntity);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetChangePassword", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetChangePassword(int? op,string pswanterior, string pswnueva, string usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGeneralDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GeneralDistribuidorEntity GetGeneralDistribuidor(int? clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_tecnicosDepartamentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<puestosEntity> GetMuestra_tecnicosDepartamentos(int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_TecnicosByFamili", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TecnicosEntity> GetMuestra_TecnicosByFamili(int? op, int? clv_puesto, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConPuestos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TecnicosEntity> GetConPuestos(int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultatecnicosReporte", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<puestosEntity> GetConsultatecnicosReporte(int? op, long? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueGeneralMsjTickets", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueGeneralMsjTickets(long? idcompania, string mensaje, bool? activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelOrdenesTecnicos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueRelOrdenesTecnicos(long? IdCompania, int? Clv_Puesto, int? Clv_Tecnico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRel_Tecnicos_Quejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueRel_Tecnicos_Quejas(long? IdCompania, int? Clv_Puesto, int? Clv_Tecnico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorRelOrdenesTecnicos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorRelOrdenesTecnicos(long? IdCompania, int? Clv_Puesto, int? Clv_Tecnico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorRel_Tecnicos_Quejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorRel_Tecnicos_Quejas(long? IdCompania, int? Clave_Familia, int? Clave_Tecnico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUEBonificacionCajeras", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNUEBonificacionCajeras(long? IdCompania, decimal? BonificacionMax);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetvalidaAccesoFacturacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetvalidaAccesoFacturacion(int? op, string ip);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetguardaPreferencia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetguardaPreferencia(PreferenciasEntity preferencias);

        [OperationContract] 
        [WebInvoke(Method = "*", UriTemplate = "GetDetallePreferencias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         PreferenciasEntity GetDetallePreferencias();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardalogos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<GuardalogoEntity> GetGuardalogos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFILTROS_LOGSISTEMA", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MovSistEntity> GetFILTROS_LOGSISTEMA(MovSistEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFILTROSINTERFAZ_CABLEMODEMS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<InterfazCablemodemEntity> GetFILTROSINTERFAZ_CABLEMODEMS(InterfazCablemodemEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaBonficacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        BonificacionEntity GetConsultaBonficacion();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBonificacionTipoUsarioDisList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BonificacionEntity> GetBonificacionTipoUsarioDisList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBonificacionTipoUsarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BonificacionEntity> GetBonificacionTipoUsarioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBonificacionAutorizaTipoUsarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BonificacionEntity> GetBonificacionAutorizaTipoUsarioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddBonficacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddBonficacion(BonificacionEntity ObjBonificacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddBonTipoUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddBonTipoUsuario(BonificacionEntity ObjBonificacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddBonTipoUsuarioAutoriza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddBonTipoUsuarioAutoriza(BonificacionEntity ObjBonificacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_AddTipoIde", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_AddTipoIde(IdentificacionEntity ObjIden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_DeleteTipoIde", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_DeleteTipoIde(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListaTbl_TipoIden", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<IdentificacionEntity> GetListaTbl_TipoIden();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListModuloPricipal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ModuleWebEntity> GetSoftvWeb_ListModuloPricipal(long? Clv_TipoUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListSubModuloAndModulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ModuleWebEntity> GetSoftvWeb_ListSubModuloAndModulo(long? Clv_TipoUsuario, long? ParentId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_LogClasbyIdModule", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<LogClasificacionEntity> GetSoftvWeb_LogClasbyIdModule(long? IdModule);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListLogSistema", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<LogSistemaEntity> GetSoftvWeb_ListLogSistema(LogSistemaEntity ObjLog);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_UpdateTipoIde", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_UpdateTipoIde(IdentificacionEntity ObjIden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_GetLogAnt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LogSistemaEntity GetSoftvWeb_GetLogAnt(long? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSaveBonficacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSaveBonficacion(decimal? Bonificacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConPromocionGen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PromocionEntity GetConPromocionGen(decimal? IdCompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModificaPromocionesGeneral2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModificaPromocionesGeneral2(PromocionEntity ObjPromo);

    }
}
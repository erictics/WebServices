﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IConsultaContratoNetCAdeudo
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaContratoNetCAdeudo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ConsultaContratoNetCAdeudoEntity GetConsultaContratoNetCAdeudo(int? ClvAparato, String Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidServAparatoAdeudo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidServAparatoAdeudo(long? Clv_UnicaNet);

    }
}


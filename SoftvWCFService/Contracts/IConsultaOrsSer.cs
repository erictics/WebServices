﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IConsultaOrsSer
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaOrsSerList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ConsultaOrsSerEntity> GetConsultaOrsSerList(long? IdOrden);
         

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUrlOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ConsultaOrsSerEntity> GetUrlOrdSer(long? IdOrden);

    }
}


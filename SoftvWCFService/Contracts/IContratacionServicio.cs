﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IContratacionServicio
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetContratacionServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratacionServicioEntity GetContratacionServicio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepContratacionServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratacionServicioEntity GetDeepContratacionServicio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetContratacionServicioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratacionServicioEntity> GetContratacionServicioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddContratacionServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddContratacionServicio(ContratacionServicioEntity objContratacionServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateContratacionServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateContratacionServicio(ContratacionServicioEntity objContratacionServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteContratacionServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteContratacionServicio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetStatusNet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<StatusEntity> GetStatusNet();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetStatusCableModem", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<StatusEntity> GetStatusCableModem();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Usuarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioSoftvEntity> GetMuestra_Usuarios(int? CLV_UNICANET, int? tipo_serv);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONRel_ContNet_Usuarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioSoftvEntity GetCONRel_ContNet_Usuarios(int? Clv_UnicaNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraPromotoresNet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<VendedoresLEntity> GetMuestraPromotoresNet();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConRelCteDescuento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ServicioDescuentoEntity GetConRelCteDescuento(int? Clv_UnicaNet, int? Clv_TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelCteDescuento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueRelCteDescuento(int? Clv_UnicaNet, int? Clv_TipSer, int? Descuento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorRelCteDescuento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorRelCteDescuento(int? Clv_UnicaNet, int? Clv_TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListServicioAdicTvDig", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Servicios_NewEntity> GetListServicioAdicTvDig();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTipoServClientePorContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Servicios_NewEntity> GetRelTipoServClientePorContrato(int? Clv_TipSer, long? contrato);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IContratoMaestroFac
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetContratoMaestroFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratoMaestroFacEntity GetContratoMaestroFac(long? IdContratoMaestro);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepContratoMaestroFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratoMaestroFacEntity GetDeepContratoMaestroFac(long? IdContratoMaestro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetContratoMaestroFacList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratoMaestroFacEntity> GetContratoMaestroFacList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddContratoMaestroFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddContratoMaestroFac(ContratoMaestroFacEntity objContratoMaestroFac);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateContratoMaestroFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateContratoMaestroFac(ContratoMaestroFacEntity objContratoMaestroFac);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteContratoMaestroFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteContratoMaestroFac(String BaseRemoteIp, int BaseIdUser, long? IdContratoMaestro);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBusquedaContratoMaestroFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratoMaestroFacEntity> GetBusquedaContratoMaestroFac(String RazonSocial, String NombreComercial, int? ClvCiudad, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddRelContratoMaestroContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratoMaestroFacEntity> GetAddRelContratoMaestroContrato(ContratoMaestroFacEntity objRep, List<RelContratoMaestro_ContratoSoftvEntity> lstRel);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetContratos_CS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratoMaestroFacEntity> GetContratos_CS();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelContratos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratoMaestroFacEntity> GetRelContratos(long? IdContratoMaestro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRomperRelContratoMaestroContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratoMaestroFacEntity> GetRomperRelContratoMaestroContrato(ContratoMaestroFacEntity objRep, List<RelContratoMaestro_ContratoSoftvEntity> lstRel);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLayoutFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        LayOutFacEntity GetLayoutFac(Stream data);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddUpdate", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ContratoMaestroFacEntity> GetAddUpdate(ContratoMaestroFacEntity objCM, List<ContratoMaestroFacEntity> Contratos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFacturasPorCliDePago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<FacturasPorCliDePagoEntity> GetFacturasPorCliDePago(int? Clv_Pago);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidarContratosLayout", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        DetValidaLayoutEntity GetValidarContratosLayout();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProcesaContratosLayout", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetProcesaContratosLayout(ProcesaLayoutEntity objprocesa);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCreateXmlBeforeReporteCorporativa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<String> GetCreateXmlBeforeReporteCorporativa(objPrincipal objPrincipal, RepVar_DistribuidorEntity[] distriArray, SucursalRepCorporativaEntity[] sucursalArray);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporte_General", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporte_General(RepCorporativa_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporte_ResumenIngresoSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporte_ResumenIngresoSucursal(RepCorporativa_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporte_GeneralDeVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporte_GeneralDeVentas(RepCorporativa_datosXmlEntity reportData);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursalByUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SucursalRepCorporativaEntity> GetSucursalByUsuario(int? clv_usuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCancelaPagoFacturaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratoMaestroFacEntity GetCancelaPagoFacturaMaestro(long? Clv_Pago);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaSipuedohacerPagoc", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratoMaestroFacEntity GetValidaSipuedohacerPagoc(long? ContratoMaestro, long? Clv_FacturaMaestro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaSipuedoCancelarPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratoMaestroFacEntity GetValidaSipuedoCancelarPago(long? Clv_Pago);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGeneraFacturaMaestroPrueba", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ContratoMaestroFacEntity GetGeneraFacturaMaestroPrueba(long? IdContratoMaestro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCuentaCableMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CuentaClabe> GetCuentaCableMaestro();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNotificacionContratoPorVencer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<NotificacionContratoPorValidar> GetNotificacionContratoPorVencer();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteMaestroPorVencer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ContratosPorVencer> GetReporteMaestroPorVencer(int Op, string FechaVencimiento, long? ContratoMaestro, int? IdDistribuidor);

    }
}

 
﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICrearNotaCredito
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCrearNotaCreditoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CrearNotaCreditoEntity> GetCrearNotaCreditoList(long? Clv_Factura);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCrearNotaCreditoCM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<CrearNotaCreditoEntity> GetCrearNotaCreditoCM(long? Clv_Factura); 

    }
}


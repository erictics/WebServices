﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICrearTicketTable
    {

        [WebInvoke(Method = "*", UriTemplate = "GetTicket", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        /*IEnumerable<CrearTicketTableEntity> GetCrearTicketTableList(long? Clv_Factura);*/
        String GetTicket(long? Clv_Factura);

        [WebInvoke(Method = "*", UriTemplate = "GetDeleteTicket", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetDeleteTicket(int? Op, String Ticket);

    }
}


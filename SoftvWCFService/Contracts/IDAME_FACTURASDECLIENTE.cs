﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDAME_FACTURASDECLIENTE
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDAME_FACTURASDECLIENTEList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DAME_FACTURASDECLIENTEEntity> GetDAME_FACTURASDECLIENTEList(long? ContratoMaestro, long? ClvNota);

    }
}


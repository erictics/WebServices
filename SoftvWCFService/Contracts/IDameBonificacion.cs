﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameBonificacion
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameBonificacionList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameBonificacionEntity> GetDameBonificacionList(long? Clv_queja);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddBonificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddBonificacion(DameBonificacionEntity objBonificacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteBonificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteBonificacion(long? Clv_queja, int? ClvUsuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDamePrecioBonificaciob", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameBonificacionEntity> GetDamePrecioBonificaciob(long? Clv_queja, int? dias);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImporteFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameBonificacionEntity> GetImporteFactura(long? Clv_queja, bool? BndPorMonto);


    }
}


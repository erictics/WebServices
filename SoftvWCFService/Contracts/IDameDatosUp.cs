﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameDatosUp
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosUp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDatosUpEntity GetDameDatosUp();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDameDatosUp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDatosUpEntity GetDeepDameDatosUp();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosUpList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameDatosUpEntity> GetDameDatosUpList(long? Clave, String Clv_Usuario, long? Clv_Sucursal, String IpMaquina);


    }
}


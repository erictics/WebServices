﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameDatosUsuario
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDatosUsuarioEntity GetDameDatosUsuario();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDameDatosUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDatosUsuarioEntity GetDeepDameDatosUsuario();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosUsuarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameDatosUsuarioEntity> GetDameDatosUsuarioList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosUsuarioPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DameDatosUsuarioEntity> GetDameDatosUsuarioPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDatosUsuarioPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DameDatosUsuarioEntity> GetDameDatosUsuarioPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDameDatosUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDameDatosUsuario(DameDatosUsuarioEntity objDameDatosUsuario);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDameDatosUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDameDatosUsuario(DameDatosUsuarioEntity objDameDatosUsuario);


    }
}


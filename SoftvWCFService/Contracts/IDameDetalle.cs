﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameDetalle
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDetalleEntity GetDameDetalle();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDameDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDetalleEntity GetDeepDameDetalle();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDetalleList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameDetalleEntity> GetDameDetalleList(long? Clv_Session);


    }
}


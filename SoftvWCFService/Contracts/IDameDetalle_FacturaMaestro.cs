﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameDetalle_FacturaMaestro
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDetalle_FacturaMaestroList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameDetalle_FacturaMaestroEntity> GetDameDetalle_FacturaMaestroList(long? ClvFacturaMaestro);


    }
}


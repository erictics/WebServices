﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameDetalle_FacturaMaestroFiscal
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDetalle_FacturaMaestroFiscalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameDetalle_FacturaMaestroFiscalEntity> GetDameDetalle_FacturaMaestroFiscalList(long? Clv_FacturaMaestro);

    }
}


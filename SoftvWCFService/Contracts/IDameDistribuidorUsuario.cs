﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameDistribuidorUsuario
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDistribuidorUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDistribuidorUsuarioEntity GetDameDistribuidorUsuario();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDameDistribuidorUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameDistribuidorUsuarioEntity GetDeepDameDistribuidorUsuario();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDistribuidorUsuarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameDistribuidorUsuarioEntity> GetDameDistribuidorUsuarioList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDistribuidorUsuarioPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DameDistribuidorUsuarioEntity> GetDameDistribuidorUsuarioPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDistribuidorUsuarioPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DameDistribuidorUsuarioEntity> GetDameDistribuidorUsuarioPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDameDistribuidorUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDameDistribuidorUsuario(DameDistribuidorUsuarioEntity objDameDistribuidorUsuario);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDameDistribuidorUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDameDistribuidorUsuario(DameDistribuidorUsuarioEntity objDameDistribuidorUsuario);


    }
}


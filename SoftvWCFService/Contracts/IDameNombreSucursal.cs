﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameNombreSucursal
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameNombreSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameNombreSucursalEntity GetDameNombreSucursal();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDameNombreSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameNombreSucursalEntity GetDeepDameNombreSucursal();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameNombreSucursalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameNombreSucursalEntity> GetDameNombreSucursalList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameNombreSucursalPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DameNombreSucursalEntity> GetDameNombreSucursalPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameNombreSucursalPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DameNombreSucursalEntity> GetDameNombreSucursalPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDameNombreSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDameNombreSucursal(DameNombreSucursalEntity objDameNombreSucursal);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDameNombreSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDameNombreSucursal(DameNombreSucursalEntity objDameNombreSucursal);


    }
}


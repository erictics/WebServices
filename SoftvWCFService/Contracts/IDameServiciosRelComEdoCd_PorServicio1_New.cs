﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameServiciosRelComEdoCd_PorServicio1_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameServiciosRelComEdoCd_PorServicio1_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameServiciosRelComEdoCd_PorServicio1_NewEntity> GetDameServiciosRelComEdoCd_PorServicio1_NewList(long? clv_plaza, long? id_compania, long? Clv_Estado, long? Clv_Ciudad, long? Clv_Servicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameServiciosRelComEdoCd_PorServicio2_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameServiciosRelComEdoCd_PorServicio1_NewEntity> GetDameServiciosRelComEdoCd_PorServicio2_NewList(long? clv_plaza, long? id_compania, long? Clv_Estado, long? Clv_Ciudad, long? Clv_Servicio);


    }
}


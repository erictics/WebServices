﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDameSessionW
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameSessionW", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameSessionWEntity GetDameSessionW(String Codigo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDameSessionW", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameSessionWEntity GetDeepDameSessionW(String Codigo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameSessionWList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DameSessionWEntity> GetDameSessionWList(int? Id, String Codigo);

    }
}


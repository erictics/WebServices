﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDamelasOrdenesque_GeneroFacturaAgendaOrdser
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDamelasOrdenesque_GeneroFacturaAgendaOrdser", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity GetDamelasOrdenesque_GeneroFacturaAgendaOrdser(long? ClvFactura);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaExistenciaOrdenPorFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DamelasOrdenesque_GeneroFacturaAgendaOrdserEntity GetValidaExistenciaOrdenPorFactura(long? ClvFactura);




    }
}


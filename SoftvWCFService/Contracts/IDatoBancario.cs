﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDatoBancario
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoBancario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatoBancarioEntity GetDatoBancario(long? IdContrato);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDatoBancario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatoBancarioEntity GetDeepDatoBancario(long? IdContrato);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoBancarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DatoBancarioEntity> GetDatoBancarioList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoBancarioPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DatoBancarioEntity> GetDatoBancarioPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoBancarioPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DatoBancarioEntity> GetDatoBancarioPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDatoBancario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDatoBancario(DatoBancarioEntity objDatoBancario);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDatoBancario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDatoBancario(DatoBancarioEntity objDatoBancario);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteDatoBancario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteDatoBancario(String BaseRemoteIp, int BaseIdUser, long? IdContrato);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoBancarioDeep", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatoBancarioEntity GetDatoBancarioDeep(long? IdContrato);

    }
}


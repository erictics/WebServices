﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDatoFiscal
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatoFiscalEntity GetDatoFiscal(long? IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDatoFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatoFiscalEntity GetDeepDatoFiscal(long? IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoFiscalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DatoFiscalEntity> GetDatoFiscalList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoFiscalPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DatoFiscalEntity> GetDatoFiscalPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatoFiscalPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DatoFiscalEntity> GetDatoFiscalPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDatoFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDatoFiscal(DatoFiscalEntity objDatoFiscal);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDatoFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDatoFiscal(DatoFiscalEntity objDatoFiscal);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteDatoFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteDatoFiscal(String BaseRemoteIp, int BaseIdUser, long IdContrato);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ChangeStateDatoFiscal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? ChangeStateDatoFiscal(DatoFiscalEntity objDatoFiscal, bool State);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDatosFiscales
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatosFiscales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatosFiscalesEntity GetDatosFiscales();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDatosFiscales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatosFiscalesEntity GetDeepDatosFiscales();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatosFiscalesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DatosFiscalesEntity> GetDatosFiscalesList(long? Contrato);
        [OperationContract]

        [WebInvoke(Method = "*", UriTemplate = "GetDatosFiscalesPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DatosFiscalesEntity> GetDatosFiscalesPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDatosFiscalesPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DatosFiscalesEntity> GetDatosFiscalesPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDatosFiscales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDatosFiscales(DatosFiscalesEntity objDatosFiscales);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDatosFiscales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDatosFiscales(DatosFiscalesEntity objDatosFiscales);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteDatosFiscales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteDatosFiscales(long? Contrato);

    }
}


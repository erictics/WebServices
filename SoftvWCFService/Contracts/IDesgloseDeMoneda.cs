﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDesgloseDeMoneda
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDesgloseDeMoneda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DesgloseDeMonedaEntity GetDesgloseDeMoneda(long? Consecutivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDesgloseDeMoneda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DesgloseDeMonedaEntity GetDeepDesgloseDeMoneda(long? Consecutivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDesgloseDeMonedaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DesgloseDeMonedaEntity> GetDesgloseDeMonedaList(String Cajera, int? B1000, int? B500, int? B200, int? B100, int? B50, int? B20, int? M100, int? M50, int? M20, int? M10, int? M5, int? M2, int? M1, int? M050, int? M020, int? M010, int? M005, Decimal? Cheques, Decimal? Tarjeta, Decimal? Total, String Referencia, Decimal? Gastos, Decimal? SaldoAnterior, Decimal? ImporteDolar, Decimal? TipoCambio, Decimal? TotalDolar, Decimal? Transferencia, Decimal? TarjetaCredito, Decimal? TarjetaDebito);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDesgloseDeMoneda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDesgloseDeMoneda(DesgloseDeMonedaEntity objDesgloseDeMoneda);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDesgloseDeMoneda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDesgloseDeMoneda(DesgloseDeMonedaEntity objDesgloseDeMoneda);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteDesgloseDeMoneda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteDesgloseDeMoneda(long? Consecutivo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDesgloseMoneda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DesgloseDeMonedaEntity> GetReporteDesgloseMoneda(long? Consecutivo);


    }
}


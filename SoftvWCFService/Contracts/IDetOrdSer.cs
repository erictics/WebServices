﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDetOrdSer
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DetOrdSerEntity GetDetOrdSer();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDetOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DetOrdSerEntity GetDeepDetOrdSer();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetOrdSerList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DetOrdSerEntity> GetDetOrdSerList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetOrdSerPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DetOrdSerEntity> GetDetOrdSerPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetOrdSerPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DetOrdSerEntity> GetDetOrdSerPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDetOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDetOrdSer(DetOrdSerEntity objDetOrdSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDetOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDetOrdSer(DetOrdSerEntity objDetOrdSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteDetOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteDetOrdSer(long? Clave);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDetalleContratosMaestros
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalleContratosMaestros", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DetalleContratosMaestrosEntity GetDetalleContratosMaestros();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDetalleContratosMaestros", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DetalleContratosMaestrosEntity GetDeepDetalleContratosMaestros();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalleContratosMaestrosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        objDetallecontrato GetDetalleContratosMaestrosList(long? Clv_SessionPadre);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDetalle_NotasdeCredito
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalle_NotasdeCreditoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Detalle_NotasdeCreditoEntity> GetDetalle_NotasdeCreditoList(long? Clv_Session);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalle_NotasdeCreditoVerHistorialList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Detalle_NotasdeCreditoEntity> GetDetalle_NotasdeCreditoVerHistorialList(long? Clv_NotadeCredito);

    }
}


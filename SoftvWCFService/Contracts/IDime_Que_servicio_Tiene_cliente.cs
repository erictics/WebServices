﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDime_Que_servicio_Tiene_cliente
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDime_Que_servicio_Tiene_cliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Dime_Que_servicio_Tiene_clienteEntity GetDime_Que_servicio_Tiene_cliente(long? Contrato);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDime_Que_servicio_Tiene_clienteList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Dime_Que_servicio_Tiene_clienteEntity> GetDime_Que_servicio_Tiene_clienteList(long? Contrato);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDimesihay_Conex
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDimesihay_Conex", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Dimesihay_ConexEntity GetDimesihay_Conex(long? ClvOS, int? Op);


    }
}


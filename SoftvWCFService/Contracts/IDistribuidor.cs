﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDistribuidor
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DistribuidorEntity GetDistribuidor(int? IdDistribuidor);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DistribuidorEntity GetDeepDistribuidor(int? IdDistribuidor);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidorList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DistribuidorEntity> GetDistribuidorList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidorPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DistribuidorEntity> GetDistribuidorPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidorPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DistribuidorEntity> GetDistribuidorPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDistribuidor(DistribuidorEntity objDistribuidor);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDistribuidor(DistribuidorEntity objDistribuidor);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteDistribuidor(String BaseRemoteIp, int BaseIdUser, int? IdDistribuidor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ChangeStateDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? ChangeStateDistribuidor(DistribuidorEntity objDistribuidor, bool State);

    }
}


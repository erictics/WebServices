﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDistribuidoresXml
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidoresXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DistribuidoresXmlEntity GetDistribuidoresXml();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDistribuidoresXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DistribuidoresXmlEntity GetDeepDistribuidoresXml();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidoresXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DistribuidoresXmlEntity> GetDistribuidoresXmlList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidoresXmlPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DistribuidoresXmlEntity> GetDistribuidoresXmlPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidoresXmlPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<DistribuidoresXmlEntity> GetDistribuidoresXmlPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDistribuidoresXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDistribuidoresXml(DistribuidoresXmlEntity objDistribuidoresXml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDistribuidoresXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDistribuidoresXml(DistribuidoresXmlEntity objDistribuidoresXml);

    }
}


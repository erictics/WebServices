﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDocumentosVendedoresClientes
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDocumentosVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DocumentosVendedoresClientesEntity> GetDameDocumentosVendedor();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDocumentosVendedorGrid", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DocumentosVendedoresClientesEntity> GetDameDocumentosVendedorGrid(int? clv_vendedor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaPerfilActivarChecksDocumentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DocumentosVendedoresClientesEntity GetValidaPerfilActivarChecksDocumentos(int? clv_tipousuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaDocumentoPDFVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int? GetGuardaDocumentoPDFVendedor();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDimeTipoDocumentoVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DocumentosVendedoresClientesEntity GetDimeTipoDocumentoVendedor(DocumentosVendedoresClientesEntity ObjTipoDocumento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDocumentoVendedorWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetDocumentoVendedorWeb(DocumentosVendedoresClientesEntity ObjDocumentoVendedor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDocumentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DocumentosVendedoresClientesEntity> GetDameDocumentos(int? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameOpcionesDocumentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DocumentosVendedoresClientesEntity GetDameOpcionesDocumentos(int? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoDocumentoEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DocumentosVendedoresClientesEntity GetTipoDocumentoEncuesta(int? IdDocumento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaDocumentoPDF", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int? GetGuardaDocumentoPDF();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameDocumentosContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DocumentosVendedoresClientesEntity> GetDameDocumentosContrato(int? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDimeTipoDocumento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DocumentosVendedoresClientesEntity GetDimeTipoDocumento(DocumentosVendedoresClientesEntity ObjDocumento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDocumentoClienteWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetDocumentoClienteWeb(DocumentosVendedoresClientesEntity ObjDocumentoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModificaRevisado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModificaRevisado(DocumentosVendedoresClientesEntity ObjRevisado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModificaRecibido", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModificaRecibido(DocumentosVendedoresClientesEntity ObjRecibido);

    }
}


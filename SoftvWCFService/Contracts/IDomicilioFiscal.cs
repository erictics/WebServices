﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDomicilioFiscal
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDomicilioFiscalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetDomicilioFiscalList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetMuestraEstado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetMuestraCiudad(int? ClvEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetMuestraLocalidad(int? ClvCiudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetMuestraColonia(int? ClvLocalidad);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetMuestraCalle(int? ClvColonia, int? ClvLocalidad);
        

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetDistribuidores();



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListaCiudadesPorPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DomicilioFiscalEntity> GetListaCiudadesPorPlaza(int? Clv_Plaza);




    }
}


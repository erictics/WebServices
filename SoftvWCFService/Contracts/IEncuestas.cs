﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IEncuestas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EncuestasEntity GetEncuestas(int? IdEncuesta);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EncuestasEntity GetDeepEncuestas(int? IdEncuesta);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEncuestasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EncuestasEntity> GetEncuestasList();

        

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEncuestas(EncuestasEntity objEncuestas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEncuestas(EncuestasEntity objEncuestas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteEncuestas(int? IdEncuesta);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EncuestasEntity> GetAddEncuesta(EncuestasEntity objEncuesta, List<PreguntasEntity> LstPregunta);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEditEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EncuestasEntity> GetEditEncuesta(EncuestasEntity objEncuesta, List<PreguntasEntity> LstPregunta, List<ResOpcMultsEntity> LstRespuestas);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImprimeEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetImprimeEncuesta(int? idencuesta);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEncuestaDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EncuestasEntity GetEncuestaDetalle(int? idencuesta);


    }
}


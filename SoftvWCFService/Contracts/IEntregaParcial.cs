﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IEntregaParcial
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EntregaParcialEntity GetEntregaParcial(long? Consecutivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EntregaParcialEntity GetDeepEntregaParcial(long? Consecutivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEntregaParcialList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EntregaParcialEntity> GetEntregaParcialList(String Cajera, Decimal? Importe, String NumeroCepsa, String Recibio, int? B1000, int? B500, int? B200, int? B100, int? B50, int? B20, int? M100, int? M50, int? M20, int? M10, int? M5, int? M2, int? M1, int? M050, int? M020, int? M010, int? M005, Decimal? cheques, Decimal? tarjeta, String Referencia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEntregaParcial(EntregaParcialEntity objEntregaParcial);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEntregaParcial(EntregaParcialEntity objEntregaParcial);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteEntregaParcial(long? Consecutivo);





        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEntregasParciales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EntregaParcialEntity> GetReporteEntregasParciales(long? Consecutivo);
        


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IEstado
    {
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //EstadoEntity GetEstado();
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetDeepEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //EstadoEntity GetDeepEstado();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EstadoEntity GetEstado(int? IdEstado);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EstadoEntity GetDeepEstado(int? IdEstado);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EstadoEntity> GetEstadoList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadoPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<EstadoEntity> GetEstadoPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadoPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<EstadoEntity> GetEstadoPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEstado(EstadoEntity objEstado);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEstado(EstadoEntity objEstado);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteEstado(String BaseRemoteIp, int BaseIdUser,);





        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEstado2_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEstado2_web(EstadoEntity objEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEstado2_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEstado2_web(EstadoEntity objEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteEstado2_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteEstado2_web(String BaseRemoteIp, int BaseIdUser, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadoList2_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EstadoEntity> GetEstadoList2_web();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadoList3_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<EstadoEntity> GetEstadoList3_web(int? IdPlaza);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepEstado2_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EstadoEntity GetDeepEstado2_web(int? IdEstado);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEstadoConIdPlaza_web", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEstadoConIdPlaza_web(EstadoEntity objEstado);



    }
}


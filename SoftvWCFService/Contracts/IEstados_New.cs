﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IEstados_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstados_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Estados_NewEntity GetEstados_New(int? Clv_Estado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepEstados_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Estados_NewEntity GetDeepEstados_New(int? Clv_Estado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstados_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Estados_NewEntity> GetEstados_NewList(int? opcion, String Nombre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEstados_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEstados_New(Estados_NewEntity objEstados_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEstados_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEstados_New(Estados_NewEntity objEstados_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteEstados_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteEstados_New(String Nombre, int? opcion, int? clv_estadomod);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaNomEdo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetValidaNomEdo(String Nombre, int? opcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadoByPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Estados_NewEntity> GetEstadoByPlaza(int? IdCompania);
    }
}


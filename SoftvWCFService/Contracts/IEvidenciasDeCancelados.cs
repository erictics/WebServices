﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IEvidenciasDeCancelados
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraFoliosCancelados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EvidenciasDeCanceladosEntity> GetMuestraFoliosCancelados(EvidenciasDeCanceladosEntity ObjFoliosCancelados);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameTipoEvidencia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EvidenciasDeCanceladosEntity GetDameTipoEvidencia(EvidenciasDeCanceladosEntity ObjTipoEvidencia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEvidenciaSerieFolioVEndedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetEvidenciaSerieFolioVEndedor(EvidenciasDeCanceladosEntity ObjEvidencia);
    }
}


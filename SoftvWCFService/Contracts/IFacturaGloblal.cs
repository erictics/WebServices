﻿using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IFacturaGloblal
    {   
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Distribuidor_RelUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<companiaEntity> GetMuestra_Distribuidor_RelUsuario(long? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRATIPOFACTGLO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MUESTRATIPOFACTURAEntity> GetMUESTRATIPOFACTGLO();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaFacturasGlobales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<FacturaGloblalEntity> GetBuscaFacturasGlobales(FacturaGloblalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_DameImporteFacturaGlobalDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaGloblalEntity GetSP_DameImporteFacturaGlobalDistribuidor(FacturaGloblalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Tipo_Nota", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipoNotaEntity> GetMuestra_Tipo_Nota(FacturaGloblalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDime_FacGlo_Ex", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaGloblalEntity GetDime_FacGlo_Ex(FacturaGloblalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUspDameClvCompañia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        companiaEntity GetUspDameClvCompañia();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getups_NUEVAFACTGLOBALporcompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaGloblalEntity Getups_NUEVAFACTGLOBALporcompania(FacturaGloblalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameCantidadALetra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CantidadLetraEntity GetDameCantidadALetra(CantidadLetraEntity ObjCanLet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDime_Aque_Compania_FacturarleGlobal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaGloblalEntity GetDime_Aque_Compania_FacturarleGlobal(FacturaGloblalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_DIMESIEXISTEFACTURADIGITAL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaDigitalEntity GetSP_DIMESIEXISTEFACTURADIGITAL(FacturaDigitalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameFacDig_Parte_1_Global", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaDigitalEntity GetDameFacDig_Parte_1_Global(FacturaDigitalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsp_Ed_DameDatosFacDigGlobal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaDigitalEntity GetUsp_Ed_DameDatosFacDigGlobal(FacturaDigitalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaReFacturaGlobal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ReFacturaGlobalEntity GetValidaReFacturaGlobal(ReFacturaGlobalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetupsDameFacDig_Clv_FacturaCDF", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacDigCDFEntity GetupsDameFacDig_Clv_FacturaCDF(FacDigCDFEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCANCELAFACTGLOBAL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        FacturaGloblalEntity GetCANCELAFACTGLOBAL(FacturaGloblalEntity ObjFacGlo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Compania_RelUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<companiaEntity> GetMuestra_Compania_RelUsuario(int? ClvUsuario, bool? esPorpecto);
    }
}
﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IFolio_Disponible
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFolio_DisponibleList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Folio_DisponibleEntity> GetFolio_DisponibleList(int? CLV_VENDEDOR, String SERIE, long? CONTRATO);
        
    }
}


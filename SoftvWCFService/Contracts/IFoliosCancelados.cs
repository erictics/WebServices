﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IFoliosCancelados
    {

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "Get_clv_session_Reportes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    FoliosCanceladosEntity Get_clv_session_Reportes(FoliosCanceladosEntity ObjClvSession);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetConVentasVendedoresPro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    List<FoliosCanceladosEntity> GetConVentasVendedoresPro(FoliosCanceladosEntity ObjVendedor);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetConVentasVendedoresTmp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    List<FoliosCanceladosEntity> GetConVentasVendedoresTmp(FoliosCanceladosEntity ObjVendedor);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetInsertarVendedorTmp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? GetInsertarVendedorTmp(FoliosCanceladosEntity ObjVendedor);

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetBorrarVendedorTmp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? GetBorrarVendedorTmp(FoliosCanceladosEntity ObjVendedor);
    
    }
    }

  
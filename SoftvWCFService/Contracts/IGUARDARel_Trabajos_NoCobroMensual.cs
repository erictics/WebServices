﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGUARDARel_Trabajos_NoCobroMensual
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateGUARDARel_Trabajos_NoCobroMensual", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateGUARDARel_Trabajos_NoCobroMensual(GUARDARel_Trabajos_NoCobroMensualEntity objGUARDARel_Trabajos_NoCobroMensual);

    }
}


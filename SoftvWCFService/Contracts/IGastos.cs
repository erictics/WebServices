﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGastos
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGastos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GastosEntity GetGastos();

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepClasificacionGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GastosEntity GetDeepClasificacionGasto(int? Clv_Clasificacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepCategoriaGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GastosEntity GetDeepCategoriaGasto(int? Clv_CateGasto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GastosEntity GetDeepDepartamento(int? IdDpto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepGastos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GastosEntity GetDeepGastos();

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClasificacionGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GastosEntity> GetClasificacionGasto();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCategoriaGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GastosEntity> GetCategoriaGasto();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GastosEntity> GetDepartamento();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGastosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GastosEntity> GetGastosList();

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddClasificacionGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddClasificacionGasto(GastosEntity objGastos);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCategoriaGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCategoriaGasto(GastosEntity objGastos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDepartamento(GastosEntity objDpto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddGastos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddGastos(GastosEntity objGastos);

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClasificacionGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateClasificacionGasto(GastosEntity objGastos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCategoriaGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateCategoriaGasto(GastosEntity objGastos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDepartamento(GastosEntity objDpto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateGastos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateGastos(GastosEntity objGastos);

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteClasificacionGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteClasificacionGasto(int? Clv_Clasificacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCategoriaGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCategoriaGasto(int? Clv_CateGasto);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteDepartamento(int? IdDpto);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteGastos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteGastos(String BaseRemoteIp, int BaseIdUser,);


        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddSolicitud", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddSolicitud(SolicitudGastosEntity solicitud, List<DetalleSolicitudGastosEntity> detalle);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateSolicitud", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateSolicitud(SolicitudGastosEntity solicitud);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUpdateDetSolicitud", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetUpdateDetSolicitud(List<DetalleSolicitudGastosEntity> detalle);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetAutorizaDetSolicitud", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? GetAutorizaDetSolicitud(DetalleSolicitudGastosEntity detalle);


       [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAutorizaSolicitud", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAutorizaSolicitud(int? IdSolicitud, int? IdUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetfiltrosGastos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       List<GastosEntity> filtrosGastos(int? fijo, int? Iddepartamento, int? Idclasificacion);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSolicitudById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SolicitudGastosEntity GetSolicitudById(int? id);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFiltrosSolicitud", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SolicitudGastosEntity> GetFiltrosSolicitud(SolicitudGastosEntity solicitud);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalleByIdSolicitud", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DetalleSolicitudGastosEntity> GetDetalleByIdSolicitud(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAutorizaConceptos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAutorizaConceptos(int? Idsolicitud, int? IdUsuario, List<DetalleSolicitudGastosEntity> conceptos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAplicaConceptos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAplicaConceptos(int? Idsolicitud, int? IdUsuario, List<DetalleSolicitudGastosEntity> conceptos);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteGastoPendiente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteGastoPendiente(SolicitudGastosEntity reporte);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteGastoAutorizados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteGastoAutorizados(SolicitudGastosEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteGastoAplicados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteGastoAplicados(SolicitudGastosEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaReversionAplicacionGasto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SolicitudGastosEntity GetValidaReversionAplicacionGasto(long? IdGasto, long? IdUsuario, long? IdDetalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFiltrosAutorizacionConceptos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DetalleSolicitudGastosEntity> GetFiltrosAutorizacionConceptos(int? idcompania, string fechaInicio, string fechaFin, int? clasificacion, int? fijo, string clv_usuario , int ? IdSucursal);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGeneraRefBanamexMaestro
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGeneraRefBanamexMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GeneraRefBanamexMaestroEntity GetGeneraRefBanamexMaestro(long? ContratoMaestro, Decimal? Importe);


    }
}


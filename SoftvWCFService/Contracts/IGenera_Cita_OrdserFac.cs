﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGenera_Cita_OrdserFac
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddGenera_Cita_OrdserFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddGenera_Cita_OrdserFac(Genera_Cita_OrdserFacEntity objGenera_Cita_OrdserFac);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGeneral_Sistema_II
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGeneral_Sistema_II", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        General_Sistema_IIEntity GetGeneral_Sistema_II();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Get_ActivaIP", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        General_Sistema_IIEntity Get_ActivaIP();

    }
}


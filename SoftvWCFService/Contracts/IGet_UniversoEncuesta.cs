﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGet_UniversoEncuesta
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGet_UniversoEncuestaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Get_UniversoEncuestaEntity> GetGet_UniversoEncuestaList(int? IdPlaza, int? IdTipSer, int? IdTipBusq, String Desconectado, String Instalado, String Suspendido, String Contratado, String Temporales, String Fuera, String Baja, int? IdTipFecha, String FechaI, String FechaF, int? IdUsuario, int? IdEncuesta, String NombreProceso);

    }
}


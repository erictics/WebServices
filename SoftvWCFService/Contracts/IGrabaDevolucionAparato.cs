﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGrabaDevolucionAparato
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGrabaDevolucionAparato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GrabaDevolucionAparatoEntity GetGrabaDevolucionAparato();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepGrabaDevolucionAparato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GrabaDevolucionAparatoEntity GetDeepGrabaDevolucionAparato();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGrabaDevolucionAparatoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GrabaDevolucionAparatoEntity> GetGrabaDevolucionAparatoList();


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGrabaFacturaCMaestro
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGrabaFacturaCMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GrabaFacturaCMaestroEntity GetGrabaFacturaCMaestro(long? ContratoMaestro, bool? Credito, String Cajera, String IpMaquina, int? Sucursal, int? IdCompania, int? IdDistribuidor, long? ClvSessionPadre, int? Tipo, String ToKen2, int? NoPagos, Decimal? PagoInicial,int ? IdCaja);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGraficasPreguntas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraficasPreguntasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GraficasPreguntasEntity GetGraficasPreguntasList(int? IdEncuesta, int? IdUniverso, String FechaI, String FechaF);


    }
}


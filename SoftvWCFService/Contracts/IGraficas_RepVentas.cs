﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGraficas_RepVentas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraficas_RepVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Graficas_RepVentasEntity> GetGraficas_RepVentasList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGraficas_RepVentasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Graficas_RepVentasEntity> GetGraficas_RepVentasXmlList(Graficas_RepVentasEntity Obj, List<Plaza_ReportesVentasEntity> ListPlaza, List<ServiciosWebEntity> ListPaquete, List<Vendores_ReportesVentasEntity> ListVendedor, List<Sucursales_ReportesVentasEntity> ListSucursal);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGrupoVentas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGrupoVentaIdCompaniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<GrupoVentasEntity> GetGrupoVentaIdCompaniaList(int? IdCompania, int? Clv_Usuario, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueGrupoVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GrupoVentasEntity GetNueGrupoVentas(GrupoVentasEntity ObjGrupo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModGrupoVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GrupoVentasEntity GetModGrupoVentas(GrupoVentasEntity ObjGrupo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelGrupoVentaPlazaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PlazaEntity> GetRelGrupoVentaPlazaList(int? ClvGrupo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddRelGrupoVentaPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GrupoVentasEntity GetAddRelGrupoVentaPlaza(int? ClvGrupo, int? IdCompania, int? Op);
    }
}


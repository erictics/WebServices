﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.Drawing;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGuardaEvidenciaCancelacionFolio
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateGuardaEvidenciaCancelacionFolio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int? UpdateGuardaEvidenciaCancelacionFolio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetimageToByteArray", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        byte[] GetimageToByteArray();
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGuardaMotivoCanServ
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepGuardaMotivoCanServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GuardaMotivoCanServEntity GetDeepGuardaMotivoCanServ(long? Clv_Orden, int? Clv_TipSer, long? ContratoNet, long? Clv_UnicaNet, int? Op);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorraMotivoCanServ2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GuardaMotivoCanServEntity GetBorraMotivoCanServ2(long? Clv_Orden, int? Clv_TipSer, long? ContratoNet, long? Clv_UnicaNet, int? Op);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGuardaMotivos
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaMotivosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GuardaMotivosEntity> GetGuardaMotivosList(long? ClvFactura, long? ClvMotivo, String Usuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaMotivosCM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GuardaMotivosEntity> GetGuardaMotivosCM(long? ClvFactura, long? ClvMotivo, String Usuario);



    }
}


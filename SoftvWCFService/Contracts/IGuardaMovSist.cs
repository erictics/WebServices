﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGuardaMovSist
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaMovSist", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GuardaMovSistEntity GetGuardaMovSist(int? Op, String Usuario, long? ClvOrdSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddGuardaMovSist", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddGuardaMovSist(GuardaMovSistEntity objGuardaMovSist);

    }
}


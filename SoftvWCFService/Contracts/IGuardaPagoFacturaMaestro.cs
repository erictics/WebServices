﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IGuardaPagoFacturaMaestro
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddGuardaPagoFacturaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddGuardaPagoFacturaMaestro(GuardaPagoFacturaMaestroEntity objGuardaPagoFacturaMaestro);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaPagoFacturaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<GuardaPagoFacturaMaestroEntity> GetGuardaPagoFacturaMaestro(long? Clv_FacturaMaestro, long? ContratoMaestro, String Cajera, int? Caja, String IpMaquina, int? Sucursal, Decimal? Monto, Decimal? GLOEFECTIVO2, Decimal? GLOCHEQUE2, int? GLOCLV_BANCOCHEQUE2, String NUMEROCHEQUE2, Decimal? GLOTARJETA2, int? GLOCLV_BANCOTARJETA2, String NUMEROTARJETA2, String TARJETAAUTORIZACION2, long? CLV_Nota3, Decimal? GLONOTA3, int? IdMedioPago, int? IdCompania, int? IdDistribuidor);


    }
}


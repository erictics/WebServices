﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IIPAQU
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetIPAQU", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IPAQUEntity GetIPAQU();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepIPAQU", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IPAQUEntity GetDeepIPAQU();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetIPAQUList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<IPAQUEntity> GetIPAQUList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetIPAQUPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<IPAQUEntity> GetIPAQUPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetIPAQUPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<IPAQUEntity> GetIPAQUPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddIPAQU", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddIPAQU(IPAQUEntity objIPAQU);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateIPAQU", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateIPAQU(IPAQUEntity objIPAQU);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteIPAQU", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteIPAQU(long? Clave, long? Clv_Orden, long? Contratonet, int? Clv_UnicaNet, int? Op);






        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddIPAQUD", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddIPAQUD(IPAQUEntity objIPAQUD);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteIPAQUD", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteIPAQUD(long? Clave, long? Clv_Orden, long? Contratonet, int? Clv_UnicaNet, int? Op);
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IImprime_Orden
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepImprime_Orden", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Imprime_OrdenEntity GetDeepImprime_Orden(long? ClvOrden);

    }
}


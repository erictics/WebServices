﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IInsertaRelColoniaLocalidad
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInsertaRelColoniaLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InsertaRelColoniaLocalidadEntity GetInsertaRelColoniaLocalidad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepInsertaRelColoniaLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InsertaRelColoniaLocalidadEntity GetDeepInsertaRelColoniaLocalidad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInsertaRelColoniaLocalidadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<InsertaRelColoniaLocalidadEntity> GetInsertaRelColoniaLocalidadList(int? Clv_Colonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddInsertaRelColoniaLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddInsertaRelColoniaLocalidad(InsertaRelColoniaLocalidadEntity objInsertaRelColoniaLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateInsertaRelColoniaLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateInsertaRelColoniaLocalidad(InsertaRelColoniaLocalidadEntity objInsertaRelColoniaLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteInsertaRelColoniaLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteInsertaRelColoniaLocalidad(int? Clv_Colonia, int? Clv_Localidad, int? Clv_Ciudad, String CodigoPostal);

    }
}


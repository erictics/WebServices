﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ILLamadasdeInternet
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLLamadasdeInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LLamadasdeInternetEntity GetLLamadasdeInternet();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepLLamadasdeInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LLamadasdeInternetEntity GetDeepLLamadasdeInternet();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLLamadasdeInternetList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<LLamadasdeInternetEntity> GetLLamadasdeInternetList(long? clv_llamada);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddLLamadasdeInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddLLamadasdeInternet(LLamadasdeInternetEntity objLLamadasdeInternet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateLLamadasdeInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateLLamadasdeInternet(LLamadasdeInternetEntity objLLamadasdeInternet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteLLamadasdeInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteLLamadasdeInternet(long? clv_llamada);

    }
}


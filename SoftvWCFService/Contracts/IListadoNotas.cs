﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IListadoNotas
    {
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetListadoNotas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ListadoNotasEntity GetListadoNotas(long? IdContrato);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetDeepListadoNotas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    ListadoNotasEntity GetDeepListadoNotas(long? IdContrato);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetListadoNotasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    IEnumerable<ListadoNotasEntity> GetListadoNotasList(long? IdContrato);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetListadoNotasPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    SoftvList<ListadoNotasEntity> GetListadoNotasPagedList(int page, int pageSize);

    
    }
    }

  
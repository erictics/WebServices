﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ILlamadas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLlamadas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LlamadasEntity GetLlamadas(int? IdLlamada);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepLlamadas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LlamadasEntity GetDeepLlamadas(int? IdLlamada);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLlamadasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<LlamadasEntity> GetLlamadasList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLlamadasPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<LlamadasEntity> GetLlamadasPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLlamadasPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<LlamadasEntity> GetLlamadasPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddLlamadas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddLlamadas(LlamadasEntity objLlamadas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateLlamadas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateLlamadas(LlamadasEntity objLlamadas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteLlamadas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteLlamadas(String BaseRemoteIp, int BaseIdUser, int? IdLlamada);

    }
}


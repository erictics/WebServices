﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ILocalidad
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LocalidadEntity GetLocalidad(int? IdLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LocalidadEntity GetDeepLocalidad(int? IdLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<LocalidadEntity> GetLocalidadList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<LocalidadEntity> GetLocalidadPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<LocalidadEntity> GetLocalidadPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddLocalidad(LocalidadEntity objLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateLocalidad(LocalidadEntity objLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteLocalidad(String BaseRemoteIp, int BaseIdUser, int? IdLocalidad);









        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelLocalidadL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelLocalidadL(LocalidadEntity lstRelLocalidad, List<RelLocalidadMunEstEntity> RelLocalidadMunEstAdd);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRellocalidadL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRellocalidadL(LocalidadEntity lstRelLocalidad, List<RelLocalidadMunEstEntity> RelLocalidadMunEstAdd);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadList2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<LocalidadEntity> GetLocalidadList2();


    }
}


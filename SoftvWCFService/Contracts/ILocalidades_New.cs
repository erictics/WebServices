﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ILocalidades_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Localidades_NewEntity GetLocalidades_New(int? Clv_Localidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepLocalidades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Localidades_NewEntity GetDeepLocalidades_New(int? Clv_Localidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidades_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Localidades_NewEntity> GetLocalidades_NewList(String Nombre, int? opcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddLocalidades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddLocalidades_New(Localidades_NewEntity objLocalidades_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateLocalidades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateLocalidades_New(Localidades_NewEntity objLocalidades_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteLocalidades_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteLocalidades_New(int? opcion, String Nombre, int? clvnuevo);

    }
}


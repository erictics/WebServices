﻿using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
  
    [ServiceContract]
    [AuthenticatingHeader]
    public interface ILogos
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getlogos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> Getlogos();

    }
}


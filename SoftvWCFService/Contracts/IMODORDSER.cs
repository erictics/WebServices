﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMODORDSER
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMODORDSER", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MODORDSEREntity GetDeepMODORDSER(long? ClvOrden, int? ClvTipSer, long? Contrato, String FecSol, String FecEje, String Visita1, String Visita2, String Status, int? ClvTecnico, bool? Impresa, long? ClvFactura, String Obs, String ListadeArticulos, long? TecnicoCuadrilla);

    }
}


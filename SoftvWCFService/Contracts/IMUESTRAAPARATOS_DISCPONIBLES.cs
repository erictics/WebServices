﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMUESTRAAPARATOS_DISCPONIBLES
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAAPARATOS_DISCPONIBLESList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRAAPARATOS_DISCPONIBLESEntity> GetMUESTRAAPARATOS_DISCPONIBLESList(String Op, String Trabajo, long? Contrato, int? ClvTecnico, long? ClvOrden, long? Clave);



    }
}


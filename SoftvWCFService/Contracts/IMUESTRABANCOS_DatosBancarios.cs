﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMUESTRABANCOS_DatosBancarios
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRABANCOS_DatosBancariosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRABANCOS_DatosBancariosEntity> GetMUESTRABANCOS_DatosBancariosList();


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMUESTRAMOTIVOS
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAMOTIVOSList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRAMOTIVOSEntity> GetMUESTRAMOTIVOSList(int? Op);


    }
}


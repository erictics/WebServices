﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMUESTRATIPOFACTURA_Reimpresion
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRATIPOFACTURA_ReimpresionList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRATIPOFACTURA_ReimpresionEntity> GetMUESTRATIPOFACTURA_ReimpresionList();


    }
}


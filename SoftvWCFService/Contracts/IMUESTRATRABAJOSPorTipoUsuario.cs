﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMUESTRATRABAJOSPorTipoUsuario
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRATRABAJOSPorTipoUsuarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRATRABAJOSPorTipoUsuarioEntity> GetMUESTRATRABAJOSPorTipoUsuarioList(int? ClvTipSer, int? TipoUser);




    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMUESTRATRABAJOSQUEJAS
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRATRABAJOSQUEJASList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRATRABAJOSQUEJASEntity> GetMUESTRATRABAJOSQUEJASList(int? TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRATRABAJOSQUEJAS_MasivosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRATRABAJOSQUEJASEntity> GetMUESTRATRABAJOSQUEJAS_MasivosList();

    }
}


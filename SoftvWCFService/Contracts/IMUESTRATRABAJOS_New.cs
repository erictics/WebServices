﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMUESTRATRABAJOS_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRATRABAJOS_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MUESTRATRABAJOS_NewEntity> GetMUESTRATRABAJOS_NewList(int? clv_TipSer);

    }
}


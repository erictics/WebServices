﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMedio_Softv
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMedio_SoftvList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Medio_SoftvEntity> GetMedio_SoftvList(Medio_SoftvEntity obj, List<RelUnicanetMedioEntity> Lst);


    }
}


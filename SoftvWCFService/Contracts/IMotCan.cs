﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMotCan
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMotCan", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MotCanEntity GetMotCan();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMotCan", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MotCanEntity GetDeepMotCan();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMotCanList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MotCanEntity> GetMotCanList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConMotCanList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MotCanEntity> GetConMotCanList();


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMovSist
    {
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMovSist", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMovSist(MovSistEntity objMovSist);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMovSist", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MovSistEntity> GetMovSist();


    }
}


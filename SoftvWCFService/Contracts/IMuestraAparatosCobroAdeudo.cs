﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraAparatosCobroAdeudo
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraAparatosCobroAdeudo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraAparatosCobroAdeudoEntity GetMuestraAparatosCobroAdeudo();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMuestraAparatosCobroAdeudo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraAparatosCobroAdeudoEntity GetDeepMuestraAparatosCobroAdeudo();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraAparatosCobroAdeudoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraAparatosCobroAdeudoEntity> GetMuestraAparatosCobroAdeudoList(long? IdSession, long? IdDetalle, long? Contrato);


    }
}


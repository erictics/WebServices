﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraAparatosDisponibles
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListClienteAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MuestraAparatosDisponiblesEntity> GetListClienteAparatos(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListAparatosDisponiblesByIdArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MuestraAparatosDisponiblesEntity> GetListAparatosDisponiblesByIdArticulo(long? Clv_Tecnico, long? Id_Articulo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_StatusAparatosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<StatusAparatosEntity> GetSP_StatusAparatosList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListTipoAparatosByIdArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MuestraTipoAparatoEntity> GetListTipoAparatosByIdArticulo(long? IdArticulo, long? ContratoNet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSetCambioAparato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSetCambioAparato(SP_GuardaIAPARATOSEntity ObjCambioAparato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCambioAparatoDeep", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CambioAparatoEntity GetCambioAparatoDeep(long? Clv_Orden, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraAparatosDisponiblesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraAparatosDisponiblesEntity> GetMuestraAparatosDisponiblesList(long? clv_orden, long? idArticulo, long? Clv_Tecnico);
    }
}


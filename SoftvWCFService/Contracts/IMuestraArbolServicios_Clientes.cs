﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraArbolServicios_Clientes
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraArbolServicios_ClientesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraArbolServicios_ClientesEntity> GetMuestraArbolServicios_ClientesList(long? Contrato);


    }
}


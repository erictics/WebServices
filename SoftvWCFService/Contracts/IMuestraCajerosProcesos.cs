﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraCajerosProcesos
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCajerosProcesosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraCajerosProcesosEntity> GetMuestraCajerosProcesosList(int? IdUsuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCajerosArqueoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraCajerosProcesosEntity> GetMuestraCajerosArqueoList(int? IdUsuario, int? IdPlaza);

    }
}


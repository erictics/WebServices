﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraCalleColonia
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCalleColoniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraCalleColoniaEntity> GetMuestraCalleColoniaList(int? clv_colonia);


    }
}


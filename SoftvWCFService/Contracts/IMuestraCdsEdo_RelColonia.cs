﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraCdsEdo_RelColonia
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCdsEdo_RelColoniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraCdsEdo_RelColoniaEntity> GetMuestraCdsEdo_RelColoniaList(int? clv_estado);


    }
}


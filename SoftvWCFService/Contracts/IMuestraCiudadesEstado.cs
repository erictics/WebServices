﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraCiudadesEstado
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCiudadesEstadoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraCiudadesEstadoEntity> GetMuestraCiudadesEstadoList(int? clv_estado, int? idcompania);


    }
}


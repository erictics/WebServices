﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraColoniaLocalidad
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraColoniaLocalidadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraColoniaLocalidadEntity> GetMuestraColoniaLocalidadList(int? clv_localidad);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraFacturasMaestro
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraFacturasMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraFacturasMaestroEntity GetMuestraFacturasMaestro();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMuestraFacturasMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraFacturasMaestroEntity GetDeepMuestraFacturasMaestro();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraFacturasMaestroList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraFacturasMaestroEntity> GetMuestraFacturasMaestroList();


    }
}


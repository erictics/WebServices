﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraGuaBor
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraGuaBor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraGuaBorEntity GetMuestraGuaBor();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMuestraGuaBor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraGuaBorEntity GetDeepMuestraGuaBor();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraGuaBorList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetMuestraGuaBorList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraGuaBorPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<MuestraGuaBorEntity> GetMuestraGuaBorPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraGuaBorPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<MuestraGuaBorEntity> GetMuestraGuaBorPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMuestraGuaBor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMuestraGuaBor(MuestraGuaBorEntity objMuestraGuaBor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMuestraGuaBor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMuestraGuaBor(MuestraGuaBorEntity objMuestraGuaBor);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteMuestraGuaBor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteMuestraGuaBor(String BaseRemoteIp, int BaseIdUser,);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRACABLEMODEMSDELCLI_porOpcion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetMUESTRACABLEMODEMSDELCLI_porOpcion(long? Contrato, String Status, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRACONTNET_PorOpcion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetMUESTRACONTNET_PorOpcion(long? ContratoNet, String Status, int? Op, long? ClvOS, long? ClvDetOs);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAIPAQU_porSOL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetMUESTRAIPAQU_porSOL(long? ClvDetOs, long? ClvOS);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaMotivoCanServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetGuardaMotivoCanServ(long? ClvOS, int? TipSer, long? ContratoNet, long? UnicaNet, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorraMotivoCanServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetBorraMotivoCanServ(long? ClvOS, int? TipSer, long? ContratoNet, long? UnicaNet, int? Op);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRACABLEMODEMSDELCLI_porOpcionDig", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetMUESTRACABLEMODEMSDELCLI_porOpcionDig(long? Contrato, String Status, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAContDig_PorOpcion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetMUESTRAContDig_PorOpcion(long? ContratoNet, String Status, int? Op, long? ClvOS, long? ClvDetOs);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAIPAQU_porSOLDig", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraGuaBorEntity> GetMUESTRAIPAQU_porSOLDig(long? ClvDetOs, long? ClvOS);
    }
}


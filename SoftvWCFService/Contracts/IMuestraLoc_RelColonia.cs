﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraLoc_RelColonia
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraLoc_RelColoniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraLoc_RelColoniaEntity> GetMuestraLoc_RelColoniaList(int? clv_colonia, int? clv_ciudad);


    }
}


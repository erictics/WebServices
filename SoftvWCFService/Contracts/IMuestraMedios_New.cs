﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraMedios_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraMedios_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraMedios_NewEntity> GetMuestraMedios_NewList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMedioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MuestraMedios_NewEntity> GetMedioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMuestraMedios_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraMedios_NewEntity GetDeepMuestraMedios_New(int? IdMedio);
        
    }
}


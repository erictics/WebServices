﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraRespuestas_Encuestas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraRespuestas_EncuestasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraRespuestas_EncuestasEntity> GetMuestraRespuestas_EncuestasList(int? IdPregunta);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraServiciosDelCli_porOpcion
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraServiciosDelCli_porOpcionList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraServiciosDelCli_porOpcionEntity> GetMuestraServiciosDelCli_porOpcionList(long? CONTRATO, String Status, int? OP, int? clv_tipser);


    }
}


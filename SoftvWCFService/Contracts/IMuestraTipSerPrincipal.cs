﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraTipSerPrincipal
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipSerPrincipal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraTipSerPrincipalEntity GetMuestraTipSerPrincipal();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMuestraTipSerPrincipal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraTipSerPrincipalEntity GetDeepMuestraTipSerPrincipal();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipSerPrincipalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraTipSerPrincipalEntity> GetMuestraTipSerPrincipalList();


    }
}


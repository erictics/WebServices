﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraTipSerPrincipal2
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipSerPrincipal2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraTipSerPrincipal2Entity> GetMuestraTipSerPrincipal2List();

    }
}


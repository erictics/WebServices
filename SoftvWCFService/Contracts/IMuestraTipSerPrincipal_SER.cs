﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraTipSerPrincipal_SER
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipSerPrincipal_SERList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraTipSerPrincipal_SEREntity> GetMuestraTipSerPrincipal_SERList();

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestraTipoFacturaHistorial
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipoFacturaHistorial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraTipoFacturaHistorialEntity GetMuestraTipoFacturaHistorial();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMuestraTipoFacturaHistorial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MuestraTipoFacturaHistorialEntity GetDeepMuestraTipoFacturaHistorial();
        [OperationContract]

        [WebInvoke(Method = "*", UriTemplate = "GetMuestraTipoFacturaHistorialList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MuestraTipoFacturaHistorialEntity> GetMuestraTipoFacturaHistorialList();


    }
}


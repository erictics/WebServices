﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_Compania_RelUsuario
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Compania_RelUsuarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_Compania_RelUsuarioEntity> GetMuestra_Compania_RelUsuarioList(int? ClvUsuario);

    }
}


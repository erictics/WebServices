﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_Descripcion_Articulo_2
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Descripcion_Articulo_2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_Descripcion_Articulo_2Entity> GetMuestra_Descripcion_Articulo_2List(long? ClvTecnico, int? ClvTipo, int? IdAlmacen);


    }
}


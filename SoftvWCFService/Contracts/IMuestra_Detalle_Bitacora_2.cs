﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_Detalle_Bitacora_2
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Detalle_Bitacora_2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_Detalle_Bitacora_2Entity> GetMuestra_Detalle_Bitacora_2List(long? ClvTecnico, int? IdAlmacen);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoArticuloTecnicoSinSerie_Mantenimiento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Muestra_Detalle_Bitacora_2Entity> GetTipoArticuloTecnicoSinSerie_Mantenimiento(long? ClvTecnico);
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_PlazaEnc
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_PlazaEncList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_PlazaEncEntity> GetMuestra_PlazaEncList(int? Clv_Plaza);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipSerEncList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_PlazaEncEntity> GetTipSerEncList();

    }
}


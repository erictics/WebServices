﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_PlazasPorUsuario
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_PlazasPorUsuarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_PlazasPorUsuarioEntity> GetMuestra_PlazasPorUsuarioList(int? Clv_Usuario);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_Plazas_ConfiguracionServicios
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Plazas_ConfiguracionServiciosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_Plazas_ConfiguracionServiciosEntity> GetMuestra_Plazas_ConfiguracionServiciosList();

    }
}


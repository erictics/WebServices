﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_Tecnicos_Almacen
    {

    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Tecnicos_AlmacenList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_Tecnicos_AlmacenEntity> GetMuestra_Tecnicos_AlmacenList(long? Contrato);

    
    }
    }

  
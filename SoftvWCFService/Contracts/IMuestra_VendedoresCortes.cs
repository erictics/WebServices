﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMuestra_VendedoresCortes
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_VendedoresCortesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_VendedoresCortesEntity> GetMuestra_VendedoresCortesList(long? IdUsuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraVendedores2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Muestra_VendedoresCortesEntity> GetMuestraVendedores2List(long? IdUsuario, long? Contrato);

    }
}


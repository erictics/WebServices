﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMunicipio
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MunicipioEntity GetMunicipio(int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        MunicipioEntity GetDeepMunicipio(int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MunicipioEntity> GetMunicipioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipioPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<MunicipioEntity> GetMunicipioPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipioPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<MunicipioEntity> GetMunicipioPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddMunicipio(MunicipioEntity objMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMunicipio(MunicipioEntity objMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteMunicipio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteMunicipio(String BaseRemoteIp, int BaseIdUser, int? IdMunicipio);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelEstMunL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelEstMunL(MunicipioEntity lstRelEstado, List<RelMunicipioEstEntity> RelMunicipioEstAdd);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelEstMunL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelEstMunL(MunicipioEntity lstRelEstado, List<RelMunicipioEstEntity> RelMunicipioEstAdd);





        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMunicipioList2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<MunicipioEntity> GetMunicipioList2();


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INUEPago_En_EfectivoDetMaestro
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNUEPago_En_EfectivoDetMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNUEPago_En_EfectivoDetMaestro(NUEPago_En_EfectivoDetMaestroEntity objNUEPago_En_EfectivoDetMaestro);


    }
}


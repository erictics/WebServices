﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INUEPago_En_EfectivoDetPago
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNUEPago_En_EfectivoDetPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNUEPago_En_EfectivoDetPago(NUEPago_En_EfectivoDetPagoEntity objNUEPago_En_EfectivoDetPago);

    }
}


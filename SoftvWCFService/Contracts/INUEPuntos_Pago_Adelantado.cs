﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INUEPuntos_Pago_Adelantado
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUEPuntos_Pago_Adelantado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NUEPuntos_Pago_AdelantadoEntity GetNUEPuntos_Pago_Adelantado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepNUEPuntos_Pago_Adelantado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NUEPuntos_Pago_AdelantadoEntity GetDeepNUEPuntos_Pago_Adelantado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUEPuntos_Pago_AdelantadoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<NUEPuntos_Pago_AdelantadoEntity> GetNUEPuntos_Pago_AdelantadoList();

 
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNUEPuntos_Pago_Adelantado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNUEPuntos_Pago_Adelantado(NUEPuntos_Pago_AdelantadoEntity objNUEPuntos_Pago_Adelantado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateNUEPuntos_Pago_Adelantado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateNUEPuntos_Pago_Adelantado(NUEPuntos_Pago_AdelantadoEntity objNUEPuntos_Pago_Adelantado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteNUEPuntos_Pago_Adelantado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteNUEPuntos_Pago_Adelantado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBUSCAPuntos_Pago_Adelantado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NUEPuntos_Pago_AdelantadoEntity GetBUSCAPuntos_Pago_Adelantado(int? clv_servicio, int? op);

    }
}


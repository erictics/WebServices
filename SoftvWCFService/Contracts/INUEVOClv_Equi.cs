﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INUEVOClv_Equi
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateNUEVOClv_Equi", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateNUEVOClv_Equi(NUEVOClv_EquiEntity objNUEVOClv_Equi);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONSULTAClv_Equi", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NUEVOClv_EquiEntity GetCONSULTAClv_Equi(int? Op, String Clv_txt);

    }
}


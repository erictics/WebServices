﻿using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INotaCredito
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBUSCANOTASDECREDITO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<NotaCreditoEntity> GetBUSCANOTASDECREDITO(int? op, int? Clv_NotadeCredito, string Fecha, string contrato, int? sucursal, string Nombre, int? Idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetStatusNotadeCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<StatusNotaEntity> GetStatusNotadeCredito();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalle_NotasdeCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DetalleNotaEntity> GetDetalle_NotasdeCredito(long Factura, long? Clv_NotadeCredito);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDAME_FACTURASDECLIENTE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<FacturaClienteEntity> GetDAME_FACTURASDECLIENTE(long? contrato, long clv_nota);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsulta_NotaCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NotaCreditoEntity GetConsulta_NotaCredito(long? Clv_NotadeCredito);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneDatosTicket", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatosTicket GetObtieneDatosTicket(long? Clv_NotadeCredito);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueva_NotadeCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueva_NotadeCredito(NotaCreditoEntity nota);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuarda_DetalleNotaweb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGuarda_DetalleNotaweb(long? Clv_Factura, long? Clv_Nota);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportesNotasDeCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportesNotasDeCredito(long? Clv_Nota);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorrar_Session_Notas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorrar_Session_Notas(long Clv_factura, long? Clv_Nota);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModifica_DetalleNotas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModifica_DetalleNotas(long? Clv_detalle, long? Clv_factura, bool? secobra, decimal? importe);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCANCELACIONFACTURAS_Notas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetCANCELACIONFACTURAS_Notas(long? Clv_Factura, int? op);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameMonto_NotadeCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        decimal? GetDameMonto_NotadeCredito(long? Clv_Nota, long? contrato);

    }
}
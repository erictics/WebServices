﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INotasDeCredito_ContraMaeFac
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNotasDeCredito_ContraMaeFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NotasDeCredito_ContraMaeFacEntity GetNotasDeCredito_ContraMaeFac();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepNotasDeCredito_ContraMaeFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NotasDeCredito_ContraMaeFacEntity GetDeepNotasDeCredito_ContraMaeFac();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNotasDeCredito_ContraMaeFacList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<NotasDeCredito_ContraMaeFacEntity> GetNotasDeCredito_ContraMaeFacList(long? Clv_NotadeCredito);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNotasDeCredito_ContraMaeFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int64? AddNotasDeCredito_ContraMaeFac(NotasDeCredito_ContraMaeFacEntity objNotasDeCredito_ContraMaeFac);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateNotasDeCredito_ContraMaeFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateNotasDeCredito_ContraMaeFac(NotasDeCredito_ContraMaeFacEntity objNotasDeCredito_ContraMaeFac);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteNotasDeCredito_ContraMaeFac", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteNotasDeCredito_ContraMaeFac(long? Factura, long? Clv_NotadeCredito);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBusquedaNotasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<NotasDeCredito_ContraMaeFacEntity> GetBusquedaNotasList(int? Op, long? Clv_NotadeCredito, String Fecha, long? ContratoMaestro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneDatosTicketList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ObjSucursalCajaCajero GetObtieneDatosTicketList(long? Factura);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddNotaCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<NotasDeCredito_ContraMaeFacEntity> GetAddNotaCredito(long? ContratoMaestro, long? Factura, String Fecha_deGeneracion, String Usuario_Captura, int? Usuario_Autorizo, String Fecha_Caducidad, Decimal? Monto, String Status, String Observaciones, int? Clv_Sucursal, int? Clv_suc_aplica, int? Tipo, int? Caja, long? Contrato_Aplicar);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuarda_DetalleNota", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<NotasDeCredito_ContraMaeFacEntity> GetGuarda_DetalleNota(long? Factura, long? Clv_NotadeCredito);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProcedimientoCancelar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<NotasDeCredito_ContraMaeFacEntity> GetProcedimientoCancelar(long? Factura, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCancelacionCM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NotasDeCredito_ContraMaeFacEntity GetCancelacionCM(long? Clv_NotadeCredito);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDimeSiAplicada_CM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NotasDeCredito_ContraMaeFacEntity GetDimeSiAplicada_CM(long? Clv_NotadeCredito);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCANCELA_FACTURASMAESTRA_PRINCIPAL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NotasDeCredito_ContraMaeFacEntity GetCANCELA_FACTURASMAESTRA_PRINCIPAL(long? ClvFacturaMaestro);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDesmarcaTodoNotaCreditoCM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDesmarcaTodoNotaCreditoCM(NotasDeCredito_ContraMaeFacEntity objDesmarcaTodoNotaCreditoCM);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMarcaTodoNotaCreditoCM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateMarcaTodoNotaCreditoCM(NotasDeCredito_ContraMaeFacEntity objMarcaTodoNotaCreditoCM);


    }
}


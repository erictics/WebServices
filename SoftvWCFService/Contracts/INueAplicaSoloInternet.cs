﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INueAplicaSoloInternet
    {


    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "AddNueAplicaSoloInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? AddNueAplicaSoloInternet(NueAplicaSoloInternetEntity objNueAplicaSoloInternet);
    
    }
    }

  
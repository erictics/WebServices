﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INueRelClienteTab
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelClienteTab", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NueRelClienteTabEntity GetNueRelClienteTab(long? Contrato, String tab);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INueRelOrdenUsuario
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelOrdenUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NueRelOrdenUsuarioEntity GetNueRelOrdenUsuario();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepNueRelOrdenUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NueRelOrdenUsuarioEntity GetDeepNueRelOrdenUsuario();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelOrdenUsuarioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<NueRelOrdenUsuarioEntity> GetNueRelOrdenUsuarioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelOrdenUsuarioPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<NueRelOrdenUsuarioEntity> GetNueRelOrdenUsuarioPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelOrdenUsuarioPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<NueRelOrdenUsuarioEntity> GetNueRelOrdenUsuarioPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNueRelOrdenUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNueRelOrdenUsuario(NueRelOrdenUsuarioEntity objNueRelOrdenUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateNueRelOrdenUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateNueRelOrdenUsuario(NueRelOrdenUsuarioEntity objNueRelOrdenUsuario);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteNueRelOrdenUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteNueRelOrdenUsuario(String BaseRemoteIp, int BaseIdUser,);

    }
}


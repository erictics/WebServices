﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface INuevoPago
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNuevoPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNuevoPago(NuevoPagoEntity objNuevoPago);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddPagoEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NuevoPagoEntity GetAddPagoEfectivo(long? ClvFactura, Decimal? ImporteEfectivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNuevoPago_FacMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNuevoPago_FacMaestro(NuevoPagoEntity objNuevoPago_FacMaestro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPagoEfectivo_FacMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPagoEfectivo_FacMaestro(NuevoPagoEntity objPagoEfectivo_FacMaestro);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddPagoEfectivo_FacMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NuevoPagoEntity GetAddPagoEfectivo_FacMaestro(long? ClvFactura, Decimal? ImporteEfectivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Plaza_RelUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Plaza_DistribuidoresNewEntity> GetMuestra_Plaza_RelUsuario(int? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRASucursalesPorDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SUCURSALESEntity> GetMUESTRASucursalesPorDistribuidor(int? CLVPlaza);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaEntregaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EntregaEfectivoEntity> GetBuscaEntregaEfectivo(EntregaEfectivoEntity ObjEntrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaEfectivoDisponiblePorSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        decimal? GetBuscaEfectivoDisponiblePorSucursal(int? Clv_sucursal, string Fecha);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetExisteEntregaDeEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EntregaEfectivoEntity GetExisteEntregaDeEfectivo(EntregaEfectivoEntity ObjEntrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueEntregaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EntregaEfectivoEntity GetNueEntregaEfectivo(EntregaEfectivoEntity ObjEntrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModEntregaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModEntregaEfectivo(EntregaEfectivoEntity ObjEntrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultarEntregaDeEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EntregaEfectivoEntity GetConsultarEntregaDeEfectivo(EntregaEfectivoEntity ObjEntrega);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IObservacionCliente
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObservacionCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ObservacionClienteEntity GetObservacionCliente(long? IdContrato);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepObservacionCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ObservacionClienteEntity GetDeepObservacionCliente(long? IdContrato);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObservacionClienteList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ObservacionClienteEntity> GetObservacionClienteList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObservacionClientePagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ObservacionClienteEntity> GetObservacionClientePagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObservacionClientePagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ObservacionClienteEntity> GetObservacionClientePagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddObservacionCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddObservacionCliente(ObservacionClienteEntity objObservacionCliente);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateObservacionCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateObservacionCliente(ObservacionClienteEntity objObservacionCliente);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteObservacionCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteObservacionCliente(String BaseRemoteIp, int BaseIdUser, long IdContrato);

    }
}


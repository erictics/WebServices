﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IObtieneEdoCuentaPorContrato
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneEdoCuentaPorContratoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ObtieneEdoCuentaPorContratoEntity> GetObtieneEdoCuentaPorContratoList(long? ContratoBueno);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDime_Si_DatosFiscales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ObtieneEdoCuentaPorContratoEntity GetDime_Si_DatosFiscales(long? Contrato);


    }
}


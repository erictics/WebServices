﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IObtieneInformacionEnvioCorreo
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepObtieneInformacionEnvioCorreo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ObtieneInformacionEnvioCorreoEntity GetDeepObtieneInformacionEnvioCorreo(long? Contrato);
        


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneInformacionEnvioCorreoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ObtieneInformacionEnvioCorreoEntity> GetObtieneInformacionEnvioCorreoList(long? Contrato, int? Id, String ContratoCom, long? IdEstadoCuenta);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IObtieneSucursalesEspeciales_Reimpresion
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneSucursalesEspeciales_ReimpresionList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ObtieneSucursalesEspeciales_ReimpresionEntity> GetObtieneSucursalesEspeciales_ReimpresionList();


    }
}


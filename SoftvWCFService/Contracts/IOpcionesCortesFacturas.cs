﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IOpcionesCortesFacturas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOpcionesCortesFacturasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<OpcionesCortesFacturasEntity> GetOpcionesCortesFacturasList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOpcionesCortesEspecialesFacList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<OpcionesCortesFacturasEntity> GetOpcionesCortesEspecialesFacList();


    }
}


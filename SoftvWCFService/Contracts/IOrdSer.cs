﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IOrdSer
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        OrdSerEntity GetOrdSer(long? clv_orden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        OrdSerEntity GetDeepOrdSer(long? clv_orden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetOrdSerList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<OrdSerEntity> GetOrdSerList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddOrdSer(OrdSerEntity objOrdSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateOrdSer(OrdSerEntity objOrdSer);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteOrdSer(long? clv_orden);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModOrdSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<OrdSerEntity> GetModOrdSer(long? clv_orden, int? Clv_TipSer, long? Contrato, String Fec_Sol, String Fec_Eje, String Visita1, String Visita2, String Status, int? Clv_Tecnico, bool? IMPRESA, long? Clv_FACTURA, String Obs, String ListadeArticulos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteOrdenServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteOrdenServicio(ReporteOrdenServicioEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_ValidaGuardaOrdSerAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetSP_ValidaGuardaOrdSerAparatos(long? CLV_ORDEN, string OPCION, string STATUS, int? OP2, long? Clv_Tecnico);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_StatusAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<StatusAparatosEntity> GetSP_StatusAparatos();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getsp_validaEliminarOrdenser", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        sp_validaEliminarOrdenEntity Getsp_validaEliminarOrdenser(sp_validaEliminarOrdenEntity sp_validaEliminarOrdenEntity);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_ObtenTipoMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Softv_ObtenTipoMaterialEntity GetSoftv_ObtenTipoMaterial(Softv_ObtenTipoMaterialEntity Softv_ObtenTipoMaterialEntity);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameCitaOrdenQueja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameCitaOrdenQuejaEntity GetDameCitaOrdenQueja(long? clv_queja_orden, int? opcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getsp_BorraArticulosAsignados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Getsp_BorraArticulosAsignados(long? clv_orden);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneTap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TapEntity> GetObtieneTap(long? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtieneNap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TapEntity> GetObtieneNap(long? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardarRelClienteTap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGuardarRelClienteTap(long? CONTRATO, string TAP, int? idtap);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardarRelClienteNap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGuardarRelClienteNap(long? CONTRATO, string TAP, int? IDTAP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTieneTapNap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClienteTapNapEntity GetTieneTapNap(ClienteTapNapEntity ObjClienteTapNap);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConTapCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TapEntity GetConTapCliente(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTraerNap", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TapEntity GetTraerNap(int? contrato);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaHoraOrden", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGuardaHoraOrden(long? Clv_orden, string horaInicio, string horaFin, int? opcion);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONCONEX", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetCONCONEX(long? Clave, long? Clv_Orden, long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueConex", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueConex(long? Clave, long? Clv_Orden, long? Contrato, int? ExtAdic);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODCONEX", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetMODCONEX(long? Clave, long? Clv_Orden, long? Contrato, int? ExtAdic);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUECANEX", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNUECANEX(ExtencionEntity ObjExt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameExteciones_Cli", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ExtencionEntity GetDameExteciones_Cli(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCONCANEX", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ExtencionEntity GetCONCANEX(ExtencionEntity ObjExt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMODCANEX", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetMODCANEX(ExtencionEntity ObjExt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBORDetOrdSer_INTELIGENTE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBORDetOrdSer_INTELIGENTE(long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBORCANEX", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBORCANEX(ExtencionEntity ObjExt);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConTecnicoAgenda", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TecnicosEntity GetConTecnicoAgenda(long? Clv, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetResumenOrdenes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResumenOrdSerEntity GetResumenOrdenes(long? IdCompania, long? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConHIHF_OrdenQeja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        HoraOrdQueEntity GetConHIHF_OrdenQeja(string Op, long? Clave);

    }
}




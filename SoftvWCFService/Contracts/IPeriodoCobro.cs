﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPeriodoCobro
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodoCobro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PeriodoCobroEntity GetPeriodoCobro(int? IdPeriodo);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPeriodoCobro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PeriodoCobroEntity GetDeepPeriodoCobro(int? IdPeriodo);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodoCobroList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PeriodoCobroEntity> GetPeriodoCobroList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodoCobroPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PeriodoCobroEntity> GetPeriodoCobroPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodoCobroPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PeriodoCobroEntity> GetPeriodoCobroPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPeriodoCobro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPeriodoCobro(PeriodoCobroEntity objPeriodoCobro);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePeriodoCobro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePeriodoCobro(PeriodoCobroEntity objPeriodoCobro);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeletePeriodoCobro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeletePeriodoCobro(String BaseRemoteIp, int BaseIdUser, int? IdPeriodo);

    }
}


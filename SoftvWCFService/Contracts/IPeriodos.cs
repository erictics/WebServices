﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPeriodos
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PeriodosEntity GetPeriodos();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPeriodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PeriodosEntity GetDeepPeriodos();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PeriodosEntity> GetPeriodosList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodosPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PeriodosEntity> GetPeriodosPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodosPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PeriodosEntity> GetPeriodosPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPeriodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPeriodos(PeriodosEntity objPeriodos);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePeriodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePeriodos(PeriodosEntity objPeriodos);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeletePeriodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeletePeriodos(String BaseRemoteIp, int BaseIdUser,);

    }
}


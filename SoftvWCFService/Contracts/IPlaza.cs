﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPlaza
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PlazaEntity GetPlaza(int? IdPlaza);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PlazaEntity GetDeepPlaza(int? IdPlaza);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PlazaEntity> GetPlazaList(long? IdUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PlazaEntity> GetPlazaPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PlazaEntity> GetPlazaPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPlaza(DireccionplazaAlmacenEntity objPlaza);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePlaza(DireccionplazaAlmacenEntity objPlaza);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeletePlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeletePlaza(String BaseRemoteIp, int BaseIdUser, int? IdPlaza);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "ChangeStatePlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? ChangeStatePlaza(PlazaEntity objPlaza, bool State);





        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPlazaL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPlazaL(PlazaEntity lstRelPlazaMunEst, List<RelPlazaEstMunEntity> RelPlazaEstMunAdd);

        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePlazaL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePlazaL(PlazaEntity lstRelPlazaMunEst, List<RelPlazaEstMunEntity> RelPlazaEstMunAdd);


        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "UpdatePlazaL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? UpdatePlazaL(PlazaEntity lstRelPlazaMunEst, List<RelPlazaEstMunEntity> RelPlazaEstMunAdd, List<RelPlazaEstMunEntity> RelPlazaEstMunDel);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtendatosPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DireccionplazaAlmacenEntity GetObtendatosPlaza(int? idcompania);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgregaEliminaRelCompaniaCiudad2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelCompaniaCiudadEntity> GetAgregaEliminaRelCompaniaCiudad2(int? opcion, int? idcompania, int? clv_ciudad, int? clv_estado);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Ciudad_RelCompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CiudadCAMDOEntity> GetMuestra_Ciudad_RelCompania(int? idcompania, int? clvestado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraEstadosFrmCompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstadoEntity> GetMuestraEstadosFrmCompania(int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBrwMuestraCompanias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PlazaEntity> GetBrwMuestraCompanias(int? opcion, string razon, int? idcompania);






    }
}


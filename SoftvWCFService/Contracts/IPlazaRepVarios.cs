﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPlazaRepVarios
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaRepVarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PlazaRepVariosEntity GetPlazaRepVarios();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPlazaRepVarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PlazaRepVariosEntity GetDeepPlazaRepVarios();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaRepVariosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PlazaRepVariosEntity> GetPlazaRepVariosList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaRepVariosPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PlazaRepVariosEntity> GetPlazaRepVariosPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaRepVariosPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PlazaRepVariosEntity> GetPlazaRepVariosPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPlazaRepVarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPlazaRepVarios(PlazaRepVariosEntity objPlazaRepVarios);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePlazaRepVarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePlazaRepVarios(PlazaRepVariosEntity objPlazaRepVarios);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeletePlazaRepVarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeletePlazaRepVarios(String BaseRemoteIp, int BaseIdUser,);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "ChangeStatePlazaRepVarios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? ChangeStatePlazaRepVarios(PlazaRepVariosEntity objPlazaRepVarios, bool State);

    }
}


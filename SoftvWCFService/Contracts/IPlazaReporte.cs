﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPlazaReporte
    {

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetPlazaReporteList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //IEnumerable<PlazaReporteEntity> GetPlazaReporteList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaDList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PlazaReporteEntity> GetPlazaDList(RepDistribuidoresEntity objDis, List<RepPlazaEntity> LstDis);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPlaza_DistribuidoresNew
    {    


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPlaza_DistribuidoresNew", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPlaza_DistribuidoresNew(Plaza_DistribuidoresNewEntity objPlaza_DistribuidoresNew);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePlaza_DistribuidoresNew", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePlaza_DistribuidoresNew(Plaza_DistribuidoresNewEntity objPlaza_DistribuidoresNew);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeletePlaza_DistribuidoresNew", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeletePlaza_DistribuidoresNew();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getdatoscomerciales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Plaza_DistribuidoresNewEntity> Getdatoscomerciales(int? Clv_plaza);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_TipoDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipodistribuidorEntity> GetMuestra_TipoDistribuidor();




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlaza_DistribuidoresNew", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Plaza_DistribuidoresNewEntity> GetPlaza_DistribuidoresNew(int? Clv_plaza, string Nombre);

    }
}


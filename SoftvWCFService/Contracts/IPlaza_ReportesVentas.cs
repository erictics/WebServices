﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPlaza_ReportesVentas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlaza_ReportesVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Plaza_ReportesVentasEntity GetPlaza_ReportesVentas();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPlaza_ReportesVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Plaza_ReportesVentasEntity GetDeepPlaza_ReportesVentas();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlaza_ReportesVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Plaza_ReportesVentasEntity> GetPlaza_ReportesVentasList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlaza_ReportesVentasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Plaza_ReportesVentasEntity> GetPlaza_ReportesVentasXmlList(Plaza_ReportesVentasEntity objPlaza, List<Muestra_PlazasPorUsuarioEntity> LstDis);

    }
}


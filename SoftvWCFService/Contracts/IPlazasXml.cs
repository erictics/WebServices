﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPlazasXml
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazasXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PlazasXmlEntity GetPlazasXml();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPlazasXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PlazasXmlEntity GetDeepPlazasXml();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PlazasXmlEntity> GetPlazasXmlList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazasXmlPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PlazasXmlEntity> GetPlazasXmlPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazasXmlPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PlazasXmlEntity> GetPlazasXmlPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPlazasXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPlazasXml(PlazasXmlEntity objPlazasXml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePlazasXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePlazasXml(PlazasXmlEntity objPlazasXml);


    }
}


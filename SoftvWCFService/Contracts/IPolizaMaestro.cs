﻿using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPolizaMaestro
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtienePolizasMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PolizasMaestroEntity> GetObtienePolizasMaestro(FiltroPolizaEntity filtros);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEliminaPoliza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetEliminaPoliza(FiltroPolizaEntity filtros);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetallesPolizaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DetallePolizaEntity> GetDetallesPolizaMaestro(FiltroPolizaEntity filtros);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGeneraNuevaPolizaMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PolizasMaestroEntity GetGeneraNuevaPolizaMaestro(FiltroPolizaEntity filtros);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPolizaTxt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetPolizaTxt(FiltroPolizaEntity filtros);
    }
}
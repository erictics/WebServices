﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IPreguntas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PreguntasEntity GetPreguntas(int? IdPregunta);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PreguntasEntity GetDeepPreguntas(int? IdPregunta);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPreguntasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PreguntasEntity> GetPreguntasList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPreguntasPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PreguntasEntity> GetPreguntasPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPreguntasPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<PreguntasEntity> GetPreguntasPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddPreguntas(PreguntasEntity objPreguntas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatePreguntas(PreguntasEntity objPreguntas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeletePreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeletePreguntas(String BaseRemoteIp, int BaseIdUser, int? IdPregunta);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddPregunta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PreguntasEntity> GetAddPregunta(PreguntasEntity objPregunta, List<ResOpcMultsEntity> ResOpcMult);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEditPregunta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<PreguntasEntity> GetEditPregunta(PreguntasEntity objPregunta, List<ResOpcMultsEntity> ResOpcMult);


    }
}


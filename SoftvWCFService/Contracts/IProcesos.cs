﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IProcesos
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRADevolucionAparatosAlmacen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DevolucionAparatosEntity> GetMUESTRADevolucionAparatosAlmacen(int? OP, long? CLV_ORDEN, string SERIE, int? FOLIO, string TIPOAPARATO, string MACCABLEMODEM, string CLV_USUARIO, int? CLV_UsuarioLogin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPROCESODevolucionAparatosAlmacen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        int? GetPROCESODevolucionAparatosAlmacen(long? CLV_ORDEN, string TIPOAPARATO, long? CLV_CABLEMODEM, string MACCABLEMODEM, bool ? ESTADOAPARATO, string USUARIO, string PROVIENE, string MARCA);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraServCteReset", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DevolucionAparatosEntity> GetMuestraServCteReset(long? Contrato, int? Clv_TipSer, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetResetServCte", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetResetServCte(long? Contrato, long? Clv_CableModem, int? Clv_TipSer,long ? clv_unicanet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraServiciosPrueba", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Servicios_NewEntity> GetMuestraServiciosPrueba(long? Clv_TipSer, long? Clv_Servicio, long? Clv_Unicanet);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRACablemodesDelClientePrueba", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<AparatosPruebaEntity> GetMUESTRACablemodesDelClientePrueba(long? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNUEtblPruebaInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNUEtblPruebaInternet(long? clv_unicanet, long? clv_servicioant, long? clv_servicionue, string fechaInicio, string fechafin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConCambioServCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         List<CambioServicioEntity> GetConCambioServCliente(long? clave, long? Contrato, string Nombre, int? Clv_TipSer, int? Op, int? idcompania, int? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspDameClientesActivos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CLIENTES_NewEntity> GetuspDameClientesActivos(long? contrato, string nombre, string calle, string numero, string ciudad, int? op, int? clvColonia, int? idcompania, string SETUPBOX, string TARJETA, int? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosClienteActuales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ServiciosClienteActualesEntity GetServiciosClienteActuales(long? contrato, int? clv_servicio);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosClientePosibles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ServiciosClienteActualesEntity GetServiciosClientePosibles(ServiciosClienteEntity parametros);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaCambioServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResponseEntity GetValidaCambioServicio(long? contrato, long? Clv_ServOld, long? Clv_ServNew);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCambiaServCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetCambiaServCliente(long? contrato, long? contratoNet, int? clvtipser, int? Clv_ServOld, int? Clv_ServNew, decimal? Monto, long? Clv_Session, long? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInserta_Precio_Art_Acom", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetInserta_Precio_Art_Acom(long? clv_articulo, decimal? precio, long? cant, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEliminar_Art_Acom", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetEliminar_Art_Acom(long? No_articulo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestra_Art_Acom", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ArticuloEntity> GetMuestra_Art_Acom(int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValida_Art_Acom", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ArticuloEntity GetValida_Art_Acom(int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_MuestraDistribuidorCartera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Plaza_DistribuidoresNewEntity> GetSp_MuestraDistribuidorCartera(int? Clv_TipSer, long? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_MuestraCompaniaCartera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<companiaEntity> GetSp_MuestraCompaniaCartera(List<DistribuidorCarteraEntity> ObjDistribuidor, int? Clv_TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_MuestraPeriodoCartera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PeriodosEntity> GetSp_MuestraPeriodoCartera(List<companiaEntity> ObjCompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGenera_CarteraDIG_PorPeriodo_ALL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GenCarteraEntity GetGenera_CarteraDIG_PorPeriodo_ALL(GenCarteraEntity ObjCartera, List<DistribuidorCarteraEntity> ObjDis, List<companiaEntity> ObjCom, List<PeriodosEntity> ObjPer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_QMEstados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMCiudadesEntity> GetSoftvWeb_QMEstados(List<MQEstadosEntity> ListEstados, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_DIMECUANTASEXT_TIENES", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_DIMECUANTASEXT_TIENES(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_VALIDAEXTECIONESFAC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_VALIDAEXTECIONESFAC(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDAMEUTLIMODIA", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetDAMEUTLIMODIA();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameFilaPreDetFacturas_2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        decimal? GetDameFilaPreDetFacturas_2(long? Clv_Session, long? Clv_Detalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameBonificaciónCompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        float? GetDameBonificaciónCompania(int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModFilaPreDetFacturasBonificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModFilaPreDetFacturasBonificacion(long? Clv_Session, long? Clv_Detalle, int? DiasBonifica, decimal? importeBonifica);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInsPreMotivoBonificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetInsPreMotivoBonificacion(long? Clv_Session, long? Clv_Detalle, decimal? Monto, string Motivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaBonOfrece", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaBonOfrece(long? Clv_TipoUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConCompaniaByContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        companiaEntity GetConCompaniaByContrato(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaUserBonAutoriza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaUserBonAutoriza(long? id,string Clv_Usuario, string Pasaporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getsoftvweb_previewServiciosPorContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DetOrdSerEntity> Getsoftvweb_previewServiciosPorContrato(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetdameImporteBonificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         decimal? GetdameImporteBonificacion(long? Clv_Session);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaBonAutoriza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaBonAutoriza(long? Clv_TipoUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorCambioServCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResponseEntity GetBorCambioServCliente(long? Clave, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetbuscaClientesFueraArea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CLIENTES_NewEntity> GetbuscaClientesFueraArea(long? ClvUsuario, int? IdCompania, int? ContratoComp, string Nombre, string SegundoNombre, string Apellido_Paterno, string Apellido_Materno, int? Clv_Estado, string Ciudad, int? Clv_Localidad, int? clv_colonia, string Calle, int? NUMERO, int? Num_Int);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInfoContratoContratado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CLIENTES_NewEntity GetInfoContratoContratado(int? Op, int? IdContrato, string ContratoCompania, int? idUsuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosContratoFueraArea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ServiciosClienteEntity> GetServiciosContratoFueraArea(int? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetafectaContratoFueraArea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetafectaContratoFueraArea(List<long> Claves);
    }
}


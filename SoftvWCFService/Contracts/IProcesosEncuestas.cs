﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IProcesosEncuestas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProcesosEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProcesosEncuestasEntity GetProcesosEncuestas(int? IdProcesoEnc);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepProcesosEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProcesosEncuestasEntity GetDeepProcesosEncuestas(int? IdProcesoEnc);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProcesosEncuestasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ProcesosEncuestasEntity> GetProcesosEncuestasList();
       
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddProcesosEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddProcesosEncuestas(ProcesosEncuestasEntity objProcesosEncuestas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateProcesosEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateProcesosEncuestas(ProcesosEncuestasEntity objProcesosEncuestas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteProcesosEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteProcesosEncuestas(String BaseRemoteIp, int BaseIdUser, int? IdProcesoEnc);

    }
}


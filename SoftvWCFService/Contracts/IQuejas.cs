﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IQuejas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        QuejasEntity GetQuejas();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepQuejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        QuejasEntity GetDeepQuejas();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuejasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<QuejasEntity> GetQuejasList(long? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuejasPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<QuejasEntity> GetQuejasPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuejasPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<QuejasEntity> GetQuejasPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddQuejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddQuejas(QuejasEntity objQuejas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateQuejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateQuejas(QuejasEntity objQuejas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteQuejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteQuejas(long? Clv_Queja);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSiHayQueja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<SiHayQuejasEntity> GetSiHayQueja(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaCortesiaQueja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaCortesiaQueja(long? Contrato);

    }
}


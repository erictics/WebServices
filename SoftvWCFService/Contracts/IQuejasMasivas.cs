﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IQuejasMasivas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuejasMasivas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        QuejasMasivasEntity GetQuejasMasivas(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepQuejasMasivas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        QuejasMasivasEntity GetDeepQuejasMasivas(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetTipoQM(long? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuejasMasivasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<QuejasMasivasEntity> GetQuejasMasivasList(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddQuejasMasivas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddQuejasMasivas(QuejasMasivasEntity objQuejasMasivas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateQuejasMasivas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateQuejasMasivas(QuejasMasivasEntity objQuejasMasivas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteQuejasMasivas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteQuejasMasivas(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddQuejaMasiva", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<QuejasMasivasEntity> GetAddQuejaMasiva(QuejasMasivasEntity Obj, List<Rel_QuejaMasiva_EdoEntity> LstQuejaEdo, List<Rel_QuejaMasiva_CdEntity> LstQuejaCd, List<Rel_QuejaMasiva_LocEntity> LstQuejaLoc, List<Rel_QuejaMasiva_ColoniaEntity> LstQuejaCol, List<Rel_QuejaMasiva_CalleEntity> LstQuejaCalle, List<Rel_QuejaMasiva_ContratoCliEntity> LstQuejaCliente, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMClientesEntity> GetSoftvWeb_ListaQMClientes(long? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMEstados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MQEstadosEntity> GetSoftvWeb_ListaQMEstados(long? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMCiudades", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMCiudadesEntity> GetSoftvWeb_ListaQMCiudades(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMLocalidades", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMLocalidadEntity> GetSoftvWeb_ListaQMLocalidades(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMColonias", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMColoniaEntity> GetSoftvWeb_ListaQMColonias(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMCalles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMCalleEntity> GetSoftvWeb_ListaQMCalles(int? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_UpdateContratoQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftvWeb_UpdateContratoQM(long? Contrato, long? Clv_Queja, int? OP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_UpdateAreaQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSoftvWeb_UpdateAreaQM(long? Clv_Queja, int? OP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMEstadoDis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MQEstadosEntity> GetSoftvWeb_ListaQMEstadoDis(long? Clv_Queja, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMCiudadDis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMCiudadesEntity> GetSoftvWeb_ListaQMCiudadDis(long? Clv_Queja, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMLocalidadDis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMLocalidadEntity> GetSoftvWeb_ListaQMLocalidadDis(long? Clv_Queja, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMColoniaDis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMColoniaEntity> GetSoftvWeb_ListaQMColoniaDis(long? Clv_Queja, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvWeb_ListaQMCallesDis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<QMCalleEntity> GetSoftvWeb_ListaQMCallesDis(long? Clv_Queja, long? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaServCliQM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEntity GetValidaServCliQM(long? Contrato);
        
    }
}


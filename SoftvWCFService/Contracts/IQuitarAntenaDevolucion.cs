﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IQuitarAntenaDevolucion
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteQuitarAntenaDevolucion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteQuitarAntenaDevolucion(long? IdSession, int? IdTipSer);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteBorraCobraAdeudo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteBorraCobraAdeudo(long? IdSession);



    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IQuitarDetalle
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuitarDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        QuitarDetalleEntity GetQuitarDetalle();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepQuitarDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        QuitarDetalleEntity GetDeepQuitarDetalle();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetQuitarDetalleList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<QuitarDetalleEntity> GetQuitarDetalleList(long? IdSession, long? ClvDetalle, int? IdTipoCli, long? Contrato);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRELCLIBANCO
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRELCLIBANCO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RELCLIBANCOEntity GetRELCLIBANCO();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRELCLIBANCO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RELCLIBANCOEntity GetDeepRELCLIBANCO();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRELCLIBANCOList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RELCLIBANCOEntity> GetRELCLIBANCOList(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRELCLIBANCO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRELCLIBANCO(RELCLIBANCOEntity objRELCLIBANCO);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRELCLIBANCO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRELCLIBANCO(RELCLIBANCOEntity objRELCLIBANCO);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRELCLIBANCO", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRELCLIBANCO(long? Contrato);

    }
}


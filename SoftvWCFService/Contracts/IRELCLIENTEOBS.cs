﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRELCLIENTEOBS
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRELCLIENTEOBS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RELCLIENTEOBSEntity GetRELCLIENTEOBS(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRELCLIENTEOBS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RELCLIENTEOBSEntity GetDeepRELCLIENTEOBS(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRELCLIENTEOBSList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RELCLIENTEOBSEntity> GetRELCLIENTEOBSList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRELCLIENTEOBS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRELCLIENTEOBS(RELCLIENTEOBSEntity objRELCLIENTEOBS);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRELCLIENTEOBS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRELCLIENTEOBS(RELCLIENTEOBSEntity objRELCLIENTEOBS);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRELCLIENTEOBS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRELCLIENTEOBS(long? Contrato);

    }
}


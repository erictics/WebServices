﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IREL_TARIFADOS_SERVICIOS_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetREL_TARIFADOS_SERVICIOS_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        REL_TARIFADOS_SERVICIOS_NewEntity GetREL_TARIFADOS_SERVICIOS_New(int? CLV_LLAVE, int? Clv_TipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepREL_TARIFADOS_SERVICIOS_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        REL_TARIFADOS_SERVICIOS_NewEntity GetDeepREL_TARIFADOS_SERVICIOS_New(int? CLV_LLAVE, int? Clv_TipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetREL_TARIFADOS_SERVICIOS_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<REL_TARIFADOS_SERVICIOS_NewEntity> GetREL_TARIFADOS_SERVICIOS_NewList(int? CLV_SERVICIO, int? OP, int? Clv_TipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddREL_TARIFADOS_SERVICIOS_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddREL_TARIFADOS_SERVICIOS_New(REL_TARIFADOS_SERVICIOS_NewEntity objREL_TARIFADOS_SERVICIOS_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateREL_TARIFADOS_SERVICIOS_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateREL_TARIFADOS_SERVICIOS_New(REL_TARIFADOS_SERVICIOS_NewEntity objREL_TARIFADOS_SERVICIOS_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteREL_TARIFADOS_SERVICIOS_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteREL_TARIFADOS_SERVICIOS_New(int? CLV_LLAVE, int? Clv_TipoCliente);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddREL_TARIFADOS_SERVICIOSAll_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddREL_TARIFADOS_SERVICIOSAll_New(REL_TARIFADOS_SERVICIOS_NewEntity objREL_TARIFADOS_SERVICIOS_New);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateREL_TARIFADOS_SERVICIOSAll_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateREL_TARIFADOS_SERVICIOSAll_New(REL_TARIFADOS_SERVICIOS_NewEntity objREL_TARIFADOS_SERVICIOS_New);


    }
}


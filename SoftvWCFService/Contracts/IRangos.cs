﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRangos
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraCatalogoDeRangos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RangosEntity> GetMuestraCatalogoDeRangos(RangosEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspChecaSiGuardaRango", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetuspChecaSiGuardaRango(RangosEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueCatalogoDeRangos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueCatalogoDeRangos(RangosEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaUpdateRango", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetValidaUpdateRango(RangosEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModCatalogoDeRangos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetModCatalogoDeRangos(RangosEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaRangosAEliminar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetValidaRangosAEliminar(RangosEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorCatalogoDeRangos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorCatalogoDeRangos(RangosEntity ObjRango);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConCatalogoDeRangos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RangosEntity GetConCatalogoDeRangos(RangosEntity ObjRango);

    }
}


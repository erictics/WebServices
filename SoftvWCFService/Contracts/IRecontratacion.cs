﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRecontratacion
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Get_uspConsultaColoniasPorUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Colonias_NewEntity> Get_uspConsultaColoniasPorUsuario(int? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Get_uspBusCliPorContratoSeparadoEnBaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CLIENTES_NewEntity> Get_uspBusCliPorContratoSeparadoEnBaja(CLIENTES_NewEntity ObjCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInfoContratoEnBaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CLIENTES_NewEntity GetInfoContratoEnBaja(int? Op, int? IdContrato, string ContratoCompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspCobraAdeudo_CuandoRecontrata_Msj", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CobraAdeudoEntity GetuspCobraAdeudo_CuandoRecontrata_Msj(int? IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDameClv_Session", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetDameClv_Session();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosEnBaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ClientesServicioEntity> GetServiciosEnBaja(int? IdContrato, int? Clv_TipSer, int? ClvSession);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddServiciosEnBaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddServiciosEnBaja(RecontratacionEntity ObjRecontracion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListaAparatosEnBaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<AparatosBajaEntity> GetListaAparatosEnBaja(int? ClvTipoServ, int? Clv_Unicanet, int? IdMedio, int? IdRecon, int? ClvSession);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddPaqueteAdicionalEnBaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddPaqueteAdicionalEnBaja(RecontratacionEntity ObjRecontracion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddApararoEnBaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddApararoEnBaja(List<RecontratacionEntity> ObjRecontratacion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetArbolRecontratacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ArbolServiciosRecontratacionEntity> GetArbolRecontratacion(int? ClvSession);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGrabaReContratacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGrabaReContratacion(int? Contrato, string Clv_Usuario, int? ClvSession);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBorReconSession", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetBorReconSession(int? CLVSESSION);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDetalleRecontratacionTmp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DetalleRecontratacionEntity GetDetalleRecontratacionTmp(long? ClvSession, long? IdRecon);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_CalculaRecontratacionPrevio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CobraAdeudoEntity GetSp_CalculaRecontratacionPrevio(long? ClvSession);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_CalculaContratacionPrevio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CobraAdeudoEntity GetSp_CalculaContratacionPrevio(long? ClvSession);
    
    }
    }

  
﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReferenciaCliente
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReferenciaCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ReferenciaClienteEntity GetReferenciaCliente(int? IdReferencia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepReferenciaCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ReferenciaClienteEntity GetDeepReferenciaCliente(int? IdReferencia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReferenciaClienteList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReferenciaClienteEntity> GetReferenciaClienteList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReferenciaClientePagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ReferenciaClienteEntity> GetReferenciaClientePagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReferenciaClientePagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ReferenciaClienteEntity> GetReferenciaClientePagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddReferenciaCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddReferenciaCliente(ReferenciaClienteEntity objReferenciaCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateReferenciaCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateReferenciaCliente(ReferenciaClienteEntity objReferenciaCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteReferenciaCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteReferenciaCliente(String BaseRemoteIp, int BaseIdUser, int? IdReferencia);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReferenciaClienteL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReferenciaClienteEntity> GetReferenciaClienteL(long? IdContrato);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateReferencia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateReferencia(ReferenciaClienteEntity lstReferencia);


    }
}


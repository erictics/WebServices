﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReimprimirFolios
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReimpresionFoliosExistentesMin", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ReimprimirFoliosEntity GetReimpresionFoliosExistentesMin(ReimprimirFoliosEntity objReimprimirFolios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReimpresionFoliosExistentes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ReimprimirFoliosEntity GetReimpresionFoliosExistentes(ReimprimirFoliosEntity objReimprimirFolios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReimpresionFolios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetReimpresionFolios(ReimprimirFoliosEntity objReimprimirFolios);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelCalleColonia
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelCalleColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelCalleColoniaEntity GetRelCalleColonia(int? IdCalle, int? IdColonia, int? IdLocalidad, int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelCalleColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelCalleColoniaEntity GetDeepRelCalleColonia(int? IdCalle, int? IdColonia, int? IdLocalidad, int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelCalleColoniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelCalleColoniaEntity> GetRelCalleColoniaList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelCalleColoniaPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelCalleColoniaEntity> GetRelCalleColoniaPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelCalleColoniaPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelCalleColoniaEntity> GetRelCalleColoniaPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelCalleColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelCalleColonia(RelCalleColoniaEntity objRelCalleColonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelCalleColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelCalleColonia(RelCalleColoniaEntity objRelCalleColonia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelCalleColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelCalleColonia(String BaseRemoteIp, int BaseIdUser, int? IdCalle, int? IdColonia, int? IdLocalidad, int? IdMunicipio, int? IdEstado);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalleRelEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelCalleColoniaEntity> GetCalleRelEst(int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalleRelMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelCalleColoniaEntity> GetCalleRelMun(int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalleRelLoc", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelCalleColoniaEntity> GetCalleRelLoc(int? IdLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCalleRelCol", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelCalleColoniaEntity> GetCalleRelCol(int? IdColonia);

    }
}


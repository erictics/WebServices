﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelColoniaLocMunEst
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaLocMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelColoniaLocMunEstEntity GetRelColoniaLocMunEst(int? IdColonia, int? IdLocalidad, int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelColoniaLocMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelColoniaLocMunEstEntity GetDeepRelColoniaLocMunEst(int? IdColonia, int? IdLocalidad, int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaLocMunEstList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelColoniaLocMunEstEntity> GetRelColoniaLocMunEstList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaLocMunEstPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelColoniaLocMunEstEntity> GetRelColoniaLocMunEstPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaLocMunEstPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelColoniaLocMunEstEntity> GetRelColoniaLocMunEstPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelColoniaLocMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelColoniaLocMunEst(RelColoniaLocMunEstEntity objRelColoniaLocMunEst);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelColoniaLocMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelColoniaLocMunEst(RelColoniaLocMunEstEntity objRelColoniaLocMunEst);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelColoniaLocMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelColoniaLocMunEst(String BaseRemoteIp, int BaseIdUser, int? IdColonia, int? IdLocalidad, int? IdMunicipio, int? IdEstado);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaRelEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelColoniaLocMunEstEntity> GetColoniaRelEst(int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaRelMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelColoniaLocMunEstEntity> GetColoniaRelMun(int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaRelLoc", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelColoniaLocMunEstEntity> GetColoniaRelLoc(int? IdLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniaRelTipoColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelColoniaLocMunEstEntity> GetColoniaRelTipoColonia(int? IdTipoColonia);

    }
}


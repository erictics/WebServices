﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelColoniaMedio
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaMedioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelColoniaMedioEntity> GetRelColoniaMedioList(int? Clv_Colonia, int? Clv_Localidad, int? Clv_Ciudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelColoniaMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelColoniaMedio(RelColoniaMedioEntity objRelColoniaMedio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelColoniaMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelColoniaMedio(int? Clv_Localidad, int? Clv_Ciudad, int? Clv_Colonia, int? IdMedio);

    }
}


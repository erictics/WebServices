﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelColoniaServicio
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelColoniaServicioEntity GetRelColoniaServicio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelColoniaServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelColoniaServicioEntity GetDeepRelColoniaServicio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaServicioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelColoniaServicioEntity> GetRelColoniaServicioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaServicioPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelColoniaServicioEntity> GetRelColoniaServicioPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniaServicioPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelColoniaServicioEntity> GetRelColoniaServicioPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelColoniaServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelColoniaServicio(RelColoniaServicioEntity objRelColoniaServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelColoniaServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelColoniaServicio(RelColoniaServicioEntity objRelColoniaServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelColoniaServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelColoniaServicio(String BaseRemoteIp, int BaseIdUser, int? IdColonia, int? IdTipSer);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelColoniasCalles_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniasCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelColoniasCalles_NewEntity GetRelColoniasCalles_New();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelColoniasCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelColoniasCalles_NewEntity GetDeepRelColoniasCalles_New();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniasCalles_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelColoniasCalles_NewEntity> GetRelColoniasCalles_NewList(int? clv_calle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelColoniasCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelColoniasCalles_New(RelColoniasCalles_NewEntity objRelColoniasCalles_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelColoniasCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelColoniasCalles_New(RelColoniasCalles_NewEntity objRelColoniasCalles_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelColoniasCalles_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelColoniasCalles_New(int? clv_estado, int? clv_ciudad, int? clv_localidad, int? clv_colonia, int? clv_calle);

    }
}


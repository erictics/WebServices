﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelColoniasSer
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelColoniasSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelColoniasSerEntity GetDeepRelColoniasSer(int? Clv_Colonia, int? Clv_Localidad, int? Clv_Ciudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelColoniasSerList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelColoniasSerEntity> GetRelColoniasSerList(int? Clv_Colonia, int? Clv_Localidad, int? Clv_Ciudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelColoniasSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelColoniasSer(RelColoniasSerEntity objRelColoniasSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelColoniasSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelColoniasSer(RelColoniasSerEntity objRelColoniasSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelColoniasSer", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelColoniasSer(int? Clv_Localidad, int? Clv_Ciudad, int? Clv_Colonia, int? Clv_TipSer);

    }
}


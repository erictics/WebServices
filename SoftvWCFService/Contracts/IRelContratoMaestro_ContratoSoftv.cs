﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelContratoMaestro_ContratoSoftv
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelContratoMaestro_ContratoSoftv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelContratoMaestro_ContratoSoftvEntity GetRelContratoMaestro_ContratoSoftv();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelContratoMaestro_ContratoSoftv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelContratoMaestro_ContratoSoftvEntity GetDeepRelContratoMaestro_ContratoSoftv();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelContratoMaestro_ContratoSoftvList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelContratoMaestro_ContratoSoftvEntity> GetRelContratoMaestro_ContratoSoftvList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelContratoMaestro_ContratoSoftv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelContratoMaestro_ContratoSoftv(RelContratoMaestro_ContratoSoftvEntity objRelContratoMaestro_ContratoSoftv);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelContratoMaestro_ContratoSoftv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelContratoMaestro_ContratoSoftv(RelContratoMaestro_ContratoSoftvEntity objRelContratoMaestro_ContratoSoftv);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteRelContratoMaestro_ContratoSoftv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteRelContratoMaestro_ContratoSoftv(String BaseRemoteIp, int BaseIdUser,);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelEnProcesos
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEnProcesos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEnProcesosEntity GetRelEnProcesos();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelEnProcesos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEnProcesosEntity GetDeepRelEnProcesos();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEnProcesosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelEnProcesosEntity> GetRelEnProcesosList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEnProcesosPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelEnProcesosEntity> GetRelEnProcesosPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEnProcesosPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelEnProcesosEntity> GetRelEnProcesosPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelEnProcesos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelEnProcesos(RelEnProcesosEntity objRelEnProcesos);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelEnProcesos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelEnProcesos(RelEnProcesosEntity objRelEnProcesos);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteRelEnProcesos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteRelEnProcesos(String BaseRemoteIp, int BaseIdUser,);

    }
}


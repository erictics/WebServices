﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelEncuestaClientes
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEncuestaClientesEntity GetRelEncuestaClientes(int? IdProceso);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelEncuestaClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEncuestaClientesEntity GetDeepRelEncuestaClientes(int? IdProceso);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaClientesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelEncuestaClientesEntity> GetRelEncuestaClientesList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaClientesPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelEncuestaClientesEntity> GetRelEncuestaClientesPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaClientesPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelEncuestaClientesEntity> GetRelEncuestaClientesPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelEncuestaClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelEncuestaClientes(RelEncuestaClientesEntity objRelEncuestaClientes);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelEncuestaClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelEncuestaClientes(RelEncuestaClientesEntity objRelEncuestaClientes);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteRelEncuestaClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteRelEncuestaClientes(String BaseRemoteIp, int BaseIdUser,int? IdProceso);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaCli", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelEncuestaClientesEntity> GetRelEncuestaCli(RelEncuestaClientesEntity objEncCli, List<RelEnProcesosEntity> LstRelEnProcesos);

    }
}


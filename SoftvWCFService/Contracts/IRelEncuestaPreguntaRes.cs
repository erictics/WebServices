﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelEncuestaPreguntaRes
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaPreguntaRes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEncuestaPreguntaResEntity GetRelEncuestaPreguntaRes(int? Id);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelEncuestaPreguntaRes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEncuestaPreguntaResEntity GetDeepRelEncuestaPreguntaRes(int? Id);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaPreguntaResList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelEncuestaPreguntaResEntity> GetRelEncuestaPreguntaResList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaPreguntaResPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelEncuestaPreguntaResEntity> GetRelEncuestaPreguntaResPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEncuestaPreguntaResPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelEncuestaPreguntaResEntity> GetRelEncuestaPreguntaResPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelEncuestaPreguntaRes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelEncuestaPreguntaRes(RelEncuestaPreguntaResEntity objRelEncuestaPreguntaRes);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelEncuestaPreguntaRes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelEncuestaPreguntaRes(RelEncuestaPreguntaResEntity objRelEncuestaPreguntaRes);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteRelEncuestaPreguntaRes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteRelEncuestaPreguntaRes(String BaseRemoteIp, int BaseIdUser,int? Id);

    }
}


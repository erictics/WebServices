﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelEstadoCiudad_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEstadoCiudad_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEstadoCiudad_NewEntity GetRelEstadoCiudad_New();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelEstadoCiudad_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelEstadoCiudad_NewEntity GetDeepRelEstadoCiudad_New();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelEstadoCiudad_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelEstadoCiudad_NewEntity> GetRelEstadoCiudad_NewList(int? Clv_Ciudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelEstadoCiudad_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelEstadoCiudad_New(RelEstadoCiudad_NewEntity objRelEstadoCiudad_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelEstadoCiudad_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelEstadoCiudad_New(RelEstadoCiudad_NewEntity objRelEstadoCiudad_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelEstadoCiudad_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelEstadoCiudad_New(int? Op, int? Clv_Estado, int? Clv_Ciudad);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaCiudades", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelEstadoCiudad_NewEntity> GetBuscaCiudades(int? Clv_Ciudad, String Nombre, int? Op);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraRelEdoCd", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelEstadoCiudad_NewEntity> GetMuestraRelEdoCd(int? Op, int? Clv_Ciudad);

    }
}


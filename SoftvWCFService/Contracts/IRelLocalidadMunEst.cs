﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelLocalidadMunEst
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelLocalidadMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelLocalidadMunEstEntity GetRelLocalidadMunEst(int? IdLocalidad, int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelLocalidadMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelLocalidadMunEstEntity GetDeepRelLocalidadMunEst(int? IdLocalidad, int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelLocalidadMunEstList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelLocalidadMunEstEntity> GetRelLocalidadMunEstList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelLocalidadMunEstPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelLocalidadMunEstEntity> GetRelLocalidadMunEstPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelLocalidadMunEstPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelLocalidadMunEstEntity> GetRelLocalidadMunEstPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelLocalidadMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelLocalidadMunEst(RelLocalidadMunEstEntity objRelLocalidadMunEst);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelLocalidadMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelLocalidadMunEst(RelLocalidadMunEstEntity objRelLocalidadMunEst);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelLocalidadMunEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelLocalidadMunEst(String BaseRemoteIp, int BaseIdUser, int? IdLocalidad, int? IdMunicipio, int? IdEstado);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadRelEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelLocalidadMunEstEntity> GetLocalidadRelEst(int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadRelMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelLocalidadMunEstEntity> GetLocalidadRelMun(int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadRel", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelLocalidadMunEstEntity> GetLocalidadRel(int? IdLocalidad);
    }
}


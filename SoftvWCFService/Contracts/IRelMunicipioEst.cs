﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelMunicipioEst
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelMunicipioEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelMunicipioEstEntity GetRelMunicipioEst(int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelMunicipioEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelMunicipioEstEntity GetDeepRelMunicipioEst(int? IdMunicipio, int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelMunicipioEstList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelMunicipioEstEntity> GetRelMunicipioEstList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelMunicipioEstPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelMunicipioEstEntity> GetRelMunicipioEstPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelMunicipioEstPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelMunicipioEstEntity> GetRelMunicipioEstPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelMunicipioEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelMunicipioEst(RelMunicipioEstEntity objRelMunicipioEst);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelMunicipioEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelMunicipioEst(RelMunicipioEstEntity objRelMunicipioEst);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelMunicipioEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelMunicipioEst(String BaseRemoteIp, int BaseIdUser, int? IdMunicipio, int? IdEstado);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadosRelMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelMunicipioEstEntity> GetEstadosRelMun(int? IdEstado);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelPlazaEstMun
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPlazaEstMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelPlazaEstMunEntity GetRelPlazaEstMun(int? IdPlaza, int? IdEstado, int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelPlazaEstMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelPlazaEstMunEntity GetDeepRelPlazaEstMun(int? IdPlaza, int? IdEstado, int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPlazaEstMunList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelPlazaEstMunEntity> GetRelPlazaEstMunList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPlazaEstMunPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelPlazaEstMunEntity> GetRelPlazaEstMunPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPlazaEstMunPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelPlazaEstMunEntity> GetRelPlazaEstMunPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelPlazaEstMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelPlazaEstMun(RelPlazaEstMunEntity objRelPlazaEstMun);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelPlazaEstMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelPlazaEstMun(RelPlazaEstMunEntity objRelPlazaEstMun);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelPlazaEstMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelPlazaEstMun(String BaseRemoteIp, int BaseIdUser, int? IdPlaza, int? IdMunicipio, int? IdEstado);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaRelEst", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelPlazaEstMunEntity> GetPlazaRelEst(int? IdEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaRelMun", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelPlazaEstMunEntity> GetPlazaRelMun(int? IdMunicipio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazaRel", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RelPlazaEstMunEntity> GetPlazaRel(int? IdPlaza);


    }
}


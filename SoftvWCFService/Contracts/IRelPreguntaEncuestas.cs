﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelPreguntaEncuestas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelPreguntaEncuestasEntity GetRelPreguntaEncuestas();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelPreguntaEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelPreguntaEncuestasEntity GetDeepRelPreguntaEncuestas();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaEncuestasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelPreguntaEncuestasEntity> GetRelPreguntaEncuestasList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaEncuestasPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelPreguntaEncuestasEntity> GetRelPreguntaEncuestasPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaEncuestasPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelPreguntaEncuestasEntity> GetRelPreguntaEncuestasPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelPreguntaEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelPreguntaEncuestas(RelPreguntaEncuestasEntity objRelPreguntaEncuestas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelPreguntaEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelPreguntaEncuestas(RelPreguntaEncuestasEntity objRelPreguntaEncuestas);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteRelPreguntaEncuestas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteRelPreguntaEncuestas(String BaseRemoteIp, int BaseIdUser,);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelPreguntaOpcMults
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelPreguntaOpcMultsEntity GetRelPreguntaOpcMults();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelPreguntaOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelPreguntaOpcMultsEntity GetDeepRelPreguntaOpcMults();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaOpcMultsList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelPreguntaOpcMultsEntity> GetRelPreguntaOpcMultsList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaOpcMultsPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelPreguntaOpcMultsEntity> GetRelPreguntaOpcMultsPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelPreguntaOpcMultsPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelPreguntaOpcMultsEntity> GetRelPreguntaOpcMultsPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelPreguntaOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelPreguntaOpcMults(RelPreguntaOpcMultsEntity objRelPreguntaOpcMults);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelPreguntaOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelPreguntaOpcMults(RelPreguntaOpcMultsEntity objRelPreguntaOpcMults);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteRelPreguntaOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteRelPreguntaOpcMults(String BaseRemoteIp, int BaseIdUser,);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelRedCiudad
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelRedCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelRedCiudadEntity GetDeepRelRedCiudad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedCiudadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedCiudadEntity> GetRelRedCiudadList(int? IdRed);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelRedCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelRedCiudad(RelRedCiudadEntity objRelRedCiudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedCiudad_Dis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedCiudadEntity> GetRelRedCiudad_Dis(int? IdRed);

    }
}


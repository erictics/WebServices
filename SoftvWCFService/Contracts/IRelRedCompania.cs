﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelRedCompania
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelRedCompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelRedCompaniaEntity GetDeepRelRedCompania();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedCompaniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedCompaniaEntity> GetRelRedCompaniaList(long? IdRed);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelRedCompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelRedCompania(RelRedCompaniaEntity objRelRedCompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedCompania_Inc", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedCompaniaEntity> GetRelRedCompania_Inc(long? IdRed);

    }
}


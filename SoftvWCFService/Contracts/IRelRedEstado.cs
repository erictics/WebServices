﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelRedEstado
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelRedEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelRedEstadoEntity GetDeepRelRedEstado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedEstadoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedEstadoEntity> GetRelRedEstadoList(int? IdRed);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelRedEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelRedEstado(RelRedEstadoEntity objRelRedEstado);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedEstado_Dis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedEstadoEntity> GetRelRedEstado_Dis(int? IdRed);


    }
}


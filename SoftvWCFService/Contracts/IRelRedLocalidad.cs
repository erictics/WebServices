﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelRedLocalidad
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelRedLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelRedLocalidadEntity GetDeepRelRedLocalidad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedLocalidadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedLocalidadEntity> GetRelRedLocalidadList(int? IdRed);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelRedLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelRedLocalidad(RelRedLocalidadEntity objRelRedLocalidad);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedLocalidad_inc", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedLocalidadEntity> GetRelRedLocalidad_inc(int? IdRed);


    }
}


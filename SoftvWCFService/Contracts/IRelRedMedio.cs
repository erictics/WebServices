﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelRedMedio
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelRedMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelRedMedioEntity GetDeepRelRedMedio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedMedioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedMedioEntity> GetRelRedMedioList(int? IdRed);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelRedMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelRedMedio(RelRedMedioEntity objRelRedMedio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedMedio_Inc", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedMedioEntity> GetRelRedMedio_Inc(int? IdRed);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelRedPlaza
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelRedPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelRedPlazaEntity GetDeepRelRedPlaza();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedPlazaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedPlazaEntity> GetRelRedPlazaList(long? IdRed);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelRedPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelRedPlaza(RelRedPlazaEntity objRelRedPlaza);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelRedPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelRedPlaza(RelRedPlazaEntity objRelRedPlaza);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelRedPlaza_Inc", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelRedPlazaEntity> GetRelRedPlaza_Inc(long? IdRed);


    }
}


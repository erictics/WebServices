﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelTarifadosServicios
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTarifadosServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelTarifadosServiciosEntity GetRelTarifadosServicios(int? IdTarifadoSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelTarifadosServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelTarifadosServiciosEntity GetDeepRelTarifadosServicios(int? IdTarifadoSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTarifadosServiciosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelTarifadosServiciosEntity> GetRelTarifadosServiciosList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTarifadosServiciosPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelTarifadosServiciosEntity> GetRelTarifadosServiciosPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTarifadosServiciosPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RelTarifadosServiciosEntity> GetRelTarifadosServiciosPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelTarifadosServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelTarifadosServicios(RelTarifadosServiciosEntity objRelTarifadosServicios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelTarifadosServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelTarifadosServicios(RelTarifadosServiciosEntity objRelTarifadosServicios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelTarifadosServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelTarifadosServicios(String BaseRemoteIp, int BaseIdUser, int? IdTarifadoSer);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTarifadosServiciosByIdServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelTarifadosServiciosEntity> GetRelTarifadosServiciosByIdServicio(int? IdServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelTarifadosServiciosL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelTarifadosServiciosL(RelTarifadosServiciosEntity lstRelTarifadosServicios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelTarifadosServiciosL", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelTarifadosServiciosL(RelTarifadosServiciosEntity lstRelTarifadosServicios);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelTarifadosServiciosCostoPorCaja_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTarifadosServiciosCostoPorCaja_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelTarifadosServiciosCostoPorCaja_NewEntity GetRelTarifadosServiciosCostoPorCaja_New(long? Clv_Llave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRelTarifadosServiciosCostoPorCaja_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RelTarifadosServiciosCostoPorCaja_NewEntity GetDeepRelTarifadosServiciosCostoPorCaja_New(long? Clv_Llave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelTarifadosServiciosCostoPorCaja_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RelTarifadosServiciosCostoPorCaja_NewEntity> GetRelTarifadosServiciosCostoPorCaja_NewList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRelTarifadosServiciosCostoPorCaja_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRelTarifadosServiciosCostoPorCaja_New(RelTarifadosServiciosCostoPorCaja_NewEntity objRelTarifadosServiciosCostoPorCaja_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRelTarifadosServiciosCostoPorCaja_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRelTarifadosServiciosCostoPorCaja_New(RelTarifadosServiciosCostoPorCaja_NewEntity objRelTarifadosServiciosCostoPorCaja_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRelTarifadosServiciosCostoPorCaja_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRelTarifadosServiciosCostoPorCaja_New();

    }
}


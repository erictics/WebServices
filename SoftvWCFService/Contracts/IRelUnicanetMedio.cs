﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelUnicanetMedio
    {


    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetRelUnicanetMedioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    IEnumerable<RelUnicanetMedioEntity> GetRelUnicanetMedioList();

    }
    }

  
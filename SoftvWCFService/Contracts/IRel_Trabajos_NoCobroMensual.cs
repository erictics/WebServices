﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRel_Trabajos_NoCobroMensual
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRel_Trabajos_NoCobroMensual", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Rel_Trabajos_NoCobroMensualEntity GetDeepRel_Trabajos_NoCobroMensual(int? Clv_Servicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRel_Trabajos_NoCobroMensualList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Rel_Trabajos_NoCobroMensualEntity> GetRel_Trabajos_NoCobroMensualList();

    }
}


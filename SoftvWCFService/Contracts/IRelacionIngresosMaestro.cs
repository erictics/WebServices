﻿using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;


namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRelacionIngresosMaestro
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelacionIngresosMaestro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetRelacionIngresosMaestro(List<int> Distribuidores, string FechaInicial, string FechaFinal, int Dolares);

    }
}

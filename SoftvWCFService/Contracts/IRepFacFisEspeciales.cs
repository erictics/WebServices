﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRepFacFisEspeciales
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRepFacFisEspecialesGralList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RepFacFisEspecialesEntity> GetRepFacFisEspecialesGralList(ReporteCortesEntity objRep, List<SucursalesEspecialesEntity> lstSuc);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRepGralComisiones_RepVentas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRepGralComisiones_RepVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RepGralComisiones_RepVentasEntity> GetRepGralComisiones_RepVentasList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRepGralComisiones_RepVentasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RepGralComisiones_RepVentasEntity> GetRepGralComisiones_RepVentasXmlList(RepGralComisiones_RepVentasEntity Obj, List<Plaza_ReportesVentasEntity> ListPlaza, List<Vendores_ReportesVentasEntity> ListVendedor);


    }
}


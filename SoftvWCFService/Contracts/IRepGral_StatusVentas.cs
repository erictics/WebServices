﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRepGral_StatusVentas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRepGral_StatusVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RepGral_StatusVentasEntity> GetRepGral_StatusVentasList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRepGral_StatusVentasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RepGral_StatusVentasEntity> GetRepGral_StatusVentasXmlList(RepGral_StatusVentasEntity Obj, List<Plaza_ReportesVentasEntity> ListPlaza, List<ServiciosWebEntity> ListPaquete, List<Vendores_ReportesVentasEntity> ListVendedor);

    }
}


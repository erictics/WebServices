﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReporteCortes
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ReporteCortesEntity GetReporteCortes();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepReporteCortes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ReporteCortesEntity GetDeepReporteCortes();

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetReporteCortesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //IEnumerable<ReporteCortesEntity> GetReporteCortesList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ReporteCortesEntity> GetReporteCortesPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ReporteCortesEntity> GetReporteCortesPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddReporteCortes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddReporteCortes(ReporteCortesEntity objReporteCortes);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateReporteCortes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateReporteCortes(ReporteCortesEntity objReporteCortes);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesFacList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesEntity> GetReporteCortesFacList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReporteCortesEspeciales
    {



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesEspGralList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesEspecialesEntity> GetReporteCortesEspGralList(ReporteCortesEntity objRep, List<SucursalesEspecialesEntity> lstSuc);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesEspSucList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesEspecialesEntity> GetReporteCortesEspSucList(ReporteCortesEntity objRep, List<SucursalesEspecialesEntity> lstSuc);

    }
}


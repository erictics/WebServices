﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReporteCortesFac
    {


        //REPORTES CAJAS
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesFList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesFList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajeroList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajeroList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajaList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesSucursalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesSucursalList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajeroConPuntosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajeroConPuntosList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);



        //REPORTES VENTAS
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesGralVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesGralVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesPorVendedorList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesPorVendedorList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajasVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajasVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesSucursalVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesSucursalVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajeroVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajeroVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajeroConPuntosVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajeroConPuntosVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);



        //REPORTES RESUMEN
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesGralResumenList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesGralResumenList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajeroResumenList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajeroResumenList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesSucursalResumenList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesSucursalResumenList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesCajeroConPuntosResumenList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetReporteCortesCajeroConPuntosResumenList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);


        // ---- crystal

       
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursal_crystal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteCortesFacEntity> GetSucursal_crystal(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        //string GetSucursal_crystal(String xml);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReporteEstadoCuentaNuevo
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEstadoCuentaNuevoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteEstadoCuentaNuevoEntity> GetReporteEstadoCuentaNuevoList(int? Id, String Contrato, long? IdEstadoCuenta);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEstadoCuentaNuevo2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteEstadoCuentaNuevoEntity> GetReporteEstadoCuentaNuevo2List(long? IdEstadoCuenta, String Contrato);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEdoCuenta_CM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteEstadoCuentaNuevoEntity> GetReporteEdoCuenta_CM(long? ClvSessionPadre, long? ContratoMaestro, String Referencia);


    }
}


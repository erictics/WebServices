﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReporteFacFis
    {

        //REPORTES CAJAS
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesGralFacFisList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteFacFisEntity> GetReporteCortesGralFacFisList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesSucFacFisList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteFacFisEntity> GetReporteCortesSucFacFisList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);


        //REPORTES VENTAS
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesGralFacFisVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteFacFisEntity> GetReporteCortesGralFacFisVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesSucFacFisVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteFacFisEntity> GetReporteCortesSucFacFisVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);


        //REPORTES RESUMEN
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesSucFacFisResumenList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteFacFisEntity> GetReporteCortesSucFacFisResumenList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReporteFacturasMaestrasVencidas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteFacturasMaestrasVencidasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReporteFacturasMaestrasVencidasEntity> GetReporteFacturasMaestrasVencidasList(String Fecha, int? Vencidas, int? PorVencer);

    }
}


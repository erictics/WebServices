﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReporteServiciosPorInstalar
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteServiciosPorInstalarList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RepServXInstEntity GetReporteServiciosPorInstalarList(long? ContratoMaestro);

    }
}


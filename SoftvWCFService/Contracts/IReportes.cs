﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReportes
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteProspectos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteProspectos(List<PlazaRepVariosEntity> distribuidores, List<companiaEntity> plazas, string fechaInicio, string fechafin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteHoteles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteHoteles(List<PlazaRepVariosEntity> distribuidores, List<companiaEntity> plazas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteSuscriptores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteSuscriptores(List<PlazaRepVariosEntity> distribuidores, List<companiaEntity> plazas, List<EstadoEntity> estados, int? mes, int? anio, int? clv_reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportePermanencia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportePermanencia(List<PlazaRepVariosEntity> distribuidores, List<companiaEntity> plazas, List<Servicios_NewEntity> servicios, int? mesInicio, int? anioInicio, int? mesFin, int? anioFin, string StrmesInicio, string StrmesFin, int? Clv_tipser);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTecnicosCompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TecnicosEntity> GetTecnicosCompania(List<companiaEntity> plazas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteAgendaTecnico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteAgendaTecnico(int? Clv_tecnico, string fechainicio, string fechafin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteListadoActividadesTecnico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteListadoActividadesTecnico(List<companiaEntity> plazas, List<TecnicosEntity> tecnicos, List<TrabajoEntity> trabajos, string fechainicio, string fechafin, int? resumen, string TipoRep);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteProductividadTecnico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteProductividadTecnico(List<companiaEntity> plazas, List<TecnicosEntity> tecnicos, string fechainicio, string fechafin);




        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDevolucionAlmacen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteDevolucionAlmacen(List<PlazaRepVariosEntity> distribuidores, List<companiaEntity> plazas, string fechainicio, string fechafin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportePendientesAreaTecnica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportePendientesAreaTecnica(List<PlazaRepVariosEntity> distribuidores, List<companiaEntity> plazas, List<CiudadCAMDOEntity> ciudades, List<LocalidadCAMDOEntity> localidades);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteOrdenes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteOrdenes(ReporteEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetImprimirFolios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetImprimirFolios(string Serie, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporte_Reimpresion_Folios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporte_Reimpresion_Folios(string Serie, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporte_Folios5", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporte_Folios5(int? Vendedor, string Serie);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteQuejas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteQuejas(ReporteEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteAtencion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteAtencion(ReporteEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportesVarios_1", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportesVarios_1(ReporteEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportesVarios_2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportesVarios_2(ReporteEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportesVarios_3", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportesVarios_3(ReporteEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportesVarios_4", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportesVarios_4(ReporteEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportesVarios_5", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportesVarios_5(ReporteEntity reporte);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInterfazAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteInterfazAparatos(InterfazCablemodemEntity ObjReporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRAUSUARIOSConEntregasParciales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioSoftvEntity> GetMUESTRAUSUARIOSConEntregasParciales(int? OP, long? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMuestraUsuariosQuecancelaronImprimieron", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioSoftvEntity> GetMuestraUsuariosQuecancelaronImprimieron(int? bandera, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEntregasParciales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteEntregasParciales(int? op, string fechaInicio, string fechaFin, List<UsuarioSoftvEntity> usuarios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteTickets", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteTickets(int? op, string fechaInicio, string fechaFin, string clv_usuario, long? idcompania, long? clvciudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteBonificacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteBonificacion(int? aplicada, string fechaInicio, string fechaFin, List<companiaEntity> plazas);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteRelacionIngresosDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteRelacionIngresosDistribuidor(int? OP, string fechaInicio, string fechaFin, List<companiaEntity> comp);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDeIngresoPorSucursalYPlaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteDeIngresoPorSucursalYPlaza(List<companiaEntity> plazas, List<SUCURSALESEntity> sucursales, string fechaInicio, string fechaFin);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteAcumuladoDeClientesEIngresos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteAcumuladoDeClientesEIngresos(List<companiaEntity> plazas);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteAcumuladoDeClientesEIngresosHistorico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteAcumuladoDeClientesEIngresosHistorico(List<companiaEntity> plazas, string fecha);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCobranzaCruzada", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteCobranzaCruzada(List<companiaEntity> plazas, string fechaInicio, string fechaFin);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEntregaDeDinero", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteEntregaDeDinero(List<SUCURSALESEntity> sucursales, string fechaInicio, string fechaFin);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteContrato(long? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteContrato_porServicio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteContrato_porServicio(long? contrato, int? tipoServ);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvweb_GetDistribuidorRegionCartera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ReportecarteraEntity> GetSoftvweb_GetDistribuidorRegionCartera(int? op, string fecha, long? Clv_Plaza, long? Clave);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCartera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteCartera(ReportecarteraEntity opciones);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteMantenimiento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteMantenimiento(ReporteMantenimientoEntity obj);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporte_TiempoDeAtencionDeOrdernesYReportes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporte_TiempoDeAtencionDeOrdernesYReportes(ReporteResumenOrdenQuejaEntity reporte);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReportePendientesAplicar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReportePendientesAplicar(SolicitudGastosEntity reporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_ReporteQuejaMasiva", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetSp_ReporteQuejaMasiva(long? Clv_Queja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDocEntregaEfectivoCorp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetDocEntregaEfectivoCorp(long? IdEntrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUSPReporteFoliosVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetUSPReporteFoliosVentas(long? Clv_Session, string FechaIni, string FechaFin);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteTrabajosTecnico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         string GetReporteTrabajosTecnico(long? op, string fechainicio, string fechafin, bool ordenes, bool quejas, List<TecnicosEntity> tecnicos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Get_NombreSistema", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string Get_NombreSistema(long? op);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRepDetallePreliminar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetRepDetallePreliminar(long? Clv_SessionBancos);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetMuestraTipServ_Cliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //List<ServiciosClientEntity> GetMuestraTipServ_Cliente(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteNotasCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         string GetReporteNotasCredito(int ? op,List<companiaEntity> plazas, string fechaInicio, string fechaFin);


    }
}


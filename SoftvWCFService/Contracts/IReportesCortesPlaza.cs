﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReportesCortesPlaza
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesPlazaCajasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReportesCortesPlazaEntity> GetReporteCortesPlazaCajasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesPlazaVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReportesCortesPlazaEntity> GetReporteCortesPlazaVentasList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesPlazaResumenList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ReportesCortesPlazaEntity> GetReporteCortesPlazaResumenList(ReporteCortesEntity objRep, List<PlazasXmlEntity> lstPlaza, List<DistribuidoresXmlEntity> lstDis);
        

    }
}


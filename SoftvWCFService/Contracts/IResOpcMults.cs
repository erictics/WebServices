﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IResOpcMults
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetResOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResOpcMultsEntity GetResOpcMults(int? Id_ResOpcMult);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepResOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResOpcMultsEntity GetDeepResOpcMults(int? Id_ResOpcMult);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetResOpcMultsList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ResOpcMultsEntity> GetResOpcMultsList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetResOpcMultsPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ResOpcMultsEntity> GetResOpcMultsPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetResOpcMultsPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ResOpcMultsEntity> GetResOpcMultsPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddResOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddResOpcMults(ResOpcMultsEntity objResOpcMults);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateResOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateResOpcMults(ResOpcMultsEntity objResOpcMults);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteResOpcMults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteResOpcMults(String BaseRemoteIp, int BaseIdUser, int? Id_ResOpcMult);

    }
}


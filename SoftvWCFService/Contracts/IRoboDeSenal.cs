﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRoboDeSenal
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRoboDeSenal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RoboDeSenalEntity GetRoboDeSenal(long? IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRoboDeSenal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RoboDeSenalEntity GetDeepRoboDeSenal(long? IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRoboDeSenalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RoboDeSenalEntity> GetRoboDeSenalList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRoboDeSenalPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RoboDeSenalEntity> GetRoboDeSenalPagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRoboDeSenalPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<RoboDeSenalEntity> GetRoboDeSenalPagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRoboDeSenal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRoboDeSenal(RoboDeSenalEntity objRoboDeSenal);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRoboDeSenal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRoboDeSenal(RoboDeSenalEntity objRoboDeSenal);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRoboDeSenal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRoboDeSenal(String BaseRemoteIp, int BaseIdUser, long IdContrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddNotasCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddNotasCliente(RoboDeSenalEntity lstNotasCliente, List<ObservacionClienteEntity> ObservacionClienteAdd);

    }
}


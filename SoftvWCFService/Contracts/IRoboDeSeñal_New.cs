﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRoboDeSeñal_New
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRoboDeSeñal_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RoboDeSeñal_NewEntity GetRoboDeSeñal_New(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepRoboDeSeñal_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RoboDeSeñal_NewEntity GetDeepRoboDeSeñal_New(long? Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRoboDeSeñal_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RoboDeSeñal_NewEntity> GetRoboDeSeñal_NewList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRoboDeSeñal_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddRoboDeSeñal_New(RoboDeSeñal_NewEntity objRoboDeSeñal_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRoboDeSeñal_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateRoboDeSeñal_New(RoboDeSeñal_NewEntity objRoboDeSeñal_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteRoboDeSeñal_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteRoboDeSeñal_New(long? Contrato);

    }
}


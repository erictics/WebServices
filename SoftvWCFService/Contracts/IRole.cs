﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRole
    {
        //[OperationContract]
        //RoleEntity GetRole(int? IdRol);
        //[OperationContract]
        //RoleEntity GetDeepRole(int? IdRol);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRolList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RoleEntity> GetRolList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUpdatePermisoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<RoleEntity> GetUpdatePermisoList(RoleEntity objRole, List<PermisoEntity> LstPermiso);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddRol", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAddRol(RoleEntity rol);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRoleById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         RoleEntity GetRoleById(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUpdateRol", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetUpdateRol(RoleEntity rol);

    }
}


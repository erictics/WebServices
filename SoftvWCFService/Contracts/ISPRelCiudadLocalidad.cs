﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISPRelCiudadLocalidad
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSPRelCiudadLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SPRelCiudadLocalidadEntity GetSPRelCiudadLocalidad();
        [OperationContract]

        [WebInvoke(Method = "*", UriTemplate = "GetDeepSPRelCiudadLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SPRelCiudadLocalidadEntity GetDeepSPRelCiudadLocalidad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSPRelCiudadLocalidadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<SPRelCiudadLocalidadEntity> GetSPRelCiudadLocalidadList(int? clv_usuario, int? clv_localidad, int? clv_ciudad, int? opcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddSPRelCiudadLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddSPRelCiudadLocalidad(SPRelCiudadLocalidadEntity objSPRelCiudadLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateSPRelCiudadLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateSPRelCiudadLocalidad(SPRelCiudadLocalidadEntity objSPRelCiudadLocalidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteSPRelCiudadLocalidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteSPRelCiudadLocalidad(int? clv_usuario, int? clv_localidad, int? clv_ciudad, int? opcion);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRelCiudadLocalidadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<SPRelCiudadLocalidadEntity> GetRelCiudadLocalidadList(int? clv_usuario, int? clv_localidad, int? clv_ciudad, int? opcion);


    }
}


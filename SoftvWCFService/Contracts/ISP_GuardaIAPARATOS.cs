﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISP_GuardaIAPARATOS
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddSP_GuardaIAPARATOS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddSP_GuardaIAPARATOS(SP_GuardaIAPARATOSEntity objSP_GuardaIAPARATOS);


    }
}


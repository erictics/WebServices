﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISP_GuardaOrdSerAparatos
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepSP_GuardaOrdSerAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SP_GuardaOrdSerAparatosEntity GetDeepSP_GuardaOrdSerAparatos(long? ClvOrden, String Op, String Status, int? Op2);


    }
}


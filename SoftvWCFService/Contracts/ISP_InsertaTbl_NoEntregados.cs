﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISP_InsertaTbl_NoEntregados
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_InsertaTbl_NoEntregados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetSP_InsertaTbl_NoEntregados(List<SP_InsertaTbl_NoEntregadosEntity> objSP_InsertaTbl_NoEntregados);


    }
}


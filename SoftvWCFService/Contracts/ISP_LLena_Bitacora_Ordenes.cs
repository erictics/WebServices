﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISP_LLena_Bitacora_Ordenes
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddSP_LLena_Bitacora_Ordenes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddSP_LLena_Bitacora_Ordenes(SP_LLena_Bitacora_OrdenesEntity objSP_LLena_Bitacora_Ordenes);



    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISP_SerieFolio
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSP_SerieFolioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<SP_SerieFolioEntity> GetSP_SerieFolioList(int? ClvUsuario);

    }
}


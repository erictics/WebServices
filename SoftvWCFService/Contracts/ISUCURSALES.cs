﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISUCURSALES
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSUCURSALES", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SUCURSALESEntity> GetSUCURSALES(long? Clv_Sucursal, string Nombre, string OP, long? idcompania, int? clv_usuario);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepSUCURSALES", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SUCURSALESEntity GetDeepSUCURSALES(int? clv_sucursal);
       
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSUCURSALESPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<SUCURSALESEntity> GetSUCURSALESPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSUCURSALESPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<SUCURSALESEntity> GetSUCURSALESPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddSUCURSALES", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddSUCURSALES(SUCURSALESEntity objSUCURSALES);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateSUCURSALES", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateSUCURSALES(SUCURSALESEntity objSUCURSALES);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMUESTRASUCURSALES2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SUCURSALESEntity> GetMUESTRASUCURSALES2(int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucuralesByplaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SUCURSALESEntity> GetSucuralesByPlaza(List<companiaEntity> plazas);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteSUCURSALES", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteSUCURSALES(String BaseRemoteIp, int BaseIdUser,);





    }
}


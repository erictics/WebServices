﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IServiciosDigital
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosDigital", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ServiciosDigitalEntity GetServiciosDigital();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepServiciosDigital", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ServiciosDigitalEntity GetDeepServiciosDigital();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosDigitalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ServiciosDigitalEntity> GetServiciosDigitalList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosDigitalPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ServiciosDigitalEntity> GetServiciosDigitalPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosDigitalPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ServiciosDigitalEntity> GetServiciosDigitalPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddServiciosDigital", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddServiciosDigital(ServiciosDigitalEntity objServiciosDigital);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateServiciosDigital", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateServiciosDigital(ServiciosDigitalEntity objServiciosDigital);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteServiciosDigital", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteServiciosDigital(String BaseRemoteIp, int BaseIdUser,);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IServiciosInternet
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ServiciosInternetEntity GetServiciosInternet();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepServiciosInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ServiciosInternetEntity GetDeepServiciosInternet();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosInternetList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ServiciosInternetEntity> GetServiciosInternetList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosInternetPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ServiciosInternetEntity> GetServiciosInternetPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosInternetPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<ServiciosInternetEntity> GetServiciosInternetPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddServiciosInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddServiciosInternet(ServiciosInternetEntity objServiciosInternet);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateServiciosInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateServiciosInternet(ServiciosInternetEntity objServiciosInternet);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteServiciosInternet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteServiciosInternet(String BaseRemoteIp, int BaseIdUser,);

    }
}


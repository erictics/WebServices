﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IServiciosWeb
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosWebList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ServiciosWebEntity> GetServiciosWebList(int? ClvTipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPaquetesRepVentasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ServiciosWebEntity> GetPaquetesRepVentasXmlList(ServiciosWebEntity Obj, List<TipServEntity> LstTipSer);

    }
}


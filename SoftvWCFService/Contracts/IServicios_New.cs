﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IServicios_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServicios_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Servicios_NewEntity GetServicios_New(int? Clv_Servicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepServicios_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Servicios_NewEntity GetDeepServicios_New(int? Clv_Servicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServicios_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Servicios_NewEntity> GetServicios_NewList(int? Clv_TipSer, int? Clv_Servicio, String Descripcion, String Clv_Txt, int? Op, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddServicios_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddServicios_New(Servicios_NewEntity objServicios_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateServicios_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateServicios_New(Servicios_NewEntity objServicios_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteServicios_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteServicios_New(int? Clv_Servicio);

    }
}


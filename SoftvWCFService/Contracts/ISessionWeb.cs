﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISessionWeb
    {
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetSessionWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //SessionWebEntity GetSessionWeb(String Token);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetDeepSessionWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //SessionWebEntity GetDeepSessionWeb(String Token);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetSessionWebList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //IEnumerable<SessionWebEntity> GetSessionWebList(String Token);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursalPorIp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SessionWebEntity GetSucursalPorIp(long? id, string ip);

      

    }
}


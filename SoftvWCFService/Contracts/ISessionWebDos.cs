﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISessionWebDos
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepSessionWebDos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SessionWebDosEntity GetDeepSessionWebDos(String Usuario, String Pass, int? Id);


    }
}


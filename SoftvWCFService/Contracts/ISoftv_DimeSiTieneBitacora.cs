﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISoftv_DimeSiTieneBitacora
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_DimeSiTieneBitacora", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Softv_DimeSiTieneBitacoraEntity GetSoftv_DimeSiTieneBitacora(long? ClvOrdSer, String TipoDescarga);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGRABAtblDescargaMaterialCableIACTV", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Softv_DimeSiTieneBitacoraEntity GetGRABAtblDescargaMaterialCableIACTV(long? ClvOrdSer);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetchecaBitacoraTecnico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Softv_DimeSiTieneBitacoraEntity GetchecaBitacoraTecnico(long? ClvOrdSer, String TipoDescarga);



    }
}


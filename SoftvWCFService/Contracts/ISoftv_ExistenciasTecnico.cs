﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISoftv_ExistenciasTecnico
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftv_ExistenciasTecnico", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Softv_ExistenciasTecnicoEntity GetSoftv_ExistenciasTecnico(long? ClvArticulo, long? ClvTecnico, int? IdAlmacen);

    }
}


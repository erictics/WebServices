﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISucursales_ReportesVentas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursales_ReportesVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Sucursales_ReportesVentasEntity> GetSucursales_ReportesVentasList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursales_ReportesVentasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Sucursales_ReportesVentasEntity> GetSucursales_ReportesVentasXmlList(Sucursales_ReportesVentasEntity obj, List<Plaza_ReportesVentasEntity> LstPlaza);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISumaDetalle
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSumaDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SumaDetalleEntity GetSumaDetalle();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepSumaDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SumaDetalleEntity GetDeepSumaDetalle();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSumaDetalleList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<SumaDetalleEntity> GetSumaDetalleList(long? Clv_Session);



    }
}


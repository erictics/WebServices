﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISuspencionTemporal
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSuspencionTemporal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SuspencionTemporalEntity GetSuspencionTemporal();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepSuspencionTemporal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SuspencionTemporalEntity GetDeepSuspencionTemporal();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSuspencionTemporalList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<SuspencionTemporalEntity> GetSuspencionTemporalList(long? Contrato, long? IdSession, long? Clv_Detalle);

    }
}


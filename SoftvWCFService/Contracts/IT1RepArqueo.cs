﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IT1RepArqueo
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetT1RepArqueoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<T1RepArqueoEntity> GetT1RepArqueoList(String Fecha, String ClvUsuario, int? Id);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITblDescargaMaterial
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTblDescargaMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TblDescargaMaterialEntity GetTblDescargaMaterial();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTblDescargaMaterial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TblDescargaMaterialEntity GetDeepTblDescargaMaterial();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTblDescargaMaterialList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TblDescargaMaterialEntity> GetTblDescargaMaterialList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDescargaMaterialArt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TblDescargaMaterialEntity> GetDescargaMaterialArt(TblDescargaMaterialEntity ObjDescargaMat, List<TblArticulosEntity> Articulos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddDescargaMaterialArt", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TblDescargaMaterialEntity> GetAddDescargaMaterialArt(TblDescargaMaterialEntity ObjDescargaMat, List<TblArticulosEntity> Articulos);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITblFacturasOpciones
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTblFacturasOpciones", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTblFacturasOpciones(TblFacturasOpcionesEntity objTblFacturasOpciones);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTblFacturasOpcionesCM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTblFacturasOpcionesCM(TblFacturasOpcionesEntity objTblFacturasOpcionesCM);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITblNet
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServicioClvEqMedioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TblNetEntity> GetServicioClvEqMedioList(int? ClvServicio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddServicioClvEqMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TblNetEntity GetAddServicioClvEqMedio(List<TblNetEntity> ObjClvEquivalente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUpdateServicioClvEqMedio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TblNetEntity GetUpdateServicioClvEqMedio(List<TblNetEntity> ObjClvEquivalente);
    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipServ
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipServEntity GetTipServ(int? IdTipSer);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTipServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipServEntity GetDeepTipServ(int? IdTipSer);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipServList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TipServEntity> GetTipServList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipServPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipServEntity> GetTipServPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipServPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipServEntity> GetTipServPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipServ(TipServEntity objTipServ);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipServ(TipServEntity objTipServ);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteTipServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteTipServ(String BaseRemoteIp, int BaseIdUser, int? IdTipSer);

    }
}


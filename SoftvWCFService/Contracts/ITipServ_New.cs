﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipServ_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipServ_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipServ_NewEntity GetTipServ_New(int? Clv_TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTipServ_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipServ_NewEntity GetDeepTipServ_New(int? Clv_TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipServ_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TipServ_NewEntity> GetTipServ_NewList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipServ_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipServ_New(TipServ_NewEntity objTipServ_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipServ_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipServ_New(TipServ_NewEntity objTipServ_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteTipServ_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteTipServ_New(int? Clv_TipSer);

    }
}


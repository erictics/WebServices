﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipoCliente
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoClienteEntity GetTipoCliente(int? IdTipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTipoCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoClienteEntity GetDeepTipoCliente(int? IdTipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoClienteList_WebSoftvnew", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TipoClienteEntity> GetTipoClienteList_WebSoftvnew();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoClientePagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipoClienteEntity> GetTipoClientePagedList(int page, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoClientePagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipoClienteEntity> GetTipoClientePagedListXml(int page, int pageSize, String xml);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipoCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipoCliente(TipoClienteEntity objTipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipoCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipoCliente(TipoClienteEntity objTipoCliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteTipoCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteTipoCliente(String BaseRemoteIp, int BaseIdUser, int? IdTipoCliente);

    }
}


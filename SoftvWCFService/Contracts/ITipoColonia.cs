﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipoColonia
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoColoniaEntity GetTipoColonia(int? IdTipoColonia);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTipoColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoColoniaEntity GetDeepTipoColonia(int? IdTipoColonia);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoColoniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TipoColoniaEntity> GetTipoColoniaList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoColoniaPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipoColoniaEntity> GetTipoColoniaPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoColoniaPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipoColoniaEntity> GetTipoColoniaPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipoColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipoColonia(TipoColoniaEntity objTipoColonia);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipoColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipoColonia(TipoColoniaEntity objTipoColonia);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteTipoColonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteTipoColonia(String BaseRemoteIp, int BaseIdUser, int? IdTipoColonia);

    }
}


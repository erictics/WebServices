﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipoDeCambio
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoDeCambio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoDeCambioEntity GetTipoDeCambio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTipoDeCambio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoDeCambioEntity GetDeepTipoDeCambio();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoDeCambioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TipoDeCambioEntity> GetTipoDeCambioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipoDeCambio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipoDeCambio(TipoDeCambioEntity objTipoDeCambio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipoDeCambio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipoDeCambio(TipoDeCambioEntity objTipoDeCambio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteTipoDeCambio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteTipoDeCambio(int? IdTipoDeCambio);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddTipoCambio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TipoDeCambioEntity> GetAddTipoCambio(Decimal? TipoDeCambio2, int? Clv_Usuario);

    }
}


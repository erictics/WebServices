﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipoPreguntas
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoPreguntasEntity GetTipoPreguntas(int? IdTipoPregunta);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTipoPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TipoPreguntasEntity GetDeepTipoPreguntas(int? IdTipoPregunta);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoPreguntasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<TipoPreguntasEntity> GetTipoPreguntasList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoPreguntasPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipoPreguntasEntity> GetTipoPreguntasPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoPreguntasPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<TipoPreguntasEntity> GetTipoPreguntasPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipoPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipoPreguntas(TipoPreguntasEntity objTipoPreguntas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipoPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipoPreguntas(TipoPreguntasEntity objTipoPreguntas);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteTipoPreguntas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteTipoPreguntas(String BaseRemoteIp, int BaseIdUser, int? IdTipoPregunta);

    }
}


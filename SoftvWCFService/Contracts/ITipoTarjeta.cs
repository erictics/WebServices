﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipoTarjeta
    {
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTipoTarjeta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    TipoTarjetaEntity GetTipoTarjeta(int? IdTipoTarjeta);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetDeepTipoTarjeta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    TipoTarjetaEntity GetDeepTipoTarjeta(int? IdTipoTarjeta);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTipoTarjetaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    IEnumerable<TipoTarjetaEntity> GetTipoTarjetaList();
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTipoTarjetaPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    SoftvList<TipoTarjetaEntity> GetTipoTarjetaPagedList(int page, int pageSize);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTipoTarjetaPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    SoftvList<TipoTarjetaEntity> GetTipoTarjetaPagedListXml(int page, int pageSize, String xml);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "AddTipoTarjeta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? AddTipoTarjeta(TipoTarjetaEntity objTipoTarjeta);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "UpdateTipoTarjeta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? UpdateTipoTarjeta(TipoTarjetaEntity objTipoTarjeta);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "DeleteTipoTarjeta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? DeleteTipoTarjeta(String BaseRemoteIp, int BaseIdUser,int? IdTipoTarjeta);
    
    }
    }

  
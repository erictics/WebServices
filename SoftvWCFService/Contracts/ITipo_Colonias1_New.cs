﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITipo_Colonias1_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipo_Colonias1_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Tipo_Colonias1_NewEntity GetTipo_Colonias1_New(int? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepTipo_Colonias1_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Tipo_Colonias1_NewEntity GetDeepTipo_Colonias1_New(int? Clave);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipo_Colonias1_NewList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Tipo_Colonias1_NewEntity> GetTipo_Colonias1_NewList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTipo_Colonias1_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTipo_Colonias1_New(Tipo_Colonias1_NewEntity objTipo_Colonias1_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTipo_Colonias1_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTipo_Colonias1_New(Tipo_Colonias1_NewEntity objTipo_Colonias1_New);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteTipo_Colonias1_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteTipo_Colonias1_New(int? Clave);

    }
}


﻿
    using Globals;
    using Softv.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.ServiceModel.Web;

    namespace SoftvWCFService.Contracts
    {
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ITiposCliente
    {
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTiposCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    TiposClienteEntity GetTiposCliente(int? IdTipoCliente);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetDeepTiposCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    TiposClienteEntity GetDeepTiposCliente(int? IdTipoCliente);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTiposClienteList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    IEnumerable<TiposClienteEntity> GetTiposClienteList();
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTiposClientePagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    SoftvList<TiposClienteEntity> GetTiposClientePagedList(int page, int pageSize);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "GetTiposClientePagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    SoftvList<TiposClienteEntity> GetTiposClientePagedListXml(int page, int pageSize, String xml);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "AddTiposCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? AddTiposCliente(TiposClienteEntity objTiposCliente);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "UpdateTiposCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? UpdateTiposCliente(TiposClienteEntity objTiposCliente);
    [OperationContract]
    [WebInvoke(Method = "*", UriTemplate = "DeleteTiposCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
    int? DeleteTiposCliente(String BaseRemoteIp, int BaseIdUser,int? IdTipoCliente);
    
    }
    }

  
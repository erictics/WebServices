﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUltimoSerieYFolio
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUltimoSerieYFolioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<UltimoSerieYFolioEntity> GetUltimoSerieYFolioList(int? Clv_Vendedor, long? Contrato);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUniversoEncuesta
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUniversoEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UniversoEncuestaEntity GetUniversoEncuesta(int? Id);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepUniversoEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UniversoEncuestaEntity GetDeepUniversoEncuesta(int? Id);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUniversoEncuestaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<UniversoEncuestaEntity> GetUniversoEncuestaList();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUniversoEncuestaPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<UniversoEncuestaEntity> GetUniversoEncuestaPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUniversoEncuestaPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<UniversoEncuestaEntity> GetUniversoEncuestaPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddUniversoEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddUniversoEncuesta(UniversoEncuestaEntity objUniversoEncuesta);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateUniversoEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateUniversoEncuesta(UniversoEncuestaEntity objUniversoEncuesta);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteUniversoEncuesta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteUniversoEncuesta(String BaseRemoteIp, int BaseIdUser, int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUniversoEncuestaAplicarList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<UniversoEncuestaEntity> GetUniversoEncuestaAplicarList(int? IdProcesoEnc);


    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUspChecaSiTieneExtensiones
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUspChecaSiTieneExtensiones", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UspChecaSiTieneExtensionesEntity GetUspChecaSiTieneExtensiones(long? CLVORDEN);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUspLlenaComboExtensionesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<UspLlenaComboExtensionesEntity> GetUspLlenaComboExtensionesList(int? NUMEXT, long? CLV_ORDEN);

    }
}


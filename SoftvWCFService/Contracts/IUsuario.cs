﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUsuario
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetusuarioByUserAndPass", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioSoftvEntity GetusuarioByUserAndPass(string Usuario, string Pass);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "LogOn", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameSessionWEntity LogOn();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuarioSoftvList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioSoftvEntity> GetUsuarioSoftvList(string Clv_Usuario, string Nombre, int? Op, int? idcompania);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConsultaIdentificacionUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioSoftvEntity> GetConsultaIdentificacionUsuario(int? clave, string Nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAddUsuarioSoftv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioSoftvEntity GetAddUsuarioSoftv(UsuarioSoftvEntity usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAgregaEliminaRelCompaniaUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<companiaEntity> GetAgregaEliminaRelCompaniaUsuario(int? clv_usuario, int? idcompania, int? opcion);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSoftvweb_GetUsuarioSoftvbyId", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioSoftvEntity GetSoftvweb_GetUsuarioSoftvbyId(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEditUsuarioSoftv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioSoftvEntity GetEditUsuarioSoftv(UsuarioSoftvEntity usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConGrupoVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<GrupoVentasEntity> GetConGrupoVentas(int? Clv_Grupo, int? Op, int? Clv_Usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetNueRelUsuarioGrupoVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetNueRelUsuarioGrupoVentas(String Clv_Usuario, int? Clv_Grupo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConRelUsuarioGrupoVentas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GrupoVentasEntity GetConRelUsuarioGrupoVentas(int? Op, String Clv_Usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetobtenUsuariosSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UsuarioSoftvEntity> GetobtenUsuariosSucursal(int? clv_sucursal);

    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IVALIDACatalogoSeries
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVALIDACatalogoSeries", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        VALIDACatalogoSeriesEntity GetVALIDACatalogoSeries(int? CLAVE, String SERIE, int? UltimoFolio_Usado, int? Clv_Vendedor, String OPCION);

    }
}


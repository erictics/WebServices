﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IVALIDAOrdenQueja
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepVALIDAOrdenQueja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        VALIDAOrdenQuejaEntity GetDeepVALIDAOrdenQueja(long? Contrato, int? TipSer, String Usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaSiRealizaQueja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool? GetValidaSiRealizaQueja(long? contrato, int clv_tipser);
    }
}


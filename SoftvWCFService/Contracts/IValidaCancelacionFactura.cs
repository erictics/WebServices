﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaCancelacionFactura
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaCancelacionFacturaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ValidaCancelacionFacturaEntity> GetValidaCancelacionFacturaList(long? ClvFactura);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaCancelacionFacturaCM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ValidaCancelacionFacturaEntity> GetValidaCancelacionFacturaCM(long? ClvFactura);


    }
}


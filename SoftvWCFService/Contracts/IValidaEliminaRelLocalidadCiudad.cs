﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaEliminaRelLocalidadCiudad
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepValidaEliminaRelLocalidadCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEliminaRelLocalidadCiudadEntity GetDeepValidaEliminaRelLocalidadCiudad(int? clv_localidad, int? clv_ciudad);

    }
}


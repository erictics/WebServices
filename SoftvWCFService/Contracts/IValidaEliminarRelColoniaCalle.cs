﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaEliminarRelColoniaCalle
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaEliminarRelColoniaCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaEliminarRelColoniaCalleEntity GetValidaEliminarRelColoniaCalle(int? clv_Calle, int? clv_colonia);

    }
}


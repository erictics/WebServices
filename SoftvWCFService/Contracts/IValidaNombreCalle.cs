﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaNombreCalle
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddValidaNombreCalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddValidaNombreCalle(ValidaNombreCalleEntity objValidaNombreCalle);


    }
}


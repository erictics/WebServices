﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaOrdSerManuales
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaOrdSerManuales", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaOrdSerManualesEntity GetValidaOrdSerManuales(long? ClvOrdSer);

    }
}


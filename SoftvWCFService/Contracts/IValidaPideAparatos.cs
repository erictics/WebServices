﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaPideAparatos
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaPideAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaPideAparatosEntity GetValidaPideAparatos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepValidaPideAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaPideAparatosEntity GetDeepValidaPideAparatos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaPideAparatosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ValidaPideAparatosEntity> GetValidaPideAparatosList(long? IdSession, long? IdDetalle);

    }
}


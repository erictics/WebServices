﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaPideSiTieneMasAparatos
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaPideSiTieneMasAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaPideSiTieneMasAparatosEntity GetValidaPideSiTieneMasAparatos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepValidaPideSiTieneMasAparatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaPideSiTieneMasAparatosEntity GetDeepValidaPideSiTieneMasAparatos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaPideSiTieneMasAparatosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ValidaPideSiTieneMasAparatosEntity> GetValidaPideSiTieneMasAparatosList(long? IdSession, long? IdDetalle, long? Contrato);


    }
}


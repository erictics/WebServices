﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaReprocesoPorContrato
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepValidaReprocesoPorContrato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaReprocesoPorContratoEntity GetDeepValidaReprocesoPorContrato(long? IdEdoCuenta);

    }
}


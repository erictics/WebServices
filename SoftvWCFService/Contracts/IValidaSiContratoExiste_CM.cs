﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidaSiContratoExiste_CM
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaSiContratoExiste_CM", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaSiContratoExiste_CMEntity GetValidaSiContratoExiste_CM(long? ContratoBueno);

    }
}


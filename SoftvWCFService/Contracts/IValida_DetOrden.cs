﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValida_DetOrden
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValida_DetOrden", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Valida_DetOrdenEntity GetValida_DetOrden(long? ClvOrden);

    }
}


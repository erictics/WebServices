﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValida_borra_servicio_New
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepValida_borra_servicio_New", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Valida_borra_servicio_NewEntity GetDeepValida_borra_servicio_New(String clv_txt, int? Id);


    }
}


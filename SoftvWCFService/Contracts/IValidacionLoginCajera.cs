﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IValidacionLoginCajera
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepValidacionLoginCajera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidacionLoginCajeraEntity GetDeepValidacionLoginCajera(String ClvUsuario, String Usuario, String Pass, int? Id);

    }
}


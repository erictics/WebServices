﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IVendedores
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepVendedores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        VendedoresEntity GetDeepVendedores(int? Clv_Vendedor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedoresList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<VendedoresEntity> GetVendedoresList(int? Op, int? ClvUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddVendedores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddVendedores(VendedoresEntity objVendedores);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateVendedores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateVendedores(VendedoresEntity objVendedores);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteVendedores", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteVendedores(int? Clv_Vendedor);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedores_dosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<VendedoresEntity> GetVendedores_dosList(int? ClvUsuario, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInserta_MovSist", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetInserta_MovSist(MovSistEntity ObjMovimientoSistema);

    }
}


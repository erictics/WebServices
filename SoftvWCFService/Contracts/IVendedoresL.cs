﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IVendedoresL
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedoresLList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<VendedoresLEntity> GetVendedoresLList(long? IdUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListTipServ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipServEntity> GetListTipServ();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListFechaCiudadCartera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<string> GetListFechaCiudadCartera(long? ClvUsuario, long? Clv_TipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListDetalleCartera", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<DetalleCarteraEntity> GetListDetalleCartera(DetalleCarteraEntity DetalleCartera);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetConGrupoVentasWeb", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<ConGrupoVentasEntity> GetConGrupoVentasWeb(ConGrupoVentasEntity obj);



    }
}


﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IVendores_ReportesVentas
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendores_ReportesVentasList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Vendores_ReportesVentasEntity> GetVendores_ReportesVentasList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendores_ReportesVentasXmlList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<Vendores_ReportesVentasEntity> GetVendores_ReportesVentasXmlList(Vendores_ReportesVentasEntity Obj, List<Muestra_PlazasPorUsuarioEntity> LstDis);

    }
}


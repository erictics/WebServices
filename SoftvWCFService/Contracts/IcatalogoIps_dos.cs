﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel; 
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IcatalogoIps_dos
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepcatalogoIps_dos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        catalogoIps_dosEntity GetDeepcatalogoIps_dos(long? IdIP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetcatalogoIps_dosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<catalogoIps_dosEntity> GetcatalogoIps_dosList(long? IdRed, int? Op, long? IdIP);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddcatalogoIps_dos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddcatalogoIps_dos(catalogoIps_dosEntity objcatalogoIps_dos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatecatalogoIps_dos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatecatalogoIps_dos(catalogoIps_dosEntity objcatalogoIps_dos);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetListCombo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<catalogoIps_dosEntity> GetListCombo();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogoIPByCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        catalogoIps_dosEntity GetCatalogoIPByCliente(int? IdCompania, int? Clv_Estado, int? Clv_Ciudad, int? Clv_Localidad, int? IdMedio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAdd_RelServicioIPTem", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetAdd_RelServicioIPTem(int? IdIP, int? Clv_UnicaNet, String FechaAsignacion);

    }
}


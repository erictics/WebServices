﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface Icompania
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getcompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        companiaEntity Getcompania();
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepcompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        companiaEntity GetDeepcompania();
       
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetcompaniaPagedList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<companiaEntity> GetcompaniaPagedList(int page, int pageSize);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetcompaniaPagedListXml", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        SoftvList<companiaEntity> GetcompaniaPagedListXml(int page, int pageSize, String xml);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Addcompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Addcompania(companiaEntity objcompania);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Updatecompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Updatecompania(companiaEntity objcompania);
        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "Deletecompania", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? Deletecompania(String BaseRemoteIp, int BaseIdUser,);

        //------------------------------------------------------------
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetcompaniaList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<companiaEntity> GetcompaniaList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDistribuidorByUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PlazaRepVariosEntity> GetDistribuidorByUsuario(int? clv_usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPlazasByDistribuidor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<companiaEntity> GetPlazasByDistribuidor(int? clv_usuario, List<PlazaRepVariosEntity> plazas);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadosByplaza", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EstadoEntity> GetEstadosByplaza(List<companiaEntity> plazas);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCiudadesBy_PlazasEstado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CiudadCAMDOEntity> GetCiudadesBy_PlazasEstado(List<companiaEntity> Plazas, List<EstadoEntity> Estados);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLocalidadesbyCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<LocalidadCAMDOEntity> GetLocalidadesbyCiudad(int? clv_usuario, List<companiaEntity> Companias, List<CiudadCAMDOEntity> Ciudades, List<EstadoEntity> Estados);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetColoniasBy_Ciudad_Localidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ColoniaCAMDOEntity> GetColoniasBy_Ciudad_Localidad(int? clv_usuario, int? banderaLocalidad, List<companiaEntity> Companias, List<EstadoEntity> Estados, List<CiudadCAMDOEntity> Ciudades, List<LocalidadCAMDOEntity> Localidades);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCallesBy_Ciudad_Localidad_Colonia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<CalleCAMDOEntity> GetCallesBy_Ciudad_Localidad_Colonia(int? clv_usuario, int? banderaLocalidad, int? banderaColonia, List<PlazaRepVariosEntity> Distribuidores,
            List<CiudadCAMDOEntity> Ciudades, List<LocalidadCAMDOEntity> Localidades, List<ColoniaCAMDOEntity> Colonias, List<companiaEntity> Companias, List<EstadoEntity> Estados);
        
     
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetServiciosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ServiciosInternetEntity>GetServiciosList(int? clv_usuario, int? clvTipServ, List<companiaEntity> Companias, int? banderaTodoSeleccionado);
     

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoClienteList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TiposClienteEntity> GetTipoClienteList();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipServicioList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipServEntity> GetTipServicioList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPeriodosRepVarList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PeriodosEntity> GetPeriodosRepVarList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMotCancelacionList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MotCanEntity> GetMotCancelacionList();


        //-----------------------------


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCreateXmlBeforeReport", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<String> GetCreateXmlBeforeReport(objPrincipal objPrincipal, objParametros objParametros,
            objRangoFechas objRangoFechas, estatusCliente estatusCliente, RepVar_DistribuidorEntity[] distriArray,
            RepVar_PlazaEntity[] plazaArray, RepVar_EstadoEntity[] estadoArray, RepVar_CiudadEntity[] ciudadArray, RepVar_LocalidadEntity[] localidadArray, RepVar_ColoniaEntity[] coloniaArray,
            RepVar_ServicioEntity[] servicioArray, RepVar_TipoClienteEntity[] tipoCliArray, RepVar_PeriodoEntity[] periodoArray, RepVar_CalleEntity[] calleArray);

        //

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_1", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_1(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_2(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_3", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_3(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_4", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_4(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_5", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_5(RepVar_datosXmlEntity reportData);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_6", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_6(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_7", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_7(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_12", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_12(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_13", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_13(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_8", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_8(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_9", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_9(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_10", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_10(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_11", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_11(RepVar_datosXmlEntity reportData);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_14", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_14(RepVar_datosXmlEntity reportData);
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_15", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_15(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDig_16", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteDig_16(RepVar_datosXmlEntity reportData);
                
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInt_1", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteInt_1(RepVar_datosXmlEntity reportData);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInt_2", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteInt_2(RepVar_datosXmlEntity reportData);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInt_3", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteInt_3(RepVar_datosXmlEntity reportData);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInt_5", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteInt_5(RepVar_datosXmlEntity reportData);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInt_7", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteInt_7(RepVar_datosXmlEntity reportData);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInt_4", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteInt_4(RepVar_datosXmlEntity reportData);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInt_6", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetReporteInt_6(RepVar_datosXmlEntity reportData);

        

    }
}


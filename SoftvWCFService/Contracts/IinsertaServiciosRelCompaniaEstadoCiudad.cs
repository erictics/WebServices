﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IinsertaServiciosRelCompaniaEstadoCiudad
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetinsertaServiciosRelCompaniaEstadoCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        insertaServiciosRelCompaniaEstadoCiudadEntity GetinsertaServiciosRelCompaniaEstadoCiudad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepinsertaServiciosRelCompaniaEstadoCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        insertaServiciosRelCompaniaEstadoCiudadEntity GetDeepinsertaServiciosRelCompaniaEstadoCiudad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetinsertaServiciosRelCompaniaEstadoCiudadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<insertaServiciosRelCompaniaEstadoCiudadEntity> GetinsertaServiciosRelCompaniaEstadoCiudadList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddinsertaServiciosRelCompaniaEstadoCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddinsertaServiciosRelCompaniaEstadoCiudad(insertaServiciosRelCompaniaEstadoCiudadEntity objinsertaServiciosRelCompaniaEstadoCiudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateinsertaServiciosRelCompaniaEstadoCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateinsertaServiciosRelCompaniaEstadoCiudad(insertaServiciosRelCompaniaEstadoCiudadEntity objinsertaServiciosRelCompaniaEstadoCiudad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteinsertaServiciosRelCompaniaEstadoCiudad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteinsertaServiciosRelCompaniaEstadoCiudad(long? clv_plaza, long? id_compania, long? Clv_Estado, long? Clv_Ciudad, long? clv_servicio);

    }
}


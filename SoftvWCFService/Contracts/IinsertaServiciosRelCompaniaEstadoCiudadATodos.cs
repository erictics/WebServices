﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IinsertaServiciosRelCompaniaEstadoCiudadATodos
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetinsertaServiciosRelCompaniaEstadoCiudadATodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        insertaServiciosRelCompaniaEstadoCiudadATodosEntity GetinsertaServiciosRelCompaniaEstadoCiudadATodos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepinsertaServiciosRelCompaniaEstadoCiudadATodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        insertaServiciosRelCompaniaEstadoCiudadATodosEntity GetDeepinsertaServiciosRelCompaniaEstadoCiudadATodos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetinsertaServiciosRelCompaniaEstadoCiudadATodosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<insertaServiciosRelCompaniaEstadoCiudadATodosEntity> GetinsertaServiciosRelCompaniaEstadoCiudadATodosList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddinsertaServiciosRelCompaniaEstadoCiudadATodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddinsertaServiciosRelCompaniaEstadoCiudadATodos(insertaServiciosRelCompaniaEstadoCiudadATodosEntity objinsertaServiciosRelCompaniaEstadoCiudadATodos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateinsertaServiciosRelCompaniaEstadoCiudadATodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateinsertaServiciosRelCompaniaEstadoCiudadATodos(insertaServiciosRelCompaniaEstadoCiudadATodosEntity objinsertaServiciosRelCompaniaEstadoCiudadATodos);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteinsertaServiciosRelCompaniaEstadoCiudadATodos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteinsertaServiciosRelCompaniaEstadoCiudadATodos(long? clv_plaza, long? clv_servicio);

    }
}


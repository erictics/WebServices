﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface Iinstalaservicios
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getinstalaservicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        instalaserviciosEntity Getinstalaservicios();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetinstalaserviciosList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<instalaserviciosEntity> GetinstalaserviciosList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Addinstalaservicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Addinstalaservicios(instalaserviciosEntity objinstalaservicios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Updateinstalaservicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Updateinstalaservicios(instalaserviciosEntity objinstalaservicios);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Deleteinstalaservicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Deleteinstalaservicios(long? Id);

    }
}


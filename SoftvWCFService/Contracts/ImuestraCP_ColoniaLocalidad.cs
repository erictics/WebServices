﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ImuestraCP_ColoniaLocalidad
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetmuestraCP_ColoniaLocalidadList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<muestraCP_ColoniaLocalidadEntity> GetmuestraCP_ColoniaLocalidadList(int? Clv_Colonia);


    }
}


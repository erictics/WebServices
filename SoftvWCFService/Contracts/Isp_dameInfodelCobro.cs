﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface Isp_dameInfodelCobro
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getsp_dameInfodelCobro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        sp_dameInfodelCobroEntity Getsp_dameInfodelCobro(long? ClvSession, long? ClvDetalle);


    }
}


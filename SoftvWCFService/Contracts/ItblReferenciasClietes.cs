﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ItblReferenciasClietes
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GettblReferenciasClietes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        tblReferenciasClietesEntity GettblReferenciasClietes(long? id_referencia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeeptblReferenciasClietes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        tblReferenciasClietesEntity GetDeeptblReferenciasClietes(long? id_referencia);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GettblReferenciasClietesList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<tblReferenciasClietesEntity> GettblReferenciasClietesList(long? contrato, String tipo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddtblReferenciasClietes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddtblReferenciasClietes(tblReferenciasClietesEntity objtblReferenciasClietes);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatetblReferenciasClietes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdatetblReferenciasClietes(tblReferenciasClietesEntity objtblReferenciasClietes);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeletetblReferenciasClietes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeletetblReferenciasClietes(long? id_referencia);

    }
}


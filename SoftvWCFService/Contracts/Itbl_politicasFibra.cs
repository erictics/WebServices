﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface Itbl_politicasFibra
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Gettbl_politicasFibra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        tbl_politicasFibraEntity Gettbl_politicasFibra(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeeptbl_politicasFibra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        tbl_politicasFibraEntity GetDeeptbl_politicasFibra(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Gettbl_politicasFibraList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<tbl_politicasFibraEntity> Gettbl_politicasFibraList();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Addtbl_politicasFibra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Addtbl_politicasFibra(tbl_politicasFibraEntity objtbl_politicasFibra);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Updatetbl_politicasFibra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Updatetbl_politicasFibra(tbl_politicasFibraEntity objtbl_politicasFibra);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Deletetbl_politicasFibra", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Deletetbl_politicasFibra(int? id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_filtroPoliticas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<tbl_politicasFibraEntity> GetSp_filtroPoliticas(String Clv_equivalente, int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSp_guardaPolitica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<tbl_politicasFibraEntity> GetSp_guardaPolitica(int? id, String Clv_equivalente, String VelSub, String VelBaj, String UnidadSub, String UnidadBaj);

    }
}


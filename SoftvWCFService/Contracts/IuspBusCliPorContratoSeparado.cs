﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IuspBusCliPorContratoSeparado
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspBusCliPorContratoSeparado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        uspBusCliPorContratoSeparadoEntity GetuspBusCliPorContratoSeparado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepuspBusCliPorContratoSeparado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        uspBusCliPorContratoSeparadoEntity GetDeepuspBusCliPorContratoSeparado();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspBusCliPorContratoSeparadoList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<uspBusCliPorContratoSeparadoEntity> GetuspBusCliPorContratoSeparadoList(String ContratoC, String Nombre, String Apellido_Paterno, String Apellido_Materno, String Calle, String Numero, int? Op, int? ClvColonia, long? Usuario, string COLONIA);


    }
}


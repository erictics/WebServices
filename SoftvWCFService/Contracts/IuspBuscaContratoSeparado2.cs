﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IuspBuscaContratoSeparado2
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspBuscaContratoSeparado2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<uspBuscaContratoSeparado2Entity> GetuspBuscaContratoSeparado2List(String ContratoCom, String Nombre, String Apellido_Paterno, String Apellido_Materno, String CALLE, String NUMERO, int? ClvColonia, String SetupBox, int? IdUsuario, int? TipoSer, int? Op);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBuscaByIdDisList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<uspBuscaContratoSeparado2Entity> GetBuscaByIdDisList(String ContratoCom, String Nombre, String Apellido_Paterno, String Apellido_Materno, String CALLE, String NUMERO, int? ClvColonia, String SetupBox, int? IdUsuario, int? TipoSer, int? Op, int? IdDistribuidor);



    }
}


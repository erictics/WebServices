﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IuspBuscaLLamadasDeInternet
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspBuscaLLamadasDeInternetList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<uspBuscaLLamadasDeInternetEntity> GetuspBuscaLLamadasDeInternetList(int? TipSer, long? NumReporte, String ContratoCom, String Nombre, String AP, String AM, String Calle, String Numero, int? clvColonia, String SetUpBox, long? IdUsuario, int? Op, int? Id_Compania, int? ClvUsuario);


    }
}


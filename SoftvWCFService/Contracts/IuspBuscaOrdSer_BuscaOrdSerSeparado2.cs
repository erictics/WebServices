﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IuspBuscaOrdSer_BuscaOrdSerSeparado2
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetuspBuscaOrdSer_BuscaOrdSerSeparado2List", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<uspBuscaOrdSer_BuscaOrdSerSeparado2Entity> GetuspBuscaOrdSer_BuscaOrdSerSeparado2List(int? Op, long? Clv_Orden, String Contrato, String Nombre, String Apellido_Paterno, String Apellido_Materno, String CALLE, String NUMERO, int? ClvColonia, int? IdCompania, String SetupBox, int? ClvUsuario, String STATUS, bool Auto);


    }
}


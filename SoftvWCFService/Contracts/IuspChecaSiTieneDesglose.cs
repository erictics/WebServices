﻿
using Globals;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IuspChecaSiTieneDesglose
    {


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeepuspChecaSiTieneDesglose", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        uspChecaSiTieneDesgloseEntity GetDeepuspChecaSiTieneDesglose(String Fecha, String ClvUsuario, int? Id);



    }
}

